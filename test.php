<form>
    <input id="timepicker_input" value="10:25 am" type="text">
</form>

<script type="text/javascript" src="<?php echo $site_url; ?>assets/js/timepicker.js"></script>
<link href="<?php echo $site_url; ?>assets/css/ng_timepicker_style.css" rel="stylesheet" type="text/css">
<link href="<?php echo $site_url; ?>assets/css/light_timepicker_style.css" rel="stylesheet" type="text/css">
<link href="assets/conponents/css/dark_timepicker_style.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
    $.ready(function () {
        var tp = new $.TimePicker({
            input: 'timepicker_input',  // the input field id
            start: '9:00 am',  // what's the first available hour
            end: '6:00 pm',  // what's the last avaliable hour
            top_hour: 12  // what's the top hour (in the clock face, 0 = midnight)
        });
    });

</script>