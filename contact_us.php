<? include("header.php"); ?>


<section class="contact-us-bg">
    <div class="container">
        <form name="frm" id="frm" method="" action="">

            <div class="row">
                <div class="con-title">
                    <h2>Contact Us <br/> <span> Send us Your Feedback </span></h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="contact-box">
                        <input class="col-md-12 col-sm-12" type="text" name="name" id="name" value="NAME">
                    </div>
                    <div class="contact-box">
                        <input class="col-md-12 col-sm-12" type="text" name="email" id="email" value="Email">
                    </div>
                    <div class="contact-box">
                        <input class="col-md-12 col-sm-12" type="text" name="phone" id="phone" value="Phone">
                    </div>
                    <div class="contact-box">
                        <input class="col-md-12 col-sm-12" type="text" name="website" id="website" value="WEBSITE">
                    </div>
                    <div class="contact-box">
                        <input class="col-md-12 col-sm-12" type="text" name="how-did" id="how-did"
                               value="HOW DID YOU FIND US?">
                    </div>
                    <div class="contact-box col-md-12 col-sm-12">
                        <input type="checkbox" name="ter_con" id="ter_con">
                        <span> I Agree all terms & condition </span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contact-box">
                        <textarea class="col-md-12 col-sm-12 contact-box-address" id="address" placeholder="ADDRESS"
                                  name="address"></textarea>
                    </div>
                    <div class="contact-box">
                        <textarea class="col-md-12 col-sm-12 contact-box-message" id="message" placeholder="MESSAGE"
                                  name="message"></textarea>
                    </div>
                    <div class="contact-box col-md-12 col-sm-12">
                        <input type="submit" name="submit" id="submit" value="SUBMIT">
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<section class="footer-contact">
    <div class="container">
        <h2 class="ftr-contact-icon mi-invisible"
            data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible, after: holdAnimClass"> Get in Touch
            <span></span></h2>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form>
                    <div class="col-md-6">
                        <input type="text" placeholder="First Name">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Last Name">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Email Address">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Phone Number">
                    </div>
                    <div class="col-md-12">
                        <textarea placeholder="Write your message"></textarea>
                    </div>
                    <div class="col-md-12 text-center">
                        <button>Send Message</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<footer class="footer-links">
    <div class="container">
        <div class="row">
            <div class="col-md-9 flinks-main">
                <ul>
                    <li><a href="about_us.php">About Us</a></li>
                    <li><a href="contact_us.php">Contact Us</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Partners</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="col-md-3 social-mda text-right">
                <ul>
                    <li><a href="#" class="fb-icon" title="Facebook"></a></li>
                    <li><a href="#" class="tw-icon" title="Twitter"></a></li>
                    <li><a href="#" class="gp-icon" title="Google Plus"></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 copyright-txt">
                <p>All rights reserved © 2015 <span class="red">servicecalibrate.com</span></p>
            </div>
        </div>
    </div>
</footer>


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>