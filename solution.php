<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="mind">
    <title>Service Calibrate</title>
    <!--<link rel="shortcut icon" type="assets/image/x-icon" href="images/favicon.png">-->
    <link rel="shortcut icon" type="assets/image" href="assets/images/favicon.png">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>

<nav class="navbar navbar-custom">
    <div class="login-register-top">
        <div class="container">
            <ul class="navbar-right">
                <li><a href="#" class="red">Login</a></li>
                <li><a href="#" class="grey37">Register</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#page-top">
                <img src="assets/images/logo.png" alt="Service Calibrate">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden"><a href="#"></a></li>
                <li><a href="#">Company</a></li>
                <li><a href="#">Solutions</a></li>
                <li><a href="#">Customers</a></li>
                <li><a href="#">Cost</a></li>
                <li><a href="#">Learn</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>


<section>
    <div class="container">
        <div class="row">
            <div class="text-center about_content_main">
                <h1> Solutions </h1>

                <p>Lorem ipsum dolor sit amet,</p>
            </div>
        </div>
    </div>
</section>

<header id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <!--<li data-target="#myCarousel" data-slide-to="2"></li>-->
    </ol>

    <!-- Wrapper for Slides -->
    <div class="carousel-inner">
        <div class="item active">
            <!-- Set the first background image using inline CSS below. -->
            <div class="fill solutions-bg-img" style="background-image:url(assets/images/solu-bg.png);"></div>
            <div class="carousel-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 table-mi-bnr solutions-bg-sub">
                            <div class="tablecell-mi"><img src="assets/images/dstributor-bnr-icon.png" alt="">

                                <h3 class="bnr-ttl btmrdlnbnr">Distributor</h3>

                                <p class="sldr-txt">How do you show your customers you are their best vendor?</p>
                                <a href="#" class="bnr-link-btn">Learn More</a></div>
                        </div>
                        <div class="col-md-6 table-mi-bnr">
                            <div class="tablecell-mi"><img src="assets/images/soli-right.png" alt=""></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <!-- Set the first background image using inline CSS below. -->
            <div class="fill solutions-bg-img" style="background-image:url(assets/images/solu-bg.png);"></div>
            <div class="carousel-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 table-mi-bnr">
                            <div class="tablecell-mi"><img src="assets/images/soli-right.png" alt=""></div>
                        </div>
                        <div class="col-md-6 table-mi-bnr solutions-bg-sub">
                            <div class="tablecell-mi"><img src="assets/images/dstributor-bnr-icon.png" alt="">

                                <h3 class="bnr-ttl btmrdlnbnr">Distributor</h3>

                                <p class="sldr-txt">How do you show your customers you are their best vendor?</p>
                                <a href="#" class="bnr-link-btn">Learn More</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="icon-prev icon-prev_sol"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span
            class="icon-next icon-next_sol"></span> </a>
</header>


<div class="container">
    <div class="row">
        <div class="col-md-offset-1">
            <div class="solutions_content col-md-11">
                <h1> Lorem ipsum dolor sit amet </h1>

                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                    massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Lorem
                    ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            </div>
        </div>
    </div>
</div>


<section class="footer-contact">
    <div class="container">
        <h2 class="ftr-contact-icon mi-invisible"
            data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible, after: holdAnimClass"> Get in Touch
            <span></span></h2>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form>
                    <div class="col-md-6">
                        <input type="text" placeholder="First Name">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Last Name">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Email Address">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Phone Number">
                    </div>
                    <div class="col-md-12">
                        <textarea placeholder="Write your message"></textarea>
                    </div>
                    <div class="col-md-12 text-center">
                        <button>Send Message</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<footer class="footer-links">
    <div class="container">
        <div class="row">
            <div class="col-md-9 flinks-main">
                <ul>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Partners</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="col-md-3 social-mda text-right">
                <ul>
                    <li><a href="#" class="fb-icon" title="Facebook"></a></li>
                    <li><a href="#" class="tw-icon" title="Twitter"></a></li>
                    <li><a href="#" class="gp-icon" title="Google Plus"></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 copyright-txt">
                <p>All rights reserved © 2015 <span class="red">servicecalibrate.com</span></p>
            </div>
        </div>
    </div>
</footer>


<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>