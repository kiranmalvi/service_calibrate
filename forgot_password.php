<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 15/4/15
 * Time: 12:59 PM
 */

require_once('include.php');

if (isset($_POST['forgot'])) {
    include_once($inc_class_path . 'user.class.php');

    $user = new user();
    $db_res = $user->check_exist_mail($_POST['email']);

    if ($_POST['email'] == "") {
        $error = 'Please Enter Email';
    } elseif (count($db_res) > 0) {
        $id = base64_encode('id=' . $db_res[0]['iUserId']);
        $DATA = array('name' => $db_res[0]['vFirstName'] . ' ' . $db_res[0]['vLastName'], 'url' => $site_url . 'reset_password.php?' . $id);
        $generalfuncobj->get_SC_SEND_mail('forgot_password', $DATA, $_POST['email'], 'Forgot Password');
        $error = "Thank You," . "</br>" . "Please check your email for a link to reset
your password";
    } else {
        $error = 'You are not Authorized User';
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.png">

    <title><?php echo $ADMIN_PANEL_TITLE ?></title>

    <link href="<?php echo $admin_url; ?>assets/js/iCheck/skins/minimal/red.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/js/iCheck/skins/square/red.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/js/iCheck/skins/flat/red.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-colorpicker/css/colorpicker.css"/>
    <!--Core CSS -->
    <link href="<?php echo $admin_url; ?>assets/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--dynamic table-->
    <link href="<?php echo $admin_url; ?>assets/js/advanced-datatable/css/demo_page.css" rel="stylesheet"/>
    <link href="<?php echo $admin_url; ?>assets/js/advanced-datatable/css/demo_table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo $admin_url; ?>assets/js/data-tables/DT_bootstrap.css"/>
    <link href="<?php echo $admin_url; ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/css/style-responsive.css" rel="stylesheet"/>


    <!-- Form CSS -->
    <link rel="stylesheet" href="<?php echo $admin_url; ?>assets/css/bootstrap-switch.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-timepicker/css/timepicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-colorpicker/css/colorpicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-datetimepicker/css/datetimepicker.css"/>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/jquery-multi-select/css/multi-select.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/jquery-tags-input/jquery.tagsinput.css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo $admin_url; ?>assets/js/select2/select2.css"/>


    <!-- Dashboard CSS -->
    <link href="<?php echo $admin_url; ?>assets/js/jvector-map/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/css/clndr.css" rel="stylesheet">
    <!--clock css-->
    <link href="<?php echo $admin_url; ?>assets/js/css3clock/css/style.css" rel="stylesheet">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo $admin_url; ?>assets/js/morris-chart/morris.css">


    <!-- Custom styles for this template -->
    <link href="<?php echo $admin_url; ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/css/style-responsive.css" rel="stylesheet"/>

    <!-- Gritter -->
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/gritter/css/jquery.gritter.css"/>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="<?php echo $admin_url; ?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo $admin_url; ?>assets/js/html5shiv.js"></script>
    <script src="<?php echo $admin_url; ?>assets/js/respond.min.js"></script>

    <!--js Required for date-picker-->
    <!--    <script src="-->
    <?php //echo $admin_url; ?><!--assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"> </script>-->
    <script src="<?php echo $admin_url; ?>assets/js/jquery-1.8.3.min.js"></script>
    <!--    <script src="--><?php //echo $admin_url; ?><!--assets/js/advanced-form.js"> </script>-->

    <!-- -->

    <script src="<?php echo $admin_url; ?>assets/js/mi_general.js"></script>
</head>
<body class="login-body">

<div class="container">

    <form class="form-signin" method="post" id="form-signin">
        <h2 class="forget_password">
            Forgot Password
            <?php
            if (isset($error)) {
                echo '<br><br>';
                echo '<span><b>' . $error . '</b></span>';
            }
            ?>

        </h2>

        <div class="login-wrap">
            <div class="user-login-info">
                <input type="text" name="email" class="form-control" placeholder="email" id="email" autofocus>
            </div>
            <input class="btn btn-lg btn-login btn-block" type="submit" value="SEND" name="forgot">
        </div>
    </form>


</div>

<link rel="stylesheet" type="text/css" href="<?= $admin_url ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.css"/>
<script type="text/javascript" src="<?= $admin_url ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.js"></script>


<!--css & js for timepicker-->

<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>


<script>
    $('.timepicker-default').timepicker({});
</script>
<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#form-signin');

            $('#form-signin').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    vQuestion: {
                        required: true
                    },
                    vAnswer: {
                        required: true
                    },
                    email: {
                        email: true,
                        required: true
                    },
                    vLink: {
                        required: true

                    },
                    eStatus: {
                        required: true
                    }

                },
                messages: {
                    vQuestion: "Please Enter Question",
                    vAnswer: "Plese Enter Answer",
                    email: {
                        required: "Please Enter Email",
                        email: "Please Enter valid Email Address "
                    },
                    vLink: {
                        required: "Please Enter Link"


                    },
                    eStatus: "Please Select Status"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#forgot').click(function () {
            $('#form-signin').submit();
        });
    });
</script>

<? include_once($admin_path . 'js_form.php'); ?>
<!--<script src="-->
<?php //echo $assets_url ?><!--plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>-->
<!--<script src="<?php /*echo $assets_url */ ?>plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="<?php /*echo $assets_url */ ?>plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!--<script src="--><?php //echo $assets_url ?><!--plugins/dist/js/pwstrength-bootstrap-1.0.0.min.js"></script>-->
<!--<script type="text/javascript" src="--><?php //echo $assets_url ?><!--js/jquery.complexify.js"></script>-->
<!--<script src="--><?php //echo $assets_url ?><!--js/file-uploader/js/main.js"></script>-->
<!--<![endif]-->
<script src="<?php echo $assets_url ?>js/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php echo $assets_url ?>bs3/js/bootstrap.min.js"></script>
<script src="<?php echo $assets_url ?>js/jquery.blockUI.js"></script>
<script src="<?php echo $assets_url ?>js/iCheck/jquery.icheck.min.js"></script>

<script src="<?php echo $assets_url ?>js/less-1.5.0.min.js"></script>

<!-- end: MAIN JAVASCRIPTS -->
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>


<link rel="stylesheet"
      href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/timepicker.css">
<link rel="stylesheet"
      href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/datetimepicker.css">
<script src="<?php echo $assets_url ?>plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?php echo $assets_url ?>plugins/bootstrap-timepicker/js/jquery-ui-timepicker-addon.js"></script>
</body>


</html>