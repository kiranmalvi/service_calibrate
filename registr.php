<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 8/4/15
 * Time: 12:07 PM
 */
include_once('include.php');

# Get Title
include_once($inc_class_path . 'user_role.class.php');
$user_role = new user_role();
$TITLE = $user_role->select_FRONT();


# Get Title
include_once($inc_class_path . 'user.class.php');
$userObj = new user();
$RETAILER = $userObj->select_API();


# Get Category
include_once($inc_class_path . 'category.class.php');
$category = new category();
$CATEGORY = $category->select_FRONT();

## GET INDUSTRIES
include_once($inc_class_path . 'industries.class.php');
$industries = new industries();
$INDUSTRIES = $industries->select_FRONT();

## GET How Hear About Us
include_once($inc_class_path . 'how_did_hear.class.php');
$how_did_hear = new how_did_hear();
$HOW_HEAR = $how_did_hear->select_FRONT();


## GET Store Type
include_once($inc_class_path . 'user_type_store.class.php');
$usertoreObj = new user_type_store();
$STORE_TYPE = $usertoreObj->select_FRONT();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Service Calibrate</title>
    <!--<link rel="shortcut icon" type="assets/image/x-icon" href="images/favicon.png">-->
    <link rel="shortcut icon" type="assets/image" href="<?php echo $site_url; ?>assets/images/favicon.png">
    <link href="<?php echo $site_url; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/custom.css" rel="stylesheet" type="text/css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo $site_url;?>assets/js/html5shiv.min.js"></script>
    <script src="<?php echo $site_url;?>assets/js/respond.min.js"></script>
    <![endif]-->


    <style>
        .rgstr-vsbl {
            opacity: 1;
            z-index: 11;
            transition-delay: 1.2s;
            -webkit-transition-delay: 1.2s;
        }

        .rgstr-novsbl {
            opacity: 0;
            z-index: 0;
            overflow: hidden;
        }

        .animated-bg-main {
            position: fixed;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            /*background:green;*/
            z-index: 10;
            overflow: hidden;

        }

        .animated-grey-bg {
            background: #373737;
            position: absolute;
            border-radius: 50%;

            height: 0px;
            width: 0px;
            margin-left: -5px;
            margin-top: -5px;

            -webkit-transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;
            transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;
        }

        .animated-white-bg {
            background: #ffffff;
            position: absolute;
            border-radius: 50%;

            height: 0px;
            width: 0px;
            margin-left: -5px;
            margin-top: -5px;

            -webkit-transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;
            transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;

            /*transition-delay:0.5s;
            -webkit-transition-delay:0.5s;*/
        }

        .animated-grey-bg.actv-bg, .animated-white-bg.actv-bg {
            height: 4000px;
            width: 4000px;
            margin-left: -2000px;
            margin-top: -2000px;
        }

        .rgstr-bck-btn-stp0 {
            background: url(assets/images/bck-arw.png) no-repeat scroll 0 3px;
            color: #373737;
            font-size: 24px;
            left: 20px;
            padding-left: 35px;
            position: absolute;
            top: 20px;
            cursor: pointer;
            z-index: 1;
        }

        .rgstr-bck-btn-stp1 {
            background: url(assets/images/bck-arw.png) no-repeat scroll 0 3px;
            color: #373737;
            font-size: 24px;
            left: 20px;
            padding-left: 35px;
            position: absolute;
            top: 20px;
            cursor: pointer;
            z-index: 1;
        }

    </style>
</head>

<body>


<!-- id="rgstr-main-option" starts -->
<section id="rgstr-main-option" class="rgstr-optn rgstr-vsbl">
    <div class="animated-bg-mi"></div>
    <div class="container-fluid text-center">
        <h2>Register</h2>

        <p>Please choose one of the following options you want to be registered as.</p>

        <div class="row rgstr-opt-tbl">
            <div class="col-md-4 rgstr-opt-tblcl">
                <h3><span>I am</span><br>Retailer<br><i class="retailer-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <p>Start measuring & comparing the service you receive
                        </span>
                    </p>
                    <a href="#rgstr-rtlr-option">Select</a>
                </div>
            </div>
            <div class="col-md-4 rgstr-opt-tblcl">
                <h3><span>I am</span><br>Manufacturer<br><i class="manufacturer-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <p>Show buyers that you will service their accounts better than their competition
                        </span>
                    </p>
                    <a href="#manufacturer-form-main">Select</a>
                </div>
            </div>
            <div class="col-md-4 rgstr-opt-tblcl">
                <h3><span>I am</span><br>Distributor<br><i class="distributor-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <p>Show your customers you are their best vendor
                        </span>
                    </p>
                    <a href="#distributor-form-main">Select</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- id="rgstr-main-option" ends -->


<!-- id="rgstr-rtlr-option" starts -->
<section id="rgstr-rtlr-option" class="rgstr-optn rgstr-novsbl">

    <div class="container-fluid text-center">
        <span class="rgstr-bck-btn-stp0">Go Back</span>

        <h2>Retailer Registration</h2>

        <p>Please choose one of the following options you want to be registered as.</p>

        <div class="row rgstr-opt-tbl">
            <div class="col-md-4 rgstr-opt-tblcl">
                <a href="#sngllocation-form-main">Select</a>

                <h3><span>I am</span><br>Owner/Manager/Franchisee<br>of a <strong>Single Location</strong><br><i
                        class="sngllctn-icon"></i>
                </h3>

                <div class="rgstr-optn-hidn-contnt">
                    <!--<p>I need to show buyers I manufacture and service better than any competitor-<span>”I’m tired of competing on price”</span></p>-->
                    <p>If you own a Single retailer Account or manage that account as an employee ,this is a perfect
                        choice to
                        manage vendors</p>

                </div>
            </div>
            <div class="col-md-4 rgstr-opt-tblcl">
                <a href="#multilocation-form-main">Select</a>

                <h3><span>I am</span><br>Owner/Manager/Franchisee<br>of a <strong>Multiple Location</strong><br><i
                        class="mltpl-lctn-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <!--<p>I need to show buyers I manufacture and service better than any competitor-<span>”I’m tired of competing on price”</span>
                    </p>-->
                    <p>If you own a more than a single retailer Account or manage multiple Accounts as an employee,this
                        is
                        a perfect choice to manage vendors,you can see all your vendors service frequencies and
                        durations of service</p>

                </div>
            </div>
            <div class="col-md-4 rgstr-opt-tblcl">
                <a href="#corporate-form-main">Select</a>

                <h3><span>I am</span><br>Owner/Manager/Franchisee<br>of a <strong>Corporate Location</strong><br><i
                        class="crprtacnt-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <!--<p>I need to show buyers I manufacture and service better than any competitor-<span>”I’m tired of competing on price”</span>
                    </p>-->
                    <p>If your company have more than 100 active retailer locations or you manage a district/territory
                        as
                        an employee please email us at sales@servicecalibrate.com to activate the account</p>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- id="rgstr-rtlr-option" ends -->


<!-- id="manufacturer-form-main" starts -->
<section id="manufacturer-form-main" class="rgstr-optn rgstr-novsbl">

    <div class="container">
        <span class="rgstr-bck-btn-stp0">Go Back</span>

        <h2><i class="manufacturer-icon icon-red-round-bg"></i><br>Manufacturer Registration</h2>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <form class="sc-rgstr-form" method="post" action="register_a.php" name="manufacturer_form"
                      id="manufacturer_form">
                    <input type="hidden" name="signup" value="manufacturer_form">

                    <p class="sc-form-dvdr"><span>Account Information</span></p>
                    <input type="text" placeholder="First Name" name="vFirstName" id="vFirstName">
                    <input type="text" placeholder="Last Name" name="vLastName" id="vLastName">


                    <p class="sc-form-dvdr"><span>Company Information</span></p>

                    <select name="roleid" id="roleid">
                        <option selected disabled>Role</option>
                        <?php
                        foreach ($TITLE as $TIT) {
                            if ($TIT['iUserTypeId'] == '3') {
                                echo '<option value="' . $TIT['iUserRoleId'] . '">' . $TIT['vName'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                    <select name="iCategoryId" id="iCategoryId">
                        <option selected disabled>Category</option>
                        <?php
                        foreach ($CATEGORY as $CAT) {
                            echo '<option value="' . $CAT['iCategoryId'] . '">' . $CAT['vCategoryName'] . '</option>';
                        }
                        ?>
                    </select>
                    <select name="iIndustriesId" id="iIndustriesId">
                        <option selected disabled>Industry</option>
                        <?php
                        foreach ($INDUSTRIES as $IND) {
                            echo '<option value="' . $IND['iIndustriesId'] . '">' . $IND['vName'] . '</option>';
                        }
                        ?>
                    </select>
                    <input type="text" placeholder="Contact Number" name="vContact" id="vContact">
                    <input type="text" placeholder="Email Address" name="vEmail" id="vEmail">
                    <input type="password" placeholder="Password" name="vPasswordmanu" id="vPasswordmanu">
                    <input type="password" placeholder="Confirm Password" name="conf_password" id="conf_password">
                    <input type="text" placeholder="Company Name" Name" name="vStoreUniqueId" id="vStoreUniqueId">


                    <p class="sc-form-dvdr"><span>Company Address</span></p>
                    <input type="text" placeholder="Street Address" name="vAddress" id="vAddress">
                    <input type="text" placeholder="Zip" name="vZip" id="vZip">
                    <input type="text" placeholder="City" name="vCity" id="vCity">
                    <input type="text" placeholder="State" name="vState" id="vState">
                    <input type="text" placeholder="Country" name="vCountry" id="vCountry" value="USA">


                    <!--  <p class="sc-form-dvdr"><span>Security Questions</span></p>

                      <div class="sc-scrtq">
                          <label><span>1</span>What is your mother's maiden name?</label> <input type="text"
                                                                                                 placeholder="Your answer here"
                                                                                                 name="sq1" id="sq1">
                          <hr>
                          <label><span>2</span>City where you born?</label>
                          <input type="text" placeholder="Your answer here" name="sq2" id="sq2">
                          <hr>
                          <label><span>3</span>First teachers name?</label>
                          <input type="text" placeholder="Your answer here" name="sq3" id="sq3">
                          <hr>
                      </div>-->

                    <p class="sc-form-dvdr"><span>How did you hear about us?</span></p>
                    <select name="aboutus" id="aboutus" onchange="getreffrencemanu(this.value)">
                        <option selected disabled>How did you hear about us?</option>
                        <?php
                        foreach ($HOW_HEAR as $HH) {
                            echo '<option value="' . $HH['iHDHId'] . '">' . $HH['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <div class="form-group" id="refemailmanu" style="display: none">
                        <label class="col-md-2 control-label"><span class="red"> </span> </label>


                        <input type="text" name="refmailmanu" id="refmailmanu"
                               placeholder="Enter Email of Refferal Person">
                    </div>

                    <p class="acpttc-rgstr text-center">
                        <input type="checkbox" id="consumer-rememberman" class="remember-chckbxx">
                        <label for="consumer-remember"></label>
                        By clicking on Register, you are accecpting <a href="#">Terms And Conditions</a
                    </p>
                    <input type="submit" value="Submit" name="submit_manufacturer" id="submit_manufacturer">
                </form>

            </div>
        </div>
    </div>
</section>
<!-- id="manufacturer-form-main" ends -->


<!-- id="distributor-form-main" starts -->
<section id="distributor-form-main" class="rgstr-optn rgstr-novsbl">

    <div class="container">
        <span class="rgstr-bck-btn-stp0">Go Back</span>

        <h2><i class="distributor-icon icon-red-round-bg"></i><br>Distributor Registration</h2>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <form class="sc-rgstr-form" method="post" action="register_a.php" name="distributor_form"
                      id="distributor_form">
                    <input type="hidden" name="signup" value="distributor_form">

                    <p class="sc-form-dvdr"><span>Account Information</span></p>


                    <input type="text" placeholder="First Name" name="vFirstName" id="vFirstName">
                    <input type="text" placeholder="Last Name" name="vLastName" id="vLastName">


                    <p class="sc-form-dvdr"><span>Company Information</span></p>
                    <select name="roleid" id="roleid">
                        <option selected disabled>Role</option>
                        <?php
                        foreach ($TITLE as $TIT) {
                            if ($TIT['iUserTypeId'] == '2') {
                                echo '<option value="' . $TIT['iUserRoleId'] . '">' . $TIT['vName'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                    <select name="iCategoryId" id="iCategoryId">
                        <option selected disabled>Category</option>
                        <?php
                        foreach ($CATEGORY as $CAT) {
                            echo '<option value="' . $CAT['iCategoryId'] . '">' . $CAT['vCategoryName'] . '</option>';
                        }
                        ?>
                    </select>
                    <select name="iIndustriesId" id="iIndustriesId">
                        <option selected disabled>Industry</option>
                        <?php
                        foreach ($INDUSTRIES as $IND) {
                            echo '<option value="' . $IND['iIndustriesId'] . '">' . $IND['vName'] . '</option>';
                        }
                        ?>
                    </select>
                    <input type="text" placeholder="Contact Number" name="vContact" id="vContact">
                    <input type="text" placeholder="Email Address" name="vEmail" id="vEmail">
                    <input type="password" placeholder="Password" name="vPassworddistri" id="vPassworddistri">
                    <input type="password" placeholder="Confirm Password" name="conf_password" id="conf_password">
                    <input type="text" placeholder="Company Name" name="vStoreUniqueId" id="vStoreUniqueId">


                    <p class="sc-form-dvdr"><span>Company Address</span></p>
                    <input type="text" placeholder="Street Address" name="vAddress" id="vAddress">
                    <input type="text" placeholder="Zip" name="vZip" id="vZip">
                    <input type="text" placeholder="City" name="vCity" id="vCity">
                    <input type="text" placeholder="State" name="vState" id="vState">
                    <input type="text" placeholder="Country" name="vCountry" id="vCountry" value="USA">>

                    <!--
                                        <p class="sc-form-dvdr"><span>Security Questions</span></p>

                                        <div class="sc-scrtq">
                                            <label><span>1</span>What is your mother's maiden name?</label> <input type="text"
                                                                                                                   placeholder="Your answer here"
                                                                                                                   name="sq1" id="sq1">
                                            <hr>
                                            <label><span>2</span>City where you born?</label>
                                            <input type="text" placeholder="Your answer here" name="sq2" id="sq2">
                                            <hr>
                                            <label><span>3</span>First teachers name?</label>
                                            <input type="text" placeholder="Your answer here" name="sq3" id="sq3">
                                            <hr>
                                        </div>-->

                    <select name="aboutus" id="aboutus" onchange="getreffrencedist(this.value)">
                        <option selected disabled>How did you hear about us?</option>
                        <?php
                        foreach ($HOW_HEAR as $HH) {
                            echo '<option value="' . $HH['iHDHId'] . '">' . $HH['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <div class="form-group" id="refemaildist" style="display: none">
                        <label class="col-md-2 control-label"><span class="red"> </span> </label>

                        <input type="text" name="refmaildist" id="refmaildist" class="form-control"
                               placeholder="Enter Email of Refferal Person">
                    </div>

                    <p class="acpttc-rgstr text-center">
                        <input type="checkbox" id="consumer-rememberdis">
                        <label for="consumer-remember"></label>
                        By clicking on Register, you are accecpting <a href="#">Terms And Conditions</a
                    </p>
                    <input type="submit" value="Submit" name="submit_distributor_store" id="submit_distributor_store">
                </form>

            </div>
        </div>
    </div>
</section>
<!-- id="distributor-form-main" ends -->


<!-- id="sngllocation-form-main" starts -->
<section id="sngllocation-form-main" class="rgstr-optn rgstr-novsbl">

    <div class="container">
        <span class="rgstr-bck-btn-stp1">Go Back</span>

        <h2><i class="sngllctn-icon icon-red-round-bg"></i><br>Single Location Register</h2>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <form class="sc-rgstr-form" method="post" action="register_a.php" name="single_store_retailer"
                      id="single_store_retailer">
                    <input type="hidden" name="signup" value="single_store_retailer">

                    <p class="sc-form-dvdr"><span>Account Information</span></p>
                    <input type="text" placeholder="Account Name" name="vStoreUniqueId" id="vStoreUniqueId">
                    <input type="text" placeholder="Account#" name="vStoreName" id="vStoreName">

                    <input type="text" placeholder="Email Address" name="vEmail" id="vEmail">
                    <input type="password" placeholder="Password" name="vPassword" id="vPassword">
                    <input type="password" placeholder="Confirm Password" name="conf_password" id="conf_password">
                    <input type="text" placeholder="Contact Number" name="vContact" id="vContact">
                    <input type="text" placeholder="First Name" name="vFirstName" id="vFirstName">
                    <input type="text" placeholder="Last Name" name="vLastName" id="vLastName">

                    <p class="sc-form-dvdr"><span>Company Address</span></p>
                    <input type="text" placeholder="Street Address" name="vAddress" id="vAddress">
                    <input type="text" placeholder="Zip" name="vZip" id="vZip">
                    <input type="text" placeholder="City" name="vCity" id="vCity">
                    <input type="text" placeholder="State" name="vState" id="vState">
                    <input type="text" placeholder="Country" name="vCountry" id="vCountry" value="USA">

                    <!--<select name="roleid" id="roleid">
                        <option selected disabled>Role</option>
                        <?php
                    /*                        foreach ($TITLE as $TIT) {
                                                if ($TIT['iUserTypeId'] == '4') {
                                                    echo '<option value="' . $TIT['iUserRoleId'] . '">' . $TIT['vName'] . '</option>';
                                                }
                                            }
                                            */
                    ?>
                    </select>-->

                    <p class="sc-form-dvdr"><span>Company Information</span></p>

                    <select name="iIndustriesId" id="iIndustriesId">
                        <option selected disabled>Industry</option>
                        <?php
                        foreach ($INDUSTRIES as $IND) {
                            echo '<option value="' . $IND['iIndustriesId'] . '">' . $IND['vName'] . '</option>';
                        }
                        ?>
                    </select>
                    <select name="storetype" id="storetype">
                        <option selected disabled>Type Of Account</option>
                        <?php
                        foreach ($STORE_TYPE as $STORE) {
                            echo '<option value="' . $STORE['iUserTypeStoreId'] . '">' . $STORE['vName'] . '</option>';
                        }
                        ?>
                    </select>


                    <p class="sc-form-dvdr"><span>Prefer Delivery time</span></p>

                    <!-- <div class="bootstrap-timepicker">

                         <input type="text" class="timepicker-default endtime" name="iPreferDeliveryTimeFrom"
                                id="iPreferDeliveryTimeFrom"
                                placeholder=" From">
                     </div>


                     <div class="bootstrap-timepicker">

                         <input type="text" class="timepicker-default endtime" name="iPreferDeliveryTimeTo"
                                id="iPreferDeliveryTimeTo"
                                placeholder=" To">
                     </div>-->
                    <input type="text" name="iPreferDeliveryTimeFrom" id="iPreferDeliveryTimeFrom" placeholder="From">
                    <input type="text" name="iPreferDeliveryTimeTo" id="iPreferDeliveryTimeTo" placeholder="To">

                    <p class="sc-form-dvdr"><span>Can you receive night delivery?</span></p>

                    <div class="sc-rdobtn">
                        <input id="eNightDelivery_Yes" value="1" type="radio" name="eNightDelivery">
                        <label for="eNightDelivery_Yes">Yes</label>

                        <input id="eNightDelivery_No" value="0" type="radio" name="eNightDelivery" checked>
                        <label for="eNightDelivery_No">No</label>
                    </div>

                    <!--   <p class="sc-form-dvdr"><span>Security Questions</span></p>

                       <div class="sc-scrtq">
                           <label><span>1</span>What is your mother's maiden name?</label> <input type="text"
                                                                                                  placeholder="Your answer here"
                                                                                                  name="sq1" id="sq1">
                           <hr>
                           <label><span>2</span>City where you born?</label>
                           <input type="text" placeholder="Your answer here" name="sq2" id="sq2">
                           <hr>
                           <label><span>3</span>First teachers name?</label>
                           <input type="text" placeholder="Your answer here" name="sq3" id="sq3">
                           <hr>
                       </div>
   -->
                    <p class="sc-form-dvdr"><span>How did you hear about us?</span></p>
                    <select name="aboutus" id="aboutus" onchange="getreffrence(this.value)">
                        <option selected disabled>How did you hear about us?</option>
                        <?php
                        foreach ($HOW_HEAR as $HH) {
                            echo '<option value="' . $HH['iHDHId'] . '">' . $HH['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <div class="form-group" id="refemail" style="display: none">
                        <label class="col-md-2 control-label"><span class="red"> </span> </label>

                        <input type="text" name="refmailsingle" id="refmailsingle" class="form-control"
                               placeholder="Enter Email of Refferal Person">
                    </div>
                    <p class="acpttc-rgstr text-center">
                        <input type="checkbox" id="consumer-remembersingle">
                        <label for="consumer-remember"></label>
                        By clicking on Register, you are accecpting <a href="#">Terms And Conditions</a
                    </p>

                    <input type="submit" value="Submit" name="submit_single_store" id="submit_single_store">
                </form>

            </div>
        </div>
    </div>
</section>
<!-- id="sngllocation-form-main" ends -->


<!-- id="multilocation-form-main" starts -->
<section id="multilocation-form-main" class="rgstr-optn rgstr-novsbl">


    <div class="container">
        <span class="rgstr-bck-btn-stp1">Go Back</span>

        <h2><i class="mltpl-lctn-icon icon-red-round-bg"></i><br>Multiple Location Register</h2>


        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <form class="sc-rgstr-form" method="post" action="register_a.php" name="multiple_store_retailer"
                      id="multiple_store_retailer">
                    <input type="hidden" name="signup" value="multiple_store_retailer">

                    <p class="sc-form-dvdr"><span>Account Information</span></p>
                    <input type="text" placeholder="Chain/Group Name" name="vStoreUniqueId" id="vStoreUniqueId">
                    <!--                    <input type="text" placeholder="Chain/Group Name" name="vStoreUniqueId" id="vStoreUniqueId"-->
                    <!--                           onchange="checkaccount(this.value)">-->
                    <!--                    -->
                    <!--                    <span id="accounterror" style="display: none">This Account Name already Exist</span>-->
                    <input type="text" placeholder="Account#" name="vStoreName" id="vStoreName">
                    <input type="text" placeholder="Number of Account" name="vStoreNumber" id="vStoreNumber">
                    <input type="text" placeholder="Email Address" name="vEmail" id="vEmail"
                           onchange="checkmail(this.value)">
                    <input type="password" placeholder="Password" name="vPasswordmulti" id="vPasswordmulti">
                    <input type="password" placeholder="Confirm Password" name="conf_password" id="conf_password">
                    <input type="text" placeholder="Contact Number" name="vContact" id="vContact">


                    <input type="text" placeholder="First Name" name="vFirstName" id="vFirstName">
                    <input type="text" placeholder="Last Name" name="vLastName" id="vLastName">

                    <p class="sc-form-dvdr"><span>Company Address</span></p>
                    <input type="text" placeholder="Street Address" name="vAddress" id="vAddress">
                    <input type="text" placeholder="Zip" name="vZip" id="vZip">
                    <input type="text" placeholder="City" name="vCity" id="vCity">
                    <input type="text" placeholder="State" name="vState" id="vState">
                    <input type="text" placeholder="Country" name="vCountry" id="vCountry" value="USA">


                    <p class="sc-form-dvdr"><span>Company Information</span></p>
                    <select name="storetype" id="storetype">
                        <option selected disabled>Type Of Account</option>
                        <?php
                        foreach ($STORE_TYPE as $STORE) {
                            echo '<option value="' . $STORE['iUserTypeStoreId'] . '">' . $STORE['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <select name="iIndustriesId" id="iIndustriesId">
                        <option selected disabled>Industry</option>
                        <?php
                        foreach ($INDUSTRIES as $IND) {
                            echo '<option value="' . $IND['iIndustriesId'] . '">' . $IND['vName'] . '</option>';
                        }
                        ?>
                    </select>


                    <p class="sc-form-dvdr"><span>Hours of Operation</span></p>
                    <input type="text" name="iHoursFrom" id="iHoursFrom" placeholder="From">
                    <input type="text" name="iHoursTo" id="iHoursTo" placeholder="To">


                    <p class="sc-form-dvdr"><span>Prefer Delivery time</span></p>
                    <input type="text" name="iPreferDeliveryTimeFrom" id="iPreferDeliveryTimeFrom" placeholder="From">
                    <input type="text" name="iPreferDeliveryTimeTo" id="iPreferDeliveryTimeTo" placeholder="To">

                    <!-- <p class="sc-form-dvdr"><span>Hours Of Operation</span></p>
                     <div class="bootstrap-timepicker">

                         <input type="text"  name="iHoursFrom"
                                id="iHoursFrom"
                                placeholder="From ">
                     </div>

                     <input type="text" class="" name="iHoursTo" id="iHoursTo" placeholder="To">

                     <p class="sc-form-dvdr"><span>Prefer Delivery time</span></p>
                     <div class="bootstrap-timepicker">

                         <input type="text"  name="iPreferDeliveryTimeFrom"
                                id="iPreferDeliveryTimeFrom"
                                placeholder="From">
                     </div>

                     <input type="text" class="" name="iPreferDeliveryTimeTo" id="iPreferDeliveryTimeTo" placeholder=" To">
 -->


                    <p class="sc-form-dvdr"><span>Can you receive night delivery?</span></p>

                    <div class="sc-rdobtn">
                        <!--<input id="eNightDelivery_Yes" value="1" type="radio" name="eNightDelivery">
                        <label for="eNightDelivery_Yes">Yes</label>

                        <input id="eNightDelivery_No" value="0" type="radio" name="eNightDelivery">
                        <label for="eNightDelivery_No">No</label>-->
                        <div class="sc-rdobtn">
                            <input id="eNightDelivery_Yes1" value="1" type="radio" name="eNightDelivery1">
                            <label for="eNightDelivery_Yes1">Yes</label>

                            <input id="eNightDelivery_No1" value="0" type="radio" name="eNightDelivery1" checked>
                            <label for="eNightDelivery_No1">No</label>
                        </div>

                    </div>

                    <!--                    <p class="sc-form-dvdr"><span>Security Questions</span></p>

                                        <div class="sc-scrtq">
                                            <label><span>1</span>What is your mother's maiden name?</label> <input type="text"
                                                                                                                   placeholder="Your answer here"
                                                                                                                   name="sq1" id="sq1">
                                            <hr>
                                            <label><span>2</span>City where you born?</label>
                                            <input type="text" placeholder="Your answer here" name="sq2" id="sq2">
                                            <hr>
                                            <label><span>3</span>First teachers name?</label>
                                            <input type="text" placeholder="Your answer here" name="sq3" id="sq3">
                                            <hr>
                                        </div>
                    -->
                    <select name="aboutus" id="aboutus" onchange="getreffrencemulti(this.value)">
                        <option selected disabled>How did you hear about us?</option>
                        <?php
                        foreach ($HOW_HEAR as $HH) {
                            echo '<option value="' . $HH['iHDHId'] . '">' . $HH['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <div class="form-group" id="refemailmulti" style="display: none">
                        <label class="col-md-2 control-label"><span class="red"> </span> </label>


                        <input type="text" name="refmailmulti" id="refmailmulti"
                               placeholder="Enter Email of Refferal Person">
                    </div>

                    <p class="acpttc-rgstr text-center">
                        <input type="checkbox" id="consumer-remembermulti">
                        <label for="consumer-remember"></label>
                        By clicking on Register, you are accecpting <a href="#">Terms And Conditions</a
                    </p>
                    <input type="submit" value="Submit" name="submit_multiple_store" id="submit_multiple_store">
                </form>

            </div>
        </div>
    </div>
</section>
<!-- id="multilocation-form-main" ends -->


<!-- id="corporate-form-main" starts -->
<section id="corporate-form-main" class="rgstr-optn rgstr-novsbl">

    <div class="container">
        <span class="rgstr-bck-btn-stp1">Go Back</span>

        <h2><i class="crprtacnt-icon icon-red-round-bg"></i><br> Corporate Location Register </h2>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <form class="sc-rgstr-form" method="post" action="register_a.php" name="corporate_store_retailer"
                      id="corporate_store_retailer">
                    <input type="hidden" name="signup" value="corporate_store_retailer">


                    <select name="parentid" id="parentid">
                        <option selected disabled>Name of Corporate Account</option>
                        <?php
                        foreach ($RETAILER as $CORPORATE) {
                            echo '<option value="' . $CORPORATE['iUserId'] . '">' . $CORPORATE['vStoreUniqueId'] . '</option>';
                        }
                        ?>
                    </select>

                    <p class="sc-form-dvdr"><span>Account Information</span></p>
                    <input type="text" placeholder="Account#" name="vStoreUniqueId" id="vStoreUniqueId">

                    <input type="text" placeholder="Email Address" name="vEmail" id="vEmail">
                    <input type="password" placeholder="Password" name="vPasswordcorporate" id="vPasswordcorporate">
                    <input type="password" placeholder="Confirm Password" name="conf_password" id="conf_password">
                    <input type="text" placeholder="Contact Number" name="vContact" id="vContact">

                    <input type="text" placeholder="First Name" name="vFirstName" id="vFirstName">
                    <input type="text" placeholder="Last Name" name="vLastName" id="vLastName">


                    <p class="sc-form-dvdr"><span>Company Address</span></p>
                    <input type="text" placeholder="Street Address" name="vAddress" id="vAddress">
                    <input type="text" placeholder="Zip" name="vZip" id="vZip">
                    <input type="text" placeholder="City" name="vCity" id="vCity">
                    <input type="text" placeholder="State" name="vState" id="vState">
                    <input type="text" placeholder="Country" name="vCountry" id="vCountry" value="USA">


                    <p class="sc-form-dvdr"><span>Company Information</span></p>
                    <select name="storetype" id="storetype">
                        <option selected disabled>Type Of Account</option>
                        <?php
                        foreach ($STORE_TYPE as $STORE) {
                            echo '<option value="' . $STORE['iUserTypeStoreId'] . '">' . $STORE['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <select name="iIndustriesId" id="iIndustriesId">
                        <option selected disabled>Industry</option>
                        <?php
                        foreach ($INDUSTRIES as $IND) {
                            echo '<option value="' . $IND['iIndustriesId'] . '">' . $IND['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <!--  <p class="sc-form-dvdr"><span>Hours Of Operation</span></p>
                      <div class="bootstrap-timepicker">

                          <input type="text"  name="iHoursFrom"
                                 id="iHoursFrom"
                                 placeholder="From ">
                      </div>
                      <input type="text" class="" name="iHoursTo" id="iHoursTo" placeholder="To">

                      <p class="sc-form-dvdr"><span>Prefer Delivery time</span></p>
                      <div class="bootstrap-timepicker">

                          <input type="text"  name="iPreferDeliveryTimeFrom"
                                 id="iPreferDeliveryTimeFrom"
                                 placeholder="From">
                      </div>

                      <input type="text" class="" name="iPreferDeliveryTimeTo" id="iPreferDeliveryTimeTo" placeholder="To">
  -->
                    <p class="sc-form-dvdr"><span>Hours of Operation</span></p>
                    <input type="text" name="iHoursFrom" id="iHoursFrom" placeholder="From">
                    <input type="text" name="iHoursTo" id="iHoursTo" placeholder="To">

                    <p class="sc-form-dvdr"><span>Prefer Delivery time</span></p>
                    <input type="text" name="iPreferDeliveryTimeFrom" id="iPreferDeliveryTimeFrom" placeholder="From">
                    <input type="text" name="iPreferDeliveryTimeTo" id="iPreferDeliveryTimeTo" placeholder="To">


                    <p class="sc-form-dvdr"><span>Can you receive night delivery?</span></p>

                    <div class="sc-rdobtn">
                        <input id="eNightDelivery_Yes2" value="1" type="radio" name="eNightDelivery2">
                        <label for="eNightDelivery_Yes2">Yes</label>

                        <input id="eNightDelivery_No2" value="0" type="radio" name="eNightDelivery2" checked>
                        <label for="eNightDelivery_No2">No</label>
                    </div>

                    <!--   <p class="sc-form-dvdr"><span>Security Questions</span></p>

                       <div class="sc-scrtq">
                           <label><span>1</span>What is your mother's maiden name?</label> <input type="text"
                                                                                                  placeholder="Your answer here"
                                                                                                  name="sq1" id="sq1">
                           <hr>
                           <label><span>2</span>City where you born?</label>
                           <input type="text" placeholder="Your answer here" name="sq2" id="sq2">
                           <hr>
                           <label><span>3</span>First teachers name?</label>
                           <input type="text" placeholder="Your answer here" name="sq3" id="sq3">
                           <hr>
                       </div>-->

                    <p class="sc-form-dvdr"><span>How did you hear about uss?</span></p>
                    <select name="aboutus" id="aboutus" onchange="getreffrencecor(this.value)">
                        <option selected disabled>How did you hear about us?</option>
                        <?php
                        foreach ($HOW_HEAR as $HH) {
                            echo '<option value="' . $HH['iHDHId'] . '">' . $HH['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <div class="form-group" id="refemailcor" style="display: none">
                        <label class="col-md-2 control-label"><span class="red"> </span> </label>


                        <input type="text" name="refmailcor" id="refmailcor"
                               placeholder="Enter Email of Refferal Person">
                    </div>

                    <p class="acpttc-rgstr text-center">
                        <input type="checkbox" id="consumer-remember">
                        <label for="consumer-remember"></label>
                        By clicking on Register, you are accecpting <a href="#">Terms And Conditions</a>
                    </p>

                    <p class="acpttc-rgstr text-center">


                        <input type="submit" value="Submit" name="submit_corporate_store" id="submit_corporate_store">
                </form>


            </div>
        </div>
    </div>
</section>
<!-- id="corporate-form-main" ends -->


<div class="animated-bg-main">
    <div class="animated-grey-bg"></div>
    <div class="animated-white-bg"></div>
</div>

<script src="<?php echo $site_url; ?>assets/js/jquery.min.js"></script>
<script src="<?php echo $site_url; ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo $site_url; ?>assets/js/anijs.js"></script>
<script src="<?php echo $site_url; ?>assets/js/anijs-helper-scrollreveal.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>
<script>
    function getreffrence(val) {

        var ref = val;
        if (ref == 1) {
            //refemail.style.display = "block";
            $('#refemail').css("display", "block");

        }
        else {
            /*  refemail.style.display = "none";*/
            $('#refemail').css("display", "none");
        }
    }
</script>

<script>
    function getreffrencecor(val) {

        var ref = val;
        if (ref == 1) {
            //refemail.style.display = "block";
            $('#refemailcor').css("display", "block");

        }
        else {
            /*  refemail.style.display = "none";*/
            $('#refemailcor').css("display", "none");
        }
    }
</script>


<script>
    function getreffrencedist(val) {

        var ref = val;
        if (ref == 1) {
            //refemail.style.display = "block";
            $('#refemaildist').css("display", "block");

        }
        else {
            /*  refemail.style.display = "none";*/
            $('#refemaildist').css("display", "none");
        }
    }
</script>

<script>
    function getreffrencemulti(val) {

        var ref = val;
        if (ref == 1) {
            //refemail.style.display = "block";
            $('#refemailmulti').css("display", "block");

        }
        else {
            /*  refemail.style.display = "none";*/
            $('#refemailmulti').css("display", "none");
        }
    }
</script>

<script>
    function getreffrencemanu(val) {

        var ref = val;
        if (ref == 1) {
            //refemail.style.display = "block";
            $('#refemailmanu').css("display", "block");

        }
        else {
            /*  refemail.style.display = "none";*/
            $('#refemailmanu').css("display", "none");
        }
    }
</script>

<!--
<script>

    function checkmail(email) {
        alert(email);

        $.ajax({
            url: '<? /*=$site_url*/ ?>register_a.php',
            type: 'POST',
            data: {"signup": 'Get_Unique_Email', "email": email},
            success: function (result) {
                alert('success');

            }
        });
    }

</script>

<script>

    function checkaccount(account) {
        alert(account);

        $.ajax({
            url: '<? /*=$site_url */ ?>register_a.php',
            type: 'POST',
            data: {"signup": 'Get_Unique_Account', "accountname": account},
            success: function (result) {
                console.log('sdf : ', result);


            }
        });
    }

</script>-->

<script>


var form_signup_single = function () {
    var run_validate_single_store_form = function () {
        $("#single_store_retailer").validate({
            rules: {
                vStoreUniqueId: {
                    required: true
                },

                vEmail: {
                    required: true,
                    email: true
                },
                vPassword: {
                    required: true,
                    minlength: "6",
                    maxlength: "10"
                },
                conf_password: {
                    required: true,
                    equalTo: "#vPassword"
                },
                vFirstName: {
                    required: true
                },
                vLastName: {
                    required: true
                },

                vAddress: {
                    required: true
                },
                vZip: {
                    required: true
                },
                vCity: {
                    required: true
                },
                vState: {
                    required: true
                },
                vCountry: {
                    required: true
                },
                iPreferDeliveryTimeFrom: {
                    required: true
                },
                iPreferDeliveryTimeTo: {
                    required: true
                },
                iIndustriesId: {
                    required: true
                },
                storetype: {
                    required: true
                },
                eNightDelivery: {
                    required: true
                },
                vContact: {
                    required: true
                }

            },
            messages: {
                vStoreUniqueId: "Please Enter Account Name",


                vEmail: {
                    required: "Please Enter Email",
                    email: "Please Enter valid Email"

                },
                vPassword: {
                    required: "Please Enter Password",
                    minlength: "Please Enter Minimum 6 Character",
                    maxlength: "Password can not be more than 10 character"

                },
                conf_password: {
                    required: "Please Enter Confirm Password",
                    equalTo: "Please Enter Confirm Password Same As Password"

                },
                vFirstName: "Please Enter First Name",
                vLastName: "Please Enter Last Name",
                vContact: "Please Enter Contact Number",
                vAddress: "Please Enter Address",
                vZip: "Please Enter Zip",
                vCity: "Please Enter City",
                vCountry: "Please Enter Country",
                vState: "Please Enter State",
                iPreferDeliveryTimeFrom: "Please Select Preferable Delivery Time Starts From",
                iPreferDeliveryTimeTo: "Please Select Preferable Delivery Time End At",
                iIndustriesId: "Please Select Industry",
                storetype: "Please Select Your Account Type",
                eNightDelivery: "Please Select Option for Night Delivery"
            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            run_validate_single_store_form();
        }
    };
}();


var form_signup_multiple = function () {
    var run_validate_multiple_store_form = function () {
        $("#multiple_store_retailer").validate({
            rules: {
                vStoreUniqueId: {
                    required: true
                },

                vStoreNumber: {
                    required: true
                },
                vEmail: {
                    required: true,
                    email: true
                },
                vPasswordmulti: {
                    required: true,
                    minlength: "6",
                    maxlength: "10"
                },
                conf_password: {
                    required: true,
                    equalTo: "#vPasswordmulti"
                },
                vFirstName: {
                    required: true
                },
                vLastName: {
                    required: true
                },

                vAddress: {
                    required: true
                },
                vZip: {
                    required: true
                },
                vCity: {
                    required: true
                },
                vCountry: {
                    required: true
                },
                vState: {
                    required: true
                },
                iPreferDeliveryTimeFrom: {
                    required: true
                },
                iPreferDeliveryTimeTo: {
                    required: true
                },
                iHoursFrom: {
                    required: true
                },
                iHoursTo: {
                    required: true
                },
                iIndustriesId: {
                    required: true
                },
                storetype: {
                    required: true
                },
                eNightDelivery: {
                    required: true
                },
                vContact: {
                    required: true
                }

            },
            messages: {
                vStoreUniqueId: "Please Enter Account Name",

                vStoreNumber: "Please Enter Number of Account",
                vEmail: {
                    required: "Please Enter Email",
                    email: "Please Eneter valid Email"

                },
                vPasswordmulti: {
                    required: "Please Enter Password",
                    minlength: "Please Enter Minimum 6 Character",
                    maxlength: "Password can not be more than 10 character"

                },
                conf_password: {
                    required: "Please Enter Confirm Password",
                    equalTo: "Please Enter Confirm Password Same As Password"

                },
                vFirstName: "Please Enter First Name",
                vLastName: "Please Enter Last Name",
                vContact: "Please Enter Contact Number",
                vAddress: "Please Enter Address",
                vZip: "Please Enter Zip",
                vCity: "Please Enter City",
                vState: "Please Enter State",
                vCountry: "Please Enter Country",
                iPreferDeliveryTimeFrom: "Please Select Preferable Delivery Time Starts From",
                iPreferDeliveryTimeTo: "Please Select Preferable Delivery Time End At",
                iHoursFrom: "Please Select Your Working Hours Starts From",
                iHoursTo: "Please Select Your Working Hours End At",
                iIndustriesId: "Please Select Industry",
                storetype: "Please Select Your Account Type",
                eNightDelivery: "Plese Select Option for Night Delivery"

            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            run_validate_multiple_store_form();
        }
    };
}();


var form_signup_corporate = function () {
    var run_validate_corporate_store_form = function () {
        $("#corporate_store_retailer").validate({
            rules: {
                vStoreUniqueId: {
                    required: true
                },

                vContact: {
                    required: true
                },
                vEmail: {
                    required: true,
                    email: true
                },
                parentid: {
                    required: true
                },
                iHoursFrom: {
                    required: true
                },
                iHoursTo: {
                    required: true
                },
                vPasswordcorporate: {
                    required: true,
                    minlength: "6",
                    maxlength: "10"
                },
                conf_password: {
                    required: true,
                    equalTo: "#vPasswordcorporate"
                },
                vFirstName: {
                    required: true
                },
                vLastName: {
                    required: true
                },
                iCategoryId: {
                    required: true
                },
                vAddress: {
                    required: true
                },
                vZip: {
                    required: true
                },
                vCity: {
                    required: true
                },
                vState: {
                    required: true
                },
                vCountry: {
                    required: true
                },
                iPreferDeliveryTimeFrom: {
                    required: true
                },
                iPreferDeliveryTimeTo: {
                    required: true
                },
                iIndustriesId: {
                    required: true
                },
                storetype: {
                    required: true
                },
                eNightDelivery: {
                    required: true
                }

            },
            messages: {
                vStoreUniqueId: "Please Enter Account Name",
                parentid: "Please Select Corporate Account",

                vContact: "Please Enter Contact Number",
                vEmail: {
                    required: "Please Enter Email",
                    email: "Please Eneter valid Email"

                },
                vPasswordcorporate: {
                    required: "Please Enter Password",
                    minlength: "Please Enter Minimum 6 Character",
                    maxlength: "Password can not be more than 10 character"
                },
                conf_password: {
                    required: "Please Enter Confirm Password",
                    equalTo: "Please Enter Confirm Password Same As Password"

                },
                vFirstName: "Please Enter First Name",
                vLastName: "Please Enter Last Name",
                iCategoryId: "Please Enter Category",
                vAddress: "Please Enter Address",
                vZip: "Please Enter Zip",
                vCity: "Please Enter City",
                vState: "Please Enter State",
                vCountry: "Please Enter Country",
                iPreferDeliveryTimeFrom: "Please Select Preferable Delivery Time Starts From",
                iPreferDeliveryTimeTo: "Please Select Preferable Delivery Time End At",
                iIndustriesId: "Please Select Industry",
                storetype: "Please Select Your Account Type",
                eNightDelivery: "Please Select Option for Night Delivery",
                iHoursFrom: "Please Select Your Working Hours Starts From",
                iHoursTo: "Please Select Your Working Hours End At"

            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            run_validate_corporate_store_form();
        }
    };
}();

var form_signup_manufacturer = function () {
    var run_validate_manufacturer_form = function () {
        $("#manufacturer_form").validate({
            rules: {
                vStoreUniqueId: {
                    required: true
                },

                vContact: {
                    required: true
                },
                vEmail: {
                    required: true,
                    email: true
                },
                vPasswordmanu: {
                    required: true,
                    minlength: "6",
                    maxlength: "10"

                },
                conf_password: {
                    required: true,
                    equalTo: "#vPasswordmanu"
                },
                vFirstName: {
                    required: true
                },
                vLastName: {
                    required: true
                },
                iCategoryId: {
                    required: true
                },
                vAddress: {
                    required: true
                },
                vZip: {
                    required: true
                },
                vCity: {
                    required: true
                },
                vState: {
                    required: true
                },
                vCountry: {
                    required: true
                },
                roleid: {
                    required: true
                },
                vStreet: {
                    required: true
                },
                iIndustriesId: {
                    required: true
                }


            },
            messages: {
                vStoreUniqueId: "Please Enter Account Name",
                vContact: "Please Enter Contact Number",
                vEmail: {
                    required: "Please Enter Email",
                    email: "Please Eneter valid Email"

                },
                vPasswordmanu: {
                    required: "Please Enter Password",
                    minlength: "Please Enter Minimum 6 Character",
                    maxlength: "Password can not be more than 10 character"

                },
                conf_password: {
                    required: "Please Enter Confirm Password",
                    equalTo: "Please Enter Confirm Password Same As Password"

                },
                vFirstName: "Please Enter First Name",
                vLastName: "Please Enter Last Name",
                iCategoryId: "Please Enter Category",
                vAddress: "Please Enter Address",
                vZip: "Please Enter Zip",
                vCity: "Please Enter City",
                vState: "Please Enter State",
                vCountry: "Please Enter Country",
                iIndustriesId: "Please Select Industry",
                roleid: "Please Select Role",
                vStreet: "Please Enter Street"
            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            run_validate_manufacturer_form();
        }
    };
}();


var form_signup_distributor = function () {
    var run_validate_distributor_form = function () {
        $("#distributor_form").validate({
            rules: {
                vStoreUniqueId: {
                    required: true
                },
                vContact: {
                    required: true
                },
                vEmail: {
                    required: true,
                    email: true
                },
                vPassworddistri: {
                    required: true,
                    minlength: "6",
                    maxlength: "10"
                },
                conf_password: {
                    required: true,
                    equalTo: "#vPassworddistri"
                },
                vFirstName: {
                    required: true
                },
                vLastName: {
                    required: true
                },
                iCategoryId: {
                    required: true
                },
                roleid: {
                    required: true
                },
                vAddress: {
                    required: true
                },
                vZip: {
                    required: true
                },
                vCity: {
                    required: true
                },
                vState: {
                    required: true
                },
                vCountry: {
                    required: true
                },
                iIndustriesId: {
                    required: true
                },
                vStreet: {
                    required: true
                }

            },
            messages: {
                vStoreUniqueId: "Please Enter Account Name",
                vContact: "Please Enter Contact Number",
                vEmail: {
                    required: "Please Enter Email",
                    email: "Please Eneter valid Email"

                },
                vPassworddistri: {
                    required: "Please Enter Password",
                    minlength: "Please Enter Minimum 6 Character",
                    maxlength: "Password can not be more than 10 character"
                },
                conf_password: {
                    required: "Please Enter Confirm Password",
                    equalTo: "Please Enter Confirm Password Same As Password"

                },
                vFirstName: "Please Enter First Name",
                vLastName: "Please Enter Last Name",
                iCategoryId: "Please Enter Category",
                vAddress: "Please Enter Address",
                vZip: "Please Enter Zip",
                vCity: "Please Enter City",
                vCountry: "Please enter Country",
                vState: "Please Enter State",
                iIndustriesId: "Please Select Industry",
                roleid: "Please Role",
                vStreet: "Please Enter Street"
            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            run_validate_distributor_form();
        }
    };
}();


</script>


<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<link rel="stylesheet" href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/timepicker.css">
<link rel="stylesheet" href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/datetimepicker.css">
<link href="<?php echo $admin_url; ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">


<script>
    $('.timepicker-default').timepicker({});
</script>

<script>

    $(document).ready(function () {
        form_signup_single.init();
        $('#submit_single_store').click(function () {
            $('#single_store_retailer').submit();
        });

        form_signup_multiple.init();
        $('#submit_multiple_store').click(function () {
            $('#multiple_store_retailer').submit();
        });

        form_signup_corporate.init();
        $('#submit_corporate_store').click(function () {
            $('#corporate_store_retailer').submit();
        });

        form_signup_manufacturer.init();
        $('#submit_manufacturer').click(function () {
            $('#manufacturer_form').submit();
        });

        form_signup_distributor.init();
        $('#submit_distributor_store').click(function () {
            $('#distributor_form').submit();
        });


        $(".rgstr-opt-tblcl").hover(
            function () {
                $(this).children(".rgstr-optn-hidn-contnt").slideDown(300);
            },
            function () {
                $(this).children(".rgstr-optn-hidn-contnt").slideUp(300);
            });


        $("a").click(function () {
            var vsblsctn = $(this).attr('href');

            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $(vsblsctn).removeClass("rgstr-novsbl");
            $(vsblsctn).addClass("rgstr-vsbl");
        });

        /*  $(".bootstrap-timepicker").click(function () {
         var vsblsctn = $(this).attr('href');

         $(vsblsctn).removeClass("rgstr-novsbl");
         $(vsblsctn).addClass("rgstr-vsbl");
         });*/


        $(".show-meridian").click(function () {
            var vsblsctn = $(this).attr('href');

            $("section.rgstr-optn").removeClass("rgstr-novsbl");
            $("section.rgstr-optn").addClass("rgstr-vsbl");
            $(vsblsctn).removeClass("rgstr-novsbl");
            $(vsblsctn).addClass("rgstr-vsbl");


        });


    });


    $(document).ready(function () {

        /*right top note starts*/
        $(".rgt-tp-note-main span").click(function () {
            $(".rgt-tp-note-main p").slideToggle();
        });
        /*right top note ends*/


        $("a").click(function () {

            var clcklft = $(this).offset().left + ($(this).width() / 2);
            var clckrgt = $(this).offset().top + ($(this).height() / 2);

            /*animation of bg starts*/
            $(".animated-grey-bg").addClass("actv-bg").css({"left": clcklft, "top": clckrgt})
                .delay(500).queue(function (next) {
                    $(".animated-white-bg").addClass("actv-bg").css({"left": clcklft, "top": clckrgt});
                    next();
                })
            /*animation of bg ends*/

            /* for display none/block section starts */

            var vsblsctn = $(this).attr('href');
            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $(vsblsctn).removeClass("rgstr-novsbl");
            $(vsblsctn).addClass("rgstr-vsbl");
            /* for display none/block section ends */
        });


        /* back button starts */
        $(".rgstr-bck-btn-stp0").click(function () {

            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $("#rgstr-main-option").removeClass("rgstr-novsbl");
            $("#rgstr-main-option").addClass("rgstr-vsbl");

            $(".animated-white-bg").removeClass("actv-bg").delay(500).queue(function (next) {
                $(".animated-grey-bg").removeClass("actv-bg");
                next();
            });

        });

        $(".rgstr-bck-btn-stp1").click(function () {

            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $("#rgstr-rtlr-option").removeClass("rgstr-novsbl");
            $("#rgstr-rtlr-option").addClass("rgstr-vsbl");


            $(".animated-white-bg").removeClass("actv-bg").delay(500).queue(function (next) {
                $(".animated-grey-bg").removeClass("actv-bg");
                next();
            });
        });
        /* back button ends */


    });
</script>


<!--css & js for timepicker-->

<? //include_once($admin_path . 'js_front_form.php');  ?>


</body>
</html>