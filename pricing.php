<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 8/4/15
 * Time: 12:07 PM
 */
include_once('include.php');

# Get Title
include_once($inc_class_path . 'user_role.class.php');
$user_role = new user_role();
$TITLE = $user_role->select_FRONT();


# Get Title
include_once($inc_class_path . 'user.class.php');
$userObj = new user();
$RETAILER = $userObj->select_API();


# Get Category
include_once($inc_class_path . 'category.class.php');
$category = new category();
$CATEGORY = $category->select_FRONT();

## GET INDUSTRIES
include_once($inc_class_path . 'industries.class.php');
$industries = new industries();
$INDUSTRIES = $industries->select_FRONT();

## GET How Hear About Us
include_once($inc_class_path . 'how_did_hear.class.php');
$how_did_hear = new how_did_hear();
$HOW_HEAR = $how_did_hear->select_FRONT();


## GET Store Type
include_once($inc_class_path . 'user_type_store.class.php');
$usertoreObj = new user_type_store();
$STORE_TYPE = $usertoreObj->select_FRONT();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Service Calibrate</title>
    <!--<link rel="shortcut icon" type="assets/image/x-icon" href="images/favicon.png">-->
    <link rel="shortcut icon" type="assets/image" href="<?php echo $site_url; ?>assets/images/favicon.png">
    <link href="<?php echo $site_url; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/custom.css" rel="stylesheet" type="text/css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo $site_url;?>assets/js/html5shiv.min.js"></script>
    <script src="<?php echo $site_url;?>assets/js/respond.min.js"></script>
    <![endif]-->


    <style>
        .rgstr-vsbl {
            opacity: 1;
            z-index: 11;
            transition-delay: 1.2s;
            -webkit-transition-delay: 1.2s;
        }

        .rgstr-novsbl {
            opacity: 0;
            z-index: 0;
            overflow: hidden;
        }

        .animated-bg-main {
            position: fixed;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            /*background:green;*/
            z-index: 10;
            overflow: hidden;

        }

        .animated-grey-bg {
            background: #373737;
            position: absolute;
            border-radius: 50%;

            height: 0px;
            width: 0px;
            margin-left: -5px;
            margin-top: -5px;

            -webkit-transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;
            transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;
        }

        .animated-white-bg {
            background: #ffffff;
            position: absolute;
            border-radius: 50%;

            height: 0px;
            width: 0px;
            margin-left: -5px;
            margin-top: -5px;

            -webkit-transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;
            transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;

            /*transition-delay:0.5s;
            -webkit-transition-delay:0.5s;*/
        }

        .animated-grey-bg.actv-bg, .animated-white-bg.actv-bg {
            height: 4000px;
            width: 4000px;
            margin-left: -2000px;
            margin-top: -2000px;
        }

        .rgstr-bck-btn-stp0 {
            background: url(assets/images/bck-arw.png) no-repeat scroll 0 3px;
            color: #373737;
            font-size: 24px;
            left: 20px;
            padding-left: 35px;
            position: absolute;
            top: 20px;
            cursor: pointer;
            z-index: 1;
        }

        .rgstr-bck-btn-stp1 {
            background: url(assets/images/bck-arw.png) no-repeat scroll 0 3px;
            color: #373737;
            font-size: 24px;
            left: 20px;
            padding-left: 35px;
            position: absolute;
            top: 20px;
            cursor: pointer;
            z-index: 1;
        }

        .bfore-thumbnail{
            background: url(assets/images/upload-rtlr.png) no-repeat center center #585858;
            border-radius:16px;
            border:none;
        }
        .upld-img-lbl{
            width: 100%;
            font-size: 24px;
            color: #373737;
            font-weight: normal;
        }

        .help-block1{
            font-size: 15px;
            color: #a94442;
        }
        .fileupload-new{
            font-size:18px;
        }

        .fileupload .btn{
            color: #373737;
            border-color: #000000;
            height: 20px;
            width: 130px;
            padding-bottom: 32px;
            padding-left: 10px;

        }
        .fileupload .btn:hover{
            text-decoration:underline;
        }
    </style>
</head>

<body>


<!-- id="rgstr-main-option" starts -->
<section id="rgstr-main-option" class="rgstr-optn rgstr-vsbl">
    <div class="animated-bg-mi"></div>
    <div class="container-fluid text-center">
        <span class="rgstr-bck-btn-stp0"><a href="index.php">Go Back</a></span>

        <h2>Pricing</h2>

        <p>Please choose one of the following options you want to see Pricing as.</p>

        <div class="row rgstr-opt-tbl">
            <div class="col-md-4 rgstr-opt-tblcl">
                <h3><span>I am</span><br>Retailer<br><i class="retailer-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <p><span>Start measuring & comparing the service you receive
                        </span>
                    </p>
                    <a href="payment_retailers.php">Select</a>
                </div>
            </div>
            <div class="col-md-4 rgstr-opt-tblcl">
                <h3><span>I am</span><br>Manufacturer<br><i class="manufacturer-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <p><span>Show buyers that you will service their accounts better than their competition
                        </span>
                    </p>
                    <a href="payment_manufacturer.php">Select</a>
                </div>
            </div>
            <div class="col-md-4 rgstr-opt-tblcl">
                <h3><span>I am</span><br>Distributor<br><i class="distributor-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <p> <span>Show your customers you are their best vendor
                        </span>
                    </p>
                    <a href="payment_distributor.php">Select</a>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- id="corporate-form-main" ends -->



<script src="<?php echo $site_url; ?>assets/js/jquery.min.js"></script>
<script src="<?php echo $site_url; ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo $site_url; ?>assets/js/anijs.js"></script>
<script src="<?php echo $site_url; ?>assets/js/anijs-helper-scrollreveal.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>




<link rel="stylesheet" type="text/css"
      href="<?php echo $admin_url; ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.css"/>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.js"></script>

<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<link rel="stylesheet" href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/timepicker.css">
<link rel="stylesheet" href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/datetimepicker.css">
<link href="<?php echo $admin_url; ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">




<script>

    $(document).ready(function () {
        form_signup_single.init();
        $('#submit_single_store').click(function () {
            checkaccount($('#vStoreUniqueId3').val(),14)
            checkmail($('#vEmail3').val(),9)
            $('#single_store_retailer').submit();
        });

        form_signup_multiple.init();
        $('#submit_multiple_store').click(function () {
            checkaccount($('#vStoreUniqueId4').val(),13)
            checkmail($('#vEmail4').val(),8)
            $('#multiple_store_retailer').submit();
        });

        form_signup_corporate.init();
        $('#submit_corporate_store').click(function () {
            checkaccount($('#vStoreUniqueId5').val(),12)
            checkmail($('#vEmail5').val(),7)
            $('#corporate_store_retailer').submit();
        });

        form_signup_manufacturer.init();
        $('#submit_manufacturer').click(function () {
            checkaccount($('#vStoreUniqueId1').val(),16)
            checkmail($('#vEmail1').val(),11)
            $('#manufacturer_form').submit();
        });

        form_signup_distributor.init();
        $('#submit_distributor_store').click(function () {
            checkaccount($('#vStoreUniqueId2').val(),15)
            checkmail($('#vEmail2').val(),10)
            $('#distributor_form').submit();
        });


        $(".rgstr-opt-tblcl").hover(
            function () {
                $(this).children(".rgstr-optn-hidn-contnt").slideDown(300);
            },
            function () {
                $(this).children(".rgstr-optn-hidn-contnt").slideUp(300);
            });


        $(".rgstr-optn-hidn-contnt > a, .rgstr-opt-tblcl > a").click(function () {
            var vsblsctn = $(this).attr('href');

            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $(vsblsctn).removeClass("rgstr-novsbl");
            $(vsblsctn).addClass("rgstr-vsbl");
        });

        /*  $(".bootstrap-timepicker").click(function () {
         var vsblsctn = $(this).attr('href');

         $(vsblsctn).removeClass("rgstr-novsbl");
         $(vsblsctn).addClass("rgstr-vsbl");
         });*/


        /*$(".show-meridian").click(function () {
         var vsblsctn = $(this).attr('href');

         $("section.rgstr-optn").removeClass("rgstr-novsbl");
         $("section.rgstr-optn").addClass("rgstr-vsbl");
         $(vsblsctn).removeClass("rgstr-novsbl");
         $(vsblsctn).addClass("rgstr-vsbl");


         });*/


    });


    $(document).ready(function () {

        /*right top note starts*/
        $(".rgt-tp-note-main span").click(function () {
            $(".rgt-tp-note-main p").slideToggle();
        });
        /*right top note starts*/ //rgt-tp-note-main-ovrly
        $(".nmbr-of-stors span").click(function () {
            $(".rgt-tp-note-main").slideToggle();
            $(".rgt-tp-note-main-ovrly").toggle();
        });
        $(".rgt-tp-note-main-ovrly").click(function () {
            $(".rgt-tp-note-main").slideToggle();
            $(".rgt-tp-note-main-ovrly").toggle();
        });
        /*right top note ends*/


        /*right top note ends*/


        $(".rgstr-optn-hidn-contnt > a, .rgstr-opt-tblcl > a").click(function () {

            var clcklft = $(this).offset().left + ($(this).width() / 2);
            var clckrgt = $(this).offset().top + ($(this).height() / 2);

            /*animation of bg starts*/
            $(".animated-grey-bg").addClass("actv-bg").css({"left": clcklft, "top": clckrgt})
                .delay(500).queue(function (next) {
                    $(".animated-white-bg").addClass("actv-bg").css({"left": clcklft, "top": clckrgt});
                    next();
                })
            /*animation of bg ends*/

            /* for display none/block section starts */

            var vsblsctn = $(this).attr('href');
            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $(vsblsctn).removeClass("rgstr-novsbl");
            $(vsblsctn).addClass("rgstr-vsbl");
            /* for display none/block section ends */
        });


        /* back button starts */
        $(".rgstr-bck-btn-stp0").click(function () {

            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $("#rgstr-main-option").removeClass("rgstr-novsbl");
            $("#rgstr-main-option").addClass("rgstr-vsbl");

            $(".animated-white-bg").removeClass("actv-bg").delay(500).queue(function (next) {
                $(".animated-grey-bg").removeClass("actv-bg");
                next();
            });

        });

        $(".rgstr-bck-btn-stp1").click(function () {

            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $("#rgstr-rtlr-option").removeClass("rgstr-novsbl");
            $("#rgstr-rtlr-option").addClass("rgstr-vsbl");


            $(".animated-white-bg").removeClass("actv-bg").delay(500).queue(function (next) {
                $(".animated-grey-bg").removeClass("actv-bg");
                next();
            });
        });
        /* back button ends */


    });
</script>




</body>
</html>