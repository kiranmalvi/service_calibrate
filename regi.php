<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 8/4/15
 * Time: 12:07 PM
 */
include_once('include.php');

# Get Title
include_once($inc_class_path . 'user_role.class.php');
$user_role = new user_role();
$TITLE = $user_role->select_FRONT();


# Get Title
include_once($inc_class_path . 'user.class.php');
$userObj = new user();
$RETAILER = $userObj->select_API();


# Get Category
include_once($inc_class_path . 'category.class.php');
$category = new category();
$CATEGORY = $category->select_FRONT();

## GET INDUSTRIES
include_once($inc_class_path . 'industries.class.php');
$industries = new industries();
$INDUSTRIES = $industries->select_FRONT();

## GET How Hear About Us
include_once($inc_class_path . 'how_did_hear.class.php');
$how_did_hear = new how_did_hear();
$HOW_HEAR = $how_did_hear->select_FRONT();


## GET Store Type
include_once($inc_class_path . 'user_type_store.class.php');
$usertoreObj = new user_type_store();
$STORE_TYPE = $usertoreObj->select_FRONT();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Service Calibrate</title>
    <!--<link rel="shortcut icon" type="assets/image/x-icon" href="images/favicon.png">-->
    <link rel="shortcut icon" type="assets/image" href="<?php echo $site_url; ?>assets/images/favicon.png">
    <link href="<?php echo $site_url; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/custom.css" rel="stylesheet" type="text/css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo $site_url;?>assets/js/html5shiv.min.js"></script>
    <script src="<?php echo $site_url;?>assets/js/respond.min.js"></script>
    <![endif]-->


    <style>
        .rgstr-vsbl {
            opacity: 1;
            z-index: 11;
            transition-delay: 1.2s;
            -webkit-transition-delay: 1.2s;
        }

        .rgstr-novsbl {
            opacity: 0;
            z-index: 0;
            overflow: hidden;
        }

        .animated-bg-main {
            position: fixed;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            /*background:green;*/
            z-index: 10;
            overflow: hidden;

        }

        .animated-grey-bg {
            background: #373737;
            position: absolute;
            border-radius: 50%;

            height: 0px;
            width: 0px;
            margin-left: -5px;
            margin-top: -5px;

            -webkit-transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;
            transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;
        }

        .animated-white-bg {
            background: #ffffff;
            position: absolute;
            border-radius: 50%;

            height: 0px;
            width: 0px;
            margin-left: -5px;
            margin-top: -5px;

            -webkit-transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;
            transition: all 1s cubic-bezier(0.215, 0.61, 0.355, 1), left 0.1ms;

            /*transition-delay:0.5s;
            -webkit-transition-delay:0.5s;*/
        }

        .animated-grey-bg.actv-bg, .animated-white-bg.actv-bg {
            height: 4000px;
            width: 4000px;
            margin-left: -2000px;
            margin-top: -2000px;
        }

        .rgstr-bck-btn-stp0 {
            background: url(assets/images/bck-arw.png) no-repeat scroll 0 3px;
            color: #373737;
            font-size: 24px;
            left: 20px;
            padding-left: 35px;
            position: absolute;
            top: 20px;
            cursor: pointer;
            z-index: 1;
        }

        .rgstr-bck-btn-stp1 {
            background: url(assets/images/bck-arw.png) no-repeat scroll 0 3px;
            color: #373737;
            font-size: 24px;
            left: 20px;
            padding-left: 35px;
            position: absolute;
            top: 20px;
            cursor: pointer;
            z-index: 1;
        }

    </style>
</head>

<body>


<!-- id="rgstr-main-option" starts -->
<section id="rgstr-main-option" class="rgstr-optn rgstr-vsbl">
    <div class="animated-bg-mi"></div>
    <div class="container-fluid text-center">
        <h2>Register</h2>

        <p>Please choose one of the following options you want to be registered as.</p>

        <div class="row rgstr-opt-tbl">
            <div class="col-md-4 rgstr-opt-tblcl">
                <h3><span>I am</span><br>Retailer<br><i class="retailer-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <p>Start measuring & comparing the service you receive
                        </span>
                    </p>
                    <a href="#rgstr-rtlr-option">Select</a>
                </div>
            </div>
            <div class="col-md-4 rgstr-opt-tblcl">
                <h3><span>I am</span><br>Manufacturer<br><i class="manufacturer-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <p>Show buyers that you will service their accounts better than their competition
                        </span>
                    </p>
                    <a href="#manufacturer-form-main">Select</a>
                </div>
            </div>
            <div class="col-md-4 rgstr-opt-tblcl">
                <h3><span>I am</span><br>Distributor<br><i class="distributor-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <p>Show your customers you are their best vendor
                        </span>
                    </p>
                    <a href="#distributor-form-main">Select</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- id="rgstr-main-option" ends -->


<!-- id="rgstr-rtlr-option" starts -->
<section id="rgstr-rtlr-option" class="rgstr-optn rgstr-novsbl">

    <div class="container-fluid text-center">
        <span class="rgstr-bck-btn-stp0">Go Back</span>

        <h2>Retailer Registration</h2>
        <p>Please choose one of the following options you want to be registered as.</p>

        <div class="row rgstr-opt-tbl">
            <div class="col-md-4 rgstr-opt-tblcl">
                <a href="#sngllocation-form-main">Select</a>

                <h3><span>I am</span><br>Owner/Manager/Franchisee<br>of a <strong>Single Location</strong><br><i
                        class="sngllctn-icon"></i>
                </h3>

                <div class="rgstr-optn-hidn-contnt">
                    <!--<p>I need to show buyers I manufacture and service better than any competitor-<span>”I’m tired of competing on price”</span></p>-->
                    <p>If you own a single retailer account or manage that account as an employee ,this is a perfect
                        choice to
                        manage vendors</p>

                </div>
            </div>
            <div class="col-md-4 rgstr-opt-tblcl">
                <a href="#multilocation-form-main">Select</a>

                <h3><span>I am</span><br>Owner/Manager/Franchisee<br>of a <strong>Multiple Location</strong><br><i
                        class="mltpl-lctn-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <!--<p>I need to show buyers I manufacture and service better than any competitor-<span>”I’m tired of competing on price”</span>
                    </p>-->
                    <p>If you own a more than a single retailer account or manage multiple accounts as an employee,this
                        is
                        a perfect choice to manage vendors,you can see all your vendors service frequencies and
                        durations of service</p>

                </div>
            </div>
            <div class="col-md-4 rgstr-opt-tblcl">
                <a href="#corporate-form-main">Select</a>

                <h3><span>I am</span><br>Owner/Manager/Franchisee<br>of a <strong>Corporate Location</strong><br><i
                        class="crprtacnt-icon"></i></h3>

                <div class="rgstr-optn-hidn-contnt">
                    <!--<p>I need to show buyers I manufacture and service better than any competitor-<span>”I’m tired of competing on price”</span>
                    </p>-->
                    <p>If your company have more than 100 active retailer locations or you manage a district/territory
                        as
                        an employee please email us at sales@servicecalibrate.com to activate the account</p>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- id="rgstr-rtlr-option" ends -->


<!-- id="manufacturer-form-main" starts -->
<section id="manufacturer-form-main" class="rgstr-optn rgstr-novsbl">

    <div class="container">
        <span class="rgstr-bck-btn-stp0">Go Back</span>

        <h2><i class="manufacturer-icon icon-red-round-bg"></i><br>Manufacturer Registration</h2>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <form class="sc-rgstr-form" method="post" action="register_a.php" name="manufacturer_form"
                      id="manufacturer_form">
                    <input type="hidden" name="signup" value="manufacturer_form">

                    <p class="sc-form-dvdr"><span>Account Information</span></p>

                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                            <div class="fileupload-preview fileupload-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
                                                   <span class="btn btn-white btn-file">
                                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                   <span class="fileupload-exists"><i
                                                           class="fa fa-undo"></i> Change</span>
                                                   <input type="file" class="default" id="vImage" name="vImage">
                                                   </span>
                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i
                                        class="fa fa-trash"></i> Remove</a>
                            </div>
                        </div>
                    </div>
                    <input type="text" placeholder="First Name" name="vFirstName" id="vFirstName">
                    <input type="text" placeholder="Last Name" name="vLastName" id="vLastName">


                    <p class="sc-form-dvdr"><span>Company Information</span></p>

                    <select name="roleid" id="roleid">
                        <option selected disabled>Role</option>
                        <?php
                        foreach ($TITLE as $TIT) {
                            if ($TIT['iUserTypeId'] == '3') {
                                echo '<option value="' . $TIT['iUserRoleId'] . '">' . $TIT['vName'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                    <select name="iCategoryId" id="iCategoryId">
                        <option selected disabled>Category</option>
                        <?php
                        foreach ($CATEGORY as $CAT) {
                            echo '<option value="' . $CAT['iCategoryId'] . '">' . $CAT['vCategoryName'] . '</option>';
                        }
                        ?>
                    </select>
                    <select name="iIndustriesId" id="iIndustriesId">
                        <option selected disabled>Industry</option>
                        <?php
                        foreach ($INDUSTRIES as $IND) {
                            echo '<option value="' . $IND['iIndustriesId'] . '">' . $IND['vName'] . '</option>';
                        }
                        ?>
                    </select>
                    <input type="text" placeholder="Contact Number" name="vContact" id="vContact">
                    <input type="text" placeholder="Email Address" name="vEmail" id="vEmail">

                    <input type="password" placeholder="Password" name="vPasswordmanu" id="vPasswordmanu">
                    <input type="password" placeholder="Confirm Password" name="conf_password" id="conf_password">
                    <input type="text" placeholder="Company Name" Name" name="vStoreUniqueId" id="vStoreUniqueId">


                    <p class="sc-form-dvdr"><span>Company Address</span></p>
                    <input type="text" placeholder="Street Address" name="vAddress" id="vAddress">
                    <input type="text" placeholder="Zip" name="vZip" id="vZip" onblur="get_locman(this.value)">
                    <input type="text" placeholder="City" name="vCity" id="cityman">
                    <input type="text" placeholder="State" name="vState" id="stateman">
                    <input type="text" placeholder="Country" name="vCountry" id="countryman" value="USA">


                    <!--  <p class="sc-form-dvdr"><span>Security Questions</span></p>

                      <div class="sc-scrtq">
                          <label><span>1</span>What is your mother's maiden name?</label> <input type="text"
                                                                                                 placeholder="Your answer here"
                                                                                                 name="sq1" id="sq1">
                          <hr>
                          <label><span>2</span>City where you born?</label>
                          <input type="text" placeholder="Your answer here" name="sq2" id="sq2">
                          <hr>
                          <label><span>3</span>First teachers name?</label>
                          <input type="text" placeholder="Your answer here" name="sq3" id="sq3">
                          <hr>
                      </div>-->

                    <p class="sc-form-dvdr"><span>How did you hear about us?</span></p>
                    <select name="aboutus" id="aboutus" onchange="getreffrencemanu(this.value)">
                        <option selected disabled>How did you hear about us?</option>
                        <?php
                        foreach ($HOW_HEAR as $HH) {
                            echo '<option value="' . $HH['iHDHId'] . '">' . $HH['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <div class="form-group" id="refemailmanu" style="display: none">
                        <label class="col-md-2 control-label"><span class="red"> </span> </label>


                        <input type="text" name="refmailmanu" id="refmailmanu"
                               placeholder="Enter Email of Refferal Person">
                    </div>

                    <p class="acpttc-rgstr text-center">
                        <input type="checkbox" id="consumer-rememberman" class="remember-chckbxx" name="manterm">
                        <label for="consumer-remember"></label>
                        By clicking on Register, you are accecpting <a href="#">Terms And Conditions</a
                    </p>
                    <input type="submit" value="Submit" name="submit_manufacturer" id="submit_manufacturer">
                </form>

            </div>
        </div>
    </div>
</section>
<!-- id="manufacturer-form-main" ends -->


<!-- id="distributor-form-main" starts -->
<section id="distributor-form-main" class="rgstr-optn rgstr-novsbl">

    <div class="container">
        <span class="rgstr-bck-btn-stp0">Go Back</span>

        <h2><i class="distributor-icon icon-red-round-bg"></i><br>Distributor Registration</h2>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <form class="sc-rgstr-form" method="post" action="register_a.php" name="distributor_form"
                      id="distributor_form">
                    <input type="hidden" name="signup" value="distributor_form">

                    <p class="sc-form-dvdr"><span>Account Information</span></p>


                    <input type="text" placeholder="First Name" name="vFirstName" id="vFirstName">
                    <input type="text" placeholder="Last Name" name="vLastName" id="vLastName">


                    <p class="sc-form-dvdr"><span>Company Information</span></p>
                    <select name="roleid" id="roleid">
                        <option selected disabled>Role</option>
                        <?php
                        foreach ($TITLE as $TIT) {
                            if ($TIT['iUserTypeId'] == '2') {
                                echo '<option value="' . $TIT['iUserRoleId'] . '">' . $TIT['vName'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                    <select name="iCategoryId" id="iCategoryId">
                        <option selected disabled>Category</option>
                        <?php
                        foreach ($CATEGORY as $CAT) {
                            echo '<option value="' . $CAT['iCategoryId'] . '">' . $CAT['vCategoryName'] . '</option>';
                        }
                        ?>
                    </select>
                    <select name="iIndustriesId" id="iIndustriesId">
                        <option selected disabled>Industry</option>
                        <?php
                        foreach ($INDUSTRIES as $IND) {
                            echo '<option value="' . $IND['iIndustriesId'] . '">' . $IND['vName'] . '</option>';
                        }
                        ?>
                    </select>
                    <input type="text" placeholder="Contact Number" name="vContact" id="vContact">
                    <input type="text" placeholder="Email Address" name="vEmail" id="vEmail">
                    <input type="password" placeholder="Password" name="vPassworddistri" id="vPassworddistri">
                    <input type="password" placeholder="Confirm Password" name="conf_password" id="conf_password">
                    <input type="text" placeholder="Company Name" name="vStoreUniqueId" id="vStoreUniqueId">


                    <p class="sc-form-dvdr"><span>Company Address</span></p>
                    <input type="text" placeholder="Street Address" name="vAddress" id="vAddress">

                    <input type="text" placeholder="Zip" name="vZip" id="vZip" onblur="get_locdis(this.value)">
                    <input type="text" placeholder="City" name="vCity" id="citydis">
                    <input type="text" placeholder="State" name="vState" id="statedis">
                    <input type="text" placeholder="Country" name="vCountry" id="countrydis" value="USA">

                    <!--
                                        <p class="sc-form-dvdr"><span>Security Questions</span></p>

                                        <div class="sc-scrtq">
                                            <label><span>1</span>What is your mother's maiden name?</label> <input type="text"
                                                                                                                   placeholder="Your answer here"
                                                                                                                   name="sq1" id="sq1">
                                            <hr>
                                            <label><span>2</span>City where you born?</label>
                                            <input type="text" placeholder="Your answer here" name="sq2" id="sq2">
                                            <hr>
                                            <label><span>3</span>First teachers name?</label>
                                            <input type="text" placeholder="Your answer here" name="sq3" id="sq3">
                                            <hr>
                                        </div>-->

                    <select name="aboutus" id="aboutus" onchange="getreffrencedist(this.value)">
                        <option selected disabled>How did you hear about us?</option>
                        <?php
                        foreach ($HOW_HEAR as $HH) {
                            echo '<option value="' . $HH['iHDHId'] . '">' . $HH['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <div class="form-group" id="refemaildist" style="display: none">
                        <label class="col-md-2 control-label"><span class="red"> </span> </label>

                        <input type="text" name="refmaildist" id="refmaildist" class="form-control"
                               placeholder="Enter Email of Refferal Person">
                    </div>

                    <p class="acpttc-rgstr text-center">
                        <input type="checkbox" id="consumer-rememberdis" name="disterm">
                        <label for="consumer-remember"></label>
                        By clicking on Register, you are accecpting <a href="#">Terms And Conditions</a
                    </p>
                    <input type="submit" value="Submit" name="submit_distributor_store" id="submit_distributor_store">
                </form>

            </div>
        </div>
    </div>
</section>
<!-- id="distributor-form-main" ends -->


<!-- id="sngllocation-form-main" starts -->
<section id="sngllocation-form-main" class="rgstr-optn rgstr-novsbl">

    <div class="container">
        <span class="rgstr-bck-btn-stp1">Go Back</span>

        <h2><i class="sngllctn-icon icon-red-round-bg"></i><br>Single Location Register</h2>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <form class="sc-rgstr-form" method="post" action="register_a.php" name="single_store_retailer"
                      id="single_store_retailer">
                    <input type="hidden" name="signup" value="single_store_retailer">

                    <p class="sc-form-dvdr"><span>Account Information</span></p>
                    <input type="text" placeholder="Account Name" name="vStoreUniqueId" id="vStoreUniqueId">
                    <input type="text" placeholder="Account#" name="vStoreName" id="vStoreName">

                    <input type="text" placeholder="Email Address" name="vEmail" id="vEmail">
                    <input type="password" placeholder="Password" name="vPassword" id="vPassword">
                    <input type="password" placeholder="Confirm Password" name="conf_password" id="conf_password">
                    <input type="text" placeholder="Contact Number" name="vContact" id="vContact">
                    <input type="text" placeholder="First Name" name="vFirstName" id="vFirstName">
                    <input type="text" placeholder="Last Name" name="vLastName" id="vLastName">
                    <p class="sc-form-dvdr"><span>Company Address</span></p>
                    <input type="text" placeholder="Street Address" name="vAddress" id="vAddress">
                    <input type="text" placeholder="Zip" name="vZip" id="vZip" onblur="get_locsingle(this.value)">
                    <input type="text" placeholder="City" name="vCity" id="citysingle">
                    <input type="text" placeholder="State" name="vState" id="statesingle">
                    <input type="text" placeholder="Country" name="vCountry" id="countrysingle" value="USA">

                    <!--<select name="roleid" id="roleid">
                        <option selected disabled>Role</option>
                        <?php
                    /*                        foreach ($TITLE as $TIT) {
                                                if ($TIT['iUserTypeId'] == '4') {
                                                    echo '<option value="' . $TIT['iUserRoleId'] . '">' . $TIT['vName'] . '</option>';
                                                }
                                            }
                                            */
                    ?>
                    </select>-->

                    <p class="sc-form-dvdr"><span>Company Information</span></p>

                    <select name="iIndustriesId" id="iIndustriesId">
                        <option selected disabled>Industry</option>
                        <?php
                        foreach ($INDUSTRIES as $IND) {
                            echo '<option value="' . $IND['iIndustriesId'] . '">' . $IND['vName'] . '</option>';
                        }
                        ?>
                    </select>
                    <select name="storetype" id="storetype">
                        <option selected disabled>Type Of Account</option>
                        <?php
                        foreach ($STORE_TYPE as $STORE) {
                            echo '<option value="' . $STORE['iUserTypeStoreId'] . '">' . $STORE['vName'] . '</option>';
                        }
                        ?>
                    </select>


                    <p class="sc-form-dvdr"><span>Prefer Delivery time</span></p>

                    <!-- <div class="bootstrap-timepicker">

                         <input type="text" class="timepicker-default endtime" name="iPreferDeliveryTimeFrom"
                                id="iPreferDeliveryTimeFrom"
                                placeholder=" From">
                     </div>


                     <div class="bootstrap-timepicker">

                         <input type="text" class="timepicker-default endtime" name="iPreferDeliveryTimeTo"
                                id="iPreferDeliveryTimeTo"
                                placeholder=" To">
                     </div>-->
                    <select id="iPreferDeliveryTimeFromsngle" name="iPreferDeliveryTimeFrom" onchange="get_diff()">

                        <option selected disabled> Select Your Prefered Delivery Hours Starts From</option>
                        <option value="01:00 AM"> 01:00 AM</option>
                        <option value="02:00 AM"> 02:00 AM</option>
                        <option value="03:00 AM"> 03:00 AM</option>
                        <option value="04:00 AM"> 04:00 AM</option>
                        <option value="05:00 AM"> 05:00 AM</option>
                        <option value="06:00 AM"> 06:00 AM</option>
                        <option value="07:00 AM"> 07:00 AM</option>
                        <option value="08:00 AM"> 08:00 AM</option>
                        <option value="09:00 AM"> 09:00 AM</option>
                        <option value="10:00 AM"> 10:00 AM</option>
                        <option value="11:00 AM"> 11:00 AM</option>
                        <option value="12:00 AM"> 12:00 AM</option>
                        <option value="01:00 PM"> 01:00 PM</option>
                        <option value="02:00 PM"> 02:00 PM</option>
                        <option value="03:00 PM"> 03:00 PM</option>
                        <option value="04:00 PM"> 04:00 PM</option>
                        <option value="05:00 PM"> 05:00 PM</option>
                        <option value="06:00 PM"> 06:00 PM</option>
                        <option value="07:00 PM"> 07:00 PM</option>
                        <option value="08:00 PM"> 08:00 PM</option>
                        <option value="09:00 PM"> 09:00 PM</option>
                        <option value="10:00 PM"> 10:00 PM</option>
                        <option value="11:00 PM"> 11:00 PM</option>
                        <option value="12:00 PM"> 12:00 PM</option>
                    </select>

                    <select id="iPreferDeliveryTimeTosngle" name="iPreferDeliveryTimeTo" onchange="get_diff()">

                        <option selected disabled> Select Your Prefered Delivery Hours End At</option>
                        <option value="01:00 AM"> 01:00 AM</option>
                        <option value="02:00 AM"> 02:00 AM</option>
                        <option value="03:00 AM"> 03:00 AM</option>
                        <option value="04:00 AM"> 04:00 AM</option>
                        <option value="05:00 AM"> 05:00 AM</option>
                        <option value="06:00 AM"> 06:00 AM</option>
                        <option value="07:00 AM"> 07:00 AM</option>
                        <option value="08:00 AM"> 08:00 AM</option>
                        <option value="09:00 AM"> 09:00 AM</option>
                        <option value="10:00 AM"> 10:00 AM</option>
                        <option value="11:00 AM"> 11:00 AM</option>
                        <option value="12:00 AM"> 12:00 AM</option>
                        <option value="01:00 PM"> 01:00 PM</option>
                        <option value="02:00 PM"> 02:00 PM</option>
                        <option value="03:00 PM"> 03:00 PM</option>
                        <option value="04:00 PM"> 04:00 PM</option>
                        <option value="05:00 PM"> 05:00 PM</option>
                        <option value="06:00 PM"> 06:00 PM</option>
                        <option value="07:00 PM"> 07:00 PM</option>
                        <option value="08:00 PM"> 08:00 PM</option>
                        <option value="09:00 PM"> 09:00 PM</option>
                        <option value="10:00 PM"> 10:00 PM</option>
                        <option value="11:00 PM"> 11:00 PM</option>
                        <option value="12:00 PM"> 12:00 PM</option>
                    </select>

                    <span id="diffmsgsngle" name="diffmsgsngle" style="display: none"> Please select time which have minimum gape of 6 hours </span>
                    <p class="sc-form-dvdr"><span>Can you receive night delivery?</span></p>
                    <div class="sc-rdobtn">
                        <input id="eNightDelivery_Yes" value="1" type="radio" name="eNightDelivery">
                        <label for="eNightDelivery_Yes">Yes</label>

                        <input id="eNightDelivery_No" value="0" type="radio" name="eNightDelivery" checked>
                        <label for="eNightDelivery_No">No</label>
                    </div>

                    <!--   <p class="sc-form-dvdr"><span>Security Questions</span></p>

                       <div class="sc-scrtq">
                           <label><span>1</span>What is your mother's maiden name?</label> <input type="text"
                                                                                                  placeholder="Your answer here"
                                                                                                  name="sq1" id="sq1">
                           <hr>
                           <label><span>2</span>City where you born?</label>
                           <input type="text" placeholder="Your answer here" name="sq2" id="sq2">
                           <hr>
                           <label><span>3</span>First teachers name?</label>
                           <input type="text" placeholder="Your answer here" name="sq3" id="sq3">
                           <hr>
                       </div>
   -->
                    <p class="sc-form-dvdr"><span>How did you hear about us?</span></p>
                    <select name="aboutus" id="aboutus" onchange="getreffrence(this.value)">
                        <option selected disabled>How did you hear about us?</option>
                        <?php
                        foreach ($HOW_HEAR as $HH) {
                            echo '<option value="' . $HH['iHDHId'] . '">' . $HH['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <div class="form-group" id="refemail" style="display: none">
                        <label class="col-md-2 control-label"><span class="red"> </span> </label>

                        <input type="text" name="refmailsingle" id="refmailsingle" class="form-control"
                               placeholder="Enter Email of Refferal Person">
                    </div>
                    <p class="acpttc-rgstr text-center">
                        <input type="checkbox" id="consumer-remembersingle" name="snglterm">
                        <label for="consumer-remember"></label>
                        By clicking on Register, you are accecpting <a href="#">Terms And Conditions</a
                    </p>

                    <input type="submit" value="Submit" name="submit_single_store" id="submit_single_store">
                </form>

            </div>
        </div>
    </div>
</section>
<!-- id="sngllocation-form-main" ends -->


<!-- id="multilocation-form-main" starts -->
<section id="multilocation-form-main" class="rgstr-optn rgstr-novsbl">

<div class="rgt-tp-note-main">
    <span>How to add Multiple Locations</span>

    <p>If this is going to be a administration account, please complete registration then you have the flexibility
        to upload multiple accounts at a time with simple excel upload.<br>Others please continue</p>
</div>
<div class="rgt-tp-note-main-ovrly"></div>

    <div class="container">
        <span class="rgstr-bck-btn-stp1">Go Back</span>
        <h2><i class="mltpl-lctn-icon icon-red-round-bg"></i><br>Multiple Location Register</h2>


        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <form class="sc-rgstr-form" method="post" action="register_a.php" name="multiple_store_retailer"
                      id="multiple_store_retailer">
                    <input type="hidden" name="signup" value="multiple_store_retailer">

                    <p class="sc-form-dvdr"><span>Account Information</span></p>
                    <input type="text" placeholder="Chain/Group Name" name="vStoreUniqueId" id="vStoreUniqueId">
                    <!--                    <input type="text" placeholder="Chain/Group Name" name="vStoreUniqueId" id="vStoreUniqueId"-->
                    <!--                           onchange="checkaccount(this.value)">-->
                    <!--                    -->
                    <!--                    <span id="accounterror" style="display: none">This Account Name already Exist</span>-->
                    <input type="text" placeholder="Account#" name="vStoreName" id="vStoreName">

                    <div class="nmbr-of-stors">
                        <span>?</span>
                        <input type="text" placeholder="Number of Store" name="vStoreNumber" id="vStoreNumber">
                    </div>
                    <input type="text" placeholder="Email Address" name="vEmail" id="vEmail"
                           onchange="checkmail(this.value)">
                    <input type="password" placeholder="Password" name="vPasswordmulti" id="vPasswordmulti">
                    <input type="password" placeholder="Confirm Password" name="conf_password" id="conf_password">
                    <input type="text" placeholder="Contact Number" name="vContact" id="vContact">



                    <input type="text" placeholder="First Name" name="vFirstName" id="vFirstName">
                    <input type="text" placeholder="Last Name" name="vLastName" id="vLastName">
                    <p class="sc-form-dvdr"><span>Company Address</span></p>
                    <input type="text" placeholder="Street Address" name="vAddress" id="vAddress">
                    <input type="text" placeholder="Zip" name="vZip" id="vZip" onblur="get_locmulti(this.value)">
                    <input type="text" placeholder="City" name="vCity" id="citymulti">
                    <input type="text" placeholder="State" name="vState" id="statemulti">
                    <input type="text" placeholder="Country" name="vCountry" id="countrymulti" value="USA">


                    <p class="sc-form-dvdr"><span>Company Information</span></p>
                    <select name="storetype" id="storetype">
                        <option selected disabled>Type Of Account</option>
                        <?php
                        foreach ($STORE_TYPE as $STORE) {
                            echo '<option value="' . $STORE['iUserTypeStoreId'] . '">' . $STORE['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <select name="iIndustriesId" id="iIndustriesId">
                        <option selected disabled>Industry</option>
                        <?php
                        foreach ($INDUSTRIES as $IND) {
                            echo '<option value="' . $IND['iIndustriesId'] . '">' . $IND['vName'] . '</option>';
                        }
                        ?>
                    </select>



                    <p class="sc-form-dvdr"><span>Hours of Operation</span></p>
                    <select id="iHoursFrommulti" name="iHoursFrom" onchange="get_diffmultiwh()">

                        <option selected disabled> Select Hours of Operation Starts From</option>
                        <option value="01:00 AM"> 01:00 AM</option>
                        <option value="02:00 AM"> 02:00 AM</option>
                        <option value="03:00 AM"> 03:00 AM</option>
                        <option value="04:00 AM"> 04:00 AM</option>
                        <option value="05:00 AM"> 05:00 AM</option>
                        <option value="06:00 AM"> 06:00 AM</option>
                        <option value="07:00 AM"> 07:00 AM</option>
                        <option value="08:00 AM"> 08:00 AM</option>
                        <option value="09:00 AM"> 09:00 AM</option>
                        <option value="10:00 AM"> 10:00 AM</option>
                        <option value="11:00 AM"> 11:00 AM</option>
                        <option value="12:00 AM"> 12:00 AM</option>
                        <option value="01:00 PM"> 01:00 PM</option>
                        <option value="02:00 PM"> 02:00 PM</option>
                        <option value="03:00 PM"> 03:00 PM</option>
                        <option value="04:00 PM"> 04:00 PM</option>
                        <option value="05:00 PM"> 05:00 PM</option>
                        <option value="06:00 PM"> 06:00 PM</option>
                        <option value="07:00 PM"> 07:00 PM</option>
                        <option value="08:00 PM"> 08:00 PM</option>
                        <option value="09:00 PM"> 09:00 PM</option>
                        <option value="10:00 PM"> 10:00 PM</option>
                        <option value="11:00 PM"> 11:00 PM</option>
                        <option value="12:00 PM"> 12:00 PM</option>
                    </select>

                    <select id="iHoursTomulti" name="iHoursTo" onchange="get_diffmultiwh()">

                        <option selected disabled> Select Hours of Operation End At</option>
                        <option value="01:00 AM"> 01:00 AM</option>
                        <option value="02:00 AM"> 02:00 AM</option>
                        <option value="03:00 AM"> 03:00 AM</option>
                        <option value="04:00 AM"> 04:00 AM</option>
                        <option value="05:00 AM"> 05:00 AM</option>
                        <option value="06:00 AM"> 06:00 AM</option>
                        <option value="07:00 AM"> 07:00 AM</option>
                        <option value="08:00 AM"> 08:00 AM</option>
                        <option value="09:00 AM"> 09:00 AM</option>
                        <option value="10:00 AM"> 10:00 AM</option>
                        <option value="11:00 AM"> 11:00 AM</option>
                        <option value="12:00 AM"> 12:00 AM</option>
                        <option value="01:00 PM"> 01:00 PM</option>
                        <option value="02:00 PM"> 02:00 PM</option>
                        <option value="03:00 PM"> 03:00 PM</option>
                        <option value="04:00 PM"> 04:00 PM</option>
                        <option value="05:00 PM"> 05:00 PM</option>
                        <option value="06:00 PM"> 06:00 PM</option>
                        <option value="07:00 PM"> 07:00 PM</option>
                        <option value="08:00 PM"> 08:00 PM</option>
                        <option value="09:00 PM"> 09:00 PM</option>
                        <option value="10:00 PM"> 10:00 PM</option>
                        <option value="11:00 PM"> 11:00 PM</option>
                        <option value="12:00 PM"> 12:00 PM</option>
                    </select>

                    <span id="diffmsgwhmulti" name="diffmsgwhmulti" style="display: none"> Please select time which have minimum gape of 6 hours </span>
                    <p class="sc-form-dvdr"><span>Prefer Delivery time</span></p>

                    <select id="iPreferDeliveryTimeFrommulti" name="iPreferDeliveryTimeFrom"
                            onchange="get_diffmultipr()">

                        <option selected disabled> Select Your Prefered Delivery Hours Starts From</option>
                        <option value="01:00 AM"> 01:00 AM</option>
                        <option value="02:00 AM"> 02:00 AM</option>
                        <option value="03:00 AM"> 03:00 AM</option>
                        <option value="04:00 AM"> 04:00 AM</option>
                        <option value="05:00 AM"> 05:00 AM</option>
                        <option value="06:00 AM"> 06:00 AM</option>
                        <option value="07:00 AM"> 07:00 AM</option>
                        <option value="08:00 AM"> 08:00 AM</option>
                        <option value="09:00 AM"> 09:00 AM</option>
                        <option value="10:00 AM"> 10:00 AM</option>
                        <option value="11:00 AM"> 11:00 AM</option>
                        <option value="12:00 AM"> 12:00 AM</option>
                        <option value="01:00 PM"> 01:00 PM</option>
                        <option value="02:00 PM"> 02:00 PM</option>
                        <option value="03:00 PM"> 03:00 PM</option>
                        <option value="04:00 PM"> 04:00 PM</option>
                        <option value="05:00 PM"> 05:00 PM</option>
                        <option value="06:00 PM"> 06:00 PM</option>
                        <option value="07:00 PM"> 07:00 PM</option>
                        <option value="08:00 PM"> 08:00 PM</option>
                        <option value="09:00 PM"> 09:00 PM</option>
                        <option value="10:00 PM"> 10:00 PM</option>
                        <option value="11:00 PM"> 11:00 PM</option>
                        <option value="12:00 PM"> 12:00 PM</option>
                    </select>

                    <select id="iPreferDeliveryTimeTomulti" name="iPreferDeliveryTimeTo" onchange="get_diffmultipr()">

                        <option selected disabled> Select Your Prefered Delivery Hours End At</option>
                        <option value="01:00 AM"> 01:00 AM</option>
                        <option value="02:00 AM"> 02:00 AM</option>
                        <option value="03:00 AM"> 03:00 AM</option>
                        <option value="04:00 AM"> 04:00 AM</option>
                        <option value="05:00 AM"> 05:00 AM</option>
                        <option value="06:00 AM"> 06:00 AM</option>
                        <option value="07:00 AM"> 07:00 AM</option>
                        <option value="08:00 AM"> 08:00 AM</option>
                        <option value="09:00 AM"> 09:00 AM</option>
                        <option value="10:00 AM"> 10:00 AM</option>
                        <option value="11:00 AM"> 11:00 AM</option>
                        <option value="12:00 AM"> 12:00 AM</option>
                        <option value="01:00 PM"> 01:00 PM</option>
                        <option value="02:00 PM"> 02:00 PM</option>
                        <option value="03:00 PM"> 03:00 PM</option>
                        <option value="04:00 PM"> 04:00 PM</option>
                        <option value="05:00 PM"> 05:00 PM</option>
                        <option value="06:00 PM"> 06:00 PM</option>
                        <option value="07:00 PM"> 07:00 PM</option>
                        <option value="08:00 PM"> 08:00 PM</option>
                        <option value="09:00 PM"> 09:00 PM</option>
                        <option value="10:00 PM"> 10:00 PM</option>
                        <option value="11:00 PM"> 11:00 PM</option>
                        <option value="12:00 PM"> 12:00 PM</option>
                    </select>
                    <span id="diffmsgprmulti" name="diffmsgprmulti" style="display: none"> Please select time which have minimum gape of 6 hours </span>

                    <!-- <p class="sc-form-dvdr"><span>Hours Of Operation</span></p>
                     <div class="bootstrap-timepicker">

                         <input type="text"  name="iHoursFrom"
                                id="iHoursFrom"
                                placeholder="From ">
                     </div>

                     <input type="text" class="" name="iHoursTo" id="iHoursTo" placeholder="To">

                     <p class="sc-form-dvdr"><span>Prefer Delivery time</span></p>
                     <div class="bootstrap-timepicker">

                         <input type="text"  name="iPreferDeliveryTimeFrom"
                                id="iPreferDeliveryTimeFrom"
                                placeholder="From">
                     </div>

                     <input type="text" class="" name="iPreferDeliveryTimeTo" id="iPreferDeliveryTimeTo" placeholder=" To">
 -->


                    <p class="sc-form-dvdr"><span>Can you receive night delivery?</span></p>

                    <div class="sc-rdobtn">
                        <!--<input id="eNightDelivery_Yes" value="1" type="radio" name="eNightDelivery">
                        <label for="eNightDelivery_Yes">Yes</label>

                        <input id="eNightDelivery_No" value="0" type="radio" name="eNightDelivery">
                        <label for="eNightDelivery_No">No</label>-->
                        <div class="sc-rdobtn">
                            <input id="eNightDelivery_Yes1" value="1" type="radio" name="eNightDelivery1">
                            <label for="eNightDelivery_Yes1">Yes</label>

                            <input id="eNightDelivery_No1" value="0" type="radio" name="eNightDelivery1" checked>
                            <label for="eNightDelivery_No1">No</label>
                        </div>

                    </div>

                    <!--                    <p class="sc-form-dvdr"><span>Security Questions</span></p>

                                        <div class="sc-scrtq">
                                            <label><span>1</span>What is your mother's maiden name?</label> <input type="text"
                                                                                                                   placeholder="Your answer here"
                                                                                                                   name="sq1" id="sq1">
                                            <hr>
                                            <label><span>2</span>City where you born?</label>
                                            <input type="text" placeholder="Your answer here" name="sq2" id="sq2">
                                            <hr>
                                            <label><span>3</span>First teachers name?</label>
                                            <input type="text" placeholder="Your answer here" name="sq3" id="sq3">
                                            <hr>
                                        </div>
                    -->
                    <select name="aboutus" id="aboutus" onchange="getreffrencemulti(this.value)">
                        <option selected disabled>How did you hear about us?</option>
                        <?php
                        foreach ($HOW_HEAR as $HH) {
                            echo '<option value="' . $HH['iHDHId'] . '">' . $HH['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <div class="form-group" id="refemailmulti" style="display: none">
                        <label class="col-md-2 control-label"><span class="red"> </span> </label>


                        <input type="text" name="refmailmulti" id="refmailmulti"
                               placeholder="Enter Email of Refferal Person">
                    </div>

                    <p class="acpttc-rgstr text-center">
                        <input type="checkbox" id="consumer-remembermulti" name="multiterm">
                        <label for="consumer-remember"></label>
                        By clicking on Register, you are accecpting <a href="#">Terms And Conditions</a
                    </p>
                    <input type="submit" value="Submit" name="submit_multiple_store" id="submit_multiple_store">
                </form>

            </div>
        </div>
    </div>
</section>
<!-- id="multilocation-form-main" ends -->


<!-- id="corporate-form-main" starts -->
<section id="corporate-form-main" class="rgstr-optn rgstr-novsbl">

    <div class="container">
        <span class="rgstr-bck-btn-stp1">Go Back</span>

        <h2><i class="crprtacnt-icon icon-red-round-bg"></i><br> Corporate Location Register </h2>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <form class="sc-rgstr-form" method="post" action="register_a.php" name="corporate_store_retailer"
                      id="corporate_store_retailer">
                    <input type="hidden" name="signup" value="corporate_store_retailer">


                    <select name="parentid" id="parentid">
                        <option selected disabled>Name of Corporate Account</option>
                        <?php
                        foreach ($RETAILER as $CORPORATE) {
                            echo '<option value="' . $CORPORATE['iUserId'] . '">' . $CORPORATE['vStoreUniqueId'] . '</option>';
                        }
                        ?>
                    </select>

                    <p class="sc-form-dvdr"><span>Account Information</span></p>
                    <input type="text" placeholder="Account#" name="vStoreUniqueId" id="vStoreUniqueId">

                    <input type="text" placeholder="Email Address" name="vEmail" id="vEmail">
                    <input type="password" placeholder="Password" name="vPasswordcorporate" id="vPasswordcorporate">
                    <input type="password" placeholder="Confirm Password" name="conf_password" id="conf_password">
                    <input type="text" placeholder="Contact Number" name="vContact" id="vContact">

                    <input type="text" placeholder="First Name" name="vFirstName" id="vFirstName">
                    <input type="text" placeholder="Last Name" name="vLastName" id="vLastName">


                    <p class="sc-form-dvdr"><span>Company Address</span></p>
                    <input type="text" placeholder="Street Address" name="vAddress" id="vAddress">
                    <input type="text" placeholder="Zip" name="vZip" id="vZip" onblur="get_loccor(this.value)">
                    <input type="text" placeholder="City" name="vCity" id="citycor">
                    <input type="text" placeholder="State" name="vState" id="statecor">
                    <input type="text" placeholder="Country" name="vCountry" id="countrycor" value="USA">


                    <p class="sc-form-dvdr"><span>Company Information</span></p>
                    <select name="storetype" id="storetype">
                        <option selected disabled>Type Of Account</option>
                        <?php
                        foreach ($STORE_TYPE as $STORE) {
                            echo '<option value="' . $STORE['iUserTypeStoreId'] . '">' . $STORE['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <select name="iIndustriesId" id="iIndustriesId">
                        <option selected disabled>Industry</option>
                        <?php
                        foreach ($INDUSTRIES as $IND) {
                            echo '<option value="' . $IND['iIndustriesId'] . '">' . $IND['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <!--  <p class="sc-form-dvdr"><span>Hours Of Operation</span></p>
                      <div class="bootstrap-timepicker">

                          <input type="text"  name="iHoursFrom"
                                 id="iHoursFrom"
                                 placeholder="From ">
                      </div>
                      <input type="text" class="" name="iHoursTo" id="iHoursTo" placeholder="To">

                      <p class="sc-form-dvdr"><span>Prefer Delivery time</span></p>
                      <div class="bootstrap-timepicker">

                          <input type="text"  name="iPreferDeliveryTimeFrom"
                                 id="iPreferDeliveryTimeFrom"
                                 placeholder="From">
                      </div>

                      <input type="text" class="" name="iPreferDeliveryTimeTo" id="iPreferDeliveryTimeTo" placeholder="To">
  -->
                    <p class="sc-form-dvdr"><span>Hours of Operation</span></p>
                    <select id="iHoursFromcor" name="iHoursFrom" onchange="get_diffcorwh()">

                        <option selected disabled> Select Hours of Operation Starts From</option>
                        <option value="01:00 AM"> 01:00 AM</option>
                        <option value="02:00 AM"> 02:00 AM</option>
                        <option value="03:00 AM"> 03:00 AM</option>
                        <option value="04:00 AM"> 04:00 AM</option>
                        <option value="05:00 AM"> 05:00 AM</option>
                        <option value="06:00 AM"> 06:00 AM</option>
                        <option value="07:00 AM"> 07:00 AM</option>
                        <option value="08:00 AM"> 08:00 AM</option>
                        <option value="09:00 AM"> 09:00 AM</option>
                        <option value="10:00 AM"> 10:00 AM</option>
                        <option value="11:00 AM"> 11:00 AM</option>
                        <option value="12:00 AM"> 12:00 AM</option>
                        <option value="01:00 PM"> 01:00 PM</option>
                        <option value="02:00 PM"> 02:00 PM</option>
                        <option value="03:00 PM"> 03:00 PM</option>
                        <option value="04:00 PM"> 04:00 PM</option>
                        <option value="05:00 PM"> 05:00 PM</option>
                        <option value="06:00 PM"> 06:00 PM</option>
                        <option value="07:00 PM"> 07:00 PM</option>
                        <option value="08:00 PM"> 08:00 PM</option>
                        <option value="09:00 PM"> 09:00 PM</option>
                        <option value="10:00 PM"> 10:00 PM</option>
                        <option value="11:00 PM"> 11:00 PM</option>
                        <option value="12:00 PM"> 12:00 PM</option>
                    </select>

                    <select id="iHoursTocor" name="iHoursTo" onchange="get_diffcorwh()">

                        <option selected disabled> Select Hours of Operation End At</option>
                        <option value="01:00 AM"> 01:00 AM</option>
                        <option value="02:00 AM"> 02:00 AM</option>
                        <option value="03:00 AM"> 03:00 AM</option>
                        <option value="04:00 AM"> 04:00 AM</option>
                        <option value="05:00 AM"> 05:00 AM</option>
                        <option value="06:00 AM"> 06:00 AM</option>
                        <option value="07:00 AM"> 07:00 AM</option>
                        <option value="08:00 AM"> 08:00 AM</option>
                        <option value="09:00 AM"> 09:00 AM</option>
                        <option value="10:00 AM"> 10:00 AM</option>
                        <option value="11:00 AM"> 11:00 AM</option>
                        <option value="12:00 AM"> 12:00 AM</option>
                        <option value="01:00 PM"> 01:00 PM</option>
                        <option value="02:00 PM"> 02:00 PM</option>
                        <option value="03:00 PM"> 03:00 PM</option>
                        <option value="04:00 PM"> 04:00 PM</option>
                        <option value="05:00 PM"> 05:00 PM</option>
                        <option value="06:00 PM"> 06:00 PM</option>
                        <option value="07:00 PM"> 07:00 PM</option>
                        <option value="08:00 PM"> 08:00 PM</option>
                        <option value="09:00 PM"> 09:00 PM</option>
                        <option value="10:00 PM"> 10:00 PM</option>
                        <option value="11:00 PM"> 11:00 PM</option>
                        <option value="12:00 PM"> 12:00 PM</option>
                    </select>
                    <span id="diffmsgwhcor" name="diffmsgwhcor" style="display: none"> Please select time which have minimum gape of 6 hours </span>

                    <p class="sc-form-dvdr"><span>Prefer Delivery time</span></p>

                    <select id="iPreferDeliveryTimeFromcor" name="iPreferDeliveryTimeFrom" onchange="get_diffcorpr()">

                        <option selected disabled> Select Your Prefered Delivery Hours Starts From</option>
                        <option value="01:00 AM"> 01:00 AM</option>
                        <option value="02:00 AM"> 02:00 AM</option>
                        <option value="03:00 AM"> 03:00 AM</option>
                        <option value="04:00 AM"> 04:00 AM</option>
                        <option value="05:00 AM"> 05:00 AM</option>
                        <option value="06:00 AM"> 06:00 AM</option>
                        <option value="07:00 AM"> 07:00 AM</option>
                        <option value="08:00 AM"> 08:00 AM</option>
                        <option value="09:00 AM"> 09:00 AM</option>
                        <option value="10:00 AM"> 10:00 AM</option>
                        <option value="11:00 AM"> 11:00 AM</option>
                        <option value="12:00 AM"> 12:00 AM</option>
                        <option value="01:00 PM"> 01:00 PM</option>
                        <option value="02:00 PM"> 02:00 PM</option>
                        <option value="03:00 PM"> 03:00 PM</option>
                        <option value="04:00 PM"> 04:00 PM</option>
                        <option value="05:00 PM"> 05:00 PM</option>
                        <option value="06:00 PM"> 06:00 PM</option>
                        <option value="07:00 PM"> 07:00 PM</option>
                        <option value="08:00 PM"> 08:00 PM</option>
                        <option value="09:00 PM"> 09:00 PM</option>
                        <option value="10:00 PM"> 10:00 PM</option>
                        <option value="11:00 PM"> 11:00 PM</option>
                        <option value="12:00 PM"> 12:00 PM</option>
                    </select>

                    <select id="iPreferDeliveryTimeTocor" name="iPreferDeliveryTimeTo" onchange="get_diffcorpr()">

                        <option selected disabled> Select Your Prefered Delivery Hours End At</option>
                        <option value="01:00 AM"> 01:00 AM</option>
                        <option value="02:00 AM"> 02:00 AM</option>
                        <option value="03:00 AM"> 03:00 AM</option>
                        <option value="04:00 AM"> 04:00 AM</option>
                        <option value="05:00 AM"> 05:00 AM</option>
                        <option value="06:00 AM"> 06:00 AM</option>
                        <option value="07:00 AM"> 07:00 AM</option>
                        <option value="08:00 AM"> 08:00 AM</option>
                        <option value="09:00 AM"> 09:00 AM</option>
                        <option value="10:00 AM"> 10:00 AM</option>
                        <option value="11:00 AM"> 11:00 AM</option>
                        <option value="12:00 AM"> 12:00 AM</option>
                        <option value="01:00 PM"> 01:00 PM</option>
                        <option value="02:00 PM"> 02:00 PM</option>
                        <option value="03:00 PM"> 03:00 PM</option>
                        <option value="04:00 PM"> 04:00 PM</option>
                        <option value="05:00 PM"> 05:00 PM</option>
                        <option value="06:00 PM"> 06:00 PM</option>
                        <option value="07:00 PM"> 07:00 PM</option>
                        <option value="08:00 PM"> 08:00 PM</option>
                        <option value="09:00 PM"> 09:00 PM</option>
                        <option value="10:00 PM"> 10:00 PM</option>
                        <option value="11:00 PM"> 11:00 PM</option>
                        <option value="12:00 PM"> 12:00 PM</option>
                    </select>

                    <span id="diffmsgprcor" name="diffmsgprcor" style="display: none"> Please select time which have minimum gape of 6 hours </span>
                    <p class="sc-form-dvdr"><span>Can you receive night delivery?</span></p>

                    <div class="sc-rdobtn">
                        <input id="eNightDelivery_Yes2" value="1" type="radio" name="eNightDelivery2">
                        <label for="eNightDelivery_Yes2">Yes</label>

                        <input id="eNightDelivery_No2" value="0" type="radio" name="eNightDelivery2" checked>
                        <label for="eNightDelivery_No2">No</label>
                    </div>

                    <!--   <p class="sc-form-dvdr"><span>Security Questions</span></p>

                       <div class="sc-scrtq">
                           <label><span>1</span>What is your mother's maiden name?</label> <input type="text"
                                                                                                  placeholder="Your answer here"
                                                                                                  name="sq1" id="sq1">
                           <hr>
                           <label><span>2</span>City where you born?</label>
                           <input type="text" placeholder="Your answer here" name="sq2" id="sq2">
                           <hr>
                           <label><span>3</span>First teachers name?</label>
                           <input type="text" placeholder="Your answer here" name="sq3" id="sq3">
                           <hr>
                       </div>-->

                    <p class="sc-form-dvdr"><span>How did you hear about uss?</span></p>
                    <select name="aboutus" id="aboutus" onchange="getreffrencecor(this.value)">
                        <option selected disabled>How did you hear about us?</option>
                        <?php
                        foreach ($HOW_HEAR as $HH) {
                            echo '<option value="' . $HH['iHDHId'] . '">' . $HH['vName'] . '</option>';
                        }
                        ?>
                    </select>

                    <div class="form-group" id="refemailcor" style="display: none">
                        <label class="col-md-2 control-label"><span class="red"> </span> </label>


                        <input type="text" name="refmailcor" id="refmailcor"
                               placeholder="Enter Email of Refferal Person">
                    </div>

                    <p class="acpttc-rgstr text-center">
                        <input type="checkbox" id="consumer-remember" name="corterm">
                        <label for="consumer-remember"></label>
                        By clicking on Register, you are accecpting <a href="#">Terms And Conditions</a>
                    </p>

                    <p class="acpttc-rgstr text-center">


                        <input type="submit" value="Submit" name="submit_corporate_store" id="submit_corporate_store">
                </form>


            </div>
        </div>
    </div>
</section>
<!-- id="corporate-form-main" ends -->


<div class="animated-bg-main">
    <div class="animated-grey-bg"></div>
    <div class="animated-white-bg"></div>
</div>

<script src="<?php echo $site_url; ?>assets/js/jquery.min.js"></script>
<script src="<?php echo $site_url; ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo $site_url; ?>assets/js/anijs.js"></script>
<script src="<?php echo $site_url; ?>assets/js/anijs-helper-scrollreveal.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>
<script>
    function getreffrence(val) {

        var ref = val;
        if (ref == 1) {
            //refemail.style.display = "block";
            $('#refemail').css("display", "block");

        }
        else {
            /*  refemail.style.display = "none";*/
            $('#refemail').css("display", "none");
        }
    }
</script>

<script>
    function getreffrencecor(val) {

        var ref = val;
        if (ref == 1) {
            //refemail.style.display = "block";
            $('#refemailcor').css("display", "block");

        }
        else {
            /*  refemail.style.display = "none";*/
            $('#refemailcor').css("display", "none");
        }
    }
</script>


<script>
    function getreffrencedist(val) {

        var ref = val;
        if (ref == 1) {
            //refemail.style.display = "block";
            $('#refemaildist').css("display", "block");

        }
        else {
            /*  refemail.style.display = "none";*/
            $('#refemaildist').css("display", "none");
        }
    }
</script>

<script>
    function getreffrencemulti(val) {

        var ref = val;
        if (ref == 1) {
            //refemail.style.display = "block";
            $('#refemailmulti').css("display", "block");

        }
        else {
            /*  refemail.style.display = "none";*/
            $('#refemailmulti').css("display", "none");
        }
    }
</script>

<script>
    function getreffrencemanu(val) {

        var ref = val;
        if (ref == 1) {
            //refemail.style.display = "block";
            $('#refemailmanu').css("display", "block");

        }
        else {
            /*  refemail.style.display = "none";*/
            $('#refemailmanu').css("display", "none");
        }
    }
</script>

<!--
<script>

    function checkmail(email) {
        alert(email);

        $.ajax({
            url: '<? /*=$site_url*/ ?>register_a.php',
            type: 'POST',
            data: {"signup": 'Get_Unique_Email', "email": email},
            success: function (result) {
                alert('success');

            }
        });
    }

</script>

<script>

    function checkaccount(account) {
        alert(account);

        $.ajax({
            url: '<? /*=$site_url */ ?>register_a.php',
            type: 'POST',
            data: {"signup": 'Get_Unique_Account', "accountname": account},
            success: function (result) {
                console.log('sdf : ', result);


            }
        });
    }

</script>-->

<script>


var form_signup_single = function () {
    var run_validate_single_store_form = function () {
        $("#single_store_retailer").validate({
            rules: {
                vStoreUniqueId: {
                    required: true
                },

                vEmail: {
                    required: true,
                    email: true
                },
                vPassword: {
                    required: true,
                    minlength: "6",
                    maxlength: "10"
                },
                conf_password: {
                    required: true,
                    equalTo: "#vPassword"
                },
                vFirstName: {
                    required: true
                },
                vLastName: {
                    required: true
                },

                vAddress: {
                    required: true
                },
                vZip: {
                    required: true,
                    number: true
                },
                vCity: {
                    required: true
                },
                vState: {
                    required: true
                },
                vCountry: {
                    required: true
                },
                iPreferDeliveryTimeFrom: {
                    required: true
                },
                iPreferDeliveryTimeTo: {
                    required: true
                },
                iIndustriesId: {
                    required: true
                },
                storetype: {
                    required: true
                },
                eNightDelivery: {
                    required: true
                },
                vContact: {
                    required: true,
                    number: true
                },
                snglterm: {
                    required: true
                }

            },
            messages: {
                vStoreUniqueId: "Please Enter Account Name",


                vEmail: {
                    required: "Please Enter Email",
                    email: "Please Enter valid Email"

                },
                vPassword: {
                    required: "Please Enter Password",
                    minlength: "Please Enter Minimum 6 Character",
                    maxlength: "Password can not be more than 10 character"

                },
                conf_password: {
                    required: "Please Enter Confirm Password",
                    equalTo: "Please Enter Confirm Password Same As Password"

                },
                vFirstName: "Please Enter First Name",
                vLastName: "Please Enter Last Name",
                vContact: {
                    required: "Please enter Contact Number",
                    number: "Enter Only Digits"
                },
                vAddress: "Please Enter Address",
                vZip: {
                    required: "Please Enter Zip",
                    number: "Please Enter Only digits"
                },
                vCity: "Please Enter City",
                vCountry: "Please Enter Country",
                vState: "Please Enter State",
                iPreferDeliveryTimeFrom: "Please Select Preferable Delivery Time Starts From",
                iPreferDeliveryTimeTo: "Please Select Preferable Delivery Time End At",
                iIndustriesId: "Please Select Industry",
                storetype: "Please Select Your Account Type",
                eNightDelivery: "Please Select Option for Night Delivery",
                snglterm: {
                    required: "Please Accept terms And Conditions"
                }
            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            run_validate_single_store_form();
        }
    };
}();


var form_signup_multiple = function () {
    var run_validate_multiple_store_form = function () {
        $("#multiple_store_retailer").validate({
            rules: {
                vStoreUniqueId: {
                    required: true
                },

                vStoreNumber: {
                    required: true
                },
                vEmail: {
                    required: true,
                    email: true
                },
                vPasswordmulti: {
                    required: true,
                    minlength: "6",
                    maxlength: "10"
                },
                conf_password: {
                    required: true,
                    equalTo: "#vPasswordmulti"
                },
                vFirstName: {
                    required: true
                },
                vLastName: {
                    required: true
                },

                vAddress: {
                    required: true
                },
                vZip: {
                    required: true,
                    number: true
                },
                vCity: {
                    required: true
                },
                vCountry: {
                    required: true
                },
                vState: {
                    required: true
                },
                iPreferDeliveryTimeFrom: {
                    required: true
                },
                iPreferDeliveryTimeTo: {
                    required: true
                },
                iHoursFrom: {
                    required: true
                },
                iHoursTo: {
                    required: true
                },
                iIndustriesId: {
                    required: true
                },
                storetype: {
                    required: true
                },
                eNightDelivery: {
                    required: true
                },
                vContact: {
                    required: true,
                    number: true
                },
                multiterm: {
                    required: true
                }

            },
            messages: {
                vStoreUniqueId: "Please Enter Account Name",

                vStoreNumber: "Please Enter Number of Account",
                vEmail: {
                    required: "Please Enter Email",
                    email: "Please Eneter valid Email"

                },
                vPasswordmulti: {
                    required: "Please Enter Password",
                    minlength: "Please Enter Minimum 6 Character",
                    maxlength: "Password can not be more than 10 character"

                },
                conf_password: {
                    required: "Please Enter Confirm Password",
                    equalTo: "Please Enter Confirm Password Same As Password"

                },
                vFirstName: "Please Enter First Name",
                vLastName: "Please Enter Last Name",
                vContact: {
                    required: "Please enter Contact Number",
                    number: "Enter Only Digits"
                },
                vAddress: "Please Enter Address",
                vZip: {
                    required: "Please Enter Zip",
                    number: "Please Enter Only digits"
                },
                vCity: "Please Enter City",
                vState: "Please Enter State",
                vCountry: "Please Enter Country",
                iPreferDeliveryTimeFrom: "Please Select Preferable Delivery Time Starts From",
                iPreferDeliveryTimeTo: "Please Select Preferable Delivery Time End At",
                iHoursFrom: "Please Select Your Working Hours Starts From",
                iHoursTo: "Please Select Your Working Hours End At",
                iIndustriesId: "Please Select Industry",
                storetype: "Please Select Your Account Type",
                eNightDelivery: "Plese Select Option for Night Delivery",
                multiterm: {
                    required: "Please Accept terms And Conditions"
                }

            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            run_validate_multiple_store_form();
        }
    };
}();


var form_signup_corporate = function () {
    var run_validate_corporate_store_form = function () {
        $("#corporate_store_retailer").validate({
            rules: {
                vStoreUniqueId: {
                    required: true
                },

                vContact: {
                    required: true,
                    number: true
                },
                vEmail: {
                    required: true,
                    email: true
                },
                parentid: {
                    required: true
                },
                iHoursFrom: {
                    required: true
                },
                iHoursTo: {
                    required: true
                },
                vPasswordcorporate: {
                    required: true,
                    minlength: "6",
                    maxlength: "10"
                },
                conf_password: {
                    required: true,
                    equalTo: "#vPasswordcorporate"
                },
                vFirstName: {
                    required: true
                },
                vLastName: {
                    required: true
                },
                iCategoryId: {
                    required: true
                },
                vAddress: {
                    required: true
                },
                vZip: {
                    required: true,
                    number: true
                },
                vCity: {
                    required: true
                },
                vState: {
                    required: true
                },
                vCountry: {
                    required: true
                },
                iPreferDeliveryTimeFrom: {
                    required: true
                },
                iPreferDeliveryTimeTo: {
                    required: true
                },
                iIndustriesId: {
                    required: true
                },
                storetype: {
                    required: true
                },
                eNightDelivery: {
                    required: true
                },
                corterm: {
                    required: true
                }

            },
            messages: {
                vStoreUniqueId: "Please Enter Account Name",
                parentid: "Please Select Corporate Account",

                vContact: {
                    required: "Please enter Contact Number",
                    number: "Enter Only Digits"
                },
                vEmail: {
                    required: "Please Enter Email",
                    email: "Please Eneter valid Email"

                },
                vPasswordcorporate: {
                    required: "Please Enter Password",
                    minlength: "Please Enter Minimum 6 Character",
                    maxlength: "Password can not be more than 10 character"
                },
                conf_password: {
                    required: "Please Enter Confirm Password",
                    equalTo: "Please Enter Confirm Password Same As Password"

                },
                vFirstName: "Please Enter First Name",
                vLastName: "Please Enter Last Name",
                iCategoryId: "Please Enter Category",
                vAddress: "Please Enter Address",
                vZip: {
                    required: "Please Enter Zip",
                    number: "Please Enter Only digits"
                },
                vCity: "Please Enter City",
                vState: "Please Enter State",
                vCountry: "Please Enter Country",
                iPreferDeliveryTimeFrom: "Please Select Preferable Delivery Time Starts From",
                iPreferDeliveryTimeTo: "Please Select Preferable Delivery Time End At",
                iIndustriesId: "Please Select Industry",
                storetype: "Please Select Your Account Type",
                eNightDelivery: "Please Select Option for Night Delivery",
                iHoursFrom: "Please Select Your Working Hours Starts From",
                iHoursTo: "Please Select Your Working Hours End At",
                corterm: {
                    required: "Please Accept terms And Conditions"
                }

            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            run_validate_corporate_store_form();
        }
    };
}();

var form_signup_manufacturer = function () {
    var run_validate_manufacturer_form = function () {
        $("#manufacturer_form").validate({
            rules: {
                vStoreUniqueId: {
                    required: true
                },

                vContact: {
                    required: true,
                    number: true
                },
                vEmail: {
                    required: true,
                    email: true
                },
                vPasswordmanu: {
                    required: true,
                    minlength: "6",
                    maxlength: "10"

                },
                conf_password: {
                    required: true,
                    equalTo: "#vPasswordmanu"
                },
                vFirstName: {
                    required: true
                },
                vLastName: {
                    required: true
                },
                iCategoryId: {
                    required: true
                },
                vAddress: {
                    required: true
                },
                vZip: {
                    required: true,
                    number: true
                },
                vCity: {
                    required: true
                },
                vState: {
                    required: true
                },
                vCountry: {
                    required: true
                },
                roleid: {
                    required: true
                },
                vStreet: {
                    required: true
                },
                iIndustriesId: {
                    required: true
                },
                manterm: {
                    required: true
                }


            },
            messages: {
                vStoreUniqueId: "Please Enter Account Name",
                vContact: {
                    required: "Please enter Contact Number",
                    number: "Enter Only Digits"
                },
                vEmail: {
                    required: "Please Enter Email",
                    email: "Please Eneter valid Email"

                },
                vPasswordmanu: {
                    required: "Please Enter Password",
                    minlength: "Please Enter Minimum 6 Character",
                    maxlength: "Password can not be more than 10 character"

                },
                conf_password: {
                    required: "Please Enter Confirm Password",
                    equalTo: "Please Enter Confirm Password Same As Password"

                },
                vFirstName: "Please Enter First Name",
                vLastName: "Please Enter Last Name",
                iCategoryId: "Please Enter Category",
                vAddress: "Please Enter Address",
                vZip: {
                    required: "Please Enter Zip",
                    number: "Please Enter Only digits"
                },
                vCity: "Please Enter City",
                vState: "Please Enter State",
                vCountry: "Please Enter Country",
                iIndustriesId: "Please Select Industry",
                roleid: "Please Select Role",
                vStreet: "Please Enter Street",
                manterm: {
                    required: "Please Accept terms And Conditions"
                }
            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            run_validate_manufacturer_form();
        }
    };
}();


var form_signup_distributor = function () {
    var run_validate_distributor_form = function () {
        $("#distributor_form").validate({
            rules: {
                vStoreUniqueId: {
                    required: true
                },
                vContact: {
                    required: true,
                    number: true
                },
                vEmail: {
                    required: true,
                    email: true
                },
                vPassworddistri: {
                    required: true,
                    minlength: "6",
                    maxlength: "10"
                },
                conf_password: {
                    required: true,
                    equalTo: "#vPassworddistri"
                },
                vFirstName: {
                    required: true
                },
                vLastName: {
                    required: true
                },
                iCategoryId: {
                    required: true
                },
                roleid: {
                    required: true
                },
                vAddress: {
                    required: true
                },
                vZip: {
                    required: true,
                    number: true
                },
                vCity: {
                    required: true
                },
                vState: {
                    required: true
                },
                vCountry: {
                    required: true
                },
                iIndustriesId: {
                    required: true
                },
                vStreet: {
                    required: true
                },
                disterm: {
                    required: true
                }

            },
            messages: {
                vStoreUniqueId: "Please Enter Account Name",
                vContact: {
                    required: "Please enter Contact Number",
                    number: "Enter Only Digits"
                },
                vEmail: {
                    required: "Please Enter Email",
                    email: "Please Eneter valid Email"

                },
                vPassworddistri: {
                    required: "Please Enter Password",
                    minlength: "Please Enter Minimum 6 Character",
                    maxlength: "Password can not be more than 10 character"
                },
                conf_password: {
                    required: "Please Enter Confirm Password",
                    equalTo: "Please Enter Confirm Password Same As Password"

                },
                vFirstName: "Please Enter First Name",
                vLastName: "Please Enter Last Name",
                iCategoryId: "Please Enter Category",
                vAddress: "Please Enter Address",
                vZip: {
                    required: "Please Enter Zip",
                    number: "Please Enter Only digits"
                },
                vCity: "Please Enter City",
                vCountry: "Please enter Country",
                vState: "Please Enter State",
                iIndustriesId: "Please Select Industry",
                roleid: "Please Role",
                vStreet: "Please Enter Street",
                disterm: {
                    required: "Please Accept terms And Conditions"
                }
            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
            run_validate_distributor_form();
        }
    };
}();


</script>


<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<link rel="stylesheet" href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/timepicker.css">
<link rel="stylesheet" href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/datetimepicker.css">
<link href="<?php echo $admin_url; ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">


<script>
    $('.timepicker-default').timepicker({});
</script>

<script>

    $(document).ready(function () {
        form_signup_single.init();
        $('#submit_single_store').click(function () {
            $('#single_store_retailer').submit();
        });

        form_signup_multiple.init();
        $('#submit_multiple_store').click(function () {
            $('#multiple_store_retailer').submit();
        });

        form_signup_corporate.init();
        $('#submit_corporate_store').click(function () {
            $('#corporate_store_retailer').submit();
        });

        form_signup_manufacturer.init();
        $('#submit_manufacturer').click(function () {
            $('#manufacturer_form').submit();
        });

        form_signup_distributor.init();
        $('#submit_distributor_store').click(function () {
            $('#distributor_form').submit();
        });


        $(".rgstr-opt-tblcl").hover(
            function () {
                $(this).children(".rgstr-optn-hidn-contnt").slideDown(300);
            },
            function () {
                $(this).children(".rgstr-optn-hidn-contnt").slideUp(300);
            });


        $("a").click(function () {
            var vsblsctn = $(this).attr('href');

            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $(vsblsctn).removeClass("rgstr-novsbl");
            $(vsblsctn).addClass("rgstr-vsbl");
        });

        /*  $(".bootstrap-timepicker").click(function () {
         var vsblsctn = $(this).attr('href');

         $(vsblsctn).removeClass("rgstr-novsbl");
         $(vsblsctn).addClass("rgstr-vsbl");
         });*/


        $(".show-meridian").click(function () {
            var vsblsctn = $(this).attr('href');

            $("section.rgstr-optn").removeClass("rgstr-novsbl");
            $("section.rgstr-optn").addClass("rgstr-vsbl");
            $(vsblsctn).removeClass("rgstr-novsbl");
            $(vsblsctn).addClass("rgstr-vsbl");


        });


    });


    $(document).ready(function () {

        /*right top note starts*/
        $(".rgt-tp-note-main span").click(function () {
            $(".rgt-tp-note-main p").slideToggle();
        });
        /*right top note starts*/ //rgt-tp-note-main-ovrly
        $(".nmbr-of-stors span").click(function () {
            $(".rgt-tp-note-main").slideToggle();
            $(".rgt-tp-note-main-ovrly").toggle();
        });
        $(".rgt-tp-note-main-ovrly").click(function () {
            $(".rgt-tp-note-main").slideToggle();
            $(".rgt-tp-note-main-ovrly").toggle();
        });
        /*right top note ends*/


        /*right top note ends*/


        $("a").click(function () {

            var clcklft = $(this).offset().left + ($(this).width() / 2);
            var clckrgt = $(this).offset().top + ($(this).height() / 2);

            /*animation of bg starts*/
            $(".animated-grey-bg").addClass("actv-bg").css({"left": clcklft, "top": clckrgt})
                .delay(500).queue(function (next) {
                    $(".animated-white-bg").addClass("actv-bg").css({"left": clcklft, "top": clckrgt});
                    next();
                })
            /*animation of bg ends*/

            /* for display none/block section starts */

            var vsblsctn = $(this).attr('href');
            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $(vsblsctn).removeClass("rgstr-novsbl");
            $(vsblsctn).addClass("rgstr-vsbl");
            /* for display none/block section ends */
        });


        /* back button starts */
        $(".rgstr-bck-btn-stp0").click(function () {

            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $("#rgstr-main-option").removeClass("rgstr-novsbl");
            $("#rgstr-main-option").addClass("rgstr-vsbl");

            $(".animated-white-bg").removeClass("actv-bg").delay(500).queue(function (next) {
                $(".animated-grey-bg").removeClass("actv-bg");
                next();
            });

        });

        $(".rgstr-bck-btn-stp1").click(function () {

            $("section.rgstr-optn").removeClass("rgstr-vsbl");
            $("section.rgstr-optn").addClass("rgstr-novsbl");
            $("#rgstr-rtlr-option").removeClass("rgstr-novsbl");
            $("#rgstr-rtlr-option").addClass("rgstr-vsbl");


            $(".animated-white-bg").removeClass("actv-bg").delay(500).queue(function (next) {
                $(".animated-grey-bg").removeClass("actv-bg");
                next();
            });
        });
        /* back button ends */


    });
</script>
<script>
    function get_locmulti(loc) {
        if (loc != "") {

        $.ajax({
            url: 'register_a.php',
            type: 'POST',
            data: {"mode": 'get_zipcode', "zipcode": loc},
            success: function (result) {
                console.log(result);
                var data = JSON.parse(result);
                var location = data['results'][0]['formatted_address'].split(',');

                var cnt = location.length;
                console.log(cnt);
                if (cnt == "3") {
                    $('#countrymulti').val(location[2]);
                    var statetrim = location[1].trim();
                    var state = statetrim.split(" ");
                    $('#statemulti').val(state[0]);
                    $('#citymulti').val(location[0]);
                }
                else {
                    $('#countrymulti').val("");
                    $('#statemulti').val("");
                    $('#citymulti').val("");
                }
                //$('#iCountryId').val(data['results'][0]['address_components'][4]['long_name']);

                }
        });
    }
        else {
            $('#countrymulti').val("");
            $('#statemulti').val("");
            $('#citymulti').val("");
        }
    }

</script>


<script>
    function get_locdis(loc) {
        if (loc != "") {
        $.ajax({
            url: 'register_a.php',
            type: 'POST',
            data: {"mode": 'get_zipcode', "zipcode": loc},
            success: function (result) {
                console.log(result);
                var data = JSON.parse(result);
                var location = data['results'][0]['formatted_address'].split(',');

                var cnt = location.length;
                console.log(cnt);
                if (cnt == "3") {
                    $('#countrydis').val(location[2]);
                    var statetrim = location[1].trim();
                    var state = statetrim.split(" ");
                    $('#statedis').val(state[0]);
                    $('#citydis').val(location[0]);
                }
                else {
                    $('#countrydis').val("");
                    $('#statedis').val("");
                    $('#citydis').val("");
                }
                //$('#iCountryId').val(data['results'][0]['address_components'][4]['long_name']);

            }
        });
    }
        else {
            $('#countrydis').val("");
            $('#statedis').val("");
            $('#citydis').val("");
        }
    }

</script>
<script>
    function get_locman(loc) {
        if (loc != "") {
        $.ajax({
            url: 'register_a.php',
            type: 'POST',
            data: {"mode": 'get_zipcode', "zipcode": loc},
            success: function (result) {
                console.log(result);
                var data = JSON.parse(result);
                var location = data['results'][0]['formatted_address'].split(',');

                var cnt = location.length;
                console.log(cnt);
                if (cnt == "3") {
                    $('#countryman').val(location[2]);
                    var statetrim = location[1].trim();
                    var state = statetrim.split(" ");
                    $('#stateman').val(state[0]);
                    $('#cityman').val(location[0]);
                }
                else {
                    $('#countryman').val("");
                    $('#stateman').val("");
                    $('#cityman').val("");
                }
                //$('#iCountryId').val(data['results'][0]['address_components'][4]['long_name']);

            }
        });
    }
        else {
            $('#countryman').val("");
            $('#stateman').val("");
            $('#cityman').val("");
        }
    }

</script>
<script>
    function get_locsingle(loc) {
        if (loc != "") {
            $.ajax({
                url: 'register_a.php',
                type: 'POST',
                data: {"mode": 'get_zipcode', "zipcode": loc},
                success: function (result) {
                    console.log(result);
                    var data = JSON.parse(result);
                    var location = data['results'][0]['formatted_address'].split(',');

                    var cnt = location.length;
                    console.log(cnt);
                    if (cnt == "3") {
                        $('#countrysingle').val(location[2]);
                        var statetrim = location[1].trim();
                        var state = statetrim.split(" ");
                        $('#statesingle').val(state[0]);
                        $('#citysingle').val(location[0]);
                    }
                    else {
                        $('#countrysingle').val("");
                        $('#statesingle').val("");
                        $('#citysingle').val("");
                    }
                    //$('#iCountryId').val(data['results'][0]['address_components'][4]['long_name']);

                }
            });
        }
        else {
            $('#countrysingle').val("");
            $('#statesingle').val("");
            $('#citysingle').val("");
        }
    }


</script>
<script>
    function get_loccor(loc) {
        if (loc != "") {
        $.ajax({
            url: 'register_a.php',
            type: 'POST',
            data: {"mode": 'get_zipcode', "zipcode": loc},
            success: function (result) {
                console.log(result);
                var data = JSON.parse(result);
                var location = data['results'][0]['formatted_address'].split(',');

                var cnt = location.length;
                console.log(cnt);
                if (cnt == "3") {
                    $('#countrycor').val(location[2]);
                    var statetrim = location[1].trim();
                    var state = statetrim.split(" ");
                    $('#statecor').val(state[0]);
                    $('#citycor').val(location[0]);
                }
                else {
                    $('#countrycor').val("");
                    $('#statecor').val("");
                    $('#citycor').val("");
                }
                //$('#iCountryId').val(data['results'][0]['address_components'][4]['long_name']);

            }
        });
    }
        else {
            $('#countrycor').val("");
            $('#statecor').val("");
            $('#citycor').val("");
        }
    }



</script>
<script>
    function get_diff() {
        var time1 = $('#iPreferDeliveryTimeFromsngle').val().split(':');
        var time2 = $('#iPreferDeliveryTimeTosngle').val().split(':');

        var t1 = time1[1].split(" ");
        var t2 = time2[1].split(" ");

        if (t1[1] == "PM") {
            var tt = (parseInt(time1[0]) + 12);
        }
        else {
            var tt = time1[0];
        }
        if (t2[1] == "PM") {
            var tt2 = (parseInt(time2[0]) + 12);
        }
        else {
            var tt2 = time2[0];
        }

        var hours1 = parseInt(tt, 10),
            hours2 = parseInt(tt2, 10),
            mins1 = parseInt(time1[1], 10),
            mins2 = parseInt(time2[1], 10);
        var hours = hours2 - hours1, mins = 0;
        if (hours < 0) hours = 24 + hours;
        if (mins2 >= mins1) {
            mins = mins2 - mins1;
        }
        else {
            mins = (mins2 + 60) - mins1;
            hours--;
        }

        mins = mins / 60; // take percentage in 60
        hours += mins;
        hours = hours.toFixed(2);
        console.log(hours);
        if (hours >= 6) {
            diffmsgsngle.style.display = "none";
        }
        else {
            diffmsgsngle.style.display = "";
            diffmsgsngle.style.fontWeight = "bold";
            diffmsgsngle.style.fontsize = "15px";
        }

    }
</script>
<script>
    function get_diffmultiwh() {
        var time1 = $('#iHoursFrommulti').val().split(':');
        var time2 = $('#iHoursTomulti').val().split(':');

        var t1 = time1[1].split(" ");
        var t2 = time2[1].split(" ");

        if (t1[1] == "PM") {
            var tt = (parseInt(time1[0]) + 12);
        }
        else {
            var tt = time1[0];
        }
        if (t2[1] == "PM") {
            var tt2 = (parseInt(time2[0]) + 12);
        }
        else {
            var tt2 = time2[0];
        }

        var hours1 = parseInt(tt, 10),
            hours2 = parseInt(tt2, 10),
            mins1 = parseInt(time1[1], 10),
            mins2 = parseInt(time2[1], 10);
        var hours = hours2 - hours1, mins = 0;
        if (hours < 0) hours = 24 + hours;
        if (mins2 >= mins1) {
            mins = mins2 - mins1;
        }
        else {
            mins = (mins2 + 60) - mins1;
            hours--;
        }

        mins = mins / 60; // take percentage in 60
        hours += mins;
        hours = hours.toFixed(2);
        console.log(hours);
        if (hours >= 6) {
            diffmsgwhmulti.style.display = "none";

        }
        else {
            diffmsgwhmulti.style.display = "";
            diffmsgwhmulti.style.fontWeight = "bold";
            diffmsgwhmulti.style.fontsize = "15px";
        }

    }
</script>


<script>
    function get_diffmultipr() {
        var time1 = $('#iPreferDeliveryTimeFrommulti').val().split(':');
        var time2 = $('#iPreferDeliveryTimeTomulti').val().split(':');

        var t1 = time1[1].split(" ");
        var t2 = time2[1].split(" ");

        if (t1[1] == "PM") {
            var tt = (parseInt(time1[0]) + 12);
        }
        else {
            var tt = time1[0];
        }
        if (t2[1] == "PM") {
            var tt2 = (parseInt(time2[0]) + 12);
        }
        else {
            var tt2 = time2[0];
        }

        var hours1 = parseInt(tt, 10),
            hours2 = parseInt(tt2, 10),
            mins1 = parseInt(time1[1], 10),
            mins2 = parseInt(time2[1], 10);
        var hours = hours2 - hours1, mins = 0;
        if (hours < 0) hours = 24 + hours;
        if (mins2 >= mins1) {
            mins = mins2 - mins1;
        }
        else {
            mins = (mins2 + 60) - mins1;
            hours--;
        }

        mins = mins / 60; // take percentage in 60
        hours += mins;
        hours = hours.toFixed(2);
        console.log(hours);
        if (hours >= 6) {
            diffmsgprmulti.style.display = "none";
        }
        else {
            diffmsgprmulti.style.display = "";
            diffmsgprmulti.style.fontWeight = "bold";
            diffmsgprmulti.style.fontsize = "15px";
        }

    }
</script>

<script>
    function get_diffcorwh() {
        var time1 = $('#iHoursFromcor').val().split(':');
        var time2 = $('#iHoursTocor').val().split(':');

        var t1 = time1[1].split(" ");
        var t2 = time2[1].split(" ");

        if (t1[1] == "PM") {
            var tt = (parseInt(time1[0]) + 12);
        }
        else {
            var tt = time1[0];
        }
        if (t2[1] == "PM") {
            var tt2 = (parseInt(time2[0]) + 12);
        }
        else {
            var tt2 = time2[0];
        }

        var hours1 = parseInt(tt, 10),
            hours2 = parseInt(tt2, 10),
            mins1 = parseInt(time1[1], 10),
            mins2 = parseInt(time2[1], 10);
        var hours = hours2 - hours1, mins = 0;
        if (hours < 0) hours = 24 + hours;
        if (mins2 >= mins1) {
            mins = mins2 - mins1;
        }
        else {
            mins = (mins2 + 60) - mins1;
            hours--;
        }

        mins = mins / 60; // take percentage in 60
        hours += mins;
        hours = hours.toFixed(2);
        console.log(hours);
        if (hours >= 6) {
            diffmsgwhcor.style.display = "none";
        }
        else {
            diffmsgwhcor.style.display = "";
            diffmsgwhcor.style.fontWeight = "bold";
            diffmsgwhcor.style.fontsize = "15px";
        }

    }
</script>


<script>
    function get_diffcorpr() {
        var time1 = $('#iPreferDeliveryTimeFromcor').val().split(':');
        var time2 = $('#iPreferDeliveryTimeTocor').val().split(':');

        var t1 = time1[1].split(" ");
        var t2 = time2[1].split(" ");

        if (t1[1] == "PM") {
            var tt = (parseInt(time1[0]) + 12);
        }
        else {
            var tt = time1[0];
        }
        if (t2[1] == "PM") {
            var tt2 = (parseInt(time2[0]) + 12);
        }
        else {
            var tt2 = time2[0];
        }

        var hours1 = parseInt(tt, 10),
            hours2 = parseInt(tt2, 10),
            mins1 = parseInt(time1[1], 10),
            mins2 = parseInt(time2[1], 10);
        var hours = hours2 - hours1, mins = 0;
        if (hours < 0) hours = 24 + hours;
        if (mins2 >= mins1) {
            mins = mins2 - mins1;
        }
        else {
            mins = (mins2 + 60) - mins1;
            hours--;
        }

        mins = mins / 60; // take percentage in 60
        hours += mins;
        hours = hours.toFixed(2);
        console.log(hours);
        if (hours >= 6) {
            diffmsgprcor.style.display = "none";
        }
        else {
            diffmsgprcor.style.display = "";
            diffmsgprcor.style.fontWeight = "bold";
            diffmsgprcor.style.fontsize = "15px";
        }

    }
</script>
<!--css & js for timepicker-->

<? //include_once($admin_path . 'js_front_form.php');  ?>


</body>
</html>