

<footer class="footer-links">
    <div class="container">
        <div class="row">
            <div class="col-md-9 flinks-main">
                <ul>
                    <!--<li><a href="about_us.php">About Us</a></li>-->
                    <li><a href="index.php#contact">Contact Us</a></li>
                    <!--<li><a href="careers.php">Careers</a></li>-->
                    <li><a href="terms_condition.php">Terms of Use</a></li>
                    <li><a href="privacy_policy.php">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="col-md-3 social-mda text-right">
        
                <ul>
                  <!-- <li><a href="#" class="fb-icon" title="Facebook"></a></li>
                    <li><a href="#" class="tw-icon" title="Twitter"></a></li>-->
                    <!-- <li><a href="#" class="gp-icon" title="Google Plus"></a></li>-->
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 copyright-txt">
                <p>All rights reserved © 2015 <span class="red">Service Calibrate, Inc.</span></p>
            </div>
            <div class="col-md-4 copyright-txt">
                <p class="text-right">Service Calibrate, Inc.</p>
            </div>
        </div>
    </div>
</footer>

<script src="<?php echo $site_url; ?>assets/js/jquery.min.js"></script>
<script src="<?php echo $site_url; ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo $site_url; ?>assets/js/anijs.js"></script>
<script src="<?php echo $site_url; ?>assets/js/anijs-helper-scrollreveal.js"></script>
<script src="<?php echo $site_url; ?>assets/js/morphext.js"></script>
<script src="analyticstracking.js"></script>
<script>
    $("#js-rotating").Morphext({
        animation: 'bounce'
    });
</script>
<script>
    $('.carousel').carousel({
        interval: 8000 //changes the speed
    })
</script>
</body>
</html>
