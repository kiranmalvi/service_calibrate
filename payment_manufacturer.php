<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 8/4/15
 * Time: 11:41 AM
 */


include_once('include.php');
include_once('header.php');


include_once($inc_class_path . 'user.class.php');
$userObj = new user();
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];

$user=$userObj->userInfo($iUserId);

$count=$user['vComment'];

if($count>0 && $count!="")
{

    $count=$user['vComment'];
}
else
{

    $count="";
}

include_once($inc_class_path . 'plan.class.php');
$planObj = new plan();
$planObj->select("3");
$mprice=$planObj->getFMonthPrice();
$dprice=$planObj->getfDayPrice();


$current= date('d');
$lastdate= date('t');

if($current==$lastdate)
{
    $days=30;
}
else
{
    $days=$lastdate-$current;
}


?>
<?php if(isset($_SESSION['frnt_payment_msg']) && $_SESSION['frnt_payment_msg']!=""){?>
    <div class="pull-center alert-danger" align="center" style="font-size: 20px">
        <?php echo $_SESSION['frnt_payment_msg']; ?>
    </div>
<?php }?>
<style>

    a.tooltips {
        position: relative;
        display: inline;
    }
    a.tooltips span {
        position: absolute;
        width:250px;
        height:auto;
        color: #fff;
        background: #ff0000;
        max-height: 110px;
        font-weight: bolder;
        line-height: 18px;

        text-align: left;
        visibility: hidden;
        border-radius: 4px;
        font-size:.9em;
        padding:3px;
    }
    a.tooltips span:after {
        content: '';
        position: absolute;
        top: 50%;
        right: 100%;
        margin-top: -8px;
        width: 0; height: 0;
        border-right: 8px solid red;
        border-top: 8px solid transparent;
        border-bottom: 8px solid transparent;
    }
    a:hover.tooltips span {
        visibility: visible;
        left: 100%;
        top: 50%;
        margin-top: -30px;
        margin-left: 15px;
        z-index: 999;
    }

</style>
<section class="pay_had_main">
    <div class="container">
        <div class="row">
            <div class="pay_had_main">
                <div class="col-md-4">
                    <img src="assets/images/payment_logo.png" alt="">
                </div>
                <div class="col-md-8">
                    <div class="pay_had_right">
                        All Features, One Price, Real Time Data
                    </div>
                    <div class="pay_had_sub_title">30 Day Free Trial</div>
                </div>
            </div>
        </div>
    </div>
</section>

<section style="background-image:url('assets/images/sc-bnr3.jpg'); background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="text-center"> <h2> Manufacturer Pricing </h2> </div>
            <div class="col-md-12">
                <div class="price_title">
                    <sup>$</sup><span> <?php echo $dprice?> </span>
                </div>
                <div class="per-acc">
                    User / Per Month
                    <p>If Paid  Annually</p>
                </div>
                <div class="col-md-3 text-center">
                    <div class="per-or">	OR	</div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="price_title_nex">
                    <sup>$</sup>	<span> <?php echo $mprice?></span>
                </div>
                <div class="per-acc_nex">
                    Per  User /Month
                    <p>If Paid Monthly</p>
                </div>
                <div class="per-link-content">For Large Enterprise: <br>
                    Please contact us on <b> <a href="mailto:sales@servicecalibrate.com"> sales@servicecalibrate.com </a> </b> for pricing</div>
            </div>
        </div>
    </div>
</section>


<section class="pay_sin-acc">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                Single User/Employee use it for <span> Free! </span> We support & promote small businesses
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>All Features</h2>
            </div>
        </div>
        <div class="row">
            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/service.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Service Frequency
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Measure the "Say vs. Do" ratio and validate your services. Show your Retailers/Customers you are the best Vendor. Know exactly how frequently your sales person/merchandisers/drivers or management visit your Retailers/Customers. Simple and powerful filters allows you to manage unlimited Retailers.</p>
                </div>
            </div>
            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/rating.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Rating (View Rating)
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Manufacturers can see which Retailer/Manager is not satisfied with your service. Service Calibrate gives you direct visibility to your customers feedback directly so  you can reward or coach your employees and improve service before you lose that customer/account forever.</p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/promation.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>View/Add Promotion
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Your goal is to sell more products and Retailers wants to purchase more products. The challenge is majority of your deals/promotions are inadequately communicated to Retailers & sometimes not communicated at all. Now you can add any promotion on our platform, pick a specific retailer/city/state and then let your target customers view that promotions instantly.</p>
                </div>
            </div>


            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/salesl.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Service As Schedule
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Easy monthly calendar view shows when you were scheduled to recieve service vs. actual service day. View missed service calls by week by Retailer.</p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/service.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Rapid Fire Visit
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                      <p>Have you noticed your employee simply "stopping by" a Retailer and leaving right away without adequately servicing? Service Calibrate identifies which employee and location visits were under duration vs. required minimum - as per your agreement with Retailers.</p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/project.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Projected Deliveries
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Management & employees can see all scheduled future deliveries in advance by  day/week/month & amount in advance. You will be able to manage this for all your sites and employees right on your smart phone & tablet.</p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/promation.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Messages
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Need to talk to your customer immediately? Want to message all, some, or just one of your Retailer without storing their contact information on your personal phone? Service Calibrate keeps all this capability in just one place. Now you can send & receive messages to/from your Retailer with the touch of a button.</p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/contact_pay.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Contacts
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Some sales calls waste time beacause many employees don't  know the decision maker at a given location. Service Calibrate saves time by identifying the decision maker for you. Tired of keeping track of so many Retailers & their contact information?  Now you can have all their contacts at your finger tips. </p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/service.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>View Broadcast
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                     <p>Save fuel & labor by knowing in advance if the account is going to be permanently or temporarily closed (and for how long). If there is a change of ownership you can immediately protect your assets by having new agreements executed.</p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/sig.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Signature Confirmation
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                      <p>You now have the option to require signatures from Retailers as a  proof of service/delivery. Every time your employee visits a your Customers/Retailers, this feature will record and validate your time, date, duration of visit.</p>

                </div>
            </div>
        </div>
    </div>
</section>



<section class="how-to-gray">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2>How do you want to Pay?</h2>
                 <div class="col-md-12 con-tab-details" style="font-size: 19px;padding: 0px 0 40px 0px;
  text-align: center;">
                    <b> Single User is FREE!</b><br> Please enter 1 in the # of employees for free registration & select.
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center how-to-bor">
                <div class="per-acc"> Monthly </div>
                <!--<div class="price_title_nex">
                    $	<span> 35.00 </span>
                </div>-->
                <div class="price_title" style="font-size:80px;">
                    <sup>$</sup><span>  <?php echo $mprice?> </span>
                </div>
                <div class="how-mont-pal"> Per User/Location/Month </div>
                <h2 class="how-plan"> </h2>
                <div class="col-md-8 col-md-offset-2">
                    <form name="frmmnth" id="frmmnth" method="post" action="transaction.php">
                        <input type="hidden" name="formname" value="frmmnth" id="frmmnth">
                        <input type="text" value="<?php echo $mprice;?>" id="mplanprice" name="mplanprice" style="display: none">
                        <input type="hidden" name="days" value="<?php echo $days?>" id="days">
                        <div class="location-price">
                            <label> Please Enter # of Employees </label>
                            <input type="text" name="text" id="mnprice" placeholder="" value="<?php echo $count?>" onkeyup="checkpromomnth()">
                        </div>
                        <div class="location-price">
                            <label> Promotional Code </label>
                            <input type="text" name="mpromocode" placeholder="Promo Code" id="mpromocode" value="" onkeyup="checkpromomnth()">
                        </div>
                        <div class="location-year">
                            <input type="text" name="total" id="mtotal" value="<?php echo round($count * $days * ($mprice/30),2)?>" readonly >
                            <span id="mspan"></span>
                            <h6 class="yer-mont">Recurring Monthly</h6>
                        </div>
                        <div class="location-price">
                            <input type="button" name="mnthbutton" id="mnthbutton" class="prices-selected" value="Select" onclick="selectmonth()">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4 text-center how-to-bor">
                <div class="per-acc"> Annually </div>
                <!--<div class="price_title_nex">
                    $	<span> 29.99 </span>
                </div>-->
                <div class="price_title" style="font-size:80px;">
                    <sup>$</sup><span> <?php echo $dprice?></span>
                </div>
                <div class="how-mont-pal"> Per User/Location/Month </div>
                <h2 class="how-plan"> </h2>
                <div class="col-md-8 col-md-offset-2">
                    <form name="frmanual" id="frmanual" method="post" action="transaction.php">
                        <input type="hidden" name="formname" value="frmmnth" id="formname">
                        <input type="text" value="<?php echo $dprice;?>" id="aplanprice" name="aplanprice" style="display: none">
                        <div class="location-price">
                            <label> Please Enter # of Employees </label>
                            <input type="text" name="text" id="aprice" placeholder="" value="<?php echo $count;?>" onkeyup="checkpromo()">
                        </div>
                        <div class="location-price">
                            <label> Promotional Code </label>
                            <input type="text" name="promocode" id="promocode" placeholder="Promo Code" value="" onkeyup="checkpromo()">
                        </div>
                        <div class="location-year">
                            <input type="text" name="total" id="atotal" value="<?php echo round($count * $dprice *12,2)?>" readonly>
                            <span id="aspan"></span>
                            <h6 class="yer-mont">Yearly</h6>
                        </div>
                        <div class="location-price">
                            <input type="button" name="annualbutton" id="annualbutton" value="Select" onclick="selectannual()">
                        </div>
                    </form>
                </div>

            </div>

            <?if(isset($_REQUEST['page'])&&$_REQUEST['page']=="regi"){?>
                <div class="col-md-4 text-center">
                    <div class="per-acc"> Free </div>
                    <div class="price_title" style="font-size:80px;">
                        <sup>$</sup><span> 0.00 </span>
                    </div>
                    <div class="how-mont-pal"> 30 Day Trial </div>
                    <h2 class="how-plan"> </h2>
                    <div class="col-md-8 col-md-offset-2">
                        <form name="frmfree" id="frmfree" method="post" action="transaction.php">
                            <input type="hidden" name="formname" value="frmmnth" id="formname">
                            <div class="location-price">
                                <label> Please Enter # of Employees </label>
                                <input type="text" name="freeprice" id="freeprice" value="<?php echo $count?>" placeholder="" onfocus="getprice()">
                            </div>

                            <div class="free-price">
                                <input type="button"name="freebutton" id="freebutton" value="Select" onclick="selectfree()" >
                            </div>
                        </form>
                    </div>
                </div>
            <?}?>

            <!--<div class="text-center per-price-con">
                <input type="checkbox" name="igree"> I agree with this <a href="#"> Payment Terms & Condition </a>
            </div>	-->
        </div>
    </div>
</section>
<section class="how-to-gray">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 con-tab-details" style="font-size: 19px;padding: 40px 0;
  text-align: center;">
                <b>  Note:</b> Billing cycle is month to month.It starts from 1st of each month, if user joins
                <br>e.g. on 15th, then user will be only charged for 15 balance days as a pro rata for first month.
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Add On</h2>
            </div>
        </div>
        <div class="row con-retailer-tab">
            <table width="100%">

                <tr>
                    <td style="width: 60%;">
                        <div class="col-md-10 con-tab-details">
                            Add Promotion (Available ONLY for Distributors & Manufacturers)
                        </div>
                        <div class="col-md-1">
                            <a href='#' class="tooltips"> <img class="text-right" src="assets/images/add-on.png" alt=""><span>Once you signup as a paid user, you can add promotions for a specific retailer or by city as needed (additional charges may apply).<br/></span> 	</a>
                        </div>
                    </td>

                    <td style="width: 20%;">
                        <div class="text-center con-tab-det-sec">Contact us for pricing & process</div>
                    </td>
                    <td>
                        <div class="text-center"> <a href="index.php#contact" class="con-tab-details-btn" target="_blank"> Contact Us </a></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 60%;">
                        <div class="col-md-10 con-tab-details">
                            Add Additional Hierarchy (e.g. managers/management by geographic preferences)
                        </div>
                        <div class="col-md-1">
                            <a href='#' class="tooltips"> <img class="text-right" src="assets/images/add-on.png" alt=""><span>On request we can give access to your data specific to employees/manager by territory e.g. Boston District Mananger can see Boston Data  & Los Angeles Manager can see Los Angeles data for a low cost. <br/></span> 	</a>
                        </div>
                    </td>

                    <td style="width: 20%;">
                        <div class="text-center con-tab-det-sec">Contact us for pricing & process</div>
                    </td>
                    <td>
                        <div class="text-center"> <a href="index.php#contact" class="con-tab-details-btn" target="_blank"> Contact Us </a></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 60%;">
                        <div class="col-md-10 con-tab-details">
                            Add active User/Employee to  a new/existing User/group
                        </div>
                        <div class="col-md-1">
                            <a href='#' class="tooltips"> <img class="text-right" src="assets/images/add-on.png" alt=""><span>On request we can link an active user who signed up prior to your signup & provide you full visibility to the employee records/data done in the past for a low cost.Terms & Conditions apply. <br/></span> 	</a>
                        </div>
                    </td>

                    <td>
                        <div class="text-center con-tab-det-sec">Contact us for pricing & process</div>
                    </td>
                    <td>
                        <div class="text-center"> <a href="index.php#contact" class="con-tab-details-btn" target="_blank"> Contact Us </a></div>
                    </td>
                </tr>

            </table>

        </div>

        <div class="row">
            <button class="con-retailer-btn"  onclick="submitform()"> Next </button>
        </div>

    </div>
</section>

<link rel="stylesheet" href="<?php echo $site_url; ?>assets/css/alertify.core.css"/>
<link rel="stylesheet" href="<?php echo $site_url; ?>assets/css/alertify.default.css" id="toggleCSS"/>

<script type="text/javascript" src="<?= $site_url; ?>assets/js/alertify.min.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $(".ser_title").click(function(){
            $(this).children("p").slideToggle();
            $(this).children("a").toggleClass("ser_title_red");
        });
    });
</script>

<script>


    function checkpromo()
    {


        var price=document.getElementById("aprice").value;
//        if(price<=0)
//        {
//            document.getElementById('aprice').value="1";
//            var price=document.getElementById("aprice").value;
//
//        }
        var planprice=document.getElementById("aplanprice").value;
        var code=document.getElementById("promocode").value;

        var ajax_url    =   '<?php echo $ajax_url; ?>';
        $.ajax({
            url:ajax_url+"ajax-promo.php",
            type:'POST',
            data:{code:code,count:price,planprice:planprice,plantype:'annual'},
            success: function (result) {
                var data=JSON.parse(result);
                if(data.OUTPUT =="0.00" )
                {

                     document.getElementById("aprice").value = data.count;
                    document.getElementById('atotal').value=data.OUTPUT ;
                }
                else
                {
                    document.getElementById("aprice").value = data.count;
                    document.getElementById('atotal').value=data.OUTPUT ;
                }
                //document.getElementById('atotal').value=data.OUTPUT ;
                document.getElementById('aspan').Text=data.MSG ;
                $("#aspan").text(data.MSG);
                $("#aspan").css("color",data.color);
                $("#aspan").css("font-size","17px");
            }
        });

    }
    function checkpromomnth()
    {

        var price=document.getElementById("mnprice").value;
//        if(price<=0)
//        {
//            document.getElementById('mnprice').value="1";
//            var price=document.getElementById("mnprice").value;
//
//        }
        var planprice=document.getElementById("mplanprice").value;
        var code=document.getElementById("mpromocode").value;

        var ajax_url    =   '<?php echo $ajax_url; ?>';
        $.ajax({
            url:ajax_url+"ajax-promo.php",
            type:'POST',
            data:{code:code,count:price,planprice:planprice,plantype:'month'},
            success: function (result) {
                var data=JSON.parse(result);
                if(data.OUTPUT =="0.00" )
                {

                   document.getElementById("mnprice").value = data.count;
                    document.getElementById('mtotal').value=data.OUTPUT ;
                }
                else
                {
                    document.getElementById("mnprice").value = data.count;
                    document.getElementById('mtotal').value=data.OUTPUT ;
                }
                // document.getElementById('mtotal').value=data.OUTPUT ;
                document.getElementById('mspan').Text=data.MSG ;
                $("#mspan").text(data.MSG);
                $("#mspan").css("color",data.color);
                $("#mspan").css("font-size","17px");



            }
        });

    }
    function getprice()
    {
        var price=document.getElementById("freeprice").value;
        if(price<=0)
        {
            document.getElementById('freeprice').value="1" ;
        }

    }

    function getmprice()
    {
        var mprice=document.getElementById("mnprice").value;
        var mplanprice=document.getElementById("mplanprice").value;
        var day=document.getElementById("days").value;
        var total=mprice * day * (mplanprice/30);
        var finalprice=Number((total).toFixed(2));
        document.getElementById('mtotal').value=finalprice ;
    }

    function selectannual()
    {
        document.getElementById("formname").value="frmanual";
        $("#annualbutton").removeClass("free-price");
        $("#annualbutton").addClass("prices-selected");
        $("#mnthbutton").addClass("free-price");
        $("#mnthbutton").removeClass("prices-selected");
        $("#freebutton").addClass("free-price");
        $("#freebutton").removeClass("prices-selected");



    }

    function selectmonth()
    {
        document.getElementById("formname").value="frmmnth";
        $("#mnthbutton").removeClass("free-price");
        $("#mnthbutton").addClass("prices-selected");
        $("#freebutton").addClass("free-price");
        $("#freebutton").removeClass("prices-selected");
        $("#annualbutton").addClass("free-price");
        $("#annualbutton").removeClass("prices-selected");

    }

    function selectfree()
    {

        document.getElementById("formname").value="frmfree";
        $("#freebutton").removeClass("free-price");
        $("#freebutton").addClass("prices-selected");
        $("#annualbutton").addClass("free-price");
        $("#annualbutton").removeClass("prices-selected");
        $("#mnthbutton").addClass("free-price");
        $("#mnthbutton").removeClass("prices-selected");
    }
    function submitform()
    {

        var form=document.getElementById("formname").value;
        if(form=="frmfree")
        {
            var fprice=document.getElementById("freeprice").value;
            if(fprice>0)
            {
                document.getElementById(form).submit();
            }
            else
            {
                alertify.error("Please Enter Employee")
            }
        }
        else if(form=="frmanual")
        {
            var fprice=document.getElementById("aprice").value;
            if(fprice>0)
            {
                document.getElementById(form).submit();
            }
            else
            {
                alertify.error("Please Enter Employee")
            }
        }
        else
        {
            var fprice=document.getElementById("mnprice").value;
            if(fprice>0)
            {
                document.getElementById(form).submit();
            }
            else
            {
                alertify.error("Please Enter Employee")
            }
        }


    }
</script>


<?php

unset($_SESSION['frnt_payment_msg']);
include("footer.php"); ?>

