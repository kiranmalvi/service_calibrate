<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 30/3/15
 * Time: 2:39 PM
 */
require_once('include.php');


//IF user session exist and his account's payment date is greater then current time then allow login else redirected to payment page according to user type
if (isset($_SESSION['SC_LOGIN']['USER']) && $_SESSION['SC_LOGIN']['USER']['allowlogin']=="yes")
{
    header('location:admin/index.php');
}
else
{



    if (isset($_REQUEST['login']))
    {
        $email = $_REQUEST['email'];
        //echo $_REQUEST['password'];
        $password = md5($_REQUEST['password']);

        $SQL = "SELECT u.* FROM user u
                LEFT JOIN user_settings us on u.iUserId = us.iUserId
                WHERE u.vEmail = '{$email}' AND u.vPassword = '{$password}' AND u.eStatus = '1'";

        $DATA = $obj->select($SQL);

        if (count($DATA) > 0) {
            $login = array(
                'iUserId' => $DATA[0]['iUserId'],
                'iParentId' => $DATA[0]['iParentId'],
                'iUserTypeId' => $DATA[0]['iUserTypeId'],
                'iUserRoleId' => $DATA[0]['iUserRoleId'],
                'iTypeOfStoreId' => $DATA[0]['iTypeOfStoreId'],
                'iPlanId' => $DATA[0]['iPlanId'],
                'iIndustriesId' => $DATA[0]['iIndustriesId'],
                'vStoreName' => $DATA[0]['vStoreName'],
                'vStoreUniqueId' => $DATA[0]['vStoreUniqueId'],
                'NumberOfStore' => $DATA[0]['NumberOfStore'],
                'vFirstName' => $DATA[0]['vFirstName'],
                'vLastName' => $DATA[0]['vLastName'],
                'vEmail' => $DATA[0]['vEmail'],
                'eActivePaidPlan' => $DATA[0]['eActivePaidPlan'],
                'ePlanType' => $DATA[0]['ePlanType'],
                'dtLastLogin' => $DATA[0]['dtLastLogin'],
                'vCity' => $DATA[0]['vCity'],
                'vZip' => $DATA[0]['vZip'],
                'vState' => $DATA[0]['vState'],
                'iCountryId' => $DATA[0]['iCountryId'],
                'iExpireTime' => $DATA[0]['iExpireTime'],

            );
#pr($login);exit;
            $_SESSION['SC_LOGIN']['USER'] = $login;
          $TIME = strtotime(gmdate('Y-m-d H:i:s'));

                if ($DATA[0]['iParentId'] == '0')
                {
                    if ($DATA[0]['iExpireTime'] > $TIME)
                    {
                        $_SESSION['SC_LOGIN']['USER']['allowlogin']="yes";
                        $PLAN = true;
                    }
                    else
                    {
                        $_SESSION['SC_LOGIN']['USER']['allowlogin']="no";
                        $PLAN = false;
                    }
                }
                else
                {
                    $PARENT_PLAN = $obj->select("SELECT iExpireTime,eActivePaidPlan FROM user WHERE iUserId = " . $DATA[0]['iParentId']);

                    //pr($PARENT_PLAN);exit;
                    if ($PARENT_PLAN[0]['iExpireTime'] > $TIME)
                    {
                        $_SESSION['SC_LOGIN']['USER']['allowlogin']="yes";
                        $PLAN = true;
                    } else
                    {
                        $_SESSION['SC_LOGIN']['USER']['allowlogin']="no";
                        $PLAN = false;
                    }
                }
            


            $UPDATE = "UPDATE user_settings SET iDtLogin = '{$TIME}',eDeviceType = 'web' , vLastLoginIP = '{$_SERVER['REMOTE_ADDR']}' WHERE iUserId = '{$DATA[0]['iUserId']}'";
            $obj->sql_query($UPDATE);

            if ($PLAN === true) {
                $generalfuncobj->func_set_temp_sess_msg($DATA[0]['vFirstName'] . ' ' . $DATA[0]['vLastName'], null, 'WelCome');
                $id = $_SESSION['SC_LOGIN']['USER']['iUserId'];
                header("location:admin/index.php?file=su-sudashboard&iId=$id");
            } else {
               if($DATA[0]['iUserTypeId']=="4" || $DATA[0]['iUserTypeId']=="5" || $DATA[0]['iUserTypeId']=="6")
                {
                    header('location:payment_retailers.php?page=login');
                }
                else if($DATA[0]['iUserTypeId']=="2")
                {
                    header('location:payment_distributor.php?page=login');
                }
                else
                {
                    header('location:payment_manufacturer.php?page=login');
                }

            }
        } else {
            $error = 'You are not Authorized User';
        }
    }

    ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="mind" >
        <link rel="shortcut icon" href="images/favicon.png">

        <title><?php echo $ADMIN_PANEL_TITLE ?></title>

        <link href="<?php echo $admin_url; ?>assets/js/iCheck/skins/minimal/red.css" rel="stylesheet">
        <link href="<?php echo $admin_url; ?>assets/js/iCheck/skins/square/red.css" rel="stylesheet">
        <link href="<?php echo $admin_url; ?>assets/js/iCheck/skins/flat/red.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css"
              href="<?php echo $admin_url; ?>assets/js/bootstrap-colorpicker/css/colorpicker.css"/>
        <!--Core CSS -->
        <link href="<?php echo $admin_url; ?>assets/bs3/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $admin_url; ?>assets/js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
        <link href="<?php echo $admin_url; ?>assets/css/bootstrap-reset.css" rel="stylesheet">
        <link href="<?php echo $admin_url; ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!--dynamic table-->
        <link href="<?php echo $admin_url; ?>assets/js/advanced-datatable/css/demo_page.css" rel="stylesheet"/>
        <link href="<?php echo $admin_url; ?>assets/js/advanced-datatable/css/demo_table.css" rel="stylesheet"/>
        <link rel="stylesheet" href="<?php echo $admin_url; ?>assets/js/data-tables/DT_bootstrap.css"/>
        <link href="<?php echo $admin_url; ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo $admin_url; ?>assets/css/style-responsive.css" rel="stylesheet"/>


        <!-- Form CSS -->
        <link rel="stylesheet" href="<?php echo $admin_url; ?>assets/css/bootstrap-switch.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo $admin_url; ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo $admin_url; ?>assets/js/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>

        <link rel="stylesheet" type="text/css"
              href="<?php echo $admin_url; ?>assets/js/bootstrap-datepicker/css/datepicker.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo $admin_url; ?>assets/js/bootstrap-timepicker/css/timepicker.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo $admin_url; ?>assets/js/bootstrap-colorpicker/css/colorpicker.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo $admin_url; ?>assets/js/bootstrap-datetimepicker/css/datetimepicker.css"/>

        <link rel="stylesheet" type="text/css"
              href="<?php echo $admin_url; ?>assets/js/jquery-multi-select/css/multi-select.css"/>
        <link rel="stylesheet" type="text/css"
              href="<?php echo $admin_url; ?>assets/js/jquery-tags-input/jquery.tagsinput.css"/>

        <link rel="stylesheet" type="text/css" href="<?php echo $admin_url; ?>assets/js/select2/select2.css"/>


        <!-- Dashboard CSS -->
        <link href="<?php echo $admin_url; ?>assets/js/jvector-map/jquery-jvectormap-1.2.2.css" rel="stylesheet">
        <link href="<?php echo $admin_url; ?>assets/css/clndr.css" rel="stylesheet">
        <!--clock css-->
        <link href="<?php echo $admin_url; ?>assets/js/css3clock/css/style.css" rel="stylesheet">
        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="<?php echo $admin_url; ?>assets/js/morris-chart/morris.css">


        <!-- Custom styles for this template -->
        <link href="<?php echo $admin_url; ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo $admin_url; ?>assets/css/style-responsive.css" rel="stylesheet"/>

        <!-- Gritter -->
        <link rel="stylesheet" type="text/css"
              href="<?php echo $admin_url; ?>assets/js/gritter/css/jquery.gritter.css"/>

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]>
        <script src="<?php echo $admin_url; ?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
    <script src="<?php echo $admin_url; ?>assets/js/html5shiv.js"></script>
    <script src="<?php echo $admin_url; ?>assets/js/respond.min.js"></script>

    <!--js Required for date-picker-->
        <!--    <script src="-->
        <?php //echo $admin_url; ?><!--assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"> </script>-->
        <script src="<?php echo $admin_url; ?>assets/js/jquery-1.8.3.min.js"></script>
        <!--    <script src="--><?php //echo $admin_url; ?><!--assets/js/advanced-form.js"> </script>-->

        <!-- -->

        <script src="<?php echo $admin_url; ?>assets/js/mi_general.js"></script>
    </head>
    <body class="login-body">

    <div class="container" style="margin: 40px auto;">
        <div class="col-md-offset-4 col-sm-offset-3">
			<a class="navbar-brand" href="#page-top">
                <img class="col-xs-12" alt="Service Calibrate" src="<? echo $site_url ?>assets/images/qrlogo.png">
			</a>
		</div>
        <form class="form-signin" method="post" style="margin: 200px auto;">
            <h2 class="form-signin-heading">
                Login
                <?php
                if (isset($error)) {
                    echo '<br><br>';
                    echo '<span><b>' . $error . '</b></span>';
                }
                ?>

            </h2>

            <div class="login-wrap">
                <div class="user-login-info">

                <input type="text" name="email" class="form-control" placeholder="User ID" autofocus>
                    <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                <input class="btn btn-lg btn-login btn-block" type="submit" value="Sign in" name="login">
                <a href="register.php" class="forgotpassword col-md-6">Register</a>
				<a href="forgot_password.php" class="forgotpassword">Forgot Password</a>
            </div>
        </form>

    </div>


    </body>

    </html>

<?php } ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
    function settime()
    {
        var visitortime = new Date();
        var visitortimezone = -visitortime.getTimezoneOffset() / 60;
        var ajax_url    =   '<?php echo $ajax_url; ?>';
        $.ajax({
            url:ajax_url+"ajax_timezone.php",
            type:'POST',
            data:{"time":visitortimezone},
            success: function (result) {
                console.log(result);
            }
        });
    }
    settime();


</script>



<?php

echo $generalfuncobj->getUserTime($_SESSION['SC_userTimeZone'])?>
