<?php

/* TOP START */

define('DASHBOARD', 'डैशबोर्ड');
define('HOME', 'मुखपृष्ठ');
define('COMING_SOON', 'शीघ्र आ रहा है');
define('MY_PROFILE', 'मेरी प्रोफाइल');
define('CHANGE_PASSWORD', 'पासवर्ड बदलें');
define('LOG_OUT', 'लॉग आउट');
define('ADMIN_LIST', 'व्यवस्थापक सूची');
define('SETTING', 'सेटिंग्स');

/* TOP END */

/* MENU START */

define('ADMIN', 'व्यवस्थापक');
define('USER', 'प्रयोक्ता');
define('CATEGORY', 'श्रेणी');
define('BUILDING', 'भवन');
define('UTILITY', 'उपयोगिता');
define('CMS', 'सीएमएस');
define('GENERAL_SETTINGS', 'सामान्य सेटिंग्स');
define('EMAIL_FORMAT', 'ईमेल प्रारूप');

/* MENU END */

/* User START */

define('USER_INFORMATION', 'उपयोगकर्ता जानकारी');
define('VIEW_USER_DETAILS', 'उपयोगकर्ता का विवरण देखें');
define('USER_UPDATE', 'अद्यतन उपयोगकर्ता की जानकारी');
define('EDIT_USER_DETAILS', 'उपयोगकर्ता प्रोफ़ाइल संपादित करें');
define('PLAYER_IMAGE', 'तस्विर अपलोड करें');
define('PLAYER_SELECT_FILE', 'फ़ाइल का चयन करें');
define('PLAYER_VIEW_IMAGE', 'छवि देखें');
define('PLAYER_REMOVE_IMAGE', 'छवि निकालें');
define('ADDRESS', 'पत्ता');
define('FULL_NAME', 'पूरा नाम');
define('USER_LIST', 'उपयोगकर्ता सूची');
define('USER_NAME', 'उपयोक्ता का नाम');
define('USER_DESC', 'उपयोगकर्ता का वर्णन');
define('USER_ADD_NEW', 'नई उपयोगकर्ता विवरण जोड़ें');
define('ADD_PLAYER_MSG', 'नई उपयोगकर्ता जानकारी सफलतापूर्वक जोड़ दिया गया है।');
define('UPDATE_PLAYER_MSG', 'उपयोगकर्ता जानकारी सफलतापूर्वक अपडेट किया गया।');
define('IMAGE', 'छवि');
define('USER_SOCIAL_LIST', 'उपयोगकर्ता सामाजिक सूची');
define('TYPE', 'प्रकार');


/* User END */

/* Start Category */

define('CATEGORY_UPDATE', 'अद्यतन श्रेणी की जानकारी');
define('CATEGORY_ADD_NEW', 'नई श्रेणी विस्तार से जोड़ने');
define('EDIT_CATEGORY_DETAILS', 'संपादित श्रेणी प्रोफ़ाइल');
define('CATEGORY_LIST', 'श्रेणी सूची');
define('CATEGORY_NAME', 'श्रेणी नाम');
define('VIEW_CATEGORY_DETAILS', 'देखें श्रेणी के विवरण');
define('ADD_CATEGORY_MSG', 'नई श्रेणी के विवरण को सफलता पूर्वक जोड़ दिया गया है।');
define('UPDATE_CATEGORY_MSG', 'श्रेणी के विवरण सफलतापूर्वक अपडेट किया गया।');
define('CATEGORY_INFORMATION', 'श्रेणी की जानकारी');


/* End Category */

/* Start Building */

define('BUILDING_UPDATE', 'अद्यतन भवन निर्माण की जानकारी');
define('BUILDING_ADD_NEW', 'नया भवन विस्तार से जोड़ने');
define('EDIT_BUILDING_DETAILS', 'संपादित निर्माण प्रोफ़ाइल');
define('BUILDING_LIST', 'भवन निर्माण की सूची');
define('BUILDING_TYPE', 'इमारत के प्रकार');
define('VIEW_BUILDING_DETAILS', 'देखें बिल्डिंग विवरण');
define('ADD_BUILDING_MSG', 'नया भवन के विवरण को सफलता पूर्वक जोड़ दिया गया है।');
define('UPDATE_BUILDING_MSG', 'बिल्डिंग विवरण सफलतापूर्वक अद्यतन किया।');
define('BUILDING_INFORMATION', 'भवन सूचना');


/* End Building */

/* Start Complaint */

define('COMPLAINT', 'शिकायत');
define('COMPLAINT_UPDATE', 'अद्यतन शिकायत की जानकारी');
define('COMPLAINT_ADD_NEW', 'नई शिकायत विस्तार से जोड़ने');
define('EDIT_COMPLAINT_DETAILS', 'संपादित शिकायत विस्तार');
define('LOG_COMPLAINT_DETAILS', 'शिकायत प्रवेश विवरण');
define('COMPLAINT_LIST', 'शिकायत सूची');
define('COMPLAINT_TYPE', 'शिकायत प्रकार');
define('VIEW_COMPLAINT_DETAILS', 'शिकायत विवरण देखें');
define('ADD_COMPLAINT_MSG', 'नई शिकायत के विवरण को सफलता पूर्वक जोड़ दिया गया है।');
define('UPDATE_COMPLAINT_MSG', 'शिकायत विवरण सफलतापूर्वक अद्यतन।');
define('COMPLAINT_INFORMATION', 'शिकायत सूचना');
define('COMPLAINTJOIN_INFORMATION', 'शिकायत प्रवेश सूचना');
define('BUILDING_NAME', 'भवन का नाम');
define('LOCATION', 'स्थान');
define('DESCRIBEISSUE', 'समस्या का वर्णन');
define('LATITUDE', 'अक्षांश');
define('LONGITUDE', 'रेखांश');


/* End Complaint */

/* ADMIN START */

define('NAME', 'नाम');
define('EMAIL', 'ईमेल');
define('ADDED_DATE', 'तिथि जोड़ी गई');
define('LAST_LOGIN', 'अंतिम लॉगइन');
define('FIRST_NAME', 'पहला नाम');
define('LAST_NAME', 'अंतिम नाम');
define('EMAIL_ADDRESS', 'ईमेल पत्ता');
define('PASSWORD', 'पासवर्ड');
define('CONFIRM_PASSWORD', 'पासवर्ड की पुष्टि');
define('ADD_NEW_ADMIN', 'नई व्यवस्थापक उपयोगकर्ता जोड़ें');
define('UPDATE_ADMIN', 'अद्यतन व्यवस्थापक');
define('UPDATE_ADMIN_MSG', 'प्रोफाइल को सफलतापूर्वक अपडेट किया गया');
define('ADD_ADMIN_MSG', 'नए व्यवस्थापक विवरण सफलतापूर्वक जोड़ दिया गया है');
define('EMAIL_MSG', 'ईमेल पहले से मौजूद है');


/* ADMIN END */

/* GENERAL KEY WORD START */

define('SHOW', 'दिखाएँ');
define('RECORDS', 'रिकार्ड');
define('SEARCH_BY', 'आधार पर खोजें');
define('STATUS', 'स्थिति');
define('W_Y_S_K', 'आपकी खोज कीवर्ड लिखें');
define('GO', 'जाएं');
define('SHOWING', 'दिखा रहा है');
define('TO', 'से');
define('OF', 'में से');
define('A_TO_Z', 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z सभी');
define('ACTION', 'क्रिया');
define('N_R_A', 'रिकार्ड उपलब्ध नहीं है');
define('SHOW_ALL', 'सब दिखाएं');
define('ADD_NEW', 'नया जोडें');
define('SELECT_STATUS', 'चयन करें स्थिति');
define('ACTIVE', 'सक्रिय');
define('INACTIVE', 'निष्क्रिय');
define('PENDING', 'लंबित');
define('CLOSE', 'बंद');
define('DELETE', 'हटाएं');
define('ADD', 'जोडें');
define('CANCEL', 'रद्द करें');
define('SAVE', 'सेव');
define('EDIT', 'संपादित करें');
define('REMOVE', 'निकालें');
define('ALL', 'सब');
define('PROFILE', 'प्रोफाइल');
define('CURPWD', 'मौजूदा पासवर्ड');
define('NPWD', 'नया पासवर्ड');
define('CONPWD', 'पासवर्ड की पुष्टि करें');
define('ENT_CURPWD', 'अपना वर्तमान पासवर्ड दर्ज करें');
define('ENT_NPWD', 'अपना नया पासवर्ड दर्ज करें');
define('ENT_CONPWD', 'अपना पासवर्ड पुन: दर्ज करें');
define('YOUR', 'आपका');
define('SEARCH', 'खोजें');
define('FOR', 'के लिए');
define('HAS', 'किया है');
define('FOUND', 'पाया गया');
define('SUCCESS', 'सफलतापूर्वक');
define('DELETE_MSG', 'रिकॉर्ड सफलतापूर्वक नष्ट कर दिया');


/* GENERAL KEY WORD END */
define('USER_SELECT', '- चुनें प्रयोक्ता -');
define('BUILDING_SELECT', '- चुनें बिल्डिंग के प्रकार -');
define('CATEGORY_SELECT', '- श्रेणी का चयन करें -');


/* Change Password Start */

define('CHNG_PWD', 'Password changed successfully.');
define('CHNG_PWD_ERROR', 'Your newpassword and confirmpassword is not matched.');
define('CUR_PWD_ERROR', 'Your current password is not matched');

/* Change Password  End */

/* Email format Start */

define('ADD_EMAIL_MSG', 'Add New Email Format');
define('UPDATE_EMAIL_MSG', 'Update Email Format');
define('EMAIL_NAME', 'Email Format Title');
define('EMAIL_SUB', 'Email Format Subject');
define('EMAIL_DESC', 'Email Format Description');
define('EMAIL_LIST', 'Email Format List');
define('NEW_EMAIL_MSG', 'New Email details has been added successfully.');
define('UPDATE_EMAIL', 'Email Format details updated successfully.');


/* Email format End */


/* All Error Message Start */

define('ADMIN_ERROR_FN', 'पहला नाम निर्दिष्ट करें।');
define('ADMIN_ERROR_LN', 'अंतिम नाम निर्दिष्ट करें।');
define('ADMIN_EMAIL1', 'हम आपसे संपर्क करने के लिए अपने ईमेल पते की जरूरत है।');
define('ADMIN_EMAIL2', 'आपका ईमेल पता name@domain.com के प्रारूप में होना चाहिए');
define('ADMIN_PWD', 'अपना पासवर्ड दर्ज करें');
define('ADMIN_PWD1', 'न्यूनतम 6 वर्ण दर्ज करें');
define('ADMIN_CPWD', 'पासवर्ड की पुष्टि दर्ज करें।');
define('ADMIN_CPWD1', 'फिर से वही मान दर्ज करें।');

define('USER_ERROR_FNAME', 'पहला नाम निर्दिष्ट करें');
define('USER_ERROR_LNAME', 'अंतिम नाम निर्दिष्ट करें');
define('USER_ERROR_ADDRESS', 'पता निर्दिष्ट करें');
define('USER_ERROR_EMAIL', 'ईमेल निर्दिष्ट करें');

define('CATEGORY_ERROR_NAME', 'श्रेणी नाम निर्दिष्ट करें');

define('BUILDING_ERROR_NAME', 'भवन का नाम निर्दिष्ट करें');


/* All Error Message End */


/* New Detail Start  */

define('CHAMPIONSHIP_TEAM_LIST', 'Championship Team List');
define('YEAR', 'Year');
define('ADD_NEW_CHAMP_WITH_TEAM', 'Add New Championship With Team');
define('UPDATE_CHAMP_TEAM_INFO', 'Update Championship Team Info');
define('ENTER_YEAR', 'Please Enter Year');

define('CHAMP_DETAIL_LIST', 'Championship Detail List');
define('TOTAL_MATCH', 'Total Match');
define('WON', 'Won');
define('DRAWN', 'Drawn');
define('LOST', 'Lost');
define('GOAL_SCORING', 'Goal Scoring');
define('GOAL_CONCEDING', 'Goal Conceding');


define('COACH_NATIONALITY', 'Coach Nationality');
define('UPLOAD_IMAGE', 'Upload Image');
define('IMAGE', 'Image');
define('SELECT_IMAGE', 'Select File');
define('CHANGE', 'Change');
define('VIEW_IMAGE', 'View Image');
define('REMOVE_IMAGE', 'Remove Image');
define('BIRTHDATE', 'BirthDate');
define('STARTDATE', 'Start Date');
define('ENDDATE', 'End Date');

define('COACH_DETAIL_LIST', 'Coach Detail List');
define('ADD_NEW_COACH_DETAIL', 'Add New Coach Detail');
define('UPDATE_COACH_DETAIL', 'Update Coach Detail');


define('PLAYER_NATIONALITY', 'Player Nationality');
define('PLAYER_BIRTHDATE', 'Player BirthDate');
define('PLAYER_TOTALMATCH', 'Player TotalMatch');
define('PLAYER_TOTALGOAL', 'Player TotalGoal');
define('PLAYER_IMAGE', 'Upload Image');
define('PLAYER_BDATE', 'Birth Date');
define('PLAYER_FDATE', 'Select Birth Date');
define('PLAYER_SELECT_FILE', 'Select file');
define('PLAYER_VIEW_IMAGE', 'View Image');
define('PLAYER_REMOVE_IMAGE', 'Remove Image');


define('PLAYER_DETAIL_LIST', 'Player Detail List');
define('ADD_NEW_PLAYER_DETAIL', 'Add New Player Detail');
define('UPDATE_PLAYER_DETAIL', 'Update Player Detail');
define('YELLOWCARD', 'Yellow Card');
define('REDCARD', 'Red Card');
define('PENALTY_GOAL', 'Penalty Goal');
define('MISSED_PENALTY_GOAL', 'Missed Penalty Goal');
define('FULL_MATCH_PLAYED', 'Full Match Played');
define('MATCH_AS_SUBSTITUTE', 'Played Match As Substitute');
define('MATCH_WAS_SUBSTITUTE', 'Played Match Was Substitute');


define('TEAM_DETAIL_LIST', 'Team Detail List');
define('ADD_NEW_TEAM_DETAIL', 'Add New Team Detail');
define('UPDATE_TEAM_DETAIL', 'Update Team Detail');
define('TOTAL_GOAL', 'Toatl Goal');


define('COACH_ERROR_COACH_NAME', 'Please Select Coach Name');
define('COACH_ERROR_CHAMP_NAME', 'Please Select Championship Name');
define('COACH_ERROR_TOTAL_MATCH', 'Enter Total Number of Match');
define('COACH_ERROR_WON', 'Please Enter Number of Won match');
define('COACH_ERROR_DRAWN', 'Please Enter Number of Drwan match');
define('COACH_ERROR_LOST', 'Please Enter Number of Lost match');
define('COACH_ERROR_GS', 'Please Enter Number of Goal Scoring ');
define('COACH_ERROR_GC', 'Please Enter Number of Goal Concending ');
define('COACH_ERROR_STATUS', 'Please Select Status');


define('PLAYER_ERROR_PLAYER_NAME', 'Please Select Player Name');
define('PLAYER_ERROR_CH_NAME', 'Please Select Championship Name');
define('PLAYER_ERROR_TOTALMATCH', 'Enter Total Number of Match');
define('PLAYER_ERROR_TOTALGOAL', 'Enter Total Number of Goal ');
define('PLAYER_ERROR_P', 'Please specify Penalty Goal');
define('PLAYER_ERROR_MP', 'Please specify Missed Penalty Goal');
define('PLAYER_ERROR_S1', 'Please specify Substitutions 1');
define('PLAYER_ERROR_R', 'Please specify Red Card');
define('PLAYER_ERROR_Y', 'Please specify Yellow cards');
define('PLAYER_FULL_MATCH_ERROR', 'Please Enter Full Match');
define('PLAYER_AS_SUB', 'Please Enter As Substitute Match');
define('PLAYER_WAS_SUB', 'Please Enter Was Substitute Match');


define('COACH_ERROR_VNATIONALITY', 'Please Specify Coach Nationality');

define('COACH_ERROR_VBIRTHDATE', 'Please Select Coach Birthdate');

define('PLAYER_ERROR_VNATIONALITY', 'Please Specify Player Nationality');

define('PLAYER_ERROR_VBIRTHDATE', 'Please Select Player Birthdate');
define('PLAYER_ERROR_VTOTALMATCH', 'Please Specify Number of match played');
define('PLAYER_ERROR_VTOTALGOAL', 'Please Specify Number of goal');


define('TEAM_ERROR_NAME', 'Please Select Team');
define('TEAM_ERROR_GOAL', 'Enter Total Number of Goal');
define('TEAM_ERROR_WON', 'Please Enter Won Match');
define('TEAM_ERROR_DRAWN', 'Please Enter Drawn Match');
define('TEAM_ERROR_LOST', 'Please Enter Lost Match');


define('CHAMPIONSHIP_CREATE_LIST', 'Championship Create List');
define('ADD_NEW_CREATE_CHAMP_WITH_SEASON', 'Create New Championship Season');
define('UPDATE_CREATE_CHAMP_SEASON_INFO', 'Update Create Championship Season Info');
define('SEASON_NAME', 'Season');
define('SEASON_ERROR_NAME', 'Please Add Season');
define('championship', 'championship');
define('Edit_champion', 'Edit Championship');
define('Match_list', 'Match list');

/* Player View start */

define('VIEW_PLAYER_DETAILS', 'View Player Details');
define('PLAYER_INFORMATION', 'Player Information');
define('FULL_NAME', 'Full Name');
define('TEAM_NAME', 'Team Name');
define('NATIONNALITY', 'Nationality');
define('GENERAL_INFORMATION', 'General Information');
define('TOTAL_MATCH', 'Total Match');
define('TOTAL_GOALS', 'Total Goals');
define('YELLOWCARD', 'Yellow card');
define('REDCARD', 'Red card');
define('PENALTYGOAL', 'Penalty Goal');
define('MISSED_PANALTY_GOALS', 'Missed penalty goal');
define('TOTALFULLMATCHPLAYED', 'Total full match played');
define('TOTALMATCHPLAYEDASSUBSTITUTE', 'Total match played as substitute');
define('TOTALMATCHPLAYEDWASUBSTITUTE', 'Total match played was substituted');

/* player view end */

/* start club member of club */

define('EDIT_CLUB_DETAILS', 'Edit Club Details');
define('COACH_DETAILS', 'Coach Details');
define('CHAMPIONSHIP_MATCH_LIST', 'Championship Match List');
define('COACH_LIST', 'Coach List');
define('MATCHID', 'MactchId');
define('TEAM1', 'Team 1');
define('TEAM2', 'Team 2');
define('PLACE', 'Place');
define('DATE', 'Date');
define('WINER', 'Winer');


/* End club member of club */

define('MATCH_CLICK_CHAMP', 'Click for Add New championship');
define('MATCH_CLICK_SEASON', 'Click for Add New Season');
define('MATCH_CLICK_VENUE', 'Click for Add venue');
define('MATCH_CLICK_MREFEREEE', 'Click for Add New Referee');
define('MATCH_CLICK_AREFEREEE', 'Click for Add New Assistant');
define('MATCH_CLICK_SREFEREEE', 'Click for Add New Substitute refree');
define('MATCH_CLICK_MNREFEREEE', 'Click for Add New Referee');
define('MATCH_CLICK_PRESIDENT', 'Click for Add New President');
define('MATCH_CLICK_COACH', 'Click for Add New coach');
define('MATCH_CLICK_PLAYER', 'Player List');
/* New Detail End  */

define('Championship_Details', 'Championship Details');
define('Referee_detail', 'Referee detail');
define('Club_1_Details', 'Club 1 Details');
define('Club_2_Details', 'Club 2 Details');
define('Match_Result', 'Match Result');
define('Team_1_score', 'Team 1 score');
define('Team_2_score', 'Team 2 score');
?>
