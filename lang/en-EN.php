<?php

/* TOP START */

define('DASHBOARD', 'Dashboard');
define('HOME', 'Home');
define('COMING_SOON', 'Coming Soon');
define('MY_PROFILE', 'My Profile ');
define('CHANGE_PASSWORD', 'Change Password');
define('LOG_OUT', 'Log Out');
define('ADMIN_LIST', 'Admin List');
define('SETTING', 'Settings');


/* TOP END */

/* MENU START */

define('ADMIN', 'Admin');
define('USER', 'User');
define('CATEGORY', 'Category');
define('BUILDING', 'Building');
define('UTILITY', 'Utility');
define('CMS', 'CMS');
define('GENERAL_SETTINGS', 'General Settings');
define('EMAIL_FORMAT', 'Email Format');

/* MENU END */

/* User START */

define('USER_INFORMATION', 'User Information');
define('VIEW_USER_DETAILS', 'View User Details');
define('USER_UPDATE', 'Update User Info');
define('EDIT_USER_DETAILS', 'Edit User Profile');
define('PLAYER_IMAGE', 'Upload Image');
define('PLAYER_SELECT_FILE', 'Select file');
define('PLAYER_VIEW_IMAGE', 'View Image');
define('PLAYER_REMOVE_IMAGE', 'Remove Image');
define('ADDRESS', 'Address');
define('FULL_NAME', 'Full Name');
define('USER_LIST', 'User List');
define('USER_NAME', 'User Name');
define('USER_DESC', 'User Description');
define('USER_ADD_NEW', 'Add New User Detail');
define('ADD_PLAYER_MSG', 'New User details has been added successfully.');
define('UPDATE_PLAYER_MSG', 'User details updated successfully.');
define('IMAGE', 'Image');
define('USER_SOCIAL_LIST', 'User Social List');
define('TYPE', 'Type');


/* User END */

/* Start Category */

define('CATEGORY_UPDATE', 'Update Category Info');
define('CATEGORY_ADD_NEW', 'Add New Category Detail');
define('EDIT_CATEGORY_DETAILS', 'Edit Category Profile');
define('CATEGORY_LIST', 'Category List');
define('CATEGORY_NAME', 'Category Name');
define('VIEW_CATEGORY_DETAILS', 'View Category Details');
define('ADD_CATEGORY_MSG', 'New Category details has been added successfully.');
define('UPDATE_CATEGORY_MSG', 'Category details updated successfully.');
define('CATEGORY_INFORMATION', 'Category Information');


/* End Category */

/* Start Building */

define('BUILDING_UPDATE', 'Update Building Info');
define('BUILDING_ADD_NEW', 'Add New Building Detail');
define('EDIT_BUILDING_DETAILS', 'Edit Building Profile');
define('BUILDING_LIST', 'Building List');
define('BUILDING_TYPE', 'Building Type');
define('VIEW_BUILDING_DETAILS', 'View Building Details');
define('ADD_BUILDING_MSG', 'New Building details has been added successfully.');
define('UPDATE_BUILDING_MSG', 'Building details updated successfully.');
define('BUILDING_INFORMATION', 'Building Information');


/* End Building */

/* Start Complaint */

define('COMPLAINT', 'Complaint');
define('COMPLAINT_UPDATE', 'Update Complaint Info');
define('COMPLAINT_ADD_NEW', 'Add New Complaint Detail');
define('EDIT_COMPLAINT_DETAILS', 'Edit Complaint Detail');
define('LOG_COMPLAINT_DETAILS', 'Complaint Log Details');
define('COMPLAINT_LIST', 'Complaint List');
define('COMPLAINT_TYPE', 'Complaint Type');
define('VIEW_COMPLAINT_DETAILS', 'View Complaint Details');
define('ADD_COMPLAINT_MSG', 'New Complaint details has been added successfully.');
define('UPDATE_COMPLAINT_MSG', 'Complaint details updated successfully.');
define('COMPLAINT_INFORMATION', 'Complaint Information');
define('COMPLAINTJOIN_INFORMATION', 'Complaint Log Information');
define('BUILDING_NAME', 'Building Name');
define('LOCATION', 'Location');
define('DESCRIBEISSUE', 'Describe Issue');
define('LATITUDE', 'Latitude');
define('LONGITUDE', 'Longitude');


/* End Complaint */

/* ADMIN START */

define('NAME', 'Name');
define('EMAIL', 'Email');
define('ADDED_DATE', 'Added Date');
define('LAST_LOGIN', 'Last Login');
define('FIRST_NAME', 'First Name');
define('LAST_NAME', 'Last Name');
define('EMAIL_ADDRESS', 'Email Address');
define('PASSWORD', 'Password');
define('CONFIRM_PASSWORD', 'Confirm Password');
define('ADD_NEW_ADMIN', 'Add New Admin User');
define('UPDATE_ADMIN', 'Update Admin');
define('UPDATE_ADMIN_MSG', 'Profile Updated Successfully');
define('ADD_ADMIN_MSG', 'New admin details has been added successfully.');
define('EMAIL_MSG', 'Email already exist.');


/* ADMIN END */

/* GENERAL KEY WORD START */

define('SHOW', 'Show');
define('RECORDS', 'Records');
define('SEARCH_BY', 'Search By');
define('STATUS', 'Status');
define('W_Y_S_K', 'Write Your Search Keyword');
define('GO', 'Go');
define('SHOWING', 'Showing');
define('TO', 'to');
define('OF', 'of');
define('A_TO_Z', 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z ALL');
define('ACTION', 'Action');
define('N_R_A', 'No Records Available');
define('SHOW_ALL', 'Show All');
define('ADD_NEW', 'Add New');
define('SELECT_STATUS', 'Select Status');
define('ACTIVE', 'Active');
define('INACTIVE', 'Inactive');
define('PENDING', 'Pending');
define('CLOSE', 'Close');
define('DELETE', 'Delete');
define('ADD', 'Add');
define('CANCEL', 'Cancel');
define('SAVE', 'Save');
define('EDIT', 'Edit');
define('REMOVE', 'Remove');
define('ALL', 'ALL');
define('PROFILE', 'Profile');
define('CURPWD', 'Current password');
define('NPWD', 'New Password');
define('CONPWD', 'Confirm Password');
define('ENT_CURPWD', 'Enter your current password');
define('ENT_NPWD', 'Enter your new password');
define('ENT_CONPWD', 'Re enter your password');
define('YOUR', 'Your');
define('SEARCH', 'search');
define('FOR', 'for');
define('HAS', 'has');
define('FOUND', 'found');
define('SUCCESS', 'Successfully');
define('DELETE_MSG', 'Record deleted successfully');


define('USER_SELECT', '- Select User -');
define('BUILDING_SELECT', '- Select Building Type -');
define('CATEGORY_SELECT', '- Select Category -');


/* GENERAL KEY WORD END */


/* Change Password Start */

define('CHNG_PWD', 'Password changed successfully.');
define('CHNG_PWD_ERROR', 'Your newpassword and confirmpassword is not matched.');
define('CUR_PWD_ERROR', 'Your current password is not matched');

/* Change Password  End */

/* Email format Start */

define('ADD_EMAIL_MSG', 'Add New Email Format');
define('UPDATE_EMAIL_MSG', 'Update Email Format');
define('EMAIL_NAME', 'Email Format Title');
define('EMAIL_SUB', 'Email Format Subject');
define('EMAIL_DESC', 'Email Format Description');
define('EMAIL_LIST', 'Email Format List');
define('NEW_EMAIL_MSG', 'New Email details has been added successfully.');
define('UPDATE_EMAIL', 'Email Format details updated successfully.');


/* Email format End */


/* All Error Message Start */

define('ADMIN_ERROR_FN', 'Please specify first name.');
define('ADMIN_ERROR_LN', 'Please specify last name.');
define('ADMIN_EMAIL1', 'We need your email address to contact you.');
define('ADMIN_EMAIL2', 'Your email address must be in the format of name@domain.com');
define('ADMIN_PWD', 'Please Enter Your Password');
define('ADMIN_PWD1', 'Please Enter min 6 Character');
define('ADMIN_CPWD', 'Please Enter Confirm Password.');
define('ADMIN_CPWD1', 'Please enter the same value again.');

define('USER_ERROR_FNAME', 'Please specify First Name');
define('USER_ERROR_LNAME', 'Please specify Last Name');
define('USER_ERROR_ADDRESS', 'Please specify Address');
define('USER_ERROR_EMAIL', 'Please specify Email');

define('CATEGORY_ERROR_NAME', 'Please specify Category Name');

define('BUILDING_ERROR_NAME', 'Please specify Building Name');


/* All Error Message End */


/* New Detail Start  */

define('CHAMPIONSHIP_TEAM_LIST', 'Championship Team List');
define('YEAR', 'Year');
define('ADD_NEW_CHAMP_WITH_TEAM', 'Add New Championship With Team');
define('UPDATE_CHAMP_TEAM_INFO', 'Update Championship Team Info');
define('ENTER_YEAR', 'Please Enter Year');

define('CHAMP_DETAIL_LIST', 'Championship Detail List');
define('TOTAL_MATCH', 'Total Match');
define('WON', 'Won');
define('DRAWN', 'Drawn');
define('LOST', 'Lost');
define('GOAL_SCORING', 'Goal Scoring');
define('GOAL_CONCEDING', 'Goal Conceding');


define('COACH_NATIONALITY', 'Coach Nationality');
define('UPLOAD_IMAGE', 'Upload Image');
define('IMAGE', 'Image');
define('SELECT_IMAGE', 'Select File');
define('CHANGE', 'Change');
define('VIEW_IMAGE', 'View Image');
define('REMOVE_IMAGE', 'Remove Image');
define('BIRTHDATE', 'BirthDate');
define('STARTDATE', 'Start Date');
define('ENDDATE', 'End Date');

define('COACH_DETAIL_LIST', 'Coach Detail List');
define('ADD_NEW_COACH_DETAIL', 'Add New Coach Detail');
define('UPDATE_COACH_DETAIL', 'Update Coach Detail');


define('PLAYER_NATIONALITY', 'Player Nationality');
define('PLAYER_BIRTHDATE', 'Player BirthDate');
define('PLAYER_TOTALMATCH', 'Player TotalMatch');
define('PLAYER_TOTALGOAL', 'Player TotalGoal');
define('PLAYER_BDATE', 'Birth Date');
define('PLAYER_FDATE', 'Select Birth Date');


define('PLAYER_DETAIL_LIST', 'Player Detail List');
define('ADD_NEW_PLAYER_DETAIL', 'Add New Player Detail');
define('UPDATE_PLAYER_DETAIL', 'Update Player Detail');
define('YELLOWCARD', 'Yellow Card');
define('REDCARD', 'Red Card');
define('PENALTY_GOAL', 'Penalty Goal');
define('MISSED_PENALTY_GOAL', 'Missed Penalty Goal');
define('FULL_MATCH_PLAYED', 'Full Match Played');
define('MATCH_AS_SUBSTITUTE', 'Played Match As Substitute');
define('MATCH_WAS_SUBSTITUTE', 'Played Match Was Substitute');


define('TEAM_DETAIL_LIST', 'Team Detail List');
define('ADD_NEW_TEAM_DETAIL', 'Add New Team Detail');
define('UPDATE_TEAM_DETAIL', 'Update Team Detail');
define('TOTAL_GOAL', 'Toatl Goal');


define('COACH_ERROR_COACH_NAME', 'Please Select Coach Name');
define('COACH_ERROR_CHAMP_NAME', 'Please Select Championship Name');
define('COACH_ERROR_TOTAL_MATCH', 'Enter Total Number of Match');
define('COACH_ERROR_WON', 'Please Enter Number of Won match');
define('COACH_ERROR_DRAWN', 'Please Enter Number of Drwan match');
define('COACH_ERROR_LOST', 'Please Enter Number of Lost match');
define('COACH_ERROR_GS', 'Please Enter Number of Goal Scoring ');
define('COACH_ERROR_GC', 'Please Enter Number of Goal Concending ');
define('COACH_ERROR_STATUS', 'Please Select Status');


define('PLAYER_ERROR_PLAYER_NAME', 'Please Select Player Name');
define('PLAYER_ERROR_CH_NAME', 'Please Select Championship Name');
define('PLAYER_ERROR_TOTALMATCH', 'Enter Total Number of Match');
define('PLAYER_ERROR_TOTALGOAL', 'Enter Total Number of Goal ');
define('PLAYER_ERROR_P', 'Please specify Penalty Goal');
define('PLAYER_ERROR_MP', 'Please specify Missed Penalty Goal');
define('PLAYER_ERROR_S1', 'Please specify Substitutions 1');
define('PLAYER_ERROR_R', 'Please specify Red Card');
define('PLAYER_ERROR_Y', 'Please specify Yellow cards');
define('PLAYER_FULL_MATCH_ERROR', 'Please Enter Full Match');
define('PLAYER_AS_SUB', 'Please Enter As Substitute Match');
define('PLAYER_WAS_SUB', 'Please Enter Was Substitute Match');


define('COACH_ERROR_VNATIONALITY', 'Please Specify Coach Nationality');

define('COACH_ERROR_VBIRTHDATE', 'Please Select Coach Birthdate');

define('PLAYER_ERROR_VNATIONALITY', 'Please Specify Player Nationality');

define('PLAYER_ERROR_VBIRTHDATE', 'Please Select Player Birthdate');
define('PLAYER_ERROR_VTOTALMATCH', 'Please Specify Number of match played');
define('PLAYER_ERROR_VTOTALGOAL', 'Please Specify Number of goal');


define('TEAM_ERROR_NAME', 'Please Select Team');
define('TEAM_ERROR_GOAL', 'Enter Total Number of Goal');
define('TEAM_ERROR_WON', 'Please Enter Won Match');
define('TEAM_ERROR_DRAWN', 'Please Enter Drawn Match');
define('TEAM_ERROR_LOST', 'Please Enter Lost Match');


define('CHAMPIONSHIP_CREATE_LIST', 'Championship Create List');
define('ADD_NEW_CREATE_CHAMP_WITH_SEASON', 'Create New Championship Season');
define('UPDATE_CREATE_CHAMP_SEASON_INFO', 'Update Create Championship Season Info');
define('SEASON_NAME', 'Season');
define('SEASON_ERROR_NAME', 'Please Add Season');
define('championship', 'championship');
define('Edit_champion', 'Edit Championship');
define('Match_list', 'Match list');

/* Player View start */

define('VIEW_USER_DETAILS', 'View User Details');
define('PLAYER_INFORMATION', 'Player Information');
define('FULL_NAME', 'Full Name');
define('TEAM_NAME', 'Team Name');
define('NATIONNALITY', 'Nationality');
define('GENERAL_INFORMATION', 'General Information');
define('TOTAL_MATCH', 'Total Match');
define('TOTAL_GOALS', 'Total Goals');
define('YELLOWCARD', 'Yellow card');
define('REDCARD', 'Red card');
define('PENALTYGOAL', 'Penalty Goal');
define('MISSED_PANALTY_GOALS', 'Missed penalty goal');
define('TOTALFULLMATCHPLAYED', 'Total full match played');
define('TOTALMATCHPLAYEDASSUBSTITUTE', 'Total match played as substitute');
define('TOTALMATCHPLAYEDWASUBSTITUTE', 'Total match played was substituted');

/* player view end */

/* start club member of club */

define('EDIT_CLUB_DETAILS', 'Edit Club Details');
define('COACH_DETAILS', 'Coach Details');
define('CHAMPIONSHIP_MATCH_LIST', 'Championship Match List');
define('COACH_LIST', 'Coach List');
define('MATCHID', 'MactchId');
define('TEAM1', 'Team 1');
define('TEAM2', 'Team 2');
define('PLACE', 'Place');
define('DATE', 'Date');
define('WINER', 'Winer');


/* End club member of club */

define('MATCH_CLICK_CHAMP', 'Click for Add New championship');
define('MATCH_CLICK_SEASON', 'Click for Add New Season');
define('MATCH_CLICK_VENUE', 'Click for Add venue');
define('MATCH_CLICK_MREFEREEE', 'Click for Add New Referee');
define('MATCH_CLICK_AREFEREEE', 'Click for Add New Assistant');
define('MATCH_CLICK_SREFEREEE', 'Click for Add New Substitute refree');
define('MATCH_CLICK_MNREFEREEE', 'Click for Add New Referee');
define('MATCH_CLICK_PRESIDENT', 'Click for Add New President');
define('MATCH_CLICK_COACH', 'Click for Add New coach');
define('MATCH_CLICK_PLAYER', 'Player List');
/* New Detail End  */

define('Championship_Details', 'Championship Details');
define('Referee_detail', 'Referee detail');
define('Club_1_Details', 'Club 1 Details');
define('Club_2_Details', 'Club 2 Details');
define('Match_Result', 'Match Result');
define('Team_1_score', 'Team 1 score');
define('Team_2_score', 'Team 2 score');

?>