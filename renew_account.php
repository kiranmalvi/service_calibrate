<?
include_once('include.php');
require_once('library/stripe_payment/lib/Stripe.php');

include_once($inc_class_path . 'user.class.php');
$userObj = new user();
include_once($inc_class_path . 'billing_info.class.php');
$billInfoObj = new billing_info();
include_once($inc_class_path . 'payment.class.php');
$payment = new payment();


//    echo 'First Date    = ' . date('Y-m-01') . '<br />';
//    echo 'Last Date     = ' . date('Y-m-t')  . '<br />';

$start=(date('Y-m-d 00:00:00'));
$end=(date('Y-m-d 23:59:59'));
$tstart = strtotime($start);
$tend = strtotime($end);
$start_prv_date = strtotime(date('Y-m-d 00:00:00', strtotime('-1 day', ($tstart))));
//echo "<br>";
$end_prv_date = strtotime(date('Y-m-d 23:59:59', strtotime('-1 day', ($tend))));
//exit;



$user=$userObj->payment_subscription($start_prv_date,$end_prv_date);


if(count($user)>0)
{
    //echo "<pre>"; print_r($user); echo "</pre>";
    Stripe::setApiKey($stripekey);

    foreach ($user AS $key => $value) {
            $amount=0;
            $uId = $value['iUserId'];
            $end_next_edate = '';
            if($value['ePlanType'] == 'annual')
            {
                $amount = ($value['fPlanAmout'] * 12 ) * $value['vComment'] ;
                $end_next_edate = strtotime(date('Y-m-d 23:59:59', strtotime('+1 year', ($tend))));

            }else
            {
                /*       month date logic     */
                $amount = $value['fPlanAmout'] * $value['vComment'] ;
                $end_next_edate = strtotime(date('Y-m-t 23:59:59', $tend));
            }

        $charge = Stripe_Charge::create(array(
                "amount"   => round($amount*100),
                "currency" => "usd",
                "customer" => $value['iCustomerId'])
        );

        //echo "<pre>"; print_r($charge); echo "</pre>";
        if(!isset($charge->error)){

            $userObj->update_user_info_subscription($uId,$amount,$end_next_edate);
            $billinfo = $billInfoObj->billingAddress($uId);
            //echo "<pre>"; print_r($billinfo); echo "</pre>";exit;
            $TRAN = $charge->status;
            $TRAN_ID = $charge->id;
            $today = strtotime(gmdate('Y-m-d H:i:s'));

            $payment->setiUserId($uId);
            $payment->setiPlanId($value['iPlanId']);
            $payment->setvTransactionId($TRAN_ID);
            $payment->setiDtAdded($today);
            $payment->setiDtExpired($end_next_edate);
            $payment->setvAddress($billinfo['vAddress']);
            $payment->setvCity($billinfo['vCity']);
            $payment->setVState($billinfo['VState']);
            $payment->setvZip($billinfo['vZip']);
            $payment->setvCountry($billinfo['vCountry']);
            $payment->setfAmount($amount);
            $payment->setiStores($value['vComment']);
            $payment->seteStatus('1');

            $iPaymentId=$payment->insert();
            echo "<br> <h3 style='color: green;'> ".$value['vEmail'] ." -> Subscription Successfully done !!</h3>";

        }
        else{
            $userObj->update_expiration_date($uId);
            echo "<br> <h3 style='color: red;'> Subscription Failed !!</h3>";
        }
    }

}


?>