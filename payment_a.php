<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 8/4/15
 * Time: 5:32 PM
 */



include('include.php');
require_once('library/stripe_payment/lib/Stripe.php');
require_once('library/class/user_settings.class.php');
require_once('library/class/user.class.php');
require_once('library/class/payment.class.php');
require_once('library/class/plan.class.php');
require_once('library/class/feature.class.php');
require_once('library/class/addtional_feature.class.php');
require_once('library/class/billing_info.class.php');

$addtional_feature = new addtional_feature();
$user_settings = new user_settings();
$userObj = new user();
$payment = new payment();
$planObj = new plan();
$featureObj = new feature();
$billingObj = new billing_info();

$userID = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$planID = $_REQUEST['SC_plan'];
$plantype = $_REQUEST['plantype'];

$priceplan = $planObj->select($planID);

//Get current Price of plan according to selected plan

if($plantype="annual")
{
    $planprice = $priceplan[0]['fDayPrice'];
}
else
{
    $planprice = $priceplan[0]['fMonthPrice'];
}

//url to redirect if transaction goes fail
if($planID=="1")
{
    $url="payment_retailers.php";
}
else if($planID=="2")
{
    $url="payment_distributor.php";
}
else
{
    $url="payment_manufacturer.php";
}

//$plan = $planObj->select($planID);
//$planprice = $planObj->getFMonthPrice();

$additional = $_REQUEST['additional_feature'];
$qr= $_REQUEST['qr'];

$fetaureprice = 0;
for ($i = 0; $i < count($additional); $i++) {
    $fid = $additional[$i];
    $featureObj->select_additional($fid);
    $price = $featureObj->getfPrice();
    $fetaureprice = $fetaureprice + $price;

}

$amount = $planprice + $fetaureprice;
$EX = explode('.', $amount);
//$NEW_AMOUNT = $EX[0] . '' . $EX[1];

$NEW_AMOUNT = $_REQUEST['am'];
$maxstore = $_REQUEST['maxstore'];
$expiredate = $_REQUEST['expiredate'];

Stripe::setApiKey($stripekey);

$expiredate = strtotime(gmdate('Y-m-t'));
$today = strtotime(gmdate('Y-m-d H:i:s'));
$checkpayment=$user_settings->checkCustomerId($userID);

//pr($_SESSION);
## Check Customer Id

//echo count($checkpayment);

if (count($checkpayment)!=0) {
     $customerID=$checkpayment[0]['iCustomerId'];
    ## Make Payment
    $charge = Stripe_Charge::create(
        array(
            "amount" => round($NEW_AMOUNT*100),
            "currency" => "usd",
            "customer" => $customerID
        )
    );
    $TRAN = $charge->status;
    
     if ($charge->status == 'succeeded') {
    $TRAN_ID = $charge->id;

    $payment->setiUserId($userID);
    $payment->setiPlanId($planID);
    $payment->setvTransactionId($TRAN_ID);
    $payment->setiDtAdded($today);
    $payment->setiDtExpired($expiredate);
    $payment->setvAddress($_REQUEST['address']);
    $payment->setvCity($_REQUEST['city']);
    $payment->setVState($_REQUEST['state']);
    $payment->setvZip($_REQUEST['country']);
    $payment->setvCountry($_REQUEST['zip']);
    $payment->setfAmount($NEW_AMOUNT);
    $payment->setiStores($maxstore);
    $payment->seteStatus('1');

    $iPaymentId=$payment->insert();



    $plan_insert="update  user_plan_info set  fPlanAmout=$planprice, ePlanType='$plantype' where iUserId=$userID";
    $plan_sql=$obj->sql_query($plan_insert);

    if($qr=="yes")
    {
        $price=8*$maxstore;
        $addtional_feature->setfAmount($price);
        $addtional_feature->setiUserId($userID);
        $addtional_feature->setiFeatureId($iPaymentId);
        $addtional_feature->setiDtAdded(strtotime(gmdate('Y-m-d H:i:s')));
        $addtional_feature->seteStatus("1");
        $addid=$addtional_feature->insert();
    }

    $userObj->setiPlanId($planID);
    $userObj->setvComment($maxstore);
    $userObj->seteActivePaidPlan("1");
    $userObj->setfAmountPaid($NEW_AMOUNT);
    $userObj->setiExpireTime($expiredate);
         $userObj->setePlanType("1");
    $userObj->update_plan($userID);
    
    $exe_update_parent = "Update user set iParentId='0' where iUserId='$userID'";
    $obj->sql_query($exe_update_parent);

    $billingObj->setvAddress($_REQUEST['address']);
    $billingObj->setvCity($_REQUEST['city']);
    $billingObj->setVState($_REQUEST['state']);
    $billingObj->setvCountry($_REQUEST['country']);
    $billingObj->setvZip($_REQUEST['zip']);
    $billingObj->setiDtUpdated($today);
    $billingObj->update_adrs($userID);

         $_SESSION['SC_LOGIN']['USER']['allowlogin']="yes";

    $_SESSION['frnt_payment'] = "Your transaction is Succesfully Done";
   // header("Location:admin/index.php?file=su-sudashboard&iId=$userID");
    header("Location:invoice.php?iId=$iPaymentId&adid=$addid");
    }
else
    {
       // echo "up";
      //  $url="Location:payment_retailers.php";
        $_SESSION['frnt_payment'] = $response->error->message;
           $_SESSION['frnt_payment_msg'] = $response->error->message;
        header("Location:$url");
        echo $response->error->message;
        exit;

    }
    } 
else {
    ## Create Token
    echo "create";
    $response = Stripe_Token::create(
        array(
            "card" => array(
                "number" => $_REQUEST['crnumber'],
                "exp_month" => $_REQUEST['expm'],
                "exp_year" => $_REQUEST['expy'],
                "cvc" => $_REQUEST['code']
            )
        )
    );
    $token = $response->id;

    if (!isset($response->error->message)) {

        $customerID = $customer->id;
        $user_settings->setICustomerId($customerID);
        $user_settings->update_customerID($userID);

        $token = $response->id;

        ## Create Customer
        $customer = Stripe_Customer::create(
            array(
                "card" => $token,
                "description" => $_SESSION['SC_LOGIN']['USER']['vEmail']
            )
        );

         $customerID = $customer->id;
        $user_settings->setICustomerId($customerID);
        $user_settings->update_customerID($userID);

        ## Make Payment
        $charge = Stripe_Charge::create(
            array(
                "amount" => round($NEW_AMOUNT*100),
                "currency" => "usd",
                "customer" => $customerID
            )
        );

        $TRAN = $charge->status;
        if ($charge->status == 'succeeded') {
  $TRAN_ID = $charge->id;


            if($qr=="yes")
            {
                $price=8*$maxstore;
                $addtional_feature->setfAmount($price);
                $addtional_feature->setiUserId($userID);
                $addtional_feature->setiFeatureId("11");
                $addtional_feature->setiDtAdded(strtotime(gmdate('Y-m-d H:i:s')));
                $addtional_feature->seteStatus("1");
                $addid=$addtional_feature->insert();
            }
            
             
            $userObj->setiPlanId($planID);
            $userObj->setvComment($maxstore);
            $userObj->seteActivePaidPlan("1");
            $userObj->setfAmountPaid($NEW_AMOUNT);
            $userObj->setiExpireTime($expiredate);
            $userObj->setePlanType("1");
            $userObj->update_plan($userID);
            
            $exe_update_parent = "Update user set iParentId='0' where iUserId='$userID'";
    $obj->sql_query($exe_update_parent);

            $payment->setiUserId($userID);
            $payment->setiPlanId($planID);
            $payment->setvTransactionId($TRAN_ID);
            $payment->setiDtAdded($today);
            $payment->setiDtExpired($expiredate);
            $payment->setvAddress($_REQUEST['address']);
            $payment->setvCity($_REQUEST['city']);
            $payment->setVState($_REQUEST['state']);
            $payment->setvZip($_REQUEST['zip']);
            $payment->setvCountry($_REQUEST['country']);
            $payment->seteStatus('1');
            $payment->setfAmount($NEW_AMOUNT);
            $payment->setiStores($maxstore);
            $iPaymentID=$payment->insert();


            $plan_insert="insert into user_plan_info (iUserId, fPlanAmout, ePlanType) VALUES ('$userID', '$planprice', '$plantype')";
            $plan_sql=$obj->insert($plan_insert);

            $billingObj->setiUserId($userID);
            $billingObj->setvAddress($_REQUEST['address']);
            $billingObj->setvCity($_REQUEST['city']);
            $billingObj->setVState($_REQUEST['state']);
            $billingObj->setvCountry($_REQUEST['country']);
            $billingObj->setvZip($_REQUEST['zip']);
            $billingObj->setiDtAdded($today);
            $billingObj->setiDtUpdated($today);
            $billingObj->seteStatus("1");
            $iBillingId=$billingObj->insert();

            $_SESSION['frnt_payment'] = "Your transaction is Succesfully Done";
              $_SESSION['SC_LOGIN']['USER']['allowlogin']="yes";
            header("Location:invoice.php?iId=$iPaymentID&adid=$addid");
        } else {
            echo $charge->error->message;
            $_SESSION['frnt_payment'] = "Some Things is wrong, Please try again";
            header("Location:admin/index.php?file=su-sudashboard&iId=$userID");
            echo " Some Things is wrong, Please try again";
            exit;
        }
    } else {
        $_SESSION['frnt_payment'] = $response->error->message;
           $_SESSION['frnt_payment_msg'] = $response->error->message;
         header("Location:$url");
        echo $response->error->message;
        exit;
    }
}
