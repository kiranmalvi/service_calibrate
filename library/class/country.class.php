<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    country
 * DATE:         06.04.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/country.class.php
 * TABLE:        country
 * DB:           mind_service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class country
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iCountryId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iCountryId;
    protected $_vName;
    protected $_iDtAdded;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iCountryId = null;
        $this->_vName = null;
        $this->_iDtAdded = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiCountryId()
    {
        return $this->_iCountryId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiCountryId($val)
    {
        $this->_iCountryId = $val;
    }

    public function getvName()
    {
        return $this->_vName;
    }

    public function setvName($val)
    {
        $this->_vName = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $WHERE = 'WHERE 1=1';
        if (!is_null($id)) {
            $WHERE .= ' AND iCountryId = ' . $id;
        }

        $sql = "SELECT * FROM country $WHERE ORDER BY vName Asc";
        $row = $this->_obj->select($sql);

        $this->_iCountryId = $row[0]['iCountryId'];
        $this->_vName = $row[0]['vName'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_eStatus = $row[0]['eStatus'];

        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM country WHERE iCountryId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iCountryId = ""; // clear key for autoincrement

        $sql = "INSERT INTO country ( vName,iDtAdded,eStatus ) VALUES ( '" . $this->_vName . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {
        $sql = " UPDATE country SET ";
        $sql .= ($this->_vName != null) ? ", vName = '" . $this->_vName . "'," : "";
        $sql .= ($this->_iDtAdded != null) ? ", iDtAdded = '" . $this->_iDtAdded . "'," : "";
        $sql .= ($this->_eStatus != null) ? ",eStatus = '" . $this->_eStatus . "' ," : "";
        $sql .= "WHERE iCountryId = $id ";

        $UPDATE = clean_sql($sql);
        $this->_obj->sql_query($UPDATE);
    }

    function check_country()
    {
        $sql = "SELECT count(iCountryId) as tot FROM country WHERE vName = '" . $this->_vName . "'";
        $row = $this->_obj->select($sql);
        $COUNT = $row[0]['tot'];

        if ($COUNT > 0) {
            echo 'Country all ready exists';
            exit;
        }
    }
}
