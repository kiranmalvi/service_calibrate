<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    plan
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/plan.class.php
 * TABLE:        plan
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class plan
{


    /**
     *   @desc Variable Declaration with default value
     */

    protected $iPlanId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iPlanId;
    protected $_vPlanName;
    protected $_iFromStore;
    protected $_iToStore;
    protected $_vDescription;
    protected $_fDayPrice;
    protected $_fMonthPrice;
    protected $_iStartDate;
    protected $_iEndDate;
    protected $_eUSerType;
    protected $_iDtAdded;
    protected $_eStatus;



    /**
     *   @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iPlanId = null;
        $this->_vPlanName = null;
        $this->_iFromStore = null;
        $this->_iToStore = null;
        $this->_vDescription = null;
        $this->_fDayPrice = null;
        $this->_fMonthPrice = null;
        $this->_iStartDate = null;
        $this->_iEndDate = null;
        $this->_eUSerType = null;
        $this->_iDtAdded = null;
        $this->_eStatus = null;
    }

    /**
     *   @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }



    /**
     *   @desc   GETTER METHODS
     */


    public function getiPlanId()
    {
        return $this->_iPlanId;
    }

    public function getvPlanName()
    {
        return $this->_vPlanName;
    }

    public function getiFromStore()
    {
        return $this->_iFromStore;
    }

    public function getiToStore()
    {
        return $this->_iToStore;
    }

    public function getvDescription()
    {
        return $this->_vDescription;
    }

    public function getfDayPrice()
    {
        return $this->_fDayPrice;
    }

    public function getfMonthPrice()
    {
        return $this->_fMonthPrice;
    }

    public function getiStartDate()
    {
        return $this->_iStartDate;
    }

    public function getiEndDate()
    {
        return $this->_iEndDate;
    }

    public function geteUSerType()
    {
        return $this->_eUSerType;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }


    /**
     *   @desc   SETTER METHODS
     */


    public function setiPlanId($val)
    {
        $this->_iPlanId =  $val;
    }

    public function setvPlanName($val)
    {
        $this->_vPlanName =  $val;
    }

    public function setiFromStore($val)
    {
        $this->_iFromStore =  $val;
    }

    public function setiToStore($val)
    {
        $this->_iToStore =  $val;
    }

    public function setvDescription($val)
    {
        $this->_vDescription =  $val;
    }

    public function setfDayPrice($val)
    {
        $this->_fDayPrice =  $val;
    }

    public function setfMonthPrice($val)
    {
        $this->_fMonthPrice =  $val;
    }

    public function setiStartDate($val)
    {
        $this->_iStartDate =  $val;
    }

    public function setiEndDate($val)
    {
        $this->_iEndDate =  $val;
    }

    public function seteUSerType($val)
    {
        $this->_eUSerType =  $val;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded =  $val;
    }

    public function seteStatus($val)
    {
        $this->_eStatus =  $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $WHERE = "WHERE 1=1";
        if (!is_null($id)) {
            $WHERE .= " AND iPlanId = $id";
        }

        $sql = "SELECT * FROM plan $WHERE";
        $row = $this->_obj->select($sql);

        $this->_iPlanId = $row[0]['iPlanId'];
        $this->_vPlanName = $row[0]['vPlanName'];
        $this->_iFromStore = $row[0]['iFromStore'];
        $this->_iToStore = $row[0]['iToStore'];
        $this->_vDescription = $row[0]['vDescription'];
        $this->_fMonthPrice = $row[0]['fMonthPrice'];
        $this->_fDayPrice = $row[0]['fDayPrice'];
        $this->_iStartDate = $row[0]['iStartDate'];
        $this->_iEndDate = $row[0]['iEndDate'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_eStatus = $row[0]['eStatus'];
        return $row;
    }

    function select_user_plan($user_type, $single_plan = null)
    {
        $WHERE = 'WHERE 1=1';
        if (!is_null($single_plan)) {
            $WHERE .= " AND (eUserType = $user_type or eUserType = $single_plan)";
        } else {
            $WHERE .= " AND eUserType = $user_type ";
        }
        $sql = "SELECT * FROM plan $WHERE";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function select_plan_list($user_type)
    {
        $sql = "SELECT * FROM plan WHERE eUserType IN (SELECT iUserTypeId  FROM user_type WHERE iUserTypeId = $user_type OR iParentId = $user_type) AND eStatus = '1'";
        $row = $this->_obj->select($sql);
        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM plan WHERE iPlanId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iPlanId = ""; // clear key for autoincrement

        $sql = "INSERT INTO plan ( vPlanName,iFromStore,iToStore,vDescription,fMonthPrice,fDayPrice,iStartDate,iEndDate,iDtAdded,eStatus ) VALUES ( '" . $this->_vPlanName . "','" . $this->_iFromStore . "','" . $this->_iToStore . "','" . $this->_vDescription . "','" . $this->_fMonthPrice . "','" . $this->_fDayPrice . "','" . $this->_iStartDate . "','" . $this->_iEndDate . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "' )";
        $result =  $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {
        $sql = " UPDATE plan SET ";
        $sql .= ($this->_vPlanName != null) ? ", vPlanName = '" . $this->_vPlanName . "'," : "";
        $sql .= ($this->_iFromStore != null) ? ", iFromStore = '" . $this->_iFromStore . "'," : "";
        $sql .= ($this->_iToStore != null) ? ", iToStore = '" . $this->_iToStore . "'," : "";
        $sql .= ($this->_vDescription != null) ? ", vDescription = '" . $this->_vDescription . "'," : "";
        $sql .= ($this->_fMonthPrice != null) ? ", fMonthPrice = '" . $this->_fMonthPrice . "'," : "";            $sql .= ($this->_fDayPrice != null) ? ", fDayPrice = '" . $this->_fDayPrice . "'," : "";
        $sql .= ($this->_iStartDate != null) ? ", iStartDate = '" . $this->_iStartDate . "'," : "";
        $sql .= ($this->_iEndDate != null) ? ", iEndDate = '" . $this->_iEndDate . "'," : "";
        $sql .= ($this->_eUSerType != null) ? ", eUSerType = '" . $this->_eUSerType . "'," : "";
        $sql .= ($this->_iDtAdded != null) ? ", iDtAdded = '" . $this->_iDtAdded . "'," : "";
        $sql .= ($this->_eStatus != null) ? ",eStatus = '" . $this->_eStatus . "' ," : "";
        $sql .= "WHERE iPlanId = $id ";

        $UPDATE = clean_sql($sql);
        $this->_obj->sql_query($UPDATE);
    }

    /* For  All Plan Listing */
    function selectAllPlan()
    {
        $sql = "SELECT * FROM plan WHERE eStatus='1'";
        $row = $this->_obj->select($sql);
        return $row;
    }



}
