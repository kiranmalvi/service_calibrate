<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    page
 * DATE:         31.07.2014
 * CLASS FILE:   /var/www/electrorentapp/library/class/class-making/generated_classes/page.class.php
 * TABLE:        page
 * DB:           electrorentapp
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class page
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iPageId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iPageId;
    protected $_vPageName;
    protected $_vPageTitle;
    protected $_tPageText;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iPageId = null;
        $this->_vPageName = null;
        $this->_vPageTitle = null;
        $this->_tPageText = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiPageId()
    {
        return $this->_iPageId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiPageId($val)
    {
        $this->_iPageId = $val;
    }

    public function getvPageName()
    {
        return $this->_vPageName;
    }

    public function setvPageName($val)
    {
        $this->_vPageName = $val;
    }

    public function getvPageTitle()
    {
        return $this->_vPageTitle;
    }

    public function setvPageTitle($val)
    {
        $this->_vPageTitle = $val;
    }

    public function gettPageText()
    {
        return $this->_tPageText;
    }

    public function settPageText($val)
    {
        $this->_tPageText = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }

    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM page WHERE iPageId = $id";
        $row = $this->_obj->select($sql);

        $this->_iPageId = $row[0]['iPageId'];
        $this->_vPageName = $row[0]['vPageName'];
        $this->_vPageTitle = $row[0]['vPageTitle'];
        $this->_tPageText = $row[0]['tPageText'];
        $this->_eStatus = $row[0]['eStatus'];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM page WHERE iPageId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iPageId = ""; // clear key for autoincrement

        $sql = "INSERT INTO page ( vPageName,vPageTitle,tPageText,eStatus) VALUES ( '" . $this->_vPageName . "','" . $this->_vPageTitle . "','" . $this->_tPageText . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {
        $sql = " UPDATE page SET  vPageName = '" . $this->_vPageName . "' , vPageTitle = '" . $this->_vPageTitle . "' , tPageText = '" . $this->_tPageText . "' , eStatus = '" . $this->_eStatus . "' WHERE iPageId = $id ";
        $result = $this->_obj->sql_query($sql);
        return $result;
    }

    function get_page_desc($pagename = "")
    {
        $ssql = "";
        if ($pagename != "") {
            $ssql .= " AND vPageName = '" . $pagename . "'";
        }
        $sql = "SELECT vPageName,vPageTitle,tPageText FROM page WHERE eStatus='active' $ssql";
        $row = $this->_obj->select($sql);

        return $row;
    }

}
