<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    addtional_feature
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/addtional_feature.class.php
 * TABLE:        addtional_feature
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class addtional_feature
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iAdditionalfeatureID;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iAdditionalfeatureID;
    protected $_iUserId;
    protected $_iFeatureId;
    protected $_fAmount;
    protected $_iDtAdded;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iAdditionalfeatureID = null;
        $this->_iUserId = null;
        $this->_iFeatureId = null;
        $this->_fAmount = null;
        $this->_iDtAdded = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiAdditionalfeatureID()
    {
        return $this->_iAdditionalfeatureID;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiAdditionalfeatureID($val)
    {
        $this->_iAdditionalfeatureID = $val;
    }

    public function getiUserId()
    {
        return $this->_iUserId;
    }

    public function setiUserId($val)
    {
        $this->_iUserId = $val;
    }

    public function getiFeatureId()
    {
        return $this->_iFeatureId;
    }

    public function setiFeatureId($val)
    {
        $this->_iFeatureId = $val;
    }

    public function getfAmount()
    {
        return $this->_fAmount;
    }

    public function setfAmount($val)
    {
        $this->_fAmount = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM addtional_feature WHERE iAdditionalfeatureID = $id";
        $row = $this->_obj->select($sql);

        $this->_iAdditionalfeatureID = $row[0]['iAdditionalfeatureID'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_iFeatureId = $row[0]['iFeatureId'];
        $this->_fAmount = $row[0]['fAmount'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_eStatus = $row[0]['eStatus'];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM addtional_feature WHERE iAdditionalfeatureID = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iAdditionalfeatureID = ""; // clear key for autoincrement

        $sql = "INSERT INTO addtional_feature ( iUserId,iFeatureId,fAmount,iDtAdded,eStatus ) VALUES ( '" . $this->_iUserId . "','" . $this->_iFeatureId . "','" . $this->_fAmount . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE addtional_feature SET  iUserId = '" . $this->_iUserId . "' , iFeatureId = '" . $this->_iFeatureId . "' , fAmount = '" . $this->_fAmount . "' , iDtAdded = '" . $this->_iDtAdded . "' , eStatus = '" . $this->_eStatus . "'  WHERE iAdditionalfeatureID = $id ";
        $this->_obj->sql_query($sql);

    }

    /*
     Get User have additional plan or not & if yes than addtional feature details
     */
    function Get_addfeatuer_by_user($id)
    {

        $sql = "select iFeatureId from addtional_feature where iUserId=$id";
        $result = $this->_obj->select($sql);
        return $result;

    }


}
