<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    city
 * DATE:         06.04.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/city.class.php
 * TABLE:        city
 * DB:           mind_service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class city
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iCityId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iCityId;
    protected $_iStateId;
    protected $_vName;
    protected $_iDtAdded;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iCityId = null;
        $this->_iStateId = null;
        $this->_vName = null;
        $this->_iDtAdded = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiCityId()
    {
        return $this->_iCityId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiCityId($val)
    {
        $this->_iCityId = $val;
    }

    public function getiStateId()
    {
        return $this->_iStateId;
    }

    public function setiStateId($val)
    {
        $this->_iStateId = $val;
    }

    public function getvName()
    {
        return $this->_vName;
    }

    public function setvName($val)
    {
        $this->_vName = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $WHERE = "WHERE 1=1";
        if (!is_null($id)) {
            $WHERE .= ' AND iCityId = ' . $id ;
        }

        $sql = "SELECT * FROM city $WHERE ORDER BY vName ASC ";
        $row = $this->_obj->select($sql);

        $this->_iCityId = $row[0]['iCityId'];
        $this->_iStateId = $row[0]['iStateId'];
        $this->_vName = $row[0]['vName'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_eStatus = $row[0]['eStatus'];
        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM city WHERE iCityId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iCityId = ""; // clear key for autoincrement

        $sql = "INSERT INTO city ( iStateId,vName,iDtAdded,eStatus ) VALUES ( '" . $this->_iStateId . "','" . $this->_vName . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {
        $sql = " UPDATE city SET ";
        $sql .= ($this->_iStateId != null) ? ", iStateId = '" . $this->_iStateId . "'," : "";
        $sql .= ($this->_vName != null) ? ", vName = '" . $this->_vName . "'," : "";
        $sql .= ($this->_iDtAdded != null) ? ", iDtAdded = '" . $this->_iDtAdded . "'," : "";
        $sql .= ($this->_eStatus != null) ? ",eStatus = '" . $this->_eStatus . "' ," : "";
        $sql .= "WHERE iCityId = $id ";

        $UPDATE = clean_sql($sql);
        $this->_obj->sql_query($UPDATE);
    }

    function select_with_state_country($id = null)
    {
        $WHERE = 'WHERE 1=1';
        if (!is_null($id)) {
            $WHERE .= ' AND iCityId = ' . $id;
        }
        $sql = "SELECT ct.*,st.vName as state,co.iCountryId,co.vName as country
                FROM city ct
                LEFT JOIN state st ON ct.iStateId = st.iStateId
                LEFT JOIN country co ON st.iCountryId = co.iCountryId $WHERE";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function select_on_state()
    {
        $sql = "SELECT iCityId,vName FROM city WHERE iStateId = " . $this->_iStateId;
        $row = $this->_obj->select($sql);
        return $row;
    }

    function select_city_on_state($id)
    {
        $sql = "SELECT iCityId,vName FROM city WHERE iStateId = " . $id;
        $row = $this->_obj->select($sql);
        return $row;
    }


}
