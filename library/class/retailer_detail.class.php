<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    retailer_detail
* DATE:         04.04.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/retailer_detail.class.php
* TABLE:        retailer_detail
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class retailer_detail
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iRetailerDetailId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iRetailerDetailId;  
	protected $_iUserId;  
	protected $_vRefference_Code;  
	protected $_vOriginal_code;  
	protected $_vQRImage;  
	protected $_iDtAdded;  
	protected $_eStatus;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iRetailerDetailId = null; 
		$this->_iUserId = null; 
		$this->_vRefference_Code = null; 
		$this->_vOriginal_code = null; 
		$this->_vQRImage = null; 
		$this->_iDtAdded = null; 
		$this->_eStatus = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiRetailerDetailId()
	{
		return $this->_iRetailerDetailId;
	}

    /**
     * @desc   SETTER METHODS
     */


    public function setiRetailerDetailId($val)
	{
        $this->_iRetailerDetailId = $val;
	}

    public function getiUserId()
	{
        return $this->_iUserId;
	}

    public function setiUserId($val)
	{
        $this->_iUserId = $val;
	}

    public function getvRefference_Code()
	{
        return $this->_vRefference_Code;
	}

    public function setvRefference_Code($val)
	{
        $this->_vRefference_Code = $val;
	}

    public function getvOriginal_code()
	{
        return $this->_vOriginal_code;
	}

    public function setvOriginal_code($val)
	{
        $this->_vOriginal_code = $val;
	}

    public function getvQRImage()
	{
        return $this->_vQRImage;
	}

    public function setvQRImage($val)
	{
        $this->_vQRImage = $val;
	}

    public function getiDtAdded()
	{
        return $this->_iDtAdded;
	}

    public function setiDtAdded($val)
	{
        $this->_iDtAdded = $val;
	}

    public function geteStatus()
	{
        return $this->_eStatus;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM retailer_detail WHERE iRetailerDetailId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iRetailerDetailId = $row[0]['iRetailerDetailId'];
		 $this->_iUserId = $row[0]['iUserId'];
		 $this->_vRefference_Code = $row[0]['vRefference_Code'];
		 $this->_vOriginal_code = $row[0]['vOriginal_code'];
		 $this->_vQRImage = $row[0]['vQRImage'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
		 $this->_eStatus = $row[0]['eStatus'];
        return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM retailer_detail WHERE iRetailerDetailId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iRetailerDetailId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO retailer_detail ( iUserId,vRefference_Code,vOriginal_code,vQRImage,iDtAdded,eStatus ) VALUES ( '".$this->_iUserId."','".$this->_vRefference_Code."','".$this->_vOriginal_code."','".$this->_vQRImage."','".$this->_iDtAdded."','".$this->_eStatus."' )";
        $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{
        $sql = " UPDATE retailer_detail SET ";
        $sql .= ($this->_iUserId != null) ? ", iUserId = '" . $this->_iUserId . "'," : "";
        $sql .= ($this->_vRefference_Code != null) ? ", vRefference_Code = '" . $this->_vRefference_Code . "'," : "";
        $sql .= ($this->_vOriginal_code != null) ? ", vOriginal_code = '" . $this->_vOriginal_code . "'," : "";
        $sql .= ($this->_vQRImage != null) ? ", vQRImage = '" . $this->_vQRImage . "', " : "";
        $sql .= ($this->_iDtAdded != null) ? ", iDtAdded = '" . $this->_iDtAdded . "'," : "";
        $sql .= ($this->_eStatus != null) ? ", eStatus = '" . $this->_eStatus . "'," : "";
        $sql .= "WHERE iRetailerDetailId = $id ";

        $UPDATE = clean_sql($sql);

        $this->_obj->sql_query($UPDATE);
	}

  // Function for get retailer Qr Code By User Id
 // Admin Side
    function get_detail_by_retailer($id)
    {
        $sql =  "SELECT * FROM retailer_detail WHERE iUserId = $id";
        $row =  $this->_obj->select($sql);
        return $row;
    }

    // Function for checking if qrcode exist or not
    // API
    function func_check_qrcode_exist($code)
    {
       $vRefference_Code = "www.servicecallibrate.com/sc".$code;
        $sql = "SELECT count(*) as total,iUserId FROM retailer_detail WHERE vOriginal_code = '$code' AND vRefference_Code='$vRefference_Code'";
       
        $row = $this->_obj->select($sql);
        return $row;

    }
function update_qr($id)
{
    $sql = " UPDATE retailer_detail SET  vRefference_Code = '".$this->_vRefference_Code."' ,  vQRImage = '".$this->_vQRImage."' , vOriginal_code = '".$this->_vOriginal_code."'   WHERE iUserId = $id ";
    $this->_obj->sql_query($sql);
}


    function qr_code_detail_with_user($code)
    {
        $sql = "SELECT u.vStoreUniqueId as account_name,vFirstName as fname,vLastName as lname, r.* FROM retailer_detail as r left join user u on r.iUserId=u.iUserId WHERE r.iUserId='$code'";
        $row = $this->_obj->select($sql);
        return $row;

    }



}
