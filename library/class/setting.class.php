<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    setting
 * DATE:         18.09.2014
 * CLASS FILE:   /var/www/sellit/library/class/class-making/generated_classes/setting.class.php
 * TABLE:        setting
 * DB:           sellit
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class setting
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $vName;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_vName;
    protected $_vDesc;
    protected $_vValue;
    protected $_iOrderBy;
    protected $_eConfigType;
    protected $_eDisplayType;
    protected $_eSource;
    protected $_vSourceValue;
    protected $_eSelectType;
    protected $_vDefValue;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_vName = null;
        $this->_vDesc = null;
        $this->_vValue = null;
        $this->_iOrderBy = null;
        $this->_eConfigType = null;
        $this->_eDisplayType = null;
        $this->_eSource = null;
        $this->_vSourceValue = null;
        $this->_eSelectType = null;
        $this->_vDefValue = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getvName()
    {
        return $this->_vName;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setvName($val)
    {
        $this->_vName = $val;
    }

    public function getvDesc()
    {
        return $this->_vDesc;
    }

    public function setvDesc($val)
    {
        $this->_vDesc = $val;
    }

    public function getvValue()
    {
        return $this->_vValue;
    }

    public function setvValue($val)
    {
        $this->_vValue = $val;
    }

    public function getiOrderBy()
    {
        return $this->_iOrderBy;
    }

    public function setiOrderBy($val)
    {
        $this->_iOrderBy = $val;
    }

    public function geteConfigType()
    {
        return $this->_eConfigType;
    }

    public function seteConfigType($val)
    {
        $this->_eConfigType = $val;
    }

    public function geteDisplayType()
    {
        return $this->_eDisplayType;
    }

    public function seteDisplayType($val)
    {
        $this->_eDisplayType = $val;
    }

    public function geteSource()
    {
        return $this->_eSource;
    }

    public function seteSource($val)
    {
        $this->_eSource = $val;
    }

    public function getvSourceValue()
    {
        return $this->_vSourceValue;
    }

    public function setvSourceValue($val)
    {
        $this->_vSourceValue = $val;
    }

    public function geteSelectType()
    {
        return $this->_eSelectType;
    }

    public function seteSelectType($val)
    {
        $this->_eSelectType = $val;
    }

    public function getvDefValue()
    {
        return $this->_vDefValue;
    }

    public function setvDefValue($val)
    {
        $this->_vDefValue = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM setting WHERE vName = '{$id}'";
        $row = $this->_obj->select($sql);

        $this->_vName = $row[0]['vName'];
        $this->_vDesc = $row[0]['vDesc'];
        $this->_vValue = $row[0]['vValue'];
        $this->_iOrderBy = $row[0]['iOrderBy'];
        $this->_eConfigType = $row[0]['eConfigType'];
        $this->_eDisplayType = $row[0]['eDisplayType'];
        $this->_eSource = $row[0]['eSource'];
        $this->_vSourceValue = $row[0]['vSourceValue'];
        $this->_eSelectType = $row[0]['eSelectType'];
        $this->_vDefValue = $row[0]['vDefValue'];
        $this->_eStatus = $row[0]['eStatus'];

        return $row[0];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM setting WHERE vName = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->vName = ""; // clear key for autoincrement

        $sql = "INSERT INTO setting ( vDesc,vValue,iOrderBy,eConfigType,eDisplayType,eSource,vSourceValue,eSelectType,vDefValue,eStatus ) VALUES ( '" . $this->_vDesc . "','" . $this->_vValue . "','" . $this->_iOrderBy . "','" . $this->_eConfigType . "','" . $this->_eDisplayType . "','" . $this->_eSource . "','" . $this->_vSourceValue . "','" . $this->_eSelectType . "','" . $this->_vDefValue . "','" . $this->_eStatus . "' )";
        $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE setting SET  vDesc = '" . $this->_vDesc . "' , vValue = '" . $this->_vValue . "' , iOrderBy = '" . $this->_iOrderBy . "' , eConfigType = '" . $this->_eConfigType . "' , eDisplayType = '" . $this->_eDisplayType . "' , eSource = '" . $this->_eSource . "' , vSourceValue = '" . $this->_vSourceValue . "' , eSelectType = '" . $this->_eSelectType . "' , vDefValue = '" . $this->_vDefValue . "' , eStatus = '" . $this->_eStatus . "'  WHERE vName = $id ";
        $this->_obj->sql_query($sql);

    }


}
