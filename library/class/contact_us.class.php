<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    contact_us
* DATE:         01.07.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/contact_us.class.php
* TABLE:        contact_us
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class contact_us
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iContactusId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iContactusId;  
	protected $_vFirstName;  
	protected $_vLastName;  
	protected $_vEmail;  
	protected $_vPhone;  
	protected $_vMessage;  
	protected $_iDtAdded;  
	protected $_iDtUpdated;  
	protected $_eStatus;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iContactusId = null; 
		$this->_vFirstName = null; 
		$this->_vLastName = null; 
		$this->_vEmail = null; 
		$this->_vPhone = null; 
		$this->_vMessage = null; 
		$this->_iDtAdded = null; 
		$this->_iDtUpdated = null; 
		$this->_eStatus = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiContactusId()
	{
		return $this->_iContactusId;
	}

	public function getvFirstName()
	{
		return $this->_vFirstName;
	}

	public function getvLastName()
	{
		return $this->_vLastName;
	}

	public function getvEmail()
	{
		return $this->_vEmail;
	}

	public function getvPhone()
	{
		return $this->_vPhone;
	}

	public function getvMessage()
	{
		return $this->_vMessage;
	}

	public function getiDtAdded()
	{
		return $this->_iDtAdded;
	}

	public function getiDtUpdated()
	{
		return $this->_iDtUpdated;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiContactusId($val)
	{
		 $this->_iContactusId =  $val;
	}

	public function setvFirstName($val)
	{
		 $this->_vFirstName =  $val;
	}

	public function setvLastName($val)
	{
		 $this->_vLastName =  $val;
	}

	public function setvEmail($val)
	{
		 $this->_vEmail =  $val;
	}

	public function setvPhone($val)
	{
		 $this->_vPhone =  $val;
	}

	public function setvMessage($val)
	{
		 $this->_vMessage =  $val;
	}

	public function setiDtAdded($val)
	{
		 $this->_iDtAdded =  $val;
	}

	public function setiDtUpdated($val)
	{
		 $this->_iDtUpdated =  $val;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM contact_us WHERE iContactusId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iContactusId = $row[0]['iContactusId'];
		 $this->_vFirstName = $row[0]['vFirstName'];
		 $this->_vLastName = $row[0]['vLastName'];
		 $this->_vEmail = $row[0]['vEmail'];
		 $this->_vPhone = $row[0]['vPhone'];
		 $this->_vMessage = $row[0]['vMessage'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
		 $this->_iDtUpdated = $row[0]['iDtUpdated'];
		 $this->_eStatus = $row[0]['eStatus'];
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM contact_us WHERE iContactusId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iContactusId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO contact_us ( vFirstName,vLastName,vEmail,vPhone,vMessage,iDtAdded,iDtUpdated,eStatus ) VALUES ( '".$this->_vFirstName."','".$this->_vLastName."','".$this->_vEmail."','".$this->_vPhone."','".$this->_vMessage."','".$this->_iDtAdded."','".$this->_iDtUpdated."','".$this->_eStatus."' )";
        $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE contact_us SET  vFirstName = '".$this->_vFirstName."' , vLastName = '".$this->_vLastName."' , vEmail = '".$this->_vEmail."' , vPhone = '".$this->_vPhone."' , vMessage = '".$this->_vMessage."' , iDtAdded = '".$this->_iDtAdded."' , iDtUpdated = '".$this->_iDtUpdated."' , eStatus = '".$this->_eStatus."'  WHERE iContactusId = $id ";
		 $this->_obj->sql_query($sql);

	}

    function check_exist_email($email)
    {
        $SQL    =   "SELECT count(*) as tot FROM contact_us WHERE vEmail = '$email' ";
        $row    =   $this->_obj->select($SQL);

        if($row[0]['tot'] == 0)
        {
            return 0;
        }
        else{
            return 1;
        }

    }
    function get_exist_email_id($email)
    {
        $SQL    =   "SELECT iContactusId FROM contcat_us WHERE vEmail = '$email' ";
        $row    =   $this->_obj->select($SQL);

        return $row[0]['iContactusId'];
    }

    function update_date($id)
    {
        $sql = " UPDATE contact_us SET  iDtUpdated = '".$this->_iDtUpdated."'  WHERE iContactusId = $id ";
        $this->_obj->sql_query($sql);
    }
}
