<?php
class generalfunc
{


    private $_requestMode = 'edit';

    function setRequestMode($mode)
    {
        $this->_requestMode = $mode;
    }

    function gm_date()
    {
        return gmdate('Y-m-d H:i:s');
    }

    function gm_date_string()
    {
        return strtotime(gmdate('Y-m-d H:i:s'));
    }


    function getUserTime($timeZone)
    {

        date_default_timezone_set('UTC');
      $offset = $timeZone* 60 * 60;
        $todayDate = strtotime(date('Y-m-d H:i:s a', strtotime("+" . $offset . " sec")));
        return $todayDate;
    }



    function getGeneralVar()
    {
        global $obj;
        $wri_usql = "SELECT * FROM setting where eStatus='active'";
        $wri_ures = $obj->select($wri_usql);
        for ($i = 0, $ni = count($wri_ures); $i < $ni; $i++) {
            $vname = $wri_ures[$i]["vName"];
            $vvalue = $wri_ures[$i]["vValue"];
            global $$vname;
            $$vname = $vvalue;
        }
    }

    function DisplayTopInListAdd($TOP_HEADER, $BACK_LABEL = '', $BACK_LINK = '', $HEADING = '', $img = '')
    {
        global $admin_images;
        $html = '<div class="screenTitle">
			    <table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>';
        if ($img == "")
            $img = "on.gif";

        $html .= '<img src="' . $admin_images . '/' . $img . '">';

        $html .= '&nbsp;' . $TOP_HEADER . '</td>';
        if ($BACK_LABEL != '')
            $html .= '<td align="right"><a href="' . $BACK_LINK . '">' . $BACK_LABEL . '</a>&nbsp;&nbsp;</td>';
        $html .= '</tr>
				</table>
			</div>';
        return $html;
    }

    function getPostForm($POST_Arr, $msg = "", $action = "")
    {
        $str = '
		<html>
		<form name="frm1" action="' . $action . '" method=post>';
        foreach ($POST_Arr as $key => $value) {
            if (is_array($value)) {
                for ($i = 0; $i < count($value); $i++)
                    $str .= '<br><input type="Hidden" name="' . $key . '[]" value="' . stripslashes($value[$i]) . '">';
            } else {
                $str .= '<br><input type="Hidden" name="' . $key . '" value="' . stripslashes($value) . '">';
            }
        }
        $str .= '<input type="Hidden" name=var_msg value="' . $msg . '">
		</form>
		<script>
			document.frm1.submit();
		</script>
		</html>';
        echo $str;
        exit;
    }

    function switch_to($field1, $field2, $table, $iId, $module, $file)
    {
        global $obj;
        if ($table == 'customer') {
            $sql = "select concat(vFirstName,' ',vLastname) as " . $field2 . " , " . $field1 . " from " . $table . " where eStatus = 'Active'";
        } else {
            $sql = "select " . $field1 . "," . $field2 . " from " . $table . " where eStatus = 'Active'";
        }
        $db_sql = $obj->select($sql);
        $total = count($db_sql);
        $html = '';
        $html .= '<strong> Switch To: </strong>
                 <select name="' . $file . '" id="' . $module . '" onchange="change_url(this.value,this.name,this.id);">
                 <option value="">--Switch To--</option>';
        for ($s = 0; $s < $total; $s++) {
            $sele = '';
            if ($iId == $db_sql[$s][$field1]) {
                $sele = "selected";
            }

            $html .= '<option value="' . $db_sql[$s][$field1] . '" ' . $sele . '>' . $db_sql[$s][$field2] . '</option>';
        }
        $html .= '</select>';

        return $html;

    }

    function gen_DisplayPaging_Top($code, $showLetter = "Y", $showPaging = "Y", $width = "100%")
    {
        //echo $code;
        global $recmsg, $page_link, $script;
        if ($showPaging == "N") $style = "style=display:none";
        else                    $style = "style=display:''";
        if ($showLetter == "N") $style_alpha = "style=display:none";
        else                    $style_alpha = "style=display:''";
        echo '<table width="' . $width . '" cellpadding="0" cellspacing="0" border="0" >
			  <tr>';
        echo '<td width="25%" align="left"><div class="dataTables_info"><span ' . $style . ' class="disprecordmsg" >' . (($showPaging == "Y") ? $recmsg : "") . '&nbsp;</span></div></td>';
        echo '<td align="left" width="47%" nowrap><div class="dataTables_info"><span ' . $style_alpha . ' >' . (($showLetter == "Y") ? $this->getSearchByLetter($code) : "") . '</span></div></td>';
        echo '<td width="35%" align="right"><div class="dataTables_paginate paging_bootstrap"><ul class="pagination">' . (($showPaging == "Y") ? $page_link : "") . '</ul></div></td>';
        echo '
	   </tr>
	   </table>';
    }

    function getSearchByLetter($fieldname)
    {
        //global $AlphaSearchArr;
        //echo $fieldname;
        //if($AlphaSearchArr=='')
        //	$AlphaSearchArr = Array();
        //	global $QUERY_STRING;

        foreach ($_GET as $key => $value) {
            if ($key != "option" && $key != "keyword")
                $qs .= "&$key=$value";
        }
        $filename = basename($PHP_SELF);
        $link = "";
        for ($i = 65; $i <= 90; $i++) {

            if ($_GET[search_keyword] == chr($i)) {
                $link .= '<font class="" size="+1">' . chr($i) . '</font> ';
            } /*
            else if(!in_array(chr($i) , $AlphaSearchArr)){
                $link .= '<font class="">'.chr($i).'</font> ';
            }
            */
            else {
                $link .= '<a href="' . $filename . '?' . $qs . '&option=' . rawurldecode($fieldname) . '&search_keyword=' . chr($i) . '&searchmode=search&search=true&start=1" title="Search with ' . chr($i) . '">' . chr($i) . '</a> ';
            }

        }
        //$link .= '<a class="bluetext" href="'.$filename.'?'.$qs.'&option='.$fieldname.'&keyword='.chr($i).'&search=truestart=1">'.chr($i).'</a> ';
        if (isset($_GET[search_keyword]) && $_GET[search_keyword] == "")
            $link .= '<font  size="+1">' . ALL . '</font>';
        else
            $link .= '<a  href="' . $filename . '?' . $qs . '&option=' . $fieldname . '&search_keyword=' . '" title="Show All">' . ALL . '</a>';

        return $link;
    }

    function gen_DisplayPaging_Bottom($code, $showLetter = "Y", $showPaging = "Y", $width = "100%")
    {
        global $recmsg, $page_link, $script;
        if ($showPaging == "N") $style = "style=display:none";
        else                    $style = "style=display:''";
        if ($showLetter == "N") $style_alpha = "style=display:none";
        else                    $style_alpha = "style=display:''";
        echo '<table width="' . $width . '" cellpadding="0" cellspacing="0" border="0"  >
		    <tr style="">';
        echo '<td width="25%" align="left"><div class="dataTables_info"><span ' . $style . ' class="disprecordmsg" >' . (($showPaging == "Y") ? $recmsg : "") . '&nbsp;</span></div></td>';
        echo '<td align="left" width="47%" nowrap><div class="dataTables_info"><span ' . $style_alpha . ' >' . (($showLetter == "Y") ? $this->getSearchByLetter($code) : "") . '</span></div></td>';
        echo '<td width="33%" align="right"><div class="dataTables_paginate paging_bootstrap"><ul class="pagination">' . (($showPaging == "Y") ? $page_link : "") . '&nbsp;</ul></div></td>';
        echo '</tr></table>';
    }

    function getTotalRecordCombo($file, $extraStr, $rec_limit, $producttype = '', $all = '')
    {
        $combo = '';
        $combo .= '<select name="TotalRecords" id="TotalRecords" class="form-control"  style="z-index:10;" onchange="return checkRecordLimit(\'' . $file . '\', \'' . $extraStr . '\', \'' . $rec_limit . '\',\'' . $producttype . '\');">';

        if ($all != '')
            $combo .= "<option value='" . $all . "'>All</option>";
        /*
                $combo .= "<option value=''>-Select Limit-</option>";
        */
        for ($i = 0; $i < 10; $i++) {
            $val = ($i + 1) * 10;
            if ($val == $rec_limit)
                $sel = "selected";
            else
                $sel = '';

            $combo .= "<option value='" . $val . "' " . $sel . ">" . $val . "</option>";
        }
#			if($all != '')
#			{
#				$combo .= "<option value='' ".$select." >All</option>";
#			}
        $combo .= "</select>";
        return $combo;
    }

    function admin_logInOut_entry($iId = '', $Type = '')
    {
        global $obj;
        if ($_SESSION["sess_esrsa_admin_iLoginLogoutHistoryId"] != '') {
            $sql = "update  loginlogouthistory set dtLogoutDate = sysdate() where iLoginLogoutHistoryId='" . $_SESSION["sess_esrsa_admin_iLoginLogoutHistoryId"] . "'";
            $db_log_id = $obj->sql_query($sql);
        } else {
            $sql = "insert into loginlogouthistory (iAdminCustomerId,eLoginType,vIPAddress,dtLoginDate) values ('" . $iId . "','" . $Type . "','" . $_SERVER['REMOTE_ADDR'] . "',sysdate())";
            $db_log_id = $obj->insert($sql);
            $_SESSION["sess_esrsa_admin_iLoginLogoutHistoryId"] = $db_log_id;
        }

        if ($_SESSION["sess_esrsa_admin_iAdminLoginLogId"] != '') {
            $sql = "update  adminloginlog set dtAdminLogoutDate = sysdate() where iAdminLoginLogId='" . $_SESSION["sess_esrsa_admin_iAdminLoginLogId"] . "'";
            $db_log_id = $obj->sql_query($sql);
        } else {
            $sql = "insert into adminloginlog (iAdminId,dtAdminLoginDate,vIPadress) values ('" . $iId . "',sysdate(),'" . $_SERVER['REMOTE_ADDR'] . "')";
            $db_log_id = $obj->insert($sql);
            $_SESSION["sess_esrsa_admin_iAdminLoginLogId"] = $db_log_id;
        }
    }

    function curPageURL($iId = '')
    {
        global $obj;
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        $sql = "select max(iLoginLogoutHistoryId) as iLoginLogoutHistoryId, max(dtLoginDate) as dtLoginDate
			from loginlogouthistory where iAdminCustomerId = '$iId' and eLoginType = 'Admin'";
        $db_login = $obj->select($sql);

        $sql = "insert into adminsideurlsurf (iAdminLoginLogId,iAdminId,vUrl,dtAdded,vIPadress) values ('" . $db_login[0]['iLoginLogoutHistoryId'] . "', '" . $iId . "','" . $pageURL . "',sysdate(),'" . $_SERVER['REMOTE_ADDR'] . "')";
        $db_log_id = $obj->insert($sql);
        $_SESSION["sess_aps_admin_iAdminsideURLSurfId"] = $db_log_id;

        return $pageURL;
    }

    /*
        $selname = To give name of the dropdown.
        $dropdownwidth = To give width to dropdown.
        $where_clause = If we want any specific data to fetch from database; exclude 'where' keyword.

    */

    function func_generate_country_dropdown($countryId, $countryName, $where_clause = '', $order_by = '', $selectedVal = '', $width = 150, $title = '', $name)
    {

        Global $obj;
        $countrydropdown = "";

        if ($where_clause != "") {
            $where_clause = " WHERE $where_clause ";
        }

        if ($order_by != "") {
            $order_by = " ORDER BY $order_by ";
        }

        $sql_query_country = "SELECT $countryId,$countryName FROM country $where_clause $order_by";
        $db_select_country = $obj->select($sql_query_country);
        #print_r($db_select_country);
        $countrydropdown .= "<select name=\"$name\" id=\"$countryId\" style=\"width:$width\" >";
        if ($title != "")
            $countrydropdown .= "<option value='\"$countryId\"' selected>" . $title . "</option>";

        for ($i = 0; $i < count($db_select_country); $i++) {
            $countryid = $db_select_country[$i][$countryId];
            $countryname = $db_select_country[$i][$countryName];


            $selected = "";

            if ($countryid == $selectedVal)
                $selected = " selected ";

            $countrydropdown .= "<option  name=\"$name\" value=\"$countryid\" " . $selected . ">" . $countryname . "</option>";
        }
        $countrydropdown .= "</select>";

//		echo $groupdropdown;exit;

        return $countrydropdown;
    }

    function func_generate_state_dropdown($stateId, $stateName, $where_clause = '', $order_by = '', $selectedVal = '', $width = 150, $title = '', $name)
    {

        Global $obj;
        $statedropdown = "";

        if ($where_clause != "") {
            $where_clause = " WHERE $where_clause ";
        }

        if ($order_by != "") {
            $order_by = " ORDER BY $order_by ";
        }
        $sql_query_state = "SELECT $stateId,$stateName FROM state $where_clause $order_by";
        $db_select_state = $obj->select($sql_query_state);
        #print_r($db_select_state);
        $tot = count($db_select_state);
        $statedropdown .= "<select name=\"$name\" id=\"$name\" style=\"width:$width\" >";
        if ($title != "")
            $statedropdown .= "<option >" . $title . "</option>";

        for ($i = 0; $i < $tot; $i++) {
            $stateid = $db_select_state[$i][$stateId];
            $statename = $db_select_state[$i][$stateName];

            $selected = "";

            if ($stateid == $selectedVal)
                $selected = " selected ";

            $statedropdown .= "<option name=\"$name\" value=\"$stateid\" " . $selected . ">" . $statename . "</option>";

        }
        $statedropdown .= "</select>";

//		echo $groupdropdown;exit;

        return $statedropdown;
    }


    function func_generate_dynamic_dropdown($CDId, $CDName, $where_clause = '', $order_by = '', $selectedVal = '', $width = '150px;', $title = '', $name, $table, $fun = '', $mul = '')
    {


        Global $obj;
        $dropdown = "";

        if ($where_clause != "") {
            $where_clause = " WHERE $where_clause ";
        }

        if ($order_by != "") {
            $order_by = " ORDER BY $order_by ";
        }
        if ($fun != '')
            $function = "onChange='" . $fun . "'";

        $sql_query = "SELECT $CDId,$CDName FROM $table $where_clause $order_by";
        $db_select = $obj->select($sql_query);
        #print_r($db_select);exit;
        $tot_CD = count($db_select);
        $dropdown .= "<select name=\"$name\" id=\"$CDId\" lang=\"*\" title=\"$title\" style=\"width:$width\" $mul $function >";
        if ($title != "")
            $dropdown .= "<option value=''>" . $title . "</option>";

        for ($i = 0; $i < $tot_CD; $i++) {
            $Id = $db_select[$i][$CDId];
            $Name = $db_select[$i][$CDName];

            $selected = "";

            if ($Id == $selectedVal)
                $selected = " selected ";

            $dropdown .= "<option value=\"$Id\" " . $selected . ">" . $Name . "</option>";
        }
        $dropdown .= "</select>";

        // $dropdown;exit;

        return $dropdown;
    }

    function gendb_dynamicDropeDown($tableName, $fieldId, $fieldName, $extVal = '', $where_clause = '', $order_by = '', $selectedVal = '', $select_name = '', $width = 150, $size = '', $title = '', $fun = '', $extraval = '', $mul = '')
    {

        Global $obj;
        $groupdropdown = "";

        if ($select_name == '')
            $select_name = $fieldId;

        if ($where_clause != "")
            $where_clause = " WHERE $where_clause ";

        if ($extVal != '')
            $ssql = "$fieldId,$fieldName,$extVal as extravalue";
        else
            $ssql = "$fieldId,$fieldName";

        if ($order_by != "")
            $order_by = " ORDER BY $order_by ";
        $sql_query = "SELECT $ssql FROM $tableName $where_clause $order_by";
        $db_select_rs = $obj->select($sql_query);
        if ($mul != '') {
            $arr = "[]";
        }
        if ($size != '')
            $size = "size=$size";

        if ($fun != '')
            $function = "onChange='" . $fun . "'";

        echo $groupdropdown .= "<select name=\"$select_name$arr\" id='" . $select_name . "'  $size $extraval style=\"width:$width\" $function $mul>";
        if ($title != "")
            $groupdropdown .= "<option value='' selected>" . $title . "</option>";

        for ($i = 0; $i < count($db_select_rs); $i++) {
            $cid = $db_select_rs[$i][$fieldId];
            $cname = $db_select_rs[$i][$fieldName];
            if ($extVal != '') {
                echo $extname = $db_select_rs[$i][extravalue];
            }
            if ($extVal != "") {
                $vData = "$cname  $extname ";
            } else {
                $vData = "$cname";
            }
            $selected = "";
            if ($mul == 'multiple') {
                if (is_array($selectedVal) && in_array($cid, $selectedVal)) {
                    $selected = " selected ";
                }
            } else {
                if ($cid == $selectedVal)
                    $selected = " selected ";
            }
            $groupdropdown .= "<option value=\"$cid\" " . $selected . ">" . $vData . "</option>";
        }
        $groupdropdown .= "</select>";

//		echo $groupdropdown;exit;

        return $groupdropdown;
    }


    function date_only($date)
    {
        $date1 = date('d-m-Y', strtotime($date));
        return $date1;
    }

    function YMDdate_format($date)
    {
        $date1 = date('Y-m-d', strtotime($date));
        return $date1;
    }

    function Monthnamedate_format($date)
    {
        $date1 = date('d-M-Y', strtotime($date));
        return $date1;
    }

    function YMDdate_format_week($date)
    {
        //$date1 = date('Y-m-d', strtotime($date));
        $date = date("Y-m-d", strtotime('+7 day' . $date));
        return $date;
    }

    function YMDdate_format_fortnight($date)
    {
        //$date1 = date('Y-m-d', strtotime($date));
        $date = date("Y-m-d", strtotime('+15 day' . $date));
        return $date;
    }

    function YMDdate_format_month($date)
    {
        //$date1 = date('Y-m-d', strtotime($date));
        $date = date("Y-m-d", strtotime('+30 day' . $date));
        return $date;
    }

    function YMDdate_format_days($date, $days)
    {
        //$date1 = date('Y-m-d', strtotime($date));
        $date = date("Y-m-d", strtotime('+' . $days . ' day' . $date));
        return $date;
    }


    function MDYdate_format($date)
    {
        $date1 = date('m/d/Y', strtotime($date));
        return $date1;
        /*$bdate 	= 	explode("-",$date);
        $returndate	= 	$bdate[1]."/".$bdate[2]."/".$bdate[0];*/

    }

    function Date_Time_Format($text)
    {
        if ($text == "" || $text == "0000-00-00 00:00:00")
            return "---";
        else
            return date('j M, Y [g:i A]', strtotime($text));
    }

    function Date_Time_FormatWithoutTime($text)
    {
        if ($text == "" || $text == "0000-00-00 00:00:00")
            return "---";
        else
            return date('j F, Y', strtotime($text));
    }

    function gmt_Date_Time_FormatWithoutTime($text)
    {
        if ($text == "" || $text == "0000-00-00 00:00:00")
            return "---";
        else
            return date('j F, Y', ($text));
    }

    function gmt_tme_Date_Time_FormatWithoutTime($text)
    {
        if ($text == "" || $text == "0000-00-00 00:00:00")
            return "---";
        else
            return date('j F, Y h:i:s', ($text));
    }

    function gmt_Date_Time_FormatWithTime($text)
    {
        if ($text == "" || $text == "0000-00-00 00:00:00")
            return "---";
        else
            return date('j F, Y H:i:s A', ($text));
    }

    function MDYdate_formatewithAMPM($date)
    {
        $date1 = date('m/d/Y h:i A', strtotime($date));
        return $date1;
    }

    function MDYdate_formatewithoutAMPM($date)
    {
        $date1 = date('m/d/Y h:i:s ', strtotime($date));
        return $date1;
    }

    function YMDdate_formatewithoutAMPM($date)
    {
        $date1 = date('Y-m-d h:i:s ', strtotime($date));
        #echo $date1;exit;
        return $date1;
    }

    function formatefortime($date)
    {
        $time = date('H:i:s ', strtotime($date));
        #echo $date1;exit;
        return $time;
    }

    function replace_str_with_all($str)
    {
        $rs_catname = trim($str);
        $rs_catname = str_replace("&", "and", $rs_catname);
        $rs_catname = str_replace("/", "-", $rs_catname);
        $rs_catname = str_replace(" ", "-", $rs_catname);
        $rs_catname = str_replace("---", "-", $rs_catname);
        $rs_catname = str_replace("--", "-", $rs_catname);
        $rs_catname = strtolower($rs_catname);

        return $rs_catname;
    }

    function tow_decimal_position($number, $decimal = "2", $delimeter = ".")
    {
        $english_format_number = number_format($number, $decimal, $delimeter, '');
        return $english_format_number;
    }


    // get the start date & end date of any week
    function func_get_start_end_date_of_week($ParamStartEnd = "CurrentWeek")
    {

        //Get current week and year
        if ($ParamStartEnd == "CurrentWeek") {
            $wk_num = date('W');
        } elseif ($ParamStartEnd == "LastWeek") {
            $wk_num = date('W') - 1;
        }

        $yr = date('Y');

        $first = 0;
        $format = 'Y-m-d';

        $wk_ts = strtotime('+' . $wk_num . ' weeks', strtotime($yr . '0101'));
        $mon_ts = strtotime('-' . date('w', $wk_ts) + $first . ' days', $wk_ts);

        $sStartDate = date($format, $mon_ts);

        //Get the ending date of the same week
        $sEndDate = date($format, strtotime('+6 days', strtotime($sStartDate)));

        return array('start' => $sStartDate,
            'end' => $sEndDate);
    }

    function replace_str_with_space($str)
    {
        $rs_catname = trim($str);
        $rs_catname = str_replace("&", "and", $rs_catname);
        $rs_catname = str_replace(" ", "-", $rs_catname);
        $rs_catname = str_replace("---", "-", $rs_catname);
        $rs_catname = str_replace("--", "-", $rs_catname);
        $rs_catname = str_replace("-", " ", $rs_catname);
        $rs_catname = str_replace("+", " ", $rs_catname);
        $rs_catname = preg_replace('/\s+/', ' ', $rs_catname);
        $rs_catname = strtolower($rs_catname);

        return $rs_catname;
    }

    function func_get_start_end_date_of_month($ParamStartEnd = "CurrentMonth")
    {

        if ($ParamStartEnd == "CurrentMonth") {
            $mon = date('m');
            $yr = date('Y');
            $sStartDate = date("m/d/Y", strtotime($mon . "/01/" . $yr));
            $number_of_days = date('t', $sStartDate);
            $sEndDate = $mon . "/" . $number_of_days . "/" . $yr;
        } else if ($ParamStartEnd == "LastMonth") {
            $mon = date('m') - 1;
            $yr = date('Y');
            $sStartDate = date("m/d/Y", strtotime($mon . "/01/" . $yr));
            $number_of_days = date('t', $sStartDate);
            $sEndDate = $mon . "/" . $number_of_days . "/" . $yr;
        }


        //Get the ending date of the same week
        //$sEndDate   = date($format, strtotime('+6 days', strtotime($sStartDate)));

        return array('start' => $sStartDate,
            'end' => $sEndDate);
    }

    // get the start date & end date of any month

    function func_generate_tab($section, $page_name, $link)
    {
        $Tab_String = "";
        $Tab_String .= '<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue">';
        $return_tab_arr = $this->func_admin_tab($section, $link);
        $tot_tab_count = count($return_tab_arr);

        for ($t = 0; $t < $tot_tab_count; $t++) {
            $active = "";
            if ($this->_requestMode != 'add') $href = $return_tab_arr[$t]['href']; else $href = '#';
            if ($this->_requestMode != 'add') $style = ''; else $style = 'cursor: not-allowed';
            if ($page_name == $return_tab_arr[$t]['TabValue']) {
                $active = "active";
                $style = '';
            }
            $Tab_String .= "<li class='$active'> <a href='" . $href . "' style='" . $style . "' > " . $return_tab_arr[$t]['TabValue'] . " </a> ";
        }

        $Tab_String .= '</ul>';

        return $Tab_String;

    }

///////////////////////////////////////////////Tabination

    function func_admin_tab($tab, $link)
    {
        $Tab_Str = array();
        if ($tab == USER) {
            $Tab_Str[] = array('TabValue' => VIEW_USER_DETAILS, "href" => "index.php?file=u-userview&mode=update" . $link);
            $Tab_Str[] = array('TabValue' => EDIT_USER_DETAILS, "href" => "index.php?file=u-useradd&mode=update" . $link);
            $Tab_Str[] = array('TabValue' => USER_SOCIAL_LIST, "href" => "index.php?file=u-usersocial&mode=update" . $link);

        }

        if ($tab == CATEGORY) {
            $Tab_Str[] = array('TabValue' => VIEW_CATEGORY_DETAILS, "href" => "index.php?file=cy-categoryview&mode=update" . $link);
            $Tab_Str[] = array('TabValue' => EDIT_CATEGORY_DETAILS, "href" => "index.php?file=cy-categoryadd&mode=update" . $link);

        }

        if ($tab == BUILDING) {
            $Tab_Str[] = array('TabValue' => VIEW_BUILDING_DETAILS, "href" => "index.php?file=b-buildingview&mode=update" . $link);
            $Tab_Str[] = array('TabValue' => EDIT_BUILDING_DETAILS, "href" => "index.php?file=b-buildingadd&mode=update" . $link);

        }

        if ($tab == COMPLAINT) {
            $Tab_Str[] = array('TabValue' => VIEW_COMPLAINT_DETAILS, "href" => "index.php?file=co-complaintview&mode=update" . $link);
            $Tab_Str[] = array('TabValue' => EDIT_COMPLAINT_DETAILS, "href" => "index.php?file=co-complaintadd&mode=update" . $link);
            $Tab_Str[] = array('TabValue' => LOG_COMPLAINT_DETAILS, "href" => "index.php?file=co-complaintlog&mode=update" . $link);

        }


        if ($tab == "Event") {
            $Tab_Str[] = array('TabValue' => "Edit Event", "href" => "index.php?file=e-eventadd&mode=update" . $link);
            $Tab_Str[] = array('TabValue' => "View Event", "href" => "index.php?file=e-eventview&mode=update" . $link);
            $Tab_Str[] = array('TabValue' => "View Event", "href" => "index.php?file=e-evevntdivaimage&mode=update" . $link);

            $Tab_Str[] = array('TabValue' => "Join User List", "href" => "index.php?file=e-eventguestlist&mode=update" . $link);
            $Tab_Str[] = array('TabValue' => "Sponser List", "href" => "index.php?file=e-eventsponserlist&mode=update" . $link);
//            $Tab_Str[] = array('TabValue' => "Log List", "href" => "index.php?file=e-eventloglist&mode=update" . $link);
//            $Tab_Str[] = array('TabValue' => "Messages List", "href" => "index.php?file=e-eventmessages&mode=update" . $link);
        }


        return $Tab_Str;


    }

    function func_set_temp_sess_msg($varmsg, $class = "alert-success", $header = 'Success')    //alert-danger
    {
        global $session_prefix;
        $_SESSION[$session_prefix . 'admin_var_msg'] = $varmsg;
        $_SESSION[$session_prefix . 'admin_msg_class'] = $class;
        $_SESSION[$session_prefix . 'admin_var_msg_title'] = $header;
    }

    function genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix, $vaildExt = "png,jpg,jpeg,gif,bmp,csv")
    {
        $msg = "";
        if (!empty($vphoto_name) and is_file($vphoto)) {
            // Remove Dots from File name

            $tmp = explode(".", $vphoto_name);
            for ($i = 0; $i < count($tmp) - 1; $i++)
                $tmp1[] = $tmp[$i];

            $file = implode("_", $tmp1);
            $ext = $tmp[count($tmp) - 1];

            $vaildExt_arr = explode(",", strtoupper($vaildExt));
            if (in_array(strtoupper($ext), $vaildExt_arr)) {
                $vphotofile = $prefix . $file . "." . $ext;
                $vphotofile = $this->replace_image_name_junk_char($vphotofile);
                $ftppath1 = $photopath . $vphotofile;


                if (!copy($vphoto, $ftppath1)) {
                    $vphotofile = '';
                    $msg = "File Not Uploaded Successfully !!";
                } else
                     $msg = "File Uploaded Successfully !!";
            } else {
                $vphotofile = '';
                $msg = "File Extension Is Not Valid, Vaild Ext are  $vaildExt !!!";
            }
        }
        $ret[0] = $vphotofile;
        $ret[1] = $msg;
        return $ret;
    }

    function replace_image_name_junk_char($str)
    {
        $rs_catname = trim($str);
        $rs_catname = str_replace("&", "", $rs_catname);
        $rs_catname = str_replace(" ", "", $rs_catname);
        $rs_catname = str_replace("---", "", $rs_catname);
        $rs_catname = str_replace("--", "", $rs_catname);
        $rs_catname = str_replace("-", "", $rs_catname);
        $rs_catname = str_replace("+", "", $rs_catname);
        $rs_catname = str_replace("%20", "", $rs_catname);
        $rs_catname = str_replace("`", "", $rs_catname);
        $rs_catname = str_replace("|", "", $rs_catname);
        $rs_catname = str_replace("^", "", $rs_catname);
        $rs_catname = str_replace("*", "", $rs_catname);
        $rs_catname = str_replace("#", "", $rs_catname);
        $rs_catname = str_replace("_", "", $rs_catname);
        $rs_catname = preg_replace('/\s+/', '', $rs_catname);
        $rs_catname = strtolower($rs_catname);

        return $rs_catname;
    }

    function removeEmptyValuesFromArray($array)
    {
        foreach ($array as $key => $value) {
            if (is_null($value)) {
                unset($array[$key]);
            }
        }
        return $array;
    }


    function generateUniqueToken($number)
    {
        $arr = array('a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'v', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'R', 'S',
            'T', 'U', 'V', 'X', 'Y', 'Z',
            '1', '2', '3', '4', '5', '6',
            '7', '8', '9', '0');
        $token = "";
        for ($i = 0; $i < $number; $i++) {
            $index = rand(0, count($arr) - 1);
            $token .= $arr[$index];
        }
        return $token;
    }

    function generateNumericUniqueToken($number)
    {
        $arr = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $token = "";
        for ($i = 0; $i < $number; $i++) {
            $index = rand(0, count($arr) - 1);
            $token .= $arr[$index];
        }
        return $token;
    }


    function getGoogleLangLat($addressStr)
    {

        $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($addressStr) . '&sensor=true');
        $coordinates = json_decode($coordinates);
        $lat = $coordinates->results[0]->geometry->location->lat;
        $long = $coordinates->results[0]->geometry->location->lng;

        foreach ($coordinates->results[0]->address_components as $v) {
            if ($v->types[0] == "country") {
                $country_code = $v->short_name;
            }
        }

        $lanlat = array("vLatitude" => $lat, "vLongitude" => $long, "vCountryCode" => $country_code);
        return $lanlat;
    }


    /**
     * Image re-size
     * @param int $width
     * @param int $height
     */
    function ImageResize($width, $height, $img_tmp_name, $img_name, $img_type, $newfilename, $upload_path)
    {

        /* Get original file size */
        list($w, $h) = getimagesize($img_tmp_name);

        /*
            $ratio = $w / $h;
            $size = $width;

            $width = $height = min($size, max($w, $h));

            if ($ratio < 1) {
                $width = $height * $ratio;
            } else {
                $height = $width / $ratio;
            }
        */

        /* Calculate new image size */
        $ratio = max($width / $w, $height / $h);
        $h = ceil($height / $ratio);
        $x = ($w - $width / $ratio) / 2;
        $w = ceil($width / $ratio);

        /* set new file name */
        $path = $upload_path . $newfilename;


        /* Save image */
        if ($img_type == 'image/jpeg') {
            /* Get binary data from image */
            $imgString = file_get_contents($img_tmp_name);

            /* create image from string */
            $image = imagecreatefromstring($imgString);
            $tmp = imagecreatetruecolor($width, $height);
            imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);

            imagejpeg($tmp, $path, 100);

        } else if ($img_type == 'image/png') {
            $image = imagecreatefrompng($img_tmp_name);
            $tmp = imagecreatetruecolor($width, $height);
            imagealphablending($tmp, false);
            imagesavealpha($tmp, true);
            imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);
            imagepng($tmp, $path, 0);
        } else if ($img_type == 'image/gif') {
            $image = imagecreatefromgif($img_tmp_name);
            $tmp = imagecreatetruecolor($width, $height);
            $transparent = imagecolorallocatealpha($tmp, 0, 0, 0, 127);
            imagefill($tmp, 0, 0, $transparent);
            imagealphablending($tmp, true);

            imagecopyresampled($tmp, $image, 0, 0, 0, 0, $width, $height, $w, $h);
            imagegif($tmp, $path);
        } else {
            return false;
        }


        imagedestroy($image);
        imagedestroy($tmp);
        return true;
    }


    function addlink($link)
    {


        $addlink = "<label><button class='btn btn-primary btn-squared ' onClick=window.location='" . $link . "' ;return false;' type='button'><i class='fa fa-plus''></i>" . ADD_NEW . "</button></label>";
        return $addlink;
    }


    function showalllink($extra_url)
    {

        $showalllink = "<label><button class='btn btn-blue btn-squared' type='button' onClick=window.location='" . $extra_url . "';return false;'>" . SHOW_ALL . "</button></label>";
        return $showalllink;

    }

    function editlink($link, $id)
    {


        $link .= '&iId=' . $id;


        $editlink = '<a href="' . $link . '" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i></a>';

        return $editlink;

    }


    function viewlink($link, $id, $adm, $per)
    {

        if ($adm == 'Admin' || $per == '') {
            $link .= '&iId=' . $id;


            $editlink = '<a href="' . $link . '" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="View"  ><i class="clip-popout"></i></a>';

            return $editlink;
        }

    }

    function singledeletelink($link, $id)
    {

        $link .= $id;

        //$singledeletelink ="<a data-original-title='Remove' data-placement='top' class='btn btn-xs btn-bricky tooltips' href='javascript://' onClick='return deleteSingleRecord (\''.$link.'\');'> <i class='fa fa-times fa fa-white'></i></a>";
        $singledeletelink = '<a data-original-title="Remove" data-placement="top" class="btn btn-xs btn-bricky tooltips" href="javascript://" onclick="return deleteSingleRecord (\'' . $link . '\');"><i class="fa fa-times fa fa-white"></i></a>';

        return $singledeletelink;


    }

    function activelink()
    {

        $a = ' changeStatusEvent ("active","frmlist")';

        $activelink = "<label><button class='btn btn-green btn-squared ' onClick= ' return $a ' type='button'><i class='fa fa-user fa fa-white '></i>" . ACTIVE . "</button></label> ";
        return $activelink;

    }

    function inactivelink()
    {

        $a = ' changeStatusEvent ("inactive","frmlist")';

        $inactivelink = "<label><button class='btn btn-orange btn-squared ' onClick= ' return $a ' type='button'><i class='fa fa-user fa fa-white '></i>" . INACTIVE . "</button></label> ";
        return $inactivelink;

    }


    function multideletelink()
    {

        $a = ' changeStatusEvent ("delete","frmlist")';

        $multideletelink = "<label><button class='btn btn-bricky btn-squared ' onClick= ' return $a ' type='button'><i class='fa fa-trash-o '></i>" . DELETE . "</button></label> ";
        return $multideletelink;


    }

    function completelink($adm, $per)
    {
        if ($adm == 'Admin' || $per == 'yes') {
            $a = ' changeStatusEvent ("complete","frmlist")';

            $completelink = "<label><button class='btn btn-green btn-squared ' onClick= ' return $a ' type='button'><i class='fa fa-user fa fa-white '></i> Complete</button></label> ";
            return $completelink;
        } else if ($per == "") {
            $completelink = "<label><button class='btn btn-green btn-squared ' onClick= ' return $a ' type='button'><i class='fa fa-user fa fa-white '></i> Complete</button></label> ";
            return $completelink;
        }
    }

    function cancellink($adm, $per)
    {
        if ($adm == 'Admin' || $per == 'yes') {
            $a = ' changeStatusEvent ("cancel","frmlist")';

            $cancellink = "<label><button class='btn btn-orange btn-squared ' onClick= ' return $a ' type='button'><i class='fa fa-user fa fa-white '></i> Cancel</button></label> ";
            return $cancellink;
        } else if ($per == "") {
            $cancellink = "<label><button class='btn btn-orange btn-squared ' onClick= ' return $a ' type='button'><i class='fa fa-user fa fa-white '></i> Cancel</button></label> ";
            return $cancellink;
        }
    }


    function pagepermission($per)
    {
        if ($per == 'yes') {


            return 1;
        } else if ($per == "") {
            return 1;

        } else {
            return 0;
        }
    }


    function cleanRequest(&$request)
    {
        global $obj;
        foreach ($request as $k => $v) {
            if (is_array($v)) {
                $request[$k] = $this->cleanRequest($v);
            } else {
                $request[$k] = mysqli_real_escape_string($obj->CONN, $v);
            }
        }
        return $request;
    }


    function getFacebookImageURL($facebookId, $type = 'large')
    {
        return "https://graph.facebook.com/" . $facebookId . "/picture?type=" . $type;
    }

    function uppercase($response)
    {
        foreach ($response as $k1 => $v1) {
            if (is_array($v1)) {
                $response["a" . ucfirst($k1)] = $this->uppercase($v1);
            } else {
                $response["a" . ucfirst($k1)] = $v1;
            }
            if (ctype_lower(substr($k1, 0, 1))) {
                unset($response[$k1]);
            }
        }
        return $response;
    }

    function get_unique($table, $field, $value, $id = null)
    {
        global $obj;
        if ($id != "") {
            $WHERE = "where $field ='$value' AND iUserId!=$id AND eStatus!='2'";
        } else {
            $WHERE = "where $field ='$value'  AND eStatus!='2'";
        }
         $sql = "select count($field) as count from $table $WHERE";
        $row = $obj->select($sql);

        if ($row[0]['count'] > 0) {
            return false;
        } else {
            return true;
        }


    }

    function get_updated_table($table, $field, $timestamp)
    {
        global $obj;
        $WHERE="";
        if ($timestamp != 0) {
            $WHERE = "where $field >='$timestamp'";
        }
        $sql = "select count($field) as count from $table $WHERE";
        $row = $obj->select($sql);

        if ($row[0]['count'] > 0) {
            return true;
        } else {
            return false;
        }


    }

    function get_amount($famount)
    {

        global $obj;

        setlocale(LC_MONETARY, 'en_IN');
        $amount = money_format('%!i', $famount);
        return $amount;
    }
    function Breadcrumbs($array)
    {
        $HTML = '';

        $HTML .= '<div class="row">';
        $HTML .= '<div class="col-md-12">';
        $HTML .= '<ul class="breadcrumbs-alt">';
        foreach ($array as $k => $v) {
            $CLASS = ($k == '#') ? 'class="current"' : '';
            $HTML .= '<li>';
            $HTML .= '<a href="' . $k . '" ' . $CLASS . '>' . $v . '</a>';
            $HTML .= '</li>';
        }
        $HTML .= '';
        $HTML .= '</ul>';
        $HTML .= '</div>';
        $HTML .= '</div>';

        return $HTML;
    }

    function TBL_header($MODULE, $LINK)
    {
        /*$HTML = '';*/
        $HTML = '<header class="panel-heading">';
        $HTML .= $MODULE . ' Listing';
        $HTML .= '<span class=" pull-right header-btn">';
        $HTML .= '<a class="btn btn-primary" href="' . $LINK . '">Add New ' . $MODULE . '</a>';
        $HTML .= '<button class="btn btn-danger" onclick="delete_record()">DELETE</button>';
        $HTML .= '</span>';
        $HTML .= '</header>';
        return $HTML;
    }

    function QR_CODE($CODE, $USER)
    {

        global $site_path;
        include_once($site_path . "library/QR/qrlib.php");

        $upload = $site_path . 'uploads/QR_CODE/';

        ## Don't Touch Parameter
        $errorCorrectionLevel = 'H';
        $matrixPointSize = 10;

        $filename = $upload . $USER . '_' . $CODE . '.png';
        QRcode::png($CODE, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
        return $USER . '_' . $CODE . '.png';
    }

    function get_SC_SEND_mail($TYPE,$DATA,$TO,$SUBJECT)
    {

        global $MAIL_REPLY;
        switch($TYPE)
        {
            case 'register':
                $HEADER = $this->get_mail_HEADER($MAIL_REPLY['support']);
                $MESSAGE = $this->register_mail($DATA);
                break;
            case 'forgot_password':
                $HEADER = $this->get_mail_HEADER($MAIL_REPLY['support']);
                $MESSAGE = $this->forget_password_mail($DATA);
                break;
            case 'feedback':
                $HEADER = $this->get_mail_HEADER($MAIL_REPLY['support']);
                $MESSAGE = $this->feedback_mail($DATA);
                break;
            case 'redeem':
                $HEADER = $this->get_mail_HEADER($MAIL_REPLY['support']);
                $MESSAGE = $this->redeem_mail($DATA);
                break;
            case 'redeem_request':
                $HEADER = $this->get_mail_HEADER($MAIL_REPLY['support']);
                $MESSAGE = $this->redeem_request_mail($DATA);
                break;
            case 'csv_register':
                $HEADER = $this->get_mail_HEADER($MAIL_REPLY['support']);
                $MESSAGE = $this->csv_register($DATA);
                break;

        }

        $RETURN = $this->SEND_SC_MAIL($HEADER,$MESSAGE,$TO,$SUBJECT);
        return $RETURN;
    }

    function get_mail_HEADER($EMAIL, $NAME = 'Service Calibrate')
    {
        $From_Name = $NAME;
        $new_date_str = strtotime(gmdate('Y-m-d H:i:s'));
        $date = date('F d, Y ', $new_date_str) . ' at ' . date('h:i A', $new_date_str);
        $headers="";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        #$headers = "MIME-Version: 1.0\n";
        #$headers .= "Content-type: text/html; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nX-Priority: 1\nX-MSMail-Priority: High\n";
        $headers .= "From: " . $From_Name . ' <' . $EMAIL . '>' . "\n" . "Reply-To: " . $EMAIL . "\n" . "Date: " . $date . "\n";
        return $headers;
    }

    function register_mail($DATA)
    {
        global $site_url_mail ,$site_url, $MAIL_LINK, $MAIL_EMAIL, $MAIL_SOCIAL_LINK;

        $HTML = '<div style="margin: 0px auto; width: 980px;">
    <div align="center"><img src="' . $site_url_mail . 'assets/images/mail_logo.png" title="Service Calibrate" alt="Service Calibrate"></div>

    <div>

        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;"> Welcome to Service Calibrate!
        </div>
        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;"> We sincerely thank you for choosing us as your information technology and service analytics solution provider.
        </div>

        <div style="margin: 20px 0px 0px; font-size: 18px;">
            <a href="" style="font-weight: bold; color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;"><i><u>Retailers:</u></i> </a>You can order the QR code via website or print your unique QR code for Free directly from website.
        </div>
        <div style="margin: 20px 0px 0px;">
            <a href="" style="font-weight: bold; color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;"><i><u>Next Steps:-</u></i></a>
        </div>
        <div style="margin: 20px 0px 0px;color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;">
        <table cellspacing="20" style="margin: -23px 0px -21px 21px; font-size: 18px;">
                <tr>
                    <td style="padding: 18px 0px 0px;vertical-align: top;">1.</td>
                    <td style="padding: 18px 0px 0px;">Download the app or login to<a href="' . $MAIL_SOCIAL_LINK['home'] . '" target="_blank"> www.servicecalibrate.com  </a>to start measuring service.
                    </td>
                </tr>


                <tr>
                    <td style="padding: 0px 0px 0px;vertical-align: top;">2.</td>
                    <td style="padding: 0px 0px 0px;">Print & Place/Stick your QR code at the location entrance & receiving areas of your facilities so vendors can easily locate it.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 0px 0px;vertical-align: top;">3.</td>
                    <td style="padding: 0px 0px 0px;">Discover "Quantify Rewards" our exclusive loyalty program with plenty of benefits for retailers:
                    </td></tr>
 <tr>
                       <td style="padding: 0px 0px 0px;"></td>

                    <td>
                <ul list-style-type: circle;>
                <li style="margin-top: -28px !important;">
                Earn Points every time you rate a vendor.</li>
                </ul>
                </td>
                </tr>                 <tr>
                    <td style="padding: 0px 0px 0px;"></td>

                    <td>
               
<ul list-style-type: circle; style="margin-top: -17px;">
                <li style="margin-top: -50px;">
              Get bonus points for every referral, when another user registers & mentions your email address on "Referred By" field.</li>
                </ul>
                </td>
                </tr>
                 <tr>

                    <td style="padding: 0px 0px 0px;"></td>


                    <td>
              
<ul list-style-type: circle; style="margin-top: -17px;">
                <li style="margin-top: -50px;">
          Easily redeem points for a gift card from the website via "Redeem Points". Terms & conditions apply.</li>
                </ul>
                </td>
                </tr>

                </tr>
            </table>
            </div>

            <div style="margin: 20px 0px 0px; font-size: 18px;" >
            <a href="" style="font-weight: bold; color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;"><i><u>Distributors & Manufacturers:</u></i>  </a>Employees can use Service Calibrate features with or without scanning QR Codes at a Retail Location:
        </div>
        <div style="margin: 20px 0px 0px;">
            <a href="" style="font-weight: bold; color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;"><i>Next Steps:-</i></a>
        </div>
        <div style="margin: 20px 0px 0px;color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;">
        <table cellspacing="20" style="margin: -23px 0px -21px 21px; font-size: 18px;">
                <tr>
                    <td style="padding: 0px 0px 0px;vertical-align: top;">1.</td>
                    <td style="padding: 0px 0px 0px;">Download the app or login to<a href="' . $MAIL_SOCIAL_LINK['home'] . '" target="_blank"> www.servicecalibrate.com  </a>stop competing on price alone and start proving you are the best vendor for your customers.
                    </td>
                </tr>

                <tr>
                    <td style="padding: 0px 0px 0px;vertical-align: top;">2.</td>
                    <td style="padding: 0px 0px 0px;">Discover "Quantify Rewards" our exclusive loyalty program with plenty of benefits for distributors/manufacturers:
                    </td>
                      <tr>
                       <td style="padding: 0px 0px 0px;"> 

                       
                       </td>

                   <td>
                <ul list-style-type: circle;>
                <li style="margin-top: -28px !important;">
                Earn points every time you rate a vendor.</li>
                </ul>
                </td>
                </tr>
                 
                 <tr>
                    <td style="padding: 0px 0px 0px;"></td>

                    <td>
                <ul list-style-type: circle;>
                <li style="margin-top: -50px !important;">
              Get bonus points for every referral, when another user registers & enters your email address in the "Referred By" field.</li>
                </ul>
                </td>
                </tr>
                 <tr>

                    <td style="padding: 0px 0px 0px;"></td>


                    <td>
                <ul list-style-type: circle;>
                <li style="margin-top: -50px !important;">
         Easily redeem points for a gift card from our website via the "Redeem Points” link. Terms & conditions apply.</li>
                </ul>
                </td>
                </tr>

                </tr>
            </table>
            </div>

        <div style="font-size: 18px; margin: 20px 0px;">
           Please use the link below for a quick easy guide on how to use Service Calibrate to increase your sales and boost your business’s service performance
        </div>

        <div style="font-size: 18px; margin: 20px 0px;"><a href="' . $MAIL_SOCIAL_LINK['home'] . '" target="_blank">www.servicecalibrate.com </a></div>


        <div style="font-size: 18px; margin: 20px 0px;">
          If you have any questions please email us <a href="mailto:contactus@servicecalibrate.com">contactus@servicecalibrate.com</a> or go to Help/FAQ on the website. 
         <br> <br>Thank you for your business. <br>
        </div>


            <div style="font-size: 18px; margin: 16px 0 20px">Service calibrate Team</div>
        </div>
    </div>

</div>';

        return $HTML;
    }

    function csv_register($DATA)
    {
        global $site_url_mail ,$site_url, $MAIL_LINK, $MAIL_EMAIL, $MAIL_SOCIAL_LINK;

        $HTML = '<div style="margin: 0px auto; width: 980px;">
    <div align="center"><img src="' . $site_url_mail  . 'assets/images/mail_logo.png" title="Service Calibrate" alt="Service Calibrate"></div>

    <div>

        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;"> Welcome to Service Calibrate!
        </div>
        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;"> We sincerely thank you for choosing us as your information technology and service analytics solution provider.
        </div>


        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;">Your company/management has registered you to use Service Calibrate & start measuring service.
        </div>


        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 50px 0px 0px 50px;">Visit:-<a href="' . $MAIL_SOCIAL_LINK['home'] . '" target="_blank">  www.servicecalibrate.com </a>
        </div>
        <b>
        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px 50px;">Please Login:
        ' . $DATA['email'] . '
        </div>
 <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px 50px;">Temporary Password: - ' . $DATA['password'] . '
        </div>
        </b>
         <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 50px 50px;">
You can reset your password anytime from www.servicecalibrate.com
        </div>

        <div style="margin: 20px 0px 0px; font-size: 18px;">
            <a href="" style="font-weight: bold; color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;"><i><u>Retailers:</u></i> </a>You can order the QR code via website or print your unique QR code for Free directly from website.
        </div>
        <div style="margin: 20px 0px 0px;">
            <a href="" style="font-weight: bold; color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;"><i>Next Steps:-</i></a>
        </div>
        <div style="margin: 20px 0px 0px;color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;">
        <table cellspacing="20" style="margin: -23px 0px -21px 21px; font-size: 18px;">
                <tr>
                    <td style="padding: 18px 0px 0px;vertical-align: top;">1.</td>
                    <td style="padding: 18px 0px 0px;">Download the app or login to<a href="' . $MAIL_SOCIAL_LINK['home'] . '" target="_blank"> www.servicecalibrate.com  </a>to start measuring service.
                    </td>
                </tr>

                <tr>
                    <td style="padding: 0px 0px 0px;vertical-align: top;">2.</td>
                    <td style="padding: 0px 0px 0px;">Print & Place/Stick your QR code at the location entrance & receiving areas of your facilities so vendors can easily locate it.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 0px 0px;vertical-align: top;">3.</td>
                    <td style="padding: 0px 0px 0px;">Discover "Quantify Rewards" our exclusive loyalty program with plenty of benefits for retailers:
                    </td>
                      <tr>
                       <td style="padding: 0px 0px 0px;"></td>

                    <td>
                <ul list-style-type: circle;>
                <li style="margin-top: -28px !important;">
                Earn points every time you rate a vendor.</li>
                </ul>
                </td>
                </tr>
                 <tr>
                    <td style="padding: 0px 0px 0px;"></td>

                    <td>
                <ul list-style-type: circle;>
                <li style="margin-top: -50px !important;">
              Get bonus points for every referral, when another user registers & mentions your email address on "Referred By" field.</li>
                </ul>
                </td>
                </tr>
                 <tr>

                    <td style="padding: 0px 0px 0px;"></td>


                    <td>
                <ul list-style-type: circle;>
                <li style="margin-top: -50px !important;">
          Easily redeem points for a gift card from the website via "Redeem Points". Terms & conditions apply.</li>
                </ul>
                </td>
                </tr>

                </tr>
            </table>
            </div>

            <div style="margin: 20px 0px 0px;font-size: 18px;" >
            <a href="" style="font-weight: bold; color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;"><i><u>Distributors & Manufacturers:</u></i>  </a>Employees can use Service Calibrate features with or without scanning QR Codes at a Retail Locations:
        </div>
        <div style="margin: 20px 0px 0px;">
            <a href="" style="font-weight: bold; color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;"><i>Next Steps:-</i></a>
        </div>
        <div style="margin: 20px 0px 0px;color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;">
        <table cellspacing="20" style="margin: -23px 0px -21px 21px; font-size: 18px;">
                <tr>
                    <td style="padding: 0px 0px 0px;vertical-align: top;">1.</td>
                    <td style="padding: 0px 0px 0px;">Download the app or login to<a href="' . $MAIL_SOCIAL_LINK['home'] . '" target="_blank"> www.servicecalibrate.com  </a>stop competing on price alone and start provingyou are the best vendor for your customers.
                    </td>
                </tr>

                <tr>
                    <td style="padding: 0px 0px 0px;vertical-align: top;">2.</td>
                    <td style="padding: 0px 0px 0px;">Discover "Quantify Rewards" our exclusive loyalty program with plenty of benefits for distributors/manufacturers:
                    </td>
                      <tr>
                       <td style="padding: 0px 0px 0px;"></td>

                    <td>
                <ul list-style-type: circle;>
                <li style="margin-top: -28px;">
              Earn points every time you scan a QR code.</li>
                </ul>
                </td>
                </tr>
                 <tr>
                    <td style="padding: 0px 0px 0px;"></td>

                    <td>
                <ul list-style-type: circle;>
                <li style="margin-top: -50px;">
              Get bonus points for every referral, when another user registers & adds your email address on "Referred By" field.</li>
                </ul>
                </td>
                </tr>
                 <tr>

                    <td style="padding: 0px 0px 0px;"></td>


                    <td>
                <ul list-style-type: circle;>
                <li style="margin-top: -50px;">
           Easily redeem points for a gift card from our  website via the "Redeem Points" link. Terms & conditions apply.</li>
                </ul>
                </td>
                </tr>

                </tr>
            </table>
            </div>

        <div style="font-size: 18px; margin: 20px 0px;">
           Please use the link below for a quick easy guide on how to use Service Calibrate to increase your sales and boost your business’s service performance
        </div>

        <div style="font-size: 18px; margin: 20px 0px;"><a href="' . $MAIL_SOCIAL_LINK['home'] . '" target="_blank">www.servicecalibrate.com </a></div>


        <div style="font-size: 18px; margin: 20px 0px;">
          If you have any questions please email us <a href="mailto:contactus@servicecalibrate.com">contactus@servicecalibrate.com</a> or go to Help/FAQ on the website.  
<br><br>Thank you for your business.<br>
        </div>


            <div style="font-size: 18px; margin: 16px 0 20px">Service calibrate Team</div>
        </div>
    </div>

</div>';

        return $HTML;
    }

    function forget_password_mail($DATA)
    {
        global $site_url_mail ,$site_url, $MAIL_LINK, $MAIL_EMAIL, $MAIL_SOCIAL_LINK;

        $HTML = '<div style="margin: 0px auto; width: 980px;">
    <div align="center"><img src="' . $site_url_mail  . 'assets/images/mail_logo.png" title="Service Calibrate" alt="Service Calibrate"></div>

    <div>
        <div style="margin: 30px 0px 0px; font-size: 18px; color: rgb(55, 55, 55);">Hi ' . $DATA['name'] . '!</div>
        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;"> Forgot your password? No worries

        </div>
        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;">Please click the link below to set a new password :
        </div>



        <div style="font-size: 18px; margin: 20px 0px;">url :-<a href=" ' . $DATA['url'] . '">Click Here</a>
 </a></div>

        <div style="margin: 20px 0px 0px;color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;">
         If you did not reset your email please contact us?
        </div>
        <div style="margin: 20px 0px 0px;color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;">
           If this message has been sent to you in error please excuse this message?
           </div>

            

            <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0 20px">Thank you for your business,</div>
            <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 16px 0 20px">Service calibrate Team</div>
        </div>
    </div>

</div>';
        return $HTML;
    }

    function redeem_mail($DATA)
    {
        global $site_url_mail ,$site_url, $MAIL_LINK, $MAIL_EMAIL, $MAIL_SOCIAL_LINK;

        $HTML = '<div style="margin: 0px auto; width: 980px;">
    <div align="center"><img src="' . $site_url_mail  . 'assets/images/mail_logo.png" title="Service Calibrate" alt="Service Calibrate"></div>

    <div>
        <div style="margin: 30px 0px 0px; font-size: 18px; color: rgb(55, 55, 55);">Hi ' . $DATA['name'] . '!</div>
        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;"> Hurrey ,you gift voucher is here for you

        </div>
        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;">Please click the link below to Get it :
        </div>



        <div style="font-size: 18px; margin: 20px 0px;">url :-<a href=" ' . $DATA['url'] . '">Click Here</a>
 </a></div>

        <div style="margin: 20px 0px 0px;color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;">
Enjoy You gift Card
        </div>
        <div style="margin: 20px 0px 0px;color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;">

           </div>


            <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0 20px">Thank you for your business,</div>
            <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 16px 0 20px">Service calibrate Team</div>
        </div>
    </div>

</div>';
        return $HTML;
    }

    function redeem_request_mail($DATA)
    {
        
        global $site_url_mail,$site_url, $MAIL_LINK, $MAIL_EMAIL, $MAIL_SOCIAL_LINK;

        $HTML = '<div style="margin: 0px auto; width: 980px;">
    <div align="center"><img src="' . $site_url_mail . 'assets/images/mail_logo.png" title="Service Calibrate" alt="Service Calibrate"></div>

    <div>
        <div style="margin: 30px 0px 0px; font-size: 18px; color: rgb(55, 55, 55);">Hi ' . $DATA['name'] . '!</div>
        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;"> We are greatful to receive your request for gift voucher

        </div>
        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;">we will send send you send you voucher confirmation mail soon.
        </div>



        <div style="font-size: 18px; margin: 20px 0px;"></a>
 </a></div>

        <div style="margin: 20px 0px 0px;color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;">
Enjoy
        </div>
        <div style="margin: 20px 0px 0px;color: rgb(0, 0, 0); line-height: 26px; font-size: 18px;">

           </div>

            

            <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0 20px">Thank you for your business,</div>
            <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 16px 0 20px">Service calibrate Team</div>
        </div>
    </div>

</div>';
        return $HTML;
    }


    function feedback_mail($DATA)
    {
        global $site_url_mail,$site_url, $MAIL_LINK, $MAIL_EMAIL, $MAIL_SOCIAL_LINK;

        $HTML = '<div style="margin: 0px auto; width: 980px;">
    <div align="center"><img src="' . $site_url_mail . 'assets/images/mail_logo.png" title="Service Calibrate" alt="Service Calibrate"></div>

    <div>
        <div style="margin: 30px 0px 0px; font-size: 18px; color: rgb(55, 55, 55);">Hi ' . $DATA['name'] . '!</div>


        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;">Did you just leave a comment?  We like you already!

        </div>
        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;"> Thank you, we absolutely love getting feedback and ideas on how we can improve.  Our team will look into this matter immediately.


        </div>
        <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;">
        For further Communication Mail To:: <a href="mailto:dharmesh@servicecalibrate.com">dharmesh@servicecalibrate.com</a>

        </div>



       <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0px 0px;">
            We appreciate the time you took to comment.
        </div>

            

            <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 20px 0 20px">Thank you for your business,</div>
            <div style="color: rgb(55, 55, 55); font-size: 18px; margin: 16px 0 20px">Service calibrate Team</div>
        </div>
    </div>

</div>';
        return $HTML;
    }


    function SEND_SC_MAIL($HEADER,$MESSAGE,$TO,$SUBJECT)
    {
        global $site_path, $debug_mode, $SENDING_EMAIL_THROUGH_FUNCTION, $SMTP_URL, $SMTP_PORT;

        $new_date           =   gmdate('Y-m-d H:i:s');
        $new_date_str       =   strtotime($new_date);
        $date   =   date('F d, Y ',$new_date_str).' at '.date('h:i A',$new_date_str);

        $cnt = "";
        $cnt .= "<br>\n\n====================================================================\n\n<br>";
        $cnt .= $_SERVER['REMOTE_ADDR'];
        $cnt .= "<br>\n\n====================================================================\n\n<br>";
        $cnt = "Headers : \n".$HEADER."\n";
        $cnt .= "<br>\n\n====================================================================\n\n<br>";
        $cnt .= "Date Time :". $date;
        $cnt .= "<br>\n\n====================================================================\n\n<br>";
        $cnt .= "To : ".$TO."\n";
        $cnt .= "<br>\n\n====================================================================\n\n<br>";
        $cnt .= "Subject : ".$SUBJECT."\n";
        $cnt .= "<br>\n\n====================================================================\n\n<br>";
        $cnt .= "Body : \n".$MESSAGE."\n";
        $cnt .= "<br>\n\n====================================================================\n\n<br>";

        #if($_SERVER['HTTP_HOST'] == "192.168.1.26")
        {
            $filename = $site_path."mail.html";
            if (!$fp = fopen($filename, 'a')) {
                print "Cannot open file ($filename)";
                exit;
            }
            if (!fwrite($fp, $cnt)) {
                print "Cannot write to file ($filename)";
                exit;
            }
            fclose($fp);
            $status = true;
        }

        if($_SERVER['HTTP_HOST'] != "192.168.1.60")
        {
            $status = @mail($TO, $SUBJECT, $MESSAGE, $HEADER);
        }
        echo "in";
        return $status;
    }

    function getdateforamate($date)
    {

        $date = explode('-', $date);
        $date = $date[2] . '-' . $date[0] . '-' . $date[1];
        $date = strtotime($date);
        return $date;
    }

}
///////////////////////////////////////////////////////////////////////	


function pr($data)
{
    echo "<pre/>";
    print_r($data);
}

function clean_sql($sql)
{
    $sql = str_replace(", ", " ", $sql);
    $sql = str_replace(" ,", " ", $sql);
    $sql = str_replace(",,", ",", $sql);
    return $sql;
}
