<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    store_master
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/store_master.class.php
 * TABLE:        store_master
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class store_master
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iStoreMasterId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iStoreMasterId;
    protected $_vStoreName;
    protected $_iDtAdded;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iStoreMasterId = null;
        $this->_vStoreName = null;
        $this->_iDtAdded = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiStoreMasterId()
    {
        return $this->_iStoreMasterId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiStoreMasterId($val)
    {
        $this->_iStoreMasterId = $val;
    }

    public function getvStoreName()
    {
        return $this->_vStoreName;
    }

    public function setvStoreName($val)
    {
        $this->_vStoreName = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM store_master WHERE iStoreMasterId = $id";
        $row = $this->_obj->select($sql);

        $this->_iStoreMasterId = $row[0]['iStoreMasterId'];
        $this->_vStoreName = $row[0]['vStoreName'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_eStatus = $row[0]['eStatus'];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM store_master WHERE iStoreMasterId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iStoreMasterId = ""; // clear key for autoincrement

        $sql = "INSERT INTO store_master ( vStoreName,iDtAdded,eStatus ) VALUES ( '" . $this->_vStoreName . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "' )";
        $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE store_master SET  vStoreName = '" . $this->_vStoreName . "' , iDtAdded = '" . $this->_iDtAdded . "' , eStatus = '" . $this->_eStatus . "'  WHERE iStoreMasterId = $id ";
        $this->_obj->sql_query($sql);

    }


}
