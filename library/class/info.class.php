<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    info
* DATE:         02.04.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/info.class.php
* TABLE:        info
* DB:           service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class info
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iInfoId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iInfoId;
    protected $_vKeyword;
    protected $_tDescription;
    protected $_VFor;
    protected $_vTitle;
    protected $_iDtAdded;
    protected $_iDtUpdated;
    protected $_eStatus;




    /**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

        $this->_iInfoId = null;
        $this->_vKeyword = null;
        $this->_tDescription = null;
        $this->_VFor = null;
        $this->_vTitle = null;
        $this->_iDtAdded = null;
        $this->_iDtUpdated = null;
        $this->_eStatus = null;

    }

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiInfoId()
	{
		return $this->_iInfoId;
	}

/**
*   @desc   SETTER METHODS
*/


	public function setiInfoId($val)
	{
		 $this->_iInfoId =  $val;
	}

	public function getvKeyword()
	{
		return $this->_vKeyword;
	}

	public function setvKeyword($val)
	{
		 $this->_vKeyword =  $val;
	}

    public function getiDtUpdated()
    {
        return $this->_iDtUpdated;
    }

    public function setiDtUpdated($val)
    {
        $this->_iDtUpdated =  $val;
    }

	public function gettDescription()
	{
		return $this->_tDescription;
	}

	public function settDescription($val)
	{
		 $this->_tDescription =  $val;
	}

	public function getiDtAdded()
	{
		return $this->_iDtAdded;
	}

	public function setiDtAdded($val)
	{
		 $this->_iDtAdded =  $val;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}

    public function getVFor()
    {
        return $this->_VFor;
    }

    public function getvTitle()
    {
        return $this->_vTitle;
    }
    public function setVFor($val)
    {
        $this->_VFor =  $val;
    }

    public function setvTitle($val)
    {
        $this->_vTitle =  $val;
    }



    /**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id = null)
	{
        $where = 'Where 1=1';
        if (!is_null($id)) {
            $where .= " AND iInfoId = $id";
        }
         $sql =  "SELECT * FROM info $where";

        $row =  $this->_obj->select($sql);

		 $this->_iInfoId = $row[0]['iInfoId'];
		 $this->_vKeyword = $row[0]['vKeyword'];
		 $this->_tDescription = $row[0]['tDescription'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
		 $this->_iDtUpdated = $row[0]['iDtUpdated'];
		 $this->_eStatus = $row[0]['eStatus'];
        $this->_VFor = $row[0]['VFor'];
        $this->_vTitle = $row[0]['vTitle'];

        return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM info WHERE iInfoId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

    function insert()
    {
        $this->iInfoId = ""; // clear key for autoincrement

        $sql = "INSERT INTO info ( vKeyword,tDescription,VFor,vTitle,iDtAdded,eStatus,iDtUpdated ) VALUES ( '".$this->_vKeyword."','".$this->_tDescription."','".$this->_VFor."','".$this->_vTitle."','".$this->_iDtAdded."','".$this->_eStatus."','".$this->_iDtUpdated."' )";
        $result=$this->_obj->insert($sql);
        return $result;
    }


/**
*   @desc   UPDATE
*/


   /* function update($id)
    {
       $sql = " UPDATE info SET ";
        $sql .= ($this->_vKeyword != null) ? " vKeyword = '" . $this->_vKeyword . "'," : "";
        $sql .= ($this->_tDescription != null) ? ", tDescription = '" . $this->_tDescription . "'," : "";
        $sql .= ($this->_VFor != null) ? ", VFor = '" . $this->_VFor . "'," : "";
        $sql .= ($this->_vTitle != null) ? ", vTitle = '" . $this->_vTitle . "'," : "";
        $sql .= ($this->_iDtAdded != null) ? ", iDtAdded = '" . $this->_iDtAdded . "'," : "";
        $sql .= ($this->_eStatus != null) ? ", eStatus = '" . $this->_eStatus . "'" : "";
        $sql .= " WHERE iInfoId = $id ";
        $UPDATE = clean_sql($sql);

        $this->_obj->sql_query($UPDATE);
    }*/


    function update($id)
    {

       $sql = " UPDATE info SET  vKeyword = '".$this->_vKeyword."' , tDescription = '".$this->_tDescription."' , VFor = '".$this->_VFor."' , vTitle = '".$this->_vTitle."' , iDtUpdated = '".$this->_iDtUpdated."' , eStatus = '".$this->_eStatus."'  WHERE iInfoId = $id ";
        $this->_obj->sql_query($sql);

    }


    function select_report_info($rname)
    {
        $sql = "SELECT * FROM info where vKeyword='$rname'";

        $row = $this->_obj->select($sql);

        $this->_iInfoId = $row[0]['iInfoId'];
        $this->_vKeyword = $row[0]['vKeyword'];
        $this->_tDescription = $row[0]['tDescription'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
 $this->_iDtUpdated = $row[0]['iDtUpdated'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_VFor = $row[0]['VFor'];
        $this->_vTitle = $row[0]['vTitle'];
        return $row;
    }

    function select_API($time = null)
    {
        $where = 'Where 1=1';
        if (!is_null($time)) {
            $where .= " AND iInfoId = $time";
        }
        $sql =  "SELECT * FROM info $where";

        $row =  $this->_obj->select($sql);

        $this->_iInfoId = $row[0]['iInfoId'];
        $this->_vKeyword = $row[0]['vKeyword'];
        $this->_tDescription = $row[0]['tDescription'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_VFor = $row[0]['VFor'];
        $this->_vTitle = $row[0]['vTitle'];

        return $row;
    }


}
