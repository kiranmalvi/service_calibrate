<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    schedule
* DATE:         14.05.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/schedule.class.php
* TABLE:        schedule
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class schedule
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iScheduleId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iScheduleId;  
	protected $_iDistributorId;  
	protected $_iRetailerId;  
	protected $_iEmployeeId;  
	protected $_iUserRoleId;  
	protected $_eSchedule;  
	protected $_tDays;  
	protected $_eStatus;  
	protected $_iDtUpdated;  
	protected $_iDtAdded;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iScheduleId = null; 
		$this->_iDistributorId = null; 
		$this->_iRetailerId = null; 
		$this->_iEmployeeId = null; 
		$this->_iUserRoleId = null; 
		$this->_eSchedule = null; 
		$this->_tDays = null; 
		$this->_eStatus = null; 
		$this->_iDtUpdated = null; 
		$this->_iDtAdded = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiScheduleId()
	{
		return $this->_iScheduleId;
	}

	public function getiDistributorId()
	{
		return $this->_iDistributorId;
	}

	public function getiRetailerId()
	{
		return $this->_iRetailerId;
	}

	public function getiEmployeeId()
	{
		return $this->_iEmployeeId;
	}

	public function getiUserRoleId()
	{
		return $this->_iUserRoleId;
	}

	public function geteSchedule()
	{
		return $this->_eSchedule;
	}

	public function gettDays()
	{
		return $this->_tDays;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}

	public function getiDtUpdated()
	{
		return $this->_iDtUpdated;
	}

	public function getiDtAdded()
	{
		return $this->_iDtAdded;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiScheduleId($val)
	{
		 $this->_iScheduleId =  $val;
	}

	public function setiDistributorId($val)
	{
		 $this->_iDistributorId =  $val;
	}

	public function setiRetailerId($val)
	{
		 $this->_iRetailerId =  $val;
	}

	public function setiEmployeeId($val)
	{
		 $this->_iEmployeeId =  $val;
	}

	public function setiUserRoleId($val)
	{
		 $this->_iUserRoleId =  $val;
	}

	public function seteSchedule($val)
	{
		 $this->_eSchedule =  $val;
	}

	public function settDays($val)
	{
		 $this->_tDays =  $val;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}

	public function setiDtUpdated($val)
	{
		 $this->_iDtUpdated =  $val;
	}

	public function setiDtAdded($val)
	{
		 $this->_iDtAdded =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM schedule WHERE iScheduleId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iScheduleId = $row[0]['iScheduleId'];
		 $this->_iDistributorId = $row[0]['iDistributorId'];
		 $this->_iRetailerId = $row[0]['iRetailerId'];
		 $this->_iEmployeeId = $row[0]['iEmployeeId'];
		 $this->_iUserRoleId = $row[0]['iUserRoleId'];
		 $this->_eSchedule = $row[0]['eSchedule'];
		 $this->_tDays = $row[0]['tDays'];
		 $this->_eStatus = $row[0]['eStatus'];
		 $this->_iDtUpdated = $row[0]['iDtUpdated'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
        return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM schedule WHERE iScheduleId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iScheduleId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO schedule ( iDistributorId,iRetailerId,iEmployeeId,iUserRoleId,eSchedule,tDays,eStatus,iDtUpdated,iDtAdded ) VALUES ( '".$this->_iDistributorId."','".$this->_iRetailerId."','".$this->_iEmployeeId."','".$this->_iUserRoleId."','".$this->_eSchedule."','".$this->_tDays."','".$this->_eStatus."','".$this->_iDtUpdated."','".$this->_iDtAdded."' )";
		 $result=$this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE schedule SET  iDistributorId = '".$this->_iDistributorId."' , iRetailerId = '".$this->_iRetailerId."' , iEmployeeId = '".$this->_iEmployeeId."' , iUserRoleId = '".$this->_iUserRoleId."' , eSchedule = '".$this->_eSchedule."' , tDays = '".$this->_tDays."' , eStatus = '".$this->_eStatus."' , iDtUpdated = '".$this->_iDtUpdated."' , iDtAdded = '".$this->_iDtAdded."'  WHERE iScheduleId = $id ";
		 $this->_obj->sql_query($sql);

	}

    function update_schedule($id)
    {
        $sql = " UPDATE schedule SET  iDistributorId = '".$this->_iDistributorId."' , iEmployeeId = '".$this->_iEmployeeId."' , iUserRoleId = '".$this->_iUserRoleId."' , eSchedule = '".$this->_eSchedule."' , tDays = '".$this->_tDays."' ,  iDtUpdated = '".$this->_iDtUpdated."'   WHERE iScheduleId = $id ";
        $this->_obj->sql_query($sql);
    }

    function get_schedule($id,$uid)
    {
        $sql =  "SELECT * FROM schedule WHERE iEmployeeId = $id AND iRetailerId='$uid'";
        $row =  $this->_obj->select($sql);
        return $row;

    }



}
