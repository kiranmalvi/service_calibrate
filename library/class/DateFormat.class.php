<?php

/*
*
* -------------------------------------------------------
* CLASSNAME:        DateFormat
* GENERATION DATE:  21.08.2006
* CLASS FILE:       /home/www1/calms/lib/classes/general/DateFormat.Class.php5
* FOR MYSQL DB:     calms
* -------------------------------------------------------
* AUTHOR:
* Ankit (WC)
* from: >> www.webcorsa.com 
* -------------------------------------------------------
*
*/

#require_once(dirname(__FILE__) . '/../../conf/paths.inc.php');
#require_once(dirname(__FILE__) . '/constants.locale.inc.php');

// Format Types
define('SHORT_TYPE', 1);
define('MEDIUM_TYPE', 2);
define('LONG_TYPE', 3);
define('FULL_TYPE', 4);
define('PREFERRED_TYPE', 5);
define('DEFAULT_TYPE', 6);
define('CREDIT_CARD_EXP_TYPE', 7);
define('DEFAULT_TIME_TYPE', 8);


// Formats. See strftime() documentation for format elements..
define('SHORT_FORMAT', '%m/%d/%y');
define('SHORT_FORMAT_DISPLAY', 'mm/dd/yy');
define('EUROPEAN_SHORT_FORMAT', '%d/%m/%y');
define('EUROPEAN_SHORT_FORMAT_DISPLAY', 'dd/mm/yy');
define('MEDIUM_FORMAT', '%b %d, %Y');
define('MEDIUM_FORMAT_DISPLAY', 'M dd, yyyy');
define('LONG_FORMAT', '%B %d, %Y');
define('LONG_FORMAT_DISPLAY', 'Mon dd, yyyy');
define('CREDIT_CARD_EXP_FORMAT', '%m/%y');
define('PREFERRED_FORMAT', '%x');
define('EUROPEAN_PREFERRED_FORMAT_DISPLAY', 'dd/mm/yyyy');
define('USA_PREFERRED_FORMAT_DISPLAY', 'mm/dd/yyyy');
define('DEFAULT_TIME_FORMAT', '%I:%M %p');


define('DB_SAFE_FORMAT', '%Y/%m/%d');
define('DB_DATE_TIME_SAFE_FORMAT', '%Y%m%d%H%M%S');


// Old stuff to be refactored.
define('DB_SAFE', 3);
//define('DB_SAFE_FORMAT', 'Y/m/d');
define('AMERICAN_DATE_FORMAT', 'mm-dd-yyyy');
define('AMERICAN_DATE_FORMAT_GMT', 'm/d/Y');
define('EUROPEAN_DATE_FORMAT', 'dd-mm-yyyy');
define('EUROPEAN_DATE_FORMAT_GMT', 'Y/m/d');
define('YEAR_FIRST_DATE_FORMAT', 'yyyy-dd-mm');
define('YEAR_FIRST_DATE_FORMAT_GMT', 'Y/d/m');


/**
 * This class formats Date objects by the given format. It can use a desired locale.
 */
class DateFormat
{
    var $format = null;
    var $formatType = null;
    var $locale = null;

    /**
     * There are two ways to create a DateFormat class.
     *
     *  <ol>
     *      <li>$format is one of the defined constants. Each type represents a
     * predetermined format for the given locale.
     *      <li>$format is a string representing a strftime() format.
     *
     * If locale is null, uses the environment's locale.
     */
    function DateFormat($format, $locale = null, $formatType = null)
    {
        global $config;

        if (is_integer($format)) {
            $this->setFormatType($format);
        } else {
            if (null == $formatType) {
                $formatType = DEFAULT_TYPE;
            }

            $this->setFormatType($formatType);
            $this->format = (!empty($format) && is_string($format)) ? $format : SHORT_FORMAT;
        }


        if (isset($locale)) {
            $this->setLocale($locale);
        } else {
            $this->setLocale($config->getValue('locale'));
        }
    }

    function setLocale($locale)
    {
        global $config;

        if (null == $locale) {
            $locale = $config->getValue('locale');
        }

        $this->locale = $locale;
    }

    /**
     * Formats the give date.
     *
     * @param $date -- A Date object to be formatted.
     * @public
     */
    function format($date)
    {
        if (null != $date) {
            setlocale(LC_ALL, $this->locale);
            $retString = strftime($this->getFormat(), $date->getTime());
            setlocale(LC_ALL, '');
            return $retString;
        } else {
            return null;
        }
    }

    /**
     * Determines the correct format for the given locale and format type.
     *
     * @protected
     */
    function getFormat($display = false)
    {
        switch ($this->locale) {
            case LOCALE_FRANCE:
            case LOCALE_GERMANY:
            case LOCALE_CANADA_FRENCH:
            case LOCALE_UK:
            case LOCALE_NORWAY:
                switch ($this->formatType) {
                    case SHORT_TYPE:
                        return EUROPEAN_SHORT_FORMAT;
                        break;
                    case PREFERRED_TYPE:
                        return ($display) ? EUROPEAN_PREFERRED_FORMAT_DISPLAY : PREFERRED_FORMAT;
                        break;
                    case CREDIT_CARD_EXP_TYPE:
                        return CREDIT_CARD_EXP_FORMAT;
                        break;
                    default:
                        return $this->format;
                        break;
                }

                break;
            case LOCALE_US:
            default:
                switch ($this->formatType) {
                    case SHORT_TYPE:
                        return ($display) ? SHORT_FORMAT_DISPLAY : SHORT_FORMAT;
                        break;
                    case MEDIUM_TYPE:
                        return ($display) ? MEDIUM_FORMAT_DISPLAY : MEDIUM_FORMAT;
                        break;
                    case LONG_TYPE:
                        return ($display) ? LONG_FORMAT_DISPLAY : LONG_FORMAT;
                        break;
                    case PREFERRED_TYPE:
                        return ($display) ? USA_PREFERRED_FORMAT_DISPLAY : PREFERRED_FORMAT;
                        break;
                    case CREDIT_CARD_EXP_TYPE:
                        return CREDIT_CARD_EXP_FORMAT;
                        break;
                    case DEFAULT_TIME_TYPE:
                        return DEFAULT_TIME_FORMAT;
                        break;
                    default:
                        return $this->format;
                        break;
                }

                break;
        }
    }

    function setFormat($format)
    {
        $this->format = $format;
    }

    function getFormatType()
    {
        return $this->formatType;
    }

    function setFormatType($formatType)
    {
        // TODO check that the format is one of the defined formats.
        $this->formatType = $formatType;
    }

    /**
     * Static method which returns a DateFormat object with the given format type. If locale
     * is null, uses the environmental locale.
     */
    function &getFormatInstance($formatType = PREFERRED_FORMAT, $locale = null)
    {
        return new DateFormat($formatType, $locale);
    }

    function &stringToDate($string, $format = null)
    {
        require_once(dirname(__FILE__) . '/Date.class.php');

        $formatter = DateFormat::getPreferredFormatInstance($format);

        if (EUROPEAN_PREFERRED_FORMAT_DISPLAY == $formatter->getDisplayFormat()) {
            // TODO This stuff is hard-coded. Better way to do this?
            $data = split('[/:-]', $string);

            $day = $data[0];
            $month = $data[1];
            $year = $data[2];

            if (!checkdate($month, $day, $year)) {
                return false;
            }

            return new Date($year, $month, $day);
        } elseif (USA_PREFERRED_FORMAT_DISPLAY == $formatter->getDisplayFormat()) {
            // TODO This stuff is hard-coded. Better way to do this?
            $data = split('[/:-]', $string);

            $month = $data[0];
            $day = $data[1];
            $year = $data[2];

            if (!checkdate($month, $day, $year)) {
                return false;
            }

            return new Date($year, $month, $day);
        } else {
            return new Date(strtotime($string));
        }
    }

    /**
     * Static method which returns a DateFormat object with the preferred format set
     * for the given locale by default. If locale is null, uses the environmental locale.
     */
    function &getPreferredFormatInstance($locale = null)
    {
        return new DateFormat(PREFERRED_FORMAT, $locale, PREFERRED_TYPE);
    }

    function getDisplayFormat()
    {
        return $this->getFormat(true);
    }
}

//DateFormat::stringToTime('02/02/2004');
/*


require_once(dirname(__FILE__) . '/Date.class.php');
$today = new Date();
//$preferred = DateFormat::getPreferredFormatInstance(LOCALE_CANADA);
//print $preferred->format($today);

$format = DateFormat::getFormatInstance(CREDIT_CARD_EXP_TYPE);
print $format->format($today);
//$format = DateFormat::getFormatInstance(FULL_TYPE, LOCALE_FRANCE);
//print $format->format($today);

//$format = new DateFormat('mm/dd/yyyy');
//$format->format($today);
*/
?>