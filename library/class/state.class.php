<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    state
 * DATE:         06.04.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/state.class.php
 * TABLE:        state
 * DB:           mind_service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class state
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iStateId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iStateId;
    protected $_iCountryId;
    protected $_vName;
    protected $_iDtAdded;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iStateId = null;
        $this->_iCountryId = null;
        $this->_vName = null;
        $this->_iDtAdded = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiStateId()
    {
        return $this->_iStateId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiStateId($val)
    {
        $this->_iStateId = $val;
    }

    public function getiCountryId()
    {
        return $this->_iCountryId;
    }

    public function setiCountryId($val)
    {
        $this->_iCountryId = $val;
    }

    public function getvName()
    {
        return $this->_vName;
    }

    public function setvName($val)
    {
        $this->_vName = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $WHERE = "WHERE 1=1";
        if (!is_null($id)) {
            $WHERE .= ' AND iStateId = ' . $id;
        }

        $sql = "SELECT * FROM state $WHERE ORDER BY vName ASC";
        $row = $this->_obj->select($sql);

        $this->_iStateId = $row[0]['iStateId'];
        $this->_iCountryId = $row[0]['iCountryId'];
        $this->_vName = $row[0]['vName'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_eStatus = $row[0]['eStatus'];
        return $row;
    }

    function select_with_country($id = null)
    {
        $WHERE = "WHERE 1=1";
        if (!is_null($id)) {
            $WHERE .= ' AND iStateId = ' . $id;
        }

        $sql = "SELECT st.*,co.vName as country FROM state st LEFT JOIN country co ON co.iCountryId = st.iCountryId $WHERE ORDER BY st.iStateId DESC";
        $row = $this->_obj->select($sql);

        return $row;
    }

    function select_on_country()
    {
        $sql = "SELECT iStateId,vName FROM state WHERE iCountryId = " . $this->_iCountryId;
        $row = $this->_obj->select($sql);
        return $row;
    }

    function select_on_country_API($id)
    {
        $sql = "SELECT iStateId,vName FROM state WHERE iCountryId = " . $id;
        $row = $this->_obj->select($sql);
        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM state WHERE iStateId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iStateId = ""; // clear key for autoincrement

     echo   $sql = "INSERT INTO state ( iCountryId,vName,iDtAdded,eStatus ) VALUES ( '" . $this->_iCountryId . "','" . $this->_vName . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {
        $sql = " UPDATE state SET ";
        $sql .= ($this->_iCountryId != null) ? ", iCountryId = '" . $this->_iCountryId . "'," : "";
        $sql .= ($this->_vName != null) ? ", vName = '" . $this->_vName . "'," : "";
        $sql .= ($this->_iDtAdded != null) ? ", iDtAdded = '" . $this->_iDtAdded . "'," : "";
        $sql .= ($this->_eStatus != null) ? ",eStatus = '" . $this->_eStatus . "' ," : "";
        $sql .= "WHERE iStateId = $id ";

        $UPDATE = clean_sql($sql);
        $this->_obj->sql_query($UPDATE);
    }


}
