<?php
/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    redeem
 * DATE:         26.05.2015
 * CLASS FILE:   /var/www/service_calibrate/library/class/class-making/generated_classes/redeem.class.php
 * TABLE:        redeem
 * DB:           mind_service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */

class redeem
{


    /**
     *   @desc Variable Declaration with default value
     */

    protected $iRedeemId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iRedeemId;
    protected $_iUserId;
    protected $_iCardId;
    protected $_vPoints;
    protected $_eStatus;
    protected $_iDtAdded;
    protected $_iUpdated;
    protected $_eAccept;



    /**
     *   @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iRedeemId = null;
        $this->_iUserId = null;
        $this->_iCardId = null;
        $this->_vPoints = null;
        $this->_eStatus = null;
        $this->_iDtAdded = null;
        $this->_iUpdated = null;
        $this->_eAccept = null;
    }

    /**
     *   @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }



    /**
     *   @desc   GETTER METHODS
     */


    public function getiRedeemId()
    {
        return $this->_iRedeemId;
    }

    public function getiUserId()
    {
        return $this->_iUserId;
    }

    public function getiCardId()
    {
        return $this->_iCardId;
    }

    public function getvPoints()
    {
        return $this->_vPoints;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function getiUpdated()
    {
        return $this->_iUpdated;
    }

    public function geteAccept()
    {
        return $this->_eAccept;
    }


    /**
     *   @desc   SETTER METHODS
     */


    public function setiRedeemId($val)
    {
        $this->_iRedeemId =  $val;
    }

    public function setiUserId($val)
    {
        $this->_iUserId =  $val;
    }

    public function setiCardId($val)
    {
        $this->_iCardId =  $val;
    }

    public function setvPoints($val)
    {
        $this->_vPoints =  $val;
    }

    public function seteStatus($val)
    {
        $this->_eStatus =  $val;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded =  $val;
    }

    public function setiUpdated($val)
    {
        $this->_iUpdated =  $val;
    }

    public function seteAccept($val)
    {
        $this->_eAccept =  $val;
    }


    /**
     *   @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql =  "SELECT * FROM redeem WHERE iRedeemId = $id";
        $row =  $this->_obj->select($sql);

        $this->_iRedeemId = $row[0]['iRedeemId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_iCardId = $row[0]['iCardId'];
        $this->_vPoints = $row[0]['vPoints'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_iUpdated = $row[0]['iUpdated'];
        $this->_eAccept = $row[0]['eAccept'];
    }

    function redeem_userlist()
    {

       $sql = "SELECT r.*, u.vStoreUniqueId, rf.vPoint as point,g.vGift_CardName,r.vPoints FROM `redeem` r left join user u on u.`iUserId` = r.`iUserId` inner join gift_card g on g.iCardId = r.iCardId inner join referral rf on rf.iUserId= u.iUserId ORDER by iRedeemId Desc";

        $row =  $this->_obj->select($sql);

        $this->_iRedeemId = $row[0]['iRedeemId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_iCardId = $row[0]['iCardId'];
        $this->_vPoints = $row[0]['vPoints'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_iUpdated = $row[0]['iUpdated'];
        $this->_eAccept = $row[0]['eAccept'];
        return $row;
    }


    /**
     *   @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM redeem WHERE iRedeemId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     *   @desc   INSERT
     */

    function insert()
    {
        $this->iRedeemId = ""; // clear key for autoincrement

        $sql = "INSERT INTO redeem ( iUserId,iCardId,vPoints,eStatus,iDtAdded,iUpdated,eAccept ) VALUES ( '".$this->_iUserId."','".$this->_iCardId."','".$this->_vPoints."','".$this->_eStatus."','".$this->_iDtAdded."','".$this->_iUpdated."','".$this->_eAccept."' )";
        $result=$this->_obj->insert($sql);
        return $result;
    }


    /**
     *   @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE redeem SET  iUserId = '".$this->_iUserId."' , iCardId = '".$this->_iCardId."' , vPoints = '".$this->_vPoints."' , eStatus = '".$this->_eStatus."' , iDtAdded = '".$this->_iDtAdded."' , iUpdated = '".$this->_iUpdated."' , eAccept = '".$this->_eAccept."'  WHERE iRedeemId = $id ";
        $this->_obj->sql_query($sql);

    }


}
