<?php

/*
*
* -------------------------------------------------------
* CLASSNAME:        clientmailservice
* GENERATION DATE:  27.05.2011
* CLASS FILE:       /home/www/ufs/library/class/email.class.php
* FOR MYSQL TABLE:  clientemailservice
* FOR MYSQL DB:     ufs
* -------------------------------------------------------
* AUTHOR:
* Rocky (WC)
* from: >> www.webcorsa.com 
* -------------------------------------------------------
*
*/

Class Email
{
    function __construct()
    {
        global $obj, $TableObj;
        $this->obj = $obj;
        $this->_TableObj = $TableObj;
    }

    function __destruct()
    {
        unset($this->obj);
    }

    function func_User_Verification_Email($userid, $vType = "User_Verification_Link_Email")
    {
        global $site_url, $obj, $EMAIL_ADMIN, $EMAIL_FOOTER, $SITE_TITLE;

        $sql_email_format = "SELECT * FROM email_format WHERE vEmailFormatType ='" . $vType . "'";
        $db_email_format = $obj->select($sql_email_format);
        //pr($db_email_format);exit;
        $sql_user = "SELECT vFirstName,vLastName, vEmailId FROM register WHERE iUserId='" . $userid . "'";
        $db_user = $obj->select($sql_user);

        $vFromEmail = $EMAIL_ADMIN;
        $subject = $db_email_format[0]['vEmailFormatSubject'];
        $tContent = stripslashes($db_email_format[0]["tEmailFormatDesc"]);
        $vToEmail = $db_user[0]['vEmailId'];
        //$vToEmail	= "kinjal.mindinventory@gmail.com";
//        $vToName = $db_user[0]['vFirstName'] . ' ' . $db_user[0]['vLastName'];

        //	$encrypt_user_id = $encryptdcryptobj->encrypt_decrypt($userid, 1);
        //$login = $site_url."register";
        #echo($verification_link);exit;
        $findarray = array('#SITE_NAME#', '#EMAIL_FOOTER#');
        $replacearray = array($SITE_TITLE, $EMAIL_FOOTER);

        $tContent = str_replace($findarray, $replacearray, $tContent);


        //   $tContent   =   "";


        $get_Body = $this->mail_format($tContent);
        $headers = $this->mail_header($vFromEmail);

        #if($eVerified == 'Yes')
        // Calling Email sending function from here.
        //mail_mailMe($vToEmail, $subject, $tContent, $vFromEmail, $format="html", $cc="", $bcc="");
        $status = $this->mail_mailMe($vToEmail, $subject, $get_Body, $headers);

        return $status;
    }

    function mail_format($tMessage)
    {
        global $admin_url, $site_url;
        $logo = '';
        $tMessage .= "<style>
					.tableborder {border:1px solid #DEE05C;}
					.font-text { font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:normal; color:#000000; text-decoration:none;}
					.coffeelink {font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; color:#DEE05C; font-weight:normal; text-decoration:none;}
					</style>";
        $logo .= '<table width="550px" border="0" colspan="2" class="tableborder" cellpadding="4"><tr><td><img src="' . $site_url . 'images/Icon-Small@3x.png" height=70px; width=70px;></td></tr><tr><td height="20">&nbsp;</td></tr>';
        $tMessage = $logo . '<tr><td class="font-text">' . $tMessage . '</td></tr></table>';

        return $tMessage;
    }

    function mail_header($Email)
    {
        global $COMPANY_NAME, $GENERAL_EMAIL_ADMIN, $OPT_IN_EMAIL_ADDRESS;

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: " . $COMPANY_NAME . ' <' . $OPT_IN_EMAIL_ADDRESS . '>' . "\n" . "Reply-To: " . $OPT_IN_EMAIL_ADDRESS . "\n";
        return $headers;
    }

    //SENDING VERIFICATION LINK TO REGISTER USER

    function mail_mailMe($to, $subject, $vBody, $headers, $format = "", $cc = "", $bcc = "info@kukdu.com")
    {
        global $site_path, $debug_mode, $SENDING_EMAIL_THROUGH_FUNCTION, $SMTP_URL, $SMTP_PORT;
        if (strlen($format) == 0)
            $format = "text/html";
        $status = 0;
        $cnt = "";
        $cnt .= "====================================================================\n\n<br>";
        $cnt .= date("F d, Y, H:i:s T");
        $cnt .= "====================================================================\n\n<br>";
        $cnt .= $_SERVER['REMOTE_ADDR'];
        $cnt .= "====================================================================\n\n<br>";
        $cnt = "Headers : \n" . $headers . "\n";
        $cnt .= "<br>Date Time :" . date("d M, Y  h:i:s") . "\r\n";
        $cnt .= "To : " . $to . "\n";
        $cnt .= "Subject : " . $subject . "\n";
        $cnt .= "Body : \n" . $vBody . "\n";
        $cnt .= "====================================================================\n\n<br>";

        $filename = $site_path . "mail.html";
        if (!$fp = fopen($filename, 'a')) {
            print "Cannot open file ($filename)";
            exit;
        }
        if (!fwrite($fp, $cnt)) {
            print "Cannot write to file ($filename)";
            exit;
        }
        fclose($fp);
        $status = true;

        if ($_SERVER['HTTP_HOST'] != "192.168.1.26") {
            $status = @mail($to, $subject, $vBody, $headers);
        }
        return $status;
    }

    function func_user_forgot_pass($vEmail, $token, $vType = "USER_FORGOT_PASSWORD_EMAIL")
    {
        global $site_url, $obj, $EMAIL_FOOTER, $OPT_IN_EMAIL_ADDRESS;

        $str = "select vEmailFormatSubject, tEmailFormatDesc from email_format where vEmailFormatType ='" . $vType . "'";
        $db_str = $obj->select($str);

        $str_user = "select u.iUserId, u.vEmail,CONCAT(u.vFirstName,' ',u.vLastName )As FullName from user AS u
                      where u.vEmail ='" . $vEmail . "'";
        $db_user = $obj->select($str_user);
        //  echo "<pre>";print_r($db_user);exit;
        $vFromEmail = $OPT_IN_EMAIL_ADDRESS;
        $subject = $db_str[0]["vEmailFormatSubject"];
        $tContent = stripslashes($db_str[0]["tEmailFormatDesc"]);
        $id = $db_user[0]['iUserId'];
        $username = $db_user[0]['FullName'];
        $vToEmail = $db_user[0]['vEmail'];
        $vPassword = $token;

        //  $fplink = '<br/><a href="'.$site_url.'resetpasswrd.php?t='.$vtempFP.'&i='.$id.'" target="_blank">Click Here</a> to reset your Password<br/><br/> OR <br/><br/> Copy and paste this URL: <br/>'.$site_url.'resetpasswrd.php?t='.$vtempFP.'&i='.$id;

        $findarray = array('#NAME#', '#MAIL_FOOTER#', '#CODE#');
        $replacearray = array($username, $EMAIL_FOOTER, $vPassword);
        $tContent = str_replace($findarray, $replacearray, $tContent);

        $get_Body = $this->mail_format($tContent);


        $headers = $this->mail_header($vFromEmail);
        $status = $this->mail_mailMe($vToEmail, $subject, $get_Body, $headers);
        return $status;
    }

    //Admin Forget Password
    function func_user_forget_pass($vType = "User_Forget_Password_Request", $emailadd)
    {
        global $site_url, $obj, $COMPANY_NAME, $EMAIL_FOOTER, $SITE_NAME;

        $str = "select vFrom,vSubject, vBody from email_format where vType ='" . $vType . "'";
        $db_str = $obj->select($str);

        $str = "select vfirstname,vlastname, vpassword from user where vemailaddress = '" . $emailadd . "'";
        $db_user = $obj->select($str);

        $vFromEmail = $db_str[0]["vFrom"];
        $subject = $db_str[0]["vSubject"];
        $tContent = stripslashes($db_str[0]["vBody"]);
        $FirstName = $db_user[0]['vfirstname'] . "&nbsp;" . $db_user[0]['vlastname'];
        $vToEmail = $emailadd;
        $vPassword = $db_user[0]['vpassword'];

        $findarray = array('#Name#', '#Email#', '#Password#', '#MailFooter#');
        $replacearray = array($FirstName, $vToEmail, $vPassword, $EMAIL_FOOTER);

        $tContent = str_replace($findarray, $replacearray, $tContent);

        $get_Body = $this->mail_format($tContent);
        $headers = $this->mail_header($vFromEmail);

        $status = $this->mail_mailMe($vToEmail, $subject, $get_Body, $headers);

        return $status;
    }

    function report_spam_mail($id, $vType = "REPORT_SPAM_EMAIL")
    {
        global $site_url, $obj, $EMAIL_FOOTER, $GENERAL_EMAIL_ADMIN, $OPT_IN_EMAIL_ADDRESS;

        $str = "select vEmailFormatSubject,tEmailFormatDesc from email_format where vEmailFormatType ='" . $vType . "'";
        $db_str = $obj->select($str);

        $all_str = " select f.eType, f.eFlagType, p.vPostName, r.vFirstName, r.vLastName from flags f left join post p on p.iPostId = f.iPostId left join register r on r.iUserId = f.iUserId where f.iFlagId = '$id' ";
        $db_all_str = $obj->select($all_str);

        $vFromEmail = $OPT_IN_EMAIL_ADDRESS;
        $subject = $db_str[0]['vEmailFormatSubject'];
        $tContent = stripslashes($db_str[0]['tEmailFormatDesc']);
        $vToEmail = $GENERAL_EMAIL_ADMIN;
        $posttitle = $db_all_str[0]['vPostName'];
        $posttype = $db_all_str[0]['eType'];
        $user = $db_all_str[0]['vFirstName'] . ' ' . $db_all_str[0]['vLastName'];
        $spamtype = '';

        if ($db_all_str[0]['eFlagType'] == '0') {
            $spamtype = 'Just a spammy post';
        } elseif ($db_all_str[0]['eFlagType'] == '1') {
            $spamtype = 'Their account is hacked';
        } elseif ($db_all_str[0]['eFlagType'] == '2') {
            $spamtype = 'This is fake account';
        }


        $findarray = array('#POST_TITLE#', '#POST_TYPE#', '#SPAMTYPE#', '#COMMENTED_USER#', '#MAIL_FOOTER#');
        $replacearray = array($posttitle, $posttype, $spamtype, $user, $EMAIL_FOOTER);

        $tContent = str_replace($findarray, $replacearray, $tContent);

        $get_Body = $this->mail_format($tContent);
        $headers = $this->mail_header($vFromEmail);

        $status = $this->mail_mailMe($vToEmail, $subject, $get_Body, $headers);

        return $status;
    }


    function func_user_request_university($uid, $uniid, $vType = "NEW_UNIVERSITY")
    {
        global $site_url, $obj, $EMAIL_FOOTER, $OPT_IN_EMAIL_ADDRESS;

        $str = "select vEmailFormatSubject, tEmailFormatDesc from email_format where vEmailFormatType ='" . $vType . "'";
        $db_str = $obj->select($str);

        $str_user = "select r.iUserId, r.vEmailId, r.vFirstName,u.vUniversityName,u.iRequestBy,u.iUniversityId from register as r LEFT JOIN university as u ON r.iUserId = u.iRequestBy where r.iUserId='$uid' AND u.iUniversityId='$uniid'  ";
        $db_user = $obj->select($str_user);

        $vFromEmail = $OPT_IN_EMAIL_ADDRESS;
        $subject = $db_str[0]["vEmailFormatSubject"];
        $tContent = stripslashes($db_str[0]["tEmailFormatDesc"]);
        $id = $db_user[0]['iUserId'];
        $username = $db_user[0]['vFirstName'];
        $vUniversityName = $db_user[0]['vUniversityName'];
        $vToEmail = $db_user[0]['vEmailId'];


        //  $fplink = '<br/><a href="'.$site_url.'resetpasswrd.php?t='.$vtempFP.'&i='.$id.'" target="_blank">Click Here</a> to reset your Password<br/><br/> OR <br/><br/> Copy and paste this URL: <br/>'.$site_url.'resetpasswrd.php?t='.$vtempFP.'&i='.$id;

        $findarray = array('#MAIL_FOOTER#', '#UNIVERSITY_NAME#', '#USER#');
        $replacearray = array($EMAIL_FOOTER, $vUniversityName, $username);
        $tContent = str_replace($findarray, $replacearray, $tContent);

        $get_Body = $this->mail_format($tContent);


        $headers = $this->mail_header($vFromEmail);
        $status = $this->mail_mailMe($vToEmail, $subject, $get_Body, $headers);
        return $status;
    }

    function func_admin_approve_university($uid, $uniid, $uniname, $vType = "UNIVERSITY_APPROVED")
    {

        global $site_url, $obj, $EMAIL_FOOTER, $OPT_IN_EMAIL_ADDRESS;

        $str = "select vEmailFormatSubject, tEmailFormatDesc from email_format where vEmailFormatType ='" . $vType . "'";
        $db_str = $obj->select($str);

        $str_user = "select iUserId, vEmailId, vFirstName, vLastName from register where iUserId IN ($uid) ";

        $db_user = $obj->select($str_user);
// pr($db_user);

        /* if(count($db_user) > 0)
         {
             for($i=0; $i<count($db_user); $i++)
             {*/
        $vFromEmail = $OPT_IN_EMAIL_ADDRESS;
        $subject = $db_str[0]["vEmailFormatSubject"];
        $tContent = stripslashes($db_str[0]["tEmailFormatDesc"]);
        $id = $db_user[0]['iUserId'];
        $username = $db_user[0]['vFirstName'] . ' ' . $db_user[0]['vLastName'];
        $vUniversityName = $uniname;
        $vToEmail = $db_user[0]['vEmailId'];

        $findarray = array('#USERNAME#', '#MAIL_FOOTER#', '#UNIVERSITY#');
        $replacearray = array($username, $EMAIL_FOOTER, $vUniversityName);
        $tContent = str_replace($findarray, $replacearray, $tContent);

        $get_Body = $this->mail_format($tContent);

        $headers = $this->mail_header($vFromEmail);
        $status = $this->mail_mailMe($vToEmail, $subject, $get_Body, $headers);

        //    pr($status);

        /* }
     }*/

        return $status;
    }
}
