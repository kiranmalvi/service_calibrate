<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    user_settings
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/user_settings.class.php
 * TABLE:        user_settings
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class user_settings
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iUserSettingsId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iUserSettingsId;
    protected $_iUserId;
    protected $_iCustomerId;
    protected $_iDtLogin;
    protected $_eDeviceType;
    protected $_vLastLoginIP;
    protected $_eNotification;
    protected $_vUserFilter;

    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iUserSettingsId = null;
        $this->_iUserId = null;
        $this->_iCustomerId = null;
        $this->_iDtLogin = null;
        $this->_eDeviceType = null;
        $this->_vLastLoginIP = null;
        $this->_eNotification = null;
        $this->_vUserFilter = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiUserSettingsId()
    {
        return $this->_iUserSettingsId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiUserSettingsId($val)
    {
        $this->_iUserSettingsId = $val;
    }

    public function getiUserId()
    {
        return $this->_iUserId;
    }

    public function setiUserId($val)
    {
        $this->_iUserId = $val;
    }

    public function getiDtLogin()
    {
        return $this->_iDtLogin;
    }

    public function setiDtLogin($val)
    {
        $this->_iDtLogin = $val;
    }

    public function geteDeviceType()
    {
        return $this->_eDeviceType;
    }

    public function seteDeviceType($val)
    {
        $this->_eDeviceType = $val;
    }

    public function getvUserFilter()
    {
        return $this->_vUserFilter;
    }

    public function setvUserFilter($val)
    {
        $this->_vUserFilter = $val;
    }
    public function getvLastLoginIP()
    {
        return $this->_vLastLoginIP;
    }

    public function setvLastLoginIP($val)
    {
        $this->_vLastLoginIP =  $val;
    }

    public function geteNotification()
    {
        return $this->_eNotification;
    }

    public function seteNotification($val)
    {
        $this->_eNotification =  $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM user_settings WHERE iUserSettingsId = $id";
        $row = $this->_obj->select($sql);

        $this->_iUserSettingsId = $row[0]['iUserSettingsId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_iDtLogin = $row[0]['iDtLogin'];
        $this->_eDeviceType = $row[0]['eDeviceType'];
        $this->_vLastLoginIP = $row[0]['vLastLoginIP'];
        $this->_eNotification = $row[0]['eNotification'];
        $this->_vUserFilter = $row[0]['vUserFilter'];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM user_settings WHERE iUserSettingsId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */
    function insert()
    {
        $this->iUserSettingsId = ""; // clear key for autoincrement

        $sql = "INSERT INTO user_settings ( iUserId,iCustomerId,iDtLogin,eDeviceType,vLastLoginIP,eNotification,vUserFilter ) VALUES ( '".$this->_iUserId."','".$this->_iCustomerId."','".$this->_iDtLogin."','".$this->_eDeviceType."','".$this->_vLastLoginIP."','".$this->_eNotification."','".$this->_vUserFilter."' )";
        $result=$this->_obj->insert($sql);
        return $result;
    }



    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE user_settings SET  iUserId = '" . $this->_iUserId . "' , iDtLogin = '" . $this->_iDtLogin . "' , eDeviceType = '" . $this->_eDeviceType . "' , vUserFilter = '" . $this->_vUserFilter . "'  WHERE iUserSettingsId = $id ";
        $this->_obj->sql_query($sql);

    }

    /**
     * @return null
     */
    public function getICustomerId()
    {
        return $this->_iCustomerId;
    }

    /**
     * @param null $iCustomerId
     */
    public function setICustomerId($iCustomerId)
    {
        $this->_iCustomerId = $iCustomerId;
    }

    function update_customerID($userID)
    {
        $sql = "UPDATE user_settings SET  iCustomerId = '" . $this->_iCustomerId . "' WHERE iUserId = " . $userID;
        $this->_obj->sql_query($sql);
    }

    function select_user_setting($id)
    {
        $sql = "SELECT * FROM user_settings WHERE iUserId = $id";
        $row = $this->_obj->select($sql);

        $this->_iUserSettingsId = $row[0]['iUserSettingsId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_iDtLogin = $row[0]['iDtLogin'];
        $this->_eDeviceType = $row[0]['eDeviceType'];
        $this->_vUserFilter = $row[0]['vUserFilter'];
        $this->_vLastLoginIP = $row[0]['vLastLoginIP'];
        $this->_eNotification = $row[0]['eNotification'];
        return $row;
    }

    function update_notification($id)
    {
        $sql = "UPDATE user_settings SET  eNotification = '" . $this->_eNotification . "' WHERE iUserId = " . $id;
        $this->_obj->sql_query($sql);
    }
     function checkCustomerId($id)
    {
         $sql = "SELECT * FROM user_settings WHERE iUserId = '$id' AND iCustomerId !=''";
        $row = $this->_obj->select($sql);
        return $row;
    }

}
