<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    user_mapping
* DATE:         06.07.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/user_mapping.class.php
* TABLE:        user_mapping
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class user_mapping
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iUser_mappingId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iUser_mappingId;  
	protected $_iUserId;  
	protected $_iUserLinkId;  
	protected $_iUserLinkedSubId;  
	protected $_iDtAdded;  
	protected $_iDtUpdated;  
	protected $_eType;  
	protected $_eStatus;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iUser_mappingId = null; 
		$this->_iUserId = null; 
		$this->_iUserLinkId = null; 
		$this->_iUserLinkedSubId = null; 
		$this->_iDtAdded = null; 
		$this->_iDtUpdated = null; 
		$this->_eType = null; 
		$this->_eStatus = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiUser_mappingId()
	{
		return $this->_iUser_mappingId;
	}

	public function getiUserId()
	{
		return $this->_iUserId;
	}

	public function getiUserLinkId()
	{
		return $this->_iUserLinkId;
	}

	public function getiUserLinkedSubId()
	{
		return $this->_iUserLinkedSubId;
	}

	public function getiDtAdded()
	{
		return $this->_iDtAdded;
	}

	public function getiDtUpdated()
	{
		return $this->_iDtUpdated;
	}

	public function geteType()
	{
		return $this->_eType;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiUser_mappingId($val)
	{
		 $this->_iUser_mappingId =  $val;
	}

	public function setiUserId($val)
	{
		 $this->_iUserId =  $val;
	}

	public function setiUserLinkId($val)
	{
		 $this->_iUserLinkId =  $val;
	}

	public function setiUserLinkedSubId($val)
	{
		 $this->_iUserLinkedSubId =  $val;
	}

	public function setiDtAdded($val)
	{
		 $this->_iDtAdded =  $val;
	}

	public function setiDtUpdated($val)
	{
		 $this->_iDtUpdated =  $val;
	}

	public function seteType($val)
	{
		 $this->_eType =  $val;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM user_mapping WHERE iUser_mappingId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iUser_mappingId = $row[0]['iUser_mappingId'];
		 $this->_iUserId = $row[0]['iUserId'];
		 $this->_iUserLinkId = $row[0]['iUserLinkId'];
		 $this->_iUserLinkedSubId = $row[0]['iUserLinkedSubId'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
		 $this->_iDtUpdated = $row[0]['iDtUpdated'];
		 $this->_eType = $row[0]['eType'];
		 $this->_eStatus = $row[0]['eStatus'];
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM user_mapping WHERE iUser_mappingId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iUser_mappingId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO user_mapping ( iUserId,iUserLinkId,iUserLinkedSubId,iDtAdded,iDtUpdated,eType,eStatus ) VALUES ( '".$this->_iUserId."','".$this->_iUserLinkId."','".$this->_iUserLinkedSubId."','".$this->_iDtAdded."','".$this->_iDtUpdated."','".$this->_eType."','".$this->_eStatus."' )";
		 $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE user_mapping SET  iUserId = '".$this->_iUserId."' , iUserLinkId = '".$this->_iUserLinkId."' , iUserLinkedSubId = '".$this->_iUserLinkedSubId."' , iDtAdded = '".$this->_iDtAdded."' , iDtUpdated = '".$this->_iDtUpdated."' , eType = '".$this->_eType."' , eStatus = '".$this->_eStatus."'  WHERE iUser_mappingId = $id ";
		 $this->_obj->sql_query($sql);

	}


}
