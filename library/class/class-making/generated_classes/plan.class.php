<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    plan
* DATE:         04.07.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/plan.class.php
* TABLE:        plan
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class plan
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iPlanId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iPlanId;  
	protected $_vPlanName;  
	protected $_iFromStore;  
	protected $_iToStore;  
	protected $_vDescription;  
	protected $_fDayPrice;  
	protected $_fMonthPrice;  
	protected $_iStartDate;  
	protected $_iEndDate;  
	protected $_eUSerType;  
	protected $_iDtAdded;  
	protected $_eStatus;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iPlanId = null; 
		$this->_vPlanName = null; 
		$this->_iFromStore = null; 
		$this->_iToStore = null; 
		$this->_vDescription = null; 
		$this->_fDayPrice = null; 
		$this->_fMonthPrice = null; 
		$this->_iStartDate = null; 
		$this->_iEndDate = null; 
		$this->_eUSerType = null; 
		$this->_iDtAdded = null; 
		$this->_eStatus = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiPlanId()
	{
		return $this->_iPlanId;
	}

	public function getvPlanName()
	{
		return $this->_vPlanName;
	}

	public function getiFromStore()
	{
		return $this->_iFromStore;
	}

	public function getiToStore()
	{
		return $this->_iToStore;
	}

	public function getvDescription()
	{
		return $this->_vDescription;
	}

	public function getfDayPrice()
	{
		return $this->_fDayPrice;
	}

	public function getfMonthPrice()
	{
		return $this->_fMonthPrice;
	}

	public function getiStartDate()
	{
		return $this->_iStartDate;
	}

	public function getiEndDate()
	{
		return $this->_iEndDate;
	}

	public function geteUSerType()
	{
		return $this->_eUSerType;
	}

	public function getiDtAdded()
	{
		return $this->_iDtAdded;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiPlanId($val)
	{
		 $this->_iPlanId =  $val;
	}

	public function setvPlanName($val)
	{
		 $this->_vPlanName =  $val;
	}

	public function setiFromStore($val)
	{
		 $this->_iFromStore =  $val;
	}

	public function setiToStore($val)
	{
		 $this->_iToStore =  $val;
	}

	public function setvDescription($val)
	{
		 $this->_vDescription =  $val;
	}

	public function setfDayPrice($val)
	{
		 $this->_fDayPrice =  $val;
	}

	public function setfMonthPrice($val)
	{
		 $this->_fMonthPrice =  $val;
	}

	public function setiStartDate($val)
	{
		 $this->_iStartDate =  $val;
	}

	public function setiEndDate($val)
	{
		 $this->_iEndDate =  $val;
	}

	public function seteUSerType($val)
	{
		 $this->_eUSerType =  $val;
	}

	public function setiDtAdded($val)
	{
		 $this->_iDtAdded =  $val;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM plan WHERE iPlanId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iPlanId = $row[0]['iPlanId'];
		 $this->_vPlanName = $row[0]['vPlanName'];
		 $this->_iFromStore = $row[0]['iFromStore'];
		 $this->_iToStore = $row[0]['iToStore'];
		 $this->_vDescription = $row[0]['vDescription'];
		 $this->_fDayPrice = $row[0]['fDayPrice'];
		 $this->_fMonthPrice = $row[0]['fMonthPrice'];
		 $this->_iStartDate = $row[0]['iStartDate'];
		 $this->_iEndDate = $row[0]['iEndDate'];
		 $this->_eUSerType = $row[0]['eUSerType'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
		 $this->_eStatus = $row[0]['eStatus'];
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM plan WHERE iPlanId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iPlanId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO plan ( vPlanName,iFromStore,iToStore,vDescription,fDayPrice,fMonthPrice,iStartDate,iEndDate,eUSerType,iDtAdded,eStatus ) VALUES ( '".$this->_vPlanName."','".$this->_iFromStore."','".$this->_iToStore."','".$this->_vDescription."','".$this->_fDayPrice."','".$this->_fMonthPrice."','".$this->_iStartDate."','".$this->_iEndDate."','".$this->_eUSerType."','".$this->_iDtAdded."','".$this->_eStatus."' )";
		 $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE plan SET  vPlanName = '".$this->_vPlanName."' , iFromStore = '".$this->_iFromStore."' , iToStore = '".$this->_iToStore."' , vDescription = '".$this->_vDescription."' , fDayPrice = '".$this->_fDayPrice."' , fMonthPrice = '".$this->_fMonthPrice."' , iStartDate = '".$this->_iStartDate."' , iEndDate = '".$this->_iEndDate."' , eUSerType = '".$this->_eUSerType."' , iDtAdded = '".$this->_iDtAdded."' , eStatus = '".$this->_eStatus."'  WHERE iPlanId = $id ";
		 $this->_obj->sql_query($sql);

	}


}
