<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    promotion_right
* DATE:         22.07.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/promotion_right.class.php
* TABLE:        promotion_right
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class promotion_right
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iPromorightId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iPromorightId;  
	protected $_iPromoIUserId;  
	protected $_iPromoadminId;  
	protected $_iCityId;  
	protected $_iDtAdded;  
	protected $_iDtUpdated;  
	protected $_eStatus;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iPromorightId = null; 
		$this->_iPromoIUserId = null; 
		$this->_iPromoadminId = null; 
		$this->_iCityId = null; 
		$this->_iDtAdded = null; 
		$this->_iDtUpdated = null; 
		$this->_eStatus = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiPromorightId()
	{
		return $this->_iPromorightId;
	}

	public function getiPromoIUserId()
	{
		return $this->_iPromoIUserId;
	}

	public function getiPromoadminId()
	{
		return $this->_iPromoadminId;
	}

	public function getiCityId()
	{
		return $this->_iCityId;
	}

	public function getiDtAdded()
	{
		return $this->_iDtAdded;
	}

	public function getiDtUpdated()
	{
		return $this->_iDtUpdated;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiPromorightId($val)
	{
		 $this->_iPromorightId =  $val;
	}

	public function setiPromoIUserId($val)
	{
		 $this->_iPromoIUserId =  $val;
	}

	public function setiPromoadminId($val)
	{
		 $this->_iPromoadminId =  $val;
	}

	public function setiCityId($val)
	{
		 $this->_iCityId =  $val;
	}

	public function setiDtAdded($val)
	{
		 $this->_iDtAdded =  $val;
	}

	public function setiDtUpdated($val)
	{
		 $this->_iDtUpdated =  $val;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM promotion_right WHERE iPromorightId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iPromorightId = $row[0]['iPromorightId'];
		 $this->_iPromoIUserId = $row[0]['iPromoIUserId'];
		 $this->_iPromoadminId = $row[0]['iPromoadminId'];
		 $this->_iCityId = $row[0]['iCityId'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
		 $this->_iDtUpdated = $row[0]['iDtUpdated'];
		 $this->_eStatus = $row[0]['eStatus'];
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM promotion_right WHERE iPromorightId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iPromorightId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO promotion_right ( iPromoIUserId,iPromoadminId,iCityId,iDtAdded,iDtUpdated,eStatus ) VALUES ( '".$this->_iPromoIUserId."','".$this->_iPromoadminId."','".$this->_iCityId."','".$this->_iDtAdded."','".$this->_iDtUpdated."','".$this->_eStatus."' )";
		 $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE promotion_right SET  iPromoIUserId = '".$this->_iPromoIUserId."' , iPromoadminId = '".$this->_iPromoadminId."' , iCityId = '".$this->_iCityId."' , iDtAdded = '".$this->_iDtAdded."' , iDtUpdated = '".$this->_iDtUpdated."' , eStatus = '".$this->_eStatus."'  WHERE iPromorightId = $id ";
		 $this->_obj->sql_query($sql);

	}


}
