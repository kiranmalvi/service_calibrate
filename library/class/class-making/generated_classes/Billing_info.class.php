<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    Billing_info
* DATE:         25.06.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/Billing_info.class.php
* TABLE:        Billing_info
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class Billing_info
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iBillinfo;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iBillinfo;  
	protected $_vAddress;  
	protected $_vCity;  
	protected $_vState;  
	protected $_vCountry;  
	protected $_vZip;  
	protected $_iUserId;  
	protected $_Extra1;  
	protected $_Extra2;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iBillinfo = null; 
		$this->_vAddress = null; 
		$this->_vCity = null; 
		$this->_vState = null; 
		$this->_vCountry = null; 
		$this->_vZip = null; 
		$this->_iUserId = null; 
		$this->_Extra1 = null; 
		$this->_Extra2 = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiBillinfo()
	{
		return $this->_iBillinfo;
	}

	public function getvAddress()
	{
		return $this->_vAddress;
	}

	public function getvCity()
	{
		return $this->_vCity;
	}

	public function getvState()
	{
		return $this->_vState;
	}

	public function getvCountry()
	{
		return $this->_vCountry;
	}

	public function getvZip()
	{
		return $this->_vZip;
	}

	public function getiUserId()
	{
		return $this->_iUserId;
	}

	public function getExtra1()
	{
		return $this->_Extra1;
	}

	public function getExtra2()
	{
		return $this->_Extra2;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiBillinfo($val)
	{
		 $this->_iBillinfo =  $val;
	}

	public function setvAddress($val)
	{
		 $this->_vAddress =  $val;
	}

	public function setvCity($val)
	{
		 $this->_vCity =  $val;
	}

	public function setvState($val)
	{
		 $this->_vState =  $val;
	}

	public function setvCountry($val)
	{
		 $this->_vCountry =  $val;
	}

	public function setvZip($val)
	{
		 $this->_vZip =  $val;
	}

	public function setiUserId($val)
	{
		 $this->_iUserId =  $val;
	}

	public function setExtra1($val)
	{
		 $this->_Extra1 =  $val;
	}

	public function setExtra2($val)
	{
		 $this->_Extra2 =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM Billing_info WHERE iBillinfo = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iBillinfo = $row[0]['iBillinfo'];
		 $this->_vAddress = $row[0]['vAddress'];
		 $this->_vCity = $row[0]['vCity'];
		 $this->_vState = $row[0]['vState'];
		 $this->_vCountry = $row[0]['vCountry'];
		 $this->_vZip = $row[0]['vZip'];
		 $this->_iUserId = $row[0]['iUserId'];
		 $this->_Extra1 = $row[0]['Extra1'];
		 $this->_Extra2 = $row[0]['Extra2'];
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM Billing_info WHERE iBillinfo = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iBillinfo = ""; // clear key for autoincrement

		 $sql = "INSERT INTO Billing_info ( vAddress,vCity,vState,vCountry,vZip,iUserId,Extra1,Extra2 ) VALUES ( '".$this->_vAddress."','".$this->_vCity."','".$this->_vState."','".$this->_vCountry."','".$this->_vZip."','".$this->_iUserId."','".$this->_Extra1."','".$this->_Extra2."' )";
		 $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE Billing_info SET  vAddress = '".$this->_vAddress."' , vCity = '".$this->_vCity."' , vState = '".$this->_vState."' , vCountry = '".$this->_vCountry."' , vZip = '".$this->_vZip."' , iUserId = '".$this->_iUserId."' , Extra1 = '".$this->_Extra1."' , Extra2 = '".$this->_Extra2."'  WHERE iBillinfo = $id ";
		 $this->_obj->sql_query($sql);

	}


}
