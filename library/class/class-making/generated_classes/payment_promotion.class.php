<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    payment_promotion
* DATE:         23.06.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/payment_promotion.class.php
* TABLE:        payment_promotion
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class payment_promotion
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iPaymentId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iPaymentId;  
	protected $_iUserId;  
	protected $_iPromotionId;  
	protected $_vTransactionId;  
	protected $_iDtAdded;  
	protected $_iDtExpired;  
	protected $_eStatus;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iPaymentId = null; 
		$this->_iUserId = null; 
		$this->_iPromotionId = null; 
		$this->_vTransactionId = null; 
		$this->_iDtAdded = null; 
		$this->_iDtExpired = null; 
		$this->_eStatus = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiPaymentId()
	{
		return $this->_iPaymentId;
	}

	public function getiUserId()
	{
		return $this->_iUserId;
	}

	public function getiPromotionId()
	{
		return $this->_iPromotionId;
	}

	public function getvTransactionId()
	{
		return $this->_vTransactionId;
	}

	public function getiDtAdded()
	{
		return $this->_iDtAdded;
	}

	public function getiDtExpired()
	{
		return $this->_iDtExpired;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiPaymentId($val)
	{
		 $this->_iPaymentId =  $val;
	}

	public function setiUserId($val)
	{
		 $this->_iUserId =  $val;
	}

	public function setiPromotionId($val)
	{
		 $this->_iPromotionId =  $val;
	}

	public function setvTransactionId($val)
	{
		 $this->_vTransactionId =  $val;
	}

	public function setiDtAdded($val)
	{
		 $this->_iDtAdded =  $val;
	}

	public function setiDtExpired($val)
	{
		 $this->_iDtExpired =  $val;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM payment_promotion WHERE iPaymentId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iPaymentId = $row[0]['iPaymentId'];
		 $this->_iUserId = $row[0]['iUserId'];
		 $this->_iPromotionId = $row[0]['iPromotionId'];
		 $this->_vTransactionId = $row[0]['vTransactionId'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
		 $this->_iDtExpired = $row[0]['iDtExpired'];
		 $this->_eStatus = $row[0]['eStatus'];
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM payment_promotion WHERE iPaymentId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iPaymentId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO payment_promotion ( iUserId,iPromotionId,vTransactionId,iDtAdded,iDtExpired,eStatus ) VALUES ( '".$this->_iUserId."','".$this->_iPromotionId."','".$this->_vTransactionId."','".$this->_iDtAdded."','".$this->_iDtExpired."','".$this->_eStatus."' )";
		 $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE payment_promotion SET  iUserId = '".$this->_iUserId."' , iPromotionId = '".$this->_iPromotionId."' , vTransactionId = '".$this->_vTransactionId."' , iDtAdded = '".$this->_iDtAdded."' , iDtExpired = '".$this->_iDtExpired."' , eStatus = '".$this->_eStatus."'  WHERE iPaymentId = $id ";
		 $this->_obj->sql_query($sql);

	}


}
