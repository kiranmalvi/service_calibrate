<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    billing_info
* DATE:         25.06.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/billing_info.class.php
* TABLE:        billing_info
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class billing_info
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iBillingId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iBillingId;  
	protected $_iDtAdded;  
	protected $_iDtUpdated;  
	protected $_eStatus;  
	protected $_vAddress;  
	protected $_vCity;  
	protected $_VState;  
	protected $_vZip;  
	protected $_vCountry;  
	protected $_iUserId;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iBillingId = null; 
		$this->_iDtAdded = null; 
		$this->_iDtUpdated = null; 
		$this->_eStatus = null; 
		$this->_vAddress = null; 
		$this->_vCity = null; 
		$this->_VState = null; 
		$this->_vZip = null; 
		$this->_vCountry = null; 
		$this->_iUserId = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiBillingId()
	{
		return $this->_iBillingId;
	}

	public function getiDtAdded()
	{
		return $this->_iDtAdded;
	}

	public function getiDtUpdated()
	{
		return $this->_iDtUpdated;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}

	public function getvAddress()
	{
		return $this->_vAddress;
	}

	public function getvCity()
	{
		return $this->_vCity;
	}

	public function getVState()
	{
		return $this->_VState;
	}

	public function getvZip()
	{
		return $this->_vZip;
	}

	public function getvCountry()
	{
		return $this->_vCountry;
	}

	public function getiUserId()
	{
		return $this->_iUserId;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiBillingId($val)
	{
		 $this->_iBillingId =  $val;
	}

	public function setiDtAdded($val)
	{
		 $this->_iDtAdded =  $val;
	}

	public function setiDtUpdated($val)
	{
		 $this->_iDtUpdated =  $val;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}

	public function setvAddress($val)
	{
		 $this->_vAddress =  $val;
	}

	public function setvCity($val)
	{
		 $this->_vCity =  $val;
	}

	public function setVState($val)
	{
		 $this->_VState =  $val;
	}

	public function setvZip($val)
	{
		 $this->_vZip =  $val;
	}

	public function setvCountry($val)
	{
		 $this->_vCountry =  $val;
	}

	public function setiUserId($val)
	{
		 $this->_iUserId =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM billing_info WHERE iBillingId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iBillingId = $row[0]['iBillingId'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
		 $this->_iDtUpdated = $row[0]['iDtUpdated'];
		 $this->_eStatus = $row[0]['eStatus'];
		 $this->_vAddress = $row[0]['vAddress'];
		 $this->_vCity = $row[0]['vCity'];
		 $this->_VState = $row[0]['VState'];
		 $this->_vZip = $row[0]['vZip'];
		 $this->_vCountry = $row[0]['vCountry'];
		 $this->_iUserId = $row[0]['iUserId'];
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM billing_info WHERE iBillingId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iBillingId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO billing_info ( iDtAdded,iDtUpdated,eStatus,vAddress,vCity,VState,vZip,vCountry,iUserId ) VALUES ( '".$this->_iDtAdded."','".$this->_iDtUpdated."','".$this->_eStatus."','".$this->_vAddress."','".$this->_vCity."','".$this->_VState."','".$this->_vZip."','".$this->_vCountry."','".$this->_iUserId."' )";
		 $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE billing_info SET  iDtAdded = '".$this->_iDtAdded."' , iDtUpdated = '".$this->_iDtUpdated."' , eStatus = '".$this->_eStatus."' , vAddress = '".$this->_vAddress."' , vCity = '".$this->_vCity."' , VState = '".$this->_VState."' , vZip = '".$this->_vZip."' , vCountry = '".$this->_vCountry."' , iUserId = '".$this->_iUserId."'  WHERE iBillingId = $id ";
		 $this->_obj->sql_query($sql);

	}


}
