<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    feature
* DATE:         15.04.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/feature.class.php
* TABLE:        feature
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class feature
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iFeatureId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iFeatureId;  
	protected $_vFeatureName;  
	protected $_eUSerType;  
	protected $_vValue;  
	protected $_eFeatureType;  
	protected $_fPrice;  
	protected $_vDescription;  
	protected $_iDtAdded;  
	protected $_eStatus;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iFeatureId = null; 
		$this->_vFeatureName = null; 
		$this->_eUSerType = null; 
		$this->_vValue = null; 
		$this->_eFeatureType = null; 
		$this->_fPrice = null; 
		$this->_vDescription = null; 
		$this->_iDtAdded = null; 
		$this->_eStatus = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiFeatureId()
	{
		return $this->_iFeatureId;
	}

    /**
     * @desc   SETTER METHODS
     */


    public function setiFeatureId($val)
	{
        $this->_iFeatureId = $val;
	}

    public function getvFeatureName()
	{
        return $this->_vFeatureName;
	}

    public function setvFeatureName($val)
	{
        $this->_vFeatureName = $val;
	}

    public function geteUSerType()
	{
        return $this->_eUSerType;
	}

    public function seteUSerType($val)
	{
        $this->_eUSerType = $val;
	}

    public function getvValue()
	{
        return $this->_vValue;
	}

    public function setvValue($val)
	{
        $this->_vValue = $val;
	}

    public function geteFeatureType()
	{
        return $this->_eFeatureType;
	}

    public function seteFeatureType($val)
	{
        $this->_eFeatureType = $val;
	}

    public function getfPrice()
	{
        return $this->_fPrice;
	}

    public function setfPrice($val)
	{
        $this->_fPrice = $val;
	}

    public function getvDescription()
	{
        return $this->_vDescription;
	}

	public function setvDescription($val)
	{
		 $this->_vDescription =  $val;
	}

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

	public function setiDtAdded($val)
	{
		 $this->_iDtAdded =  $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM feature WHERE iFeatureId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iFeatureId = $row[0]['iFeatureId'];
		 $this->_vFeatureName = $row[0]['vFeatureName'];
		 $this->_eUSerType = $row[0]['eUSerType'];
		 $this->_vValue = $row[0]['vValue'];
		 $this->_eFeatureType = $row[0]['eFeatureType'];
		 $this->_fPrice = $row[0]['fPrice'];
		 $this->_vDescription = $row[0]['vDescription'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
		 $this->_eStatus = $row[0]['eStatus'];
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM feature WHERE iFeatureId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iFeatureId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO feature ( vFeatureName,eUSerType,vValue,eFeatureType,fPrice,vDescription,iDtAdded,eStatus ) VALUES ( '".$this->_vFeatureName."','".$this->_eUSerType."','".$this->_vValue."','".$this->_eFeatureType."','".$this->_fPrice."','".$this->_vDescription."','".$this->_iDtAdded."','".$this->_eStatus."' )";
		 $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE feature SET  vFeatureName = '".$this->_vFeatureName."' , eUSerType = '".$this->_eUSerType."' , vValue = '".$this->_vValue."' , eFeatureType = '".$this->_eFeatureType."' , fPrice = '".$this->_fPrice."' , vDescription = '".$this->_vDescription."' , iDtAdded = '".$this->_iDtAdded."' , eStatus = '".$this->_eStatus."'  WHERE iFeatureId = $id ";
		 $this->_obj->sql_query($sql);

	}


}
