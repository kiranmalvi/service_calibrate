<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    category
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/category.class.php
 * TABLE:        category
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class category
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iCategoryId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iCategoryId;
    protected $_vCategoryName;
    protected $_iDtAdded;
    protected $_iDtUpdated;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iCategoryId = null;
        $this->_vCategoryName = null;
        $this->_iDtAdded = null;
        $this->_iDtUpdated = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiCategoryId()
    {
        return $this->_iCategoryId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiCategoryId($val)
    {
        $this->_iCategoryId = $val;
    }

    public function getvCategoryName()
    {
        return $this->_vCategoryName;
    }

    public function setvCategoryName($val)
    {
        $this->_vCategoryName = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }
 public function getiDtUpdated()
    {
        return $this->_iDtUpdated;
    }

    public function setiDtUpdated($val)
    {
        $this->_iDtUpdated = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $WHERE = 'WHERE 1=1';
        if (!is_null($id)) {
            $WHERE .= " AND iCategoryId = $id";
        }

        $sql = "SELECT * FROM category $WHERE AND eStatus!='2'";
        $row = $this->_obj->select($sql);

        $this->_iCategoryId = $row[0]['iCategoryId'];
        $this->_vCategoryName = $row[0]['vCategoryName'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_iDtUpdated = $row[0]['iDtUpdated'];
        $this->_eStatus = $row[0]['eStatus'];

        return $row;
    }

    function select_FRONT($id = null)
    {
        $WHERE = 'WHERE 1=1 AND eStatus = "1"';
        if (!is_null($id)) {
            $WHERE .= " AND iCategoryId = $id";
        }

        $sql = "SELECT * FROM category $WHERE ORDER BY vCategoryName ASC";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function select_API()
    {
        $sql = "SELECT iCategoryId,vCategoryName FROM category WHERE eStatus = '1' order by vCategoryName";
        $row = $this->_obj->select($sql);
        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM category WHERE iCategoryId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iCategoryId = ""; // clear key for autoincrement

        $sql = "INSERT INTO category ( vCategoryName,iDtAdded,eStatus,iDtUpdated ) VALUES ( '" . $this->_vCategoryName . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "','" . $this->_iDtUpdated . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE category SET ";
        $sql .= ($this->_vCategoryName != null) ? ", vCategoryName = '" . $this->_vCategoryName . "'," : "";
        $sql .= ($this->_iDtAdded != null) ? ", iDtAdded = '" . $this->_iDtAdded . "'," : "";
        $sql .= ($this->_iDtUpdated != null) ? ", iDtUpdated = '" . $this->_iDtUpdated . "'," : "";
        $sql .= ($this->_eStatus != null) ? ",eStatus = '" . $this->_eStatus . "' ," : "";
        $sql .= "WHERE iCategoryId = $id ";

        $UPDATE = clean_sql($sql);

        $this->_obj->sql_query($UPDATE);
    }

    function selectall()
    {
        $sql = "SELECT * FROM category where eStatus='1'";
        $row = $this->_obj->select($sql);
        return $row;
    }


}
