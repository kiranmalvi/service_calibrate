<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    help_faq
* DATE:         22.05.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/help_faq.class.php
* TABLE:        help_faq
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class help_faq
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iHelpId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iHelpId;  
	protected $_idtAdded;  
	protected $_idtUpdated;  
	protected $_eStatus;  
	protected $_vQuestion;  
	protected $_vAnswer;  
	protected $_vLink;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iHelpId = null; 
		$this->_idtAdded = null; 
		$this->_idtUpdated = null; 
		$this->_eStatus = null; 
		$this->_vQuestion = null; 
		$this->_vAnswer = null; 
		$this->_vLink = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiHelpId()
	{
		return $this->_iHelpId;
	}

	public function getidtAdded()
	{
		return $this->_idtAdded;
	}

	public function getidtUpdated()
	{
		return $this->_idtUpdated;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}

	public function getvQuestion()
	{
		return $this->_vQuestion;
	}

	public function getvAnswer()
	{
		return $this->_vAnswer;
	}

	public function getvLink()
	{
		return $this->_vLink;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiHelpId($val)
	{
		 $this->_iHelpId =  $val;
	}

	public function setidtAdded($val)
	{
		 $this->_idtAdded =  $val;
	}

	public function setidtUpdated($val)
	{
		 $this->_idtUpdated =  $val;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}

	public function setvQuestion($val)
	{
		 $this->_vQuestion =  $val;
	}

	public function setvAnswer($val)
	{
		 $this->_vAnswer =  $val;
	}

	public function setvLink($val)
	{
		 $this->_vLink =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id = null)
	{
        $where = 'Where 1=1 AND eStatus != "2"';
        if (!is_null($id)) {
            $where .= " AND iHelpId = $id";
        }
	  $sql =  "SELECT * FROM help_faq $where";
		 $row =  $this->_obj->select($sql); 

		 $this->_iHelpId = $row[0]['iHelpId'];
		 $this->_idtAdded = $row[0]['idtAdded'];
		 $this->_idtUpdated = $row[0]['idtUpdated'];
		 $this->_eStatus = $row[0]['eStatus'];
		 $this->_vQuestion = $row[0]['vQuestion'];
		 $this->_vAnswer = $row[0]['vAnswer'];
		 $this->_vLink = $row[0]['vLink'];
        return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		// $sql = "DELETE FROM help_faq WHERE iHelpId = $id";
		 $sql = "UPDATE help_faq SET eStatus = '2'  WHERE iHelpId = $id";

		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iHelpId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO help_faq ( idtAdded,idtUpdated,eStatus,vQuestion,vAnswer,vLink ) VALUES ( '".$this->_idtAdded."','".$this->_idtUpdated."','".$this->_eStatus."','".$this->_vQuestion."','".$this->_vAnswer."','".$this->_vLink."' )";
		 $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE help_faq SET  idtUpdated = '".$this->_idtUpdated."' , eStatus = '".$this->_eStatus."' , vQuestion = '".$this->_vQuestion."' , vAnswer = '".$this->_vAnswer."' , vLink = '".$this->_vLink."'  WHERE iHelpId = $id ";
		 $this->_obj->sql_query($sql);

	}


}
