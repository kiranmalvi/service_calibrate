<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    message_conversation
 * DATE:         23.04.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/message_conversation.class.php
 * TABLE:        message_conversation
 * DB:           mind_service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class message_conversation
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iConversationId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iConversationId;
    protected $_iUserId;
    protected $_iUseraddedId;
    protected $_iDtAdded;
    protected $_eStatus;
    protected $_iDtUpdated;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iConversationId = null;
        $this->_iUserId = null;
        $this->_iUseraddedId = null;
        $this->_iDtAdded = null;
        $this->_eStatus = null;
        $this->_iDtUpdated = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiConversationId()
    {
        return $this->_iConversationId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiConversationId($val)
    {
        $this->_iConversationId = $val;
    }

    public function getiUserId()
    {
        return $this->_iUserId;
    }

    public function setiUserId($val)
    {
        $this->_iUserId = $val;
    }

    public function getiUseraddedId()
    {
        return $this->_iUseraddedId;
    }

    public function setiUseraddedId($val)
    {
        $this->_iUseraddedId = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }

    public function getiDtUpdated()
    {
        return $this->_iDtUpdated;
    }

    public function setiDtUpdated($val)
    {
        $this->_iDtUpdated = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM message_conversation WHERE iConversationId = $id";
        $row = $this->_obj->select($sql);

        $this->_iConversationId = $row[0]['iConversationId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_iUseraddedId = $row[0]['iUseraddedId'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_iDtUpdated = $row[0]['iDtUpdated'];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM message_conversation WHERE iConversationId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iConversationId = ""; // clear key for autoincrement

        $sql = "INSERT INTO message_conversation ( iUserId,iUseraddedId,iDtAdded,eStatus,iDtUpdated ) VALUES ( '" . $this->_iUserId . "','" . $this->_iUseraddedId . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "','" . $this->_iDtUpdated . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE message_conversation SET  iUserId = '" . $this->_iUserId . "' , iUseraddedId = '" . $this->_iUseraddedId . "' , iDtAdded = '" . $this->_iDtAdded . "' , eStatus = '" . $this->_eStatus . "' , iDtUpdated = '" . $this->_iDtUpdated . "'  WHERE iConversationId = $id ";
        $this->_obj->sql_query($sql);

    }

    function get_meassage_list($id)
    {
        $sql = "SELECT * FROM message_conversation WHERE iUserId = '$id' AND eStatus='1' order by iDtUpdated desc";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function check_user_added($iUserId, $iUseraddedId)
    {
        $sql = "SELECT * FROM message_conversation WHERE iUserId = '$iUserId' AND iUseraddedId='$iUseraddedId'";
        $row = $this->_obj->select($sql);
        return $row;

    }

    function update_status($id)
    {
        $sql = " UPDATE message_conversation SET eStatus = '" . $this->_eStatus . "' , iDtUpdated = '" . $this->_iDtUpdated . "'  WHERE iConversationId = $id ";
        $this->_obj->sql_query($sql);
    }

 function update_deleted_status($iUserId,$iUserAddedId)
   {
       $sql = " UPDATE message_conversation SET eStatus = '" . $this->_eStatus . "' , iDtUpdated = '" . $this->_iDtUpdated . "'  WHERE iUserId = '$iUserId' AND iUseraddedId='$iUserAddedId' ";
       $result=$this->_obj->sql_query($sql);
       return $result;
   }

}