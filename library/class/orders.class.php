<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    orders
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/orders.class.php
 * TABLE:        orders
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class orders
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iOrderId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iOrderId;
    protected $_iRetailerId;
    protected $_iUserId;
    protected $_iTotalTime;
    protected $_fAmount;
    protected $_iDeliveryDate;
    protected $_vSignature;
    protected $_vLatitude;
    protected $_vLongitude;
    protected $_eFlag;
    protected $_eStatus;
    protected $_eServiceType;
    protected $_vRetailer_name;
    protected $_iDtAdded;
    protected $_sCurrentTime;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iOrderId = null;
        $this->_iRetailerId = null;
        $this->_iUserId = null;
        $this->_iTotalTime = null;
        $this->_fAmount = null;
        $this->_iDeliveryDate = null;
        $this->_vSignature = null;
        $this->_vLatitude = null;
        $this->_vLongitude = null;
        $this->_eFlag = null;
        $this->_eStatus = null;
        $this->_eServiceType = null;
        $this->_vRetailer_name = null;
        $this->_iDtAdded = null;
        $this->_sCurrentTime = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiOrderId()
    {
        return $this->_iOrderId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiOrderId($val)
    {
        $this->_iOrderId = $val;
    }

    public function getiRetailerId()
    {
        return $this->_iRetailerId;
    }

    public function setiRetailerId($val)
    {
        $this->_iRetailerId = $val;
    }

    public function getiUserId()
    {
        return $this->_iUserId;
    }

    public function setiUserId($val)
    {
        $this->_iUserId = $val;
    }

    public function getiTotalTime()
    {
        return $this->_iTotalTime;
    }

    public function setiTotalTime($val)
    {
        $this->_iTotalTime = $val;
    }

    public function getfAmount()
    {
        return $this->_fAmount;
    }

    public function setfAmount($val)
    {
        $this->_fAmount = $val;
    }

    public function getiDeliveryDate()
    {
        return $this->_iDeliveryDate;
    }

    public function setiDeliveryDate($val)
    {
        $this->_iDeliveryDate = $val;
    }

    public function getvSignature()
    {
        return $this->_vSignature;
    }

    public function setvSignature($val)
    {
        $this->_vSignature = $val;
    }

    public function getvLatitude()
    {
        return $this->_vLatitude;
    }

    public function setvLatitude($val)
    {
        $this->_vLatitude = $val;
    }

    public function getvLongitude()
    {
        return $this->_vLongitude;
    }

    public function setvLongitude($val)
    {
        $this->_vLongitude = $val;
    }

    public function geteFlag()
    {
        return $this->_eFlag;
    }

    public function seteFlag($val)
    {
        $this->_eFlag = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }

    public function geteServiceType()
    {
        return $this->_eServiceType;
    }

    public function seteServiceType($val)
    {
        $this->_eServiceType = $val;
    }

    public function getvRetailer_name()
    {
        return $this->_vRetailer_name;
    }

    public function setvRetailer_name($val)
    {
        $this->_vRetailer_name = $val;
    }
    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }
    public function getsCurrentTime()
    {
        return $this->_sCurrentTime;
    }

    public function setsCurrentTime($val)
    {
        $this->_sCurrentTime = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM orders WHERE iOrderId = $id";
        $row = $this->_obj->select($sql);

        $this->_iOrderId = $row[0]['iOrderId'];
        $this->_iRetailerId = $row[0]['iRetailerId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_iTotalTime = $row[0]['iTotalTime'];
        $this->_fAmount = $row[0]['fAmount'];
        $this->_iDeliveryDate = $row[0]['iDeliveryDate'];
        $this->_vSignature = $row[0]['vSignature'];
        $this->_vLatitude = $row[0]['vLatitude'];
        $this->_vLongitude = $row[0]['vLongitude'];
        $this->_eFlag = $row[0]['eFlag'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_eServiceType = $row[0]['eServiceType'];
        $this->_vRetailer_name = $row[0]['vRetailer_name'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_sCurrentTime = $row[0]['iDtAdded'];
        return $row;

    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM orders WHERE iOrderId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iOrderId = ""; // clear key for autoincrement

        $sql = "INSERT INTO orders ( iRetailerId,iUserId,iTotalTime,fAmount,iDeliveryDate,vSignature,vLatitude,vLongitude,eFlag,eStatus,eServiceType,vRetailer_name, iDtAdded,sCurrentTime ) VALUES ( '" . $this->_iRetailerId . "','" . $this->_iUserId . "','" . $this->_iTotalTime . "','" . $this->_fAmount . "','" . $this->_iDeliveryDate . "','" . $this->_vSignature . "','" . $this->_vLatitude . "','" . $this->_vLongitude . "','" . $this->_eFlag . "','" . $this->_eStatus . "' ,'" . $this->_eServiceType . "','" . $this->_vRetailer_name . "','" . $this->_iDtAdded . "','" . $this->_sCurrentTime . "')";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE orders SET  iRetailerId = '" . $this->_iRetailerId . "' , iUserId = '" . $this->_iUserId . "' , iTotalTime = '" . $this->_iTotalTime . "' , fAmount = '" . $this->_fAmount . "' , iDeliveryDate = '" . $this->_iDeliveryDate . "' , vSignature = '" . $this->_vSignature . "' , vLatitude = '" . $this->_vLatitude . "' , vLongitude = '" . $this->_vLongitude . "' , eFlag = '" . $this->_eFlag . "' , eStatus = '" . $this->_eStatus . "', eServiceType = '" . $this->_eServiceType . "', vRetailer_name = '" . $this->_vRetailer_name . "' ,  iDtAdded = '" . $this->_iDtAdded . "',  sCurrentTime = '" . $this->_sCurrentTime . "' WHERE iOrderId = $id ";
        $this->_obj->sql_query($sql);

    }

//Get Details of distributor & mnaufacturer who have scanned Qr Code of log in Retailer

    function select_retailer($id)
    {
        global $inc_class_path;
        $sql = "SELECT DISTINCT iUserId, iRetailerId FROM orders WHERE iRetailerId = '$id' AND eStatus='1'";
        $row = $this->_obj->select($sql);
        

        include_once($inc_class_path . 'user.class.php');
        $userObj = new user();

        for ($i = 0; $i < count($row); $i++) 
{
            $rid = $row[$i]['iUserId'];
            $retailer_detail = $userObj->select($rid);

            $data[$i]['vAccountName'] = $userObj->getvStoreUniqueId();
            $data[$i]['vFirstName'] = $userObj->getvFirstName();
            $data[$i]['vLastName'] = $userObj->getvLastName();
            $data[$i]['iScannedId'] = $rid;
            $data[$i]['iUserId'] = $id;
        }
        return $data;
    }

 function select_retailer1($id)
    {
        global $inc_class_path;

       $sql = "SELECT  iUserId, iRetailerId FROM orders WHERE iRetailerId in (Select iUserId from  user where iParentId='$id' or iuserId='$id' AND eStatus='1') AND eStatus='1' GROUP BY iUserId";
        $row = $this->_obj->select($sql);


        include_once($inc_class_path . 'user.class.php');
        $userObj = new user();

        for ($i = 0; $i < count($row); $i++)
        {
            $rid = $row[$i]['iUserId'];
            $retailer_detail = $userObj->select($rid);

            $data[$i]['vAccountName'] = $userObj->getvStoreUniqueId();
            $data[$i]['vFirstName'] = $userObj->getvFirstName();
            $data[$i]['vLastName'] = $userObj->getvLastName();
            $data[$i]['iScannedId'] = $id;
            $data[$i]['iUserId'] = $row[$i]['iUserId'];
        }
        return $data;
    }

//Get Details of Retailer who have scanned Qr Code of log in Distributor & Manufacturer

    function select_dist_man($id)
    {
        global $inc_class_path;
        $sql = "SELECT DISTINCT iUserId, iRetailerId FROM orders WHERE iUserId = $id AND eStatus='1'";
        $row = $this->_obj->select($sql);


        include_once($inc_class_path . 'user.class.php');
        $userObj = new user();

        for ($i = 0; $i < count($row); $i++) {
            $rid = $row[$i]['iRetailerId'];
            $retailer_detail = $userObj->select($rid);

            $data[$i]['vAccountName'] = $userObj->getvStoreUniqueId();
            $data[$i]['vFirstName'] = $userObj->getvFirstName();
            $data[$i]['vLastName'] = $userObj->getvLastName();
            $data[$i]['iScannedId'] = $rid;
            $data[$i]['iUserId'] = $id;
        }
        return $data;
    }
function select_dist_man1($id)
    {
        global $inc_class_path;
        $sql = "SELECT iRetailerId, iUserId FROM orders WHERE iUserId in (Select iUserId from  user where iParentId='$id' or iuserId='$id' AND eStatus='1' )  AND eStatus='1' GROUP by iRetailerId";
        $row = $this->_obj->select($sql);


        include_once($inc_class_path . 'user.class.php');
        $userObj = new user();

        for ($i = 0; $i < count($row); $i++) {
            $rid = $row[$i]['iRetailerId'];
            $retailer_detail = $userObj->select($rid);

            $data[$i]['vAccountName'] = $userObj->getvStoreUniqueId();
            $data[$i]['vFirstName'] = $userObj->getvFirstName();
            $data[$i]['vLastName'] = $userObj->getvLastName();
            $data[$i]['iScannedId'] = $row[$i]['iUserId'];
            $data[$i]['iUserId'] = $row[$i]['iRetailerId'];
        }
        return $data;
    }

    //Get Details of last five orders
    function get_last_five($userid,$usertype)
    {


        if($usertype=="2" || $usertype=="3" )
        {
            $WHERE="WHERE iUserId = $userid AND eStatus='1' ORDER BY iOrderId DESC
LIMIT 5";
        }
        else
        {
            $WHERE= "WHERE iRetailerId= $userid AND eStatus='1' ORDER BY iOrderId DESC
LIMIT 5";
        }

        $sql = "SELECT * FROM orders $WHERE";
        $row = $this->_obj->select($sql);

        return $row;
    }

    //Get Details of curretn month orders
    function get_month_report($userid,$fdate,$ldate,$usertype)
    {
        if($usertype=="2" || $usertype=="3" )
        {
            $WHERE="WHERE iUserId = $userid AND eStatus='1'  AND iDtAdded BETWEEN '$fdate' AND '$ldate' ORDER BY iOrderId DESC";
        }
        else
        {
            $WHERE= "WHERE iRetailerId = $userid AND eStatus='1'  AND iDtAdded BETWEEN '$fdate' AND '$ldate' ORDER BY iOrderId DESC";
        }
       $sql = "SELECT * FROM orders $WHERE";

        $row = $this->_obj->select($sql);
        return $row;
    }

    //Get Details of custom date's orders
    function get_custom_report($userid,$fdate,$ldate,$usertype)
    {
        if($usertype=="2" || $usertype=="3" )
        {
            $WHERE="WHERE iUserId = $userid AND eStatus='1'  AND iDtAdded BETWEEN '$fdate' AND '$ldate' ORDER BY iOrderId DESC";
        }
        else
        {
            $WHERE= "WHERE iRetailerId = $userid AND eStatus='1'  AND iDtAdded BETWEEN '$fdate' AND '$ldate' ORDER BY iOrderId DESC";
        }
        $sql = "SELECT * FROM orders $WHERE";

        $row = $this->_obj->select($sql);
        return $row;
    }

    function get_last_five_with_user($userid,$usertype,$refid)
    {
        if($usertype=="2" || $usertype=="3" )
        {
            $WHERE="WHERE iUserId = $userid AND iRetailerId = $refid AND eStatus='1' ORDER BY iOrderId DESC
LIMIT 5";
        }
        else
        {
            $WHERE= "WHERE iRetailerId = $userid AND iUserId = $refid AND eStatus='1' ORDER BY iOrderId DESC
LIMIT 5";
        }

        $sql = "SELECT * FROM orders $WHERE";
        $row = $this->_obj->select($sql);

        return $row;
    }

    //Get Details of curretn month orders
    function get_month_report_with_user($userid,$fdate,$ldate,$usertype,$refid)
    {
        if($usertype=="2" || $usertype=="3" )
        {
            $WHERE="WHERE iUserId = $userid AND iRetailerId = $refid AND eStatus='1'  AND iDtAdded BETWEEN '$fdate' AND '$ldate' ORDER BY iOrderId DESC";
        }
        else
        {
            $WHERE= "WHERE iRetailerId = $userid AND iUserId = $refid AND eStatus='1'  AND iDtAdded BETWEEN '$fdate' AND '$ldate' ORDER BY iOrderId DESC";
        }
       $sql = "SELECT * FROM orders $WHERE";
        $row = $this->_obj->select($sql);
        return $row;
    }

    //Get Details of custom date's orders
    function get_custom_report_with_user($userid,$fdate,$ldate,$usertype,$refid)
    {
        if($usertype=="2" || $usertype=="3" )
        {
            $WHERE="WHERE iUserId = $userid AND iRetailerId = $refid AND eStatus='1'  AND iDtAdded BETWEEN '$fdate' AND '$ldate' ORDER BY iOrderId DESC";
        }
        else
        {
            $WHERE= "WHERE iRetailerId = $userid AND iUserId = $refid AND eStatus='1'  AND iDtAdded BETWEEN '$fdate' AND '$ldate' ORDER BY iOrderId DESC";
        }
        $sql = "SELECT * FROM orders $WHERE";

        $row = $this->_obj->select($sql);
        return $row;
    }

   function get_flag($zipcode, $iUserId)
    {

        $sql = "select  vZip,iUserId from user where iUserId='$iUserId' AND vZip='$zipcode'";
        $result = $this->_obj->sql_query($sql);
        if((count($result)>0))
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
   function get_delivery_report($userid, $usertype, $start, $end)
    {
        $sql='';
        if($start != 0 && $end != 0 && $start != '' && $end != ''){
             $sql = 'AND iDeliveryDate BETWEEN '.$start.' AND '.$end.'';
        }
        if ($usertype == "2" || $usertype == "3") {
            $WHERE = "WHERE iUserId = $userid AND eStatus='1' AND eServiceType='0'  $sql";
        } else {
            $WHERE = "WHERE iRetailerId = $userid AND eStatus='1' AND eServiceType='0'  $sql";
        }
        $sql = "SELECT * FROM orders $WHERE";

        $row = $this->_obj->select($sql);
        return $row;
    }

     function get_history($id,$start,$end)
    {
         $sql = "SELECT * FROM orders WHERE iUserId = $id AND eStatus='1' AND iDeliveryDate  BETWEEN '$end' AND '$start' order by iDeliveryDate Desc";
        $row = $this->_obj->select($sql);
        return $row;
    }
    function get_history_all($id,$start,$end)
    {
        $sql = "SELECT * FROM orders WHERE iUserId in(select iUserId from user where iParentId='$id' or iUserId='$id') AND eStatus='1' AND iDeliveryDate  BETWEEN '$end' AND '$start' order by iDeliveryDate Desc";
        $row = $this->_obj->select($sql);
        return $row;
    }
  function check_last($iUserId,$rid)
    {
      $sql = "SELECT * FROM orders WHERE iUserId=$iUserId AND iRetailerId=$rid AND  eStatus='1' order by iDtAdded Desc";
        $row = $this->_obj->select($sql);
        return $row;
    }


}
