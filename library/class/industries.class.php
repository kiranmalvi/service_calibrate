<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    industries
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/industries.class.php
 * TABLE:        industries
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class industries
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iIndustriesId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iIndustriesId;
    protected $_vName;
    protected $_iDtAdded;
    protected $_iDtUpdated;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iIndustriesId = null;
        $this->_vName = null;
        $this->_iDtAdded = null;
        $this->_iDtUpdated = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiIndustriesId()
    {
        return $this->_iIndustriesId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiIndustriesId($val)
    {
        $this->_iIndustriesId = $val;
    }

    public function getvName()
    {
        return $this->_vName;
    }

    public function setvName($val)
    {
        $this->_vName = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }
    public function getiDtUpdated()
    {
        return $this->_iDtUpdated;
    }

    public function setiDtUpdated($val)
    {
        $this->_iDtUpdated = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $WHERE = "WHERE estatus!='2'";
        if (!is_null($id)) {
            $WHERE .= " AND iIndustriesId = $id";
        }
$WHERE .= " order by iDtAdded desc";
        $sql = "SELECT * FROM industries $WHERE";
        $row = $this->_obj->select($sql);

        $this->_iIndustriesId = $row[0]['iIndustriesId'];
        $this->_vName = $row[0]['vName'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_iDtUpdated = $row[0]['iDtUpdated'];
        $this->_eStatus = $row[0]['eStatus'];

        return $row;
    }

    function select_FRONT($id = null)
    {
        $WHERE = "WHERE 1=1 AND eStatus = '1'";
        if (!is_null($id)) {
            $WHERE .= " AND iIndustriesId = $id";
        }

        $sql = "SELECT * FROM industries $WHERE ORDER BY vName ASC";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function select_API()
    {
        $sql = "SELECT vName,iIndustriesId FROM industries WHERE eStatus = '1' order by vName";
        $row = $this->_obj->select($sql);
        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM industries WHERE iIndustriesId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iIndustriesId = ""; // clear key for autoincrement

        $sql = "INSERT INTO industries ( vName,iDtAdded,eStatus,iDtUpdated ) VALUES ( '" . $this->_vName . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "','" . $this->_iDtUpdated . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {
        $sql = " UPDATE industries SET ";
        $sql .= ($this->_vName != null) ? ", vName = '" . $this->_vName . "'," : "";
        $sql .= ($this->_iDtAdded != null) ? ", iDtAdded = '" . $this->_iDtAdded . "'," : "";
        $sql .= ($this->_iDtUpdated != null) ? ", iDtUpdated = '" . $this->_iDtUpdated . "'," : "";
        $sql .= ($this->_eStatus != null) ? ",eStatus = '" . $this->_eStatus . "' ," : "";
        $sql .= "WHERE iIndustriesId = $id ";

        $UPDATE = clean_sql($sql);
        $this->_obj->sql_query($UPDATE);
    }

    function selectall()
    {
        $sql = "SELECT * FROM industries where eStatus='1'";
        $row = $this->_obj->select($sql);
        return $row;
    }

}
