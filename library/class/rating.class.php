<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    rating
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/rating.class.php
 * TABLE:        rating
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class rating
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iRatingId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iRatingId;
    protected $_iDistributorId;
    protected $_iEmployeeId;
    protected $_iUserId;
    protected $_eRating;
    protected $_vDescription;
    protected $_vImage;
    protected $_eFlag;
    protected $_iDtAdded;
    protected $_vTitle;
    protected $_vBrand;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iRatingId = null;
        $this->_iDistributorId = null;
        $this->_iEmployeeId = null;
        $this->_iUserId = null;
        $this->_eRating = null;
        $this->_vDescription = null;
        $this->_vImage = null;
        $this->_eFlag = null;
        $this->_iDtAdded = null;
        $this->_vTitle = null;
        $this->_vBrand = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiRatingId()
    {
        return $this->_iRatingId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiRatingId($val)
    {
        $this->_iRatingId = $val;
    }

    public function getiDistributorId()
    {
        return $this->_iDistributorId;
    }

    public function setiDistributorId($val)
    {
        $this->_iDistributorId = $val;
    }

    public function getiEmployeeId()
    {
        return $this->_iEmployeeId;
    }

    public function setiEmployeeId($val)
    {
        $this->_iEmployeeId = $val;
    }

    public function getiUserId()
    {
        return $this->_iUserId;
    }

    public function setiUserId($val)
    {
        $this->_iUserId = $val;
    }

    public function geteRating()
    {
        return $this->_eRating;
    }

    public function seteRating($val)
    {
        $this->_eRating = $val;
    }

    public function getvDescription()
    {
        return $this->_vDescription;
    }

    public function setvDescription($val)
    {
        $this->_vDescription = $val;
    }

    public function getvImage()
    {
        return $this->_vImage;
    }

    public function setvImage($val)
    {
        $this->_vImage = $val;
    }

    public function geteFlag()
    {
        return $this->_eFlag;
    }

    public function seteFlag($val)
    {
        $this->_eFlag = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function getvTitle()
    {
        return $this->_vTitle;
    }

    public function setvTitle($val)
    {
        $this->_vTitle = $val;
    }

    public function getvBrand()
    {
        return $this->_vBrand;
    }

    public function setvBrand($val)
    {
        $this->_vBrand = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM rating WHERE iRatingId = $id";
        $row = $this->_obj->select($sql);

        $this->_iRatingId = $row[0]['iRatingId'];
        $this->_iDistributorId = $row[0]['iDistributorId'];
        $this->_iEmployeeId = $row[0]['iEmployeeId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_eRating = $row[0]['eRating'];
        $this->_vDescription = $row[0]['vDescription'];
        $this->_vImage = $row[0]['vImage'];
        $this->_eFlag = $row[0]['eFlag'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_vTitle = $row[0]['vTitle'];
        $this->_vBrand = $row[0]['vBrand'];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM rating WHERE iRatingId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iRatingId = ""; // clear key for autoincrement

        $sql = "INSERT INTO rating ( iDistributorId,iEmployeeId,iUserId,eRating,vDescription,vImage,eFlag,iDtAdded,vTitle,vBrand ) VALUES ( '" . $this->_iDistributorId . "','" . $this->_iEmployeeId . "','" . $this->_iUserId . "','" . $this->_eRating . "','" . $this->_vDescription . "','" . $this->_vImage . "','" . $this->_eFlag . "','" . $this->_iDtAdded . "','" . $this->_vTitle . "','" . $this->_vBrand . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE rating SET  iDistributorId = '" . $this->_iDistributorId . "' , iEmployeeId = '" . $this->_iEmployeeId . "' , iUserId = '" . $this->_iUserId . "' , eRating = '" . $this->_eRating . "' , vDescription = '" . $this->_vDescription . "' , vImage = '" . $this->_vImage . "' , eFlag = '" . $this->_eFlag . "' , iDtAdded = '" . $this->_iDtAdded . "' , vTitle = '" . $this->_vTitle . "' , vBrand = '" . $this->_vBrand . "'  WHERE iRatingId = $id ";
        $this->_obj->sql_query($sql);

    }

    function get_last_rating($uid)
    {
        $sql = "SELECT iUserId,iDtAdded  FROM rating WHERE iUserId='$uid' ORDER BY iDtAdded Desc ";
        $result = $this->_obj->sql_query($sql);
        return $result;

    }

    function check_last_rating($uid,$eid,$today, $lastdate)
    {
         $sql = "SELECT count(*) as total FROM rating WHERE iUserId='$uid' AND iEmployeeId='$eid' AND  iDtAdded BETWEEN '$lastdate' AND '$today'";
        $row = $this->_obj->select($sql);

        if ($row[0]['total'] > 0) {
            return false;
        } else {
            return true;
        }

    }
   function get_rating($id, $type, $rating = '',$sid)
    {
        $q = '';
        $selecteduser="";
        if ($rating != '' and $rating < 3) {
            $q = " AND eRating = '$rating'";
        }

        if ($type == "2" || $type == "3") {
            if($sid>0)
            {
                $selecteduser = " AND r.iUserId = '$sid'";
            }
             $sql = "select res.* from(SELECT u.vFirstName,u.vLastName,u.iUserId as user,u.vLatitude,u.vLongitude,u.vStoreName,u.vStoreUniqueId,u.vAddress,ud.vStoreUniqueId as dvndr,us.vFirstName as fVender,us.vLastName as lVender,r.* FROM rating r Left join user u on (r.iUserid=u.iUserId) Left join user us on (r.iEmployeeId=us.iUserId) left join user ud on r.iDistributorId=ud.iUserId WHERE r.iEmployeeId in(select iUserId from user where iUserId='$id' or iParentId='$id')  AND eFlag='1' $q $selecteduser ORDER by r.iRatingId desc) as res group by iUserId";
        } else {
            if($sid>0)
            {
                $selecteduser = " AND r.iDistributorId = '$sid'";
            }

           $sql = "select res.* from(SELECT u.vFirstName,u.vLastName,u.iUserId as user,u.vLatitude,u.vLongitude,u.vStoreName,u.vStoreUniqueId,u.vAddress,ud.vStoreUniqueId as dvndr,us.vFirstName as fVender,us.vLastName as lVender,r.* FROM rating r Left join user u on (r.iUserid=u.iUserId) Left join user us on (r.iEmployeeId=us.iUserId) left join user ud on r.iDistributorId=ud.iUserId WHERE r.iUserId in(select iUserId from user where iUserId='$id' or iParentId='$id') AND eFlag='1' $q $selecteduser  ORDER by r.iRatingId desc)as res group by iEmployeeId";

        }

        $row = $this->_obj->select($sql);
        return $row;
    }
    
      function get_rated_retailer($userid)
    {
        $sql = "SELECT u.iUserId as iUser,u.vLastName,u.vStoreUniqueId as vAccountName,r.* FROM rating r left join user u on r.iUserId=u.iUserId where r.iDistributorId ='$userid' AND eFlag='1' group by r.iUserId ORDER by r.iRatingId desc";
        $row = $this->_obj->select($sql);
        return $row;
    }
    function get_rated_distributor($userid)
    {
        $sql = "SELECT u.iUserId as iUser,u.vLastName,u.vStoreUniqueId as vAccountName,r.* FROM rating r left join user u on r.iDistributorId=u.iUserId where r.iUserId ='$userid' AND eFlag='1' group by r.iDistributorId ORDER by r.iRatingId desc";
        $row = $this->_obj->select($sql);
        return $row;
    }


}
