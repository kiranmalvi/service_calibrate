<?php
/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    referral
 * DATE:         01.05.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/referral.class.php
 * TABLE:        referral
 * DB:           mind_service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */

class referral
{


    /**
     *   @desc Variable Declaration with default value
     */

    protected $iReferralId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iReferralId;
    protected $_iUserId;
    protected $_vPoint;
    protected $_vReason;
    protected $_iDtAdded;
    protected $_iDtUpdated;
    protected $_eStatus;



    /**
     *   @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iReferralId = null;
        $this->_iUserId = null;
        $this->_vPoint = null;
        $this->_vReason = null;
        $this->_iDtAdded = null;
        $this->_iDtUpdated = null;
        $this->_eStatus = null;
    }

    /**
     *   @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }



    /**
     *   @desc   GETTER METHODS
     */


    public function getiReferralId()
    {
        return $this->_iReferralId;
    }

    public function getiUserId()
    {
        return $this->_iUserId;
    }

    public function getvPoint()
    {
        return $this->_vPoint;
    }

    public function getvReason()
    {
        return $this->_vReason;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function getiDtUpdated()
    {
        return $this->_iDtUpdated;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }


    /**
     *   @desc   SETTER METHODS
     */


    public function setiReferralId($val)
    {
        $this->_iReferralId =  $val;
    }

    public function setiUserId($val)
    {
        $this->_iUserId =  $val;
    }

    public function setvPoint($val)
    {
        $this->_vPoint =  $val;
    }

    public function setvReason($val)
    {
        $this->_vReason =  $val;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded =  $val;
    }

    public function setiDtUpdated($val)
    {
        $this->_iDtUpdated =  $val;
    }

    public function seteStatus($val)
    {
        $this->_eStatus =  $val;
    }


    /**
     *   @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql =  "SELECT * FROM referral WHERE iReferralId = $id";
        $row =  $this->_obj->select($sql);

        $this->_iReferralId = $row[0]['iReferralId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_vPoint = $row[0]['vPoint'];
        $this->_vReason = $row[0]['vReason'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_iDtUpdated = $row[0]['iDtUpdated'];
        $this->_eStatus = $row[0]['eStatus'];
    }


    /**
     *   @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM referral WHERE iReferralId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     *   @desc   INSERT
     */

    function insert()
    {
        $this->iReferralId = ""; // clear key for autoincrement

        $sql = "INSERT INTO referral ( iUserId,vPoint,vReason,iDtAdded,iDtUpdated,eStatus ) VALUES ( '".$this->_iUserId."','".$this->_vPoint."','".$this->_vReason."','".$this->_iDtAdded."','".$this->_iDtUpdated."','".$this->_eStatus."' )";
        $result=$this->_obj->insert($sql);
        return $result;
    }


    /**
     *   @desc   UPDATE
     */

    function update($id)
    {
        $sql = " UPDATE referral SET  iUserId = '".$this->_iUserId."' , vPoint = '".$this->_vPoint."' , vReason = '".$this->_vReason."' , iDtAdded = '".$this->_iDtAdded."' , iDtUpdated = '".$this->_iDtUpdated."' , eStatus = '".$this->_eStatus."'  WHERE iReferralId = $id ";
        $this->_obj->sql_query($sql);
    }
    function check_existing_user($userid)
    {
        $sql = "SELECT COUNT(iUserId) as total FROM referral WHERE iUserId = '{$userid}' AND eStatus='1'";
        $result = $this->_obj->sql_query($sql);

        if ($result[0]['total'] > 0)
            return true;
        else
            return false;
    }
    function update_points($userid,$point)
    {
        $point_count = "update referral set iDtUpdated = '".$this->_iDtUpdated."', vPoint= CASE WHEN vPoint>=0 THEN vPoint +$point ELSE 0 END where iUserId in ($userid)";
        $this->_obj->sql_query($point_count);
    }

function  get_total_point($uid)
{
    $sql = "SELECT  iUserId,vPoint FROM referral WHERE iUserId = '{$uid}' AND eStatus='1'";
    $result = $this->_obj->sql_query($sql);
    return $result;

}

}