<?php
/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    message
 * DATE:         23.04.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/message.class.php
 * TABLE:        message
 * DB:           mind_service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */

class message
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iMessageId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iMessageId;
    protected $_iSenderId;
    protected $_iReceiverId;
    protected $_vMessage;
    protected $_eStatus;
    protected $_eRead;
    protected $_eDeleted;
    protected $_iDateAdded;
    protected $_iDtUpdated;
    protected $_eMessage_Type;




    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iMessageId = null;
        $this->_iSenderId = null;
        $this->_iReceiverId = null;
        $this->_vMessage = null;
        $this->_eStatus = null;
        $this->_eRead = null;
        $this->_eDeleted = null;
        $this->_iDateAdded = null;
        $this->_iDtUpdated = null;
        $this->_eMessage_Type = null;


    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiMessageId()
    {
        return $this->_iMessageId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiMessageId($val)
    {
        $this->_iMessageId = $val;
    }

    public function getiSenderId()
    {
        return $this->_iSenderId;
    }

    public function setiSenderId($val)
    {
        $this->_iSenderId = $val;
    }

    public function getiReceiverId()
    {
        return $this->_iReceiverId;
    }

    public function setiReceiverId($val)
    {
        $this->_iReceiverId = $val;
    }

    public function getvMessage()
    {
        return $this->_vMessage;
    }

    public function setvMessage($val)
    {
        $this->_vMessage = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }

    public function geteRead()
    {
        return $this->_eRead;
    }

    public function seteRead($val)
    {
        $this->_eRead = $val;
    }

    public function geteDeleted()
    {
        return $this->_eDeleted;
    }

    public function seteDeleted($val)
    {
        $this->_eDeleted = $val;
    }

    public function getiDateAdded()
    {
        return $this->_iDateAdded;
    }

    public function setiDateAdded($val)
    {
        $this->_iDateAdded = $val;
    }

    public function getiDtUpdated()
    {
        return $this->_iDtUpdated;
    }

    public function setiDtUpdated($val)
    {
        $this->_iDtUpdated = $val;
    }
    public function geteMessage_Type()
    {
        return $this->_eMessage_Type;
    }

    public function seteMessage_Type($val)
    {
        $this->_eMessage_Type = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM message WHERE iMessageId = $id";
        $row = $this->_obj->select($sql);

        $this->_iMessageId = $row[0]['iMessageId'];
        $this->_iSenderId = $row[0]['iSenderId'];
        $this->_iReceiverId = $row[0]['iReceiverId'];
        $this->_vMessage = $row[0]['vMessage'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_eRead = $row[0]['eRead'];
        $this->_eDeleted = $row[0]['eDeleted'];
        $this->_iDateAdded = $row[0]['iDateAdded'];
        $this->_iDtUpdated = $row[0]['iDtUpdated'];
        $this->_eMessage_Type = $row[0]['eMessage_Type'];

        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM message WHERE iMessageId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iMessageId = ""; // clear key for autoincrement

        $sql = "INSERT INTO message ( iSenderId,iReceiverId,vMessage,eStatus,eRead,eDeleted,iDateAdded,iDtUpdated,eMessage_Type ) VALUES ( '" . $this->_iSenderId . "','" . $this->_iReceiverId . "','" . $this->_vMessage . "','" . $this->_eStatus . "','" . $this->_eRead . "','" . $this->_eDeleted . "','" . $this->_iDateAdded . "','" . $this->_iDtUpdated . "','" . $this->_eMessage_Type  . "')";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE message SET  iSenderId = '" . $this->_iSenderId . "' , iReceiverId = '" . $this->_iReceiverId . "' , vMessage = '" . $this->_vMessage . "' , eStatus = '" . $this->_eStatus . "' , eRead = '" . $this->_eRead . "' , eDeleted = '" . $this->_eDeleted . "' , iDateAdded = '" . $this->_iDateAdded . "' , iDtUpdated = '" . $this->_iDtUpdated . "' , eMessage_Type = '" . $this->_eMessage_Type . "'   WHERE iMessageId = $id ";
        return $this->_obj->sql_query($sql);

    }

    function select_message($id)
    {
        $sql = "SELECT * FROM message WHERE iMessageId = $id";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function get_unread($receiver, $sender)
    {
        $sql = "SELECT count(*) as total FROM message WHERE iReceiverId='$receiver' AND iSenderId='$sender' AND eRead='0' AND eStatus='1'  ";
        $row = $this->_obj->select($sql);
        return $row[0]['total'];
    }

    function chat_messages($sid, $rid, $time = '', $type = '')
    {

        global $user_image_path, $user_image_url, $dafault_image_logo_user;

        $ssql = '';

        if ($type != "" && $time != "")
        {
            if ($type == '0')
            {
                $ssql = " and m.iDateAdded < '$time'  ";
            } elseif ($type == '1')
            {
                $ssql = " and m.iDateAdded > '$time'  ";
            }
        }

        /*$sql1 = " UPDATE message SET  eRead='yes' WHERE (iSenderId= '$sid' AND iReceiverId='$rid') OR (iSenderId= '$rid' AND iReceiverId='$sid') ";
        $this->_obj->sql_query($sql1);*/

        $sql = " select m.iMessageId,m.vMessage,m.eStatus,m.eMessage_Type, m.eRead, m.iDateAdded, m.iSenderId, m.iReceiverId, u.vImage from message m LEFT JOIN user u ON u.iUserId = m.iSenderId where m.iSenderId IN ($sid,$rid) and m.iReceiverId IN ($sid,$rid) and NOT find_in_set({$sid}, m.eDeleted) and u.eStatus = '1' {$ssql} order by m.iDateAdded DESC ";

        $result = $this->_obj->select($sql);
//pr($result);exit;


        if (count($result) > 0) {

            for ($i = 0; $i < count($result); $i++) {
                $vImage = $result[$i]['vImage'];
                $image_path = $user_image_path . $vImage;
                $image_url = $user_image_url . $vImage;

                $result[$i]['vImage'] = is_file($image_path) ? $image_url : $dafault_image_logo_user;
                /* $result[$i]['iDateAdded'] = gmdate('m-d-Y H:i:s', $result[$i]['iDateAdded']);*/

            }
            return $result;

        } else {
            $result = array();
            return $result;
        }

    }

    function delete_chat($iUserId, $iEmployeeId)
    {
        $sql = " update message set eDeleted=CASE WHEN eDeleted='' THEN {$iUserId} ELSE CONCAT(eDeleted,',',{$iUserId}) END  where NOT FIND_IN_SET({$iUserId}, eDeleted) AND (iSenderId IN ($iUserId,$iEmployeeId) AND iReceiverId IN ($iUserId,$iEmployeeId)) ";

        $result = $this->_obj->sql_query($sql);
        return $result;
    }
    function func_check_sender_receiverid_exist($uid, $fid)
    {
        $query = "select count(*) tot from message where (iSenderId= '$uid' AND iReceiverId='$fid') OR (iSenderId= '$fid' AND iReceiverId='$uid') ";
        $row = $this->_obj->sql_query($query);
        if ($row[0]['tot'] == 0)
        {
            return 0;
        }
        else
        {
            return $row[0]['tot'];
        }

    }
    function func_update_status($uid, $fid)
    {

        $sql = "UPDATE message SET  eRead='1' WHERE iSenderId= '$fid' AND iReceiverId='$uid' ";
        $reult = $this->_obj->sql_query($sql);
        return $reult;

    }
    function func_count_user_unread_message($id)
    {
         $query = "select count(*) as tot from message where iReceiverId= '{$id}' AND eStatus='1' AND eRead='0'";
        $row = $this->_obj->sql_query($query);

        if ($row[0]['tot'] == 0)
        {
            return $row[0]['tot'];
        }
        else
        {
            return $row[0]['tot'];
        }

    }

/**
    ADMIN:Brodcast message Delete
     *  delete selected broadcast message*/
    function delete_message($mid,$uid)
    {
        $sql = " update message set eDeleted=CASE WHEN eDeleted='' THEN {$uid} ELSE CONCAT(eDeleted,',',{$uid}) END  where iMessageId='$mid' ";
        $result = $this->_obj->sql_query($sql);
        return $result;
    }


}
