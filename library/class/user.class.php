<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    user
 * DATE:         25.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/user.class.php
 * TABLE:        user
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class user
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iUserId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iUserId;
    protected $_iParentId;
    protected $_iUserTypeId;
    protected $_iUserRoleId;
    protected $_iTypeOfStoreId;
    protected $_iPlanId;
    protected $_iIndustriesId;
    protected $_vStoreName;
    protected $_vStoreUniqueId;
    protected $_NumberOfStore;
    protected $_vEmail;
    protected $_vPassword;
    protected $_vFirstName;
    protected $_vLastName;
    protected $_vImage;
    protected $_iCategoryId;
    protected $_vAddress;
    protected $_vLatitude;
    protected $_vLongitude;
    protected $_vZip;
    protected $_vCity;
    protected $_vState;
    protected $_iDeliveryHoursFrom;
    protected $_iDeliveryHoursTo;
    protected $_iPreferDeliveryTimeFrom;
    protected $_iPreferDeliveryTimeTo;
    protected $_iExpireTime;
    protected $_eHowAboutUs;
    protected $_vReferralEmail;
    protected $_eNightDelivery;
    protected $_eActivePaidPlan;
    protected $_eStatus;
    protected $_iDtAdded;
    protected $_vContact;
    protected $_vComment;
    protected $_fAmountPaid;
    protected $_iStoreCount;
    protected $_ePlanType;
    protected $_iCountryId;



    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iUserId = null;
        $this->_iParentId = null;
        $this->_iUserTypeId = null;
        $this->_iUserRoleId = null;
        $this->_iTypeOfStoreId = null;
        $this->_iPlanId = null;
        $this->_iIndustriesId = null;
        $this->_vStoreName = null;
        $this->_vStoreUniqueId = null;
        $this->_NumberOfStore = null;
        $this->_vEmail = null;
        $this->_vPassword = null;
        $this->_vFirstName = null;
        $this->_vLastName = null;
        $this->_vImage = null;
        $this->_iCategoryId = null;
        $this->_vAddress = null;
        $this->_vLatitude = null;
        $this->_vLongitude = null;
        $this->_vZip = null;
        $this->_vCity = null;
        $this->_vState = null;
        $this->_iDeliveryHoursFrom = null;
        $this->_iDeliveryHoursTo = null;
        $this->_iPreferDeliveryTimeFrom = null;
        $this->_iPreferDeliveryTimeTo = null;
        $this->_iExpireTime = null;
        $this->_eHowAboutUs = null;
        $this->_vReferralEmail = null;
        $this->_eNightDelivery = null;
        $this->_eActivePaidPlan = null;
        $this->_eStatus = null;
        $this->_iDtAdded = null;
        $this->_vContact = null;
        $this->_vComment = null;
        $this->_fAmountPaid = null;
        $this->_iStoreCount = null;
        $this->_ePlanType = null;
        $this->_iCountryId = null;


    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiUserId()
    {
        return $this->_iUserId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiUserId($val)
    {
        $this->_iUserId = $val;
    }

    public function getiParentId()
    {
        return $this->_iParentId;
    }

    public function setiParentId($val)
    {
        $this->_iParentId = $val;
    }

    public function getiUserTypeId()
    {
        return $this->_iUserTypeId;
    }

    public function setiUserTypeId($val)
    {
        $this->_iUserTypeId = $val;
    }

    public function getiUserRoleId()
    {
        return $this->_iUserRoleId;
    }

    public function setiUserRoleId($val)
    {
        $this->_iUserRoleId = $val;
    }

    public function getiTypeOfStoreId()
    {
        return $this->_iTypeOfStoreId;
    }

    public function setiTypeOfStoreId($val)
    {
        $this->_iTypeOfStoreId = $val;
    }

    public function getiPlanId()
    {
        return $this->_iPlanId;
    }

    public function setiPlanId($val)
    {
        $this->_iPlanId = $val;
    }

    public function getiIndustriesId()
    {
        return $this->_iIndustriesId;
    }

    public function setiIndustriesId($val)
    {
        $this->_iIndustriesId = $val;
    }

    public function getvStoreName()
    {
        return $this->_vStoreName;
    }

    public function setvStoreName($val)
    {
        $this->_vStoreName = $val;
    }

    public function getvStoreUniqueId()
    {
        return $this->_vStoreUniqueId;
    }

    public function setvStoreUniqueId($val)
    {
        $this->_vStoreUniqueId = $val;
    }

    public function getNumberOfStore()
    {
        return $this->_NumberOfStore;
    }

    public function setNumberOfStore($val)
    {
        $this->_NumberOfStore = $val;
    }

    public function getvEmail()
    {
        return $this->_vEmail;
    }

    public function setvEmail($val)
    {
        $this->_vEmail = $val;
    }

    public function getvPassword()
    {
        return $this->_vPassword;
    }

    public function setvPassword($val)
    {
        $this->_vPassword = $val;
    }

    public function getvFirstName()
    {
        return $this->_vFirstName;
    }

    public function setvFirstName($val)
    {
        $this->_vFirstName = $val;
    }

    public function getvLastName()
    {
        return $this->_vLastName;
    }

    public function setvLastName($val)
    {
        $this->_vLastName = $val;
    }

    public function getvImage()
    {
        return $this->_vImage;
    }

    public function setvImage($val)
    {
        $this->_vImage = $val;
    }

    public function getiCategoryId()
    {
        return $this->_iCategoryId;
    }

    public function setiCategoryId($val)
    {
        $this->_iCategoryId = $val;
    }

    public function getvAddress()
    {
        return $this->_vAddress;
    }

    public function setvAddress($val)
    {
        $this->_vAddress = $val;
    }

    public function getvLatitude()
    {
        return $this->_vLatitude;
    }

    public function setvLatitude($val)
    {
        $this->_vLatitude = $val;
    }

    public function getvLongitude()
    {
        return $this->_vLongitude;
    }

    public function setvLongitude($val)
    {
        $this->_vLongitude = $val;
    }

    public function getvZip()
    {
        return $this->_vZip;
    }

    public function setvZip($val)
    {
        $this->_vZip = $val;
    }

    public function getvCity()
    {
        return $this->_vCity;
    }

    public function setvCity($val)
    {
        $this->_vCity = $val;
    }

    public function getvState()
    {
        return $this->_vState;
    }

    public function setvState($val)
    {
        $this->_vState = $val;
    }

    public function getiDeliveryHoursFrom()
    {
        return $this->_iDeliveryHoursFrom;
    }

    public function setiDeliveryHoursFrom($val)
    {
        $this->_iDeliveryHoursFrom = $val;
    }

    public function getiDeliveryHoursTo()
    {
        return $this->_iDeliveryHoursTo;
    }

    public function setiDeliveryHoursTo($val)
    {
        $this->_iDeliveryHoursTo = $val;
    }

    public function getiPreferDeliveryTimeFrom()
    {
        return $this->_iPreferDeliveryTimeFrom;
    }

    public function setiPreferDeliveryTimeFrom($val)
    {
        $this->_iPreferDeliveryTimeFrom = $val;
    }

    public function getiPreferDeliveryTimeTo()
    {
        return $this->_iPreferDeliveryTimeTo;
    }

    public function setiPreferDeliveryTimeTo($val)
    {
        $this->_iPreferDeliveryTimeTo = $val;
    }

    public function getiExpireTime()
    {
        return $this->_iExpireTime;
    }

    public function setiExpireTime($val)
    {
        $this->_iExpireTime = $val;
    }

    public function geteHowAboutUs()
    {
        return $this->_eHowAboutUs;
    }

    public function seteHowAboutUs($val)
    {
        $this->_eHowAboutUs = $val;
    }

    public function getvReferralEmail()
    {
        return $this->_vReferralEmail;
    }

    public function setvReferralEmail($val)
    {
        $this->_vReferralEmail = $val;
    }

    public function geteNightDelivery()
    {
        return $this->_eNightDelivery;
    }

    public function seteNightDelivery($val)
    {
        $this->_eNightDelivery = $val;
    }

    public function geteActivePaidPlan()
    {
        return $this->_eActivePaidPlan;
    }

    public function seteActivePaidPlan($val)
    {
        $this->_eActivePaidPlan = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function getvContact()
    {
        return $this->_vContact;
    }

    public function setvContact($val)
    {
        $this->_vContact = $val;
    }

    public function getvComment()
    {
        return $this->_vComment;
    }

    public function setvComment($val)
    {
        $this->_vComment = $val;
    }

    public function getfAmountPaid()
    {
        return $this->_fAmountPaid;
    }

    public function setfAmountPaid($val)
    {
        $this->_fAmountPaid = $val;
    }

    public function getiStoreCount()
    {
        return $this->_iStoreCount;
    }

    public function setiStoreCount($val)
    {
        $this->_iStoreCount = $val;
    }

    public function getePlanType()
    {
        return $this->_ePlanType;
    }

    public function setePlanType($val)
    {
        $this->_ePlanType = $val;
    }
    public function getiCountryId()
    {
        return $this->_iCountryId;
    }

    public function setiCountryId($val)
    {
        $this->_iCountryId = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $where = 'Where 1=1';
        if (!is_null($id)) {
            $where .= " AND iUserId = $id";
        }

        $sql = "SELECT * FROM user $where";

        $row = $this->_obj->select($sql);

        $this->_iUserId = $row[0]['iUserId'];
        $this->_iParentId = $row[0]['iParentId'];
        $this->_iUserTypeId = $row[0]['iUserTypeId'];
        $this->_iUserRoleId = $row[0]['iUserRoleId'];
        $this->_iTypeOfStoreId = $row[0]['iTypeOfStoreId'];
        $this->_iPlanId = $row[0]['iPlanId'];
        $this->_iIndustriesId = $row[0]['iIndustriesId'];
        $this->_vStoreName = $row[0]['vStoreName'];
        $this->_vStoreUniqueId = $row[0]['vStoreUniqueId'];
        $this->_NumberOfStore = $row[0]['NumberOfStore'];
        $this->_vEmail = $row[0]['vEmail'];
        $this->_vPassword = $row[0]['vPassword'];
        $this->_vFirstName = $row[0]['vFirstName'];
        $this->_vLastName = $row[0]['vLastName'];
        $this->_vImage = $row[0]['vImage'];
        $this->_iCategoryId = $row[0]['iCategoryId'];
        $this->_vAddress = $row[0]['vAddress'];
        $this->_vLatitude = $row[0]['vLatitude'];
        $this->_vLongitude = $row[0]['vLongitude'];
        $this->_vZip = $row[0]['vZip'];
        $this->_vCity = $row[0]['vCity'];
        $this->_vState = $row[0]['vState'];
        $this->_iDeliveryHoursFrom = $row[0]['iDeliveryHoursFrom'];
        $this->_iDeliveryHoursTo = $row[0]['iDeliveryHoursTo'];
        $this->_iPreferDeliveryTimeFrom = $row[0]['iPreferDeliveryTimeFrom'];
        $this->_iPreferDeliveryTimeTo = $row[0]['iPreferDeliveryTimeTo'];
        $this->_iExpireTime = $row[0]['iExpireTime'];
        $this->_eHowAboutUs = $row[0]['eHowAboutUs'];
        $this->_vReferralEmail = $row[0]['vReferralEmail'];
        $this->_eNightDelivery = $row[0]['eNightDelivery'];
        $this->_eActivePaidPlan = $row[0]['eActivePaidPlan'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_vContact = $row[0]['vContact'];
        $this->_vComment = $row[0]['vComment'];
        $this->_fAmountPaid = $row[0]['fAmountPaid'];
        $this->_iStoreCount = $row[0]['iStoreCount'];
        $this->_iCountryId = $row[0]['iCountryId'];

        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM user WHERE iUserId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iUserId = ""; // clear key for autoincrement

        $sql = "INSERT INTO user ( iParentId,iUserTypeId,iUserRoleId,iTypeOfStoreId,iPlanId,iIndustriesId,vStoreName,vStoreUniqueId,NumberOfStore,vEmail,vPassword,vFirstName,vLastName,vImage,iCategoryId,vAddress,vLatitude,vLongitude,vZip,vCity,vState,iDeliveryHoursFrom,iDeliveryHoursTo,iPreferDeliveryTimeFrom,iPreferDeliveryTimeTo,iExpireTime,eHowAboutUs,vReferralEmail,eNightDelivery,eActivePaidPlan,eStatus,iDtAdded,vContact,vComment,fAmountPaid,iStoreCount,ePlanType,iCountryId ) VALUES ( '" . $this->_iParentId . "','" . $this->_iUserTypeId . "','" . $this->_iUserRoleId . "','" . $this->_iTypeOfStoreId . "','" . $this->_iPlanId . "','" . $this->_iIndustriesId . "','" . $this->_vStoreName . "','" . $this->_vStoreUniqueId . "','" . $this->_NumberOfStore . "','" . $this->_vEmail . "','" . $this->_vPassword . "','" . $this->_vFirstName . "','" . $this->_vLastName . "','" . $this->_vImage . "','" . $this->_iCategoryId . "','" . $this->_vAddress . "','" . $this->_vLatitude . "','" . $this->_vLongitude . "','" . $this->_vZip . "','" . $this->_vCity . "','" . $this->_vState . "','" . $this->_iDeliveryHoursFrom . "','" . $this->_iDeliveryHoursTo . "','" . $this->_iPreferDeliveryTimeFrom . "','" . $this->_iPreferDeliveryTimeTo . "','" . $this->_iExpireTime . "','" . $this->_eHowAboutUs . "','" . $this->_vReferralEmail . "','" . $this->_eNightDelivery . "','" . $this->_eActivePaidPlan . "','" . $this->_eStatus . "','" . $this->_iDtAdded . "','" . $this->_vContact . "','" . $this->_vComment . "','" . $this->_fAmountPaid . "','" . $this->_iStoreCount . "','" . $this->_ePlanType . "','" . $this->_iCountryId . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE user SET  iParentId = '" . $this->_iParentId . "' , iUserTypeId = '" . $this->_iUserTypeId . "' , iUserRoleId = '" . $this->_iUserRoleId . "' , iTypeOfStoreId = '" . $this->_iTypeOfStoreId . "' , iPlanId = '" . $this->_iPlanId . "' , iIndustriesId = '" . $this->_iIndustriesId . "' , vStoreName = '" . $this->_vStoreName . "' , vStoreUniqueId = '" . $this->_vStoreUniqueId . "' , NumberOfStore = '" . $this->_NumberOfStore . "' , vEmail = '" . $this->_vEmail . "' , vPassword = '" . $this->_vPassword . "' , vFirstName = '" . $this->_vFirstName . "' , vLastName = '" . $this->_vLastName . "' , vImage = '" . $this->_vImage . "' , iCategoryId = '" . $this->_iCategoryId . "' , vAddress = '" . $this->_vAddress . "' , vLatitude = '" . $this->_vLatitude . "' , vLongitude = '" . $this->_vLongitude . "' , vZip = '" . $this->_vZip . "' , vCity = '" . $this->_vCity . "' , vState = '" . $this->_vState . "' , iDeliveryHoursFrom = '" . $this->_iDeliveryHoursFrom . "' , iDeliveryHoursTo = '" . $this->_iDeliveryHoursTo . "' , iPreferDeliveryTimeFrom = '" . $this->_iPreferDeliveryTimeFrom . "' , iPreferDeliveryTimeTo = '" . $this->_iPreferDeliveryTimeTo . "' , iExpireTime = '" . $this->_iExpireTime . "' , eHowAboutUs = '" . $this->_eHowAboutUs . "' , vReferralEmail = '" . $this->_vReferralEmail . "' , eNightDelivery = '" . $this->_eNightDelivery . "' , eActivePaidPlan = '" . $this->_eActivePaidPlan . "' , eStatus = '" . $this->_eStatus . "' , iDtAdded = '" . $this->_iDtAdded . "' , vContact = '" . $this->_vContact . "' , vComment = '" . $this->_vComment . "', fAmountPaid = '" . $this->_fAmountPaid . "', iStoreCount = '" . $this->_iStoreCount . "', ePlanType = '" . $this->_ePlanType . "', iCountryId = '" . $this->_iCountryId . "'  WHERE iUserId = $id ";
        $this->_obj->sql_query($sql);

    }

    function update_profile_img($id)
    {

        $sql = " UPDATE user SET  vImage = '" . $this->_vImage . "'  WHERE iUserId = $id ";
        $this->_obj->sql_query($sql);

    }
    
    function getStoreName($uid)
    {
        $sql = "SELECT vStoreUniqueId,vFirstName,vLastName,vStoreName  FROM user WHERE iUserId = $uid";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function check_exist_mail($mail)
    {
        $sql = "SELECT vFirstName,vLastName,iUserId,vEmail FROM user WHERE vEmail = '$mail'";
        $row = $this->_obj->select($sql);
        return $row;

    }

    function check_exist_mail_withid($mail, $id)
    {
        $sql = "SELECT vEmail FROM user WHERE vEmail = '$mail' AND iUserId!='$id'";
        $row = $this->_obj->select($sql);
        return $row;

    }

    function updateuser($id)
    {

        $sql = " UPDATE user SET   iUserTypeId = '" . $this->_iUserTypeId . "' , iUserRoleId = '" . $this->_iUserRoleId . "' , iTypeOfStoreId = '" . $this->_iTypeOfStoreId . "' , iIndustriesId = '" . $this->_iIndustriesId . "' , vStoreName = '" . $this->_vStoreName . "' , vStoreUniqueId = '" . $this->_vStoreUniqueId . "' , NumberOfStore = '" . $this->_NumberOfStore . "' , vEmail = '" . $this->_vEmail . "' , vFirstName = '" . $this->_vFirstName . "' , vLastName = '" . $this->_vLastName . "' , vImage = '" . $this->_vImage . "' , iCategoryId = '" . $this->_iCategoryId . "' , vAddress = '" . $this->_vAddress . "' , vLatitude = '" . $this->_vLatitude . "' , vLongitude = '" . $this->_vLongitude . "' , vZip = '" . $this->_vZip . "' , vCity = '" . $this->_vCity . "' , vState = '" . $this->_vState . "' , iDeliveryHoursFrom = '" . $this->_iDeliveryHoursFrom . "' , iDeliveryHoursTo = '" . $this->_iDeliveryHoursTo . "' , iPreferDeliveryTimeFrom = '" . $this->_iPreferDeliveryTimeFrom . "' , iPreferDeliveryTimeTo = '" . $this->_iPreferDeliveryTimeTo . "' ,eNightDelivery = '" . $this->_eNightDelivery . "' , eStatus = '" . $this->_eStatus . "' , iDtAdded = '" . $this->_iDtAdded . "' , vContact = '" . $this->_vContact . "' , vComment = '" . $this->_vComment . "',iCountryId = '" . $this->_iCountryId . "'  WHERE iUserId = $id ";
        $this->_obj->sql_query($sql);

    }

    /* API : Demo */
    function selectall()
    {
        $sql = "SELECT * FROM user WHERE iUserId = 1";
        $row = $this->_obj->select($sql);
        return $row;
    }

    /* API : signup_retailer_single, forgotpassword
      For :  check useremail */
    function func_check_existing_email($email)
    {
        $sql = "SELECT COUNT(iUserId) as total FROM user WHERE vEmail = '{$email}'";
        $result = $this->_obj->sql_query($sql);
        if ($result[0]['total'] > 0)
            return false;
        else
            return true;
    }

    function func_check_existing_account($vStoreUniqueId, $id = null)
    {
        if ($id != null) {
            $WHERE = "WHERE vStoreUniqueId = '$vStoreUniqueId' AND iUserId!='$id'";
        } else {
            $WHERE = "WHERE vStoreUniqueId = '$vStoreUniqueId'";
        }
        $sql = "SELECT COUNT(iUserId) as total FROM user $WHERE";
        $result = $this->_obj->sql_query($sql);
        if ($result[0]['total'] > 0)
            return false;
        else
            return true;
    }

    /* API : signup_retailer_single
      For: register/signup  */
    function registerRetailerSingle()
    {
        $this->iUserId = ""; // clear key for autoincrement

        //$sql = "INSERT INTO user ( iParentId,iUserTypeId,iTypeOfStoreId,iIndustriesId,vStoreName,vStoreUniqueId,NumberOfStore,vEmail,vPassword,vFirstName,vLastName,vAddress,vLatitude,vLongitude,vZip,vCity,vState,iPreferDeliveryTimeFrom,iPreferDeliveryTimeTo,eHowAboutUs,eNightDelivery,eStatus,iDtAdded,eActivePaidPlan,iPlanId) VALUES ( '" . $this->_iParentId . "','" . $this->_iUserTypeId . "','" . $this->_iTypeOfStoreId . "','" . $this->_iIndustriesId . "','" . $this->_vStoreName . "','" . $this->_vStoreUniqueId . "','" . $this->_NumberOfStore . "','" . $this->_vEmail . "','" . $this->_vPassword . "','" . $this->_vFirstName . "','" . $this->_vLastName . "','" . $this->_vAddress . "','" . $this->_vLatitude . "','" . $this->_vLongitude . "','" . $this->_vZip . "','" . $this->_vCity . "','" . $this->_vState . "','" . $this->_iPreferDeliveryTimeFrom . "','" . $this->_iPreferDeliveryTimeTo . "','" . $this->_eHowAboutUs . "','" . $this->_eNightDelivery . "','" . $this->_eStatus . "','" . $this->_iDtAdded . "','" . $this->_eActivePaidPlan . "','" . $this->_iPlanId . "' )";
        $sql = "INSERT INTO user SET
               vStoreUniqueId = '$this->_vStoreUniqueId',
               vStoreName = '$this->_vStoreName',
               vEmail = '$this->_vEmail',
               vPassword = '$this->_vPassword',
               vFirstName = '$this->_vFirstName',
               vLastName = '$this->_vLastName',
               vAddress = '$this->_vAddress',
               vZip = '$this->_vZip',
               vCity = '$this->_vCity',
               vState = '$this->_vState',
               iIndustriesId = '$this->_iIndustriesId',
               iTypeOfStoreId = '$this->_iTypeOfStoreId',
               iPreferDeliveryTimeFrom = '$this->_iPreferDeliveryTimeFrom',
               iPreferDeliveryTimeTo = '$this->_iPreferDeliveryTimeTo',
               iDeliveryHoursFrom = '$this->_iDeliveryHoursFrom',
               iDeliveryHoursTo = '$this->_iDeliveryHoursTo',
               eNightDelivery = '$this->_eNightDelivery',
               eHowAboutUs = '$this->_eHowAboutUs',
               vContact = '$this->_vContact',
               ePlanType = '$this->_ePlanType',
               eActivePaidPlan = '$this->_eActivePaidPlan',
               iExpireTime = '$this->_iExpireTime',
               fAmountPaid = '$this->_fAmountPaid',
               iStoreCount = '$this->_iStoreCount',
               vReferralEmail = '$this->_vReferralEmail',
               vLatitude = '$this->_vLatitude',
               vLongitude = '$this->_vLongitude',
               iUserTypeId = '$this->_iUserTypeId',
               iPlanId = '$this->_iPlanId',
               iCountryId = '$this->_iCountryId',
               iDtAdded = '$this->_iDtAdded',
               eStatus = '$this->_eStatus'";

        $result = $this->_obj->insert($sql);
        return $result;
    }


    /* API : login
      For: login  */
    function userInfo($userid)
    {
        $sql = "SELECT * FROM user AS u
                WHERE eStatus = '1' AND u.iUserId='{$userid}'";
        $RET = $this->_obj->sql_query($sql);
        return $RET[0];
    }

    /* API : login
      For: login  */
    function func_check_login($email, $password, $type)
    {
        $sql = "SELECT iUserId,iUserTypeId,eStatus,vImage,vStoreName,vFirstName,vLastName,vAddress,vZip,vCity,vState,iPreferDeliveryTimeFrom,iPreferDeliveryTimeTo,eNightDelivery FROM user WHERE vEmail = '{$email}' AND vPassword = '{$password}' AND iUserTypeId IN ($type) AND eStatus!='2' ";
        $result = $this->_obj->sql_query($sql);
        return $result[0];
    }

    /* API : forgotpassword
      For: to get user information for mail  */
    function get_userdetail_from_email($email)
    {
        $sql = "SELECT * FROM user WHERE vEmail='" . $email . "' AND eStatus='1'";
        return $this->_obj->select($sql);
    }

    /* API : forgotpassword
      For: to set new password  */
    function updatePassword($userid)
    {
        $query = " UPDATE user SET vPassword='" . $this->_vPassword . "' WHERE iUserId='" . $userid . "'";
        return $this->_obj->sql_query($query);
    }

    /* API : signup_retailer_single, signup_retailer_
          For: to check store unique id  */
    function check_store_unique_id($uid)
    {
        $sql = "SELECT COUNT(iUserId) as total FROM user WHERE vStoreUniqueId = '{$uid}'";
        $result = $this->_obj->sql_query($sql);
        //print_r($result);
        if ($result[0]['total'] > 0)
            return false;
        else
            return true;
    }

    /* API : signup_retailer_multiple
      For: register/signup for retailer multiple store  */
    function registerRetailerMultiple()
    {
        $this->iUserId = ""; // clear key for autoincrement

        $sql = "INSERT INTO user SET
               vStoreUniqueId = '$this->_vStoreUniqueId',
               vStoreName = '$this->_vStoreName',
               vEmail = '$this->_vEmail',
               NumberOfStore = '$this->_NumberOfStore',
               vComment = '$this->_vComment',
               vPassword = '$this->_vPassword',
               vFirstName = '$this->_vFirstName',
               vLastName = '$this->_vLastName',
               vAddress = '$this->_vAddress',
               vZip = '$this->_vZip',
               vCity = '$this->_vCity',
               vState = '$this->_vState',
               iIndustriesId = '$this->_iIndustriesId',
               iTypeOfStoreId = '$this->_iTypeOfStoreId',
               iDeliveryHoursFrom = '$this->_iDeliveryHoursFrom',
               iDeliveryHoursTo = '$this->_iDeliveryHoursTo',
               iPreferDeliveryTimeFrom = '$this->_iPreferDeliveryTimeFrom',
               iPreferDeliveryTimeTo = '$this->_iPreferDeliveryTimeTo',
               eNightDelivery = '$this->_eNightDelivery',
               eHowAboutUs = '$this->_eHowAboutUs',
               vContact = '$this->_vContact',
               ePlanType = '$this->_ePlanType',
               eActivePaidPlan = '$this->_eActivePaidPlan',
               iExpireTime = '$this->_iExpireTime',
               fAmountPaid = '$this->_fAmountPaid',
               iStoreCount = '$this->_iStoreCount',
               vReferralEmail = '$this->_vReferralEmail',
               vLatitude = '$this->_vLatitude',
               vLongitude = '$this->_vLongitude',
               iUserTypeId = '$this->_iUserTypeId',
               iPlanId = '$this->_iPlanId',
               iCountryId = '$this->_iCountryId',
               iDtAdded = '$this->_iDtAdded',
               eStatus = '$this->_eStatus'";
        $result = $this->_obj->insert($sql);
        return $result;
    }

    /* API : signup_retailer_corporate
      For: register/signup for retailer multiple store  */
    function registerRetailerCorporate()
    {
        $this->iUserId = ""; // clear key for autoincrement

        $sql = "INSERT INTO user SET
               vStoreUniqueId = '$this->_vStoreUniqueId',
vStoreName = '$this->_vStoreName',
               iParentId = '$this->_iParentId',
               vEmail = '$this->_vEmail',
               NumberOfStore = '$this->_NumberOfStore',
               vPassword = '$this->_vPassword',
               vFirstName = '$this->_vFirstName',
               vLastName = '$this->_vLastName',
               vAddress = '$this->_vAddress',
               vZip = '$this->_vZip',
               vCity = '$this->_vCity',
               vState = '$this->_vState',
               iIndustriesId = '$this->_iIndustriesId',
               iTypeOfStoreId = '$this->_iTypeOfStoreId',
               iDeliveryHoursFrom = '$this->_iDeliveryHoursFrom',
               iDeliveryHoursTo = '$this->_iDeliveryHoursTo',
               iPreferDeliveryTimeFrom = '$this->_iPreferDeliveryTimeFrom',
               iPreferDeliveryTimeTo = '$this->_iPreferDeliveryTimeTo',
               eNightDelivery = '$this->_eNightDelivery',
               eHowAboutUs = '$this->_eHowAboutUs',
               vContact = '$this->_vContact',
               ePlanType = '$this->_ePlanType',
               eActivePaidPlan = '$this->_eActivePaidPlan',
               iExpireTime = '$this->_iExpireTime',
               fAmountPaid = '$this->_fAmountPaid',
               iStoreCount = '$this->_iStoreCount',
               vReferralEmail = '$this->_vReferralEmail',
               vLatitude = '$this->_vLatitude',
               vLongitude = '$this->_vLongitude',
               iUserTypeId = '$this->_iUserTypeId',
               iPlanId = '$this->_iPlanId',
               iCountryId = '$this->_iCountryId',
               iDtAdded = '$this->_iDtAdded',
               eStatus = '$this->_eStatus'";
        $result = $this->_obj->insert($sql);
        return $result;
    }

   /* API : user_setting
     For :  check old password is correct */
    function get_detail_chnage_pwd($pwd,$uid)
    {
       $sql = "SELECT * FROM user where iUserId='$uid' AND vPassword='$pwd' AND eStatus='1'";
       $row = $this->_obj->select($sql);
        return $row;
    }

    /* API : broadcast_new_store_opening
     For :  check userid */
    function func_check_existing_userid($userid)
    {
        $sql = "SELECT COUNT(iUserId) as total FROM user WHERE iUserId = '{$userid}' AND eStatus='1'";
        $result = $this->_obj->sql_query($sql);

        if ($result[0]['total'] > 0)
            return true;
        else
            return false;
    }

    /* API : signup_retailer_single
     For: register/signup  */
    function registerCorporate()
    {
        $this->iUserId = ""; // clear key for autoincrement

        $sql = "INSERT INTO user ( iParentId,iUserTypeId,iCategoryId,iIndustriesId,vStoreName,vEmail,vPassword,vFirstName,vLastName,vAddress,vLatitude,vLongitude,vZip,vCity,vState,eHowAboutUs,eStatus,iDtAdded,eActivePaidPlan,iPlanId,vContact) VALUES ( '" . $this->_iParentId . "','" . $this->_iUserTypeId . "','" . $this->_iCategoryId . "','" . $this->_iIndustriesId . "','" . $this->_vStoreName . "','" . $this->_vEmail . "','" . $this->_vPassword . "','" . $this->_vFirstName . "','" . $this->_vLastName . "','" . $this->_vAddress . "','" . $this->_vLatitude . "','" . $this->_vLongitude . "','" . $this->_vZip . "','" . $this->_vCity . "','" . $this->_vState . "','" . $this->_eHowAboutUs . "','" . $this->_eStatus . "','" . $this->_iDtAdded . "','" . $this->_eActivePaidPlan . "','" . $this->_iPlanId . "','" . $this->_vContact . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }

    /* API : signup_Distributor
           For: register/signup  */
    function registerDistributor()
    {
        $this->iUserId = ""; // clear key for autoincrement

        //$sql = "INSERT INTO user ( iParentId,iUserTypeId,iTypeOfStoreId,iIndustriesId,vStoreName,vStoreUniqueId,NumberOfStore,vEmail,vPassword,vFirstName,vLastName,vAddress,vLatitude,vLongitude,vZip,vCity,vState,iPreferDeliveryTimeFrom,iPreferDeliveryTimeTo,eHowAboutUs,eNightDelivery,eStatus,iDtAdded,eActivePaidPlan,iPlanId) VALUES ( '" . $this->_iParentId . "','" . $this->_iUserTypeId . "','" . $this->_iTypeOfStoreId . "','" . $this->_iIndustriesId . "','" . $this->_vStoreName . "','" . $this->_vStoreUniqueId . "','" . $this->_NumberOfStore . "','" . $this->_vEmail . "','" . $this->_vPassword . "','" . $this->_vFirstName . "','" . $this->_vLastName . "','" . $this->_vAddress . "','" . $this->_vLatitude . "','" . $this->_vLongitude . "','" . $this->_vZip . "','" . $this->_vCity . "','" . $this->_vState . "','" . $this->_iPreferDeliveryTimeFrom . "','" . $this->_iPreferDeliveryTimeTo . "','" . $this->_eHowAboutUs . "','" . $this->_eNightDelivery . "','" . $this->_eStatus . "','" . $this->_iDtAdded . "','" . $this->_eActivePaidPlan . "','" . $this->_iPlanId . "' )";
        $sql = "INSERT INTO user SET
               vStoreUniqueId = '$this->_vStoreUniqueId',
               vEmail = '$this->_vEmail',
               vPassword = '$this->_vPassword',
               vFirstName = '$this->_vFirstName',
               vLastName = '$this->_vLastName',
               vAddress = '$this->_vAddress',
               vZip = '$this->_vZip',
               vCity = '$this->_vCity',
               vState = '$this->_vState',
               iIndustriesId = '$this->_iIndustriesId',
               eHowAboutUs = '$this->_eHowAboutUs',
               vLatitude = '$this->_vLatitude',
               vLongitude = '$this->_vLongitude',
               iUserTypeId = '$this->_iUserTypeId',
               iPlanId = '$this->_iPlanId',
               iExpireTime = '$this->_iExpireTime',
               iCategoryId = '$this->_iCategoryId',
               iUserRoleId = '$this->_iUserRoleId',
               iParentId = '$this->_iParentId',
               vContact = '$this->_vContact',
               eActivePaidPlan = '$this->_eActivePaidPlan',
               ePlanType = '$this->_ePlanType',
               fAmountPaid = '$this->_fAmountPaid',
               iStoreCount = '$this->_iStoreCount',
               vReferralEmail = '$this->_vReferralEmail',
               iCountryId = '$this->_iCountryId',
               iDtAdded = '$this->_iDtAdded',
               eStatus = '$this->_eStatus'";

        $result = $this->_obj->insert($sql);
        return $result;
    }

    /* API : signup_Distributor
           For: register/signup  */
    function registerManufacturer()
    {
        $this->iUserId = ""; // clear key for autoincrement

        //$sql = "INSERT INTO user ( iParentId,iUserTypeId,iTypeOfStoreId,iIndustriesId,vStoreName,vStoreUniqueId,NumberOfStore,vEmail,vPassword,vFirstName,vLastName,vAddress,vLatitude,vLongitude,vZip,vCity,vState,iPreferDeliveryTimeFrom,iPreferDeliveryTimeTo,eHowAboutUs,eNightDelivery,eStatus,iDtAdded,eActivePaidPlan,iPlanId) VALUES ( '" . $this->_iParentId . "','" . $this->_iUserTypeId . "','" . $this->_iTypeOfStoreId . "','" . $this->_iIndustriesId . "','" . $this->_vStoreName . "','" . $this->_vStoreUniqueId . "','" . $this->_NumberOfStore . "','" . $this->_vEmail . "','" . $this->_vPassword . "','" . $this->_vFirstName . "','" . $this->_vLastName . "','" . $this->_vAddress . "','" . $this->_vLatitude . "','" . $this->_vLongitude . "','" . $this->_vZip . "','" . $this->_vCity . "','" . $this->_vState . "','" . $this->_iPreferDeliveryTimeFrom . "','" . $this->_iPreferDeliveryTimeTo . "','" . $this->_eHowAboutUs . "','" . $this->_eNightDelivery . "','" . $this->_eStatus . "','" . $this->_iDtAdded . "','" . $this->_eActivePaidPlan . "','" . $this->_iPlanId . "' )";
        $sql = "INSERT INTO user SET
               vStoreUniqueId = '$this->_vStoreUniqueId',
               vEmail = '$this->_vEmail',
               vPassword = '$this->_vPassword',
               vFirstName = '$this->_vFirstName',
               vLastName = '$this->_vLastName',
               vAddress = '$this->_vAddress',
               vZip = '$this->_vZip',
               vCity = '$this->_vCity',
               vState = '$this->_vState',
               iExpireTime = '$this->_iExpireTime',
               iIndustriesId = '$this->_iIndustriesId',
               eHowAboutUs = '$this->_eHowAboutUs',
               vLatitude = '$this->_vLatitude',
               vLongitude = '$this->_vLongitude',
               iUserTypeId = '$this->_iUserTypeId',
               iPlanId = '$this->_iPlanId',
               iCategoryId = '$this->_iCategoryId',
               iUserRoleId = '$this->_iUserRoleId',
               iParentId = '$this->_iParentId',
               vContact = '$this->_vContact',
               eActivePaidPlan = '$this->_eActivePaidPlan',
               ePlanType = '$this->_ePlanType',
               fAmountPaid = '$this->_fAmountPaid',
               iStoreCount = '$this->_iStoreCount',
               vReferralEmail = '$this->_vReferralEmail',
               iCountryId = '$this->_iCountryId',
               iDtAdded = '$this->_iDtAdded',
               eStatus = '$this->_eStatus'";

        $result = $this->_obj->insert($sql);
        return $result;
    }



  function GENERATE_SC_QR_CODE($ID, $account=null)
    {
        global $generalfuncobj;
        ## GET RANDOM NUMBER
 $NUMBER = '';
        if($account==null)
        {
            $NUMBER = $generalfuncobj->generateNumericUniqueToken(10);
        }
else
        {
            $NUMBER = $account . $generalfuncobj->generateNumericUniqueToken(7);
        }
       // echo $NUMBER; exit;
        ## CHECK RENDOM NUMBER
        $SQL = "SELECT COUNT(iRetailerDetailId) as tot FROM retailer_detail WHERE vOriginal_code = '{$NUMBER}'";
        $RESULT = $this->_obj->sql_query($SQL);

        if ($RESULT[0]['tot'] > 0) {
            $this->GENERATE_SC_QR_CODE($ID);
        } else {
            ## GENERATE QR CODE IMG
            $QR_CODE = $generalfuncobj->QR_CODE($NUMBER, $ID);

            ## CHECK USER
            $SQL_USER = "SELECT iRetailerDetailId FROM retailer_detail WHERE iUserId = '{$ID}'";
            $RESULT_USER = $this->_obj->sql_query($SQL_USER);

            global $inc_class_path;
            global $vRefference_Code;
            include_once($inc_class_path . 'retailer_detail.class.php');
            $retailer_detailObj = new retailer_detail();

            if (isset($RESULT_USER[0]['iRetailerDetailId'])) {
                ## UPDATE
                $retailer_detailObj->setvRefference_Code($vRefference_Code . $NUMBER);
                $retailer_detailObj->setvOriginal_code($NUMBER);
                $retailer_detailObj->setvQRImage($QR_CODE);
                $retailer_detailObj->update($RESULT_USER[0]['iRetailerDetailId']);
            } else {
                ## INSERT
                $retailer_detailObj->setiUserId($ID);
                $retailer_detailObj->setvRefference_Code($vRefference_Code . $NUMBER);
                $retailer_detailObj->setvOriginal_code($NUMBER);
                $retailer_detailObj->setvQRImage($QR_CODE);
                $retailer_detailObj->setiDtAdded($generalfuncobj->gm_date_string());
                $retailer_detailObj->seteStatus('1');
                $retailer_detailObj->insert();
            }
            #return $NUMBER;
        }
    }

    function update_user_settings($iUserId)
    {
        $sql = " UPDATE user SET ";
        $sql .= ($this->_vEmail != null) ? ", vEmail = '" . $this->_vEmail . "'," : "";
         $sql .= ($this->_vContact != null) ? ", vContact = '" . $this->_vContact . "'," : "";
        $sql .= ($this->_vPassword != null) ? ", vPassword = '" . $this->_vPassword . "'," : "";
        $sql .= ($this->_vFirstName != null) ? ", vFirstName = '" . $this->_vFirstName . "'," : "";
        $sql .= ($this->_vLastName != null) ? ", vLastName = '" . $this->_vLastName . "'," : "";
        $sql .= ($this->_vStoreName != null) ? ", vStoreName = '" . $this->_vStoreName . "'," : "";
        $sql .= ($this->_vStoreUniqueId != null) ? ", vStoreUniqueId = '" . $this->_vStoreUniqueId . "'," : "";
        $sql .= ($this->_iIndustriesId != null) ? ", iIndustriesId = '" . $this->_iIndustriesId . "'," : "";
        $sql .= ($this->_vAddress != null) ? ", vAddress = '" . $this->_vAddress . "'," : "";
        $sql .= ($this->_vLatitude != null) ? ",vLatitude = '" . $this->_vLatitude . "'," : "";
        $sql .= ($this->_vLongitude != null) ? ",vLongitude = '" . $this->_vLongitude . "'," : "";
       
        $sql .= ($this->_vZip != null) ? ", vZip = '" . $this->_vZip . "'," : "";
        $sql .= ($this->_vCity != null) ? ", vCity = '" . $this->_vCity . "'," : "";
        $sql .= ($this->_vState != null) ? ", vState = '" . $this->_vState . "'," : "";
        $sql .= ($this->_iTypeOfStoreId != null) ? ", iTypeOfStoreId = '" . $this->_iTypeOfStoreId . "'," : "";
        $sql .= ($this->_iPreferDeliveryTimeFrom != null) ? ", iPreferDeliveryTimeFrom = '" . $this->_iPreferDeliveryTimeFrom . "'," : "";
        $sql .= ($this->_iPreferDeliveryTimeTo != null) ? ", iPreferDeliveryTimeTo = '" . $this->_iPreferDeliveryTimeTo . "'," : "";
        $sql .= ($this->_eNightDelivery != null) ? ", eNightDelivery = '" . $this->_eNightDelivery . "'," : "";
        $sql .= ($this->_iUserId != null) ? ", iUserId = '" . $this->_iUserId . "', " : "";
        $sql .= "WHERE iUserId = $iUserId ";

        $UPDATE = clean_sql($sql);

        $this->_obj->sql_query($UPDATE);
    }

    function get_data($uid, $type)
    {
        global $user_image_path, $user_image_url, $qr_image_url, $qr_image_path, $dafault_image_logo_user,$dafault_image_logo,$inc_class_path;


        $sql = "SELECT ur.vName as role_name ,u.* From user u left join user_role ur on u.iUserRoleId=ur.iUserRoleId  WHERE u.eStatus = '1' AND u.iUserId = '$uid' ";
        $user_data_arr = $this->_obj->select($sql);

        $user_data = $user_data_arr[0];

        $vImage = $user_data['vImage'];


        $image_path = $user_image_path . $vImage;
        $image_url = $user_image_url . $vImage;

        $data['vImage'] = is_file($image_path) ? $image_url : $dafault_image_logo_user;


        $data['iUserId'] = $user_data['iUserId'];;
        $data['vFirstName'] = $user_data['vFirstName'];
        $data['vLastName'] = $user_data['vLastName'];
        $data['vStoreUniqueId'] = $user_data['vStoreUniqueId'];
        $data['vEmail'] = $user_data['vEmail'];;
      
        $data['vCity'] = $user_data['vCity'];
        $data['vState'] = $user_data['vState'];
        $data['iCountryId'] = $user_data['iCountryId'];
        $data['vZip'] = $user_data['vZip'];
        $data['vLatitude'] = $user_data['vLatitude'];
        $data['vLongitude'] = $user_data['vLongitude'];
        $data['iIndustriesId'] = $user_data['iIndustriesId'];
        $data['eHowAboutUs'] = $user_data['eHowAboutUs'];
        $data['iPlan_type'] = $user_data['eActivePaidPlan'];
        $data['iUserRoleId'] = $user_data['iUserRoleId'];
        $data['vRoleName'] = $user_data['role_name'];

        include_once($inc_class_path . 'referral.class.php');
        $referralObj = new referral();
        $Points=$referralObj->get_total_point($uid);

        $data['iPoint'] =  $Points['0']['vPoint'];
        $data['vContact'] = $user_data['vContact'];
        $data['iExpireTime'] = date('m-d-Y h:i:s', $user_data['iExpireTime']);
   $data['vStoreName'] = $user_data['vStoreName'];

        if ($type == "6") {

            $data['iParentId'] = $user_data['iParentId'];

        }

        if ($type == "7") {
            $adrs = explode("#@", $user_data['vAddress']);
            $vAddress = $adrs[0];
            $street = $adrs[1];
            $data['vAddress'] = $vAddress;
            $data['vStreet'] = $street;


            $data['iCategoryId'] = $user_data['iCategoryId'];
        } else {
            $data['vAddress'] = $user_data['vAddress'];
        }

        if ($type == "4" || $type == "5" || $type == "6") {
            $data['iTypeOfStoreId'] = $user_data['iTypeOfStoreId'];
            $data['iPreferDeliveryTimeFrom'] = $user_data['iPreferDeliveryTimeFrom'];
            $data['iPreferDeliveryTimeTo'] = $user_data['iPreferDeliveryTimeTo'];
            $data['eNightDelivery'] = $user_data['eNightDelivery'];
$data['iDeliveryHoursFrom'] = $user_data['iDeliveryHoursFrom'];
            $data['iDeliveryHoursTo'] = $user_data['iDeliveryHoursTo'];

            include_once($inc_class_path . 'retailer_detail.class.php');
            $retailer_detailObj = new retailer_detail();
            $qr_data = $retailer_detailObj->get_detail_by_retailer($uid);
            $qrImg = $qr_data[0]['vQRImage'];
            $qr_path = $qr_image_path . $qrImg;
            $qr_url = $qr_image_url . $qrImg;

            $data['vQrCodeURL'] = is_file($qr_path) ? $qr_url : $dafault_image_logo;
            $data['vQrCode'] = $qr_data[0]['vOriginal_code'];
         


        }

        if ($type == "5") {

            $data['NumberOfStore'] = $user_data['NumberOfStore'];

        }

       
include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();

   $setting=$user_settingsObj->select_user_setting($uid);
 $data['eNotification'] =$setting[0]['eNotification'];
        $type=$this->select_employee($data['iUserId']);
        if(count($type)=="1")
        {
            $data['eUser_Type']="0";
        }
        else
        {
            $data['eUser_Type']="1";
        }
        return $data;


    }


    /* API : general data & front side registration for corporate account
          For: List of corporate Retailers  */

    function select_API()
    {
        $sql = "SELECT iUserId,vStoreUniqueId FROM user WHERE eStatus = '1' AND iUserTypeId= '6' AND iParentId='0' ";
        $row = $this->_obj->select($sql);
        return $row;
    }


    function get_detail_forget_pwd($email, $type)
    {
        $sql = "SELECT count(iUserId) as total FROM user WHERE eStatus = '1' AND iUserTypeId IN ($type) AND vEmail='$email' ";
        $row = $this->_obj->select($sql);
       
        if ($row[0]['total'] > 0) {
            return true;
        } else {
            return false;
        }


    }

    /* API : guest_user
          For: Regiattration of guest user  */
    function insert_guest()
    {
        $this->iUserId = ""; // clear key for autoincrement

        //$sql = "INSERT INTO user ( iParentId,iUserTypeId,iTypeOfStoreId,iIndustriesId,vStoreName,vStoreUniqueId,NumberOfStore,vEmail,vPassword,vFirstName,vLastName,vAddress,vLatitude,vLongitude,vZip,vCity,vState,iPreferDeliveryTimeFrom,iPreferDeliveryTimeTo,eHowAboutUs,eNightDelivery,eStatus,iDtAdded,eActivePaidPlan,iPlanId) VALUES ( '" . $this->_iParentId . "','" . $this->_iUserTypeId . "','" . $this->_iTypeOfStoreId . "','" . $this->_iIndustriesId . "','" . $this->_vStoreName . "','" . $this->_vStoreUniqueId . "','" . $this->_NumberOfStore . "','" . $this->_vEmail . "','" . $this->_vPassword . "','" . $this->_vFirstName . "','" . $this->_vLastName . "','" . $this->_vAddress . "','" . $this->_vLatitude . "','" . $this->_vLongitude . "','" . $this->_vZip . "','" . $this->_vCity . "','" . $this->_vState . "','" . $this->_iPreferDeliveryTimeFrom . "','" . $this->_iPreferDeliveryTimeTo . "','" . $this->_eHowAboutUs . "','" . $this->_eNightDelivery . "','" . $this->_eStatus . "','" . $this->_iDtAdded . "','" . $this->_eActivePaidPlan . "','" . $this->_iPlanId . "' )";
        $sql = "INSERT INTO user SET
               vStoreUniqueId = '$this->_vStoreUniqueId',
               vAddress = '$this->_vAddress',
               vFirstName = '$this->_vFirstName',
               vZip = '$this->_vZip',
               vCity = '$this->_vCity',
               vState = '$this->_vState',
               iUserTypeId = '$this->_iUserTypeId',
               iDtAdded = '$this->_iDtAdded',
               iParentId = '$this->_iParentId',
               eStatus = '$this->_eStatus'";

        $result = $this->_obj->insert($sql);
        return $result;
    }

    /* API : guest_user
          For:listing data of guest user  */
    function select_guest($id = null)
    {
        global $dafault_image_logo;
        if ($id != null) {
            $WHERE = "WHERE iUserId='$id'";
        } else {
            $WHERE = "WHERE 1 = 1 ";
        }

        $sql = "SELECT * FROM user $WHERE";
        $row = $this->_obj->select($sql);


        $data['iUserId'] = $row[0]['iUserId'];
        $data['vAccountName'] = $row[0]['vStoreUniqueId'];
       $data['vFirstName'] = $row[0]['vFirstName'];
       /* $data['vContact'] = $row[0]['vContact'];
        $data['vComment'] = $row[0]['vComment'];*/

        $data['iUserTypeId'] = $row[0]['iUserTypeId'];


        $data['vAddress'] = $row[0]['vAddress'];

        $data['vCity'] = $row[0]['vCity'];
        $data['vState'] = $row[0]['vState'];

        $data['vZip'] = $row[0]['vZip'];

        $data['eStatus'] = $row[0]['eStatus'];
        $data['iDtAdded'] = $row[0]['iDtAdded'];


        return $data;
    }
   function upload_csv()
    {

        $this->iUserId = ""; // clear key for autoincrement

        // $sql = "INSERT INTO user ( iParentId,iUserTypeId,iTypeOfStoreId,iIndustriesId,vStoreName,vStoreUniqueId,NumberOfStore,vEmail,vPassword,vFirstName,vLastName,vAddress,vLatitude,vLongitude,vZip,vCity,vState,iPreferDeliveryTimeFrom,iPreferDeliveryTimeTo,eHowAboutUs,eNightDelivery,eStatus,iDtAdded,eActivePaidPlan,iPlanId) VALUES ( '" . $this->_iParentId . "','" . $this->_iUserTypeId . "','" . $this->_iTypeOfStoreId . "','" . $this->_iIndustriesId . "','" . $this->_vStoreName . "','" . $this->_vStoreUniqueId . "','" . $this->_NumberOfStore . "','" . $this->_vEmail . "','" . $this->_vPassword . "','" . $this->_vFirstName . "','" . $this->_vLastName . "','" . $this->_vAddress . "','" . $this->_vLatitude . "','" . $this->_vLongitude . "','" . $this->_vZip . "','" . $this->_vCity . "','" . $this->_vState . "','" . $this->_iPreferDeliveryTimeFrom . "','" . $this->_iPreferDeliveryTimeTo . "','" . $this->_eHowAboutUs . "','" . $this->_eNightDelivery . "','" . $this->_eStatus . "','" . $this->_iDtAdded . "','" . $this->_eActivePaidPlan . "','" . $this->_iPlanId . "' )";

        $sql = "INSERT INTO user SET
               vStoreUniqueId = '$this->_vStoreUniqueId',
               vFirstName = '$this->_vFirstName',
               vLastName = '$this->_vLastName',
               vStoreName = '$this->_vStoreName',
               vEmail = '$this->_vEmail',
               vAddress = '$this->_vAddress',
               vZip = '$this->_vZip',
               vCity = '$this->_vCity',
               vState = '$this->_vState',
               vContact = '$this->_vContact',
               iCountryId = '$this->_iCountryId',
               NumberOfStore= '$this->_NumberOfStore',
               iUserRoleId='$this->_iUserRoleId',
                iTypeOfStoreId='$this->_iTypeOfStoreId',
                iPlanId='$this->_iPlanId',
                iIndustriesId='$this->_iIndustriesId',
                iCategoryId='$this->_iCategoryId',
                vLatitude='$this->_vLatitude',
                vLongitude='$this->_vLongitude',
                iExpireTime='$this->_iExpireTime',
                eHowAboutUs='$this->_eHowAboutUs',
                eActivePaidPlan='$this->_eActivePaidPlan',
                ePlanType='$this->_ePlanType',
                fAmountPaid='$this->_fAmountPaid',
               vPassword= '$this->_vPassword',
               iDtAdded = '$this->_iDtAdded',
               iParentId = '$this->_iParentId',
               iUserTypeId = '$this->_iUserTypeId',
               eStatus = '$this->_eStatus',
        iDeliveryHoursFrom='$this->_iDeliveryHoursFrom',
        iDeliveryHoursTo='$this->_iDeliveryHoursTo',
        iPreferDeliveryTimeFrom='$this->_iPreferDeliveryTimeFrom',
        iPreferDeliveryTimeTo='$this->_iPreferDeliveryTimeTo',
        eNightDelivery='$this->_eNightDelivery'";

        $result = $this->_obj->insert($sql);
        return $result;
    }
      function update_plan($id)
    {
        $sql = " UPDATE user SET  iPlanId = '" . $this->_iPlanId . "' ,ePlanType = '" . $this->_ePlanType . "' ,  eActivePaidPlan = '" . $this->_eActivePaidPlan . "' ,  iExpireTime = '" . $this->_iExpireTime . "' , fAmountPaid = '" . $this->_fAmountPaid . "', vComment = '" . $this->_vComment . "' WHERE iUserId = $id or iParentId = $id ";
        $this->_obj->sql_query($sql);
    }
    

    function update_password($id, $pass)
    {
        $sql = " UPDATE user SET  vPassword = '" . $pass . "' WHERE iUserId = $id ";
        $this->_obj->sql_query($sql);
    }

function get_detail_from_storeuniqueid($account)
{
    $sql = "SELECT vEmail FROM user WHERE vStoreUniqueId='" . $account . "' AND eStatus='1'";
    return $this->_obj->select($sql);
}

    function select_employee($pid)
    {
         $sql = "SELECT iUserId,iParentId,vStoreUniqueId,vFirstName,vLastName  FROM user WHERE eStatus = '1' AND iParentId='$pid'OR  iUserId='$pid' ";

        $row = $this->_obj->select($sql);


        for($i=0;$i<count($row);$i++)
        {
            $data[$i]['iEmployeeId'] = $row[$i]['iUserId'];
            $data[$i]['iParentId'] = $row[$i]['iParentId'];
            $data[$i]['vAccountName'] = $row[$i]['vStoreUniqueId'];
             $data[$i]['vFirstName'] = $row[$i]['vFirstName']." ".$row[$i]['vLastName'];


        }
        return $data;
    }
function get_subuser($uid)
    {
        $sql = "SELECT *  FROM user WHERE eStatus = '1' AND iParentId='$uid' ANd eStatus!='2' order by iUserId desc";
        $row = $this->_obj->select($sql);
        return $row;
    }

 function select_employee1($pid)
    {
        $sql = "SELECT *  FROM user WHERE eStatus = '1' AND iParentId='$pid'OR  iUserId='$pid' ";

        $row = $this->_obj->select($sql);


        for ($i = 0; $i < count($row); $i++) {
            $data[$i]['iEmployeeId'] = $row[$i]['iUserId'];
            $data[$i]['iParentId'] = $row[$i]['iParentId'];
            $data[$i]['vAccountName'] = $row[$i]['vStoreUniqueId'];
            $data[$i]['vFirstName'] = $row[$i]['vFirstName']." ".$row[$i]['vLastName'];
            $data[$i]['vStoreName'] = $row[$i]['vStoreName'];
        }
        return $row;
    }
      function setmaxstore($id)
    {
        $sql = " UPDATE user SET vComment = '".$this->_vComment."',fAmountPaid = '".$this->_fAmountPaid."', iExpireTime = '".$this->_iExpireTime."' WHERE iUserId = $id ";
        $row = $this->_obj->select($sql);
        return $row;
    }
    
 /* For playment  subscription */
    function payment_subscription($start_prv_date,$end_prv_date)
    {
         // comment = totalstore
         $sql = "SELECT u.iUserId,u.vEmail,u.vComment,u.iPlanId, us.iCustomerId, pi.fPlanAmout, pi.ePlanType
                    FROM user u
                    LEFT JOIN user_settings us on u.iUserId=us.iUserId
                    JOIN user_plan_info pi ON u.iUserId =pi.iUserId
                    WHERE u.iParentId='0' AND u.eStatus='1' AND u.eActivePaidPlan='1' AND u.ePlanType = '1' AND u.iExpireTime between $start_prv_date AND $end_prv_date";
        $row = $this->_obj->select($sql);
        return $row;

    }
    function update_expiration_date($id)
    {
      $sql = " UPDATE user SET eActivePaidPlan = '2'  WHERE iUserId = $id ";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function update_user_info_subscription($id,$amount,$end_next_edate)
    {
        $sql = " UPDATE user SET fAmountPaid = '".$amount."'  and iExpireTime = '".$end_next_edate."' WHERE iUserId = $id ";
        $row = $this->_obj->select($sql);
        return $row;
    }
    
    
    function select_userdata($typeid)
    {
        if ($typeid == 1) {
            $lsql = ' iUserTypeId IN (4,5,6) ';
        } elseif ($typeid == 2) {
            $lsql = ' iUserTypeId IN (2) ';
        } elseif ($typeid == 3) {
            $lsql = ' iUserTypeId IN (3) ';
        }
        $sql = "SELECT iUserId,vStoreUniqueId FROM user WHERE" . $lsql . " AND iParentId = 0";
        $row = $this->_obj->select($sql);
        return $row;
    }
     function select_user_data($userid)
    {
        $sql = "SELECT iUserId,vFirstName FROM user WHERE iUserId = '$userid' OR iParentId = '$userid'";
        $row = $this->_obj->select($sql);
        return $row;
    }
function select_unsub()
{
    $sql = "select u.*,us.*,up.* from user u
left join user_settings us on u.iUserId=us.iUserId
left join user_plan_info up on u.iUserId=up.iUserId
where u.eStatus='0' AND u.iParentId='0'";
    $row = $this->_obj->select($sql);
    return $row;
}


    function get_admin_store_detail($id)
    {
        $sql = "select vComment,iStoreCount,iUserId from user where iUSerId=$id"  ;
        $row = $this->_obj->select($sql);
        return $row;
    }
}