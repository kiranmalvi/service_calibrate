<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    broadcast
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/broadcast.class.php
 * TABLE:        broadcast
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class broadcast
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iBroadcastId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iBroadcastId;
    protected $_iUserId;
    protected $_vMessage;
    protected $_eType;
    protected $_iDtAdded;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iBroadcastId = null;
        $this->_iUserId = null;
        $this->_vMessage = null;
        $this->_eType = null;
        $this->_iDtAdded = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiBroadcastId()
    {
        return $this->_iBroadcastId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiBroadcastId($val)
    {
        $this->_iBroadcastId = $val;
    }

    public function getiUserId()
    {
        return $this->_iUserId;
    }

    public function setiUserId($val)
    {
        $this->_iUserId = $val;
    }

    public function getvMessage()
    {
        return $this->_vMessage;
    }

    public function setvMessage($val)
    {
        $this->_vMessage = $val;
    }

    public function geteType()
    {
        return $this->_eType;
    }

    public function seteType($val)
    {
        $this->_eType = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM broadcast WHERE iBroadcastId = $id";
        $row = $this->_obj->select($sql);

        $this->_iBroadcastId = $row[0]['iBroadcastId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_vMessage = $row[0]['vMessage'];
        $this->_eType = $row[0]['eType'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_eStatus = $row[0]['eStatus'];
        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM broadcast WHERE iBroadcastId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    /* API : broadcast_new_store_opening
        For :  insert Broadcast */
    function insert()
    {
        $this->iBroadcastId = ""; // clear key for autoincrement

        $sql = "INSERT INTO broadcast ( iUserId,vMessage,eType,iDtAdded,eStatus ) VALUES ( '" . $this->_iUserId . "','" . $this->_vMessage . "','" . $this->_eType . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE broadcast SET  iUserId = '" . $this->_iUserId . "' , vMessage = '" . $this->_vMessage . "' , eType = '" . $this->_eType . "' , iDtAdded = '" . $this->_iDtAdded . "' , eStatus = '" . $this->_eStatus . "'  WHERE iBroadcastId = $id ";
        $this->_obj->sql_query($sql);

    }


}
