<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    user_type_store
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/user_type_store.class.php
 * TABLE:        user_type_store
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class user_type_store
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iUserTypeStoreId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iUserTypeStoreId;
    protected $_vName;
    protected $_iDtAdded;
    protected $_iDtUpdated;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iUserTypeStoreId = null;
        $this->_vName = null;
        $this->_iDtAdded = null;
        $this->_iDtUpdated = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiUserTypeStoreId()
    {
        return $this->_iUserTypeStoreId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiUserTypeStoreId($val)
    {
        $this->_iUserTypeStoreId = $val;
    }

    public function getvName()
    {
        return $this->_vName;
    }

    public function setvName($val)
    {
        $this->_vName = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }
    public function getiDtUpdated()
    {
        return $this->_iDtUpdated;
    }

    public function setiDtUpdated($val)
    {
        $this->_iDtUpdated = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $WHERE = "WHERE 1=1";
        if (!is_null($id)) {
            $WHERE .= " AND iUserTypeStoreId = $id";
        }

        $sql = "SELECT * FROM user_type_store $WHERE";
        $row = $this->_obj->select($sql);

        $this->_iUserTypeStoreId = $row[0]['iUserTypeStoreId'];
        $this->_vName = $row[0]['vName'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_iDtUpdated = $row[0]['iDtUpdated'];
        $this->_eStatus = $row[0]['eStatus'];

        return $row;
    }

    function select_API()
    {
        $sql = "SELECT iUserTypeStoreId,vName FROM user_type_store WHERE eStatus = '1' order by vName";
        $row = $this->_obj->select($sql);
        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM user_type_store WHERE iUserTypeStoreId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iUserTypeStoreId = ""; // clear key for autoincrement

        $sql = "INSERT INTO user_type_store ( vName,iDtAdded,eStatus,iDtUpdated ) VALUES ( '" . $this->_vName . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "','" . $this->_iDtUpdated . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {
        $sql = " UPDATE user_type_store SET ";
        $sql .= ($this->_vName != null) ? ", vName = '" . $this->_vName . "'," : "";
        $sql .= ($this->_iDtAdded != null) ? ", iDtAdded = '" . $this->_iDtAdded . "'," : "";
        $sql .= ($this->_iDtUpdated != null) ? ", iDtUpdated = '" . $this->_iDtUpdated . "'," : "";
        $sql .= ($this->_eStatus != null) ? ",eStatus = '" . $this->_eStatus . "' ," : "";
        $sql .= "WHERE iUserTypeStoreId = $id ";

        $UPDATE = clean_sql($sql);
        $this->_obj->sql_query($UPDATE);
    }

    function selectall()
    {
        $sql = "SELECT * FROM user_type_store";
        $row = $this->_obj->select($sql);
        return $row;

    }

    function select_FRONT($id = null)
    {
        $WHERE = "WHERE 1=1 AND eStatus = '1'";
        if (!is_null($id)) {
            $WHERE .= " AND iUserTypeStoreId = $id";
        }

        $sql = "SELECT vName,iUserTypeStoreId FROM user_type_store $WHERE ORDER BY vName ASC";
        $row = $this->_obj->select($sql);
        return $row;
    }




}
