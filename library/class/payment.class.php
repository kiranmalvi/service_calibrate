<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    payment
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/payment.class.php
 * TABLE:        payment
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */

class payment
{

protected $iPaymentId;   // KEY ATTR. WITH AUTOINCREMENT

protected $_iPaymentId;
protected $_iUserId;
protected $_iPlanId;
protected $_vTransactionId;
protected $_vAddress;
protected $_vCity;
protected $_VState;
protected $_vCountry;
protected $_vZip;
protected $_iDtAdded;
protected $_iDtExpired;
protected $_eStatus;
protected $_fAmount;
protected $_iStores;



/**
 *   @desc   CONSTRUCTOR METHOD
 */

function __construct()
{
    global $obj;
    $this->_obj = $obj;

    $this->_iPaymentId = null;
    $this->_iUserId = null;
    $this->_iPlanId = null;
    $this->_vTransactionId = null;
    $this->_vAddress = null;
    $this->_vCity = null;
    $this->_VState = null;
    $this->_vCountry = null;
    $this->_vZip = null;
    $this->_iDtAdded = null;
    $this->_iDtExpired = null;
    $this->_eStatus = null;
    $this->_fAmount = null;
    $this->_iStores = null;

}

/**
 *   @desc   DECONSTRUCTOR METHOD
 */

function __destruct()
{
    unset($this->_obj);
}



/**
 *   @desc   GETTER METHODS
 */


public function getiPaymentId()
{
    return $this->_iPaymentId;
}

public function getiUserId()
{
    return $this->_iUserId;
}

public function getiPlanId()
{
    return $this->_iPlanId;
}

public function getvTransactionId()
{
    return $this->_vTransactionId;
}

public function getvAddress()
{
    return $this->_vAddress;
}

public function getvCity()
{
    return $this->_vCity;
}

public function getVState()
{
    return $this->_VState;
}

public function getvCountry()
{
    return $this->_vCountry;
}

public function getvZip()
{
    return $this->_vZip;
}

public function getiDtAdded()
{
    return $this->_iDtAdded;
}

public function getiDtExpired()
{
    return $this->_iDtExpired;
}

public function geteStatus()
{
    return $this->_eStatus;
}
    public function getfAmount()
{
    return $this->_fAmount;
}
 public function getiStores()
{
    return $this->_iStores;
}


/**
 *   @desc   SETTER METHODS
 */


public function setiPaymentId($val)
{
    $this->_iPaymentId =  $val;
}

public function setiUserId($val)
{
    $this->_iUserId =  $val;
}

public function setiPlanId($val)
{
    $this->_iPlanId =  $val;
}

public function setvTransactionId($val)
{
    $this->_vTransactionId =  $val;
}

public function setvAddress($val)
{
    $this->_vAddress =  $val;
}

public function setvCity($val)
{
    $this->_vCity =  $val;
}

public function setVState($val)
{
    $this->_VState =  $val;
}

public function setvCountry($val)
{
    $this->_vCountry =  $val;
}

public function setvZip($val)
{
    $this->_vZip =  $val;
}

public function setiDtAdded($val)
{
    $this->_iDtAdded =  $val;
}

public function setiDtExpired($val)
{
    $this->_iDtExpired =  $val;
}

public function seteStatus($val)
{
    $this->_eStatus =  $val;
}
    public function setfAmount($val)
{
    $this->_fAmount =  $val;
}
    public function setiStores($val)
{
    $this->_iStores =  $val;
}


/**
 *   @desc   SELECT METHOD / LOAD
 */

function select($id)
{
    $sql =  "SELECT * FROM payment WHERE iPaymentId = $id";
    $row =  $this->_obj->select($sql);

    $this->_iPaymentId = $row[0]['iPaymentId'];
    $this->_iUserId = $row[0]['iUserId'];
    $this->_iPlanId = $row[0]['iPlanId'];
    $this->_vTransactionId = $row[0]['vTransactionId'];
    $this->_vAddress = $row[0]['vAddress'];
    $this->_vCity = $row[0]['vCity'];
    $this->_VState = $row[0]['VState'];
    $this->_vCountry = $row[0]['vCountry'];
    $this->_vZip = $row[0]['vZip'];
    $this->_iDtAdded = $row[0]['iDtAdded'];
    $this->_iDtExpired = $row[0]['iDtExpired'];
    $this->_fAmount = $row[0]['fAmount'];
    $this->_eStatus = $row[0]['eStatus'];
    $this->_iStores = $row[0]['iStores'];
    return $row;
}


/**
 *   @desc   DELETE
 */

function delete($id)
{
    $sql = "DELETE FROM payment WHERE iPaymentId = $id";
    $this->_obj->sql_query($sql);
}


/**
     * @desc   INSERT
     */
    function insert()
    {
        $this->iPaymentId = ""; // clear key for autoincrement

        $sql = "INSERT INTO payment ( iUserId,iPlanId,vTransactionId,vAddress,vCity,VState,vCountry,vZip,iDtAdded,iDtExpired,eStatus,fAmount,iStores ) VALUES ( '".$this->_iUserId."','".$this->_iPlanId."','".$this->_vTransactionId."','".$this->_vAddress."','".$this->_vCity."','".$this->_VState."','".$this->_vCountry."','".$this->_vZip."','".$this->_iDtAdded."','".$this->_iDtExpired."','".$this->_eStatus."','".$this->_fAmount."','".$this->_iStores."' )";
        $result=$this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE payment SET  iUserId = '".$this->_iUserId."' ,iStores = '".$this->_iStores."' ,fAmount = '".$this->_fAmount."' , iPlanId = '".$this->_iPlanId."' , vTransactionId = '".$this->_vTransactionId."' , vAddress = '".$this->_vAddress."' , vCity = '".$this->_vCity."' , VState = '".$this->_VState."' , vCountry = '".$this->_vCountry."' , vZip = '".$this->_vZip."' , iDtAdded = '".$this->_iDtAdded."' , iDtExpired = '".$this->_iDtExpired."' , eStatus = '".$this->_eStatus."'  WHERE iPaymentId = $id ";
        $this->_obj->sql_query($sql);

    }


    function select_payment_by_user($id)
    {
        $sql = "SELECT * FROM payment WHERE iUserId = $id ORDER BY iPaymentId desc";
        $row = $this->_obj->select($sql);

        $this->_iPaymentId = $row[0]['iPaymentId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_iPlanId = $row[0]['iPlanId'];
        $this->_vTransactionId = $row[0]['vTransactionId'];
        $this->_vAddress = $row[0]['vAddress'];
        $this->_vCity = $row[0]['vCity'];
        $this->_VState = $row[0]['VState'];
        $this->_vCountry = $row[0]['vCountry'];
        $this->_vZip = $row[0]['vZip'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_iDtExpired = $row[0]['iDtExpired'];
        $this->_fAmount = $row[0]['fAmount'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_iStores= $row[0]['iStores'];
        return $row;
    }
    
    function select_payment_five_user($id)
    {
        //$sql = "SELECT * FROM payment WHERE iUserId = $id ORDER BY iPaymentId desc limit 5";
        $sql = "select res.* from(
    SELECT 0 as type,p.iUserId,p.iPaymentId,p.fAmount,p.iDtAdded,p.vTransactionId  FROM payment p where iUserId=$id
UNION
SELECT 1 as type,pr.iUserId,pr.iPaymentId,pr.fAmount,pr.iDtAdded,pr.vTransactionId FROM payment_promotion pr where iUserId=$id
    ) as res ORDER BY iDtAdded desc limit 5";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function check_user($id)
    {
        $sql = "SELECT * FROM payment WHERE iUserId = $id ORDER BY iPaymentId desc";
        $row = $this->_obj->select($sql);
        return $row;
    }



}
