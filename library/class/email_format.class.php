<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    email_format
 * DATE:         06.02.2015
 * CLASS FILE:   /var/www/html/ublast/library/class/class-making/generated_classes/email_format.class.php
 * TABLE:        email_format
 * DB:           uBlast
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class email_format
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iEmailFormatId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iEmailFormatId;
    protected $_vEmailFormatTitle;
    protected $_vEmailFormatType;
    protected $_vEmailFormatSubject;
    protected $_tEmailFormatDesc;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iEmailFormatId = null;
        $this->_vEmailFormatTitle = null;
        $this->_vEmailFormatType = null;
        $this->_vEmailFormatSubject = null;
        $this->_tEmailFormatDesc = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiEmailFormatId()
    {
        return $this->_iEmailFormatId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiEmailFormatId($val)
    {
        $this->_iEmailFormatId = $val;
    }

    public function getvEmailFormatTitle()
    {
        return $this->_vEmailFormatTitle;
    }

    public function setvEmailFormatTitle($val)
    {
        $this->_vEmailFormatTitle = $val;
    }

    public function getvEmailFormatType()
    {
        return $this->_vEmailFormatType;
    }

    public function setvEmailFormatType($val)
    {
        $this->_vEmailFormatType = $val;
    }

    public function getvEmailFormatSubject()
    {
        return $this->_vEmailFormatSubject;
    }

    public function setvEmailFormatSubject($val)
    {
        $this->_vEmailFormatSubject = $val;
    }

    public function gettEmailFormatDesc()
    {
        return $this->_tEmailFormatDesc;
    }

    public function settEmailFormatDesc($val)
    {
        $this->_tEmailFormatDesc = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM email_format WHERE iEmailFormatId = $id";
        $row = $this->_obj->select($sql);

        $this->_iEmailFormatId = $row[0]['iEmailFormatId'];
        $this->_vEmailFormatTitle = $row[0]['vEmailFormatTitle'];
        $this->_vEmailFormatType = $row[0]['vEmailFormatType'];
        $this->_vEmailFormatSubject = $row[0]['vEmailFormatSubject'];
        $this->_tEmailFormatDesc = $row[0]['tEmailFormatDesc'];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM email_format WHERE iEmailFormatId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iEmailFormatId = ""; // clear key for autoincrement

        $sql = "INSERT INTO email_format ( vEmailFormatTitle,vEmailFormatType,vEmailFormatSubject,tEmailFormatDesc ) VALUES ( '" . $this->_vEmailFormatTitle . "','" . $this->_vEmailFormatType . "','" . $this->_vEmailFormatSubject . "','" . $this->_tEmailFormatDesc . "' )";
        $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE email_format SET  vEmailFormatTitle = '" . $this->_vEmailFormatTitle . "' , vEmailFormatType = '" . $this->_vEmailFormatType . "' , vEmailFormatSubject = '" . $this->_vEmailFormatSubject . "' , tEmailFormatDesc = '" . $this->_tEmailFormatDesc . "'  WHERE iEmailFormatId = $id ";
        $this->_obj->sql_query($sql);

    }


}
