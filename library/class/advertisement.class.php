<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    advertisement
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/advertisement.class.php
 * TABLE:        advertisement
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class advertisement
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iAdvertisementId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iAdvertisementId;
    protected $_vAddName;
    protected $_vImage;
    protected $_iStartDate;
    protected $_iEndDate;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iAdvertisementId = null;
        $this->_vAddName = null;
        $this->_vImage = null;
        $this->_iStartDate = null;
        $this->_iEndDate = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiAdvertisementId()
    {
        return $this->_iAdvertisementId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiAdvertisementId($val)
    {
        $this->_iAdvertisementId = $val;
    }

    public function getvAddName()
    {
        return $this->_vAddName;
    }

    public function setvAddName($val)
    {
        $this->_vAddName = $val;
    }

    public function getvImage()
    {
        return $this->_vImage;
    }

    public function setvImage($val)
    {
        $this->_vImage = $val;
    }

    public function getiStartDate()
    {
        return $this->_iStartDate;
    }

    public function setiStartDate($val)
    {
        $this->_iStartDate = $val;
    }

    public function getiEndDate()
    {
        return $this->_iEndDate;
    }

    public function setiEndDate($val)
    {
        $this->_iEndDate = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $WHERE = "WHERE 1=1 AND eStatus !='2'";
        if (!is_null($id)) {
            $WHERE .= " AND iAdvertisementId = $id";
        }
        $sql = "SELECT * FROM advertisement $WHERE";
        $row = $this->_obj->select($sql);

        $this->_iAdvertisementId = $row[0]['iAdvertisementId'];
        $this->_vAddName = $row[0]['vAddName'];
        $this->_vImage = $row[0]['vImage'];
        $this->_iStartDate = $row[0]['iStartDate'];
        $this->_iEndDate = $row[0]['iEndDate'];
        $this->_eStatus = $row[0]['eStatus'];
        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM advertisement WHERE iAdvertisementId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iAdvertisementId = ""; // clear key for autoincrement

        $sql = "INSERT INTO advertisement ( vAddName,vImage,iStartDate,iEndDate,eStatus ) VALUES ( '" . $this->_vAddName . "','" . $this->_vImage . "','" . $this->_iStartDate . "','" . $this->_iEndDate . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        echo $sql = " UPDATE advertisement SET  vAddName = '" . $this->_vAddName . "' , vImage = '" . $this->_vImage . "' , iStartDate = '" . $this->_iStartDate . "' , iEndDate = '" . $this->_iEndDate . "' , eStatus = '" . $this->_eStatus . "'  WHERE iAdvertisementId = $id ";
        $this->_obj->sql_query($sql);

    }


}
