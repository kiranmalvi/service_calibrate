<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    user_role
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/user_role.class.php
 * TABLE:        user_role
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class user_role
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iUserRoleId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iUserRoleId;
    protected $_vName;
    protected $_iUserTypeId;
    protected $_iDtAdded;
    protected $_iDtUpdated;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iUserRoleId = null;
        $this->_vName = null;
        $this->_iUserTypeId = null;
        $this->_iDtAdded = null;
        $this->_iDtUpdated = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiUserRoleId()
    {
        return $this->_iUserRoleId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiUserRoleId($val)
    {
        $this->_iUserRoleId = $val;
    }

    public function getvName()
    {
        return $this->_vName;
    }

    public function setvName($val)
    {
        $this->_vName = $val;
    }

    public function getiUserTypeId()
    {
        return $this->_iUserTypeId;
    }

    public function setiUserTypeId($val)
    {
        $this->_iUserTypeId = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }
 public function getiDtUpdated()
    {
        return $this->_iDtUpdated;
    }

    public function setiDtUpdated($val)
    {
        $this->_iDtUpdated = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $WHERE = "WHERE 1=1";
        if (!is_null($id))
        {
            $WHERE .= " AND iUserRoleId = $id";
        }

        $sql = "SELECT * FROM user_role $WHERE";
        $row = $this->_obj->select($sql);

        $this->_iUserRoleId = $row[0]['iUserRoleId'];
        $this->_vName = $row[0]['vName'];
        $this->_iUserTypeId = $row[0]['iUserTypeId'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_iDtUpdated = $row[0]['iDtUpdated'];
        $this->_eStatus = $row[0]['eStatus'];
        return $row;
    }

    function select_FRONT($id = null)
    {
        $WHERE = "WHERE 1=1 AND eStatus = '1'";
        if (!is_null($id)) {
            $WHERE .= " AND iUserRoleId = $id";
        }

        $sql = "SELECT * FROM user_role $WHERE ORDER BY vName ASC";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function select_with_type()
    {
        $sql = "SELECT ur.*,ut.vName as ROLE
                FROM user_role ur
                LEFT JOIN user_type ut ON ur.iUserTypeId = ut.iUserTypeId where ur.eStatus!='2'";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function select_with_type_API()
    {
        $sql = "SELECT ur.iUserRoleId,ur.vName,ut.vName as Role
                FROM user_role ur
                LEFT JOIN user_type ut ON ur.iUserTypeId = ut.iUserTypeId";
        $row = $this->_obj->select($sql);
        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM user_role WHERE iUserRoleId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iUserRoleId = ""; // clear key for autoincrement

        $sql = "INSERT INTO user_role ( vName,iUserTypeId,iDtAdded,iDtUpdated,eStatus ) VALUES ( '" . $this->_vName . "','" . $this->_iUserTypeId . "','" . $this->_iDtAdded . "','" . $this->_iDtUpdated . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE user_role SET ";
        $sql .= ($this->_vName != null) ? ", vName = '" . $this->_vName . "'," : "";
        $sql .= ($this->_iUserTypeId != null) ? ", iUserTypeId = '" . $this->_iUserTypeId . "'," : "";
        $sql .= ($this->_iDtAdded != null) ? ", iDtAdded = '" . $this->_iDtAdded . "'," : "";
        $sql .= ($this->_iDtUpdated != null) ? ", iDtUpdated = '" . $this->_iDtUpdated . "'," : "";
        $sql .= ($this->_eStatus != null) ? ",eStatus = '" . $this->_eStatus . "' ," : "";
        $sql .= "WHERE iUserRoleId = $id ";

        $UPDATE = clean_sql($sql);
        $this->_obj->sql_query($UPDATE);
    }

    function selectall($id)
    {
        $sql = "SELECT iUserRoleId,vName FROM user_role where iUserTypeId=$id";
        $row = $this->_obj->select($sql);
        return $row;
    }


}
