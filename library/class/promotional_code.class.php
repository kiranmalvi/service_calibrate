<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    promotional_code
* DATE:         01.07.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/promotional_code.class.php
* TABLE:        promotional_code
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class promotional_code
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iPromotionalcodeId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iPromotionalcodeId;  
	protected $_vPromotionalCode;  
	protected $_vPromotinalName;  
	protected $_vPromoPrice;  
	protected $_iDtAdded;  
	protected $_iDtUpdated;  
	protected $_eStatus;
    protected $_iExpireDate;



    /**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iPromotionalcodeId = null; 
		$this->_vPromotionalCode = null; 
		$this->_vPromotinalName = null; 
		$this->_vPromoPrice = null; 
		$this->_iDtAdded = null; 
		$this->_iDtUpdated = null; 
		$this->_eStatus = null;
        $this->_iExpireDate = null;
    }

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiPromotionalcodeId()
	{
		return $this->_iPromotionalcodeId;
	}

	public function getvPromotionalCode()
	{
		return $this->_vPromotionalCode;
	}

	public function getvPromotinalName()
	{
		return $this->_vPromotinalName;
	}

	public function getvPromoPrice()
	{
		return $this->_vPromoPrice;
	}

	public function getiDtAdded()
	{
		return $this->_iDtAdded;
	}

	public function getiDtUpdated()
	{
		return $this->_iDtUpdated;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}
    public function getiExpireDate()
    {
        return $this->_iExpireDate;
    }



    /**
*   @desc   SETTER METHODS
*/


	public function setiPromotionalcodeId($val)
	{
		 $this->_iPromotionalcodeId =  $val;
	}

	public function setvPromotionalCode($val)
	{
		 $this->_vPromotionalCode =  $val;
	}

	public function setvPromotinalName($val)
	{
		 $this->_vPromotinalName =  $val;
	}

	public function setvPromoPrice($val)
	{
		 $this->_vPromoPrice =  $val;
	}

	public function setiDtAdded($val)
	{
		 $this->_iDtAdded =  $val;
	}

	public function setiDtUpdated($val)
	{
		 $this->_iDtUpdated =  $val;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}
    public function setiExpireDate($val)
    {
        $this->_iExpireDate =  $val;
    }


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM promotional_code WHERE iPromotionalcodeId = $id  ";
		 $row =  $this->_obj->select($sql); 

		 $this->_iPromotionalcodeId = $row[0]['iPromotionalcodeId'];
		 $this->_vPromotionalCode = $row[0]['vPromotionalCode'];
		 $this->_vPromotinalName = $row[0]['vPromotinalName'];
		 $this->_vPromoPrice = $row[0]['vPromoPrice'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
		 $this->_iDtUpdated = $row[0]['iDtUpdated'];
		 $this->_eStatus = $row[0]['eStatus'];
        $this->_iExpireDate = $row[0]['iExpireDate'];
        return $row;

	}
    function check_promo($code)
	{
		 $date=strtotime(date('d-m-Y 00:00:00'));
		 $sql =  "SELECT * FROM promotional_code WHERE vPromotionalCode = '$code' AND eStatus!='2' AND iExpireDate > '$date'";
		 $row =  $this->_obj->select($sql);
         return $row;

	}

    function selectall()
    {
        $sql =  "SELECT * FROM promotional_code WHERE eStatus!='2' order by iPromotionalcodeId desc";
        $row =  $this->_obj->select($sql);

        $this->_iPromotionalcodeId = $row[0]['iPromotionalcodeId'];
        $this->_vPromotionalCode = $row[0]['vPromotionalCode'];
        $this->_vPromotinalName = $row[0]['vPromotinalName'];
        $this->_vPromoPrice = $row[0]['vPromoPrice'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_iDtUpdated = $row[0]['iDtUpdated'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_iExpireDate = $row[0]['iExpireDate'];
        return $row;
    }


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM promotional_code WHERE iPromotionalcodeId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

    function insert()
    {
        $this->iPromotionalcodeId = ""; // clear key for autoincrement

        $sql = "INSERT INTO promotional_code ( vPromotionalCode,vPromotinalName,vPromoPrice,iDtAdded,iDtUpdated,eStatus,iExpireDate ) VALUES ( '".$this->_vPromotionalCode."','".$this->_vPromotinalName."','".$this->_vPromoPrice."','".$this->_iDtAdded."','".$this->_iDtUpdated."','".$this->_eStatus."','".$this->_iExpireDate."' )";
        $result=$this->_obj->insert($sql);
        return $result;
    }



/**
*   @desc   UPDATE
*/

    function update($id)
    {

        $sql = " UPDATE promotional_code SET  vPromotionalCode = '".$this->_vPromotionalCode."' , vPromotinalName = '".$this->_vPromotinalName."' , vPromoPrice = '".$this->_vPromoPrice."' , iDtAdded = '".$this->_iDtAdded."' , iDtUpdated = '".$this->_iDtUpdated."' , eStatus = '".$this->_eStatus."' , iExpireDate = '".$this->_iExpireDate."'  WHERE iPromotionalcodeId = $id ";
        $this->_obj->sql_query($sql);

    }


}
