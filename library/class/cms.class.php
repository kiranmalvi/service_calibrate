<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    cms
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/cms.class.php
 * TABLE:        cms
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class cms
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iCmsId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iCmsId;
    protected $_vCmsName;
    protected $_vDescription;
    protected $_eStatus;
    protected $_iDtAdded;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iCmsId = null;
        $this->_vCmsName = null;
        $this->_vDescription = null;
        $this->_eStatus = null;
        $this->_iDtAdded = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiCmsId()
    {
        return $this->_iCmsId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiCmsId($val)
    {
        $this->_iCmsId = $val;
    }

    public function getvCmsName()
    {
        return $this->_vCmsName;
    }

    public function setvCmsName($val)
    {
        $this->_vCmsName = $val;
    }

    public function getvDescription()
    {
        return $this->_vDescription;
    }

    public function setvDescription($val)
    {
        $this->_vDescription = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM cms WHERE iCmsId = $id";
        $row = $this->_obj->select($sql);

        $this->_iCmsId = $row[0]['iCmsId'];
        $this->_vCmsName = $row[0]['vCmsName'];
        $this->_vDescription = $row[0]['vDescription'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM cms WHERE iCmsId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iCmsId = ""; // clear key for autoincrement

        $sql = "INSERT INTO cms ( vCmsName,vDescription,eStatus,iDtAdded ) VALUES ( '" . $this->_vCmsName . "','" . $this->_vDescription . "','" . $this->_eStatus . "','" . $this->_iDtAdded . "' )";
        $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE cms SET  vCmsName = '" . $this->_vCmsName . "' , vDescription = '" . $this->_vDescription . "' , eStatus = '" . $this->_eStatus . "' , iDtAdded = '" . $this->_iDtAdded . "'  WHERE iCmsId = $id ";
        $this->_obj->sql_query($sql);

    }


}
