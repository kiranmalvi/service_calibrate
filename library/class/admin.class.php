<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    admin
* DATE:         23.03.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/admin.class.php
* TABLE:        admin
* DB:           service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class admin
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iAdminId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iAdminId;
    protected $_vFirstName;
    protected $_vLastName;
    protected $_vEmail;
    protected $_vPasswd;
    protected $_dtAdded;
    protected $_dtUpdated;
    protected $_dtLastLogin;
    protected $_vLastLoginIP;
    protected $_eType;
    protected $_eStatus;



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

        $this->_iAdminId = null;
        $this->_vFirstName = null;
        $this->_vLastName = null;
        $this->_vEmail = null;
        $this->_vPasswd = null;
        $this->_dtAdded = null;
        $this->_dtUpdated = null;
        $this->_dtLastLogin = null;
        $this->_vLastLoginIP = null;
        $this->_eType = null;
        $this->_eStatus = null;
    }

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiAdminId()
	{
		return $this->_iAdminId;
	}

/**
*   @desc   SETTER METHODS
*/


	public function setiAdminId($val)
	{
		 $this->_iAdminId =  $val;
	}

	public function getvFirstName()
	{
		return $this->_vFirstName;
	}

	public function setvFirstName($val)
	{
		 $this->_vFirstName =  $val;
	}

	public function getvLastName()
	{
		return $this->_vLastName;
	}

	public function setvLastName($val)
	{
		 $this->_vLastName =  $val;
	}

	public function getvEmail()
	{
		return $this->_vEmail;
	}

	public function setvEmail($val)
	{
		 $this->_vEmail =  $val;
	}

	public function getvPasswd()
	{
		return $this->_vPasswd;
	}

	public function setvPasswd($val)
	{
		 $this->_vPasswd =  $val;
	}

	public function getdtAdded()
	{
		return $this->_dtAdded;
	}

	public function setdtAdded($val)
	{
		 $this->_dtAdded =  $val;
	}

	public function getdtUpdated()
	{
		return $this->_dtUpdated;
	}

	public function setdtUpdated($val)
	{
		 $this->_dtUpdated =  $val;
	}

	public function getdtLastLogin()
	{
		return $this->_dtLastLogin;
	}

	public function setdtLastLogin($val)
	{
		 $this->_dtLastLogin =  $val;
	}

	public function getvLastLoginIP()
	{
		return $this->_vLastLoginIP;
	}

	public function setvLastLoginIP($val)
	{
		 $this->_vLastLoginIP =  $val;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}

    public function geteType()
    {
        return $this->_eType;
    }

    public function seteType($val)
    {
        $this->_eType = $val;
    }


    /**
*   @desc   SELECT METHOD / LOAD
*/

    function select($id = null)
	{
        $where = 'Where 1=1';
        if (!is_null($id)) {
            $where .= " AND iAdminId = $id";
        }
        $sql = "SELECT * FROM admin $where";
		 $row =  $this->_obj->select($sql); 

		 $this->_iAdminId = $row[0]['iAdminId'];
		 $this->_vFirstName = $row[0]['vFirstName'];
		 $this->_vLastName = $row[0]['vLastName'];
		 $this->_vEmail = $row[0]['vEmail'];
		 $this->_vPasswd = $row[0]['vPasswd'];
		 $this->_dtAdded = $row[0]['dtAdded'];
		 $this->_dtUpdated = $row[0]['dtUpdated'];
		 $this->_dtLastLogin = $row[0]['dtLastLogin'];
		 $this->_vLastLoginIP = $row[0]['vLastLoginIP'];
		 $this->_eStatus = $row[0]['eStatus'];
        $this->_eType = $row[0]['eType'];

        return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM admin WHERE iAdminId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

    function insert()
    {
        $this->iAdminId = ""; // clear key for autoincrement

        $sql = "INSERT INTO admin ( vFirstName,vLastName,vEmail,vPasswd,dtAdded,dtUpdated,dtLastLogin,vLastLoginIP,eType,eStatus ) VALUES ( '" . $this->_vFirstName . "','" . $this->_vLastName . "','" . $this->_vEmail . "','" . $this->_vPasswd . "','" . $this->_dtAdded . "','" . $this->_dtUpdated . "','" . $this->_dtLastLogin . "','" . $this->_vLastLoginIP . "','" . $this->_eType . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


/**
*   @desc   UPDATE
*/

	function update($id)
	{
        $sql = " UPDATE admin SET ";
        $sql .= ($this->_vFirstName != null) ? ", vFirstName = '" . $this->_vFirstName . "'," : "";
        $sql .= ($this->_vLastName != null) ? ", vLastName = '" . $this->_vLastName . "'," : "";
        $sql .= ($this->_vEmail != null) ? ", vEmail = '" . $this->_vEmail . "'," : "";
        $sql .= ($this->_vPasswd != null) ? ", vPasswd = '" . $this->_vPasswd . "'," : "";
        $sql .= ($this->_dtAdded != null) ? ", dtAdded = '" . $this->_dtAdded . "'," : "";
        $sql .= ($this->_dtUpdated != null) ? ", dtUpdated = '" . $this->_dtUpdated . "'," : "";
        $sql .= ($this->_dtLastLogin != null) ? ", dtLastLogin = '" . $this->_dtLastLogin . "'," : "";
        $sql .= ($this->_vLastLoginIP != null) ? ", vLastLoginIP = '" . $this->_vLastLoginIP . "'," : "";
        $sql .= ($this->_eStatus != null) ? ", eStatus = '" . $this->_eStatus . "'," : "";
        $sql .= ($this->_eType != null) ? ", eType = '" . $this->_eType . "'," : "";
        $sql .= " WHERE iAdminId = $id ";

        $UPDATE = clean_sql($sql);

        $this->_obj->sql_query($UPDATE);
	}
	
	function chk_pass($id,$old)
    {
        $sql = "SELECT * FROM admin where iAdminId=$id";
        $row =  $this->_obj->select($sql);
        if($row[0]['vPasswd']==base64_encode($old))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


}
