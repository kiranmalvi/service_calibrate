<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    gift_card
* DATE:         22.05.2015
* CLASS FILE:   /var/www/service_calibrate/library/class/class-making/generated_classes/gift_card.class.php
* TABLE:        gift_card
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class gift_card
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iCardId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iCardId;  
	protected $_vGift_CardName;  
	protected $_vPoints;  
	protected $_vDescription;  
	protected $_idtAdded;  
	protected $_idtupdate;  
	protected $_eStatus;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iCardId = null; 
		$this->_vGift_CardName = null; 
		$this->_vPoints = null; 
		$this->_vDescription = null; 
		$this->_idtAdded = null; 
		$this->_idtupdate = null; 
		$this->_eStatus = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiCardId()
	{
		return $this->_iCardId;
	}

	public function getvGift_CardName()
	{
		return $this->_vGift_CardName;
	}

	public function getvPoints()
	{
		return $this->_vPoints;
	}

	public function getvDescription()
	{
		return $this->_vDescription;
	}

	public function getidtAdded()
	{
		return $this->_idtAdded;
	}

	public function getidtupdate()
	{
		return $this->_idtupdate;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiCardId($val)
	{
		 $this->_iCardId =  $val;
	}

	public function setvGift_CardName($val)
	{
		 $this->_vGift_CardName =  $val;
	}

	public function setvPoints($val)
	{
		 $this->_vPoints =  $val;
	}

	public function setvDescription($val)
	{
		 $this->_vDescription =  $val;
	}

	public function setidtAdded($val)
	{
		 $this->_idtAdded =  $val;
	}

	public function setidtupdate($val)
	{
		 $this->_idtupdate =  $val;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM gift_card WHERE iCardId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iCardId = $row[0]['iCardId'];
		 $this->_vGift_CardName = $row[0]['vGift_CardName'];
		 $this->_vPoints = $row[0]['vPoints'];
		 $this->_vDescription = $row[0]['vDescription'];
		 $this->_idtAdded = $row[0]['idtAdded'];
		 $this->_idtupdate = $row[0]['idtupdate'];
		 $this->_eStatus = $row[0]['eStatus'];
        return $row;
	}
    function giftcardlist()
    {
         $sql =  "SELECT * FROM gift_card WHERE eStatus != '2'";
        $row =  $this->_obj->select($sql);

        $this->_iCardId = $row[0]['iCardId'];
        $this->_vGift_CardName = $row[0]['vGift_CardName'];
        $this->_vPoints = $row[0]['vPoints'];
        $this->_vDescription = $row[0]['vDescription'];
        $this->_idtAdded = $row[0]['idtAdded'];
        $this->_idtupdate = $row[0]['idtupdate'];
        $this->_eStatus = $row[0]['eStatus'];
        return $row;
    }

/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM gift_card WHERE iCardId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iCardId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO gift_card ( vGift_CardName,vPoints,vDescription,idtAdded,idtupdate,eStatus ) VALUES ( '".$this->_vGift_CardName."','".$this->_vPoints."','".$this->_vDescription."','".$this->_idtAdded."','".$this->_idtupdate."','".$this->_eStatus."' )";
        $result= $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE gift_card SET  vGift_CardName = '".$this->_vGift_CardName."' , vPoints = '".$this->_vPoints."' , vDescription = '".$this->_vDescription."' , idtAdded = '".$this->_idtAdded."' , idtupdate = '".$this->_idtupdate."' , eStatus = '".$this->_eStatus."'  WHERE iCardId = $id ";
		 $this->_obj->sql_query($sql);

	}


}
