<?php

/*
*
* -------------------------------------------------------
* CLASSNAME:        Dateormat
* GENERATION DATE:  21.08.2006
* CLASS FILE:       /home/www1/calms/lib/classes/general/Date.Class.php5
* FOR MYSQL DB:     calms
* -------------------------------------------------------
* AUTHOR:
* Ankit (WC)
* from: >> www.webcorsa.com 
* -------------------------------------------------------
*
*/

require_once(dirname(__FILE__) . '/DateFormat.class.php');

if (!defined('AMERICAN_DATE_FORMAT')) {
    define('AMERICAN_DATE_FORMAT', 'mm-dd-yyyy');
}

if (!defined('AMERICAN_DATE_FORMAT_GMT')) {
    define('AMERICAN_DATE_FORMAT_GMT', 'm/d/Y');
}

if (!defined('EUROPEAN_DATE_FORMAT')) {
    define('EUROPEAN_DATE_FORMAT', 'dd-mm-yyyy');
}

if (!defined('EUROPEAN_DATE_FORMAT_GMT')) {
    define('EUROPEAN_DATE_FORMAT_GMT', 'Y/m/d');
}

if (!defined('YEAR_FIRST_DATE_FORMAT')) {
    define('YEAR_FIRST_DATE_FORMAT', 'yyyy-dd-mm');
}

if (!defined('YEAR_FIRST_DATE_FORMAT_GMT')) {
    define('YEAR_FIRST_DATE_FORMAT_GMT', 'Y/d/m');
}

if (!defined('THIS_DB_SAFE_FORMAT')) {
    define('THIS_DB_SAFE_FORMAT', 'Y/m/d');
}

if (!defined('ONE_ARG_CONSTRUCTOR')) {
    define('ONE_ARG_CONSTRUCTOR', 1);
}

if (!defined('TWO_ARG_CONSTRUCTOR')) {
    define('TWO_ARG_CONSTRUCTOR', 2);
}

if (!defined('THREE_ARG_CONSTRUCTOR')) {
    define('THREE_ARG_CONSTRUCTOR', 3);
}

if (!defined('SIX_ARG_CONSTRUCTOR')) {
    define('SIX_ARG_CONSTRUCTOR', 6);
}

if (!defined('MAX_32_BIT_SIGNED_YEAR')) {
    define('MAX_32_BIT_SIGNED_YEAR', 2037);
}

if (!defined('MIN_32_BIT_SIGNED_YEAR')) {
    define('MIN_32_BIT_SIGNED_YEAR', (strstr(strtoupper(PHP_OS), 'WIN')) ? 1971 : 1902);
}


/**
 * This class represents a point in time.
 */
class Date
{
    var $format;
    var $time = null;

    /**
     * <pre>
     * Constructor. There are four types of constructores:
     *
     *  <ul>
     *      <li>time -- pass a long representing the time since epoch (deprecated)
     *      <li>time with format -- pass a long representing the time since epoch with a string format. (deprecated)
     *      <li>pass year, month, day integers.
     *      <li>pass year, month, day, hour, minute, second integers.
     *  </ul>
     *
     *
     * @public
     */
    function Date()
    {
        $numArgs = func_num_args();

        switch ($numArgs) {
            case ONE_ARG_CONSTRUCTOR:
                $gmtTime = func_get_arg(0);
                $this->setTime($gmtTime);
                $this->format = $this->formatFactoryMethod(AMERICAN_DATE_FORMAT_GMT);
                break;
            case TWO_ARG_CONSTRUCTOR:
                $gmtTime = func_get_arg(0);
                $format = func_get_arg(1);

                $this->setTime($gmtTime);
                $this->format = $this->formatFactoryMethod($format);
                break;
            case THREE_ARG_CONSTRUCTOR:
                $year = $this->_maxYear(func_get_arg(0));
                $month = func_get_arg(1);
                $day = func_get_arg(2);

                if (!checkdate($month, $day, $year)) {
                    trigger_error('Incorrect date entered.', E_USER_ERROR);
                }

                $this->time = mktime(0, 0, 0, $month, $day, $year);

                break;
            case SIX_ARG_CONSTRUCTOR:
                $year = $this->_maxYear(func_get_arg(0));
                $month = func_get_arg(1);
                $day = func_get_arg(2);
                $hour = func_get_arg(3);
                $min = func_get_arg(4);
                $sec = func_get_arg(5);

                if (!checkdate($month, $day, $year)) {
                    trigger_error('Incorrect date entered.', E_USER_ERROR);
                }

                $this->time = mktime($hour, $min, $sec, $month, $day, $year);
                break;
            default:
                $this->setTime();
        }
    }


    // This is a hack to work with Windows and others.

    function formatFactoryMethod($format)
    {
        if (EUROPEAN_DATE_FORMAT == $format) {
            $format = AMERICAN_DATE_FORMAT_GMT;
        } elseif (YEAR_FIRST_DATE_FORMAT == $format) {
            $format = YEAR_FIRST_DATE_FORMAT_GMT;
        } else {
            $format = AMERICAN_DATE_FORMAT_GMT;
        }


        return $format;
    }


    // deprecated

    function _maxYear($year)
    {
        // check if number in 32-bit signed range
        if ((abs(1) <= 0x7FFFFFFF)) {
            if (2 == strlen($year)) {
                $year += 2000;
            }

            if ($year < MIN_32_BIT_SIGNED_YEAR) {
                $year = MIN_32_BIT_SIGNED_YEAR;
            }

            if (MAX_32_BIT_SIGNED_YEAR < $year) {
                $year = MAX_32_BIT_SIGNED_YEAR;
            }
        }

        return $year;
    }

    function getFormattedTime()
    {
        return $this->toString();
    }


    // deprecated

    function toString($format = null)
    {
        // I don't think this is best
        return (DB_SAFE_FORMAT == $format) ? gmdate(THIS_DB_SAFE_FORMAT, $this->time) :
            gmdate($this->format, $this->time);
    }


    // deprecated

    function setFormat($format)
    {
        $this->format = $format;
    }


    // deprecated use DateFormat
    // protected

    function getYearShort()
    {
        return $this->getYear(true);
    }

    /**
     * Strictly speaking, this shouldn't be here but should be handled in a DateFormat
     * object instead. Here for convenience.
     */
    function getYear($short = false)
    {
        $format = ($short) ? '%y' : '%Y';
        return strftime($format, $this->time);
    }

    /**
     * Determines whether this date is before the given date. if $byDate is true, compares
     * by month, year and day. Otherwise compares by strict time.
     *
     * $when -- The date to compare this date with.
     * $byDate -- Compare by month, year and day if true or by strict time if not. Defaults to true.
     * @public
     */
    function before($when, $byDate = true)
    {
        if ($byDate) {
            return ($this->compareDates($when) < 0);
        } else {
            return !$this->after($when);
        }
    }

    /**
     * Protected method which compares the given date to this date by month, year and day.
     *
     * $when -- The date to compare this date with.
     * @public
     */
    function compareDates($when)
    {
        $thatArr = getdate($when->getTime());
        $thatString = $thatArr['year'] . str_pad($thatArr['mon'], 2, '0', STR_PAD_LEFT) . str_pad($thatArr['mday'], 2, '0', STR_PAD_LEFT);

        $thissArr = getdate($this->getTime());

        $thisString = $thissArr['year'] . str_pad($thissArr['mon'], 2, '0', STR_PAD_LEFT) . str_pad($thissArr['mday'], 2, '0', STR_PAD_LEFT);

        return strcmp($thisString, $thatString);
    }

    /**
     * Get the time since epoch of this object.
     *
     * @public
     */
    function getTime()
    {
        return $this->time;
    }

    function setTime($gmtTime = null)
    {
        $this->time = (null == $gmtTime || '' == $gmtTime) ? time() : $gmtTime;
    }

    /**
     * Determines whether this date is after the given date. if $byDate is true, compares
     * by month, year and day. Otherwise compares by strict time.
     *
     * $when -- The date to compare this date with.
     * $byDate -- Compare by month, year and day if true or by strict time if not. Defaults to true.
     * @public
     */
    function after($when, $byDate = true)
    {
        if ($byDate) {
            return (0 < $this->compareDates($when));
        } else {
            return ($when->getTime() < $this->getTime());
        }
    }

    /**
     * Determines whether this date is equal to the given date. if $byDate is true, compares
     * by month, year and day. Otherwise compares by strict time.
     *
     * $when -- The date to compare this date with.
     * $byDate -- Compare by month, year and day if true or by strict time if not. Defaults to true.
     * @public
     */
    function equals($when, $byDate = true)
    {

        if ($byDate) {
            return (0 === $this->compareDates($when));
        } else {
            return $this->getTime() == $when->getTime();
        }
    }

    function &getInternationalDate()
    {
        $today = ($this->time) ? $this : new Date();

        return ($today->getYear() . $today->getMonth() . $today->getDay());
    }

    function getMonth()
    {
        return strftime('%m', $this->time);
    }

    function getDay()
    {
        return strftime('%d', $this->time);
    }

    ###################################################
    # To Add day, month, year and hour to passed date
    ###################################################

    function datefunctions_report()
    {
        $date_arr = $this->datefunctions();
        foreach ($date_arr as $key => $val) {
            $date = explode("-", $val);
            $date_arr1[$key] = $date[2] . "-" . $date[0] . "-" . $date[1];
        }// en
        return $date_arr1;
    }

    function datefunctions()
    {
        //Today.....................
        $today = date("m-d-Y");

        //Yesterday.....................
        $yes = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -1, $ma = 0, $ya = 0, $ha = 0)));

        //Current Week..................
        $currweekFDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -($this->findfirstday($this->rpt_dateconvert($today, "D"))), $ma = 0, $ya = 0, $ha = 0)));
        $currweekTDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = 6 - ($this->findfirstday($this->rpt_dateconvert($today, "D"))), $ma = 0, $ya = 0, $ha = 0)));

        //Previous Week..................
        $prevweekFDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -(7 + ($this->findfirstday($this->rpt_dateconvert($today, "D")))), $ma = 0, $ya = 0, $ha = 0)));
        $prevweekTDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = 6 - (7 + ($this->findfirstday($this->rpt_dateconvert($today, "D")))), $ma = 0, $ya = 0, $ha = 0)));

        //Current Month................
        $currmonthFDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -($this->rpt_dateconvert($today, "d") - 1), $ma = 0, $ya = 0, $ha = 0)));
        $currmonthTDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -($this->rpt_dateconvert($today, "d")), $ma = 1, $ya = 0, $ha = 0)));

        //Previous Month................
        $prevmonthFDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -($this->rpt_dateconvert($today, "d") - 1), $ma = -1, $ya = 0, $ha = 0)));
        $prevmonthTDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -($this->rpt_dateconvert($today, "d")), $ma = 0, $ya = 0, $ha = 0)));

        //Current Quarter................
        if ($this->rpt_dateconvert($today, "m") > 3 && $this->rpt_dateconvert($today, "m") < 7) $num = 4;
        elseif ($this->rpt_dateconvert($today, "m") > 6 && $this->rpt_dateconvert($today, "m") < 10) $num = 7;
        elseif ($this->rpt_dateconvert($today, "m") > 9) $num = 10;
        elseif ($this->rpt_dateconvert($today, "m") < 4) $num = 1;
        $currQuarterFDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -($this->rpt_dateconvert($today, "d") - 1), $ma = -($this->rpt_dateconvert($today, "m") - $num), $ya = 0, $ha = 0)));
        $currQuarterTDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($currQuarterFDate, "Y-m-d"), $da = -1, $ma = 3, $ya = 0, $ha = 0)));

        //Previous Quarter................
        $prevQuarterFDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($currQuarterFDate, "Y-m-d"), $da = 0, $ma = -3, $ya = 0, $ha = 0)));
        $prevQuarterTDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($currQuarterTDate, "Y-m-d"), $da = -($this->rpt_dateconvert($currQuarterTDate, "d")), $ma = -2, $ya = 0, $ha = 0)));

        //Current Year................
        $currYearFDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -($this->rpt_dateconvert($today, "d") - 1), $ma = -($this->rpt_dateconvert($today, "m")) + 1, $ya = 0, $ha = 0)));
        $currYearTDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -($this->rpt_dateconvert($today, "d")), $ma = 12 - ($this->rpt_dateconvert($today, "m")) + 1, $ya = 0, $ha = 0)));

        //Previous Year................
        $prevYearFDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -($this->rpt_dateconvert($today, "d") - 1), $ma = -($this->rpt_dateconvert($today, "m")) + 1, $ya = -1, $ha = 0)));
        $prevYearTDate = date("m-d-Y", strtotime($this->addDate($this->rpt_dateconvert($today, "Y-m-d"), $da = -($this->rpt_dateconvert($today, "d")), $ma = 12 - ($this->rpt_dateconvert($today, "m")) + 1, $ya = -1, $ha = 0)));


        $datearray[today] = $today;
        $datearray[yesterday] = $yes;
        $datearray[currweekFDate] = $currweekFDate;
        $datearray[currweekTDate] = $currweekTDate;
        $datearray[prevweekFDate] = $prevweekFDate;
        $datearray[prevweekTDate] = $prevweekTDate;
        $datearray[currmonthFDate] = $currmonthFDate;
        $datearray[currmonthTDate] = $currmonthTDate;
        $datearray[prevmonthFDate] = $prevmonthFDate;
        $datearray[prevmonthTDate] = $prevmonthTDate;
        $datearray[currQuarterFDate] = $currQuarterFDate;
        $datearray[currQuarterTDate] = $currQuarterTDate;
        $datearray[prevQuarterFDate] = $prevQuarterFDate;
        $datearray[prevQuarterTDate] = $prevQuarterTDate;
        $datearray[currYearFDate] = $currYearFDate;
        $datearray[currYearTDate] = $currYearTDate;
        $datearray[prevYearFDate] = $prevYearFDate;
        $datearray[prevYearTDate] = $prevYearTDate;
        return $datearray;
    }

    function addDate($text, $da = 0, $ma = 0, $ya = 0, $ha = 0)
    {
        $h = date('H', strtotime($text));
        $d = date('d', strtotime($text));
        $m = date('m', strtotime($text));
        $y = date('Y', strtotime($text));
        $fromTime = date("Y-m-d", mktime($h + $ha, 0, 0, $m + $ma, $d + $da, $y + $ya));
        //	echo "<br>".$fromTime;
        return $fromTime;
    }

    function rpt_dateconvert($ddate, $format)
    {
        # From mm-dd-yyyy H:i:s
        //rpt_dateconvert($today,"Y-m-d")
        $temp = explode(" ", $ddate);
        $date1 = $temp[0];
        $time1 = $temp[1];

        $temp = explode("-", $date1);
        $month = $temp[0];
        $day = $temp[1];
        $year = $temp[2];

        $temp = explode(":", $time1);
        $hour = ($temp[0] == "") ? 0 : $hour;
        $min = ($temp[1] == "") ? 0 : $min;
        $sec = ($temp[2] == "") ? 0 : $sec;
        //echo "$hour, $min, $sec, $month, $day, $year";exit;
        return date($format, mktime($hour, $min, $sec, $month, $day, $year));
    }

    function findfirstday($cw)
    {
        $daynum = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        for ($i = 0; $i < 7; $i++) {
            if ($daynum[$i] == $cw)
                return $i;
        }
    }

    /**
     *
     * @desc        $diffd = getDateDifference($dateFrom, $dateTo, 'd');
     *            $diffh = getDateDifference($dateFrom, $dateTo, 'h');
     *            $diffm = getDateDifference($dateFrom, $dateTo, 'm');
     *            $diffs = getDateDifference($dateFrom, $dateTo, 's');
     *            $diffa = getDateDifference($dateFrom, $dateTo, 'a');
     */

    function getDateDifference($dateFrom, $dateTo, $unit = 'd')
    {
        $difference = null;
        $dateFromElements = @split(' ', $dateFrom);
        $dateToElements = @split(' ', $dateTo);
        $dateFromDateElements = @split('-', $dateFromElements[0]);
        $dateFromTimeElements = @split(':', $dateFromElements[1]);
        $dateToDateElements = @split('-', $dateToElements[0]);
        $dateToTimeElements = @split(':', $dateToElements[1]);

        // Get unix timestamp for both dates
        $dateFromTimeElements[0] = ($dateFromTimeElements[0] == '') ? 0 : $dateFromTimeElements[0];
        $dateFromTimeElements[1] = ($dateFromTimeElements[1] == '') ? 0 : $dateFromTimeElements[1];
        $dateFromTimeElements[2] = ($dateFromTimeElements[2] == '') ? 0 : $dateFromTimeElements[2];

        $date1 = mktime($dateFromTimeElements[0], $dateFromTimeElements[1], $dateFromTimeElements[2], $dateFromDateElements[1], $dateFromDateElements[2], $dateFromDateElements[0]);

        $dateToTimeElements[0] = ($dateToTimeElements[0] == '') ? 0 : $dateToTimeElements[0];
        $dateToTimeElements[1] = ($dateToTimeElements[1] == '') ? 0 : $dateToTimeElements[1];
        $dateToTimeElements[2] = ($dateToTimeElements[2] == '') ? 0 : $dateToTimeElements[2];

        $date2 = mktime($dateToTimeElements[0], $dateToTimeElements[1], $dateToTimeElements[2], $dateToDateElements[1], $dateToDateElements[2], $dateToDateElements[0]);
        if ($date1 > $date2) {
            return null;
        }

        $diff = $date2 - $date1;
        $days = 0;
        $hours = 0;
        $minutes = 0;
        $seconds = 0;
        if ($diff % 86400 <= 0)  // there are 86,400 seconds in a day
        {
            $days = $diff / 86400;
        }
        if ($diff % 86400 > 0) {
            $rest = ($diff % 86400);
            $days = ($diff - $rest) / 86400;
            if ($rest % 3600 > 0) {
                $rest1 = ($rest % 3600);
                $hours = ($rest - $rest1) / 3600;
                if ($rest1 % 60 > 0) {
                    $rest2 = ($rest1 % 60);
                    $minutes = ($rest1 - $rest2) / 60;
                    $seconds = $rest2;
                } else {
                    $minutes = $rest1 / 60;
                }
            } else {
                $hours = $rest / 3600;
            }
        }
        switch ($unit) {
            case 'd':
            case 'D':
                $partialDays = 0;
                $partialDays += ($seconds / 86400);
                $partialDays += ($minutes / 1440);
                $partialDays += ($hours / 24);
                $difference = $days + $partialDays;
                break;
            case 'h':
            case 'H':
                $partialHours = 0;
                $partialHours += ($seconds / 3600);
                $partialHours += ($minutes / 60);
                $difference = $hours + ($days * 24) + $partialHours;
                break;
            case 'm':
            case 'M':
                $partialMinutes = 0;
                $partialMinutes += ($seconds / 60);
                $difference = $minutes + ($days * 1440) + ($hours * 60) + $partialMinutes;
                break;
            case 's':
            case 'S':
                $difference = $seconds + ($days * 86400) + ($hours * 3600) + ($minutes * 60);
                break;
            case 'a':
            case 'A':
                $difference = array(
                    "days" => $days,
                    "hours" => $hours,
                    "minutes" => $minutes,
                    "seconds" => $seconds
                );
                break;
        }
        return $difference;
    }
}

?>