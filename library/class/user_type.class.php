<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    user_type
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/user_type.class.php
 * TABLE:        user_type
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class user_type
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iUserTypeId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iUserTypeId;
    protected $_vName;
    protected $_iParentId;
    protected $_iDateAdded;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iUserTypeId = null;
        $this->_vName = null;
        $this->_iParentId = null;
        $this->_iDateAdded = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiUserTypeId()
    {
        return $this->_iUserTypeId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiUserTypeId($val)
    {
        $this->_iUserTypeId = $val;
    }

    public function getvName()
    {
        return $this->_vName;
    }

    public function setvName($val)
    {
        $this->_vName = $val;
    }

    public function getiParentId()
    {
        return $this->_iParentId;
    }

    public function setiParentId($val)
    {
        $this->_iParentId = $val;
    }

    public function getiDateAdded()
    {
        return $this->_iDateAdded;
    }

    public function setiDateAdded($val)
    {
        $this->_iDateAdded = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM user_type WHERE iUserTypeId = $id";
        $row = $this->_obj->select($sql);

        $this->_iUserTypeId = $row[0]['iUserTypeId'];
        $this->_vName = $row[0]['vName'];
        $this->_iParentId = $row[0]['iParentId'];
        $this->_iDateAdded = $row[0]['iDateAdded'];
        $this->_eStatus = $row[0]['eStatus'];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM user_type WHERE iUserTypeId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iUserTypeId = ""; // clear key for autoincrement

        $sql = "INSERT INTO user_type ( vName,iParentId,iDateAdded,eStatus ) VALUES ( '" . $this->_vName . "','" . $this->_iParentId . "','" . $this->_iDateAdded . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE user_type SET  vName = '" . $this->_vName . "' , iParentId = '" . $this->_iParentId . "' , iDateAdded = '" . $this->_iDateAdded . "' , eStatus = '" . $this->_eStatus . "'  WHERE iUserTypeId = $id ";
        $this->_obj->sql_query($sql);

    }

    function getparent($type)
    {
        $sql = "SELECT iParentId,iUserTypeId,vName FROM user_type WHERE iUserTypeId = $type or iParentId=$type";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function get_name($type)
    {
        $sql = "SELECT iUserTypeId,vName FROM user_type WHERE iUserTypeId = $type";
        $row = $this->_obj->select($sql);
        return $row;
    }

}
