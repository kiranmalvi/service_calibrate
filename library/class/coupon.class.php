<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    coupon
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/coupon.class.php
 * TABLE:        coupon
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class coupon
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iCouponId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iCouponId;
    protected $_vCouponCode;
    protected $_tCouponDescription;
    protected $_iExpireDate;
    protected $_idtAdded;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iCouponId = null;
        $this->_vCouponCode = null;
        $this->_tCouponDescription = null;
        $this->_iExpireDate = null;
        $this->_idtAdded = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiCouponId()
    {
        return $this->_iCouponId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiCouponId($val)
    {
        $this->_iCouponId = $val;
    }

    public function getvCouponCode()
    {
        return $this->_vCouponCode;
    }

    public function setvCouponCode($val)
    {
        $this->_vCouponCode = $val;
    }

    public function gettCouponDescription()
    {
        return $this->_tCouponDescription;
    }

    public function settCouponDescription($val)
    {
        $this->_tCouponDescription = $val;
    }

    public function getiExpireDate()
    {
        return $this->_iExpireDate;
    }

    public function setiExpireDate($val)
    {
        $this->_iExpireDate = $val;
    }

    public function getidtAdded()
    {
        return $this->_idtAdded;
    }

    public function setidtAdded($val)
    {
        $this->_idtAdded = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $WHERE = 'WHERE 1=1';
        if (!is_null($id)) {
            $WHERE .= " AND iCouponId = $id";
        }

        $sql = "SELECT * FROM coupon $WHERE";
        $row = $this->_obj->select($sql);

        $this->_iCouponId = $row[0]['iCouponId'];
        $this->_vCouponCode = $row[0]['vCouponCode'];
        $this->_tCouponDescription = $row[0]['tCouponDescription'];
        $this->_iExpireDate = $row[0]['iExpireDate'];
        $this->_idtAdded = $row[0]['idtAdded'];
        $this->_eStatus = $row[0]['eStatus'];

        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM coupon WHERE iCouponId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iCouponId = ""; // clear key for autoincrement

        $sql = "INSERT INTO coupon ( vCouponCode,tCouponDescription,iExpireDate,idtAdded,eStatus ) VALUES ( '" . $this->_vCouponCode . "','" . $this->_tCouponDescription . "','" . $this->_iExpireDate . "','" . $this->_idtAdded . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {
        $sql = " UPDATE coupon SET ";
        $sql .= ($this->_vCouponCode != null) ? ", vCouponCode = '" . $this->_vCouponCode . "'," : "";
        $sql .= ($this->_tCouponDescription != null) ? ", tCouponDescription = '" . $this->_tCouponDescription . "'," : "";
        $sql .= ($this->_iExpireDate != null) ? ", iExpireDate = '" . $this->_iExpireDate . "'," : "";
        $sql .= ($this->_idtAdded != null) ? ", idtAdded = '" . $this->_idtAdded . "'," : "";
        $sql .= ($this->_eStatus != null) ? ",eStatus = '" . $this->_eStatus . "' ," : "";
        $sql .= "WHERE iCouponId = $id ";

        $UPDATE = clean_sql($sql);
        $this->_obj->sql_query($UPDATE);
    }


}
