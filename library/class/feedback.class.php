<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    feedback
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/feedback.class.php
 * TABLE:        feedback
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class feedback
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iFeedbackId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iFeedbackId;
    protected $_iUserId;
    protected $_vName;
    protected $_vEmail;
    protected $_vPhone;
    protected $_tFeedback;
    protected $_iDtAdded;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iFeedbackId = null;
        $this->_iUserId = null;
        $this->_vName = null;
        $this->_vEmail = null;
        $this->_vPhone = null;
        $this->_tFeedback = null;
        $this->_iDtAdded = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiFeedbackId()
    {
        return $this->_iFeedbackId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiFeedbackId($val)
    {
        $this->_iFeedbackId = $val;
    }

    public function getiUserId()
    {
        return $this->_iUserId;
    }

    public function setiUserId($val)
    {
        $this->_iUserId = $val;
    }

    public function getvName()
    {
        return $this->_vName;
    }

    public function setvName($val)
    {
        $this->_vName = $val;
    }

    public function getvEmail()
    {
        return $this->_vEmail;
    }

    public function setvEmail($val)
    {
        $this->_vEmail = $val;
    }

    public function getvPhone()
    {
        return $this->_vPhone;
    }

    public function setvPhone($val)
    {
        $this->_vPhone = $val;
    }

    public function gettFeedback()
    {
        return $this->_tFeedback;
    }

    public function settFeedback($val)
    {
        $this->_tFeedback = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $where = 'Where 1=1 AND eStatus != "3"';
        if (!is_null($id)) {
            $where .= " AND iFeedbackId = $id";
        }
        $sql = "SELECT * FROM feedback $where";
        $row = $this->_obj->select($sql);

        $this->_iFeedbackId = $row[0]['iFeedbackId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_vName = $row[0]['vName'];
        $this->_vEmail = $row[0]['vEmail'];
        $this->_vPhone = $row[0]['vPhone'];
        $this->_tFeedback = $row[0]['tFeedback'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_eStatus = $row[0]['eStatus'];
        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM feedback WHERE iFeedbackId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iFeedbackId = ""; // clear key for autoincrement

        $sql = "INSERT INTO feedback ( iUserId,vName,vEmail,vPhone,tFeedback,iDtAdded,eStatus ) VALUES ( '" . $this->_iUserId . "','" . $this->_vName . "','" . $this->_vEmail . "','" . $this->_vPhone . "','" . $this->_tFeedback . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "' )";
        $result=$this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE feedback SET  iUserId = '" . $this->_iUserId . "' , vName = '" . $this->_vName . "' , vEmail = '" . $this->_vEmail . "' , vPhone = '" . $this->_vPhone . "' , tFeedback = '" . $this->_tFeedback . "' , iDtAdded = '" . $this->_iDtAdded . "' , eStatus = '" . $this->_eStatus . "'  WHERE iFeedbackId = $id ";
        $this->_obj->sql_query($sql);

    }


}
