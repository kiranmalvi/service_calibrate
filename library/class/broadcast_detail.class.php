<?php
/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    broadcast_detail
 * DATE:         24.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/broadcast_detail.class.php
 * TABLE:        broadcast_detail
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */

class broadcast_detail
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iBroadCastDetailId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iBroadCastDetailId;
    protected $_iBroadcastId;
    protected $_tLocationAddress;
    protected $_vStreetAddress;
    protected $_vZip;
    protected $_vCity;
    protected $_vState;
    protected $_vContact;
    protected $_vPhone;
    protected $_tTime;
    protected $_vOwnerName;
    protected $_dtFromDate;
    protected $_dtToDate;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iBroadCastDetailId = null;
        $this->_iBroadcastId = null;
        $this->_tLocationAddress = null;
        $this->_vStreetAddress = null;
        $this->_vZip = null;
        $this->_vCity = null;
        $this->_vState = null;
        $this->_vContact = null;
        $this->_vPhone = null;
        $this->_tTime = null;
        $this->_vOwnerName = null;
        $this->_dtFromDate = null;
        $this->_dtToDate = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiBroadCastDetailId()
    {
        return $this->_iBroadCastDetailId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiBroadCastDetailId($val)
    {
        $this->_iBroadCastDetailId = $val;
    }

    public function getiBroadcastId()
    {
        return $this->_iBroadcastId;
    }

    public function setiBroadcastId($val)
    {
        $this->_iBroadcastId = $val;
    }

    public function gettLocationAddress()
    {
        return $this->_tLocationAddress;
    }

    public function settLocationAddress($val)
    {
        $this->_tLocationAddress = $val;
    }

    public function getvStreetAddress()
    {
        return $this->_vStreetAddress;
    }

    public function setvStreetAddress($val)
    {
        $this->_vStreetAddress = $val;
    }

    public function getvZip()
    {
        return $this->_vZip;
    }

    public function setvZip($val)
    {
        $this->_vZip = $val;
    }

    public function getvCity()
    {
        return $this->_vCity;
    }

    public function setvCity($val)
    {
        $this->_vCity = $val;
    }

    public function getvState()
    {
        return $this->_vState;
    }

    public function setvState($val)
    {
        $this->_vState = $val;
    }

    public function getvContact()
    {
        return $this->_vContact;
    }

    public function setvContact($val)
    {
        $this->_vContact = $val;
    }

    public function getvPhone()
    {
        return $this->_vPhone;
    }

    public function setvPhone($val)
    {
        $this->_vPhone = $val;
    }

    public function gettTime()
    {
        return $this->_tTime;
    }

    public function settTime($val)
    {
        $this->_tTime = $val;
    }

    public function getvOwnerName()
    {
        return $this->_vOwnerName;
    }

    public function setvOwnerName($val)
    {
        $this->_vOwnerName = $val;
    }

    public function getdtFromDate()
    {
        return $this->_dtFromDate;
    }

    public function setdtFromDate($val)
    {
        $this->_dtFromDate = $val;
    }

    public function getdtToDate()
    {
        return $this->_dtToDate;
    }

    public function setdtToDate($val)
    {
        $this->_dtToDate = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM broadcast_detail WHERE iBroadCastDetailId = $id";
        $row = $this->_obj->select($sql);

        $this->_iBroadCastDetailId = $row[0]['iBroadCastDetailId'];
        $this->_iBroadcastId = $row[0]['iBroadcastId'];
        $this->_tLocationAddress = $row[0]['tLocationAddress'];
        $this->_vStreetAddress = $row[0]['vStreetAddress'];
        $this->_vZip = $row[0]['vZip'];
        $this->_vCity = $row[0]['vCity'];
        $this->_vState = $row[0]['vState'];
        $this->_vContact = $row[0]['vContact'];
        $this->_vPhone = $row[0]['vPhone'];
        $this->_tTime = $row[0]['tTime'];
        $this->_vOwnerName = $row[0]['vOwnerName'];
        $this->_dtFromDate = $row[0]['dtFromDate'];
        $this->_dtToDate = $row[0]['dtToDate'];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM broadcast_detail WHERE iBroadCastDetailId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */
    /* API : broadcast_new_store_opening
            For :  insert Broadcast detail */
    function insert()
    {
        $this->iBroadCastDetailId = ""; // clear key for autoincrement

        $sql = "INSERT INTO broadcast_detail ( iBroadcastId,tLocationAddress,vStreetAddress,vZip,vCity,vState,vContact,vPhone,tTime,vOwnerName,dtFromDate,dtToDate ) VALUES ( '" . $this->_iBroadcastId . "','" . $this->_tLocationAddress . "','" . $this->_vStreetAddress . "','" . $this->_vZip . "','" . $this->_vCity . "','" . $this->_vState . "','" . $this->_vContact . "','" . $this->_vPhone . "','" . $this->_tTime . "','" . $this->_vOwnerName . "','" . $this->_dtFromDate . "','" . $this->_dtToDate . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE broadcast_detail SET  iBroadcastId = '" . $this->_iBroadcastId . "' , tLocationAddress = '" . $this->_tLocationAddress . "' , vStreetAddress = '" . $this->_vStreetAddress . "' , vZip = '" . $this->_vZip . "' , vCity = '" . $this->_vCity . "' , vState = '" . $this->_vState . "' , vContact = '" . $this->_vContact . "' , vPhone = '" . $this->_vPhone . "' , tTime = '" . $this->_tTime . "' , vOwnerName = '" . $this->_vOwnerName . "' , dtFromDate = '" . $this->_dtFromDate . "' , dtToDate = '" . $this->_dtToDate . "'  WHERE iBroadCastDetailId = $id ";
        $this->_obj->sql_query($sql);

    }


}
