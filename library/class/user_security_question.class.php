<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    user_security_question
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/user_security_question.class.php
 * TABLE:        user_security_question
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class user_security_question
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iUserSecQueId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iUserSecQueId;
    protected $_iUserId;
    protected $_vQuestion;
    protected $_tAnswer;
    protected $_iDtAdded;
    protected $_eStatus;


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iUserSecQueId = null;
        $this->_iUserId = null;
        $this->_vQuestion = null;
        $this->_tAnswer = null;
        $this->_iDtAdded = null;
        $this->_eStatus = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiUserSecQueId()
    {
        return $this->_iUserSecQueId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiUserSecQueId($val)
    {
        $this->_iUserSecQueId = $val;
    }

    public function getiUserId()
    {
        return $this->_iUserId;
    }

    public function setiUserId($val)
    {
        $this->_iUserId = $val;
    }

    public function getvQuestion()
    {
        return $this->_vQuestion;
    }

    public function setvQuestion($val)
    {
        $this->_vQuestion = $val;
    }

    public function gettAnswer()
    {
        return $this->_tAnswer;
    }

    public function settAnswer($val)
    {
        $this->_tAnswer = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id)
    {
        $sql = "SELECT * FROM user_security_question WHERE iUserSecQueId = $id";
        $row = $this->_obj->select($sql);

        $this->_iUserSecQueId = $row[0]['iUserSecQueId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_vQuestion = $row[0]['vQuestion'];
        $this->_tAnswer = $row[0]['tAnswer'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_eStatus = $row[0]['eStatus'];
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM user_security_question WHERE iUserSecQueId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iUserSecQueId = ""; // clear key for autoincrement

        $sql = "INSERT INTO user_security_question ( iUserId,vQuestion,tAnswer,iDtAdded,eStatus ) VALUES ( '" . $this->_iUserId . "','" . $this->_vQuestion . "','" . $this->_tAnswer . "','" . $this->_iDtAdded . "','" . $this->_eStatus . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE user_security_question SET  iUserId = '" . $this->_iUserId . "' , vQuestion = '" . $this->_vQuestion . "' , tAnswer = '" . $this->_tAnswer . "' , iDtAdded = '" . $this->_iDtAdded . "' , eStatus = '" . $this->_eStatus . "'  WHERE iUserSecQueId = $id ";
        $this->_obj->sql_query($sql);

    }


}
