<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    cities
 * DATE:         07.01.2015
 * CLASS FILE:   /var/www/html/syaran/library/class/class-making/generated_classes/cities.class.php
 * TABLE:        cities
 * DB:           syaran5
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class function_all
{


    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }

    /* listing of cities  for api_citylist */
    function city_list($country_id, $timestamp)
    {
        global $offset, $limit, $lang;
        $sql1 = '';
        if ($timestamp != "")
            $sql1 .= ' AND modified > "' . $timestamp . '"';
        if ($country_id != '')
            $sql1 .= ' AND country_id = ' . $country_id;
        $sql = 'SELECT id, country_id, ' . $lang . '_name AS name, status, created AS created_date, modified AS updated_date FROM cities WHERE 1=1 ' . $sql1 . ' ORDER BY ' . $lang . '_name LIMIT ' . $offset . ', ' . $limit;
        return $this->_obj->select($sql);
    }


    /* listing of accessory categories for api_accessory_categories  */

    function func_list_all_category($keyword, $timestamp = '0000-00-00 00:00:00', $sql)
    {
        global $offset, $limit, $lang;
        if ($timestamp != '')
            $sql1 = 'AND modified >= "' . $timestamp . '"';
        $sql = 'SELECT id, ' . $lang . '_name AS name, status, created AS created_date, modified AS updated_date FROM accessory_categories WHERE status = "1" ' . $sql1 . ' AND ' . $lang . '_name LIKE "%' . $keyword . '%"  ORDER BY ' . $lang . '_name ASC LIMIT ' . $offset . ', ' . $limit;
        return $this->_obj->sql_query($sql);
    }

    /* check banner for api_allbannerlist */

    function check_exist_banner($timestamp)
    {
        $time = "";
        if ($timestamp == "") {
            $time .= "";
        } else {
            $time .= " AND modified > '{$timestamp}' ";
        }
        $sql = "select count(*) tot from banners where status ='1' {$time} ";
        $result = $this->_obj->sql_query($sql);
        if ($result[0]['tot'] > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /*list of all banner for api_allbannerlist */

    function selectallbanner($timestamp)
    {
        global $offset, $limit;
        $time = "";
        if ($timestamp == "") {
            $time .= "";
        } else {
            $time .= " AND modified > '{$timestamp}' ";
        }

        $sql = " select * from banners where status ='1' {$time} limit {$offset}, {$limit}";
        $row = $this->_obj->select($sql);

        return $row;


    }

}

/* $functionObj  */

