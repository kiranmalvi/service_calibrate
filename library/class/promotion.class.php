<?php

/**
 *
 * -------------------------------------------------------
 * CLASSNAME:    promotion
 * DATE:         23.03.2015
 * CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/promotion.class.php
 * TABLE:        promotion
 * DB:           service_calibrate
 * -------------------------------------------------------
 * AUTHOR:
 * Mindinventory (MI)
 * -------------------------------------------------------
 *
 */
class promotion
{


    /**
     * @desc Variable Declaration with default value
     */

    protected $iPromotionId;   // KEY ATTR. WITH AUTOINCREMENT

    protected $_iPromotionId;
    protected $_eUniqueType;
    protected $_eUniqueId;
    protected $_vPromotionName;
    protected $_vDescription;
    protected $_vImage;
    protected $_iDtAdded;
    protected $_iStartDate;
    protected $_iEndDate;
    protected $_eStatus;
    protected $_iRefId;
    protected $_iUserId;
    protected $_vLogo;

    /**
     * @desc   CONSTRUCTOR METHOD
     */

    function __construct()
    {
        global $obj;
        $this->_obj = $obj;

        $this->_iPromotionId = null;
        $this->_eUniqueType = null;
        $this->_eUniqueId = null;
        $this->_vPromotionName = null;
        $this->_vDescription = null;
        $this->_vImage = null;
        $this->_iDtAdded = null;
        $this->_iStartDate = null;
        $this->_iEndDate = null;
        $this->_eStatus = null;
        $this->_iRefId = null;
        $this->_iUserId = null;
        $this->_vLogo = null;
    }

    /**
     * @desc   DECONSTRUCTOR METHOD
     */

    function __destruct()
    {
        unset($this->_obj);
    }


    /**
     * @desc   GETTER METHODS
     */


    public function getiPromotionId()
    {
        return $this->_iPromotionId;
    }

    /**
     * @desc   SETTER METHODS
     */


    public function setiPromotionId($val)
    {
        $this->_iPromotionId = $val;
    }

    public function geteUniqueType()
    {
        return $this->_eUniqueType;
    }

    public function seteUniqueType($val)
    {
        $this->_eUniqueType = $val;
    }

    public function geteUniqueId()
    {
        return $this->_eUniqueId;
    }

    public function seteUniqueId($val)
    {
        $this->_eUniqueId = $val;
    }

    public function getvPromotionName()
    {
        return $this->_vPromotionName;
    }

    public function setvPromotionName($val)
    {
        $this->_vPromotionName = $val;
    }

    public function getvDescription()
    {
        return $this->_vDescription;
    }

    public function setvDescription($val)
    {
        $this->_vDescription = $val;
    }

    public function getvImage()
    {
        return $this->_vImage;
    }

    public function setvImage($val)
    {
        $this->_vImage = $val;
    }

    public function getiDtAdded()
    {
        return $this->_iDtAdded;
    }

    public function setiDtAdded($val)
    {
        $this->_iDtAdded = $val;
    }

    public function getiStartDate()
    {
        return $this->_iStartDate;
    }

    public function setiStartDate($val)
    {
        $this->_iStartDate = $val;
    }

    public function getiEndDate()
    {
        return $this->_iEndDate;
    }

    public function setiEndDate($val)
    {
        $this->_iEndDate = $val;
    }

    public function geteStatus()
    {
        return $this->_eStatus;
    }

    public function seteStatus($val)
    {
        $this->_eStatus = $val;
    }

    public function getiRefId()
    {
        return $this->_iRefId;
    }

    public function setiRefId($val)
    {
        $this->_iRefId = $val;
    }

    public function getiUserId()
    {
        return $this->_iUserId;
    }

    public function setiUserId($val)
    {
        $this->_iUserId = $val;
    }

    public function getvLogo()
    {
        return $this->_vLogo;
    }

    public function setvLogo($val)
    {
        $this->_vLogo = $val;
    }


    /**
     * @desc   SELECT METHOD / LOAD
     */

    function select($id = null)
    {
        $WHERE = "WHERE 1=1 AND eStatus!='2'";
        if (!is_null($id)) {
            $WHERE .= " AND iPromotionId = $id";
        }

        $sql = "SELECT * FROM promotion $WHERE";
        $row = $this->_obj->select($sql);

        $this->_iPromotionId = $row[0]['iPromotionId'];
        $this->_eUniqueType = $row[0]['eUniqueType'];
        $this->_eUniqueId = $row[0]['eUniqueId'];
        $this->_vPromotionName = $row[0]['vPromotionName'];
        $this->_vDescription = $row[0]['vDescription'];
        $this->_vImage = $row[0]['vImage'];
        $this->_iDtAdded = $row[0]['iDtAdded'];
        $this->_iStartDate = $row[0]['iStartDate'];
        $this->_iEndDate = $row[0]['iEndDate'];
        $this->_eStatus = $row[0]['eStatus'];
        $this->_iRefId = $row[0]['iRefId'];
        $this->_iUserId = $row[0]['iUserId'];
        $this->_vLogo = $row[0]['vLogo'];
        return $row;
    }


    /**
     * @desc   DELETE
     */

    function delete($id)
    {
        $sql = "DELETE FROM promotion WHERE iPromotionId = $id";
        $this->_obj->sql_query($sql);
    }


    /**
     * @desc   INSERT
     */

    function insert()
    {
        $this->iPromotionId = ""; // clear key for autoincrement

        $sql = "INSERT INTO promotion ( eUniqueType,eUniqueId,vPromotionName,vDescription,vImage,iDtAdded,iStartDate,iEndDate,eStatus,iRefId,iUserId,vLogo ) VALUES ( '" . $this->_eUniqueType . "','" . $this->_eUniqueId . "','" . $this->_vPromotionName . "','" . $this->_vDescription . "','" . $this->_vImage . "','" . $this->_iDtAdded . "','" . $this->_iStartDate . "','" . $this->_iEndDate . "','" . $this->_eStatus . "','" . $this->_iRefId . "','" . $this->_iUserId . "','" . $this->_vLogo . "' )";
        $result = $this->_obj->insert($sql);
        return $result;
    }


    /**
     * @desc   UPDATE
     */

    function update($id)
    {

        $sql = " UPDATE promotion SET  eUniqueType = '" . $this->_eUniqueType . "' , eUniqueId = '" . $this->_eUniqueId . "' , vPromotionName = '" . $this->_vPromotionName . "' , vDescription = '" . $this->_vDescription . "' , vImage = '" . $this->_vImage . "' , iDtAdded = '" . $this->_iDtAdded . "' , iStartDate = '" . $this->_iStartDate . "' , iEndDate = '" . $this->_iEndDate . "' , eStatus = '" . $this->_eStatus . "', iRefId = '" . $this->_iRefId . "', iUserId = '" . $this->_iUserId . "' ,vLogo = '" . $this->_vLogo . "' WHERE iPromotionId = $id ";
        $this->_obj->sql_query($sql);

    }
    function update1($id)
    {

        $sql = " UPDATE promotion SET  eUniqueType = '" . $this->_eUniqueType . "' ,  vPromotionName = '" . $this->_vPromotionName . "' , vDescription = '" . $this->_vDescription . "' , vImage = '" . $this->_vImage . "' , iStartDate = '" . $this->_iStartDate . "' , iEndDate = '" . $this->_iEndDate . "' , eStatus = '" . $this->_eStatus . "', iRefId = '" . $this->_iRefId . "' ,vLogo = '" . $this->_vLogo . "' WHERE iPromotionId = $id ";
        $result=$this->_obj->sql_query($sql);


    }

    /* API : promotions
          For: to get promotions list_old */
    function listingPromotions($sql)
    {
        $sql = "SELECT * FROM promotion " . $sql;
        $row = $this->_obj->select($sql);
        return $row;
    }

    function get_retailers()
    {
        $sql = "SELECT vStoreUniqueId,iUserId FROM user Where iUserTypeId in (4,5,6) And iParentId='0' And eStatus='1' ORDER BY vStoreUniqueId ASC";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function select_Promotion($userid, $country, $state, $city, $zip)
    {


        global $promotion_image_path, $promotion_image_url, $dafault_image_logo, $generalfuncobj;

        $today=strtotime(date('Y-m-d 00:00:00'));

        $sql = "SELECT u.vZip,us.iUserId,ct.vName as vCity,us.iUserId,st.vName as vState,cnt.vName as vCountry,pr.* FROM promotion pr
  LEFT JOIN city ct ON (pr.eUniqueType = '0' AND (SELECT iCityId from city where iCityId IN (pr.iRefId)))
  LEFT JOIN user us ON (pr.eUniqueType = '3' AND FIND_IN_SET('$userid',pr.iRefId))
LEFT JOIN user u ON (pr.eUniqueType = '4' AND  FIND_IN_SET('$zip',pr.iRefId))
  LEFT JOIN state st ON (pr.eUniqueType = '2' AND (SELECT iStateId from state where iStateId IN (pr.iRefId)))
  LEFT JOIN country cnt ON (pr.eUniqueType = '1' AND (SELECT iCountryId from country where iCountryId IN (pr.iRefId)))
WHERE ct.vName = '$city' OR st.vName ='$state' or cnt.vName ='$country' or u.vZip='$zip' or us.iUserId=$userid AND pr.eStatus='1' AND pr.iEndDate >'$today' GROUP BY pr.iPromotionId order by pr.iEndDate ASC ";
        $row = $this->_obj->select($sql);

        for ($i = 0; $i < count($row); $i++) {

            $vImage = $row[$i]['vImage'];
            $image_path = $promotion_image_path . $vImage;
            $image_url = $promotion_image_url . $vImage;
            $data[$i]['vImage'] = is_file($image_path) ? $image_url : $dafault_image_logo;

            $vLogo = $row[$i]['vLogo'];
            $logo_path = $promotion_image_path . $vLogo;
            $logo_url = $promotion_image_url . $vLogo;
            $data[$i]['vLogo'] = is_file($logo_path) ? $logo_url : $dafault_image_logo;

            $data[$i]['iPromotionId'] = $row[$i]['iPromotionId'];
            $data[$i]['iUserId'] = $row[$i]['iUserId'];
            $data[$i]['vPromotionName'] = $row[$i]['vPromotionName'];
            $data[$i]['vDescription'] = $row[$i]['vDescription'];
            $data[$i]['iStartDate'] = $generalfuncobj->gmt_Date_Time_FormatWithoutTime($row[$i]["iStartDate"]);
            $data[$i]['iEndDate'] = $generalfuncobj->gmt_Date_Time_FormatWithoutTime($row[$i]['iEndDate']);
            $data[$i]['eStatus'] = $row[$i]['eStatus'];
            $data[$i]['eUniqueType'] = $row[$i]['eUniqueType'];
        }
        return $data;


    }

    function select_Promotion_for_retailer($userid, $scannedid)
    {
        global $promotion_image_path, $promotion_image_url, $dafault_image_logo, $generalfuncobj;

        $today=strtotime(date('Y-m-d 00:00:00'));
        $sql = "SELECT * from promotion where eUniqueType='3' AND FIND_IN_SET($userid,iRefId)AND iUserId='$scannedid' AND eStatus='1' AND iEndDate>'$today' order by iEndDate Asc";
        $row = $this->_obj->select($sql);

        for ($i = 0; $i < count($row); $i++) {

            $vImage = $row[$i]['vImage'];
            $image_path = $promotion_image_path . $vImage;
            $image_url = $promotion_image_url . $vImage;
            $data[$i]['vImage'] = is_file($image_path) ? $image_url : $dafault_image_logo;

            $vLogo = $row[$i]['vLogo'];
            $logo_path = $promotion_image_path . $vLogo;
            $logo_url = $promotion_image_url . $vLogo;
            $data[$i]['vLogo'] = is_file($logo_path) ? $logo_url : $dafault_image_logo;

            $data[$i]['iPromotionId'] = $row[$i]['iPromotionId'];
            $data[$i]['iUserId'] = $row[$i]['iUserId'];
            $data[$i]['vPromotionName'] = $row[$i]['vPromotionName'];
            $data[$i]['vDescription'] = $row[$i]['vDescription'];
            $data[$i]['iStartDate'] = $generalfuncobj->gmt_Date_Time_FormatWithoutTime($row[$i]["iStartDate"]);
            $data[$i]['iEndDate'] = $generalfuncobj->gmt_Date_Time_FormatWithoutTime($row[$i]['iEndDate']);
            $data[$i]['eStatus'] = $row[$i]['eStatus'];
            $data[$i]['eUniqueType'] = $row[$i]['eUniqueType'];
        }
        return $data;


    }


    function select_promotion_by_man_dist($userid)
    {
        global $promotion_image_path, $promotion_image_url, $dafault_image_logo, $generalfuncobj;
        $today=strtotime(date('Y-m-d 00:00:00'));
        $sql = "SELECT * from promotion where iUserId='$userid' AND eStatus!='2' AND iEndDate>'$today' order by iEndDate Asc";
        $row = $this->_obj->select($sql);

        for ($i = 0; $i < count($row); $i++) {

            $vImage = $row[$i]['vImage'];
            $image_path = $promotion_image_path . $vImage;
            $image_url = $promotion_image_url . $vImage;
            $data[$i]['vImage'] = is_file($image_path) ? $image_url : $dafault_image_logo;
            $vLogo = $row[$i]['vLogo'];
            $image_path = $promotion_image_path . $vLogo;
            $image_url = $promotion_image_url . $vLogo;
            $data[$i]['vLogo'] = is_file($image_path) ? $image_url : $dafault_image_logo;

            $data[$i]['iPromotionId'] = $row[$i]['iPromotionId'];
            $data[$i]['iUserId'] = $row[$i]['iUserId'];
            $data[$i]['vPromotionName'] = $row[$i]['vPromotionName'];
            $data[$i]['vDescription'] = $row[$i]['vDescription'];
            $data[$i]['iStartDate'] = $generalfuncobj->gmt_Date_Time_FormatWithoutTime($row[$i]["iStartDate"]);
            $data[$i]['iEndDate'] = $generalfuncobj->gmt_Date_Time_FormatWithoutTime($row[$i]['iEndDate']);
            $data[$i]['eStatus'] = $row[$i]['eStatus'];
            $data[$i]['eUniqueType'] = $row[$i]['eUniqueType'];
        }
        return $data;


    }

    /* ADMIN : promotions
              For: to know promotion is added by user or not */
    function check_promotion_exist($uid)
    {
        $sql = "SELECT * from promotion where iUserId='$uid' AND eStatus!='2'";
        $row = $this->_obj->select($sql);
        return $row;
    }
    function get_logo($uid)
    {
        $sql = "SELECT vImage as vLogo,iUserId from user where iUserId='$uid'";
        $row = $this->_obj->select($sql);
        return $row;
    }

    function select_zip()
    {
        $sql = "SELECT vZip from user group BY vzip";
        $row = $this->_obj->select($sql);
        return $row;
    }



}