<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    plan_feature_mapping
* DATE:         27.03.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/plan_feature_mapping.class.php
* TABLE:        plan_feature_mapping
* DB:           service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class plan_feature_mapping
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iPlanFeatureMapId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iPlanFeatureMapId;  
	protected $_iPlanId;  
	protected $_iFeatureId;  
	protected $_DtAdded;  
	protected $_eStatus;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iPlanFeatureMapId = null; 
		$this->_iPlanId = null; 
		$this->_iFeatureId = null; 
		$this->_DtAdded = null; 
		$this->_eStatus = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiPlanFeatureMapId()
	{
		return $this->_iPlanFeatureMapId;
	}

    /**
     * @desc   SETTER METHODS
     */


    public function setiPlanFeatureMapId($val)
	{
        $this->_iPlanFeatureMapId = $val;
	}

    public function getiPlanId()
	{
        return $this->_iPlanId;
	}

    public function setiPlanId($val)
	{
        $this->_iPlanId = $val;
	}

    public function getiFeatureId()
	{
        return $this->_iFeatureId;
	}

    public function setiFeatureId($val)
	{
        $this->_iFeatureId = $val;
	}

    public function getDtAdded()
	{
        return $this->_DtAdded;
	}

    public function setDtAdded($val)
	{
        $this->_DtAdded = $val;
	}

    public function geteStatus()
	{
        return $this->_eStatus;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM plan_feature_mapping WHERE iPlanFeatureMapId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iPlanFeatureMapId = $row[0]['iPlanFeatureMapId'];
		 $this->_iPlanId = $row[0]['iPlanId'];
		 $this->_iFeatureId = $row[0]['iFeatureId'];
		 $this->_DtAdded = $row[0]['DtAdded'];
		 $this->_eStatus = $row[0]['eStatus'];
	}

    function select_plan_feature($planId, $featureId)
    {
        $sql = "SELECT count(iPlanFeatureMapId) as tot FROM plan_feature_mapping WHERE iPlanId = $planId AND iFeatureId = $featureId";
        $row = $this->_obj->select($sql);
        return $row[0];
    }


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM plan_feature_mapping WHERE iPlanFeatureMapId = $id";
		 $this->_obj->sql_query($sql);
	}

    function delete_plan_feature($planId, $featureId)
    {
        $sql = "DELETE FROM plan_feature_mapping WHERE iFeatureId = $featureId AND iPlanId = $planId";
        $this->_obj->sql_query($sql);
    }


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iPlanFeatureMapId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO plan_feature_mapping ( iPlanId,iFeatureId,DtAdded,eStatus ) VALUES ( '".$this->_iPlanId."','".$this->_iFeatureId."','".$this->_DtAdded."','".$this->_eStatus."' )";
		$result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE plan_feature_mapping SET  iPlanId = '".$this->_iPlanId."' , iFeatureId = '".$this->_iFeatureId."' , DtAdded = '".$this->_DtAdded."' , eStatus = '".$this->_eStatus."'  WHERE iPlanFeatureMapId = $id ";
		 $this->_obj->sql_query($sql);

	}


}
