<?php
/**
*
* -------------------------------------------------------
* CLASSNAME:    promotion_temp
* DATE:         19.06.2015
* CLASS FILE:   /var/www/html/service_calibrate/library/class/class-making/generated_classes/promotion_temp.class.php
* TABLE:        promotion_temp
* DB:           mind_service_calibrate
* -------------------------------------------------------
* AUTHOR:
* Mindinventory (MI)
* -------------------------------------------------------
*
*/

class promotion_temp
{


/**
*   @desc Variable Declaration with default value
*/

	protected $iPromotionId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iPromotionId;  
	protected $_eUniqueType;  
	protected $_eUniqueId;  
	protected $_iUserId;  
	protected $_iRefId;  
	protected $_vPromotionName;  
	protected $_vDescription;  
	protected $_vImage;  
	protected $_iDtAdded;  
	protected $_iStartDate;  
	protected $_iEndDate;  
	protected $_eStatus;  
	protected $_vLogo;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iPromotionId = null; 
		$this->_eUniqueType = null; 
		$this->_eUniqueId = null; 
		$this->_iUserId = null; 
		$this->_iRefId = null; 
		$this->_vPromotionName = null; 
		$this->_vDescription = null; 
		$this->_vImage = null; 
		$this->_iDtAdded = null; 
		$this->_iStartDate = null; 
		$this->_iEndDate = null; 
		$this->_eStatus = null; 
		$this->_vLogo = null; 
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiPromotionId()
	{
		return $this->_iPromotionId;
	}

	public function geteUniqueType()
	{
		return $this->_eUniqueType;
	}

	public function geteUniqueId()
	{
		return $this->_eUniqueId;
	}

	public function getiUserId()
	{
		return $this->_iUserId;
	}

	public function getiRefId()
	{
		return $this->_iRefId;
	}

	public function getvPromotionName()
	{
		return $this->_vPromotionName;
	}

	public function getvDescription()
	{
		return $this->_vDescription;
	}

	public function getvImage()
	{
		return $this->_vImage;
	}

	public function getiDtAdded()
	{
		return $this->_iDtAdded;
	}

	public function getiStartDate()
	{
		return $this->_iStartDate;
	}

	public function getiEndDate()
	{
		return $this->_iEndDate;
	}

	public function geteStatus()
	{
		return $this->_eStatus;
	}

	public function getvLogo()
	{
		return $this->_vLogo;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiPromotionId($val)
	{
		 $this->_iPromotionId =  $val;
	}

	public function seteUniqueType($val)
	{
		 $this->_eUniqueType =  $val;
	}

	public function seteUniqueId($val)
	{
		 $this->_eUniqueId =  $val;
	}

	public function setiUserId($val)
	{
		 $this->_iUserId =  $val;
	}

	public function setiRefId($val)
	{
		 $this->_iRefId =  $val;
	}

	public function setvPromotionName($val)
	{
		 $this->_vPromotionName =  $val;
	}

	public function setvDescription($val)
	{
		 $this->_vDescription =  $val;
	}

	public function setvImage($val)
	{
		 $this->_vImage =  $val;
	}

	public function setiDtAdded($val)
	{
		 $this->_iDtAdded =  $val;
	}

	public function setiStartDate($val)
	{
		 $this->_iStartDate =  $val;
	}

	public function setiEndDate($val)
	{
		 $this->_iEndDate =  $val;
	}

	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}

	public function setvLogo($val)
	{
		 $this->_vLogo =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM promotion_temp WHERE iUserId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iPromotionId = $row[0]['iPromotionId'];
		 $this->_eUniqueType = $row[0]['eUniqueType'];
		 $this->_eUniqueId = $row[0]['eUniqueId'];
		 $this->_iUserId = $row[0]['iUserId'];
		 $this->_iRefId = $row[0]['iRefId'];
		 $this->_vPromotionName = $row[0]['vPromotionName'];
		 $this->_vDescription = $row[0]['vDescription'];
		 $this->_vImage = $row[0]['vImage'];
		 $this->_iDtAdded = $row[0]['iDtAdded'];
		 $this->_iStartDate = $row[0]['iStartDate'];
		 $this->_iEndDate = $row[0]['iEndDate'];
		 $this->_eStatus = $row[0]['eStatus'];
		 $this->_vLogo = $row[0]['vLogo'];
        return $row;
	}




/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM promotion_temp WHERE iUserId = $id";
		 $this->_obj->sql_query($sql);
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iPromotionId = ""; // clear key for autoincrement

        $sql = "INSERT INTO promotion_temp ( eUniqueType,eUniqueId,iUserId,iRefId,vPromotionName,vDescription,vImage,iDtAdded,iStartDate,iEndDate,eStatus,vLogo ) VALUES ( '".$this->_eUniqueType."','".$this->_eUniqueId."','".$this->_iUserId."','".$this->_iRefId."','".$this->_vPromotionName."','".$this->_vDescription."','".$this->_vImage."','".$this->_iDtAdded."','".$this->_iStartDate."','".$this->_iEndDate."','".$this->_eStatus."','".$this->_vLogo."' )";
        $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id)
	{

		 $sql = " UPDATE promotion_temp SET  eUniqueType = '".$this->_eUniqueType."' , eUniqueId = '".$this->_eUniqueId."' , iUserId = '".$this->_iUserId."' , iRefId = '".$this->_iRefId."' , vPromotionName = '".$this->_vPromotionName."' , vDescription = '".$this->_vDescription."' , vImage = '".$this->_vImage."' , iDtAdded = '".$this->_iDtAdded."' , iStartDate = '".$this->_iStartDate."' , iEndDate = '".$this->_iEndDate."' , eStatus = '".$this->_eStatus."' , vLogo = '".$this->_vLogo."'  WHERE iPromotionId = $id ";
		 $this->_obj->sql_query($sql);

	}

    function getPromoInfo($id)
    {
        $sql =  "SELECT * FROM promotion_temp WHERE iPromotionId = $id";
        $row =  $this->_obj->select($sql);
        return $row[0];
    }
}
