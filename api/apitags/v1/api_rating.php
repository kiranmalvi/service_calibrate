<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 18/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: Rating
 * Tag: rating
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for rating==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=rating&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "rating",
 * "feedback" : "feedback,feedback/feedback",
 * "user_id" :"1",
 * "employee_id" :"6",
 * "dist_man_id" :"6",
 * "brand" :"Amul",
 * "rating" :"1",
 * "vImage" :"Amul",
 * "post_type" : "0"
 *
 * }
 *
 ****************************************************************/


include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'rating.class.php');
$ratingObj = new rating();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();

$iUserId = $_REQUEST['user_id'];
$iDistManId = $_REQUEST['dist_man_id'];
$iEmployeeId = $_REQUEST['employee_id'];
$vFeedback = $_REQUEST['feedback'];
$vBrand = $_REQUEST['brand'];
$eRating = $_REQUEST['rating'];
$vImage = $_REQUEST['vImage'];
$ePostType = $_REQUEST['post_type'];
$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
$currenttime=$_REQUEST['current_time'];

$today = strtotime(gmdate('Y-m-d H:i:s'));
/*$lastdate_rate=$ratingObj->get_last_rating($iUserId);*/
$lastdate = strtotime(date('Y-m-1 00:00:00'));


if ($userObj->func_check_existing_userid($iUserId))
{
    if ($ratingObj->check_last_rating($iUserId,$iEmployeeId, $today, $lastdate) === true) {
        $ratingObj->setiUserId($iUserId);
        $ratingObj->setiDistributorId($iDistManId);
        $ratingObj->setiEmployeeId($iEmployeeId);
        $ratingObj->setvBrand($vBrand);
        $ratingObj->seteRating($eRating);
        $ratingObj->setvDescription(addslashes($vFeedback));
        $ratingObj->seteFlag($ePostType);
        $ratingObj->setiDtAdded($iDtAdded);
        $ratingObj->setvTitle($currenttime);


        if (isset($_FILES['vImage'])) {
            $photopath = $rate_image_path;
            $vphoto = $_FILES["vImage"]["tmp_name"];
            $vphoto_name = $_FILES["vImage"]["name"];
            $vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");
            $image_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);
            $image_name = $image_arr[0];

            if ($image_name != '') {
                ## SET IMAGE
                $ratingObj->setvImage($image_name);
            }
        }


        $iRatingId = $ratingObj->insert();
        if($referralObj->check_existing_user($iUserId))
        {
            $referralObj->setiDtUpdated($iDtAdded);
            $referralObj->update_points($iUserId,"100");
        }
        else
        {
            $referralObj->setiUserId($iUserId);
            $referralObj->setvPoint("100");
            $referralObj->seteStatus("1");
            $referralObj->setiDtAdded($iDtAdded);
            $referralObj->setiDtUpdated($iDtAdded);
            $referralObj->setvReason("Rating");
            $iReffaralId=$referralObj->insert();
        }

        if ($iRatingId > 0)
        {
            $points=$referralObj->get_total_point($iUserId);
            $data = $points;
            $status = "1";
            $msg = "Rating Given Successfully";
        } else
        {
            $status = "0";
            $msg = "Rating is not Given,Try Again";
        }

    }

    else
    {
  //  $data = $lastdate;
    $status = "0";
    $msg = "You are allowed to rate once a month to each employee.Please try in 30 days to change your rating.";
    }
} else {
    $status = "0";
    $msg = "User Does not Exist";
}



?>