<?php

/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 21/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: For Send Message
 * Tag: message_send
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for message_send==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=message_send&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "message_send",
 * "user_id" : "1",
 * "employee_id" : "5"
 * "message" : "fdsf"
 * }
 *
 ****************************************************************/
include_once($inc_class_path . 'user.class.php');
$userObj = new user();


include_once($inc_class_path . 'message.class.php');
$messageObj = new message();

include_once($inc_class_path . 'message_conversation.class.php');
$conversationObj = new message_conversation();

include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();


$iUserId     = $_REQUEST['user_id'];
$iEmployeeId = $_REQUEST['friendid'];
$vMessage    = addslashes($_REQUEST['message']);

$eStatus = "1";
$iDtAdded = strtotime(date('d-m-Y H:i:s'));

$data = array();

if ($userObj->func_check_existing_userid($iUserId))
{
    if ($userObj->func_check_existing_userid($iEmployeeId)) {
        $check_exist = $conversationObj->check_user_added($iEmployeeId, $iUserId);

        if (count($check_exist) > 0) {
            $iConversationId = $check_exist[0]['iConversationId'];
            $conversationObj->seteStatus("1");
            $conversationObj->setiDtUpdated($iDtAdded);
            $conversationObj->update_status($iConversationId);
        } else {
            $conversationObj->setiUserId($iEmployeeId);
            $conversationObj->setiUseraddedId($iUserId);
            $conversationObj->seteStatus($eStatus);
            $conversationObj->setiDtAdded($iDtAdded);
            $conversationObj->setiDtUpdated($iDtAdded);
            $iConversationId = $conversationObj->insert();

        }

        if ($iConversationId > 0) {
            $messageObj->setiSenderId($iUserId);
            $messageObj->setiReceiverId($iEmployeeId);
            $messageObj->setvMessage($vMessage);
            $messageObj->seteStatus("1");
            $messageObj->setiDateAdded($iDtAdded);
            $messageObj->setiDtUpdated($iDtAdded);
            $messageObj->seteMessage_Type("m");
            $messageObj->seteRead("0");

            $iMessageId = $messageObj->insert();

            $row = $messageObj->select($iMessageId);

            $data['iMessageId'] = $row[0]['iMessageId'];
            $data['iSenderId'] = $row[0]['iSenderId'];
            $data['iReceiverId'] = $row[0]['iReceiverId'];
            $data['vMessage'] = $row[0]['vMessage'];
            $data['iDateAdded'] = $row[0]['iDateAdded'];
            $data['iDateUpdated'] = $row[0]['iDtUpdated'];
            $data['eStatus'] = $row[0]['eStatus'];


            $setting = $user_settingsObj->select_user_setting($iEmployeeId);
         
            if ($setting[0]['eNotification'] == "1") {
                $senderdata = $userObj->select($iUserId);
                $message1 = "New Message from " . $senderdata[0]['vFirstName'];
                $apiObj->notify($iEmployeeId, $message1, 'M', $iUserId);
            }
            if ($iMessageId > 0) {
                $data = $data;
                $status = "1";
                $msg = "Message is sent";
            } else {
                $status = "0";
                $msg = "Message is not sent";
            }
        } else {
            $status = "0";
            $msg = "User is not added to your chat list";
        }

    }
    else
    {
        $status = "0";
        $msg = "This Employee Does Not Exist";
        
    }
}
else
{
    $status = "0";
    $msg = "This User Does Not Exist";
}

?>