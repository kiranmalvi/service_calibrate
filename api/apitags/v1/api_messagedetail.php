<?php
/**
 * Created by Bipin.
 * User: Bipin
 * Date: 23/4/15
 * Time: 2:32 PM
 *
 * === API Detail===
 * tag  =   messagedetail
 * for  =   message conversation between two users
 *
 * ===API Parameters===
 * Required =   userid, friendid
 * Optional =   timestamp,type (0/1) 0 => OLD, 1 => NEW
 *
 * ===API URL ===
 * http://localhost/service_calibrate/api/?tag=messagedetail&version=1&userid=1&friendid=2&timestamp=1429781400&type=0
 * http://www.magentodevelopments.com/service_calibrate/api/?tag=messagedetail&version=1&userid=1&friendid=2&timestamp=1429781400&type=0
 *
 *************************************************************************************************************************/

/*pr($_REQUEST);exit;*/

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'message_conversation.class.php');
$conversationObj = new message_conversation();

include_once($inc_class_path . 'message.class.php');
$msgObj = new message();

$userid = $_REQUEST['userid'];
$friendid = $_REQUEST['friendid'];
$time = $_REQUEST['timestamp'];

/*if(isset($_REQUEST['timestamp'])) {
    $a = explode(" ", $_REQUEST['timestamp']);
    $date = $a[0];
    $time = $a[1];
    $a = explode("-", $date);
    $month = $a[0];
    $day = $a[1];
    $year = $a[2];
    $a = explode(":", $time);
    $hour = $a[0];
    $min = $a[1];
    $sec = $a[2];

    $time = mktime($hour, $min, $sec, $month, $day, $year);
    pr($time); exit;
}*/

$type = $_REQUEST['type'];

$data = array();

$new_timestamp = strtotime(gmdate('Y-m-d H:i:s'));

if ($userObj->func_check_existing_userid($userid)) {
    if ($userObj->func_check_existing_userid($userid)) {

        $data = $msgObj->chat_messages($userid, $friendid, $time, $type);

        if (count($data) > 0) {
            $status = 1;
            $msg = 'All chat messages';
            $extras->vCurrentTimestamp = $new_timestamp;
        } else {
            $status = 1;
            $msg = 'No more chat messages';
            $extras->vCurrentTimestamp = $new_timestamp;
        }
    } else {
        $msg = 'Invalid friend.';
    }
} else {
    $msg = 'Invalid user.';
}