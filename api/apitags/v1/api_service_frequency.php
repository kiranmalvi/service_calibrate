<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 20/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: Service Frequency Reports
 * Tag: service_frequency
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for service_frequency==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=service_frequency&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "service_frequency",
 * "user_id" : "",
 * "type" :
 *
 *
 * "email" : "chintan.mindinventory1@gmail.com",
 * }
 *
 ****************************************************************/


include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'orders.class.php');
$ordersObj = new  orders();


$iRefId = $_REQUEST['ref_id'];
$iUserId = $_REQUEST['user_id'];
$rtype = $_REQUEST['report_type'];


$type = $userObj->select($iUserId);
$utype = $userObj->getiUserTypeId();


if ($userObj->func_check_existing_userid($iUserId)) {
    if ($rtype == "five") {
        if (!isset($_REQUEST['ref_id'])) {
            $row = $ordersObj->get_last_five($iUserId, $utype);
        } else {
            $row = $ordersObj->get_last_five_with_user($iUserId, $utype, $iRefId);
        }
        $total_amount = 0;
        $total_time = "00:00:00";
        $data = array();
        $SUM = 0;
        for ($i = 0; $i < count($row); $i++) {
            $eid = $row[$i]['iUserId'];
            $scaner_detail = $userObj->select($eid);

            $data[$i]['iOrderId'] = $row[$i]['iOrderId'];
            $data[$i]['vAccountName'] = $userObj->getvFirstName();
            $data[$i]['iScannedId'] =  $row[$i]['iRetailerId'];
            $data[$i]['fAmount'] = $row[$i]['fAmount'];
            $data[$i]['iServiceTime'] = $row[$i]['iTotalTime'];
            $data[$i]['iDeliveryDate'] = $row[$i]['sCurrentTime'];
            /*$data[$i]['iTime'] = date('H:i:s', $row[$i]['iDeliveryDate']);*/
            $data[$i]['iUserId'] = $row[$i]['iUserId'];
            $data[$i]['iDtAdded'] = date('m-d-Y', $row[$i]['iDtAdded']);
            $data[$i]['eFlag'] = $row[$i]['eFlag'];
            $total_amount = $total_amount + $row[$i]['fAmount'];

            //pr($row[$i]['iTotalTime']);

            $T_A = explode(':', $row[$i]['iTotalTime']);
            $SUM += (int)($T_A[0] * 3600) + ($T_A[1] * 60) + ($T_A[2]);

        }
        if (count($row) > 0) {
            $hours = (strlen(floor($SUM / 3600)) == 1) ? '0' . floor($SUM / 3600) : floor($SUM / 3600);
            $minutes = (strlen(floor(($SUM / 60) % 60)) == 1) ? '0' . floor(($SUM / 60) % 60) : floor(($SUM / 60) % 60);
            $seconds = (strlen($SUM % 60) == 1) ? '0' . $SUM % 60 : $SUM % 60;

            $FINAL = $hours . ":" . $minutes . ":" . $seconds;
            $data = $data;
            $extras->fTotal_Amount = $total_amount;
            $extras->fTotal_Time = $FINAL;
            $extras->iStops = count($row);
            $status = "1";
            $msg = "Reports Found";
        } else {
            $status = "0";
            $msg = "No Reports Found";
        }
    } else if ($rtype == "month") {

         $month = $_REQUEST['monthly'];
        $lastdate = strtotime(gmdate('t-' . $month . ' H:i:s'));
       $firstdate = strtotime(gmdate('01-' . $month . ' H:i:s'));


        if (!isset($_REQUEST['ref_id'])) {
            $row = $ordersObj->get_month_report($iUserId, $firstdate, $lastdate, $utype);
        } else {
            $row = $ordersObj->get_month_report_with_user($iUserId, $firstdate, $lastdate, $utype, $iRefId);
        }


        $total_amount = 0;
        $total_time = 0;
        $data = array();
        for ($i = 0; $i < count($row); $i++) {

            $eid = $row[$i]['iUserId'];
            $scaner_detail = $userObj->select($eid);


            $data[$i]['iOrderId'] = $row[$i]['iOrderId'];
            $data[$i]['vAccountName'] = $userObj->getvFirstName();
            $data[$i]['iScannedId'] =  $row[$i]['iRetailerId'];
            $data[$i]['fAmount'] = $row[$i]['fAmount'];
            $data[$i]['iServiceTime'] = $row[$i]['iTotalTime'];
            $data[$i]['iDeliveryDate'] = $row[$i]['sCurrentTime'];
            /*$data[$i]['iTime'] = date('H:i:s', $row[$i]['iDeliveryDate']);*/
            $data[$i]['iUserId'] = $row[$i]['iUserId'];
            $data[$i]['iDtAdded'] = date('m-d-Y H:i:s', $row[$i]['iDtAdded']);
            $data[$i]['eFlag'] = $row[$i]['eFlag'];
            $total_amount = $total_amount + $row[$i]['fAmount'];
            $T_A = explode(':', $row[$i]['iTotalTime']);
            $SUM += (int)($T_A[0] * 3600) + ($T_A[1] * 60) + ($T_A[2]);
        }
        if (count($row) > 0) {
            $hours = (strlen(floor($SUM / 3600)) == 1) ? '0' . floor($SUM / 3600) : floor($SUM / 3600);
            $minutes = (strlen(floor(($SUM / 60) % 60)) == 1) ? '0' . floor(($SUM / 60) % 60) : floor(($SUM / 60) % 60);
            $seconds = (strlen($SUM % 60) == 1) ? '0' . $SUM % 60 : $SUM % 60;

            $FINAL = $hours . ":" . $minutes . ":" . $seconds;
            $data = $data;
            $extras->fTotal_Amount = $total_amount;
            $extras->fTotal_Time = $FINAL;
            $extras->iStops = count($row);
            $status = "1";
            $msg = "Reports Found";
        } else {
            $status = "0";
            $msg = "No Reports Found";

        }
    } else if ($rtype == "custom") {

         $firstdate = strtotime($_REQUEST['start_date'].'00:01:01');
        $lastdate = strtotime($_REQUEST['end_date'].'23:59:59');


        if (!isset($_REQUEST['ref_id'])) {
            $row = $ordersObj->get_custom_report($iUserId, $firstdate, $lastdate, $utype);
        } else {
            $row = $ordersObj->get_custom_report_with_user($iUserId, $firstdate, $lastdate, $utype, $iRefId);
        }

        $total_amount = 0;
        $max_date = "00:00:00";
        $data = array();
        for ($i = 0; $i < count($row); $i++) {

            $eid = $row[$i]['iUserId'];
            $scaner_detail = $userObj->select($eid);

            $data[$i]['iOrderId'] = $row[$i]['iOrderId'];
            $data[$i]['vAccountName'] = $userObj->getvFirstName();
            $data[$i]['iScannedId'] =  $row[$i]['iRetailerId'];
            $data[$i]['fAmount'] = $row[$i]['fAmount'];
            $data[$i]['iServiceTime'] = $row[$i]['iTotalTime'];
            $data[$i]['iDeliveryDate'] = $row[$i]['sCurrentTime'];
            /*$data[$i]['iTime'] = date('H:i:s', $row[$i]['iDeliveryDate']);*/
            $data[$i]['iUserId'] = $row[$i]['iUserId'];
            $data[$i]['iDtAdded'] = date('m-d-Y H:i:s', $row[$i]['iDtAdded']);
            $total_amount = $total_amount + $row[$i]['fAmount'];
            $data[$i]['eFlag'] = $row[$i]['eFlag'];
            $T_A = explode(':', $row[$i]['iTotalTime']);
            $SUM += (int)($T_A[0] * 3600) + ($T_A[1] * 60) + ($T_A[2]);
        }

        if (count($row) > 0) {
            $hours = (strlen(floor($SUM / 3600)) == 1) ? '0' . floor($SUM / 3600) : floor($SUM / 3600);
            $minutes = (strlen(floor(($SUM / 60) % 60)) == 1) ? '0' . floor(($SUM / 60) % 60) : floor(($SUM / 60) % 60);
            $seconds = (strlen($SUM % 60) == 1) ? '0' . $SUM % 60 : $SUM % 60;

            $FINAL = $hours . ":" . $minutes . ":" . $seconds;
            $data = $data;
            $extras->fTotal_Amount = $total_amount;
            $extras->fTotal_Time = $FINAL;
            $extras->iStops = count($row);
            $status = "1";
            $msg = "Reports Found";
        } else {
            $status = "0";
            $msg = "No Reports Found.";

        }
    }

} else {

    $status = "0";
    $msg = "This User Does Not Exist";
}



?>