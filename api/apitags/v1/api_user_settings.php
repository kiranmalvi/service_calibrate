<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 4/4/15
 * Time: 6:54 PM
 */

$iUserId = $_REQUEST['user_id'];

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();

$userObj->setiUserId($iUserId);
$status = 1;

if (isset($_REQUEST['email'])) {
    $emailexist = $userObj->check_exist_mail_withid($_REQUEST['email'], $iUserId);
    if (count($emailexist) > 0) {
        $_SESSION['API_ERROR_MSG'] = "This EmailId Already Exist";
        $status = 0;
    } else {
        $status = 1;
        $userObj->setvEmail($_REQUEST['email']);
    }
}


if (isset($_REQUEST['account_name'])) {
    $accountexist = $userObj->func_check_existing_account($_REQUEST['account_name'], $iUserId);
    if (!$accountexist) {
        $_SESSION['API_ERROR_MSG'] = "Account Already Exist";
        $status = 0;
    } else {
        $status = 1;
        $userObj->setvStoreUniqueId($_REQUEST['account_name']);
    }
}

if (isset($_REQUEST['password'])) 
{

    $oldpassword=md5($_REQUEST['old_password']);
    $password=$_REQUEST['password'];
    $chng=$userObj->get_detail_chnage_pwd($oldpassword,$iUserId);
    if(count($chng)==1)
    {
        $userObj->setvPassword(md5($_REQUEST['password']));
    }
    else
    {
        $_SESSION['API_ERROR_MSG'] = "old Password is wrong";
        $status = 0;
    }
}


if (isset($_REQUEST['first_name'])) {
    $userObj->setvFirstName($_REQUEST['first_name']);
}

if (isset($_REQUEST['last_name'])) {
    $userObj->setvLastName($_REQUEST['last_name']);
}

if (isset($_REQUEST['store_name'])) {
    $userObj->setvStoreName($_REQUEST['store_name']);
}

if (isset($_REQUEST['contact_number'])) {
    $userObj->setvContact($_REQUEST['contact_number']);

}

if (isset($_REQUEST['street_address'])) {
    $userObj->setvAddress($_REQUEST['street_address']);
 $vAddress=$_REQUEST['street_address'];
    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($vAddress) . '&sensor=true');
    $coordinates = json_decode($coordinates);
    $vLatitude = $coordinates->results[0]->geometry->location->lat;
    $vLongitude = $coordinates->results[0]->geometry->location->lng;
    $userObj->setvLatitude($vLatitude);
    $userObj->setvLongitude($vLongitude);

}

if (isset($_REQUEST['zip'])) {
    $userObj->setvZip($_REQUEST['zip']);
}

if (isset($_REQUEST['city'])) {
    $userObj->setvCity($_REQUEST['city']);
}

if (isset($_REQUEST['state'])) {
    $userObj->setvState($_REQUEST['state']);
}
//profile image

if (isset($_REQUEST['store_type_id'])) {
    $userObj->setiTypeOfStoreId($_REQUEST['store_type_id']);
}

if (isset($_REQUEST['prefered_from_time'])) {
    $userObj->setiPreferDeliveryTimeFrom($_REQUEST['prefered_from_time']);
}

if (isset($_REQUEST['prefered_to_time'])) {
    $userObj->setiPreferDeliveryTimeTo($_REQUEST['prefered_to_time']);
}

if (isset($_REQUEST['night_delivery'])) {
    $userObj->seteNightDelivery($_REQUEST['night_delivery']);
}

if (isset($_REQUEST['industries_id'])) {
    $userObj->setiIndustriesId($_REQUEST['industries_id']);
}

if (isset($_REQUEST['notification'])) {
    $user_settingsObj->seteNotification($_REQUEST['notification']);
    $user_settingsObj->update_notification($iUserId);
    $status = 1;
}

if ($status != 0) {
    $RESULT = $userObj->update_user_settings($iUserId);
    $data = array();
    $msg = "User Profile updated successfully";
    $status = 1;
} else {
    $data = array();
    $msg = $_SESSION['API_ERROR_MSG'];
    $status = 0;
}





