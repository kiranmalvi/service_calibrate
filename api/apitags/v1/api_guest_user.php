<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 13/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: Guest User
 * Tag: guest_user
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for guest_user==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=guest_user&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "guest_user",
 * "account_name" : "Chintan",
 * "email" : "chintan.mindinventory1@gmail.com",
 * "contact_number" : "9898989898"
 * "address" : "feedback,feedback/feedback",
 * "comment":"commment by user",
 * "city" : "ahmedabad",
 * "state" : "gujarat",
 * "country" : "India",
 * "zip" : "380027",
 * "street" : "hill Road",
 * "latitude": "32",
 * "longitude" : "72"
 *
 *
 * }
 *
 ****************************************************************/

include_once($inc_class_path . 'user.class.php');
$userObj = new user();


$vAccountName = addslashes($_REQUEST['account_name']);
$ownername=addslashes($_REQUEST['owner_name']);
/*$vEmail = $_REQUEST['email'];
$vContact = $_REQUEST['contact_number'];
$vAddress = addslashes($_REQUEST['address']);

$vComment = addslashes($_REQUEST['comment']);*/
$vCity = addslashes($_REQUEST['city_name']);
$vState = addslashes($_REQUEST['state_name']);
/*$iCountryId = addslashes($_REQUEST['country_name']);*/
$vZip = $_REQUEST['zip'];
$vStreet = addslashes($_REQUEST['address']);
/*$vLatitude = $_REQUEST['latitude'];
$vLongitude = $_REQUEST['longitude'];*/

$iUserTypeId = "7";
$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
$status = "1";
$iParentId = "0";

$adrs = $vAddress . "#@" . $vStreet;


   
        $userObj->setvStoreUniqueId($vAccountName);
        $userObj->setvFirstName($ownername);
      /*  $userObj->setvEmail($vEmail);
        $userObj->setvContact($vContact);*/

        $userObj->setvZip($vZip);
        $userObj->setvAddress($vStreet);
        $userObj->setvCity($vCity);
       /* $userObj->setiCountryId($iCountryId);*/
        $userObj->setvState($vState);
      /*  $userObj->setvComment($vComment);*/
        $userObj->setiUserTypeId($iUserTypeId);
        $userObj->setiDtAdded($iDtAdded);
     /*   $userObj->setvLatitude($vLatitude);
        $userObj->setvLongitude($vLongitude);*/
     /*   $userObj->setvAddress($adrs);*/
        $userObj->seteStatus($status);
        $userObj->setiParentId($iParentId);
        $iUserId = $userObj->insert_guest();
        if ($iUserId > 0) {
            $data = $userObj->select_guest($iUserId);
            $status = 1;
            $msg = 'User Registered Successfully';
        } else {
            $status = 0;
            $msg = 'Try Again';
        }



?>