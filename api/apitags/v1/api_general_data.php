<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 1/4/15
 * Time: 1:23 PM
 */

$data = array();

$timestamp=$_REQUEST['timestamp'];

$date=strtotime(date('Y-m-d H:i:s'));
## Info
include_once($inc_class_path . 'info.class.php');
$info = new info();


$infoc=$generalfuncobj->get_updated_table('info','iDtUpdated',$timestamp);

if($infoc)
{
    $data['info'] = $info->select();
}



/*## city state country
include_once($inc_class_path . 'city.class.php');
include_once($inc_class_path . 'country.class.php');
include_once($inc_class_path . 'state.class.php');

$city = new city();
$country = new country();
$state = new state();

$COUNTRY = $country->select();
$COUNTRY_DATA = array();
$data['country'] = array();
foreach ($COUNTRY as $country_d) {
    #pr($country);
    $COUNTRY_DATA['iCountryId'] = $country_d['iCountryId'];
    $COUNTRY_DATA['vCountryName'] = $country_d['vName'];

    $STATE_DATA = $state->select_on_country_API($country_d['iCountryId']);

    $COUNTRY_DATA['vState'] = array();
    foreach ($STATE_DATA as $state_d) {
        $CITY_R = array();
        $CITY_DATA = $city->select_city_on_state($state_d['iStateId']);
        foreach ($CITY_DATA as $city_d) {
            array_push($CITY_R, $city_d);
        }
        $state_d['vCity'] = $CITY_R;
        array_push($COUNTRY_DATA['vState'], $state_d);
    }
    array_push($data['country'], $COUNTRY_DATA);
}*/


## title
include_once($inc_class_path . 'user_role.class.php');
$user_role = new user_role();

$rolec=$generalfuncobj->get_updated_table('user_role','iDtUpdated',$timestamp);

if($rolec)
{
$ROLE_RESULT = $user_role->select_with_type_API();
$ROLES = array();
foreach ($ROLE_RESULT as $v) {
    $NAME = str_replace(' ', '_', $v['Role']);
    $ROLES['v' . ucfirst(strtolower($NAME))][] = $v;
    #pr($ROLES);

}
    $data['title'] = $ROLES;
}
#pr($ROLES);exit;


## Category
include_once($inc_class_path . 'category.class.php');
$categoryObj = new category();

$catec=$generalfuncobj->get_updated_table('category','iDtUpdated',$timestamp);

if($catec)
{
    $category = $categoryObj->select_API();
    $data['category'] = $category;
}
## How did you hear about us
include_once($inc_class_path . 'how_did_hear.class.php');
$how_did_hearObj = new how_did_hear();

$howc=$generalfuncobj->get_updated_table('how_did_hear','iDtUpdated',$timestamp);

if($howc) {
    $how_did_hear = $how_did_hearObj->select_API();
    $data['how_did_you_hear'] = $how_did_hear;
}
## Industry
include_once($inc_class_path . 'industries.class.php');
$industriesObj = new industries();

$industriesc=$generalfuncobj->get_updated_table('industries','iDtUpdated',$timestamp);

if($industriesc) {
    $industries = $industriesObj->select_API();
    $data['industries'] = $industries;
}
## Store
include_once($inc_class_path . 'user_type_store.class.php');
$user_type_storeObj = new user_type_store();

$user_type_storec=$generalfuncobj->get_updated_table('user_type_store','iDtUpdated',$timestamp);

if($user_type_storec) {
    $user_type_store = $user_type_storeObj->select_API();
    $data['store'] = $user_type_store;
}
## corporate
/*include_once($inc_class_path . 'user.class.php');
$userObj = new user();



    $user = $userObj->select_API();
    $data['corporate'] = $user;
*/
   $data=$data;
    $extras->vTimeStamp = $date;
    $status = 1;
    $msg = "General data found.";