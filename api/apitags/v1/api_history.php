<?php

/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 30/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: For History
 * Tag: history
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for history==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=history&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "history",
 * "user_id" : "1",
 * "friendid" : "5"
 * }
 *
 ****************************************************************/
include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'orders.class.php');
$ordersObj = new orders();

$iUserId=$_REQUEST['user_id'];

$type=$userObj->select_employee($iUserId);

if(count($type)==1)
{
    $usertype="0";
}
else
{
    $usertype="1";
}


$iEmployeeId=$_REQUEST['employee_id'];
$date = strtotime(date('Y-m-d 00:00:00'));
$end = strtotime("-180 day", $date);

if ($userObj->func_check_existing_userid($iUserId)) {

    if(isset($_REQUEST['employee_id']) && $_REQUEST['employee_id']=="0")
    {
        $row = $ordersObj->get_history_all($iUserId,$date,$end);
    }
    else if($iUserId == $iEmployeeId)
    {
        $row = $ordersObj->get_history($iUserId,$date,$end);
    }
    else
    {
        $row = $ordersObj->get_history($iEmployeeId,$date,$end);
    }
    if(count($row)>0)
    {
        $data=array();
        for($i=0;$i<count($row);$i++) {

            $rid = $row[$i]['iRetailerId'];
            $retailer_detail = $userObj->select($rid);
            $data[$i]['iOrderId'] = $row[$i]['iOrderId'];
            $data[$i]['vAccountName'] = $userObj->getvStoreUniqueId();
            $data[$i]['iScannedId'] = $rid;
            $data[$i]['fAmount'] = $row[$i]['fAmount'];
            $data[$i]['iServiceTime'] = $row[$i]['iTotalTime'];
            $data[$i]['iDeliveryDate'] = $row[$i]['iDeliveryDate'];
            $data[$i]['iUserId'] = $row[$i]['iUserId'];
            $data[$i]['iDtAdded'] = date('m-d-Y H:i:s', $row[$i]['iDtAdded']);
            $data[$i]['eFlag'] = $row[$i]['eFlag'];
            $total_amount = $total_amount + $row[$i]['fAmount'];

            //pr($row[$i]['iTotalTime']);

            $T_A = explode(':', $row[$i]['iTotalTime']);
            $SUM += (int)($T_A[0] * 3600) + ($T_A[1] * 60) + ($T_A[2]);
        }



        $hours = (strlen(floor($SUM / 3600)) == 1) ? '0' . floor($SUM / 3600) : floor($SUM / 3600);
        $minutes = (strlen(floor(($SUM / 60) % 60)) == 1) ? '0' . floor(($SUM / 60) % 60) : floor(($SUM / 60) % 60);
        $seconds = (strlen($SUM % 60) == 1) ? '0' . $SUM % 60 : $SUM % 60;

        $FINAL = $hours . ":" . $minutes . ":" . $seconds;
        $data = $data;
        $extras->fTotal_Amount = $total_amount;
        $extras->fTotal_Time = $FINAL;
        $extras->eUser_Type = $usertype;

        $extras->iStops = count($row);
        $status = "1";
        $msg = "History Found";

    }
    else
    {
        $status = "0";
        $msg = "No History Found";
    }
}
else
{
    $status = "0";
    $msg = "This User Does Not Exist";
}