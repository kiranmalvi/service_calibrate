<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 4/4/15
 * Time: 10:37 AM
 */

$iUserId = $_REQUEST['user_id'];
$data = array();

if ($_FILES['image']) {
    include_once($inc_class_path . 'user.class.php');
    $userObj = new user();

    $CHECK = $userObj->func_check_existing_userid($iUserId);

    if ($CHECK === true) {
        if ($_FILES['image']["error"] > 0) {
            $status = 0;
            $msg = 'image not uploaded ' . $_FILES["image"]["error"] . '.';
        } else {
            $photopath = $upload_image_path . 'user/';
            $vphoto = $_FILES["image"]["tmp_name"];
            $vphoto_name = $_FILES["image"]["name"];
            $vphoto_type = $_FILES["image"]["type"];
            $image_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);
            $image_name = $image_arr[0];

            if ($image_name != '') {
                ## UPDATE IMAGE
                $userObj->setvImage($image_name);
                $UPDATE = $userObj->update_profile_img($iUserId);

                 $data['vImage'] = $upload_image_url.'user/'.$image_name;
                $status = 1;
                $msg = "Image Uploaded successfully!!!";
            } else {
                $status = 0;
                $msg = "Image is not Uploaded!";
            }
        }
    } else {
        $status = 0;
        $msg = 'User not found.';
    }
} else {
    $status = 0;
    $msg = 'image is required';
}