<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 18/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: Get Employee of distributor And Manufacurer
 * Tag: get_employee
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for rating==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=rating&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "get_employee",
 * "user_id" :"1"
 *
 * }
 *
 ****************************************************************/

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

$iUserId=$_REQUEST['dist_man_id'];
$user=$userObj->func_check_existing_userid($iUserId);

if($user)
{
    $emplyee=$userObj->select_employee($iUserId);
    if(count($emplyee)>0)
    {
        $data=$emplyee;
        $status="1";
        $msg="done";
    }
    else
    {
        $status="0";
        $msg="No Employee Found";

    }

}
else{
    $status="0";
  $msg="This User Does not Exist";
}
?>