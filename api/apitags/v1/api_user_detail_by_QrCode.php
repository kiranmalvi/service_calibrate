<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 4/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: Get User Details from Qr code
 * Tag: user_detail_by_QrCode
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for user_detail_by_QrCode==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=user_detail_by_QrCode&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "user_detail_by_QrCode",
 * "qr_code" : "1268926463",
 * "login_id" : "1"
 * }
 *
 ****************************************************************/
?>

<?
include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'retailer_detail.class.php');
$retailerdetailObj = new retailer_detail();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();

$qrcode = $_REQUEST['qr_code'];
$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));

$qrexist = $retailerdetailObj->func_check_qrcode_exist($qrcode);
if ($qrexist[0]['total'] > 0) {
    $userid = $qrexist[0]['iUserId'];
    if ($userObj->func_check_existing_userid($userid)) {
        $type = "4,5,6";
      
     
  $userdetail = $userObj->get_data($userid, $type);

        $data = $userdetail;
        $msg = 'Data Found.';
        $status = 1;
    } else {

        $status = 0;
        $msg = 'User Of this QR Code does not Exist.';
    }

} else {
    $status = 0;
    $msg = 'This QR Code Does not Exist.';
}


?>