<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 13/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: Job Done
 * Tag: job_done
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for job_done==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=job_done&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "job_done",
 * "user_id" : "1",
 * "retailer_id" : "1",
 * "famount" : "10.22"
 * "total_time" : "10:05:00",
 * "lat" :"72",
 * "long" :"32",
 * "sing_img" :"abc.jpg",
 * "retailer_name":"abcd",
 * "date_delivery" : "14/04/2015",
 * "service":"0"
 *
 *
 * }
 *
 ****************************************************************/
?>


<?php
/**
 * Created by PhpStorm.
 * User: mind
 * Date: 13/4/15
 * Time: 4:09 PM
 */

?>
<?

include_once($inc_class_path . 'orders.class.php');
$ordersObj = new  orders();

include_once($inc_class_path . 'user.class.php');
$userObj = new  user();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();

$iUserId = $_REQUEST['user_id'];
$iRetailerId = $_REQUEST['retailer_id'];
$eService_type = $_REQUEST['service_type'];

if($eService_type=="0")
{
    $fAmount = $_REQUEST['famount'];
}
else
{
    $fAmount = "0.00";
}

$sCurrentTime=$_REQUEST['current_time'];
$iTotalTime = $_REQUEST['total_time'];
$vLatitude = $_REQUEST['latitude'];
$vLongitude = $_REQUEST['longitude'];
$vImage = $_REQUEST['vImage'];
$vRetailer_Name = addslashes($_REQUEST['retailer_name']);
$iDelivery_date = strtotime('d-m-Y H:i:s',$_REQUEST['date_delivery']);
$time=date('H:i:s');
$date=$_REQUEST['date_delivery']."".$time;
$deliverydate=strtotime($date);



$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
$zipcode = $_REQUEST['zipcode'];


if ($userObj->func_check_existing_userid($iRetailerId)) {
    $ordersObj->setiUserId($iUserId);
    $ordersObj->setfAmount($fAmount);
    $ordersObj->setiTotalTime($iTotalTime);
    $ordersObj->setiDeliveryDate($deliverydate);
    $ordersObj->setiRetailerId($iRetailerId);
    $ordersObj->setvLatitude($vLatitude);
    $ordersObj->setvLongitude($vLongitude);
    $ordersObj->seteServiceType($eService_type);
    $ordersObj->setvRetailer_name($vRetailer_Name);
    $ordersObj->setsCurrentTime($sCurrentTime);

    if ($zipcode == "") {
        $ordersObj->seteFlag("2");
    } else {
        $flag = $ordersObj->get_flag($zipcode, $iRetailerId);
        $ordersObj->seteFlag($flag);
    }



    $ordersObj->seteStatus("1");
    $ordersObj->setiDtAdded($iDtAdded);

    if (isset($_FILES['vImage'])) {
        $photopath = $upload_image_path . 'signature/';
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];
        $vphoto_type = $_FILES["vImage"]["type"];
  $prefix = date("YmdHis");
        $image_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);
        $image_name = $image_arr[0];

        if ($image_name != '') {
            ## SET IMAGE
            $ordersObj->setvSignature($image_name);
        }
    }
    $iOrdersId = $ordersObj->insert();
    
     $user_type=$userObj->userInfo($iRetailerId);
    $type=$user_type[0]['iUserTypeId'];

    if ($iOrdersId > 0) {
 if($type!=7) {
        $check_last_rate = $ordersObj->check_last($iUserId, $iRetailerId);
         $today=strtotime(date('Y-m-d 00:00:00'));

        if ($check_last_rate[1]['iDtAdded'] < $today) {
            if ($referralObj->check_existing_user($iUserId)) {
                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->update_points($iUserId, "20");
            } else {
                $referralObj->setiUserId($iUserId);
                $referralObj->setvPoint("20");
                $referralObj->seteStatus("1");
                $referralObj->setiDtAdded($iDtAdded);
                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->setvReason("Scan Qr Code");
                $iReffaralId = $referralObj->insert();
            }
        }
        }
            $data = $ordersObj->select($iOrdersId);
            $point = $referralObj->get_total_point($iUserId);
            $extras->iPoint = $point['0']['vPoint'];
            $msg = "Orders Details Added Successfully";
            $status = 1;

    }else {
        $data = array();
        $msg = "Try again";
        $status = 0;
    }
} else {
        $data = array();
    $msg = "This Retailer Does not Exist";
        $status = 0;
    }

?>