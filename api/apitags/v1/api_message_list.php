<?php

/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 21/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: For list message conversation
 * Tag: message_list
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for message_list==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=message_list&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "message_list",
 * "user_id" : "1",
 * "user_added_id" : "5"
 *
 *
 * "email" : "chintan.mindinventory1@gmail.com",
 * }
 *
 ****************************************************************/
include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'user_role.class.php');
$userroleObj = new user_role();

include_once($inc_class_path . 'message_conversation.class.php');
$conversationObj = new message_conversation();

include_once($inc_class_path . 'message.class.php');
$messageObj = new message();


$iUserId = $_REQUEST['user_id'];

if ($userObj->func_check_existing_userid($iUserId)) {
    $row_msg = $conversationObj->get_meassage_list($iUserId);
   $data = array();
    if (count($row_msg) > 0) {
     
        for ($i = 0; $i < count($row_msg); $i++) {
            $iUserAddedId = $row_msg[$i]['iUseraddedId'];
            $row = $userObj->select($iUserAddedId);

            $data[$i]['iConversationId'] = $row_msg[$i]['iConversationId'];
            $data[$i]['iUserId'] = $iUserId;
            $data[$i]['iEmployeeId'] = $iUserAddedId;
            $data[$i]['vUser_Name'] = $row[0]['vFirstName'] . " " . $row[0]['vLastName'];
            $data[$i]['vContact'] = $row[0]['vContact'];
            $data[$i]['vAccountName'] = $row[0]['vStoreUniqueId'];
  $data[$i]['vDate'] = $row_msg[$i]['iDtUpdated'];

            if($row[0]['iUserRoleId']!="0")
            {
                $rolename = $userroleObj->select($row[0]['iUserRoleId']);
                $data[$i]['vRole'] = $userroleObj->getvName();
            }
           
            $vImage = $row[0]['vImage'];
            $image_path = $user_image_path . $vImage;
            $image_url = $user_image_url . $vImage;

            $data[$i]['vImage'] = is_file($image_path) ? $image_url : $dafault_image_logo_user;

           $get_unread=$messageObj->get_unread($iUserId,$iUserAddedId);

            if($get_unread > 0)
            {
                $data[$i]['eReadStatus'] = "unread";
            }
            else
            {
                $data[$i]['eReadStatus'] = "read";
            }
        }
        $data = $data;
        $status = "1";
        $msg = "Chat list Found";
    } else
    {
        $status = "0";
        $msg = "Message List is Empty";
    }



} else {
    $status = "0";
    $msg = "This User Does Not Exist";
}

?>