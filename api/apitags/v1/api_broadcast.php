<?php

/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 30/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: For broadcast
 * Tag: broadcast
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for broadcast==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=broadcast&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "broadcast",
 * "user_id" : "1",
 * "broadcast_type" :"1"
 * "date" :"05-24-2015"
 * "message" : "message by user"
 * }
 *
 ****************************************************************/
include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'broadcast.class.php');
$broadcastObj = new broadcast();

include_once($inc_class_path . 'broadcast_detail.class.php');
$broadcast_detailObj = new broadcast_detail();

include_once($inc_class_path . 'orders.class.php');
$ordersObj = new orders();


include_once($inc_class_path . 'message.class.php');
$messageObj = new message();

include_once($inc_class_path . 'message_conversation.class.php');
$conversationObj = new message_conversation();

include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();


$iUserId=$_REQUEST['user_id'];
$vMessage=$_REQUEST['message'];

$DtDate = strtotime($_REQUEST['date']);
$eBrodcastType=$_REQUEST['broadcast_type'];
$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
//date is not proper
if($userObj->func_check_existing_userid($iUserId))
{
    //get parameters according to type & set message
    $data=array();
    if($eBrodcastType=="1")
    {
        $type="New Location Opening";
        $broadcastObj->seteType("0");
        $broadcast_detailObj->setvStreetAddress($_REQUEST['street']);
        $broadcast_detailObj->setvZip($_REQUEST['zip']);
        $broadcast_detailObj->setvCity($_REQUEST['city']);
        $broadcast_detailObj->setvState($_REQUEST['state']);
        $broadcast_detailObj->setvContact($_REQUEST['contact']);
        $broadcast_detailObj->setvphone($_REQUEST['phone']);
        $broadcast_detailObj->settTime($_REQUEST['time']);

       $sendmessage=$type."\n"."  Address Below::"."\n"." ". $_REQUEST['street'].","."\n"." ".","."\n"." ".$_REQUEST['city'].","."\n"." ".$_REQUEST['state'].",".$_REQUEST['zip']." "."\n"." "."Contact Person: ".$_REQUEST['contact'].","."\n"." "."Phone:".$_REQUEST['phone'].","."\n"." "."Opening Date::"."".date('m-d-Y',$DtDate).","."\n"." ".$vMessage;

    }
    else if ($eBrodcastType=="2")
    {
        $type="Temporary close";
        $broadcastObj->seteType("1");
       $sendmessage=$type."\n"."We are closed from ". date('m-d-Y',$DtDate)." to ".date('m-d-Y',strtotime($_REQUEST['date_open']))."  ".$vMessage;
        $broadcast_detailObj->setdtToDate(strtotime($_REQUEST['date_open']));
    }
    else if ($eBrodcastType=="3")
    { 
       $type="Permanent Close";
       $broadcastObj->seteType("2");
       $sendmessage=$type."\n"."We are going to close our store permanently from ". date('m-d-Y',$DtDate)."  ".$vMessage;
    }
    else if ($eBrodcastType=="4")
    {
        $type="OwnerShip change";
       $sendmessage=$type."\n"."The Owner of our store will be ". $_REQUEST['owner_name']." from ". date('m-d-Y',$DtDate)."  ".$vMessage;
        $broadcastObj->seteType("3");
        $broadcast_detailObj->setvOwnerName($_REQUEST['owner_name']);
    }

//set data in broadcast table
    $broadcastObj->setiUserId($iUserId);
    $broadcastObj->setiDtAdded($iDtAdded);
    $broadcastObj->seteStatus("1");
    $broadcastObj->setvMessage($sendmessage);

    $ibrodacastId=$broadcastObj->insert();
//set data in broadcast_detaile table which are common in all form
    $broadcast_detailObj->setiBroadcastId($ibrodacastId);
    $broadcast_detailObj->setdtFromDate($DtDate);

    $ibrodacastdetailId=$broadcast_detailObj->insert();
//get user type & get list of vendor who have scanned their qr code according to type
    $user=$userObj->select($iUserId);
    $type=$userObj->getiUserTypeId();
    if($type == "2" || $type == "3" )
    {
        $receiver=$ordersObj->select_dist_man($iUserId);
    }
    else
    {
        $receiver=$ordersObj->select_retailer($iUserId);
    }

    if(count($receiver) > 0) {
        //send message to all vendor who have scanned qr code
        for ($i = 0; $i < count($receiver); $i++) {
            $receiverId = $receiver[$i]['iScannedId'];
//check if there is any conversation between user & employee,add if no conversation or update status
            $check_exist = $conversationObj->check_user_added($iUserId, $receiverId);

            if (count($check_exist) > 0) {
                $iConversationId = $check_exist[0]['iConversationId'];
                $conversationObj->seteStatus("1");
                $conversationObj->setiDtUpdated($iDtAdded);
                $conversationObj->update_status($iConversationId);
            } else {
                $conversationObj->setiUserId($iUserId);
                $conversationObj->setiUseraddedId($receiverId);
                $conversationObj->seteStatus("1");
                $conversationObj->setiDtAdded($iDtAdded);
                $conversationObj->setiDtUpdated($iDtAdded);
                $iConversationId = $conversationObj->insert();

            }
//same for employee side
            if ($iConversationId > 0) {

                $check_exist_receiver = $conversationObj->check_user_added($receiverId, $iUserId);


                if (count($check_exist_receiver) > 0) {
                    $iConversationId = $check_exist_receiver[0]['iConversationId'];
                    $conversationObj->seteStatus("1");
                    $conversationObj->setiDtUpdated($iDtAdded);
                    $conversationObj->update_status($iConversationId);
                } else {
                    $conversationObj->setiUserId($receiverId);
                    $conversationObj->setiUseraddedId($iUserId);
                    $conversationObj->seteStatus("1");
                    $conversationObj->setiDtAdded($iDtAdded);
                    $conversationObj->setiDtUpdated($iDtAdded);
                    $iConversationId = $conversationObj->insert();

                }

                //send message & set eMessage_Type = "b' for brodacast type messages
                if ($iConversationId > 0) {
                    $messageObj->setiSenderId($iUserId);
                    $messageObj->setiReceiverId($receiverId);
                    $messageObj->setvMessage($sendmessage);
                    $messageObj->seteStatus("1");
                    $messageObj->setiDateAdded($iDtAdded);
                    $messageObj->setiDtUpdated($iDtAdded);
                    $messageObj->seteMessage_Type("b");
                    $messageObj->seteRead("0");

                    $iMessageId = $messageObj->insert();

                    $setting = $user_settingsObj->select_user_setting($iUserId);

                    if ($setting[0]['eNotification'] == "1") {
                        echo "send";
                        $message1 = "New Message from " . $user[0]['vFirstName'];
                        $apiObj->notify($receiverId, $message1, 'M', $iUserId);
                    }

                    if ($iMessageId > 0) {
                        $data[$i]['iUserId'] = $iUserId;
                        $data[$i]['iReceiverId'] = $receiverId;
                        $data[$i]['vMessage'] = $sendmessage;
                        $data[$i]['eMessageType'] = "b";
                    }
                }
            }
        }


        $data = $data;
        $extras->eType = $type;
        $status = "1";
        $msg = "Brodcast sent successfully";
    }
    else
    {
        $status = "0";
        $msg = "There are no vendors";
    }

}
else
{
    $status = "0";
    $msg = "This User Does Not Exist";
}


