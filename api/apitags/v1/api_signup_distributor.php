<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 4/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: Distributor signup
 * Tag: signup_distributor
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for signup_distributor==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=signup_distributor&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "signup_distributor",
 * "account_name" : "CHintan",
 * "email" : "chintan.mindinventory1@gmail.com",
 * "password" : "chintandave",
 * "first_name" : "Chintan",
 * "last_name" : "Dave",
 * "address" : "Mindinventory",
 * "zip" : "380000",
 * "city_name" : "Ahmedabad",
 * "state_name" : "Gujarat",
 * "industries_id" : "1",
 * "category_id" : "1",
 * "role_id" : "1",
 * "contact_number" : "9898989898"
 * "how_about_us" : "1",
 * "user_type_id" : "2",
 * "parent_id" : "0",
 * "plan_type" : "0",
 * "active_plan" : "0",
 * "expire_time" : "0",
 * "amount_paid" : "0.00",
 * "store_count" : "1",
 * "reffral_email": "foram@gmail.com"
 * "longitude" : "32.00",
 * "latitude" : "72.00"
 * }
 *
 ****************************************************************/


include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();

##
$vStoreUniqueId = addslashes($_REQUEST['account_name']);
//$vStoreName = addslashes($_REQUEST['store']);
// parent_id
$iParentId = $_REQUEST['parent_id'];
$vEmail = $_REQUEST['email'];
$vPassword = md5($_REQUEST['password']);
$vFirstName = addslashes($_REQUEST['first_name']);
$vLastName = addslashes($_REQUEST['last_name']);


$vAddress = addslashes($_REQUEST['address']);


$vZip = $_REQUEST['zip'];
$vCity = addslashes($_REQUEST['city_name']);
$vState = addslashes($_REQUEST['state_name']);
$iCoutryId=$_REQUEST['country_name'];
$iIndustriesId = $_REQUEST['industries_id'];
$iCategoryId=$_REQUEST['category_id'];
$iroleId=$_REQUEST['role_id'];
$vContact=$_REQUEST['contact_number'];
$ePlanType="0";
$eActivePaidPlan="0";
$iStoreCount="1";
$fAmountPaid="0.00";


$iExpireTime = strtotime("+30 day", strtotime(date('Y-m-d 23:59:59')));
$eHowAboutUs = $_REQUEST['how_about_us'];
$vReferralEmail=$_REQUEST['reffral_email'];
## Extra
$Address=$vAddress.",".$vCity.",".$vState.",".$vZip;
$coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($Address) . '&sensor=true');

$coordinates = json_decode($coordinates);
$vLatitude = $coordinates->results[0]->geometry->location->lat;
$vLongitude = $coordinates->results[0]->geometry->location->lng;

//$vLongitude = $_REQUEST['longitude'];
//$vLatitude = $_REQUEST['latitude'];
$iUserTypeId = "2";

//for free plan id for Distributor
$iPlanId = '2';

$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
$eStatus = '1';

if ($userObj->func_check_existing_email($vEmail) === true) {
    if ($userObj->func_check_existing_account($vStoreUniqueId) === true) {
        $userObj->setvStoreUniqueId($vStoreUniqueId);
        $userObj->setiParentId($iParentId);
        $userObj->setvEmail($vEmail);
        $userObj->setvPassword($vPassword);
        $userObj->setvFirstName($vFirstName);
        $userObj->setvLastName($vLastName);
        $userObj->setvAddress($vAddress);
        $userObj->setvZip($vZip);
        $userObj->setvCity($vCity);
        $userObj->setvState($vState);
        $userObj->setiIndustriesId($iIndustriesId);
        $userObj->seteHowAboutUs($eHowAboutUs);
        $userObj->setvLatitude($vLatitude);
        $userObj->setvLongitude($vLongitude);
        $userObj->setiUserTypeId($iUserTypeId);
        $userObj->setiPlanId($iPlanId);
        $userObj->setiDtAdded($iDtAdded);
        $userObj->seteStatus($eStatus);

        $userObj->setiCountryId($iCoutryId);
        $userObj->setiUserRoleId($iroleId);
        $userObj->setiCategoryId($iCategoryId);
        $userObj->setvContact($vContact);
        $userObj->setePlanType($ePlanType);
        $userObj->seteActivePaidPlan($eActivePaidPlan);
        $userObj->setiExpireTime($iExpireTime);
        $userObj->setfAmountPaid($fAmountPaid);
        $userObj->setiStoreCount($iStoreCount);
        $userObj->setvReferralEmail($vReferralEmail);


        $userid = $userObj->registerDistributor();

        if ($userid) {
            include_once($inc_class_path . 'user_settings.class.php');
            $user_settings = new user_settings();
            $user_settings->setiUserId($userid);
            $user_settings->setiDtLogin('0');
            $user_settings->seteNotification('1');
            $user_settings->seteDeviceType('phone');

            $user_settings->insert();


            if(isset($_REQUEST['reffral_email']))
            {
                $refmail = $_REQUEST['reffral_email'];
                $row = $userObj->check_exist_mail($refmail);

                $point="100";


                if (count($row) > 0) {
                    $refuserid = $row[0]['iUserId'];
                    if ($referralObj->check_existing_user($refuserid)) {

                        $referralObj->setiDtUpdated($iDtAdded);
                        $referralObj->update_points($refuserid,$point);
                    } else {

                        $referralObj->setiUserId($refuserid);
                        $referralObj->setvPoint($point);
                        $referralObj->seteStatus("1");
                        $referralObj->setiDtAdded($iDtAdded);
                        $referralObj->setiDtUpdated($iDtAdded);
                        $referralObj->setvReason("Reference");
                        $iReffaralId = $referralObj->insert();
                    }
                }
            }

            $data = $userObj->get_data($userid, "2");

            $DATA = array('name' => $vStoreUniqueId);
            echo $generalfuncobj->get_SC_SEND_mail('register', $DATA, $vEmail, 'Welcome to Service Calibrate');
            $status = 1;
            $msg = 'User Register Successfully.';


        } else {
            $status = 0;
            $msg = 'Please try again.';
        }


    } else {
        $status = 0;
        $msg = 'This Account Name already exists.Please try again';
    }
} else {
    $status = 0;
    $msg = 'This Email already exists.Please try again';
}

