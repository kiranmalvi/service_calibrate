<?php

/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 21/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: For Start message conversation
 * Tag: message_start
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for message_start==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=message_start&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "message_start",
 * "user_id" : "1",
 * "employee_id" : "5"
 *
 *
 * "email" : "chintan.mindinventory1@gmail.com",
 * }
 *
 ****************************************************************/
include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'message_conversation.class.php');
$conversationObj = new message_conversation();


$iUserId = $_REQUEST['user_id'];
$iUserAddedId = $_REQUEST['employee_id'];
$eStatus = "1";
$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
$data = array();

if ($userObj->func_check_existing_userid($iUserId)) {
    if ($userObj->func_check_existing_userid($iUserAddedId)) {

        echo $check_exist = $conversationObj->check_user_added($iUserId, $iUserAddedId);
        echo count($check_exist);

        if (count($check_exist) > 0) {
            $iConversationId = $check_exist[0]['iConversationId'];
            $conversationObj->seteStatus("1");
            $conversationObj->setiDtUpdated($iDtAdded);
            $conversationObj->update_status($iConversationId);

            $row = $userObj->select($iUserAddedId);
            $data[0]['iConversationId'] = $iConversationId;
            $data[0]['iUserId'] = $iUserId;
            $data[0]['iEmployeeId'] = $iUserAddedId;
            $data[0]['vUser_Name'] = $row[0]['vFirstName'] . " " . $row[0]['vLastName'];
            $data[0]['vContact'] = $row[0]['vContact'];
            $data[0]['vAccountName'] = $row[0]['vStoreUniqueId'];
     $data[0]['vDate'] = $iDtAdded;

   

            $vImage = $row[0]['vImage'];
            $image_path = $user_image_path . $vImage;
            $image_url = $user_image_url . $vImage;

            $data['0']['vImage'] = is_file($image_path) ? $image_url : $dafault_image_logo_user;


            $data = $data;

            $status = "1";
            $msg = "User Added Successfully";
        } else {
            $conversationObj->setiUserId($iUserId);
            $conversationObj->setiUseraddedId($iUserAddedId);
            $conversationObj->seteStatus($eStatus);
            $conversationObj->setiDtAdded($iDtAdded);
            $conversationObj->setiDtUpdated($iDtAdded);
            $iConversationId = $conversationObj->insert();
            if ($iConversationId > 0) {

                $row = $userObj->select($iUserAddedId);
                $data[0]['iConversationId'] = $iConversationId;
                $data[0]['iUserId'] = $iUserId;
                $data[0]['iUserAdddedId'] = $iUserAddedId;
                $data[0]['vUser_Name'] = $row[0]['vFirstName'] . " " . $row[0]['vLastName'];
                $data[0]['vContact'] = $row[0]['vContact'];
                $data[0]['vAccountName'] = $row[0]['vStoreUniqueId'];
     $data[0]['vDate'] = $iDtAdded;


                $vImage = $row[0]['vImage'];
                $image_path = $user_image_path . $vImage;
                $image_url = $user_image_url . $vImage;

                $data['0']['vImage'] = is_file($image_path) ? $image_url : $dafault_image_logo_user;


                $data = $data;
                $status = "1";
                $msg = "User Added Successfully";
            } else {
                $status = "0";
                $msg = "Please try again";
            }


        }
    } else {
        $status = "0";
        $msg = "This User Does Not Exist";
    }

} else {
    $status = "0";
    $msg = "This User Does Not Exist";
}

?>