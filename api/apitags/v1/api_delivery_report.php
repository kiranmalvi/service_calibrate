<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 21/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: For Delivery Report
 * Tag: delivery_report
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for delivery_report==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=delivery_report&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "delivery_report",
 * "user_id" : "",
 * "type" :
 *
 *
 * "email" : "chintan.mindinventory1@gmail.com",
 * }
 *
 ****************************************************************/
include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'orders.class.php');
$ordersObj = new  orders();


$iUserId = $_REQUEST['user_id'];
$rtype = $_REQUEST['report_type'];


$type = $userObj->select($iUserId);
$utype = $userObj->getiUserTypeId();


if ($userObj->func_check_existing_userid($iUserId)) {
    $start = 0;
    $end = 0;
    if ($rtype == "today") {
        $start = strtotime(gmdate('Y-m-d 00:00:00'));
        $end = strtotime(gmdate('Y-m-d 23:59:59'));
    } else if ($rtype == "tomorrow") {
        $date = strtotime(gmdate('Y-m-d 00:00:00'));
        $start = strtotime("+1 day", $date);

        $date1 = strtotime(gmdate('Y-m-d 23:59:59'));
        $end = strtotime("+1 day", $date1);
    } else if ($rtype == "seven") {
        $start = strtotime(gmdate('Y-m-d 00:00:00'));
        $date = strtotime(gmdate('Y-m-d 23:59:59'));
        $end = strtotime("+6 day", $date);
    } else if ($rtype == "month") {
        $start = strtotime(gmdate('Y-m-d 00:00:00'));
        $date = strtotime(gmdate('Y-m-d 23:59:59'));
        $end = strtotime("+30 day", $date);
    }


    $row = $ordersObj->get_delivery_report($iUserId, $utype, $start, $end);
    $total_amount = 0;
    $total_time = "00:00:00";
    $data = array();
    $SUM = 0;
    for ($i = 0; $i < count($row); $i++) {

        if ($utype == "2" || $utype == "3") {
            $id = $row[$i]['iRetailerId'];
        } else {
            $id = $row[$i]['iUserId'];
        }

        $retailer_detail = $userObj->select($id);

        $data[$i]['iOrderId'] = $row[$i]['iOrderId'];
        $data[$i]['vAccountName'] = $userObj->getvStoreUniqueId();
        $data[$i]['fAmount'] = $row[$i]['fAmount'];
        $data[$i]['iServiceTime'] = $row[$i]['iTotalTime'];
        $data[$i]['iDeliveryDate'] = date('m-d-Y', $row[$i]['iDeliveryDate']);
        $data[$i]['iUserId'] = $row[$i]['iUserId'];
        $data[$i]['iDtAdded'] = date('m-d-Y', $row[$i]['iDtAdded']);
        $data[$i]['eFlag'] = $row[$i]['eFlag'];
        $total_amount = $total_amount + $row[$i]['fAmount'];

        //pr($row[$i]['iTotalTime']);

        $T_A = explode(':', $row[$i]['iTotalTime']);
        $SUM += (int)($T_A[0] * 3600) + ($T_A[1] * 60) + ($T_A[2]);

    }
    if (count($row) > 0) {
        $hours = (strlen(floor($SUM / 3600)) == 1) ? '0' . floor($SUM / 3600) : floor($SUM / 3600);
        $minutes = (strlen(floor(($SUM / 60) % 60)) == 1) ? '0' . floor(($SUM / 60) % 60) : floor(($SUM / 60) % 60);
        $seconds = (strlen($SUM % 60) == 1) ? '0' . $SUM % 60 : $SUM % 60;

        $FINAL = $hours . ":" . $minutes . ":" . $seconds;
        $data = $data;
        $extras->fTotal_Amount = number_format($total_amount,2);
        $extras->fTotal_Time = $FINAL;
        $extras->iStops = count($row);
        $status = "1";
        $msg = "Reports Found";
    } else {
        $status = "0";
        $msg = "No Reports Found";
    }
} else {

    $status = "0";
    $msg = "This User Does Not Exist";
}



?>