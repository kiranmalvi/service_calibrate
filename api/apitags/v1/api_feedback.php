<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 4/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: Feedback To Service Calibrate
 * Tag: feedback
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for signup_distributor==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=feedback&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "feedback",
 * "user_name" : "Chintan",
 * "email" : "chintan.mindinventory1@gmail.com",
 * "contact_number" : "9898989898"
 * "feedback" : "feedback,feedback/feedback",
 * "user_id" :"1"
 *
 * }
 *
 ****************************************************************/


include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'feedback.class.php');
$feedbackObj = new feedback();

##
$vName = addslashes($_REQUEST['user_name']);
$vEmail = $_REQUEST['email'];
$vphone=$_REQUEST['contact_number'];
$tFeedback = addslashes($_REQUEST['feedback']);
$iUserId=$_REQUEST['user_id'];


$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
$eStatus = '1';

if ($userObj->func_check_existing_userid($iUserId)) {

    $feedbackObj->setvName($vName);
    $feedbackObj->setiUserId($iUserId);
    $feedbackObj->setvEmail($vEmail);
    $feedbackObj->setvPhone($vphone);
    $feedbackObj->settFeedback($tFeedback);
    $feedbackObj->seteStatus($eStatus);
    $feedbackObj->setiDtAdded($iDtAdded);

    $feedbackid = $feedbackObj->insert();

    if($feedbackid)
    {
       // $data = $feedbackid;
        $DATA = array('name' => $vName);
        echo $generalfuncobj->get_SC_SEND_mail('feedback', $DATA, $vEmail, 'Thank you for Feedback');
        $status = 1;
        $msg = 'Feedback send Successfully.';
    }
    else
    {
        $status = 0;
        $msg = 'Please try again.';
    }

}
else
{
    $status = 0;
    $msg = "This User Does Not Exist";
}

