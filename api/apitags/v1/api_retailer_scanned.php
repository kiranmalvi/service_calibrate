<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 4/4/2015
 * /**************************************************************
 *
 * ==API DETAILS==
 * For: Get Details of distributor & mnaufacturer who have scanned Qr Code of log in Retailer
 * Tag: retailer_scanned
 *
 *
 * NOTE :- default language is english
 *
 * ==URL==
 *
 *  localhost/service_calibrate/api/?tag=retailer_scanned&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "retailer_scanned",
 * "user_id" : "32" :
 * }
 *
 ****************************************************************/
?>




<?
include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'orders.class.php');
$ordersObj = new orders();

$iUserId = $_REQUEST['user_id'];

if ($userObj->func_check_existing_userid($iUserId)) {
    $scanned = $ordersObj->select_retailer($iUserId);
    if (count($scanned) > 0) {
        $data = $scanned;
        $status = 1;
        $msg = 'Data Found.';
    } else {
        $status = 0;
        $msg = 'There Are no manufacturers or distributors for this retailer.';
    }


} else {
    $status = 0;
    $msg = 'This User is Not Registered With Us.';
}
?>