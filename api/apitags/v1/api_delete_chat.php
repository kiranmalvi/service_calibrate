<?php

/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 21/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: For Send Message
 * Tag: delete_chat
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for delete_chat==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=delete_chat&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "delete_chat",
 * "user_id" : "1",
 * "friendid" : "5"
 * }
 *
 ****************************************************************/
include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'message.class.php');
$messageObj = new message();

include_once($inc_class_path . 'message_conversation.class.php');
$conversationObj = new message_conversation();

$iDtupdated=strtotime(gmdate('Y-m-d H:i:s'));


$iUserId = $_REQUEST['user_id'];
$iEmployeeId = $_REQUEST['friendid'];

if ($userObj->func_check_existing_userid($iUserId))
{
    if($userObj->func_check_existing_userid($iEmployeeId)) {

        $delete = $messageObj->delete_chat($iUserId, $iEmployeeId);
        if($delete=="1")
        {
            $conversationObj->seteStatus("2");
            $conversationObj->setiDtUpdated($iDtupdated);
            $connection=$conversationObj->update_deleted_status($iUserId,$iEmployeeId);
            $data=$delete;
            $status = "1";
            $msg = "Chat Deleted Successfully";
        }
        else
        {
            $status = "0";
            $msg = "Some error occured";
        }

    }
    else
    {
        $status = "0";
        $msg = "This User Does Not Exist";
    }

}
else
{
    $status = "0";
    $msg = "This User Does Not Exist";
}

?>