<?php
/**
 * Created by PhpStorm.
 * User: Vikrant
 * Date: 30/03/2015
 * Time: 4:45 PM
 */
/**************************************************************
 * User: Vikrant
 * Date: 30/03/2015
 * Time: 4:45 PM
 *
 *
 * ==API DETAILS==
 * For: Single Store retailer signup
 * Tag: signup_retailer_single
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for signup_retailer_single==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=signup_retailer_single&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "signup_retailer_single",
 * "account_name" : "CHintan",
 * "store" : "Chintan",
 * "email" : "chintan.mindinventory1@gmail.com",
 * "number_of_store" : "10",
 * "password" : "chintandave",
 * "first_name" : "Chintan",
 * "last_name" : "Dave",
 * "address" : "Mindinventory",
 * "zip" : "380000",
 * "city_name" : "Ahmedabad",
 * "state_name" : "Gujarat",
 * "industries_id" : "1",
 * "type_of_store_id" : "1",
 * "delivery_hours_from" : "11",
 * "delivery_hours_to" : "12",
 * "prefer_delivery_time_from" : "100",
 * "prefer_delivery_time_to" : "110",
 * "night_delivery" : "1",
 * "how_about_us" : "1",
 * "reffral_email" : "foram@gmail.com",
 * "longitude" : "32.00",
 * "latitude" : "72.00"
 * }
 *
 ****************************************************************/

include_once($inc_class_path . 'user.class.php');
$userObj = new user();


include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();

##
$vStoreUniqueId = addslashes($_REQUEST['account_name']);
$vStoreName = addslashes($_REQUEST['store']);
$vEmail = $_REQUEST['email'];
//Number of Store
$NumberOfStore = $_REQUEST['number_of_store'];
$vPassword = md5($_REQUEST['password']);
$vFirstName = addslashes($_REQUEST['first_name']);
$vLastName = addslashes($_REQUEST['last_name']);
$vAddress = addslashes($_REQUEST['address']);
$vZip = $_REQUEST['zip'];
$vCity = addslashes($_REQUEST['city_name']);
$vState = addslashes($_REQUEST['state_name']);
$iCoutryId=$_REQUEST['country_name'];
$iIndustriesId = $_REQUEST['industries_id'];
$iTypeOfStoreId = $_REQUEST['type_of_store_id'];
//Hours of operation From
//Hours of Operation
$iDeliveryHoursFrom = addslashes($_REQUEST['delivery_hours_from']);
$iDeliveryHoursTO = addslashes($_REQUEST['delivery_hours_to']);
$iPreferDeliveryTimeFrom = addslashes($_REQUEST['prefer_delivery_time_from']);
$iPreferDeliveryTimeTo = addslashes($_REQUEST['prefer_delivery_time_to']);
$eNightDelivery = $_REQUEST['night_delivery'];                  // 0 or 1
$eHowAboutUs = $_REQUEST['how_about_us'];
## Extra

$Address=$vAddress.",".$vCity.",".$vState.",".$vZip;
$coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($Address) . '&sensor=true');

$coordinates = json_decode($coordinates);
$vLatitude = $coordinates->results[0]->geometry->location->lat;
$vLongitude = $coordinates->results[0]->geometry->location->lng;

//$vLongitude = $_REQUEST['longitude'];
//$vLatitude = $_REQUEST['latitude'];
## single store Retailer
$iUserTypeId = "5";

//$iUserRoleId = $_REQUEST['role_id'];
$vContact = $_REQUEST['contact_number'];
$ePlanType="0";
$eActivePaidPlan="0";
$iStoreCount="1";
$fAmountPaid="0.00";

$iExpireTime = strtotime("+30 day", strtotime(date('Y-m-d 23:59:59')));

$vReferralEmail=$_REQUEST['reffral_email'];

//for free plan id for single store retailer
$iPlanId = '1';

$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
$eStatus = '1';

if ($userObj->func_check_existing_email($vEmail) === true) {
    if ($userObj->func_check_existing_account($vStoreUniqueId) === true) {
        $userObj->setvStoreUniqueId($vStoreUniqueId);
        $userObj->setvStoreName($vStoreName);
        $userObj->setvEmail($vEmail);
        $userObj->setNumberOfStore($NumberOfStore);
        $userObj->setvPassword($vPassword);
        $userObj->setvFirstName($vFirstName);
        $userObj->setvLastName($vLastName);
        $userObj->setvAddress($vAddress);
        $userObj->setvZip($vZip);
        $userObj->setvCity($vCity);
        $userObj->setvState($vState);
        $userObj->setiIndustriesId($iIndustriesId);
        $userObj->setiTypeOfStoreId($iTypeOfStoreId);
        $userObj->setiDeliveryHoursFrom($iDeliveryHoursFrom);
        $userObj->setiDeliveryHoursTo($iDeliveryHoursTO);
        $userObj->setiPreferDeliveryTimeFrom($iPreferDeliveryTimeFrom);
        $userObj->setiPreferDeliveryTimeTo($iPreferDeliveryTimeTo);
        $userObj->seteNightDelivery($eNightDelivery);
        $userObj->seteHowAboutUs($eHowAboutUs);
        $userObj->setvContact($vContact);

        // $userObj->setiUserRoleId($iUserRoleId);
        $userObj->setiCountryId($iCoutryId);
        $userObj->setePlanType($ePlanType);
        $userObj->seteActivePaidPlan($eActivePaidPlan);
        $userObj->setiExpireTime($iExpireTime);
        $userObj->setfAmountPaid($fAmountPaid);
        $userObj->setiStoreCount($iStoreCount);
        $userObj->setvReferralEmail($vReferralEmail);

        $userObj->setvLatitude($vLatitude);
        $userObj->setvLongitude($vLongitude);
        $userObj->setiUserTypeId($iUserTypeId);
        $userObj->setiPlanId($iPlanId);
        $userObj->setiDtAdded($iDtAdded);
        $userObj->setvComment($NumberOfStore);
        $userObj->seteStatus($eStatus);

        $userid = $userObj->registerRetailerMultiple();



        if ($userid) {
            include_once($inc_class_path . 'user_settings.class.php');
            $user_settings = new user_settings();
            $user_settings->setiUserId($userid);
            $user_settings->setiDtLogin('0');
    $user_settings->seteNotification('1');
            $user_settings->seteDeviceType('phone');

            $user_settings->insert();
            $vStoreUniqueId1 = str_replace(' ', '', $vStoreUniqueId);
            $account=substr($vStoreUniqueId1,0,3);
            $userObj->GENERATE_SC_QR_CODE($userid);

 if(isset($_REQUEST['reffral_email']))
            {
                $refmail = $_REQUEST['reffral_email'];
                $row = $userObj->check_exist_mail($refmail);

                $point="200";


                if (count($row) > 0) {
                    $refuserid = $row[0]['iUserId'];
                    if ($referralObj->check_existing_user($refuserid)) {

                        $referralObj->setiDtUpdated($iDtAdded);
                        $referralObj->update_points($refuserid,$point);
                    } else {

                        $referralObj->setiUserId($refuserid);
                        $referralObj->setvPoint($point);
                        $referralObj->seteStatus("1");
                        $referralObj->setiDtAdded($iDtAdded);
                        $referralObj->setiDtUpdated($iDtAdded);
                        $referralObj->setvReason("Reference");
                        $iReffaralId = $referralObj->insert();
                    }
                }
            }

            $data = $userObj->get_data($userid, "5");
            $DATA = array('name' => $vStoreUniqueId);
            echo $generalfuncobj->get_SC_SEND_mail('register', $DATA, $vEmail, 'Welcome to Service Calibrate');
            $status = 1;
            $msg = 'User Register Successfully.';
        } else {
            $status = 0;
            $msg = 'Please try again.';
        }
    } else {
        $status = 0;
        $msg = 'This Account Name already exists.Please try again';
    }
} else {
    $status = 0;
    $msg = 'This Email already exists.Please try again';
}