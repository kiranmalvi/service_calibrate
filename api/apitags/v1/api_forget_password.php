<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 4/4/2015
 */
/**************************************************************
 *
 * ==API DETAILS==
 * For: Forget Password
 * Tag: forget_password
 *
 *
 * NOTE :- default language is english
 *
 * ==URL for forget_password==
 *
 *
 *
 *  localhost/service_calibrate/api/?tag=forget_password&version=1
 *
 *
 * {
 * "version" : "1",
 * "tag" : "forget_password",
 * "email" : "chintan.mindinventory1@gmail.com",
 * }
 *
 ****************************************************************/


include_once($inc_class_path . 'user.class.php');
$userObj = new user();


$vEmail = $_REQUEST['email'];

$iUserTypeId = $_REQUEST['type'];

if ($iUserTypeId == "1") {
    $iUserTypeId = "4,5,6";
}

if ($userObj->func_check_existing_email($vEmail) === false) {

    $user = $userObj->get_detail_forget_pwd($vEmail, $iUserTypeId);

    if ($user) {
        $user_detail = $userObj->get_userdetail_from_email($vEmail);
        $name = $user_detail[0]['vFirstName']. ' ' . $user_detail[0]['vLastName'];
        $id = base64_encode('id=' . $user_detail[0]['iUserId']);

        $data = $site_url . 'reset_password.php?' . $id;


        $DATA = array('name' => $name, 'url' => $site_url . 'reset_password.php?' . $id);
        echo $generalfuncobj->get_SC_SEND_mail('forgot_password', $DATA, $vEmail, 'Forgot Password');
        $data = array('vName' => 'foram');
        $status = 1;
        $msg = 'Email sent Successfully.';
    } else {
        $data = $user;
        $status = 0;
        $msg = 'You not belong to this Account Type';
    }



} else {
    $status = 0;
     $msg = "We didn't recognize this email! Please try again";
}

