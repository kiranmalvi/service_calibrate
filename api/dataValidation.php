<?php
/**
 * Created by PhpStorm.
 * User: mayur
 * Date: 20/6/14
 * Time: 12:01 PM
 */

/*

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Built-in Validation Rules

required - Required field
equals - Field must match another field (email/password confirmation)
different - Field must be different than another field
accepted - Checkbox or Radio must be accepted (yes, on, 1, true)
numeric - Must be numeric
integer - Must be integer number
length - String must be certain length
lengthBetween - String must be between given lengths
lengthMin - String must be greater than given length
lengthMax - String must be less than given length
min - Minimum
max - Maximum
in - Performs in_array check on given array values
notIn - Negation of in rule (not in array of values)
ip - Valid IP address
email - Valid email address
url - Valid URL
urlActive - Valid URL with active DNS record
alpha - Alphabetic characters only
alphaNum - Alphabetic and numeric characters only
slug - URL slug characters (a-z, 0-9, -, _)
regex - Field matches given regex pattern
date - Field is a valid date
dateFormat - Field is a valid date in the given format
dateBefore - Field is a valid date and is before the given date
dateAfter - Field is a valid date and is after the given date
contains - Field is a string and contains the given string
creditCard - Field is a valid credit card number

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
*/

include_once($inc_class_path . 'validation.class.php');

$check_in = $_REQUEST;
$validatorObj = new Validator($check_in);

//pr($_REQUEST);exit;

switch ($tag) {

    case 'signup_retailer_single':
        $validatorObj->rule('required', ['account_name','email', 'password']);
        $validatorObj->rule('required', ['first_name', 'last_name', 'address', 'zip']);
        $validatorObj->rule('required', ['city_name', 'state_name', 'industries_id', 'type_of_store_id']);
        $validatorObj->rule('required', ['prefer_delivery_time_from', 'prefer_delivery_time_to', 'night_delivery']);
        $validatorObj->rule('required', ['longitude', 'latitude', 'contact_number']);
        $validatorObj->rule('email', 'email');
        break;

    case 'signup_retailer_multiple':
        $validatorObj->rule('required', ['account_name','email', 'number_of_store', 'password']);
        $validatorObj->rule('required', ['first_name', 'last_name', 'address', 'zip']);
        $validatorObj->rule('required', ['city_name', 'state_name', 'industries_id', 'type_of_store_id']);
        $validatorObj->rule('required', ['delivery_hours_from', 'delivery_hours_to']);
        $validatorObj->rule('required', ['prefer_delivery_time_from', 'prefer_delivery_time_to', 'night_delivery']);
        $validatorObj->rule('required', ['longitude', 'latitude', 'contact_number']);
        $validatorObj->rule('email', 'email');
        break;

    case 'signup_retailer_corporate':
        $validatorObj->rule('required', ['account_name', 'email', 'password']);
        $validatorObj->rule('required', ['first_name', 'last_name', 'address', 'zip']);
        $validatorObj->rule('required', ['city_name', 'state_name', 'industries_id', 'type_of_store_id']);
        $validatorObj->rule('required', ['delivery_hours_from', 'delivery_hours_to']);
        $validatorObj->rule('required', ['prefer_delivery_time_from', 'prefer_delivery_time_to', 'night_delivery']);
        $validatorObj->rule('required', ['longitude', 'latitude', 'contact_number']);
        $validatorObj->rule('email', 'email');
        break;

    case 'login' :
        $validatorObj->rule('required', ['email', 'password', 'type']);
        break;
case 'general_data' :
        $validatorObj->rule('required', ['timestamp']);
        break;

    case 'image_upload' :
        $validatorObj->rule('required', ['user_id']);
        break;

    case 'promotion' :
        $validatorObj->rule('required', ['user_id', 'type']);
        break;


    case 'user_detail_by_QrCode' :
        $validatorObj->rule('required', ['qr_code']);
        break;

    case 'retailer_scanned' :
        $validatorObj->rule('required', ['user_id']);
        break;

   case 'dist_man_scanned' :
        $validatorObj->rule('required', ['user_id']);
        break;

 case 'service_frequency' :
        $validatorObj->rule('required', ['user_id','report_type' ]);
        break;

 case 'delivery_report' :
        $validatorObj->rule('required', ['user_id']);
        break;

    case 'promotion_for_retailer' :
        $validatorObj->rule('required', ['scanned_id', 'user_id']);
        break;

    case 'job_done' :
        $validatorObj->rule('required', ['user_id', 'retailer_id', 'service_type', 'latitude']);
        $validatorObj->rule('required', ['longitude', 'total_time','current_time']);
        break;

    case 'guest_user' :
        $validatorObj->rule('required', ['account_name',  'owner_name', 'address']);
        $validatorObj->rule('required', ['zip', 'city_name', 'state_name']);
        break;

     case 'get_employee':
        $validatorObj->rule('required', ['dist_man_id']);
        break;

    case 'rating':
        $validatorObj->rule('required', ['user_id','dist_man_id', 'employee_id']);
        $validatorObj->rule('required', ['rating','post_type','current_time']);
        break;


    case 'signup_distributor':
        $validatorObj->rule('required', ['account_name', 'email', 'password']);
        $validatorObj->rule('required', ['first_name', 'last_name', 'address', 'zip']);
        $validatorObj->rule('required', ['city_name', 'state_name', 'industries_id', 'category_id']);
        $validatorObj->rule('required', ['role_id', 'contact_number']);
   
        $validatorObj->rule('required', ['longitude', 'latitude']);
        $validatorObj->rule('email', 'email');
        break;

    case 'signup_manufacturer':
        $validatorObj->rule('required', ['account_name', 'email', 'password']);
        $validatorObj->rule('required', ['first_name', 'last_name', 'address', 'zip']);
        $validatorObj->rule('required', ['city_name', 'state_name', 'industries_id', 'category_id']);
        $validatorObj->rule('required', ['role_id', 'contact_number']);
    
        $validatorObj->rule('required', ['longitude', 'latitude']);
        $validatorObj->rule('email', 'email');
        break;
    case 'feedback':
        $validatorObj->rule('required', ['user_id', 'user_name', 'email', 'feedback']);
        $validatorObj->rule('email', 'email');
        break;

    case 'user_settings' :
        $validatorObj->rule('required', ['user_id']);
        break;

    case 'forget_password' :
        $validatorObj->rule('required', ['email', 'type']);
        $validatorObj->rule('email', 'email');
  break;
    
    case 'message_start' :
        $validatorObj->rule('required', ['user_id','employee_id']);
        break;        

    case 'message_list' :
        $validatorObj->rule('required', ['user_id']);
        break;

    case 'message_send' :
        $validatorObj->rule('required', ['user_id','friendid','message']);
        break;


    case 'messagedetail' :
        $validatorObj->rule('required', ['userid', 'friendid']);
        break;

 case 'delete_chat' :
        $validatorObj->rule('required', ['user_id', 'friendid']);
        break;
    case 'unreadmessagecount' :
        $validatorObj->rule('required', ['userid']);
        break;

    case 'readunread' :
        $validatorObj->rule('required', ['userid','friendid']);
        break;

 case 'broadcast' :
        $validatorObj->rule('required', ['user_id','message','date','broadcast_type']);
        break;
    case 'history' :
        $validatorObj->rule('required', ['user_id','employee_id']);
        break;
}


if (!$validatorObj->validate()) {
    $msg = $validatorObj->errors();
    $keys = array_keys($msg);
    $msg = $msg[$keys[0]][0];
    $validation_error = true;
}
