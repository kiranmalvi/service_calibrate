<?php
header('Access-Control-Allow-Origin: *');
include_once("include.php");

if (!is_null(json_decode(file_get_contents("php://input"), true))) {
    $_REQUEST = json_decode(file_get_contents("php://input"), true);
}

$generalfuncobj->cleanRequest($_REQUEST);
//pr($_REQUEST);exit;
$version = isset($_REQUEST['version']) ? (is_numeric($_REQUEST['version']) ? $_REQUEST['version'] : NULL) : (NULL);
$tag = isset($_REQUEST['tag']) ? $_REQUEST['tag'] : "";
$userAgent = $_SERVER['HTTP_USER_AGENT'];
$data = new stdClass();
$status = 0;
$msg = "";
$validation_error = false;
$response = new stdClass();
$extras = new stdClass();

// storing data for API log
$VAR_QUERY_STRING = json_encode($_REQUEST);

if (!is_dir('apitags/v' . $version)) {
    $data = 'https://docs.google.com/document/d/1FmT9gmO41cEfVJUH92KGNNqtI_ZSUNuZAA-kiZO8YJw/edit';
    $msg = 'Invalid API Version. Please refer the API docs carefully for proper API usage.';
} else {
    if (in_array($tag, $allowedTags)) {


        include('dataValidation.php');

        if (!$validation_error) {
            include_once('apitags/v' . $version . '/api_' . $tag . '.php');
        }
    } else {

        $data = 'https://docs.google.com/document/d/1FmT9gmO41cEfVJUH92KGNNqtI_ZSUNuZAA-kiZO8YJw/edit';
        $msg = 'Invalid usage of API. Please refer the ' . $COMPANY_NAME . ' API document for proper usage.';
    }
}


$apiObj->sendJSON($apiObj->buildResponse($data, $msg, $status, $extras));
