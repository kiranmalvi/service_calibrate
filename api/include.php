<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 23/3/15
 * Time: 2:35 PM
 */

ob_start();
session_start();
include_once("../library/dbconfig.php");
include_once("../library/settings.php");

include_once($inc_class_path . 'dbconnection.class.php');
if (!isset($obj)) {
    $obj = new myclass($SERVER, $DBASE, $USERNAME, $PASSWORD);
}

include_once($inc_class_path . 'generalfunction.class.php');
$generalfuncobj = new generalfunc();

if(isset($_REQUEST['OS']) && $_REQUEST['OS']=='android') {

    include_once($inc_class_path . 'apia.class.php');
    $apiObj = new APIA();
}else{

    include_once($inc_class_path . 'api.class.php');
    $apiObj = new API();
}
