<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 9/4/15
 * Time: 12:09 PM
 */

include_once('include.php');

if (!isset($_SESSION['SC_LOGIN']['USER'])) {
    header('location:login.php');
    exit;
}

## Plan Type
$USER_PLAN = $_SESSION['SC_LOGIN']['USER']['iPlanId'];
$GET_USER_TYPE = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
$USER_ID = $_SESSION['SC_LOGIN']['USER']['iUserId'];

## User Type
$SQL = 'SELECT iParentId FROM user_type WHERE iUserTypeId = ' . $GET_USER_TYPE;
$USER_TYPE_DATA = $obj->select($SQL);
$USER_TYPE = ($USER_TYPE_DATA[0]['iParentId'] == '0') ? $GET_USER_TYPE : $USER_TYPE_DATA[0]['iParentId'];

$SQL_N = 'SELECT * FROM user_type WHERE iUserTypeId = ' . $USER_TYPE;
$USER_TYPE_NAME = $obj->select($SQL_N);


## GET user plan
include_once($inc_class_path . 'plan.class.php');
$plan = new plan();
$PLAN_DATA = $plan->select_plan_list($USER_TYPE);

## GET user feature
include_once($inc_class_path . 'feature.class.php');
$feature = new feature();
$FEATURE_DATA = $feature->select_feature_list($USER_TYPE);
$ADDITIONAL_FEATURE_DATA = $feature->select_Additional_feature_list($USER_TYPE);

## GET plan feature mapping
include_once($inc_class_path . 'plan_feature_mapping.class.php');
$p_f_mappinf = new plan_feature_mapping();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="mind">
    <title>Service Calibrate</title>
    <!--<link rel="shortcut icon" type="assets/image/x-icon" href="images/favicon.png">-->
    <link rel="shortcut icon" type="assets/image" href="<?php echo $site_url; ?>assets/images/favicon.png">
    <link href="<?php echo $site_url; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/custom.css" rel="stylesheet" type="text/css">

    <!--<link href="css/morphext.css" rel="stylesheet" type="text/css">
    <link href="css/price-style.css" rel="stylesheet" type="text/css">
    -->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo $site_url;?>assets/js/html5shiv.min.js"></script>
    <script src="<?php echo $site_url;?>assets/js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        function popup_display() {
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block'
        }
        function popup_close() {
            document.getElementById('light').style.display = 'none';
            document.getElementById('fade').style.display = 'none'
        }
    </script>
</head>

<body>
<nav class="navbar navbar-custom">
    <div class="login-register-top">
        <div class="container">
            <ul class="navbar-right">
                <li><a href="logout.php" class="red">Logout</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#page-top">
                <img src="<?php echo $site_url; ?>assets/images/logo.png" alt="Service Calibrate">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden"><a href="#"></a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Solutions</a></li>
                <li><a href="#">Customers</a></li>
                <li><a href="#">Company</a></li>
                <li><a href="#">Learn</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>


<section>

    <div class="container">
        <div class="row">

            <?if (isset($_SESSION['err_payment'])) {
                ?>



                <div class="col-md-12">
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Warning!</strong> <?php echo $_SESSION['err_payment']; ?>
                    </div>
                </div>

            <?
            }
            ?>
            <div class="price-ti-mi">
                <h2><?php echo $USER_TYPE_NAME[0]['vName']; ?> Pricing</h2>

                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                    massa.Aenean commodo ligula eget dolor. Aenean massa.</p>
            </div>
        </div>
    </div>
</section>

<form name="SC_plan_feature_Form" id="SC_plan_feature_Form" action="payment_a.php" method="post">
    <section class="price_sec_main">
        <div class="container">
            <div class="row">
                <!--<div class="price-tab-box">-->
                <div class="new-prz-tbl">                    
                    <table>
                    	<thead>
                        	<tr>
                            	<td width="34%"><h3>Features & Details</h3></td>
                                <td width="22%" class="text-center plan-ttl">Basic<span>1-10 Accounts</span></td>
                                <td width="22%" class="text-center plan-ttl">Professional<span>11-50 Accounts</span></td>
                                <td width="22%" class="text-center plan-ttl">Enterprise<span>50+ Accounts</span></td>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
                            	<td><h3>A day Per Account</h3></td>
                                <td class="text-center day-prz">$ 1</td>
                                <td class="text-center day-prz">$ 1.37</td>
                                <td class="text-center day-prz">$ 1.67</td>
                            </tr>
                            <tr>
                            	<td><h3>Monthly Per Account/Store</h3></td>
                                <td class="text-center mnth-prz">$ 30.00</td>
                                <td class="text-center mnth-prz">$ 39.91</td>
                                <td class="text-center mnth-prz">$ 49.50</td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Vendor Messages <span>?</span></h3>
                                    <p>No more wrting behind a paper,Now you can send message to your vendors easily</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Rating <span>?</span></h3>
                                    <p>Now you can communicate & escalate your feedback good/bad on a vendor employee on how is your service</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Stay Anonymous <span>?</span></h3>
                                    <p>When you send a feedback we give you option to hide your info</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Add Vendors <span>?</span></h3>
                                    <p>Add unlimited sales,drivers,merchandisers & managers</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Earn Points & Rewards <span>?</span></h3>
                                    <p>Have fun & earn points to rate a vendor & provide reference</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Add Preffered Delivery time <span>?</span></h3>
                                    <p>Your vendors can effecitently schedule service based on your requirments</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Service Frequency Reports <span>?</span></h3>
                                    <p>See all vendor activities (Measure Say Do Ratio)</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Projected Delivery Report <span>?</span></h3>
                                    <p>No more guess work.See this Daya in real time</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>View Promotions <span>?</span></h3>
                                    <p>See promotions directly from all vendors & distributors (no more depending on employees)</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Check Service Time & amount by Vendor <span>?</span></h3>
                                    <p>Filter by Last 5 visits, monthly visitits or custom Dayes.See how much time each vendor is spending as they committed</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Broadcast <span>?</span></h3>
                                    <p>Let your vendors in your area know if you are a new/temporary closed/new management</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Notify Blind Orders <span>?</span></h3>
                                    <p>See if vendors are writing orders without coming to your account</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Can validate the frequecy comitted <span>?</span></h3>
                                    <p>See if all your vendors are visiting weekly/Monthly or yearly</p>
                                </td>
                               	<td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Can Compare your Service by vendor by Category <span>?</span></h3>
                                    <p>let he best service provider win.Now you can make a smart decision on display/shelf space</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Rapid Fire Order <span>?</span></h3>
                                    <p>See all vendors who serviced your store in less then 5 minutes</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Profile Customization <span>?</span></h3>
                                    <p>Edit your profile anytime</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td>
                                	<h3>Advertisment Free <span>?</span></h3>
                                    <p>We do not show any advetisemnet</p>
                                </td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/yes.png" alt=""></td>
                                <td class="text-center"><img src="http://192.168.1.148/service_calibrate/assets/images/close.png" alt=""></td>
                            </tr>
                            <tr>
                            	<td style="border:none;">
                                	
                                </td>
                                <td class="text-center"><input type="radio" id="basic" name="selectplan"><label for="basic">Select</label></td>
                                <td class="text-center"><input type="radio" id="professional" name="selectplan"><label for="professional">Select</label></td>
                                <td class="text-center"><input type="radio" id="enterprise" name="selectplan"><label for="enterprise">Select</label></td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
               <!-- </div>-->
            </div>
        </div>
    </section>


    <section>
        <div class="container">
            <div class="row">
                <div class="addi_pack_main">
                    <table class="col-md-12" border="1">
                        <tr>
                            <th> Additional Pakages</th>
                            <th class="sel-plan-title"> A Day Per Account</th>
                            <th class="sel-plan-title"> A Day Per Account</th>
                        </tr>
                        <?php
                        foreach ($ADDITIONAL_FEATURE_DATA as $ADDITIONAL) {
                            ?>
                            <tr>
                                <td>
                                    <span><strong> <?php echo $ADDITIONAL['vFeatureName']; ?> </strong> </span>

                                    <p> <?php echo $ADDITIONAL['vDescription']; ?> </p>
                                </td>
                                <td class="addi_par_ac"> $<?php echo $ADDITIONAL['fPrice']; ?> </td>
                                <td>
                                    <div class="oth-sel-chckbx">
                                        <input id="oth-plan<?php echo $ADDITIONAL['iFeatureId']; ?>" type="checkbox"
                                               name="additional_feature[]"
                                               value="<?php echo $ADDITIONAL['iFeatureId']; ?>">
                                        <label for="oth-plan<?php echo $ADDITIONAL['iFeatureId']; ?>">Select</label>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>

                <div class="col-md-12">
                    <div class="col-md-3 addi_how_man"> How many store you have</div>
                    <div class="col-md-4"><input class="addi_how_input" type="text" name="how" id="how"></div>
                </div>
                <div class="col-md-12 addi_how_tem_main">
                    <div class="col-md-6 addi_how_tem">
                        <input type="checkbox" name="how" id="how"> I have agreed <a href="#"> Pricing Terms &
                            Conditions </a>
                    </div>
                    <div class="col-md-12">
                        <a href="#" class="grey-submit" onClick="return popup_display();"> submit </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="light" class="bright_content">
        <a class="sub_close" href="javascript:void(0)" onClick="return popup_close();"> X </a>

        <div class="pop-content">
            <label class="col-md-4"> Card Number </label>
            <input class="col-md-8 col-xs-12" type="text" name="cart-no" id="cart-no">
        </div>
        <div class="pop-content">
            <label class="col-md-4"> CSV Code </label>
            <input class="col-md-8 col-xs-12" type="text" name="csv-code" id="csv-code">
        </div>
        <div class="pop-content">
            <label class="col-md-4"> Expire Month </label>
            <select class="col-md-8 col-xs-12" name="exp-mon" id="exp-mon">
                <option> Select expire month</option>
                <? for ($i = 1; $i <= 12; $i++) { ?>
                    <option value="<?php echo $i; ?>"> <?php echo $i; ?></option>
                <? } ?>

            </select>
        </div>
        <div class="pop-content">
            <label class="col-md-4"> Expire year for payment </label>
            <select class="col-md-8 col-xs-12" name="exp-year" id="exp-year">
                <option> Select expire year for payment</option>

                <option> 2015</option>
                <option> 2016</option>
                <option> 2017</option>
                <option> 2018</option>
                <option> 2019</option>
                <option> 2020</option>
                <option> 2021</option>
            </select>
        </div>
        <div class="pop-content">
            <input type="submit" name="payment" id="payment" value="Payment">
        </div>
    </div>
    <div id="fade" class="dark_overlay"></div>
</form>


<section class="footer-contact">
    <div class="container">
        <h2 class="ftr-contact-icon mi-invisible"
            data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible, after: holdAnimClass"> Get in Touch
            <span></span></h2>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form>
                    <div class="col-md-6">
                        <input type="text" placeholder="First Name">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Last Name">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Email Address">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Phone Number">
                    </div>
                    <div class="col-md-12">
                        <textarea placeholder="Write your message"></textarea>
                    </div>
                    <div class="col-md-12 text-center">
                        <button>Send Message</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<footer class="footer-links">
    <div class="container">
        <div class="row">
            <div class="col-md-9 flinks-main">
                <ul>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Partners</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
            <div class="col-md-3 social-mda text-right">
                <ul>
                    <li><a href="#" class="fb-icon" title="Facebook"></a></li>
                    <li><a href="#" class="tw-icon" title="Twitter"></a></li>
                    <li><a href="#" class="gp-icon" title="Google Plus"></a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 copyright-txt">
                <p>All rights reserved © 2015 <span class="red">servicecalibrate.com</span></p>
            </div>
        </div>
    </div>
</footer>

<script src="<?php echo $site_url; ?>assets/js/jquery.min.js"></script>
<script src="<?php echo $site_url; ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo $site_url; ?>assets/js/anijs.js"></script>
<script src="<?php echo $site_url; ?>assets/js/anijs-helper-scrollreveal.js"></script>
<script src="<?php echo $site_url; ?>assets/js/morphext.js"></script>
<script>
    $("#js-rotating").Morphext({
        animation: 'bounce'
    });
</script>
<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
	
	$(".new-prz-tbl table td h3 span").mouseover(
		function(){			
			$(this).parent().parent().addClass("opened-contant");
	});
	$(".new-prz-tbl table td h3 span").mouseout(
		function(){			
			$(this).parent().parent().removeClass("opened-contant");
	});
</script>
</body>
</html>
<? unset($_SESSION['err_payment']); ?>