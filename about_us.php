<? include("header.php"); ?>
    <section class="container-fluid brad-cramb">
        <div class="container">
            <div class="row">
                <div class="brad-cramb_cont">
                    <a href="index.php">Home <img src="assets/images/arrow_greay.png" alt=""/></a>
                    <strong> &nbsp; About Us <img src="assets/images/arrow_red.png" alt=""/></strong>
                </div>
            </div>
        </div>
    </section>


    <section>
        <div class="container">
            <div class="row">
                <div class="about_con_ntp col-md-12">
                    <h2>Our Story</h2>
                    <div class="about_content_p">We saw opportunity everywhere to help the world’s businesses quantify
                        service. We knew through our 100+ years of industry experience the old adage, “service drives
                        sales”, but we didn’t see any solutions that enabled companies to quantify how much or how often
                        they were either providing or receiving service.When we rolled up our sleeves to really get down
                        to the work of creating a service measurement company we made sure it addressed all the things
                        we cared about, all the things we knew we would want if we were a retailer, distributor or
                        manufacturer. We are the people behind you at Service Calibrate….
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="about_con_ntp col-md-12">
                    <h2>Our Team</h2>
                    <div class="abt-sub-tit">The Master Mind Alliance</div>
                    <div class="about_content_p"> <span> Service Calibrate </span> originated from our Master Mind Team’s obsession with the idea that “Great Service Drives Outstanding Results”. Our mission is to accurately measure the level of service our clients (be it a business or consumer) receive from everyday products and services. Such a simple concept that has never been adequately measured or quantified before, until now….We call it the " The Age of Service Evolution "
                    </div>
                    <div class="about_content_p"> Our Advisory team members have a combined 100 years of experience in sales, marketing, information technology, finance, retail and change management. It is the depth and breadth of our experience that instills confidence into our current and future customers.
                    </div>
                </div>
                <div class="col-md-3 col-sm-5">
                		<img class="abt-sub-tit-img" src="assets/images/Paulo.png" alt=""/>
                </div>
                <div class="col-md-9 col-sm-7">
                		<div class="abt-user">Paulo Azevedo Antunes</div>
                		<div class="about_content_p"> <span> Paulo </span> brings 15 years of his solid financial forecasting and planning experience to the Service Calibrate Team.
                    </div>
                </div>
                <div class="about_con_ntp col-md-12">
                    <div class="about_content_p"> He holds a Masters of Business Administration from the University of Southern California, Marshall School of Business and a B.A. in Business Administration from the University of Sao Paulo with 
Major in Finance and Marketing. He is a performance-driven business strategist with 15 years of leadership driving exceptional financial and operational results and double-digit growth at global organizations across North/South America and Europe.  Key strengths include corporate finance/controlling, strategic planning, business development, identifying opportunities for growth, bottom-line improvement, leading initiatives to optimize margins, boosting revenues, and increasing productivity in fast-paced competitive environments with changing priorities. He is an influential change agent in the Logistics Industry with proven success implementing financial turnaround plans and competitive positioning strategies both domestically and internationally for Fortune 500 organizations.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-5">
                		<img class="abt-sub-tit-img" src="assets/images/Derrick.png" alt=""/>
                </div>
                <div class="col-md-9 col-sm-7">
                		<div class="abt-user">Derrick Bell</div>
                		<div class="about_content_p"> <span> Derrick </span> is Service Calibrate's Communication Strategist and Change Management Guru in an era where “Change is the only constant."
                    </div>
                </div>
                <div class="about_con_ntp col-md-12">
                    <div class="about_content_p"> Derrick holds a B.A. in Political Science from U.C.L.A and has over two decades of successful international and domestic experience in selling and leading change for Industrial & Consumer Packaged Goods (CPG) companies.  He has received numerous awards and accolades for his work with fortune 500 companies like Edward Jones, Bank of America, Merrill Lynch, Coldwell Banker, The Stanley Works, and Nestle. His extensive change management experience and strong breadth of experience within various industries, uniquely qualifies him to advise Service Calibrate through the scope and scale of change that the successful launch of our company will generate. Derrick is a firm believer in practicing what you preach and this philosophy led him to successfully complete several iron distance triathlons.  He is a lifetime member of the Golden Key Honor Society, and is a native Southern Californian.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-5">
                		<img class="abt-sub-tit-img" src="assets/images/Hiram.png" alt=""/>
                </div>
                <div class="col-md-9 col-sm-7">
                		<div class="abt-user">Hiram Mac</div>
                		<div class="about_content_p"> <span> Hiram </span> brings his concrete information technology background in directing, developing and expanding the Service Calibrate Platform
                    </div>
                </div>
                <div class="about_con_ntp col-md-12">
                    <div class="about_content_p"> Hiram holds a B.A. in Economics from the University of Pennsylvania and an M.A. in Statistics from Columbia University. Founder of Nexgence, Inc. an algorithmic driven retail analytics platform that helps small retailers harness the power of sophisticated statistics to manage sales, inventory, and product pricing. He gained the bulk of his information technology experience building mission critical trading and portfolio management systems for various hedge funds in New York City and Los Angeles. As an occasional contributor to open source projects, he is 
drawn to code bases intersecting data science, big data analytics, and human psychology decision processes.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-5">
                		<img class="abt-sub-tit-img" src="assets/images/Dharmesh.png" alt=""/>
                </div>
                <div class="col-md-9 col-sm-7">
                		<div class="abt-user">Dharmesh Vadchhedia</div>
                		<div class="about_content_p"> Creating unique solutions is what <span> Dharmesh </span> enjoys best. His goal with Service Calibrate is to disrupt status quo and create a technologically sound business model using unconventional methods of operation.
                    </div>
                </div>
                <div class="about_con_ntp col-md-12">
                    <div class="about_content_p"> Dharmesh holds a major in business and finance and at heart is passionate about bringing ideas to life. He brings 2 decades of management experience with multi-million dollar Consumer Packaged 
Goods (CPG) projects. He has received numerous awards and accolades for his work with fortune 500 companies like Pepsi, Nestle & Unilever. His extensive CPG industry experience and sound grasp of the B2B selling techniques led him to speak and share his knowledge at the 2014 CITE conference in San Francisco, CA. Dharmesh strongly believes in disruptive innovation that challenges status quo. You will often hear him saying, “If you desire a particular outcome, you will get it, failure is a sign that the desire wasn’t strong enough”.  In his rare downtimes, he enjoys playing soccer & golf. He is an active member of Toast Masters International and has been recognized by them as a Competent Communicator.
                    </div>
                </div>
            </div>
         </div>
    </section>


<? include("footer.php"); ?>