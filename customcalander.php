<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July","August", "September", "October", "November", "December");
if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");
if (!isset($_REQUEST["year"])) $_REQUEST["year"] = date("Y");
$cMonth = $_REQUEST["month"];
$cMonth_1 = $cMonth - 1;
$cMonth_2 = $cMonth - 2;
$cYear = $_REQUEST["year"];
$cYear_1 = $_REQUEST["year"];
$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth-1;
echo $prev_month_1 = $cMonth_1;
#$prev_month_2 = $cMonth_2-2;
$next_month = $cMonth+1;
$next_month_1 = $cMonth_1+2;
#$next_month_2 = $cMonth_1+3;

//current month
if($prev_month == 0){
    $prev_month = 12;
    $prev_year = $cYear - 1;
}
if($next_month == 13 ){
    $next_month = 1;
    $next_year = $cYear + 1;
}

//previous 1 month

if($prev_month_1 <= 0){
    $prev_month_1 = 12;
    $prev_year = $cYear - 1;
    $cMonth_1 = 12;
    $cYear_1 = $prev_year;
    #$cYear = $prev_year;
}
if($next_month_1 >= 13){
    $next_month_1 = 1;
    $next_year = $cYear + 1;
}

//previous 2 month
/*if($prev_month_2 <= 0){
    $prev_month_2 = 12;
    $prev_year = $cYear - 1;
    $cMonth_2 = 12;
    $cYear = $prev_year;
}
if($next_month_2 >= 13){
    $next_month_2 = 1;
    $next_year = $cYear + 1;
}*/


?>
<table>
    <tr>



        <td>
            <table width="200">
                <tr align="center">
                    <td bgcolor="#999999" style="color:#FFFFFF">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="50%" align="left">  <a href="<?php echo $_SERVER["PHP_SELF"] . "?month=". $prev_month_1 . "&year=" . $prev_year; ?>" style="color:#FFFFFF">Previous</a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="100%" border="0" cellpadding="2" cellspacing="2">
                            <tr align="center">
                                <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth_1-1].' '.$cYear_1; ?></strong></td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
                            </tr>
                            <?php
                            $timestamp_1 = mktime(0,0,0,$cMonth_1,1,$cYear_1);
                            $maxday_1 = date("t",$timestamp_1);
                            $thismonth_1 = getdate ($timestamp_1);
                            $startday_1 = $thismonth_1['wday'];

                            for ($i=0; $i<($maxday_1+$startday_1); $i++){
                                if(($i % 7) == 0 ) echo "<tr>";
                                if($i < $startday_1) echo "<td></td>";
                                else echo "<td align='center' valign='middle' height='20px'>". ($i - $startday_1 + 1) . "</td>";
                                if(($i % 7) == 6 ) echo "</tr>";
                            }
                            ?>
                        </table>
                    </td>
                </tr>
            </table>
        </td>



        <td>
            <table width="200">
                <tr align="center">
                    <td bgcolor="#999999" style="color:#FFFFFF">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <!--<td width="50%" align="right"><a href="<?php /*echo $_SERVER["PHP_SELF"] . "?month=". $next_month . "&year=" . $next_year; */?>" style="color:#FFFFFF">Next</a>  </td>-->
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="100%" border="0" cellpadding="2" cellspacing="2">
                            <tr align="center">
                                <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
                                <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
                            </tr>
                            <?php
                            $timestamp = mktime(0,0,0,$cMonth,1,$cYear);
                            $maxday = date("t",$timestamp);
                            $thismonth = getdate ($timestamp);
                            $startday = $thismonth['wday'];

                            for ($i=0; $i<($maxday+$startday); $i++){
                                if(($i % 7) == 0 ) echo "<tr>";
                                if($i < $startday) echo "<td></td>";
                                else echo "<td align='center' valign='middle' height='20px'>". ($i - $startday + 1) . "</td>";
                                if(($i % 7) == 6 ) echo "</tr>";
                            }
                            ?>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>