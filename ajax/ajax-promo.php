<?php
ob_get_clean();
include_once("../include.php");

//pr($_REQUEST);

include_once($inc_class_path . 'promotional_code.class.php');
$promotional_code = new promotional_code();


//pr($_REQUEST);


$code=$_REQUEST['code'];

if($_REQUEST['count'] <= 1 )
{

    $count=$_REQUEST['count'];

}
else
{
    $count=$_REQUEST['count'];
}
$planprice=$_REQUEST['planprice'];
$plantype=$_REQUEST['plantype'];
$qr=$_REQUEST['qr'];
$amount="0.00";

$response = array("MSG" => "", "OUTPUT" => $amount, "color" => "green", "count" => $count);


if($count>1) {
    if ($plantype == "annual") {
        $amount_1 = sprintf("%.2f", $count * $planprice * 12);
        if ($qr == "yes") {
            $famount = ($count * 8) + $amount_1;
            $amount = sprintf("%.2f", $famount);
        } else {
            $amount = $amount_1;
        }

    } else {
        $current = date('d');
        $lastdate = date('t');

        if ($current == $lastdate) {
            $days = 30;
        } else {
            $days = $lastdate - $current;
        }
        $total = $count * $days * ($planprice / $lastdate);
        $amount_1 = sprintf("%.2f", $total);
        if ($qr == "yes") {
            $amount = sprintf("%.2f", ($count * 8) + $amount_1);
        } else {
            $amount = $amount_1;
        }

    }

    if ($code != "") {

        $promo = $promotional_code->check_promo($code);
        if (count($promo) > 0) {

            $offer = $promo[0]['vPromoPrice'];
            $FINAL = ($amount * $offer) / 100;
            $FINALAMOUNT = $amount - $FINAL;
            $FINALAMOUNT1 = sprintf("%.2f", $FINALAMOUNT);

            $response = array("MSG" => "Offer Applied Successfully", "OUTPUT" => $FINALAMOUNT1, "color" => "green", "count" => $count);
        } else {
            $response = array("MSG" => "Invalid Promo Code", "OUTPUT" => $amount, "color" => "red", "count" => $count);
        }
    } else {
        $response = array("MSG" => "Amount To Pay", "OUTPUT" => $amount, "color" => "green", "count" => $count);
    }
}
echo json_encode($response);
exit;
?>