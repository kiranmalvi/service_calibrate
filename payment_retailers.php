<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 8/4/15
 * Time: 11:41 AM
 */


include_once('include.php');
include_once('header.php');
include_once($inc_class_path . 'plan.class.php');
$planObj = new plan();
$planObj->select("1");
$mprice=$planObj->getFMonthPrice();
$dprice=$planObj->getfDayPrice();

include_once($inc_class_path . 'user.class.php');
$userObj = new user();
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];

$user=$userObj->userInfo($iUserId);

$count=$user['vComment'];

if($count>0 && $count!="")
{

    $count=$user['vComment'];
}
else
{

    $count="1";
}



$current= date('d');
$lastdate= date('t');

if($current==$lastdate)
{
    $days=30;
}
else
{
    $days=$lastdate-$current;
}


//echo $date=strtotime(date('d-m-Y'));
//echo "??".$lastdate=strtotime(date('t-m-Y'));
?>
<?php if(isset($_SESSION['frnt_payment_msg']) && $_SESSION['frnt_payment_msg']!=""){?>
    <div class="pull-center alert-danger" align="center" style="font-size: 20px">
        <?php echo $_SESSION['frnt_payment_msg']; ?>
    </div>
<?php }?>
<section class="pay_had_main">
    <div class="container">
        <div class="row">
            <div class="pay_had_main">
                <div class="col-md-4">
                    <img src="assets/images/payment_logo.png" alt="">
                </div>
                <div class="col-md-8">
                    <div class="pay_had_right">
                        All Features, One Price, Real Time Data
                    </div>
                    <div class="pay_had_sub_title">30 Day Free Trial</div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>

    a.tooltips {
        position: relative;
        display: inline;
    }
    a.tooltips span {
        position: absolute;
        width:250px;
        height:auto;
        color: #fff;
        background: #ff0000;
        max-height: 110px;
        font-weight: bolder;
        line-height: 18px;

        text-align: left;
        visibility: hidden;
        border-radius: 4px;
        font-size:.9em;
        padding:3px;
    }
    a.tooltips span:after {
        content: '';
        position: absolute;
        top: 50%;
        right: 100%;
        margin-top: -8px;
        width: 0; height: 0;
        border-right: 8px solid red;
        border-top: 8px solid transparent;
        border-bottom: 8px solid transparent;
    }
    a:hover.tooltips span {
        visibility: visible;
        left: 100%;
        top: 50%;
        margin-top: -30px;
        margin-left: 15px;
        z-index: 999;
    }

</style>
<section style="background-image:url('assets/images/pay-banner.png'); background-size: cover; background-position: right;">
    <div class="container">
        <div class="row">
            <div class="text-center"> <h2> Retailer Pricing </h2> </div>
            <div class="col-md-12">
                <div class="price_title">
                    <sup>$ </sup>  <span> <?php echo $dprice?> </span>
                </div>
                <div class="per-acc">
                    Per Account/Location <span> ( 99 &cent; a Day ) </span>
                    <p>Per Month If Paid Annually</p>
                </div>
                <div class="col-md-3 text-center">
                    <div class="per-or">	OR	</div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="price_title_nex">
                    <sup>$</sup>	<span><?php echo $mprice?> </span>
                </div>
                <div class="per-acc_nex">
                    Per Account/Month
                    <p>If Paid Monthly</p>
                </div>
                <div class="per-link-content">For Large Enterprise: <br>
                    Please contact us on <b> <a href="mailto:sales@servicecalibrate.com"> sales@servicecalibrate.com </a> </b> for pricing</div>
            </div>
        </div>
    </div>
</section>


<section class="pay_sin-acc">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                Single Account/Employee use it for <span> Free! </span> We support & promote small businesses
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>All Features</h2>
            </div>
        </div>
        <div class="row">
            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/service.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Service Frequency
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p> Measure the "Say vs. Do" ratio and know exactly how often & how much time your Vendors/Distributors/Manufacturers or your Employees spend at any of your locations. Simple and powerful filters allows you to manage unlimited Vendors/Employees. </p>

                </div>
            </div>
            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/rating.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Rating (Option To Stay Anonymous)
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Have your complaints/feedback fallen on deaf ears? Are your managers tired of a certain Vendors but don't have any quantifiable data to prove their poor performance? Service Calibrate gives you detail, a photo & map views of all the ratings given by your Employees/Managers to a particular Vendor (thumbs up or thumbs down).  Now you can communicate & escalate feedback to the management (Vendor) when you receive substandard/poor service.  It also allow for anonymous feedback.</p>

                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/promation.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>View Promotions
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>"This deal/promotion is no longer available" ever heard that before? We can help ensure you have all the latest promos are right at your fingertips!  You want to buy deals so your customers gets the best price.  Service Calibrate provides you direct access to all the promotions as soon as Vendor uploads on Service Calibrate platform. </p>
                </div>
            </div>


            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/salesl.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Service As Schedule
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>
                        Easy monthly calendar view shows when you are scheduled to receive service vs. actual service day. Monitor missed service calls by week or see if your locations are serviced on expected time/day.</p>

                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/service.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Rapid Fire Visit
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Have you noticed your Vendor representative simply "stop by" your location and then leave right away? Service Calibrate identifies exactly which Vendor visits were under duration vs. required minimum as per your  service frequency agreements/expectations.</p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/project.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Projected Deliveries
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Expecting a delivery? But don't know when its scheduled, would you like to know the day/week/month & amount in advance?  Now you will be able to manage this for all your Vendors, right on your smart phone & tablet.</p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/promation.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Messages
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Need service immediately? Want to message all, some, or just one of your Vendors without storing their contact information on your personal phone? Service Calibrate saves all your Vendor contact list automatically at one place. Now you can send & receive messages to/from your Vendors with the touch of a button.</p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/contact_pay.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Contacts
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Tired of keeping track of Vendors & their contact information? Has your sales person switched jobs? Now you can have all the contacts at your finger tips for all your Vendors.</p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/service.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Broadcast
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Communicate with all your Vendors quick with the information such as, a new location needs service, you are temporary closed, permanently closed or if there is a change in management/ownership.</p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/salesl.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Compare Service
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>Have you ever experienced "over commit & under deliver" from Vendors? Make more informed decisions based on facts provided by Service Calibrate. Need to justify your purchases for an internal compliance department?  No problem, you can compare service for up to 5 Vendors and see who provides the best service. Stop negotiating on price alone and gain the upper hand in negotiations with suppliers large and small based on their performance. </p>
                </div>
            </div>

            <div class="service_box_main">
                <div class="col-md-1">
                    <img src="assets/images/sig.png" alt="">
                </div>
                <div class="col-md-10 ser_title">
                    <a>Signature Confirmation
                        <img src="assets/images/check.png" alt="">
                        <img class="ser_right_arrow" src="assets/images/drop.png" alt="">
                    </a>
                    <p>You can mandate Vendors/Distributors/Manufacturers employees to sign in electronically! This validates their visit at your location.</p>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="how-to-gray">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2>How do you want to Pay?</h2>
            </div>
        </div>
        <div class="row">

            <div class="col-md-4 text-center how-to-bor">
                <div class="per-acc"> Monthly </div>
                <div class="price_title" style="font-size:80px;">
                    <sup>$ </sup><span> <?php echo $mprice?> </span>
                </div>
                <div class="how-mont-pal"> Per Account/Location/Month </div>
                <h2 class="how-plan"> </h2>
                <div class="col-md-8 col-md-offset-2">
                    <form name="frmmnth" id="frmmnth" method="post" action="transaction.php">
                        <input type="hidden" value="no" id="qr" name="qr">
                        <input type="hidden" name="days" value="<?php echo $days?>" id="days">
                        <input type="hidden" name="formname" value="frmmnth" id="formname">
                        <input type="text" value="<?php echo $mprice;?>" name="mplanprice" id="mplanprice" style="display: none">
                        <div class="location-price">
                            <label> Please Enter # of Location </label>
                            <input type="text" name="mnprice" id="mnprice" value="<?php echo $count?>" onkeyup="checkpromomnth()" placeholder="">
                        </div>
                        <div class="location-price">
                            <label> Promotional Code </label>
                            <input type="text" name="mpromocode" placeholder="Promo Code" id="mpromocode" onkeyup="checkpromomnth()">
                        </div>
                        <div class="location-year">
                            <input type="text" name="total" id="mtotal" value="<?php echo sprintf ("%.2f", $count * $days * ($mprice/30));?>" readonly>
                            <span id="mspan"></span>
                            <h6 class="yer-mont">Recurring Monthly</h6>
                        </div>
                        <div class="location-price">
                            <input type="button" name="mnthbutton" id="mnthbutton" value="Select" class="prices-selected" onclick="selectmonth()">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4 text-center how-to-bor">
                <div class="per-acc"> Annually </div>
                <!--<div class="price_title_nex">
                    $	<span> 29.99 </span>
                </div>-->
                <div class="price_title" style="font-size:80px;">
                    <sup>$  </sup><span><?php echo $dprice?></span>
                </div>
                <div class="how-mont-pal"> Per Account/Location/Month </div>
                <h2 class="how-plan"> </h2>
                <div class="col-md-8 col-md-offset-2">
                    <form name="frmanual" id="frmanual"  method="post" action="transaction.php">
                        <input type="hidden" value="no" id="qr" name="qr">
                        <input type="hidden" name="formname" value="frmmnth id="formname">
                        <input type="text" value="<?php echo $dprice;?>" name="aplanprice" id="aplanprice" style="display: none">
                        <div class="location-price">
                            <label> Please Enter # of Location </label>
                            <input type="text" name="aprice"  id="aprice" placeholder="" value="<?php echo $count?>" onkeyup="checkpromo()">
                        </div>
                        <div class="location-price">
                            <label> Promotional Code </label>
                            <input type="text"  name="promocode" placeholder="Promo Code" id="promocode" onkeyup="checkpromo()">
                        </div>
                        <div class="location-year">
                            <input type="text" name="total" id="atotal" value="<?php echo sprintf ("%.2f",$count * $dprice*12)?>" readonly>
                            <span id="aspan"></span>
                            <h6 class="yer-mont">Yearly</h6>
                        </div>
                        <div class="location-price">
                            <input type="button"  name="annualbutton" id="annualbutton" value="Select" onclick="selectannual()" >
                        </div>
                    </form>
                </div>
            </div>


            <?if(isset($_REQUEST['page'])&&$_REQUEST['page']=="regi"){?>

                <div class="col-md-4 text-center">
                    <div class="per-acc"> Free </div>
                    <!--<div class="price_title_nex">
                        $	<span> 35.00 </span>
                    </div>-->
                    <div class="price_title" style="font-size:80px;">
                        <sup>$</sup><span> 0.00 </span>
                    </div>
                    <div class="how-mont-pal"> 30 Day Trial </div>
                    <h2 class="how-plan"> </h2>
                    <div class="col-md-8 col-md-offset-2">
                        <form name="frmfree" id="frmfree" method="post" action="transaction.php">
                            <input type="hidden" value="no" id="qr" name="qr">
                            <input type="hidden" name="formname" value="frmmnth" id="formname">
                            <div class="location-price">
                                <label> Please Enter # of Location </label>
                                <input type="text" name="freeprice" id="freeprice" placeholder="" value="<?php echo $count?>" onfocus="getprice1()">
                            </div>

                            <div class="free-price">
                                <input type="button" name="freebutton" id="freebutton" value="Select" onclick="selectfree()">
                            </div>

                    </div>
                </div>
            <?}?>

            <!--<div class="text-center per-price-con">
                <input type="checkbox" name="igree"> I agree with this <a href="#"> Payment Terms & Condition </a>
            </div>-->
            </form>
        </div>
    </div>

</section>
<section class="how-to-gray">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 con-tab-details" style="font-size: 19px;padding: 40px 0;
  text-align: center;">
                <b>  Note:</b> Billing cycle is month to month.It starts from 1st of each month, if user joins
                <br>e.g. on 15th, then user will be only charged for 15 balance days as a pro rata for first month.
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Add On</h2>
            </div>
        </div>
        <div class="row con-retailer-tab">
            <table width="100%">
                <tr>
                    <td style="width: 60%;">
                        <input type="hidden" value="no" id="qr" name="qr">
                        <div class="col-md-10 con-tab-details">
                            2 Custom QR Code Sticker Per Location
                        </div>
                        <div class="col-md-1">
                            <a href='#' class="tooltips"> <img class="text-right" src="assets/images/add-on.png" alt=""><span>You can download & print free QR code from your account or order <br />the UV proof custom QR code from here.<br/></span> 	</a>

                        </div>
                    </td>

                    <td style="width: 20%;">
                        <div class="text-center con-tab-det-sec"><span> $8</span>/Location</div>
                    </td>
                    <td>
                        <div class="text-center"> <a id="addonlink" class="con-tab-details-btn 1" onclick="chngclr();"> Select </a></div>
                    </td>
                </tr>

                <tr>
                    <td style="width: 60%;">
                        <div class="col-md-10 con-tab-details">
                            Add Additional Hierarchy (e.g. managers/management by geographic preferences)
                        </div>
                        <div class="col-md-1">
                            <a href='#' class="tooltips"> <img class="text-right" src="assets/images/add-on.png" alt=""><span>Give admin rights to your employees by territory<br />e.g. Only those manager/employee can see the data for the defined territory.<br /></span> 	</a>
                        </div>
                    </td>

                    <td style="width: 20%;">
                        <div class="text-center con-tab-det-sec">Contact us for pricing & process</div>
                    </td>
                    <td>
                        <div class="text-center"> <a href="index.php#contact" class="con-tab-details-btn"> Contact Us </a></div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 60%;">
                        <div class="col-md-10 con-tab-details">
                            Add active account/employee to  a new/existing account/group
                        </div>
                        <div class="col-md-1">
                            <a href='#' class="tooltips"> <img class="text-right" src="assets/images/add-on.png" alt=""><span>You can add new employees to existing accounts<br />e.g. If a user/account signed up independently & now wants to integrate the data <br />with another user,contact us.<br /></span> 	</a>
                        </div>
                    </td>

                    <td>
                        <div class="text-center con-tab-det-sec">Contact us for pricing & process</div>
                    </td>
                    <td>
                        <div class="text-center"> <a href="index.php#contact" class="con-tab-details-btn"> Contact Us </a></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</section>


<!--<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center con-retailer">
				<div class="con-retailer-conte"> Coming Soon for Retailer's/Locations <br> Get feedback from consumers directly! </div>
				<input type="submit" value="select">
				<p>You will be contacted when its live Free for Beta test accounts</p>
			</div>
		</div>
		<div class="row text-right">
			<a class="con-retailer-btn" href="#"> Next </a>
		</div>
	</div>
</section>-->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center con-retailer">
                <div class="con-retailer-next"> <h2 style="color:#FF0000;">
                        <div class="pulse animated infinite" > Coming Soon </div> </h2>
                </div>
                <div class="con-retailer-next"> Get feedback from Consumers directly. <br> Click "Contact Sales" if you are interested in <b>Free Beta Testing.</b></div>
                <a href="mailto:sales@servicecalibrate.com" ><input type="button" value="Contact sale"></a>
                <p>You will be contacted when its live</p>
            </div>
        </div>
        <div class="row text-right">
            <button class="con-retailer-btn" onclick="submitform()"> Next </button>
        </div>
    </div>
</section>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $(".ser_title").click(function(){
            $(this).children("p").slideToggle();
            $(this).children("a").toggleClass("ser_title_red");
        });
    });

    //Tooltrip
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<link rel="stylesheet" href="<?php echo $site_url; ?>assets/css/alertify.core.css"/>
<link rel="stylesheet" href="<?php echo $site_url; ?>assets/css/alertify.default.css" id="toggleCSS"/>

<script type="text/javascript" src="<?= $site_url; ?>assets/js/alertify.min.js"></script>
<script>

function checkpromo()
{

    var qr=document.getElementById("qr").value;
    var price=document.getElementById("aprice").value;

//    if(price<=0)
//    {
//        document.getElementById('aprice').value="1";
//        price=document.getElementById("aprice").value;
//
//    }
    var planprice=document.getElementById("aplanprice").value;
    var code=document.getElementById("promocode").value;

    var ajax_url    =   '<?php echo $ajax_url; ?>';
    $.ajax({
        url:ajax_url+"ajax-promo.php",
        type:'POST',
        data:{code:code,count:price,planprice:planprice,plantype:'annual',qr:qr},
        success: function (result) {
            var data=JSON.parse(result);
            if(data.OUTPUT =="0.00" )
            {
                document.getElementById("aprice").value = data.count;
                document.getElementById('atotal').value=data.OUTPUT ;
            }
            else
            {
                document.getElementById("aprice").value = data.count;
                document.getElementById('atotal').value=data.OUTPUT ;
            }
            //document.getElementById('atotal').value=data.OUTPUT ;
            document.getElementById('aspan').Text=data.MSG ;
            $("#aspan").text(data.MSG);
            $("#aspan").css("color",data.color);
            $("#aspan").css("font-size","17px");

        }
    });



}
function checkpromomnth()
{

    var qr=document.getElementById("qr").value;

    var price=document.getElementById("mnprice").value;
//    if(price<=0)
//    {
//        document.getElementById('mnprice').value="1";
//        var price=document.getElementById("mnprice").value;
//
//    }
    var planprice=document.getElementById("mplanprice").value;
    var code=document.getElementById("mpromocode").value;

    var ajax_url    =   '<?php echo $ajax_url; ?>';
    $.ajax({
        url:ajax_url+"ajax-promo.php",
        type:'POST',
        data:{code:code,count:price,planprice:planprice,plantype:'month',qr:qr},
        success: function (result) {
            var data=JSON.parse(result);

            if(data.OUTPUT =="0.00" )
            {

                document.getElementById("mnprice").value = data.count;
                document.getElementById('mtotal').value=data.OUTPUT ;
            }
            else
            {
                document.getElementById("mnprice").value = data.count;
                document.getElementById('mtotal').value=data.OUTPUT ;
            }


            document.getElementById('mspan').Text=data.MSG ;
            $("#mspan").text(data.MSG);
            $("#mspan").css("color",data.color);
            $("#mspan").css("font-size","17px");

        }


    });


}
function getprice1()
{

    var price=document.getElementById("freeprice").value;
    if(price<=0)
    {
        document.getElementById('freeprice').value="1" ;
    }

}

function getprice()
{
    var price=document.getElementById("aprice").value;
    var planprice=document.getElementById("aplanprice").value;
    var total=price * planprice *12;
    // var finalprice=parseFloat(Number((total).toFixed(2)));
    var finalprice=(total).toFixed(2);
    document.getElementById('atotal').value=finalprice ;
}

function getmprice()
{
    var mprice=document.getElementById("mnprice").value;
    var mplanprice=document.getElementById("mplanprice").value;
    var day=document.getElementById("days").value;
    var total=mprice * day * (mplanprice/30);
    //var finalprice=parseFloat(Number((total).toFixed(2)));
    var finalprice=(total).toFixed(2);

    document.getElementById('mtotal').value=finalprice ;

    var className = $('#addonlink').attr('class');
    if(className=="con-tab-details-btn 2")
    {
        var mprice=parseInt(document.getElementById("mnprice").value);
        var adtotal=parseInt(document.getElementById("mtotal").value);
        var total=adtotal + (8 * mprice) ;
        var finalprice=(total).toFixed(2);

        document.getElementById('mtotal').value=finalprice ;
    }
}

function selectannual()
{
    document.getElementById("formname").value="frmanual";
    $("#annualbutton").removeClass("free-price");
    $("#annualbutton").addClass("prices-selected");
    $("#mnthbutton").addClass("free-price");
    $("#mnthbutton").removeClass("prices-selected");
    $("#freebutton").addClass("free-price");
    $("#freebutton").removeClass("prices-selected");

}

function selectmonth()
{
    document.getElementById("formname").value="frmmnth";
    $("#mnthbutton").removeClass("free-price");
    $("#mnthbutton").addClass("prices-selected");
    $("#freebutton").addClass("free-price");
    $("#freebutton").removeClass("prices-selected");
    $("#annualbutton").addClass("free-price");
    $("#annualbutton").removeClass("prices-selected");

}

function selectfree()
{

    document.getElementById("formname").value="frmfree";
    $("#freebutton").removeClass("free-price");
    $("#freebutton").addClass("prices-selected");
    $("#annualbutton").addClass("free-price");
    $("#annualbutton").removeClass("prices-selected");
    $("#mnthbutton").addClass("free-price");
    $("#mnthbutton").removeClass("prices-selected");
}

function chngclr()
{

    var className = $('#addonlink').attr('class');

    if(className=="con-tab-details-btn 1")
    {
        $("#addonlink").removeClass("con-tab-details-btn 1");
        $("#addonlink").addClass("con-tab-details-btn 2");
        $("#addonlink").css("background-color", "#373737 ");


        var mprice=parseInt(document.getElementById("mnprice").value);
        var adtotal=parseInt(document.getElementById("mtotal").value);
        var total=adtotal + (8 * mprice) ;
        var finalprice=Number((total).toFixed(2));
        document.getElementById('qr').value="yes";



        var mprice1=parseInt(document.getElementById("aprice").value);
        var adtotal1=parseInt(document.getElementById("atotal").value);
        var total1=adtotal1 + (8 * mprice1) ;
        var finalprice1=Number((total1).toFixed(2));


        document.getElementById('atotal').value=finalprice1 ;
        document.getElementById('mtotal').value=finalprice ;

    }
    else  if(className=="con-tab-details-btn 2")
    {
        $("#addonlink").removeClass("con-tab-details-btn 2");
        $("#addonlink").addClass("con-tab-details-btn 1");
        $("#addonlink").css("background-color", "#ff0000");

        var mprice=parseInt(document.getElementById("mnprice").value);
        var adtotal=parseInt(document.getElementById("mtotal").value);
        var total=adtotal - (8 * mprice) ;
        var finalprice=Number((total).toFixed(2));
        document.getElementById('mtotal').value=finalprice ;

        var mprice1=parseInt(document.getElementById("aprice").value);
        var adtotal1=parseInt(document.getElementById("atotal").value);
        var total1=adtotal1 - (8 * mprice1) ;
        var finalprice1=Number((total1).toFixed(2));


        document.getElementById('atotal').value=finalprice1 ;
        document.getElementById('qr').value="no";

    }
}
function submitform()
{

    var form=document.getElementById("formname").value;
    if(form=="frmfree")
    {
        var fprice=document.getElementById("freeprice").value;
        if(fprice>0)
        {
            document.getElementById(form).submit();
        }
        else
        {
            alertify.error("Please Enter Location")
        }
    }
    else if(form=="frmanual")
    {
        var fprice=document.getElementById("aprice").value;
        if(fprice>0)
        {
            document.getElementById(form).submit();
        }
        else
        {
            alertify.error("Please Enter Location");
        }
    }
    else
    {
        var fprice=document.getElementById("mnprice").value;
        if(fprice>0)
        {
            document.getElementById(form).submit();
        }
        else
        {
            alertify.error("Please Enter Location")
        }
    }


}
</script>


<?php

unset($_SESSION['frnt_payment_msg']);
include("footer.php"); ?>
