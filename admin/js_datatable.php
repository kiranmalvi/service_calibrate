<!--Core js-->
<script src="<?php echo $admin_url; ?>assets/js/jquery.js"></script>
<script src="<?php echo $admin_url; ?>assets/bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="<?php echo $admin_url; ?>assets/js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="<?php echo $admin_url; ?>assets/js/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<!--<script src="<?php /*echo $admin_url; */ ?>assets/js/flot-chart/jquery.flot.js"></script>
<script src="<?php /*echo $admin_url; */ ?>assets/js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="<?php /*echo $admin_url; */ ?>assets/js/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php /*echo $admin_url; */ ?>assets/js/flot-chart/jquery.flot.pie.resize.js"></script>-->

<!--dynamic table-->
<script type="text/javascript" language="javascript"
        src="<?php echo $admin_url; ?>assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo $admin_url; ?>assets/js/data-tables/DT_bootstrap.js"></script>
<!--common script init for all pages-->
<script src="<?php echo $admin_url; ?>assets/js/scripts.js"></script>

<!--dynamic table initialization -->
<script src="<?php echo $admin_url; ?>assets/js/dynamic_table_init.js"></script>

<!--script for this page-->
<script type="text/javascript" src="<?php echo $admin_url; ?>assets/js/gritter/js/jquery.gritter.js"></script>
