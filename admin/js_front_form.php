<!--Core js-->
<script src="<?php echo $admin_url; ?>assets/js/jquery.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo $admin_url; ?>assets/bs3/js/bootstrap.min.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script class="include" type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/easypiechart/jquery.easypiechart.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/jquery.nicescroll.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/bootstrap-switch.js"></script>

<script type="text/javascript" src="<?php echo $admin_url; ?>assets/js/fuelux/js/spinner.min.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/jquery-multi-select/js/jquery.quicksearch.js"></script>

<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/jquery-tags-input/jquery.tagsinput.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/select2/select2.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/select-init.js"></script>


<!--common script init for all pages-->
<script src="<?php echo $admin_url; ?>assets/js/scripts.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/toggle-init.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/advanced-form.js"></script>
<!--Easy Pie Chart-->
<script src="<?php echo $admin_url; ?>assets/js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="<?php echo $admin_url; ?>assets/js/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<!--
<script src="<?php /*echo $admin_url; */ ?>assets/js/flot-chart/jquery.flot.js"></script>
<script src="<?php /*echo $admin_url; */ ?>assets/js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="<?php /*echo $admin_url; */ ?>assets/js/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php /*echo $admin_url; */ ?>assets/js/flot-chart/jquery.flot.pie.resize.js"></script>
-->

<!--script for this page-->
<script type="text/javascript" src="<?php echo $admin_url; ?>assets/js/gritter/js/jquery.gritter.js"></script>



