<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 28/3/15
 * Time: 1:40 PM
 */

include_once($inc_class_path . 'admin.class.php');

$admin = new admin();
$MODE = $_REQUEST['mode'];

/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $admin->setvFirstName($_REQUEST['vFirstName']);
    $admin->setvLastName($_REQUEST['vLastName']);
    $admin->setvEmail($_REQUEST['vEmail']);
    $admin->setvPasswd(base64_encode($_REQUEST['vPasswd']));
    $admin->setdtAdded($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $admin->setdtUpdated('0');
    $admin->setdtLastLogin('0');
    $admin->setvLastLoginIP('');
    $admin->seteStatus($_REQUEST['eStatus']);
    $admin->seteType($_REQUEST['eType']);

    $admin->insert();
    $generalfuncobj->func_set_temp_sess_msg("Admin Details Added Successfully");
    header("Location:index.php?file=sc-adminlist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $admin->setvFirstName($_REQUEST['vFirstName']);
    $admin->setvLastName($_REQUEST['vLastName']);
    $admin->setvEmail($_REQUEST['vEmail']);
    $admin->setvPasswd(base64_encode($_REQUEST['vPasswd']));
    $admin->setdtUpdated($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $admin->seteStatus($_REQUEST['eStatus']);
    $admin->seteType($_REQUEST['eType']);

    $admin->update($_REQUEST['iAdminId']);

    $_SESSION['SC_LOGIN']['vFirstName'] = $_REQUEST['vFirstName'];
    $_SESSION['SC_LOGIN']['vLastName'] = $_REQUEST['vLastName'];

    $generalfuncobj->func_set_temp_sess_msg("Admin Details <b>Update</b> Successfully");

    if($_REQUEST['page']=="update")
    {
        header("Location:index.php?file=sc-adminlist");
    }
    else
    {
        header("Location:index.php");
    }

    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $admin->delete($ID);

        $generalfuncobj->func_set_temp_sess_msg("Admin Details Deleted Successfully");
        header("Location:index.php?file=sc-adminlist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $admin->delete($ID);
        }

        $generalfuncobj->func_set_temp_sess_msg("Admin Details Deleted Successfully");
        header("Location:index.php?file=sc-adminlist");
        exit;
    }
}
if ($MODE == 'changepwd')
{
    pr($_REQUEST);
    $id=$_SESSION['SC_LOGIN']['ADMIN']['iAdminId'];
    $old=$_REQUEST['oPasswd'];
    $password=base64_encode($_REQUEST['nPasswd']);
    $check=$admin->chk_pass($id,$old);
    $date=strtotime(date('d-m-Y H:i:s'));
    if($check)
    {
        $update_sql="UPDATE admin SET vPasswd='$password' ,dtUpdated=$date where iAdminId=$id";
        $obj->sql_query($update_sql);
        $_SESSION['admin_pwd_err']="Password Changed successfully";
        $_SESSION['admin_pwd_clas']="alert-success";
        header("Location:index.php?file=sc-changepassword");
        exit;
    }
    else
    {
        $_SESSION['admin_pwd_err']="Please Enter Correct Password";
        $_SESSION['admin_pwd_clas']="alert-danger";
        header("Location:index.php?file=sc-changepassword");
        exit;
    }

}

