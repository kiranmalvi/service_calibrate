<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 28/3/15
 * Time: 1:40 PM
 */



include_once($inc_class_path . 'promotional_code.class.php');
$promotional_code = new promotional_code();
$MODE = $_REQUEST['mode'];

$todaydate = strtotime(date('Y-m-d H:i:s'));

/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $promotional_code->setvPromoPrice($_REQUEST['vPromoPrice']);
    $promotional_code->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
    $promotional_code->setiDtUpdated(strtotime(date('Y-m-d H:i:s')));
    $promotional_code->seteStatus($_REQUEST['eStatus']);
    $promotional_code->setvPromotinalName($_REQUEST['vPromoname']);
    $promotional_code->setvPromotionalCode($_REQUEST['vPromocode']);

    $expiredate=strtotime('d-m-Y',$_REQUEST['iEndDate']);
    $originalDateend = $_REQUEST['iEndDate'];
    $dend = explode("-",$originalDateend);
    $d2 = $dend[2]."-".$dend[0]."-".$dend[1];
    $edate = (date('Y-m-d',strtotime($d2)));
    $promotional_code->setiExpireDate(strtotime($edate));

    $promotional_code->insert();
    $generalfuncobj->func_set_temp_sess_msg("Promotinal Details Added Successfully");
    header("Location:index.php?file=pc-promotionalcodelist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $promotional_code->setvPromoPrice($_REQUEST['vPromoPrice']);
    $promotional_code->setiDtUpdated(strtotime(date('Y-m-d H:i:s')));
    $promotional_code->seteStatus($_REQUEST['eStatus']);
    $promotional_code->setvPromotinalName($_REQUEST['vPromoname']);
    $promotional_code->setvPromotionalCode($_REQUEST['vPromocode']);
    $expiredate=strtotime('d-m-Y',$_REQUEST['iEndDate']);
    $originalDateend = $_REQUEST['iEndDate'];
    $dend = explode("-",$originalDateend);
    $d2 = $dend[2]."-".$dend[0]."-".$dend[1];
    $edate = (date('Y-m-d',strtotime($d2)));
    $promotional_code->setiExpireDate(strtotime($edate));
    $promotional_code->update($_REQUEST['iId']);

    $generalfuncobj->func_set_temp_sess_msg("Promotinal Details Update Successfully");
    header("Location:index.php?file=pc-promotionalcodelist");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $sql = " UPDATE promotional_code SET eStatus = '2' , iDtUpdated = '$todaydate' WHERE iPromotionalcodeId = '$ID'";
        $obj->sql_query($sql);

        $generalfuncobj->func_set_temp_sess_msg("Promotinal Details Deleted Successfully");
        header("Location:index.php?file=pc-promotionalcodelist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $sql = " UPDATE promotional_code SET eStatus = '2' , iDtUpdated = '$todaydate' WHERE iPromotionalcodeId = '$ID'";
            $obj->sql_query($sql);
        }

        $generalfuncobj->func_set_temp_sess_msg("Promotinal Details Deleted Successfully");
        header("Location:index.php?file=pc-promotionalcodelist");
        exit;
    }
}

