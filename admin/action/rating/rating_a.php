<?php
/**
 * Created by PhpStorm.
 * User: Hardik
 * Date: 2/6/15
 * Time: 5:25 PM
 */
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];

include_once($inc_class_path . 'user.class.php');
$MODE = $_REQUEST['mode'];
$userobj = new user();
//$db_res = $userobj->select_employee($iUserId);
include_once($inc_class_path . 'rating.class.php');
$ratingObj = new rating();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();
$today = strtotime(gmdate('Y-m-d H:i:s'));
/*$lastdate_rate=$ratingObj->get_last_rating($iUserId);*/
$lastdate = strtotime(date('Y-m-1 H:i:s'));

if ($MODE == 'get_employee') {
    // echo "test"; exit;
    ob_get_clean();
    #pr($_REQUEST);exit;
    $iUserid = $_REQUEST['user_id'];
    $selected = $_REQUEST['selected'];
    //$state->setiCountryId($iCountryId);
    $userobj->setiUserId($iUserId);
    $DATA = $userobj->select_employee($iUserid);


    #echo $CALL;exit;
    $HTML = '<select class="form-control" name="manufacture" id="manufacture">';
    if (count($DATA) > 0) {
        $HTML .= '<option selected disabled>- Select Employee -</option>';
        foreach ($DATA as $employee) {
            $SELECTED = ($selected == $employee['iEmployeeId']) ? 'selected' : '';
            $HTML .= '<option value="' . $employee['iEmployeeId'] . '" ' . $SELECTED . '>' . $employee['vFirstName'] . '</option>';
        }
    } else {
        $HTML .= '<option disabled> No Data Found. </option>';
    }
    $HTML .= '</select>';
    echo $HTML;
    exit;
}
if ($MODE == 'add') {


    if ($userObj->func_check_existing_userid($iUserId)) {
        if ($ratingObj->check_last_rating($iUserId, $_REQUEST['manufacture'], $today, $lastdate) === true) {

            $ratingObj->setiDistributorId($_REQUEST['distributor']);
            $ratingObj->setiEmployeeId($_REQUEST['manufacture']);
            $ratingObj->setiUserId($_REQUEST['uid']);
            $ratingObj->setvDescription($_REQUEST['feedback']);
            $ratingObj->setvBrand($_REQUEST['brand']);
            $ratingObj->seteRating($_REQUEST['like_dislike']);
            $adddate= date('m-d-Y H:i:s A');
            $ratingObj->setvTitle($adddate);

            $ratingObj->seteFlag($_REQUEST['eFlag']);
            //echo "trtfsfdasf"; exit;
            $iDtAdded = $ratingObj->setiDtAdded(strtotime(gmdate('Y-m-d H:i:s')));
            if (isset($_FILES['vImage'])) {
                $photopath = $rate_image_path;
                $vphoto = $_FILES["vImage"]["tmp_name"];
                $vphoto_name = $_FILES["vImage"]["name"];
                $vphoto_type = $_FILES["vImage"]["type"];

                $prefix = date("YmdHis");
                $image_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);
                $image_name = $image_arr[0];

                if ($image_name != '') {
                    ## SET IMAGE
                    $ratingObj->setvImage($image_name);
                }
            }

            $iRatingId = $ratingObj->insert();

            if ($referralObj->check_existing_user($iUserId)) {
                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->update_points($iUserId, "100");


            } else {
                $referralObj->setiUserId($iUserId);
                $referralObj->setvPoint("100");
                $referralObj->seteStatus("1");
                $referralObj->setiDtAdded($iDtAdded);
                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->setvReason("Rating");
                $iReffaralId = $referralObj->insert();

            }


            if ($iRatingId > 0) {
                $points = $referralObj->get_total_point($iUserId);
                $data = $points;

                $generalfuncobj->func_set_temp_sess_msg("Rating Give Successfully");
                header("Location:index.php?file=r-rating");
                exit;
            } else {
                $status = "0";
                $msg = "Rating is not Given,Try Again";
                $generalfuncobj->func_set_temp_sess_msg("Rating is not Given,Try Again");

            }

        }

        $_SESSION['rate_err'] = "You  are allowed to rate once a month to each employee.Please try in 30 days to change your rating.";
        // $generalfuncobj->func_set_temp_sess_msg("You  are allowed to rate once a month to each employee.Please try in 30 days to change your rating.");
        header("Location:index.php?file=r-addreting");
        exit;
    }
}
?>