<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 28/3/15
 * Time: 1:40 PM
 */
pr($_REQUEST);
include_once($inc_class_path . 'promotion_right.class.php');

$promotion_right= new promotion_right();
$MODE = $_REQUEST['mode'];
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $promotion_right->setiPromoadminId($iUserId);
    $promotion_right->setiPromoIUserId($_REQUEST['employee']);
    $promotion_right->setiCityId($_REQUEST['city']);
    $promotion_right->setiDtAdded(strtotime(gmdate('Y-m-d H:i:s')));
    $promotion_right->setiDtUpdated(strtotime(gmdate('Y-m-d H:i:s')));
    $promotion_right->seteStatus($_REQUEST['eStatus']);

    $promotion_right->insert();
    $generalfuncobj->func_set_temp_sess_msg("Promotion Right Details Added Successfully");
    header("Location:index.php?file=p-promouserlist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {


    $promotion_right->setiPromoIUserId($_REQUEST['employee']);
    $promotion_right->setiCityId($_REQUEST['city']);
    $promotion_right->setiDtUpdated(strtotime(gmdate('Y-m-d H:i:s')));
    $promotion_right->seteStatus($_REQUEST['eStatus']);

    $promotion_right->update_right($_REQUEST['iId']);

    $generalfuncobj->func_set_temp_sess_msg("Promotion Right Details Updated Successfully");
    header("Location:index.php?file=p-promouserlist");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $date=strtotime(date('d-m-Y H:i:s'));
        $exe_del = "Update promotion_right set eStatus='2',iDtAdded='$date' where iPromorightId in ($ID)";
        $obj->sql_query($exe_del);

        $generalfuncobj->func_set_temp_sess_msg("Promotion Right Details Deleted Successfully");
        header("Location:index.php?file=p-promouserlist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $exe_del = "Update promotion_right set eStatus='2',iDtAdded='$date' where iPromorightId in ($ID)";
            $obj->sql_query($exe_del);
        }

        $generalfuncobj->func_set_temp_sess_msg("Promotion Right Details Deleted Successfully");
        header("Location:index.php?file=p-promouserlist");
        exit;
    }
}

