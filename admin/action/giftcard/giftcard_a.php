<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 28/3/15
 * Time: 1:40 PM
 */


include_once($inc_class_path . 'gift_card.class.php');
$gift_card_Obj = new gift_card();
$MODE = $_REQUEST['mode'];

/* Add Mode */
if ($MODE == 'add') {
    //pr($_REQUEST);exit;

    $gift_card_Obj->setvGift_CardName($_REQUEST['vGift_CardName']);
    $gift_card_Obj->setvPoints($_REQUEST['vPoints']);
    $gift_card_Obj->setvDescription($_REQUEST['vDescription']);
    $gift_card_Obj->setidtAdded(strtotime(date('Y-m-d H:i:s')));
    $gift_card_Obj->setidtupdate(strtotime(date('Y-m-d H:i:s')));
    $gift_card_Obj->seteStatus($_REQUEST['eStatus']);


    $gift_card_Obj->insert();
    $generalfuncobj->func_set_temp_sess_msg("Gift Card Details Added Successfully");
    header("Location:index.php?file=gc-giftcardlist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $gift_card_Obj->setvGift_CardName($_REQUEST['vGift_CardName']);
    $gift_card_Obj->setvPoints($_REQUEST['vPoints']);
    $gift_card_Obj->setvDescription($_REQUEST['vDescription']);
    $gift_card_Obj->setidtAdded(strtotime(gmdate('Y-m-d H:i:s')));
    $gift_card_Obj->setidtupdate(strtotime(gmdate('Y-m-d H:i:s')));
    $gift_card_Obj->seteStatus($_REQUEST['eStatus']);

    $gift_card_Obj->update($_REQUEST['iCardId']);

    /*$_SESSION['SC_LOGIN']['vFirstName'] = $_REQUEST['vFirstName'];
    $_SESSION['SC_LOGIN']['vLastName'] = $_REQUEST['vLastName'];*/

    $generalfuncobj->func_set_temp_sess_msg(" Gift Card <b>Update</b> Successfully");
    header("Location:index.php?file=gc-giftcardlist");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $exe_del = "Update gift_card set eStatus='2' where iCardId in ($ID)";
        $obj->sql_query($exe_del);
        $generalfuncobj->func_set_temp_sess_msg("Gift Card Details Deleted Successfully");
        header("Location:index.php?file=gc-giftcardlist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $exe_del = "Update gift_card set eStatus='2' where iCardId in ($ID)";
            $obj->sql_query($exe_del);
        }

        $generalfuncobj->func_set_temp_sess_msg("Gift Card Details Deleted Successfully");
        header("Location:index.php?file=gc-giftcardlist");
        exit;
    }
}

