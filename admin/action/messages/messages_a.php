<?
include_once($inc_class_path . 'api.class.php');
$apiObj = new API();

    include_once ($inc_class_path . 'user.class.php');
    $userObj = new user();

    include_once ($inc_class_path . 'message_conversation.class.php');
    $conversationObj = new message_conversation();
    include_once ($inc_class_path . 'message.class.php');
    $messageObj = new message();

    include_once($inc_class_path . 'user_settings.class.php');
    $user_settingsObj = new user_settings();


    $iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
    $iUserAddedId = $_POST['iEmployeeId'];
    $eStatus = "1";
    $iDtAdded = $start = strtotime (gmdate ('Y-m-d H:i:s'));
    $data = array();
    //echo $iUserId." ".$iUserAddedId;
    $MODE = $_REQUEST['mode'];

    $frndid = $_REQUEST['frndid'];

    $vMessage = $_REQUEST['vMessage'];

    if ($MODE == 'get_emp') {
        ob_get_clean ();
        #pr($_REQUEST);exit;
        $iDistributorId = $_REQUEST['distributor_id'];
        $selected = $_REQUEST['selected'];
        $DATA = $userObj->select_employee ($iDistributorId);

        $HTML = '<select class="form-control" name="iEmployeeId" id="iEmployeeId">';
        if (count ($DATA) > 0) {
            $HTML .= '<option selected disabled>- Select Employee -</option>';
            foreach ($DATA as $emp) {
                $SELECTED = ($selected == $emp['iEmployeeId']) ? 'selected' : '';
                $HTML .= '<option value="' . $emp['iEmployeeId'] . '" ' . $SELECTED . '>' . $emp['vFirstName'] . " " . $emp ['vLastName'] . '</option>';
            }
        } else {
            $HTML .= '<option disabled> No Data Found. </option>';
        }
        $HTML .= '</select>';
        echo $HTML;
        exit;
    }
    if ($MODE == 'get_search') {
        ob_get_clean ();

        $Searchuser = $_REQUEST['search_id'];

        $userlist = "SELECT u.vFirstName,u.vLastName,u.vImage,m.* FROM message_conversation as m left join user u on m.iUseraddedId=u.iUserId WHERE m.iUserId = $iUserId AND m.eStatus='1' and u.vFirstName like '%$Searchuser%' order by m.iDtupdated desc";
        $db_res_ser = $obj->select ($userlist);

        $html .= ' <ul class="conversation-list"  id="search_li">';
        if (count ($db_res_ser) > 0) {
            for ($j = 0; $j < count ($db_res_ser); $j++) {

                $selected = 'class="clearfix odd"';
                if ($frndid == $db_res_ser[$j]['iUseraddedId']) {
                    $selected = 'class="clearfix"';

                }

                ?>

                <li <?php echo $selected; ?>
                    onclick="location.href='index.php?file=m-messages&frndid=<?php echo $db_res_ser[$j]['iUseraddedId']; ?>'"
                    style="cursor: pointer" id="search_li">

                    <div class="chat-avatar">
                        <?php if ( $db_res_ser[$j]['vImage'] == '' || (!is_file ($user_image_path .  $db_res_ser[$j]['vImage']))) {
                            $image = $dafault_image_logo_user;
                        } else {
                            $image = $user_image_url .  $db_res_ser[$j]['vImage'];
                        } ?>
                        <img src="<?php echo $image; ?>" alt=""
                             style="width: 45px;height: 45px">

                    </div>
                    <div class="conversation-text">
                        <div class="ctext-wrap">
                            <i><?php echo $db_res_ser[$j]['vFirstName'] . " " . $db_res_ser[$j]['vLastName']; ?></i>

                        </div>
                    </div>
                </li>

            <?php
            }
        } else {
            echo "No Search Result Found";

        }
        $html .= "</ul>";
        echo $html;

    }

    if ($MODE == 'send_msg_ajax') {
        ob_get_clean ();
        #pr($_REQUEST);exit;
        $SNR = $_REQUEST['sender'];
        $REC = $_REQUEST['receiver'];
        $MSG = $_REQUEST['msg'];

        $check_exist = $conversationObj->check_user_added ($REC, $SNR);
        // count ($check_exist);


        if (count ($check_exist) > 0) {
            $iConversationId = $check_exist[0]['iConversationId'];
            $conversationObj->seteStatus ("1");
            $conversationObj->setiDtUpdated ($iDtAdded);
            $conversationObj->update_status ($iConversationId);


        } else {
            $conversationObj->setiUserId ($REC);
            $conversationObj->setiUseraddedId ($SNR);
            $conversationObj->setiDtAdded ($iDtAdded);
            $conversationObj->seteStatus ($eStatus);
            $conversationObj->setiDtUpdated ($iDtAdded);
            $iConversationId = $conversationObj->insert ();


        }
        if ($iConversationId > 0) {

            $messageObj->setiSenderId($SNR);
            $messageObj->setiReceiverId($REC);
            $messageObj->setvMessage($MSG);
            $messageObj->seteStatus("1");
            $messageObj->setiDateAdded($iDtAdded);
            $messageObj->setiDtUpdated($iDtAdded);
            $messageObj->seteMessage_Type("m");
            $messageObj->seteRead("0");

            $iMessageId = $messageObj->insert();
            $setting = $user_settingsObj->select_user_setting($REC);

            if ($setting[0]['eNotification'] == "1") {
                $senderdata = $userObj->select($iUserId);
                $message1 = "New Message from " . $senderdata[0]['vFirstName'];
                $apiObj->notify($REC, $message1, 'M', $SNR);
            }

            echo date('H:i:s', $iDtAdded);
            exit;
        }
    }

    if ($MODE == 'add') {

        // print_r($_REQUEST);


        $check_exist = $conversationObj->check_user_added ($iUserId, $iUserAddedId);
        count ($check_exist);


        if (count ($check_exist) > 0) {
            $iConversationId = $check_exist[0]['iConversationId'];
            $conversationObj->seteStatus ("1");
            $conversationObj->setiDtUpdated ($iDtAdded);
            $conversationObj->update_status ($iConversationId);

            $generalfuncobj->func_set_temp_sess_msg ("Messages Details Update Successfully");
            header ("Location:index.php?file=m-messages");
            exit;
        } else {
            $conversationObj->setiUserId ($iUserId);
            $conversationObj->setiUseraddedId ($iUserAddedId);

            $conversationObj->setiDtAdded ($iDtAdded);
            $conversationObj->seteStatus ($eStatus);
            $conversationObj->setiDtUpdated ($iDtAdded);
            $iConversationId = $conversationObj->insert ();


            $generalfuncobj->func_set_temp_sess_msg ("Messages Details Added Successfully");
            header ("Location:index.php?file=m-messages");
            exit;
        }


    }
    if ($MODE == 'message_send') {
        //pr ($_REQUEST);
         $check_exist = $conversationObj->check_user_added ($frndid, $iUserId);
       // count ($check_exist);


        if (count ($check_exist) > 0) {
            $iConversationId = $check_exist[0]['iConversationId'];
            $conversationObj->seteStatus ("1");
            $conversationObj->setiDtUpdated ($iDtAdded);
            $conversationObj->update_status ($iConversationId);


        } else {
            $conversationObj->setiUserId ($frndid);
            $conversationObj->setiUseraddedId ($iUserId);
            $conversationObj->setiDtAdded ($iDtAdded);
            $conversationObj->seteStatus ($eStatus);
            $conversationObj->setiDtUpdated ($iDtAdded);
            $iConversationId = $conversationObj->insert ();


        }
        if ($iConversationId > 0) {
            $messageObj->setiSenderId ($iUserId);
            $messageObj->setiReceiverId ($frndid);
            $messageObj->setvMessage ($vMessage);
            $messageObj->seteStatus ("1");
            $messageObj->setiDateAdded ($iDtAdded);
            $messageObj->setiDtUpdated ($iDtAdded);
            $messageObj->seteMessage_Type ("m");
            $messageObj->seteRead ("0");

            $iMessageId = $messageObj->insert ();
            $setting = $user_settingsObj->select_user_setting($iUserId);

            if ($setting[0]['eNotification'] == "1") {
                $senderdata = $userObj->select($iUserId);
                $message1 = "New Message from " . $senderdata[0]['vFirstName'];
                $apiObj->notify($iEmployeeId, $message1, 'M', $iUserId);
            }


            $generalfuncobj->func_set_temp_sess_msg ("Messages Send Successfully");
            header ("Location:index.php?file=m-messages&frndid=$frndid");
            exit;

        }

    }
?>
<?php
    if ($MODE == 'get_time') {
        ob_get_clean ();
        #pr($_REQUEST);exit;
        #echo date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))-2);exit;
        $frndid = $_REQUEST['frid'];
        $timed = $_REQUEST['time_d'];
        // echo $timed;
        $uid = $_REQUEST['uid'];
        $dt = strtotime (date ('Y-m-d H:i:s')) - 5;
        $sql = "select m.*,u.vImage,u.vFirstName,u.vLastName from message as m left join user u on u.iUserId = m.iSenderId where m.iSenderId in ($frndid,$uid) AND m.iReceiverId in ($frndid,$uid) AND m.iDateAdded >= $dt ORDER BY m.iDateAdded ASC";
        $display_d = $obj->select ($sql);

        $html = '';
        $html .= $_SERVER['REQUEST_TIME'] . '&^&|';
        ?>
        <?php for ($j1 = 0; $j1 < count ($display_d); $j1++)
        {
            //  pr($display_d);
            $t = date ('d-m-Y');
            $d = date ('d-m-Y', $display_d[$j1]['iDateAdded']);
            if ($t == $d) {
                $time = date ('H:i:s', $display_d[$j1]['iDateAdded']);
            } else {
                $time = date ('m-d-Y', $display_d[$j1]['iDateAdded']);
            }
            if ($display_d[$j1]['vImage'] != '' && file_exists ($user_image_path . $display_d[$j1]['vImage'])) {
                $image = $user_image_url . $display_d[$j1]['vImage'];
            } else {
                $image = $dafault_image_logo_user;
            }
            if ($display_d[$j1]['iSenderId'] == $frndid) {
                $sender = $display_d[$j1]['iSenderId'];
                $time_d = time_elapsed_string (date ($display_d[$j1]['iDateAdded']));


                $html .= '<li class="clearfix">
                <div class="chat-avatar">
                    <img src="'. $image.'" alt="" style="width: 45px;height: 45px">

                    <i>'.$time.'
            </i>
                </div>';

                $html .= '<div class="conversation-text">';
                $clrcls = ''; if($display_d[$j1]['eMessage_Type'] == 'b') {$clrcls='style="color: #802A81;"';}else{$clrcls='';}


                   $html.=' <div class="ctext-wrap" '.$clrcls.'.>
                        <i>'.$display_d[$j1]['vFirstName'].' '.$display_d[$j1]['vLastName'].'</i>
                        <p>'.$display_d[$j1]['vMessage'].'</p>
                    </div>
                </div>
            </li>';
            }
            if ($display_d[$j1]['iReceiverId'] == $frndid) {
                //$time_d= time_elapsed_string(date( $display_d[$j1]['iDateAdded']));

                /*$html.='<li class="clearfix odd">
                     <div class="chat-avatar">
                         <img src="'. $user_image_url.$display_d[$j1]['vImage'].'" alt="" style="width: 45px;height: 45px">

                         <i>
                             '. $time_d.'</i>
                     </div>';
                     $html.= '<div class="conversation-text">
                         <div class="ctext-wrap">
                             <i>'.$display_d[$j1]['vFirstName'].' '.$display_d[$j1]['vLastName'].'</i>
                             <p>
                                 '.$display_d[$j1]['vMessage'].'
                             </p>
                         </div>
                     </div>
                 </li>';*/
            }
        }
        ob_get_clean ();
        echo $html;
        exit;

    }

if ($MODE == 'get_time_list') {
    ob_get_clean ();
    #pr($_REQUEST);exit;
    #echo date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))-2);exit;
    $timed = $_REQUEST['time_d'];
    // echo $timed;
    $uid = $_REQUEST['uid'];
    $frndid=$_REQUEST['frndid'];
    $dt = strtotime (date ('Y-m-d H:i:s')) - 5;
    $sql = "SELECT u.vFirstName,u.vLastName,u.vImage,m.* FROM message_conversation as m left join user u on m.iUseraddedId=u.iUserId WHERE m.iUserId = $uid AND m.eStatus='1' order by m.iDtupdated desc";


    $display_d = $obj->select ($sql);
    $html='';
    $html .= ' <ul class="conversation-list1"  id="search_li">';
    if (count ($display_d) > 0) {
        for ($j = 0; $j < count ($display_d); $j++) {

            $selected = 'class="clearfix odd"';
            if ($frndid == $display_d[$j]['iUseraddedId']) {
                $selected = 'class="clearfix"';

            }

            ?>

            <li <?php echo $selected; ?>
                onclick="location.href='index.php?file=m-messages&frndid=<?php echo $display_d[$j]['iUseraddedId']; ?>'"
                style="cursor: pointer" id="search_li">

                <div class="chat-avatar">
                    <?php if ( $display_d[$j]['vImage'] == '' || (!is_file ($user_image_path .  $display_d[$j]['vImage']))) {
                        $image = $dafault_image_logo_user;
                    } else {
                        $image = $user_image_url .  $display_d[$j]['vImage'];
                    } ?>
                    <img src="<?php echo $image; ?>" alt=""
                         style="width: 45px;height: 45px">

                </div>
                <div class="conversation-text">
                    <div class="ctext-wrap">
                        <i><?php echo $display_d[$j]['vFirstName'] . " " . $display_d[$j]['vLastName']; ?></i>

                    </div>
                </div>
            </li>

        <?php
        }
    }
    else
    {
        echo "No Conversation Found";
    }
    $html .= "</ul>";

    echo $html;
    exit;

}

    if ($MODE == 'get_notification') {
        $timed = $_REQUEST['time_d'];
        // echo $timed;
        $uid = $_REQUEST['uid'];
        $dt = strtotime(gmdate('Y-m-d H:i:s')) - 5;
        $sql = "select m.*,u.vImage,u.vFirstName,u.vLastName from message as m left join user u on u.iUserId = m.iSenderId where m.iReceiverId = $uid AND iDateAdded >= $dt AND iDateAdded < " . $_SERVER['REQUEST_TIME'] . " ORDER BY m.iDateAdded ASC";
        $display_d = $obj->select ($sql);
        $display_d[0]['time'] = $_SERVER['REQUEST_TIME'];
        ob_get_clean ();
        echo json_encode ($display_d[0]);
        exit;
    }
    
    else if($mode="get_unread")
    {
    ob_get_clean();

    $iId = $_REQUEST['uid'];
    $count = $messageObj->func_count_user_unread_message($iId);

    if ($count > 0 && $count<="99")
    {
        $count = $count;
    }
    else if($count>99)
    {
        $count = "99+";
    }
    else {
        $count = "";
    }
        $HTML="";


        if ($count > 0) {
            //$HTML .= "<span id='spanmsg'>Messsages <p class='message-count'> "  . $count . "  <span></span> </p>";
            $HTML .= " <span id='spanmsg'>Messages ("  . $count . ") </span>";
        } else {
            $HTML .= " <span id='spanmsg'>Messages </span>";
        }


        echo $HTML;
}

?>
<?php
    function time_elapsed_string ($ptime)
    {

        // $time1=explode(":",$ptime);
        //print_r($time1);
        $etime = time () - $ptime;
        $c = 0;
        if ($etime < 1) {
            return $c++ . 'seconds';
        }

        $a = array(365 * 24 * 60 * 60 => 'year', 30 * 24 * 60 * 60 => 'month', 24 * 60 * 60 => 'day', 60 * 60 => 'hour', 60 => 'minute', 1 => 'second');
        $a_plural = array('year' => 'years', 'month' => 'months', 'day' => 'days', 'hour' => 'hours', 'minute' => 'minutes', 'second' => 'seconds');

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round ($d);

                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }
?>