<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 6/4/15
 * Time: 12:38 PM
 */
include_once($inc_class_path . 'country.class.php');

$country = new country();
$MODE = $_REQUEST['mode'];

$LIST_LINK = 'index.php?file=ma-countrylist';
$MODULE = 'Country';

/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $country->setvName($_REQUEST['vName']);
    $country->setiDtAdded($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $country->seteStatus($_REQUEST['eStatus']);

    $country->insert();
    $generalfuncobj->func_set_temp_sess_msg($MODULE . " Added Successfully");
    header("Location:" . $LIST_LINK);
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $country->setvName($_REQUEST['vName']);
    $country->seteStatus($_REQUEST['eStatus']);

    $country->update($_REQUEST['iCountryId']);
    $generalfuncobj->func_set_temp_sess_msg($MODULE . " Updated Successfully");
    header("Location:" . $LIST_LINK);
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $country->delete($ID);
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $country->delete($ID);
        }
    }

    $generalfuncobj->func_set_temp_sess_msg($MODULE . " Deleted Successfully");
    header("Location:" . $LIST_LINK);
    exit;
}

if ($MODE == 'check_country') {
    $vName = $_REQUEST['vName'];
    $country->setvName($vName);
    $country->check_country();
}