<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 6/4/15
 * Time: 1:24 PM
 */
include_once($inc_class_path . 'state.class.php');

$state = new state();
$MODE = $_REQUEST['mode'];

$LIST_LINK = 'index.php?file=ma-statelist';
$MODULE = 'State';

/* Add Mode */
if ($MODE == 'add') {
    $state->setiCountryId($_REQUEST['iCountryId']);
    $state->setvName($_REQUEST['vName']);
    $state->setiDtAdded($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $state->seteStatus($_REQUEST['eStatus']);

    $state->insert();
    $generalfuncobj->func_set_temp_sess_msg($MODULE . " Added Successfully");
    header("Location:" . $LIST_LINK);
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $state->setiCountryId($_REQUEST['iCountryId']);
    $state->setvName($_REQUEST['vName']);
    $state->seteStatus($_REQUEST['eStatus']);

    $state->update($_REQUEST['iStateId']);
    $generalfuncobj->func_set_temp_sess_msg($MODULE . " Updated Successfully");
    header("Location:" . $LIST_LINK);
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $state->delete($ID);
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $state->delete($ID);
        }
    }

    $generalfuncobj->func_set_temp_sess_msg($MODULE . " Deleted Successfully");
    header("Location:" . $LIST_LINK);
    exit;
}

/* Get State From Country */
if ($MODE == 'get_state') {
    ob_get_clean();
    #pr($_REQUEST);exit;
    $iCountryId = $_REQUEST['country_id'];
    $selected = $_REQUEST['selected'];
    $state->setiCountryId($iCountryId);
    $DATA = $state->select_on_country();

    $CALL123 = ((isset($_REQUEST['Promotion'])) AND $_REQUEST['Promotion'] == 'yes') ? 'onchange="get_city(this.value)"' : "";
    #echo $CALL;exit;
    $HTML = '<select class="form-control" name="iStateId1" id="iStateId1" ' . $CALL123 . '>';
    if (count($DATA) > 0) {
        $HTML .= '<option selected disabled>- Select State -</option>';
        foreach ($DATA as $state) {
            $SELECTED = ($selected == $state['iStateId']) ? 'selected' : '';
            $HTML .= '<option value="' . $state['iStateId'] . '" ' . $SELECTED . '>' . $state['vName'] . '</option>';
        }
    } else {
        $HTML .= '<option disabled> No Data Found. </option>';
    }
    $HTML .= '</select>';
    echo $HTML;
    exit;
}
