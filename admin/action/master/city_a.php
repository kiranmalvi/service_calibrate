<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 6/4/15
 * Time: 4:14 PM
 */
include_once($inc_class_path . 'city.class.php');

$city = new city();
$MODE = $_REQUEST['mode'];

$LIST_LINK = 'index.php?file=ma-citylist';
$MODULE = 'City';
#pr($_REQUEST);exit;
/* Add Mode */
if ($MODE == 'add') {
    $city->setiStateId($_REQUEST['iStateId']);
    $city->setvName($_REQUEST['vName']);
    $city->setiDtAdded($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $city->seteStatus($_REQUEST['eStatus']);

    $city->insert();
    $generalfuncobj->func_set_temp_sess_msg($MODULE . " Added Successfully");
    header("Location:" . $LIST_LINK);
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $city->setiStateId($_REQUEST['iStateId']);
    $city->setvName($_REQUEST['vName']);
    $city->seteStatus($_REQUEST['eStatus']);

    $city->update($_REQUEST['iCityId']);
    $generalfuncobj->func_set_temp_sess_msg($MODULE . " Updated Successfully");
    header("Location:" . $LIST_LINK);
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $city->delete($ID);
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $city->delete($ID);
        }
    }

    $generalfuncobj->func_set_temp_sess_msg($MODULE . " Deleted Successfully");
    header("Location:" . $LIST_LINK);
    exit;
}


if ($MODE == 'get_city') {
    ob_get_clean();
    #pr($_REQUEST);exit;
    $iStateId = $_REQUEST['state_id'];
    $selected = $_REQUEST['selected'];
    $city->setiStateId($iStateId);
    $DATA = $city->select_on_state();

    $HTML = '<select class="form-control" name="iCityId" id="iCityId">';
    if (count($DATA) > 0) {
        $HTML .= '<option selected disabled>- Select City -</option>';
        foreach ($DATA as $state) {
            $SELECTED = ($selected == $state['iCityId']) ? 'selected' : '';
            $HTML .= '<option value="' . $state['iCityId'] . '" ' . $SELECTED . '>' . $state['vName'] . '</option>';
        }
    } else {
        $HTML .= '<option disabled> No Data Found. </option>';
    }
    $HTML .= '</select>';
    echo $HTML;
    exit;
}

