<?php

//echo "<pre>";print_r($_REQUEST);echo "</pre>";exit;
include_once($inc_class_path . 'plan.class.php');
$planObj = new plan();
include_once($inc_class_path . 'feature.class.php');
$featureObj = new feature();

include_once($inc_class_path . 'plan_feature_mapping.class.php');
$planFeatureMappingObj = new plan_feature_mapping();

//$type = $_REQUEST['type'];

if (isset($_REQUEST['TYPE']) AND $_REQUEST['TYPE'] == 'Change_Plan') {
    ob_clean();

    $planId = $_REQUEST['planId'];
    $featureId = $_REQUEST['featureId'];
    $action = $_REQUEST['action'];

    if ($action == 'insert') {
        $planFeatureMappingObj->setDtAdded(strtotime(gmdate('Y-m-d h:i:s')));
        $planFeatureMappingObj->seteStatus('1');
        $planFeatureMappingObj->setiFeatureId($featureId);
        $planFeatureMappingObj->setiPlanId($planId);
        $planFeatureMappingObj->insert();
    } elseif ($action == 'delete') {
        $planFeatureMappingObj->delete_plan_feature($planId, $featureId);
    }
}


if(isset($_REQUEST['planFeature']) && $_REQUEST['planFeature'] == 'Add')
{
    $planFeatureMappingObj->setiPlanId($_REQUEST['iPlanId']);
    $planFeatureMappingObj->setiFeatureId($_REQUEST['iFeatureId']);
    $planFeatureMappingObj->setDtAdded(strtotime(date('Y-m-d H:i:s')));
    $planFeatureMappingObj->seteStatus('1');
    $insRec =  $planFeatureMappingObj->insert();

    if($insRec) {
        $generalfuncobj->func_set_temp_sess_msg("User Details Added Successfully");
        header("Location:index.php?file=pl-planfeatureadd");
        exit;
    }
    else{
        $generalfuncobj->func_set_temp_sess_msg("User Details not Added Successfully");
        header("Location:index.php?file=pl-planfeatureadd");
        exit;
    }
}

if(isset($_REQUEST['action']) && $_REQUEST['action']=="featureList")
{
    ob_get_clean();
   $featureList = $featureObj->featureListPlan($_REQUEST['iPlanId']);
    $output = '';
    $output .= '<option value="">Select Feature</option>';
    foreach($featureList as $feature) {
        $output .= '<option value="'.$feature['iFeatureId'].'">'.$feature['vFeatureName'].'</option>';
    }
    echo $output;
    exit;

}

if (isset($_REQUEST['mode']) AND $_REQUEST['mode'] == 'edit') {
    $planObj->setvPlanName($_REQUEST['vPlanName']);
    $planObj->setvDescription($_REQUEST['vDescription']);
    $planObj->setFMonthPrice($_REQUEST['fMonthPrice']);
    $planObj->setfDayPrice($_REQUEST['fDayPrice']);
    $planObj->seteStatus($_REQUEST['eStatus']);
    $planObj->update($_REQUEST['iPlanId']);

    $generalfuncobj->func_set_temp_sess_msg("Plan Updated Successfully");
    header("Location: index.php?file=pl-planslist");
    exit;
}