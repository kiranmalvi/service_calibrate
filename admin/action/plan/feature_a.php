<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 15/4/15
 * Time: 12:50 PM
 */

include_once($inc_class_path . 'feature.class.php');
$featureObj = new feature();

if (isset($_REQUEST['mode']) AND $_REQUEST['mode'] == 'edit') {

    $featureObj->setvFeatureName($_POST['vFeatureName']);
    $featureObj->setvDescription($_POST['vDescription']);
    $featureObj->seteStatus($_POST['eStatus']);
    $featureObj->update($_POST['iFeatureId']);

    $generalfuncobj->func_set_temp_sess_msg("Feature Updated Successfully");
    header("Location: index.php?file=pl-featurelist&type=" . $_REQUEST['eUSerType']);
    exit;
}