<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 28/3/15
 * Time: 1:40 PM
 */
//echo "<pre>"; print_r($_REQUEST);print_r($_FILES);exit;


include_once($inc_class_path . 'advertisement.class.php');
$advertisement = new advertisement();
$MODE = $_REQUEST['mode'];

/* Add Mode */
if ($MODE == 'add') {
    $advertisement->setvAddName($_REQUEST['vAddName']);
    if ($_FILES["vImage"]["name"] != "") {
        $photopath = $ad_image_path;
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];

        //$vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");

        $advertisement_img_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);
        $advertisement_img_name = $advertisement_img_arr[0];


        if ($advertisement_img_name == 'no') {
            $generalfuncobj->func_set_temp_sess_msg("Please upload pictures only in png,jpg,jpeg,gif or bmp format.", "alert-danger","Error");
            header("Location:index.php?file=ad-advertisementadd&mode=add");
            exit;
        } else {
            $advertisement->setvImage($advertisement_img_name);
        }
    }

    $advertisement_img_name;

    $sdate = $_REQUEST['iStartDate'];
    $edate = $_REQUEST['iEndDate'];

    $advertisement->setiStartDate(strtotime($sdate));
    $advertisement->setiEndDate(strtotime($edate));

    $advertisement->seteStatus($_REQUEST['eStatus']);

    $advertisement->insert();
    $generalfuncobj->func_set_temp_sess_msg("advertisement Details Added Successfully");
    header("Location:index.php?file=ad-advertisementlist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    //echo "<pre>"; print_r($_REQUEST);exit;
    $iId=$_REQUEST['iAdvertisementId'];
    $advertisement->setvAddName($_REQUEST['vAddName']);
    if ($_FILES["vImage"]["name"] == "") {

        $advertisement->setvImage($_POST['image_hid']);
    } else {
        $photopath = $ad_image_path;
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];

        //$vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");

        $advertisement_img_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);

        $advertisement_img_name = $advertisement_img_arr[0];
        if ($advertisement_img_name == '') {
            $generalfuncobj->func_set_temp_sess_msg("Please upload pictures only in png,jpg,jpeg,gif or bmp format.", "alert-danger","Error");
            header("Location:index.php?file=ad-advertisementadd&mode=edit&iId=$iId");
            exit;
        } else {

            $advertisement->setvImage($advertisement_img_name);
        }
    }
    $sdate = $_REQUEST['iStartDate'];
    $edate = $_REQUEST['iEndDate'];

    $advertisement->setiStartDate(strtotime($sdate));
    $advertisement->setiEndDate(strtotime($edate));

    $advertisement->seteStatus($_REQUEST['eStatus']);


    $advertisement->update($_REQUEST['iAdvertisementId']);

    $generalfuncobj->func_set_temp_sess_msg("Advertisement Details <b>Update</b> Successfully");
    header("Location:index.php?file=ad-advertisementlist");
    exit;
}


/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $exe_del = "Update advertisement set eStatus='2' where iAdvertisementId in ($ID)";
        $obj->sql_query($exe_del);

        $generalfuncobj->func_set_temp_sess_msg("Advertisement Details Deleted Successfully");
        header("Location:index.php?file=ad-advertisementlist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $exe_del = "Update advertisement set eStatus='2' where iAdvertisementId in ($ID)";
            $obj->sql_query($exe_del);
        }

        $generalfuncobj->func_set_temp_sess_msg("Advertisement Details Deleted Successfully");
        header("Location:index.php?file=ad-advertisementlist");
        exit;
    }
}


