<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 28/3/15
 * Time: 1:40 PM
 */

include_once($inc_class_path . 'help_faq.class.php');

$helpfaqObj = new help_faq();
$MODE = $_REQUEST['mode'];

/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $helpfaqObj->setvQuestion($_REQUEST['vQuestion']);
    $helpfaqObj->setvAnswer($_REQUEST['vAnswer']);
    $helpfaqObj->setvLink($_REQUEST['vLink']);

    $helpfaqObj->setidtAdded(strtotime(gmdate('Y-m-d H:i:s')));
    $helpfaqObj->setidtUpdated(strtotime(gmdate('Y-m-d H:i:s')));

    $helpfaqObj->seteStatus($_REQUEST['eStatus']);


    $helpfaqObj->insert();
    $generalfuncobj->func_set_temp_sess_msg("Help Details Added Successfully");
    header("Location:index.php?file=hfq-help_faq_map");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {


    $helpfaqObj->setvQuestion($_REQUEST['vQuestion']);
    $helpfaqObj->setvAnswer($_REQUEST['vAnswer']);
    $helpfaqObj->setvLink($_REQUEST['vLink']);

    $helpfaqObj->setidtAdded(strtotime(gmdate('Y-m-d H:i:s')));
    $helpfaqObj->setidtUpdated(strtotime(gmdate('Y-m-d H:i:s')));

    $helpfaqObj->seteStatus($_REQUEST['eStatus']);


    $helpfaqObj->update($_REQUEST['iHelpId']);

    $_SESSION['SC_LOGIN']['vFirstName'] = $_REQUEST['vFirstName'];
    $_SESSION['SC_LOGIN']['vLastName'] = $_REQUEST['vLastName'];

    $generalfuncobj->func_set_temp_sess_msg("Help Details <b>Update</b> Successfully");
    header("Location:index.php?file=hfq-help_faq_map");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $helpfaqObj->delete($ID);

        $generalfuncobj->func_set_temp_sess_msg("Help Details Deleted Successfully");
        header("Location:index.php?file=hfq-help_faq_map");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $helpfaqObj->delete($ID);
        }

        $generalfuncobj->func_set_temp_sess_msg("Help Details Deleted Successfully");
        header("Location:index.php?file=hfq-help_faq_map");
        exit;
    }
}