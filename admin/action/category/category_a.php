<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 31/3/15
 * Time: 11:33 AM
 */

include_once($inc_class_path . 'category.class.php');

$category = new category();
$MODE = $_REQUEST['mode'];
$todaydate = $generalfuncobj->gm_date_string();
/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $category->setvCategoryName($_REQUEST['vCategoryName']);
    $category->setiDtAdded($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone'] ));
    $category->setiDtUpdated($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $category->seteStatus($_REQUEST['eStatus']);

    $category->insert();
    $generalfuncobj->func_set_temp_sess_msg("Category Details Added Successfully");
    header("Location:index.php?file=cat-categorylist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $category->setvCategoryName($_REQUEST['vCategoryName']);
    $category->seteStatus($_REQUEST['eStatus']);
      $category->setiDtUpdated($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $category->update($_REQUEST['iCategoryId']);
    $generalfuncobj->func_set_temp_sess_msg("Category Details Updated Successfully");
    header("Location:index.php?file=cat-categorylist");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
       /* $category->delete($ID);*/

        $sql = " UPDATE category SET eStatus = '2',iDtUpdated='$todaydate' WHERE iCategoryId = '$ID'";
        $obj->sql_query($sql);

        $generalfuncobj->func_set_temp_sess_msg("Category Details Deleted Successfully");
        header("Location:index.php?file=cat-categorylist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
           /* $category->delete($ID);*/
            $sql = " UPDATE category SET eStatus = '2', iDtUpdated='$todaydate' WHERE iCategoryId = '$ID'";
            $obj->sql_query($sql);
        }

        $generalfuncobj->func_set_temp_sess_msg("Category Details Deleted Successfully");
        header("Location:index.php?file=cat-categorylist");
        exit;
    }
}

