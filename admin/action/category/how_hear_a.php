<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 31/3/15
 * Time: 1:01 PM
 */
include_once($inc_class_path . 'how_did_hear.class.php');

$how_did_hear = new how_did_hear();
$MODE = $_REQUEST['mode'];

/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $how_did_hear->setvName($_REQUEST['vName']);
   $how_did_hear->setiDtAdded($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $how_did_hear->setiDtUpdated($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $how_did_hear->seteStatus($_REQUEST['eStatus']);

    $how_did_hear->insert();
    $generalfuncobj->func_set_temp_sess_msg("Data Added Successfully");
    header("Location:index.php?file=cat-how_hear_list");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $how_did_hear->setvName($_REQUEST['vName']);
    $how_did_hear->seteStatus($_REQUEST['eStatus']);
    $how_did_hear->setiDtUpdated($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));

    $how_did_hear->update($_REQUEST['iHDHId']);
    $generalfuncobj->func_set_temp_sess_msg("Data Updated Successfully");
    header("Location:index.php?file=cat-how_hear_list");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $how_did_hear->delete($ID);

        $date=$generalfuncobj->gm_date_string();
        $exe_del = "Update how_did_hear set eStatus='2'  AND iDtUpdated='$date' where iHDHId in ($ID)";
        $obj->sql_query($exe_del);


        $generalfuncobj->func_set_temp_sess_msg("Data Deleted Successfully");
        header("Location:index.php?file=cat-how_hear_list");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $date=$generalfuncobj->gm_date_string();
            $exe_del = "Update how_did_hear set eStatus='2'  AND iDtUpdated='$date' where iHDHId in ($ID)";
            $obj->sql_query($exe_del);
        }

        $generalfuncobj->func_set_temp_sess_msg("Data Deleted Successfully");
        header("Location:index.php?file=cat-how_hear_list");
        exit;
    }
}