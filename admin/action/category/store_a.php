<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 1/4/15
 * Time: 11:16 AM
 */
include_once($inc_class_path . 'user_type_store.class.php');

$user_type_Store = new user_type_Store();
$MODE = $_REQUEST['mode'];
$todaydate = $generalfuncobj->gm_date_string();

/*echo $todaydate;
exit;*/

/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $user_type_Store->setvName($_REQUEST['vName']);
    $user_type_Store->setiDtAdded($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $user_type_Store->setiDtUpdated($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $user_type_Store->seteStatus($_REQUEST['eStatus']);

    $user_type_Store->insert();
    $generalfuncobj->func_set_temp_sess_msg("Store Details Added Successfully");
    header("Location:index.php?file=cat-storelist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $user_type_Store->setvName($_REQUEST['vName']);
    $user_type_Store->setiDtUpdated($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $user_type_Store->seteStatus($_REQUEST['eStatus']);

    $user_type_Store->update($_REQUEST['iUserTypeStoreId']);
    $generalfuncobj->func_set_temp_sess_msg("Store Details Updated Successfully");
    header("Location:index.php?file=cat-storelist");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
/*        $user_type_Store->delete($ID);*/
        $sql = " UPDATE user_type_store SET eStatus = '2' , iDtUpdated = '$todaydate' WHERE iUserTypeStoreId = '$ID'";
        $obj->sql_query($sql);

        $generalfuncobj->func_set_temp_sess_msg("Store Details Deleted Successfully");
        header("Location:index.php?file=cat-storelist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
        /*    $user_type_Store->delete($ID);*/
            $sql = " UPDATE user_type_store SET eStatus = '2' , iDtUpdated = '$todaydate' WHERE iUserTypeStoreId = '$ID'";
            $obj->sql_query($sql);

        }

        $generalfuncobj->func_set_temp_sess_msg("Store Details Deleted Successfully");
        header("Location:index.php?file=cat-storelist");
        exit;
    }
}