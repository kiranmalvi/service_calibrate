<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 1/4/15
 * Time: 10:53 AM
 */

include_once($inc_class_path . 'user_role.class.php');

$user_role = new user_role();
$MODE = $_REQUEST['mode'];

$todaydate = $generalfuncobj->gm_date_string();
/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $user_role->setvName($_REQUEST['vName']);
    $user_role->setiUserTypeId($_REQUEST['iUserTypeId']);
      $user_role->setiDtAdded($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $user_role->setiDtUpdated($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));

    $user_role->seteStatus($_REQUEST['eStatus']);

    $user_role->insert();
    $generalfuncobj->func_set_temp_sess_msg("Role Added Successfully");
    header("Location:index.php?file=cat-titlelist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $user_role->setvName($_REQUEST['vName']);
        $user_role->setiDtUpdated($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
    $user_role->setiUserTypeId($_REQUEST['iUserTypeId']);
    $user_role->seteStatus($_REQUEST['eStatus']);

    $user_role->update($_REQUEST['iUserRoleId']);
    $generalfuncobj->func_set_temp_sess_msg("Role Updated Successfully");
    header("Location:index.php?file=cat-titlelist");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        /*$user_role->delete($ID);*/
        $sql = " UPDATE user_role SET eStatus = '2',iDtUpdated='$todaydate' WHERE iUserRoleId = '$ID'";
        $obj->sql_query($sql);

        $generalfuncobj->func_set_temp_sess_msg("Role Deleted Successfully");
        header("Location:index.php?file=cat-titlelist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
           /* $user_role->delete($ID);*/
            $sql = " UPDATE user_role SET eStatus = '2',iDtUpdated='$todaydate' WHERE iUserRoleId = '$ID'";
            $obj->sql_query($sql);
        }

        $generalfuncobj->func_set_temp_sess_msg("Role Deleted Successfully");
        header("Location:index.php?file=cat-titlelist");
        exit;
    }
}