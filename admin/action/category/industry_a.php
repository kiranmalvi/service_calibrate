<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 31/3/15
 * Time: 12:23 PM
 */
include_once($inc_class_path . 'industries.class.php');

$industries = new industries();
$MODE = $_REQUEST['mode'];
$todaydate = $generalfuncobj->gm_date_string();
/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $industries->setvName($_REQUEST['vName']);
     $industries->setiDtAdded($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone'] ));
    $industries->setiDtUpdated($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone'] ));
    //$industries->setiDtAdded($generalfuncobj->gm_date_string());
    //$industries->setiDtUpdated($generalfuncobj->gm_date_string());
    $industries->seteStatus($_REQUEST['eStatus']);

    $industries->insert();
    $generalfuncobj->func_set_temp_sess_msg("Industries Details Added Successfully");
    header("Location:index.php?file=cat-industrylist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $industries->setvName($_REQUEST['vName']);
    $industries->seteStatus($_REQUEST['eStatus']);
      $industries->setiDtUpdated($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone'] ));

    $industries->update($_REQUEST['iIndustriesId']);
    $generalfuncobj->func_set_temp_sess_msg("Industries Details Updated Successfully");
    header("Location:index.php?file=cat-industrylist");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        /*$industries->delete($ID);*/
        $sql = " UPDATE industries SET eStatus = '2', iDtUpdated='$todaydate' WHERE iIndustriesId = '$ID'";
        $obj->sql_query($sql);
        $generalfuncobj->func_set_temp_sess_msg("Industries Details Deleted Successfully");
        header("Location:index.php?file=cat-industrylist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            /*$industries->delete($ID);*/
            $sql = " UPDATE industries SET eStatus = '2', iDtUpdated='$todaydate' WHERE iIndustriesId = '$ID'";
            $obj->sql_query($sql);
        }

        $generalfuncobj->func_set_temp_sess_msg("Industries Details Deleted Successfully");
        header("Location:index.php?file=cat-industrylist");
        exit;
    }
}