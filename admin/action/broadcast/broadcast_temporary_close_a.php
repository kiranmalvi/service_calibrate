<?
$mode = "add";
include_once($inc_class_path . 'api.class.php');
$apiObj = new API();
//$mode = $_REQUEST['mode'];
include_once($inc_class_path . 'broadcast.class.php');
$broadcastObj = new broadcast();
include_once($inc_class_path . 'orders.class.php');
$ordersObj = new  orders();

include_once($inc_class_path . 'user.class.php');
$userObj = new  user();

include_once($inc_class_path . 'message_conversation.class.php');
$conversationObj = new message_conversation();
include_once($inc_class_path . 'message.class.php');
$messageObj = new message();

include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();
include_once($inc_class_path . 'broadcast_detail.class.php');
$broadcast_detailObj = new broadcast_detail();

include_once($inc_class_path . 'broadcast_detail.class.php');
$broadcastdtlObj = new broadcast_detail();
$type = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$DtDate = strtotime($_POST['date']);
$vMessage = $_REQUEST['message'];

//$mode1=$_REQUEST["mode1"];
$eBrodcastType = $_REQUEST['broadcast_type'];
$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));

if ($mode == "add") {
    $type1 = "Temporary Close";
    // $broadcastObj->seteType("1");
    $broadcast_detailObj->setdtToDate(strtotime($_REQUEST['date_open']));
    $broadcastObj->setiUserId($iUserId);
    $broadcastObj->seteType("1");
    $broadcastObj->setvMessage($_REQUEST['message']);
    $broadcastObj->seteStatus("1");
    $broadcastObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));

    $iBroadcastId = $broadcastObj->insert();

    $date = $generalfuncobj->getdateforamate($_REQUEST['closing_date']);
    $edate = $generalfuncobj->getdateforamate($_REQUEST['opening_date']);
    $broadcastdtlObj->setdtFromDate($date);
    $broadcastdtlObj->setdtToDate($edate);
    $broadcastdtlObj->setiBroadcastId($iBroadcastId);
    $sendmessage = $type1 . "\n" . "We are closed from " . $_REQUEST['closing_date'] . " to " . $_REQUEST['opening_date'] . "  " . $vMessage;

    $iBroadCastDetailId = $broadcastdtlObj->insert();
    $user = $userObj->select($iUserId);

    if ($type == "2" || $type == "3") {
        // echo $type;
        $receiver = $ordersObj->select_dist_man($iUserId);
        // pr($receiver);
    } else {
        $receiver = $ordersObj->select_retailer($iUserId);
    }
    // pr($receiver);exit;
    if (count($receiver) > 0) {

        //send message to all vendor who have scanned qr code
        for ($i = 0; $i < count($receiver); $i++) {
            $receiverId = $receiver[$i]['iScannedId'];
//check if there is any conversation between user & employee,add if no conversation or update status
            $check_exist = $conversationObj->check_user_added($iUserId, $receiverId);

            if (count($check_exist) > 0) {
                $iConversationId = $check_exist[0]['iConversationId'];
                $conversationObj->seteStatus("1");
                $conversationObj->setiDtUpdated($iDtAdded);
                $conversationObj->update_status($iConversationId);
            } else {
                $conversationObj->setiUserId($iUserId);
                $conversationObj->setiUseraddedId($receiverId);
                $conversationObj->seteStatus("1");
                $conversationObj->setiDtAdded($iDtAdded);
                $conversationObj->setiDtUpdated($iDtAdded);
                $iConversationId = $conversationObj->insert();

            }
//same for employee side
            if ($iConversationId > 0) {

                $check_exist_receiver = $conversationObj->check_user_added($receiverId, $iUserId);


                if (count($check_exist_receiver) > 0) {
                    $iConversationId = $check_exist_receiver[0]['iConversationId'];
                    $conversationObj->seteStatus("1");
                    $conversationObj->setiDtUpdated($iDtAdded);
                    $conversationObj->update_status($iConversationId);
                } else {
                    $conversationObj->setiUserId($receiverId);
                    $conversationObj->setiUseraddedId($iUserId);
                    $conversationObj->seteStatus("1");
                    $conversationObj->setiDtAdded($iDtAdded);
                    $conversationObj->setiDtUpdated($iDtAdded);
                    $iConversationId = $conversationObj->insert();

                }

                //send message & set eMessage_Type = "b' for brodacast type messages
                if ($iConversationId > 0) {
                    $messageObj->setiSenderId($iUserId);
                    $messageObj->setiReceiverId($receiverId);
                    $messageObj->setvMessage($sendmessage);
                    $messageObj->seteStatus("1");
                    $messageObj->setiDateAdded($iDtAdded);
                    $messageObj->setiDtUpdated($iDtAdded);
                    $messageObj->seteMessage_Type("b");
                    $messageObj->seteRead("0");

                    $iMessageId = $messageObj->insert();

                    $setting = $user_settingsObj->select_user_setting($iUserId);

                    if ($setting[0]['eNotification'] == "1") {
                        echo "send";
                        $message1 = "New Message from " . $user[0]['vFirstName'];
                        $apiObj->notify($receiverId, $message1, 'M', $iUserId);
                        $generalfuncobj->func_set_temp_sess_msg("Brodcast sent successfully.");
                        header("Location:index.php?file=b-broadcast");
                    }

                    if ($iMessageId > 0) {
                        $data[$i]['iUserId'] = $iUserId;
                        $data[$i]['iReceiverId'] = $receiverId;
                        $data[$i]['vMessage'] = $sendmessage;
                        $data[$i]['eMessageType'] = "b";
                    }
                }
            }
        }


        $data = $data;
        $extras->eType = $type;
        $status = "1";
        $msg = "Brodcast sent successfully";
        $generalfuncobj->func_set_temp_sess_msg("Brodcast sent successfully.");
        header("Location:index.php?file=b-broadcast");
        exit;

    } else {
        //echo "hi   vendor";
        $status = "0";
        $msg = "There are no vendors";
        $generalfuncobj->func_set_temp_sess_msg("There are no vendors");
        header("Location:index.php?file=b-broadcast");
        exit;

    }

//   $generalfuncobj->func_set_temp_sess_msg("Broadcast for Temporary close sent successfully.");
    header("Location:index.php?file=b-broadcast");
    exit;
}


