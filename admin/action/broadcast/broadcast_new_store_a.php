<?php echo "<pre/>";
//print_R($_REQUEST);
//print_r($_FILES);
?>
<?

//$mode = $_REQUEST['mode'];
$mode = "add";
//$iId = $_REQUEST['iId'];
include_once($inc_class_path . 'api.class.php');
$apiObj = new API();


include_once($inc_class_path . 'broadcast.class.php');
$broadcastObj = new broadcast();
include_once($inc_class_path . 'orders.class.php');
$ordersObj = new  orders();

include_once($inc_class_path . 'user.class.php');
$userObj = new  user();

include_once($inc_class_path . 'message_conversation.class.php');
$conversationObj = new message_conversation();
include_once($inc_class_path . 'message.class.php');
$messageObj = new message();

include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();


include_once($inc_class_path . 'broadcast_detail.class.php');
$broadcastdtlObj = new broadcast_detail();
$type = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$DtDate = strtotime($_REQUEST['date']);
$vMessage = $_REQUEST['message'];

$eBrodcastType = $_REQUEST['broadcast_type'];
$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
$date = $_REQUEST['opening_date'];
//$date = explode('-',$date);
//$date = $date[2].'-'.$date[0].'-'.$date[1];
////echo $date;

//$date = "09/27/2015 00:00:00";
//$your_date = date('Y-m-d',strtotime($date));
//$date1=explode("-",$your_date);

//$result = implode(" ",$date1);
//echo "<br>".strtotime($date);
$date = $generalfuncobj->getdateforamate($date);
if ($mode == "add") {
    $type1 = "New Store Opening";


    $broadcastObj->setiUserId($iUserId);
    $broadcastObj->seteType("0");
    $broadcastObj->setvMessage($_POST['message']);
    $broadcastObj->seteStatus("1");
    $broadcastObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));

    $iBroadcastId = $broadcastObj->insert();


    $broadcastdtlObj->settLocationAddress($_POST['location_address']);
    $broadcastdtlObj->setvStreetAddress($_POST['street_address']);
    $broadcastdtlObj->setvZip($_POST['zip']);
    $broadcastdtlObj->setvCity($_POST['city']);
    $broadcastdtlObj->setvState($_POST['state']);
    $broadcastdtlObj->setvContact($_POST['contact']);
    $broadcastdtlObj->setvPhone($_POST['phone']);
    $broadcastdtlObj->setdtFromDate($date);
    // $broadcastdtlObj->settTime(strtotime($_POST['opening_time']));

    $broadcastdtlObj->setiBroadcastId($iBroadcastId);

    $iBroadCastDetailId = $broadcastdtlObj->insert();
    $sendmessage = $type1 . "\n" . "  Address Below::" . "\n" . " " . $_POST['street_address'] . "," . "\n" . " " . "\n" . " " . $_POST['city'] . "," . "\n" . " " . $_REQUEST['state'] . "," . $_POST['zip'] . " ," . "\n" . " " . "Contact Person: " . $_POST['contact'] . "," . "\n" . " " . "Phone:" . $_POST['phone'] . "," . "\n" . "" . "Opening Date::" . "" . $_REQUEST['opening_date'] . "," . "\n" . " " . $vMessage;
//echo $sendmessage;

    // $type=$userObj->getiUserTypeId();
    // echo $type;
    $user = $userObj->select($iUserId);

    if ($type == "2" || $type == "3") {
        // echo $type;
        $receiver = $ordersObj->select_dist_man($iUserId);
        // pr($receiver);
    } else {
        $receiver = $ordersObj->select_retailer($iUserId);
    }
    // pr($receiver);exit;
    if (count($receiver) > 0) {

        //send message to all vendor who have scanned qr code
        for ($i = 0; $i < count($receiver); $i++) {
            $receiverId = $receiver[$i]['iScannedId'];
//check if there is any conversation between user & employee,add if no conversation or update status
            $check_exist = $conversationObj->check_user_added($iUserId, $receiverId);

            if (count($check_exist) > 0) {
                $iConversationId = $check_exist[0]['iConversationId'];
                $conversationObj->seteStatus("1");
                $conversationObj->setiDtUpdated($iDtAdded);
                $conversationObj->update_status($iConversationId);
            } else {
                $conversationObj->setiUserId($iUserId);
                $conversationObj->setiUseraddedId($receiverId);
                $conversationObj->seteStatus("1");
                $conversationObj->setiDtAdded($iDtAdded);
                $conversationObj->setiDtUpdated($iDtAdded);
                $iConversationId = $conversationObj->insert();

            }
//same for employee side
            if ($iConversationId > 0) {

                $check_exist_receiver = $conversationObj->check_user_added($receiverId, $iUserId);


                if (count($check_exist_receiver) > 0) {
                    $iConversationId = $check_exist_receiver[0]['iConversationId'];
                    $conversationObj->seteStatus("1");
                    $conversationObj->setiDtUpdated($iDtAdded);
                    $conversationObj->update_status($iConversationId);
                } else {
                    $conversationObj->setiUserId($receiverId);
                    $conversationObj->setiUseraddedId($iUserId);
                    $conversationObj->seteStatus("1");
                    $conversationObj->setiDtAdded($iDtAdded);
                    $conversationObj->setiDtUpdated($iDtAdded);
                    $iConversationId = $conversationObj->insert();

                }

                //send message & set eMessage_Type = "b' for brodacast type messages
                if ($iConversationId > 0) {
                    $messageObj->setiSenderId($iUserId);
                    $messageObj->setiReceiverId($receiverId);
                    $messageObj->setvMessage($sendmessage);
                    $messageObj->seteStatus("1");
                    $messageObj->setiDateAdded($iDtAdded);
                    $messageObj->setiDtUpdated($iDtAdded);
                    $messageObj->seteMessage_Type("b");
                    $messageObj->seteRead("0");

                    $iMessageId = $messageObj->insert();

                    $setting = $user_settingsObj->select_user_setting($iUserId);

                    if ($setting[0]['eNotification'] == "1") {

                        $message1 = "New Message from " . $user[0]['vFirstName'];
                        $apiObj->notify($receiverId, $message1, 'M', $iUserId);
                        $generalfuncobj->func_set_temp_sess_msg("Brodcast sent successfully.");
                        header("Location:index.php?file=b-broadcast");
                    }

                    if ($iMessageId > 0) {
                        $data[$i]['iUserId'] = $iUserId;
                        $data[$i]['iReceiverId'] = $receiverId;
                        $data[$i]['vMessage'] = $sendmessage;
                        $data[$i]['eMessageType'] = "b";
                    }
                }
            }
        }


        $data = $data;
        $extras->eType = $type;
        $status = "1";
        $msg = "Brodcast sent successfully";
        $generalfuncobj->func_set_temp_sess_msg("Brodcast sent successfully.");
        header("Location:index.php?file=b-broadcast");
        exit;

    } else {
        //echo "hi   vendor";
        $status = "0";
        $msg = "There are no vendors";
        $generalfuncobj->func_set_temp_sess_msg("There are no vendors");
        header("Location:index.php?file=b-broadcast");
        exit;

    }
    //   echo "hi   vendor";


//    $generalfuncobj->func_set_temp_sess_msg("Broadcast for New Store sent successfully.");
    header("Location:index.php?file=b-broadcast");
    exit;
}