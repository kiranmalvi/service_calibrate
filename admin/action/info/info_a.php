<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 28/3/15
 * Time: 1:40 PM
 */
pr($_REQUEST);

include_once($inc_class_path . 'info.class.php');
$info = new info();
$MODE = $_REQUEST['mode'];

/* Add Mode */
if ($MODE == 'add') {

    $info->setvKeyword($_REQUEST['vKeyword']);
    $info->settDescription(($_POST['tDescription']));
    $info->setiDtAdded(strtotime(gmdate('Y-m-d H:i:s')));
    $info->setiDtUpdated(strtotime(gmdate('Y-m-d H:i:s')));
    $info->seteStatus($_REQUEST['eStatus']);
    $info->setVFor($_REQUEST['vFor']);
    $info->setvTitle($_REQUEST['vTitle']);

    $iInfoId=$info->insert();
    $generalfuncobj->func_set_temp_sess_msg("information Details Added Successfully");
    header("Location:index.php?file=i-infolist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $info->setvKeyword($_REQUEST['vKeyword']);
    $info->settDescription(($_POST['tDescription']));
    $info->setVFor($_REQUEST['vFor']);
    $info->setvTitle($_REQUEST['vTitle']);
    $info->seteStatus($_REQUEST['eStatus']);
    $info->setiDtUpdated(strtotime(gmdate('Y-m-d H:i:s')));
    $info->update($_REQUEST['iInfoId']);

    $generalfuncobj->func_set_temp_sess_msg("Information <b>Update</b> Successfully");
    header("Location:index.php?file=i-infolist");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $date=$generalfuncobj->gm_date_string();


        $exe_del = "Update info set eStatus='2' , iDtUpdated='$date'  where iInfoId in ($ID)";
        $obj->sql_query($exe_del);

        $generalfuncobj->func_set_temp_sess_msg("Information Details Deleted Successfully");
        header("Location:index.php?file=i-infolist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $exe_del = "Update info set eStatus='2' , iDtUpdated='$date' where iInfoId in ($ID)";
            $obj->sql_query($exe_del);
        }

        $generalfuncobj->func_set_temp_sess_msg("Information Details Deleted Successfully");
        header("Location:index.php?file=i-infolist");
        exit;
    }
}

