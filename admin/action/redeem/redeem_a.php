<?php
/**
 * Created by PhpStorm.
 * User: jitesh
 * Date: 26/5/15
 * Time: 11:42 AM
 */

include_once($inc_class_path . 'redeem.class.php');
$redeem_Obj = new redeem();

include_once($inc_class_path . 'user.class.php');
$user = new user();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();


$vStoreUniqueId = $_SESSION['SC_LOGIN']['USER']['vStoreUniqueId'];
$MODE = $_REQUEST['mode'];
$updatedate = strtotime(date('Y-m-d H:i:s'));

$uid = ($_REQUEST['iuserId']);
$cardid = ($_REQUEST['iId']);
/* Add Mode */
if ($MODE == 'addredeempoints') {
    //pr($_REQUEST);exit;

    $redeem_Obj->setiUserId($_REQUEST['iuserId']);
    $redeem_Obj->setiCardId($_REQUEST['iId']);
    $redeem_Obj->setvPoints($_REQUEST['points']);
    $redeem_Obj->seteStatus('1');
    $redeem_Obj->setiDtAdded((strtotime(date('Y-m-d H:i:s'))));
    $redeem_Obj->setiUpdated((strtotime(date('Y-m-d H:i:s'))));
    $redeem_Obj->seteAccept('0');
    $redeem_Obj->insert();
    $mail = $user->get_detail_from_storeuniqueid($vStoreUniqueId);
    $email = $mail[0]['vEmail'];
    $DATA = array('name' => $vStoreUniqueId);
    $usid = $_REQUEST['iuserId'];
    $updatedate = strtotime(date('Y-m-d H:i:s'));

    $points = $referralObj->get_total_point($usid);
    $totalpoint = $points[0]['vPoint'];
    $point = $totalpoint - $_REQUEST['points'];
    $query = "UPDATE referral set vPoint='$point',iDtUpdated='$updatedate' where iUserId='$usid'";
    $obj->sql_query($query);

    echo $generalfuncobj->get_SC_SEND_mail('redeem_request', $DATA, $email, 'Gift Voucher Request');
    $generalfuncobj->func_set_temp_sess_msg("Regarding Email is sent successfully , please check your inbox");
    header("Location:index.php?file=rp-redeem");
    exit;

}
if ($MODE == 'accept') {

    $vStoreUniqueId = $_REQUEST['iuniquestoreid'];
    $iredeemId = $_REQUEST['iredeemId'];
    $usercoin = $_REQUEST['usercoins'];
    $Totalcoint = $_REQUEST['Totalcoins'];
    /* $query = "UPDATE redeem r JOIN referral rf ON r.iUserId = rf.iUserId SET r.eAccept = '1',r.eStatus = '2',rf.vPoint = $usercoin - $Totalcoint WHERE iRedeemId = $iredeemId";
     $obj->sql_query($query);*/
    $query = "UPDATE redeem set eAccept = '1',eStatus = '2',iUpdated='$updatedate' where iRedeemId='$iredeemId'";
    $obj->sql_query($query);


    $mail = $user->get_detail_from_storeuniqueid($vStoreUniqueId);
    $email = $mail[0]['vEmail'];
    $DATA = array('name' => $vStoreUniqueId, 'url' => $site_url);

    echo $generalfuncobj->get_SC_SEND_mail('redeem', $DATA, $email, 'Get Your Voucher');
    $generalfuncobj->func_set_temp_sess_msg("Gift Card Sent Succesfully");
    header("Location:index.php?file=rp-redeemlist_user");
}
?>