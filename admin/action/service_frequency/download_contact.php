<?php

ob_get_clean();
$file_name = 'exel_report';
$utype=$_SESSION['SC_LOGIN']['USER']['iUserTypeId'];

if(isset($_REQUEST['file_name']) && $_REQUEST['file_name'] != ''){
    $file_name = $_REQUEST['file_name'];
}
$filename = $file_name.".csv";
$res_db = $_SESSION[$file_name];
$cols= count($res_db[0]);
$contents="";

if ($utype == "2" || $utype == "3")
{
$contents = "Account Name\tAccount#\tEmployee Name\tEmail\tContact No\tAddress\tCity\tState\tzip\n";
}
else
{
    $contents = "Vendor Name\tEmployee Name\tEmail\tRole\tContact No\tAddress\tCity\tState\tzip\n";

}
foreach($res_db AS $key => $value)
    {
        for($i=0;$i<$cols;$i++)
        {
            if ($utype == "2" || $utype == "3") {
                $col1 = $value['Account Name'];
                $col2 = $value['Account#'];
                $col3 = $value['Employee Name'];
                $col4 = $value['Email'];
                $col5 = $value['Contact no'];
                $col6 = $value['Address'];
                $col7 = $value['city'];
                $col8 = $value['state'];
                $col9 = $value['zip'];
            }
            else
            {
                $col1 = $value['Vendor Name'];
                $col2 = $value['Employee Name'];
                $col3 = $value['Email'];
                $col4 = $value['Role'];
                $col5 = $value['Contact no'];
                $col6 = $value['Address'];
                $col7 = $value['city'];
                $col8 = $value['state'];
                $col9 = $value['zip'];

            }
        }
        $contents .= $col1 . "\t" . $col2 . "\t" . $col3 . "\t" . $col4 . "\t" . $col5 . "\t" . $col6 . "\t" . $col7 . "\t" . $col8 . "\t" . $col9  . "\n";


        // $contents .= implode('\t',$value)."\n";
    }



ob_get_clean();
header('Content-type: application/ms-csv');
header('Content-Disposition: attachment; filename=' . $filename);
/*
header("Content-Disposition: attachment; filename=\"$filename\"");
header("Content-Type: application/vnd.ms-excel");*/

echo $contents;
exit;
?>