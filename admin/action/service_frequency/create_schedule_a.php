<?

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'schedule.class.php');
$scheduleObj = new schedule();

$MODE=$_REQUEST['mode'];
if ($MODE == 'get_emp') {
ob_get_clean();
#pr($_REQUEST);exit;
$iDistributorId = $_REQUEST['distributor_id'];
$selected = $_REQUEST['selected'];
$DATA = $userObj->select_employee($iDistributorId);

$HTML = '<select class="form-control" name="iEmployeeId" id="iEmployeeId">';
    if (count($DATA) > 0) {
    $HTML .= '<option selected disabled>- Select Employee -</option>';
    foreach ($DATA as $emp) {
    $SELECTED = ($selected == $emp['iEmployeeId']) ? 'selected' : '';
    $HTML .= '<option value="' . $emp['iEmployeeId']  . '" ' . $SELECTED . '>' . $emp['vFirstName'] ." ".$emp ['vLastName'] .'</option>';
    }
    } else {
    $HTML .= '<option disabled> No Data Found. </option>';
    }
    $HTML .= '</select>';
echo $HTML;
exit;
}

else if($MODE=="add")
{
    $rid=$_REQUEST['retailer'];

    $sch = $scheduleObj->get_schedule($_POST['iEmployeeId'], $rid);
    if (count($sch) > 0)
    {
        $_SESSION['schedule_error'] = "Schedule For this employee already exists,You can update his/her Schedule.";
        $generalfuncobj->func_set_temp_sess_msg("Schedule For this employee already exists");
        header("Location:index.php?file=sf-schedulelist");
        exit;
    } else {
        $scheduleObj->setiRetailerId($rid);
        $scheduleObj->setiDistributorId($_POST['vendor']);
        $scheduleObj->setiEmployeeId($_POST['iEmployeeId']);
        $scheduleObj->seteSchedule($_POST['schedule']);
        $scheduleObj->seteStatus("1");
        $scheduleObj->setiDtAdded(strtotime(gmdate('Y-m-d H:i:s')));
        $scheduleObj->setiDtUpdated(strtotime(gmdate('Y-m-d H:i:s')));
        if (isset($_POST['schedule']) && $_POST['schedule'] == "2 days a week") {
            $days = $_POST['day'] . "," . $_POST['day2'];
            $scheduleObj->settDays($days);
        } else {
            $scheduleObj->settDays($_POST['day']);
        }
        $iScheduleId = $scheduleObj->insert();
        $generalfuncobj->func_set_temp_sess_msg("Schedule Details Added Successfully");
        header("Location:index.php?file=sf-schedulelist");
        exit;
    }
}

else if($MODE=="edit")
{
    $sid=$_REQUEST['iScheduleId'];
    $rid = $_REQUEST['retailer'];
    $sch = $scheduleObj->get_schedule($_POST['iEmployeeId'], $rid);

    if (count($sch) >= 1)
    {
        $scheduleObj->setiDistributorId($_POST['vendor']);
        $scheduleObj->setiEmployeeId($_POST['iEmployeeId']);
        $scheduleObj->seteSchedule($_POST['schedule']);
        $scheduleObj->setiDtUpdated(strtotime(gmdate('Y-m-d H:i:s')));
        if (isset($_POST['schedule']) && $_POST['schedule'] == "2 days a week") {
            $days = $_POST['day'] . "," . $_POST['day2'];
            $scheduleObj->settDays($days);
        } else {
            $scheduleObj->settDays($_POST['day']);
        }
        $ssid = $sch[0]['iScheduleId'];
        $scheduleObj->update_schedule($ssid);
    } else {
        $scheduleObj->setiDistributorId($_POST['vendor']);
        $scheduleObj->setiEmployeeId($_POST['iEmployeeId']);
        $scheduleObj->seteSchedule($_POST['schedule']);
        $scheduleObj->setiDtUpdated(strtotime(gmdate('Y-m-d H:i:s')));
        if (isset($_POST['schedule']) && $_POST['schedule'] != "") {
            $days = $_POST['day'] . "," . $_POST['day2'];
            $scheduleObj->settDays($days);
        } else {
            $scheduleObj->settDays($_POST['day']);
        }
        $scheduleObj->update_schedule($sid);
    }
    $generalfuncobj->func_set_temp_sess_msg("Schedule Details Updated Successfully");
    header("Location:index.php?file=sf-schedulelist");
    exit;
}

if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $exe_del = "Update schedule set eStatus='2' where iScheduleId in ($ID)";
        $obj->sql_query($exe_del);


        $generalfuncobj->func_set_temp_sess_msg("Schedule Details Deleted Successfully");
        header("Location:index.php?file=sf-schedulelist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $exe_del = "Update schedule set eStatus='2' where iScheduleId in ($ID)";
            $obj->sql_query($exe_del);
        }

        $generalfuncobj->func_set_temp_sess_msg("Schedule Details Deleted Successfully");
        header("Location:index.php?file=sf-schedulelist");
        exit;
    }
}



