<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 6/4/15
 */

print_r($_REQUEST);

include_once($inc_class_path . 'feedback.class.php');

$feedback = new feedback();
$MODE = $_REQUEST['mode'];

/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $admin->setvFirstName($_REQUEST['vFirstName']);
    $admin->setvLastName($_REQUEST['vLastName']);
    $admin->setvEmail($_REQUEST['vEmail']);
    $admin->setvPasswd(base64_encode($_REQUEST['vPasswd']));
    $admin->setdtAdded(strtotime(gmdate('Y-m-d H:i:s')));
    $admin->setdtUpdated('0');
    $admin->setdtLastLogin('0');
    $admin->setvLastLoginIP('');
    $admin->seteStatus($_REQUEST['eStatus']);

    $admin->insert();
    $generalfuncobj->func_set_temp_sess_msg("Admin Details Added Successfully");
    header("Location:index.php?file=sc-adminlist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $admin->setvFirstName($_REQUEST['vFirstName']);
    $admin->setvLastName($_REQUEST['vLastName']);
    $admin->setvEmail($_REQUEST['vEmail']);
    $admin->setvPasswd(base64_encode($_REQUEST['vPasswd']));
    $admin->setdtUpdated(strtotime(gmdate('Y-m-d H:i:s')));
    $admin->seteStatus($_REQUEST['eStatus']);

    $admin->update($_REQUEST['iAdminId']);

    $_SESSION['SC_LOGIN']['vFirstName'] = $_REQUEST['vFirstName'];
    $_SESSION['SC_LOGIN']['vLastName'] = $_REQUEST['vLastName'];

    $generalfuncobj->func_set_temp_sess_msg("Admin Details <b>Update</b> Successfully");
    header("Location:index.php?file=sc-adminlist");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];

        $exe_del = "Update feedback set eStatus='3' where iFeedbackId in ($ID)";
        $obj->sql_query($exe_del);
        $generalfuncobj->func_set_temp_sess_msg("Feedback Details Deleted Successfully");
        header("Location:index.php?file=f-feedback");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $exe_del = "Update feedback set eStatus='3' where iFeedbackId in ($ID)";
            $obj->sql_query($exe_del);
        }

        $generalfuncobj->func_set_temp_sess_msg("Feedback Details Deleted Successfully");
        header("Location:index.php?file=f-feedback");
        exit;
    }
}

/* Active/Inactive Mode */
if ($MODE == 'active' || $MODE == 'inactive') {

        $status = ($MODE=="active") ? '1' : '0';

        foreach ($_REQUEST['delete'] as $ID) {
          echo  $exe_del = "Update feedback set eStatus='$status' where iFeedbackId in ($ID)";
            $obj->sql_query($exe_del);

        }

        $generalfuncobj->func_set_temp_sess_msg("Feedback Details $MODE Successfully");
        header("Location:index.php?file=f-feedback");
        exit;

}

