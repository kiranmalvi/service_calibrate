<?php
//error_reporting(E_ALL);

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();

include_once($inc_class_path . 'user_security_question.class.php');
$user_securityObj = new user_security_question();

include_once($inc_class_path . 'user_role.class.php');

$user_role = new user_role();

include_once($inc_class_path . 'retailer_detail.class.php');
$retailer_detailObj = new retailer_detail();

$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$utype = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();

$mode = $_REQUEST['mode'];
$iId = $_REQUEST['iId'];

$type = $_REQUEST['type'];

$iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));



if($mode=="recurring")
{
    $recurring=$_REQUEST['recurring'];
    $subscribe=$_REQUEST['subscribe'];
    $date=strtotime(date('d-m-Y H:i:s'));

    $recur = "update user set eStatus='$subscribe',ePlanType='$recurring',iDtAdded='$date' where iUserId in ($iUserId)";
    $sm = $obj->sql_query($recur);

    if($subscribe=="0")
    {

        unset($_SESSION['SC_LOGIN']);
        header('location:../login.php');
        exit;
    }
    else
    {

        header("Location: index.php?file=su-recurring");
        exit;
    }
}

if ($mode == "jpg_chk") {
    ob_get_clean();
    //echo "heloloo";
    $id = $_REQUEST['id'];
    $imageFileType = pathinfo($id, PATHINFO_EXTENSION);
    if ($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG"
        && $imageFileType != "gif" && $imageFileType != "GIF" && $imageFileType != "bmp" && $imageFileType != "BMP"
    ) {
        ?>
        <span style="color:maroon;"
              id="error_msg"><?php echo "Sorry, only JPG, JPEG, PNG, GIF & BMP files are allowed."; ?></span>
        <?php

        //  $uploadOk = 0;
    } else {
        ?>
        <span style="" id="error_msg"><?php echo $id; ?></span>
    <?php

    }

    exit;

}
if ($mode == 'upload_CSV1') {
    echo "in";

    $parentid = $_REQUEST['iId'];
    $counter=0;
    $fail=0;
    $sql_p = $userObj->select($parentid);
    $photopath = $upload_image_path . 'CSV/';
    $vphoto = $_FILES["CSV"]["tmp_name"];
    $vphoto_name = $_FILES["CSV"]["name"];
    $vphoto_type = $_FILES["CSV"]["type"];
    $prefix=date('Ymd');
    $image_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);
    @chmod($upload_image_path . 'CSV/' . $image_arr[0], 0777);
    if (($handle = fopen($upload_image_path . 'CSV/' . $image_arr[0], "r")) !== FALSE) {
        $CSV = 0;
        while (!feof($handle)) {
            if ($CSV != 0) {

                $data = fgetcsv($handle);
                $currentcount = 0;

                if($utype=="4"||$utype=="5"||$utype=="6")
                {


                    $col1 = $data[0];     //  Account Name
                    $col11 = $data[1];     //  Account #
                    $col2 = $data[2];     //Email
                    $col3 = $data[3];     //Contact
                    $col4 = $data[4];     //	First Name
                    $col5 = $data[5];     //	Last Name
                    $col6 = $data[6];     //Address
                    $col7 = $data[7];     //City
                    $col8 = $data[8];     //State
                    $col9 = $data[9];     //country*/
                    $col10 = $data[10];    //Zip
                }
                else
                {

                    $col1 = $data[0];     //  Account Name
                    $col2 = $data[1];     //Email
                    $col3 = $data[2];     //Contact
                    $col4 = $data[3];     //	First Name
                    $col5 = $data[4];     //	Last Name
                    $col6 = $data[5];     //Address
                    $col7 = $data[6];     //City
                    $col8 = $data[7];     //State
                    $col9 = $data[8];     //country*/
                    $col10 = $data[9];    //Zip

                }
                if ($utype == 4) {
                    // $col11 = $data[10];//account#
                    $col50 = $data[11];   //preferable from
                    $col51 = $data[12];//preferable to
                    $col30 = $data[13];//night delivery
                    $col111 = strtolower($col30);
                    if ($col111 == 'yes') {
                        $col30 = 1;
                    } else if ($col111 == 'no') {
                        $col30 = 0;
                    }
                    else
                    {
                        $col30 = 2;
                    }

                }elseif ($utype == 5) {
                    //  $col11 = $data[10];//account#
                    $col52 = $data[11];   //No of Store*/
                    $col50 = $data[12];   //preferable from
                    $col51 = $data[13];   //preferable to
                    $col30 = $data[15];//night delivery
                    $col111 = strtolower($col30);
                    if ($col111 == 'yes') {
                        $col30 = 1;
                    } else if ($col111 == 'no') {
                        $col30 = 0;

                    }
                    else
                    {
                        $col30 = 2;
                    }

                }elseif ($utype == 6) {
                    //  $col11 = $data[10];//account#
                    $col50 = $data[11];   //preferable from
                    $col51 = $data[12];   //Preferable To
                    $col53 = $data[13];   //Preferable from hour
                    $col54 = $data[13];   //Preferable To hour
                    $col30 = $data[14];//night delivery
                    $col111 = strtolower($col30);
                    if ($col111 == 'yes') {
                        $col30 = 1;
                    } else if ($col111 == 'no') {
                        $col30 = 0;

                    }
                    else
                    {
                        $col30 = 2;
                    }


                }
                elseif ($utype == 2 || $utype == 3) {
                    $col11 = $data[10]; //role
                }

                $location = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address='.$col10.'&sensor=true");
                $adrs = json_decode($location);
                $city = $adrs->results[0]->address_components[1]->long_name;
                $state = $adrs->results[0]->address_components[3]->long_name;
                $country = $adrs->results[0]->address_components[4]->long_name;
                if ($city == '' || $state == '' || $country == '') {
                    $col7 = $col7;      //City
                    $col8 = $col8;   //State
                    $col9 = $col9; //country
                } else {
                    $col7 = $city;       //City
                    $col8 = $state;   //State
                    $col9 = $country; //country
                }

                $col15 = $sql_p[0]['iUserTypeId'];
                $col16 = $sql_p[0]['iPlanId'];
                $col17 = $sql_p[0]['iIndustriesId'];
                $col18 = $sql_p[0]['iCategoryId'];
                $col21 = $sql_p[0]['iExpireTime'];
                $col22 = $sql_p[0]['eHowAboutUs'];
                $col23 = $sql_p[0]['eActivePaidPlan'];
                $col24 = $sql_p[0]['ePlanType'];
                $col25 = $sql_p[0]['fAmountPaid'];

                //get lat long from address
                $address = $col6;
                $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=true');
                $coordinates = json_decode($coordinates);
                $lat[0] = $coordinates->results[0]->geometry->location->lat;
                $long[0] = $coordinates->results[0]->geometry->location->lng;

                $name = $generalfuncobj->get_unique("user", "vStoreUniqueId", $col1);
                $email = $generalfuncobj->get_unique("user", "vEmail", $col2);


                if ($name) {
                    $userObj->setvStoreUniqueId($col1);
                }
                else
                {
                    $currentcount = 1;
                }

                if ($email) {
                    $userObj->setvEmail($col2);
                } else {
                    $currentcount = 1;
                }
                echo $currentcount;


                if($currentcount==0)
                {

                    $m_store = "select vComment, iStoreCount from user  where  iUserId='$parentid'";
                    $maxstore = $obj->sql_query($m_store);
                    $maxstore1 = $maxstore[0]['vComment'];

                    $storecount = $maxstore[0]['iStoreCount'];
                    $currentavlstore=$maxstore1-$storecount;



                    if ($storecount < $maxstore1) {
                        if($col2!="Email") {
                            if ($col1 != '' && $col2 != '' && $col3 != '' && $col4 != '' && $col5 != '' && $col6 != '' && $col7 != '' && $col8 != '' && $col9 != '' && $col10 != '') {

                                $userObj->setvStoreUniqueId($col1);
                                $userObj->setvEmail($col2);
                                $userObj->setvContact($col3);
                                $userObj->setvFirstName($col4);
                                $userObj->setvLastName($col5);
                                $userObj->setvAddress($col6);
                                $userObj->setvCity($col7);
                                $userObj->setvState($col8);
                                $userObj->setiCountryId($col9);
                                $userObj->setvZip($col10);
                                $userObj->setNumberOfStore($col52);
                                $NUMBER = $generalfuncobj->generateNumericUniqueToken(6);
                                $userObj->setvPassword(md5($NUMBER));
                                $userObj->seteStatus("1");
                                $userObj->setiDtAdded(strtotime(gmdate('Y-m-d H:i:s')));
                                $userObj->setiParentId($parentid);
                                $userObj->setiUserTypeId($utype);
                                $userObj->setiUserTypeId($col15);
                                $userObj->setiPlanId($col16);
                                $userObj->setiIndustriesId($col17);
                                $userObj->setiCategoryId($col18);
                                $userObj->setvLatitude($lat[0]);
                                $userObj->setvLongitude($long[0]);
                                $userObj->setiExpireTime($col21);
                                $userObj->seteHowAboutUs($col22);
                                $userObj->seteActivePaidPlan($col23);
                                $userObj->setePlanType($col24);
                                $userObj->setfAmountPaid($col25);
                                $userObj->setiDeliveryHoursFrom($col53);
                                $userObj->setiDeliveryHoursTo($col54);
                                $userObj->setiPreferDeliveryTimeFrom($col50);
                                $userObj->setiPreferDeliveryTimeTo($col51);
                                $userObj->seteNightDelivery($col30);
                                $userObj->setvStoreName($col11);
                                $userObj->seteNightDelivery($col11);
                                if ($utype == 2 || $utype == 3) {

                                    if ($col11 != '') {
                                        echo "->All->";
                                        $rolee = "select vName,iUserRoleId from user_role where vName='$col11' AND iUserTypeId='$utype'";
                                        $roleexist = $obj->sql_query($rolee);
                                        if (count($roleexist) > 0) {
                                            $userObj->setiUserRoleId($roleexist[0]['iUserRoleId']);
                                        } else {
                                            echo "else";
                                            $user_role->setvName($col11);
                                            $user_role->seteStatus("1");
                                            $user_role->setiUserTypeId($utype);
                                            $user_role->setiDtAdded($iDtAdded);
                                            $iUserRoleId = $user_role->insert();
                                            $userObj->setiUserRoleId($iUserRoleId);
                                        }
                                        $iUserId = $userObj->upload_csv();
                                        $counter++;
                                        echo "->Done->";
                                        $DATA = array('email' => $col2, 'password' => $NUMBER);
                                        echo   $generalfuncobj->get_SC_SEND_mail('csv_register', $DATA, $col2, 'Welcome To Service Calibrate');

                                        $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($parentid)";
                                        $m = $obj->sql_query($store_count);


                                    }
                                    else {
                                        echo "fail";

                                        $fail++;

                                    }} else if ($utype == 4) {

                                    if ($col11 != '' && $col50 != '' && $col51 != '' && $col30 != '2') {
                                        $iUserId = $userObj->upload_csv();
                                        echo $iUserId;
                                        $vStoreUniqueId = $sql_p[0]['vStoreUniqueId'];
                                        $qr = $retailer_detailObj->get_detail_by_retailer($sql_p[0]['iUserId']);
                                        $qrcode = $qr[0]['vOriginal_code'];

                                        $account = substr($qrcode, 0, 3);
                                        $userObj->GENERATE_SC_QR_CODE($iUserId, $account);
                                        echo $counter++;
                                        $DATA = array('email' => $col2, 'password' => $NUMBER);
                                        echo     $generalfuncobj->get_SC_SEND_mail('csv_register', $DATA, $col2, 'Welcome To Service Calibrate');

                                        $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($parentid)";
                                        $m = $obj->sql_query($store_count);
                                    } else {
                                        $fail++;
                                    }
                                } else if ($utype == 5)
                                {
                                    if ($col11 != '' && $col50 != '' && $col51 != '' && $col52 != '') {
                                        $iUserId = $userObj->upload_csv();
                                        $vStoreUniqueId = $sql_p[0]['vStoreUniqueId'];
                                        $qr = $retailer_detailObj->get_detail_by_retailer($sql_p[0]['iUserId']);
                                        $qrcode = $qr[0]['vOriginal_code'];

                                        $account = substr($qrcode, 0, 3);
                                        $userObj->GENERATE_SC_QR_CODE($iUserId, $account);
                                        $counter++;
                                        $DATA = array('email' => $col2, 'password' => $NUMBER);
                                        echo   $generalfuncobj->get_SC_SEND_mail('csv_register', $DATA, $col2, 'Welcome To Service Calibrate');

                                        $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($parentid)";
                                        $m = $obj->sql_query($store_count);
                                    } else {
                                        $fail++;
                                    }
                                }
                                else if ($utype == 6)
                                {

                                    if ($col11 != '' && $col50 != '' && $col51 != '' && $col53 != '' && $col54 != '') {
                                        $iUserId = $userObj->upload_csv();
                                        $vStoreUniqueId = $sql_p[0]['vStoreUniqueId'];
                                        $qr = $retailer_detailObj->get_detail_by_retailer($sql_p[0]['iUserId']);
                                        $qrcode = $qr[0]['vOriginal_code'];

                                        $account = substr($qrcode, 0, 3);
                                        $userObj->GENERATE_SC_QR_CODE($iUserId, $account);
                                        $counter++;
                                        $DATA = array('email' => $col2, 'password' => $NUMBER);
                                        $generalfuncobj->get_SC_SEND_mail('csv_register', $DATA, $col2, 'Welcome To Service Calibrate');
                                        $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($parentid)";
                                        $m = $obj->sql_query($store_count);
                                    } else {
                                        $fail++;
                                    }
                                }
                            }
                        }
                        else
                        {
                            $fail++;
                        }

                    }
                    else
                    {
                        $currentstore= $sql_p[0]['iStoreCount'];
                        if($counter>0) {

                            $_SESSION['upload_csvmsg'] = $counter . " Account/Users uploaded sucessfully.To add additional accounts please email  <a href='mailto:sales@servicecalibrate.com'>sales@servicecalibrate.com</a>";
                            $_SESSION['upload_cls'] = "alert-success";
                        }
                        else
                        {
                            $_SESSION['upload_csvmsg'] = "To add additional accounts please email  <a href='mailto:sales@servicecalibrate.com'>sales@servicecalibrate.com</a>";
                            // $_SESSION['upload_csvmsg'] = "To ";
                            $_SESSION['upload_cls'] = "alert-danger";
                        }
                        header("Location: index.php?file=su-manage_store");
                        exit;
                    }
                }
                $fail++;
            }
            $CSV++;


        }
    }
    else
    {  $_SESSION['upload_csvmsg'] = "File is not uploaded succesfully";
        header("Location: index.php?file=su-manage_store");
    }



    if($counter>0)
    {
        $_SESSION['upload_csvmsg']=$counter." Account/Users have been uploaded successfully.";
        $_SESSION['upload_cls']="alert-success";
    }
    else if($fail>=0)
    {
        $_SESSION['upload_csvmsg']="Sorry, Please check the data on excel and try again";
        $_SESSION['upload_cls']="alert-danger";
    }
    header("Location: index.php?file=su-manage_store");
}
if ($mode == 'upload_CSV') {


    // pr($_FILES);

    $parentid = $_REQUEST['iId'];
    $type = $_REQUEST['type'];
    $maxstore = $_REQUEST['max'];
    $counter = 0;
    $fail = 0;
    $existcount = "0";
    // echo "'$parentid'";
    $sql_p = $userObj->select($parentid);
    //pr($sql_p);

    $exist = array();

    $photopath = $upload_image_path . 'CSV/';
    $vphoto = $_FILES["CSV"]["tmp_name"];
    $vphoto_name = $_FILES["CSV"]["name"];
    $vphoto_type = $_FILES["CSV"]["type"];
    $prefix=date('Ymd');
    $image_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);

    @chmod($upload_image_path . 'CSV/' . $image_arr[0], 0777);

    if (($handle = fopen($upload_image_path . 'CSV/' . $image_arr[0], "r")) !== FALSE) {
        $temp = 0;
        $CSV=0;
        while (!feof($handle)){
            $test = array();
            $data = fgetcsv($handle);
            //pr($data);

            $temp = 0;



            if ($CSV != 0) {
                $currentcount = 0;

                $col1 = $data[0];     //  Account Name
                $col2 = $data[1];     //Email
                $col3 = $data[2];     //Contact
                $col4 = $data[3];     //	First Name
                $col5 = $data[4];     //	Last Name
                $col6 = $data[5];     //Address
                $col7 = $data[6];     //City
                $col8 = $data[7];     //State
                $col9 = $data[8];     //country*/
                $col10 = $data[9];    //Zip
//                $col11 = $data[10];//Account ID
//                $col12 = $data[11];
//                $col13 = $data[12];
//                $col14 = $data[13];
//                $col15 = $data[14];
//                $col16 = $data[15];
                //   pr($data);exit;

                if ($utype == 4) {
                    $col11 = $data[10];//account#
                    $userObj->setvStoreName($col11);
                    $col50 = $data[11];   //preferable from
                    $col51 = $data[12];//preferable to
                    $col30 = $data[13];//night delivery
                    $col111 = strtolower($col30);
                    if ($col111 == 'yes') {
                        $col30 = 1;
                    } else if ($col111 == 'no') {
                        $col30 = 0;
                    }
                } elseif ($utype == 5) {
                    $col11 = $data[10];//account#
                    $userObj->setvStoreName($col11);

                    //  $col11=$data[10];
                    $col52 = $data[11];   //No of Store*/
                    $col50 = $data[12];   //preferable from
                    $col51 = $data[13];   //preferable to
                    $col30 = $data[15];//night delivery
                    $col111 = strtolower($col30);
                    if ($col111 == 'yes') {
                        $col30 = 1;
                    } else if ($col111 == 'no') {
                        $col30 = 0;

                    }
                } elseif ($utype == 6) {
                    $col11 = $data[10];//account#
                    $userObj->setvStoreName($col11);


                    $col50 = $data[11];   //preferable from
                    $col51 = $data[12];   //Preferable To
                    $col53 = $data[13];   //Preferable from hour
                    $col54 = $data[13];   //Preferable To hour
                    $col30 = $data[14];//night delivery
                    $col111 = strtolower($col30);
                    if ($col111 == 'yes') {
                        $col30 = 1;
                    } else if ($col111 == 'no') {
                        $col30 = 0;

                    }

                } elseif ($utype == 2 || $utype == 3) {
                    $col11 = $data[10];
                    if($col11!="")
                    {
                        $rolee = "select vName,iUserRoleId from user_role where vName='$col11' AND iUserTypeId='$utype'";
                        $roleexist = $obj->sql_query($rolee);
                        if (count($roleexist) > 0) {
                            $userObj->setiUserRoleId($roleexist[0]['iUserRoleId']);
                        } else {
                            $user_role->setvName($col11);
                            $user_role->seteStatus("1");
                            $user_role->setiUserTypeId($utype);
                            $user_role->setiDtAdded($iDtAdded);
                            $iUserRoleId = $user_role->insert();
                            $userObj->setiUserRoleId($iUserRoleId);
                        }}
                }

                $location = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address='.$col10.'&sensor=true");
                $adrs = json_decode($location);
                $city = $adrs->results[0]->address_components[1]->long_name;
                $state = $adrs->results[0]->address_components[3]->long_name;
                $country = $adrs->results[0]->address_components[4]->long_name;
                if ($city == '' || $state == '' || $country == '') {
                    $col7 = $data[6];       //City
                    $col8 = $data[7];   //State
                    $col9 = $data[8]; //country
                } else {
                    $col7 = $city;       //City
                    $col8 = $state;   //State
                    $col9 = $country; //country
                }
                // $col14=$sql_p[0]['iUserRoleId'];
                $col15 = $sql_p[0]['iUserTypeId'];
                $col16 = $sql_p[0]['iPlanId'];
                $col17 = $sql_p[0]['iIndustriesId'];
                $col18 = $sql_p[0]['iCategoryId'];
                // $col19=$sql_d[0]['vLatitude'];
                //  $col20=$sql_d[0]['vLongitude'];
                $col21 = $sql_p[0]['iExpireTime'];
                $col22 = $sql_p[0]['eHowAboutUs'];
                $col23 = $sql_p[0]['eActivePaidPlan'];
                $col24 = $sql_p[0]['ePlanType'];
                $col25 = $sql_p[0]['fAmountPaid'];
                // $col26=$sql_p[0]['iDeliveryHoursFrom'];
                //$col27=$sql_p[0]['iDeliveryHoursTo'];
                //$col28=$sql_p[0]['iPreferDeliveryTimeFrom'];
                //$col29=$sql_p[0]['iPreferDeliveryTimeTo'];
                //$col30=$sql_p[0]['eNightDelivery'];

                //  $col33=$sql_p[0]['iStoreCount'];exit;
                // pr($sql_p);

                $address = $col7;
                $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=true');
                $coordinates = json_decode($coordinates);
                $lat[0] = $coordinates->results[0]->geometry->location->lat;
                $long[0] = $coordinates->results[0]->geometry->location->lng;

                $name = $generalfuncobj->get_unique("user", "vStoreUniqueId", $col1);
                $email = $generalfuncobj->get_unique("user", "vEmail", $col2);


                if ($name) {
                    $userObj->setvStoreUniqueId($col1);
                }
                else
                {
                    $currentcount = 1;
                }

                if ($email) {
                    $userObj->setvEmail($col2);
                } else {
                    $currentcount = 1;
                }



                $userObj->setvStoreUniqueId($col1);
                $userObj->setvEmail($col2);
                $userObj->setvContact($col3);
                $userObj->setvFirstName($col4);
                $userObj->setvLastName($col5);
                $userObj->setvAddress($col6);
                $userObj->setvCity($col7);
                $userObj->setvState($col8);
                $userObj->setiCountryId($col9);
                $userObj->setvZip($col10);

                $userObj->setNumberOfStore($col52);
                $NUMBER = $generalfuncobj->generateNumericUniqueToken(6);
                $userObj->setvPassword(md5($NUMBER));
                $userObj->seteStatus("1");
                $userObj->setiDtAdded(strtotime(gmdate('Y-m-d H:i:s')));
                $userObj->setiParentId($parentid);
                $userObj->setiUserTypeId($type);
                //  $userObj->setiUserRoleId($col14);
                $userObj->setiUserTypeId($col15);
                $userObj->setiPlanId($col16);
                $userObj->setiIndustriesId($col17);
                $userObj->setiCategoryId($col18);
                $userObj->setvLatitude($lat[0]);
                $userObj->setvLongitude($long[0]);
                $userObj->setiExpireTime($col21);
                $userObj->seteHowAboutUs($col22);
                $userObj->seteActivePaidPlan($col23);
                $userObj->setePlanType($col24);
                $userObj->setfAmountPaid($col25);
                $userObj->setiDeliveryHoursFrom($col53);
                $userObj->setiDeliveryHoursTo($col54);
                $userObj->setiPreferDeliveryTimeFrom($col50);
                $userObj->setiPreferDeliveryTimeTo($col51);
                $userObj->seteNightDelivery($col30);
                if ($currentcount == 0) {

                    $temp = 1;
                    $m_store = "select p.iPlanId,p.iToStore as maxstore,u.iPlanId from user u left join plan as p on u.iplanId=p.iplanId where u.iUserId='$parentid'";
                    $maxstore = $obj->sql_query($m_store);

                    $maxstore1 = $maxstore[0]['maxstore'];
                    $storecount = $sql_p[0]['iStoreCount'];


                    if ($storecount < $maxstore1) {


                        if ($utype == 2 || $utype == 3) {
                            if ($col1 != '' && $col2 != '' && $col3 != '' && $col4 != '' && $col5 != '' && $col6 != '' && $col7 != '' && $col8 != '' && $col9 != '' && $col10 != '' && $col11 != '') {

                                $iUserId = $userObj->upload_csv();
                                // echo $counter;
                                $counter++;

                            } else {

                                $fail++;
                            }

                        } elseif ($utype == 4) {

                            // if($col1 != '' && $col2 != '' && $col3 != '' && $col4 != '' && $col5 != '' && $col6 != '' && $col7 != '' && $col8 != '' && $col9 != '' && $col10 != '' && $col11 != '' && $col50 != ''&& $col51 != ''&& $col30 != '' )
                            if ($col1 != '' && $col2 != '') {

                                $iUserId = $userObj->upload_csv();
                                $counter++;

                            } else {

                                $fail++;


                                // $generalfuncobj->func_set_temp_sess_msg("No Data Updated.");

                                // header("Location:index.php?file=su-manage_store");

                            }

                        } elseif ($utype == 5) {
                            if ($col1 != '' && $col2 != '' && $col3 != '' && $col4 != '' && $col5 != '' && $col6 != '' && $col7 != '' && $col8 != '' && $col9 != '' && $col10 != '' && $col11 != '' && $col50 != '' && $col51 != '' && $col52 != '') {
                                $iUserId = $userObj->upload_csv();
                                $counter++;
                            } else {
                                $fail++;

                                //$generalfuncobj->func_set_temp_sess_msg("No Data Updated.");

                                // header("Location:index.php?file=su-manage_store");
                            }

                        } elseif ($utype == 6) {
                            if ($col1 != '' && $col2 != '' && $col3 != '' && $col4 != '' && $col5 != '' && $col6 != '' && $col7 != '' && $col8 != '' && $col9 != '' && $col10 != '' && $col11 != '' && $col50 != '' && $col51 != '' && $col53 != '' && $col54 != '') {
                                $iUserId = $userObj->upload_csv();
                                $counter++;
                            } else {
                                $fail++;

                                //$generalfuncobj->func_set_temp_sess_msg("No Data Updated.");

                                // header("Location:index.php?file=su-manage_store");

                            }

                        }

                        $user_settingsObj->setiUserId($iUserId);
                        $user_settingsObj->seteNotification('1');
                        $iUserSettingsId = $user_settingsObj->insert();

                        if ($type == "4" || $type == "5" || $type == "6") {
                            $vStoreUniqueId = $sql_p[0]['vStoreUniqueId'];
                            $qr = $retailer_detailObj->get_detail_by_retailer($sql_p[0]['iUserId']);
                            $qrcode = $qr[0]['vOriginal_code'];

                            $account = substr($qrcode, 0, 3);
                            $userObj->GENERATE_SC_QR_CODE($iUserId, $account);
                        }
                        $DATA = array('name' => $col1, 'password' => $NUMBER);
                        //echo   $generalfuncobj->get_SC_SEND_mail('csv_register', $DATA, $col2, 'Welcome To Service Calibrate');
                        $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($parentid)";
                        $m = $obj->sql_query($store_count);
                    } else {


                        $_SESSION['upload_csvmsg'] = "You have reached to your limit of accounts";
                    }
                } else {
                    $fail++;

                    //exit;
                }


            }


            $CSV++;

        }



        $_SESSION['upload_csvmsg'] = $counter . " Data sucessfully and " . $fail . " Data not inserted";
        header("Location: index.php?file=su-manage_store");
        fclose($handle);

        exit;

    } else {
        $_SESSION['upload_csvmsg'] = "File is not uploaded successfully";

        header("Location: index.php?file=su-manage_store");
        exit;
    }


    header("Location: index.php?file=su-manage_store");
    exit;
}



if ($mode == "add") {


    $sql_p = $userObj->select($iUserId);

    $userObj->seteActivePaidPlan($sql_p[0]['eActivePaidPlan']);
    $userObj->setiExpireTime($sql_p[0]['iExpireTime']);
    $userObj->setePlanType($sql_p[0]['ePlanType']);
    $userObj->setfAmountPaid($sql_p[0]['fAmountPaid']);
    $userObj->setiPlanId($sql_p[0]['iPlanId']);

    $mail = $_POST['email'];
    $mailchk = $userObj->check_exist_mail($mail);
    //pr($mailchk);
    if (count($mailchk) > 0) {
        $generalfuncobj->func_set_temp_sess_msg("This Email Already Exist", "alert-danger", "Error");
        header("Location:index.php?file=su-manage_store&iId=$iId&type=$type");
        exit;
    } else {
        $userObj->setvEmail($mail);
    }

    if ($_FILES["vImage"]["name"] != "") {
        $photopath = $user_image_path;
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];

        //$vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");

        $user_img_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);

        $user_img_name = $user_img_arr[0];
        if ($user_img_name == '') {
            $generalfuncobj->func_set_temp_sess_msg("Please upload pictures only in png,jpg,jpeg,gif or bmp format.", "alert-danger", "Error");
            header("Location:index.php?file=su-manage_store&iId=$iId&type=$type");
            exit;
        } else {

            $userObj->setvImage($user_img_name);
        }
    }

    $userObj->setvFirstName(addslashes($_POST['firstname']));
    $userObj->setvLastName(addslashes($_POST['lastname']));
    $userObj->setiIndustriesId($_POST['industry']);
    $userObj->setiParentId($iUserId);

    $result = $generalfuncobj->get_unique("user", "vStoreUniqueId", $_POST['uniqid']);
    if ($result) {
        $userObj->setvStoreUniqueId(addslashes($_POST['uniqid']));
    } else {
        $generalfuncobj->func_set_temp_sess_msg("Account Name already Exist,Please Enter Another.", "alert-danger", "Error");
        header("Location:index.php?file=su-manage_store&iId=$iId&type=$type");
        exit;
    }

    $userObj->setvAddress(addslashes($_POST['adrs']));

    $address = $_POST['adrs'];
    $city = $_POST['iCityId'];
    $state = $_POST['iStateId'];
    $zip = $_POST['zip'];
    $fulladdress=$address.",".$city.",".$state.",".$zip;

    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($fulladdress) . '&sensor=true');

    $coordinates = json_decode($coordinates);
    $lat[0] = $coordinates->results[0]->geometry->location->lat;
    $long[0] = $coordinates->results[0]->geometry->location->lng;
    $userObj->setvLatitude($lat[0]);
    $userObj->setvLongitude($long[0]);

    $userObj->setvZip($_POST['zip']);
    $userObj->setvCity($_POST['iCityId']);
    $userObj->setvState($_POST['iStateId']);
    $userObj->setiCountryId($_POST['iCountryId']);
    $userObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
    $userObj->seteStatus("1");
    $userObj->setvContact($_POST['contact']);
    $userObj->setvPassword(md5("123456"));
    $userObj->seteHowAboutUs($_POST['aboutus']);
    $userObj->setvReferralEmail($_POST['refmail']);


    if (isset($_POST['refmail'])) {
        $refmail = $_POST['refmail'];
        $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
        $row = $userObj->check_exist_mail($refmail);
        if ($type == "4" || $type == "5" || $type == "6") {
            $point = "200";
        } else {
            $point = "100";
        }

        if (count($row) > 0) {
            $userid = $row[0]['iUserId'];
            if ($referralObj->check_existing_user($userid)) {

                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->update_points($userid, $point);
            } else {

                $referralObj->setiUserId($userid);
                $referralObj->setvPoint($point);
                $referralObj->seteStatus("1");
                $referralObj->setiDtAdded($iDtAdded);
                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->setvReason("Reference");
                $iReffaralId = $referralObj->insert();
            }
        }
    }




    $userObj->setiUserTypeId($utype);
    $userObj->setiUserRoleId($_POST['userrole']);

    /* $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=true');

     $coordinates = json_decode($coordinates);
     $lat[0] = $coordinates->results[0]->geometry->location->lat;
     $long[0] = $coordinates->results[0]->geometry->location->lng;

     $userObj->setvLatitude($lat[0]);
     $userObj->setvLongitude($long[1]);*/


    if ($utype == "4" || $utype == "5" || $utype == "6") {

        $userObj->seteNightDelivery($_POST['nightdelivery']);
        $userObj->setiPreferDeliveryTimeFrom($_POST['deliverytimefrom']);
        $userObj->setiPreferDeliveryTimeTo($_POST['deliverytimeto']);
        $userObj->setiDeliveryHoursTo($_POST['hoursto']);
        $userObj->setiDeliveryHoursFrom($_POST['hoursfrom']);
        $userObj->setiTypeOfStoreId($_POST['storetype']);
        $userObj->setvStoreName($_POST['store']);


    }
    if ($utype == "5") {
        $userObj->setNumberOfStore($_POST['storenum']);

    }




    if ($utype == "2" || $utype == "3") {

        $userObj->setiCategoryId($_POST['category']);
    }


    $isubUserId = $userObj->insert();

    $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($iUserId)";
    $obj->sql_query($store_count);

    $user_settingsObj->setiUserId($isubUserId);
    $user_settingsObj->seteNotification('1');
    $iUserSettingsId = $user_settingsObj->insert();

    if ($type == "4" || $type == "5" || $type == "6") {
        $qr = $retailer_detailObj->get_detail_by_retailer($iUserId);
        $qrcode = $qr[0]['vOriginal_code'];
        $account = substr($qrcode, 0, 3);
        $userObj->GENERATE_SC_QR_CODE($isubUserId, $account);
    }


    /*  $user_securityObj->setiUserId($iUserId);
      $user_securityObj->setvQuestion($_POST['sque']);
      $user_securityObj->settAnswer($_POST['ans']);
      $user_securityObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
      $user_securityObj->seteStatus("1");

      $iUserSecQueId = $user_securityObj->insert();*/

    $generalfuncobj->func_set_temp_sess_msg("Employee Details Added Successfully");

    $vStoreUniqueId = $_POST['uniqid'];
    $DATA = array('name' => $vStoreUniqueId);
    $generalfuncobj->get_SC_SEND_mail('register', $DATA, $mail, 'Welcome To Service Calibrate');

    header("Location:index.php?file=su-manage_store&iId=$iId&type=$type");
    exit;

} else if ($mode == "update") {
    $mail = $_POST['email'];
    $pid = $_REQUEST['pId'];


    $mailchk = $userObj->check_exist_mail_withid($mail, $iId);

    if (count($mailchk) > 0) {
        $generalfuncobj->func_set_temp_sess_msg("This Email Already Exist", "alert-danger", "Error");
        if ($type == '') {
            header("Location:index.php?file=su-manage_store&iId=$iId&type=$type");
            exit;
        } else {
            header("Location:index.php?file=su-sudashboard&iId=$iId&type=$type");
            exit;

        }
        exit;
    } else {
        $userObj->setvEmail($mail);
    }


    if ($_FILES["vImage"]["name"] == "") {
        $userObj->setvImage($_POST['image_hid']);
    } else {
        $photopath = $user_image_path;
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];

        //$vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");

        $user_img_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);

        $user_img_name = $user_img_arr[0];
        if ($user_img_name == '') {
            $generalfuncobj->func_set_temp_sess_msg("Please upload pictures only in png,jpg,jpeg,gif or bmp format.", "alert-danger", "Error");
            if ($type == '') {
                header("Location:index.php?file=su-manage_store&iId=$iId&type=$type");
                exit;
            } else {
                header("Location:index.php?file=su-sudashboard&iId=$iId&type=$type");
                exit;

            }
            exit;
        }


        $userObj->setvImage($user_img_name);
    }
//    $userObj->select($iId);
    $userObj->setvFirstName(addslashes($_POST['firstname']));
    $userObj->setvLastName(addslashes($_POST['lastname']));
    $userObj->setiIndustriesId($_POST['industry']);
    $userObj->setiParentId($pid);

    $result = $generalfuncobj->get_unique("user", "vStoreUniqueId", $_POST['uniqid'], $iId);
    if ($result) {
        $userObj->setvStoreUniqueId(addslashes($_POST['uniqid']));
    } else {
        $generalfuncobj->func_set_temp_sess_msg("Account Name already Exist,Please Enter Another.", "alert-danger", "Error");
        if ($type == '') {
            header("Location:index.php?file=su-manage_store&iId=$iId&type=$type");
            exit;
        } else {
            header("Location:index.php?file=su-sudashboard&iId=$iId&type=$type");
            exit;

        }
        exit;
    }


    $userObj->setvAddress(addslashes($_POST['adrs']));
    $address = $_POST['adrs'];
    $city = $_POST['iCityId'];
    $state = $_POST['iStateId'];
    $zip = $_POST['zip'];
    $fulladdress=$address.",".$city.",".$state.",".$zip;

    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($fulladdress) . '&sensor=true');

    $coordinates = json_decode($coordinates);
    $lat[0] = $coordinates->results[0]->geometry->location->lat;
    $long[0] = $coordinates->results[0]->geometry->location->lng;

    $userObj->setvLatitude($lat[0]);
    $userObj->setvLongitude($long[0]);

    $userObj->setvZip($_POST['zip']);
    $userObj->setvCity($_POST['iCityId']);
    $userObj->setvState($_POST['iStateId']);
    $userObj->setiCountryId($_POST['iCountryId']);
    $userObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
    $userObj->seteStatus("1");
    $userObj->setvContact($_POST['contact']);
    $userObj->setiUserTypeId($utype);
    $userObj->setvEmail($_POST['email']);
    $userObj->setiUserRoleId($_POST['userrole']);


    if ($utype == "4" || $utype == "5" || $utype == "6") {

        $userObj->seteNightDelivery($_POST['nightdelivery']);
        $userObj->setiPreferDeliveryTimeFrom($_POST['deliverytimefrom']);
        $userObj->setiPreferDeliveryTimeTo($_POST['deliverytimeto']);
        $userObj->setiDeliveryHoursTo($_POST['hoursto']);
        $userObj->setiDeliveryHoursFrom($_POST['hoursfrom']);
        $userObj->setiTypeOfStoreId($_POST['storetype']);
        $userObj->setvStoreName($_POST['store']);


    }
    if ($utype == "5") {
        $userObj->setNumberOfStore($_POST['storenum']);
        $userObj->setvComment($_POST['storenum']);

    }


    if ($utype == "2" || $utype == "3") {

        $userObj->setiCategoryId($_POST['category']);
    }


    $userObj->updateuser($iId);
    $generalfuncobj->func_set_temp_sess_msg("Employee Details Updated Successfully");
    if ($type == '') {
        header("Location:index.php?file=su-manage_store&iId=$iId&type=$type");
        exit;
    } else {
        $_SESSION['SC_LOGIN']['USER']['vFirstName']= $_POST['firstname'];
        $_SESSION['SC_LOGIN']['USER']['vLastName']=$_POST['lastname'];
        header("Location:index.php?file=su-sudashboard&iId=$iId&type=$type");
        exit;

    }
    exit;
} else if ($mode == 'delete') {

    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $iId = $_REQUEST['pid'];
        $exe_del = "Update user set eStatus='2' where iUserId in ($ID)";
        $obj->sql_query($exe_del);

        $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>0 THEN iStoreCount -1 ELSE 0 END where iUserId in ($iId)";
        $obj->sql_query($store_count);

        $generalfuncobj->func_set_temp_sess_msg("Employee Details Deleted Successfully");

        header("Location:index.php?file=su-manage_store&iId=$iId&type=$type");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $exe_del = "Update user set eStatus='2' where iUserId in ($ID)";
            $obj->sql_query($exe_del);
            $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>0 THEN iStoreCount -1 ELSE 0 END where iUserId in ($iId)";
            $obj->sql_query($store_count);

        }

        $generalfuncobj->func_set_temp_sess_msg("Employee Details Deleted Successfully");
        header("Location:index.php?file=su-manage_store&iId=$iId&type=$type");
        exit;
    }
} elseif ($mode == "get_zipcode") {
    ob_get_clean();
    $zip = $_REQUEST['zipcode'];
    $location = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address='.$zip.'&sensor=true");
    $adrs = json_decode($location);
    /* $city=$adrs->results[0]->address_components[1]->long_name;
     $state=$adrs->results[0]->address_components[3]->long_name;
     $country=$adrs->results[0]->address_components[4]->long_name;*/


    print_r($location);
    exit;

} elseif ($mode == "upadateplan") {
    $price = $_POST['sprice'];
    $date = strtotime($_POST['sdate']);
    $exe_update = "Update user set fAmountPaid='$price',iExpireTime='$date'  where iUserId='$iId'";


    $obj->sql_query($exe_update);
    $generalfuncobj->func_set_temp_sess_msg("Employee Plan Updated Successfully");
    header("Location:index.php?file=su-sudashboard&iId=$iId");
    exit;


} elseif ($mode == "chk_pass") {
    $old = md5($_REQUEST['oldpass']);
    $newp = md5($_REQUEST['nwpass']);
    $userdata = $userObj->select($iId);
    // pr($userdata);
    if ($old != $userdata[0]['vPassword']) {
        //echo "mathc";exit;
        $generalfuncobj->func_set_temp_sess_msg("Old Password Does Not Match");
        header("Location:index.php?file=su-change_password");


    } else {
        //  echo "not mathc";
        $sql = "update user  set vPassword='$newp'where iUserId='$iUserId'";
        $obj->sql_query($sql);

        $generalfuncobj->func_set_temp_sess_msg("Password Changed Successful");
        header("Location:index.php?file=su-sudashboard");
    }
    exit;


}
?>
<?php
include_once($inc_class_path . 'user.class.php');
$userObj = new user();


include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();
$mode = $_REQUEST['mode'];
echo $mode;
$iId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
if ($mode == "delete_acc") {
    $sql = "update user set eStatus='2' where iuserId='$iId' or iParentId='$iId'";
    $obj->sql_query($sql);
    $generalfuncobj->func_set_temp_sess_msg("Account Delete Successfully");
    header("Location:logout.php");
    exit;
    //  pr($delete_data);
}

?>