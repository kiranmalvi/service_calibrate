<?
include_once($inc_class_path . 'billing_info.class.php');
$BillinginfoObj = new billing_info();

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

$mode=$_REQUEST['mode'];
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$date=strtotime(date('Y-m-d H:i:s'));
if($mode=="edit")
{
    $BillinginfoObj->setvCountry($_REQUEST['country']);
    $BillinginfoObj->setvZip($_REQUEST['zip']);
    $BillinginfoObj->setvCity($_REQUEST['city']);
    $BillinginfoObj->setvState($_REQUEST['state']);
    $BillinginfoObj->setvAddress($_REQUEST['address']);
    $BillinginfoObj->setiDtUpdated($date);

    $bill=$BillinginfoObj->check_user($iUserId);
    if(count($bill)>0)
    {
        $BillinginfoObj->update_adrs($iId);
    }
    else
    {
        $BillinginfoObj->seteStatus("1");
        $BillinginfoObj->setiDtAdded($date);
        $BillinginfoObj->setiUserId($iUserId);
        $BillinginfoObj->insert();
    }
    header("Location: index.php?file=su-payment_info");
}

if($mode=="unsubscribe")
{
    ob_get_clean();
    $unsub = "update user set ePlanType='2' where iUserId in ($iUserId)";
    $obj->sql_query($unsub);
    $msg="You have successfully Unsubscribed to your plan";
    $output = '<div class="col-lg-offset-2 col-lg-10">
                       <button id="subscribe" type="button" class="btn btn-primary sub_unsub_btn" value="1" name="subscribe" onclick="subscribe_unscribe(1,\'subscribe\')">Subscribe</button>
             </div>';
    $response=array("MSG"=>$msg, "OUTPUT" =>$output);
    echo json_encode($response);
    exit;
}

if($mode=="subscribe")
{
    ob_get_clean();
    $sub = "update user set ePlanType='1' where iUserId in ($iUserId)";
    $obj->sql_query($sub);
    $msg="You have successfully Subscribed to your plan";
    $output = '<div class="col-lg-offset-2 col-lg-10">
                       <button id="unsubscribe" type="button" class="btn btn-primary sub_unsub_btn" value="2" name="unsubscribe" onclick="subscribe_unscribe(2,\'unsubscribe\')">Unsubscribe</button>
               </div>';
    $response=array("MSG"=>$msg, "OUTPUT" =>$output);
    echo json_encode($response);
    exit;
}

?>