<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <!--    --><?php /*echo "<pre/>";
        print_R($_REQUEST);
        print_r($_FILES);exit; */
        ?>
<?

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();

include_once($inc_class_path . 'user_security_question.class.php');
$user_securityObj = new user_security_question();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();

include_once($inc_class_path . 'billing_info.class.php');
$BillinginfoObj = new billing_info();

include_once($inc_class_path . 'retailer_detail.class.php');
$retailer_detailObj = new retailer_detail();


include_once($inc_class_path . 'user_role.class.php');

$user_role = new user_role();
$mode = $_REQUEST['mode'];
$iId = $_REQUEST['iId'];
$pId = $_REQUEST['pId'];
$type = $_REQUEST['type'];


if ($mode == "add") {
    $mail = $_POST['email'];
    $mailchk = $userObj->check_exist_mail($mail);
    if (count($mailchk) > 0) {
        $generalfuncobj->func_set_temp_sess_msg("This Email Already Exist", "alert-danger", "Error");
        header("Location:index.php?file=u-useradd&type=$type&mode=add");
        exit;
    } else {
        $userObj->setvEmail($mail);
    }

    if ($_FILES["vImage"]["name"] != "") {
        $photopath = $user_image_path;
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];

        //$vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");

        $user_img_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);

        $user_img_name = $user_img_arr[0];
        if ($user_img_name == '') {
            $generalfuncobj->func_set_temp_sess_msg("Please upload pictures only in png,jpg,jpeg,gif or bmp format.", "alert-danger", "Error");
            header("Location:index.php?file=u-useradd&type=$type&mode=add");
            exit;
        }
        else
        {
            $userObj->setvImage($user_img_name);
        }
    }

    $userObj->setvFirstName(addslashes($_POST['firstname']));
    $userObj->setvLastName(addslashes($_POST['lastname']));
    $userObj->setiIndustriesId($_POST['industry']);
    if($_REQUEST['page']=="sublist")
    {
        $userObj->setiParentId($iId);
    }
    else
    {
        $userObj->setiParentId(0);
    }


    $result = $generalfuncobj->get_unique("user", "vStoreUniqueId", $_POST['uniqid']);
    if ($result) {
        $userObj->setvStoreUniqueId(addslashes($_POST['uniqid']));
    } else {
        $generalfuncobj->func_set_temp_sess_msg("Account Name already Exist,Please Enter Another.", "alert-danger", "Error");
        header("Location:index.php?file=u-useradd&type=$type&mode=add");
        exit;
    }

    if($type!= "4")
    {
        $userObj->setiUserRoleId($_POST['userrole']);
    }

    $userObj->setvAddress(addslashes($_POST['adrs']));
    $address = $_POST['adrs'];
    $city = $_POST['iCityId'];
    $state = $_POST['iStateId'];
    $zip = $_POST['zip'];
    $fulladdress=$address.",".$city.",".$state.",".$zip;

    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($fulladdress) . '&sensor=true');

    $coordinates = json_decode($coordinates);
    $lat[0] = $coordinates->results[0]->geometry->location->lat;
    $long[0] = $coordinates->results[0]->geometry->location->lng;
    $userObj->setvLatitude($lat[0]);
    $userObj->setvLongitude($long[0]);

    $userObj->setvZip($_POST['zip']);
    $userObj->setvCity(addslashes($_POST['iCityId']));
    $userObj->setvState(addslashes($_POST['iStateId']));
    $userObj->setiCountryId(addslashes($_POST['iCountryId']));
    $userObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
    $userObj->seteStatus($_POST['eStatus']);
    $userObj->setvContact($_POST['contact']);
    $userObj->setvPassword(md5("123456"));
    $userObj->seteHowAboutUs($_POST['aboutus']);
    $userObj->setvReferralEmail(addslashes($_POST['refmail']));

    if(isset($_POST['refmail']))
    {
        $refmail = $_POST['refmail'];
        $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
        $row = $userObj->check_exist_mail($refmail);
        if($type=="4" || $type=="5" || $type=="6")
        {
            $point="200";
        }
        else
        {
            $point="100";
        }

        if (count($row) > 0) {
          $userid = $row[0]['iUserId'];
          if ($referralObj->check_existing_user($userid)) {

                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->update_points($userid,$point);
            } else {

                $referralObj->setiUserId($userid);
                $referralObj->setvPoint($point);
                $referralObj->seteStatus("1");
                $referralObj->setiDtAdded($iDtAdded);
                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->setvReason("Reference");
                $iReffaralId = $referralObj->insert();
            }
        }
    }

    $userObj->seteActivePaidPlan("0");
    $userObj->setiUserTypeId($type);

    $userObj->setePlanType("0");
    $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
    $iExpireTime = strtotime("+30 day", strtotime(date('Y-m-d 23:59:59')));
    $userObj->setiExpireTime($iExpireTime);

    //Static Plan For user as he will get its free user plan according to usertype

    if ($type == 2) {
        $userObj->setiPlanId("2");
    } else if ($type == 3) {
        $userObj->setiPlanId("3");
    } else if ($type == 4) {
        $userObj->setiPlanId("1");
    } else if ($type == 5|| $type = 6) {
        $userObj->setiPlanId("1");
    }


    if ($type == "4" || $type == "5" || $type == "6") {

        $userObj->seteNightDelivery($_POST['nightdelivery']);
        $userObj->setiPreferDeliveryTimeFrom(addslashes($_POST['deliverytimefrom']));
        $userObj->setiPreferDeliveryTimeTo(addslashes($_POST['deliverytimeto']));
          $userObj->setiDeliveryHoursTo(addslashes($_POST['hoursto']));
        $userObj->setiDeliveryHoursFrom(addslashes($_POST['hoursfrom']));
        $userObj->setiTypeOfStoreId($_POST['storetype']);
        $userObj->setvStoreName(addslashes($_POST['store']));


    }

   
    if ($type == "5") {
        $userObj->setNumberOfStore($_POST['storenum']);
        $userObj->setvComment($_POST['storenum']);

    }


    if ($type == "2" || $type == "3") {
//        $userObj->setvTitle(addslashes($_POST['title']));
        $userObj->setiCategoryId($_POST['category']);
    }

    $iUserId = $userObj->insert();

    $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($iUserId)";
    $obj->sql_query($store_count);

    $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
    $point=$_REQUEST['Points'];
    if ($referralObj->check_existing_user($iUserId)) {

        $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));

        $points="Update referral set vPoint = '$point',iDtUpdated='$iDtAdded' where iUserId=$iUserId ";

       $pointsql=$obj->sql_query($points);
    } else {

        $referralObj->setiUserId($iUserId);
        $referralObj->setvPoint($point);
        $referralObj->seteStatus("1");
        $referralObj->setiDtAdded($iDtAdded);
        $referralObj->setiDtUpdated($iDtAdded);
        $referralObj->setvReason("ADmin");
        $iReffaralId = $referralObj->insert();
    }

    $user_settingsObj->setiUserId($iUserId);
    $user_settingsObj->seteNotification('1');
    $iUserSettingsId=$user_settingsObj->insert();

    /*  $user_securityObj->setiUserId($iUserId);
      $user_securityObj->setvQuestion($_POST['sque']);
      $user_securityObj->settAnswer($_POST['ans']);
      $user_securityObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
      $user_securityObj->seteStatus("1");

      $iUserSecQueId=$user_securityObj->insert();*/
    if($type =="4" || $type =="5" || $type =="6")
    {
      
        if($_REQUEST['page']=="sublist")
        {
            $qr = $retailer_detailObj->get_detail_by_retailer($iId);
            $qrcode = $qr[0]['vOriginal_code'];

            $account = substr($qrcode, 0, 3);
            $userObj->GENERATE_SC_QR_CODE($iUserId, $account);
        }
        else
        {
            $userObj->GENERATE_SC_QR_CODE($iUserId);
        }
    }

    /* $coordinates = json_decode($coordinates);
     $lat[0] = $coordinates->results[0]->geometry->location->lat;
     $long[0] = $coordinates->results[0]->geometry->location->lng;

     $userObj->setvLatitude($lat[0]);
     $userObj->setvLongitude($long[1]);*/
    $generalfuncobj->func_set_temp_sess_msg("User Details Added Successfully");
    $vStoreUniqueId = $_POST['uniqid'];
    $DATA = array('name' => $vStoreUniqueId);
  /*  echo $generalfuncobj->get_SC_SEND_mail('register', $DATA, $mail, 'Welcome To service_calibrate');*/

    if($_REQUEST['page']=="sublist")
    {
        header("Location:index.php?file=u-subuserlist&type=$type&iId=$iId");
    }
    else
    {
        header("Location:index.php?file=u-userlist&type=$type");
    }

    exit;

} else if ($mode == "update") {
    $mail = $_POST['email'];
    $mailchk = $userObj->check_exist_mail_withid($mail, $iId);

     if (count($mailchk) > 0) {
         $generalfuncobj->func_set_temp_sess_msg("This Email Already Exist", "alert-danger", "Error");
         header("Location:index.php?file=u-useradd&type='$type'");
         exit;
     } else {
         $userObj->setvEmail($mail);
     }


    if ($_FILES["vImage"]["name"] == "") {
        $userObj->setvImage($_POST['image_hid']);
    } else {
        $photopath = $user_image_path;
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];

        //$vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");

        $user_img_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);

        $user_img_name = $user_img_arr[0];
        if ($user_img_name == '') {
            $generalfuncobj->func_set_temp_sess_msg("Please upload pictures only in png,jpg,jpeg,gif or bmp format.", "alert-danger", "Error");
            header("Location:index.php?file=u-useradd&mode=update&iId=$iId&type=$type");
            exit;
        } else {
            $userObj->setvImage($user_img_name);
        }



    }
//    $userObj->select($iId);
    $userObj->setvFirstName(addslashes($_POST['firstname']));
    $userObj->setvLastName(addslashes($_POST['lastname']));
    $userObj->setiIndustriesId($_POST['industry']);

    $userObj->setvAddress(addslashes($_POST['adrs']));
    $userObj->setvZip($_POST['zip']);
    $userObj->setvCity($_POST['iCityId']);
    $userObj->setvState($_POST['iStateId']);
    $userObj->setiCountryId($_POST['iCountryId']);

   $address = $_POST['adrs'];
    $city = $_POST['iCityId'];
    $state = $_POST['iStateId'];
    $zip = $_POST['zip'];
    $fulladdress=$address.",".$city.",".$state.",".$zip;

    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($fulladdress) . '&sensor=true');

    $coordinates = json_decode($coordinates);
    $lat[0] = $coordinates->results[0]->geometry->location->lat;
    $long[0] = $coordinates->results[0]->geometry->location->lng;
    $userObj->setvLatitude($lat[0]);
    $userObj->setvLongitude($long[0]);

    /* $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=true');*/

    $userObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
    $userObj->seteStatus($_POST['eStatus']);
    $userObj->setvContact($_POST['contact']);
    $userObj->setiUserTypeId($type);
    $userObj->setvEmail($_POST['email']);
    if ($_POST['pwd'] == "") {
        $pswd = $_REQUEST['pswd'];
    } else {
        $pswd = $_POST['pwd'];
    }
    $userObj->setvPassword($pswd);

    $result = $generalfuncobj->get_unique("user", "vStoreUniqueId", $_POST['uniqid'], $iId);
    if ($result) {
        $userObj->setvStoreUniqueId(addslashes($_POST['uniqid']));
    } else {
        $generalfuncobj->func_set_temp_sess_msg("Account Name already Exist,Please Enter Another.", "alert-danger", "Error");
        header("Location:index.php?file=u-useradd&type=$type&mode=update&iId=$iId");
        exit;
    }


    if ($type == "4" || $type == "5" || $type == "6") {

        $userObj->seteNightDelivery($_POST['nightdelivery']);
        $userObj->setiPreferDeliveryTimeFrom($_POST['deliverytimefrom']);
        $userObj->setiPreferDeliveryTimeTo($_POST['deliverytimeto']);
  $userObj->setiDeliveryHoursTo(addslashes($_POST['hoursto']));
        $userObj->setiDeliveryHoursFrom(addslashes($_POST['hoursfrom']));
        $userObj->setiTypeOfStoreId($_POST['storetype']);
        $userObj->setvStoreName($_POST['store']);


    }
    if ($type == "5") {
        $userObj->setNumberOfStore($_POST['storenum']);
          $userObj->setvComment($_POST['storenum']);

    }


    if ($type == "2" || $type == "3") {

        $userObj->setiCategoryId($_POST['category']);
    }

    
    if($type!= "4")
    {
        $userObj->setiUserRoleId($_POST['userrole']);
    }


    $userObj->updateuser($iId);

    $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
    $point=$_REQUEST['Points'];
    if ($referralObj->check_existing_user($iId)) {

        $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
        $points="Update referral set vPoint = '$point',iDtUpdated='$iDtAdded' where iUserId=$iId ";
        $pointsql=$obj->sql_query($points);
    } else {

        $referralObj->setiUserId($iId);
        $referralObj->setvPoint($point);
        $referralObj->seteStatus("1");
        $referralObj->setiDtAdded($iDtAdded);
        $referralObj->setiDtUpdated($iDtAdded);
        $referralObj->setvReason("ADmin");
        $iReffaralId = $referralObj->insert();
    }



    $generalfuncobj->func_set_temp_sess_msg("User Details Updated Successfully");
    if($_REQUEST['page']=="sublist")
    {
        header("Location:index.php?file=u-subuserlist&type=$type&iId=$pId");
    }
    else
    {
        header("Location:index.php?file=u-userlist&type=$type");
    }
   // header("Location:index.php?file=u-userlist&type=$type");
    exit;

} else if ($mode == 'delete') {

    $pId=$_REQUEST['pid'];
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];

      $exe_del = "Update user set eStatus='2' where iUserId= '$ID' or iParentId='$ID'";
        $obj->sql_query($exe_del);
        
  $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>0 THEN iStoreCount -1 ELSE 0 END where iUserId in ($pId)";
        $obj->sql_query($store_count);        
        
        $generalfuncobj->func_set_temp_sess_msg("User Details Deleted Successfully");
        if($_REQUEST['page']=="sublist")
        {
            header("Location:index.php?file=u-subuserlist&type=$type&iId=$pId");
        }
        else
        {
            header("Location:index.php?file=u-userlist&type=$type");
        }

        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {

        foreach ($_REQUEST['delete'] as $ID) {
           echo $exe_del = "Update user set eStatus='2' where iUserId='$ID' or iParentId='$ID'";
            $obj->sql_query($exe_del);
            
            $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>0 THEN iStoreCount -1 ELSE 0 END where iUserId in ($pId)";
        $obj->sql_query($store_count);     
        }

        $generalfuncobj->func_set_temp_sess_msg("User Details Deleted Successfully");
        if($_REQUEST['page']=="sublist")
        {
            header("Location:index.php?file=u-subuserlist&type=$type&iId=$pId");
        }
        else
        {
            header("Location:index.php?file=u-userlist&type=$type");
        }

        exit;
    }
} else if ($mode == "resetpassword") {

    $pwd = md5($_REQUEST['password']);
    $id = $_REQUEST['iId'];
    $type = $_REQUEST['type'];

    $exe_del = "Update user set vPassword = '$pwd' where iUserId='$id'";
    $obj->sql_query($exe_del);

    $generalfuncobj->func_set_temp_sess_msg("Password Chanegd successfully");
    header("Location:index.php?file=u-useradd&mode=update&iId=$id&type=$type");
    exit;

}
else if($mode=="update_payment")
{
    pr($_REQUEST);
     $type=$_REQUEST['type'];
    $iId=$_REQUEST['uid'];
    $BillinginfoObj->setVState($_REQUEST['vState']);
    $BillinginfoObj->setvCity($_REQUEST['vCity']);
    $BillinginfoObj->setvCountry($_REQUEST['vCountryId']);
    $BillinginfoObj->setvZip($_REQUEST['vZip']);
    $BillinginfoObj->setvAddress($_REQUEST['vAddress']);

    if(isset($_REQUEST['bId'])&& $_REQUEST['bId']!="")
    {
        $bid=$_REQUEST['bId'];
        $BillinginfoObj->update_adrs_admin($bid);
    }
    else
    {
        $BillinginfoObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
        $BillinginfoObj->setiDtUpdated(strtotime(date('Y-m-d H:i:s')));
        $BillinginfoObj->seteStatus("1");
        $BillinginfoObj->setiUserId($iId);
        $BillinginfoObj->insert();
    }
    $originalDateend = $_REQUEST['iEndDate'];
    $dend = explode("-",$originalDateend);
    $d2 = $dend[2]."-".$dend[0]."-".$dend[1];
    $edate = strtotime(date('Y-m-d H:i:s',strtotime($d2)));
    $amount=$_REQUEST['fAmountPaid'];
    $plan=$_REQUEST['eStatus'];
    $plantype=$_REQUEST['plantype'];
    $exe_del = "Update user set iExpireTime=$edate, eActivePaidPlan='$plan' where iUserId in ($iId)";
    $obj->sql_query($exe_del);

    $exe_select = "select * from user_plan_info where iUserId in ($iId)";
    $ex=$obj->sql_query($exe_select);

    if(count($ex)>0)
    {
        $exe_del1 = "Update user_plan_info set  fPlanAmout=$amount, ePlanType='$plantype' where iUserId in ($iId)";
        $obj->sql_query($exe_del1);
    }
    else
    {
        $plan_insert="insert into user_plan_info (iUserId, fPlanAmout, ePlanType) VALUES ('$iId', '$amount', '$plantype')";
        $plan_sql=$obj->insert($plan_insert);
    }



    $generalfuncobj->func_set_temp_sess_msg("Payment Details Updated successfully");
      header("Location:index.php?file=u-paymentadd&iId=$iId&type=$type");


}


else if($mode=="Get_Unique")
{
    ob_get_clean();

    $value=$_REQUEST['name'];
$iId=$_REQUEST['id'];    
    $result = $generalfuncobj->get_unique("user", "vStoreUniqueId", $value, $iId);
    if ($result)
    {
        $response = array("MSG" => "");
    }
    else
    {
        $response = array("MSG" => "This Account Name already exist,Please enter another");
    }
    echo json_encode($response);
    exit;
}
else if($mode=="Get_Uniquemail")
{
    ob_get_clean();

    $value=$_REQUEST['name'];
$iId=$_REQUEST['id'];
    $result = $generalfuncobj->get_unique("user", "vEmail", $value, $iId);
    if ($result)
    {
        $response = array("MSG" => "");
    }
    else
    {
        $response = array("MSG" => "This Email already exist,Please enter another");
    }
    echo json_encode($response);
    exit;
}
else if($mode=="upload_CSV")
{
    pr($_REQUEST);
    $utype=$_REQUEST['type'];

    $parentid=$_REQUEST['iId'];
    $sql_p = $userObj->select($parentid);

    $counter="0";
    $fail="0";

    $photopath = $upload_image_path . 'CSV/';
    $vphoto = $_FILES["CSV"]["tmp_name"];
    $vphoto_name = $_FILES["CSV"]["name"];
    $vphoto_type = $_FILES["CSV"]["type"];
    $prefix="admin".date('Ymd');
    $image_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);
    @chmod($upload_image_path . 'CSV/' . $image_arr[0], 0777);
    if( $image_arr[0]!='')
    {

        if (($handle = fopen($upload_image_path . 'CSV/' . $image_arr[0], "r")) !== FALSE)
        {

            $CSV = 0;
            while (!feof($handle)) {

                if($CSV!=0)
                {
                    $data = fgetcsv($handle);

                    if($utype=="4"||$utype=="5"||$utype=="6")
                    {


                    $col1 = $data[0];     //  Account Name
                    $col11 = $data[1];     //  Account #
                   $col2 = $data[2];     //Email
                   $col3 = $data[3];     //Contact
                   $col4 = $data[4];     //	First Name
                   $col5 = $data[5];     //	Last Name
                   $col6 = $data[6];     //Address
                   $col7 = $data[7];     //City
                   $col8 = $data[8];     //State
                   $col9 = $data[9];     //country*/
                   $col10 = $data[10];    //Zip
                    }
                    else
                    {

                        $col1 = $data[0];     //  Account Name
                        $col2 = $data[1];     //Email
                        $col3 = $data[2];     //Contact
                        $col4 = $data[3];     //	First Name
                        $col5 = $data[4];     //	Last Name
                        $col6 = $data[5];     //Address
                        $col7 = $data[6];     //City
                        $col8 = $data[7];     //State
                        $col9 = $data[8];     //country*/
                        $col10 = $data[9];    //Zip

                    }


                    if ($utype == 4)
                    {
                        $userObj->setvStoreName($col11);
                        //$col11 = $data[10];//account#
                        $col50 = $data[11];   //preferable from
                        $col51 = $data[12];//preferable to
                        $col30 = $data[13];//night delivery
                        $col111 = strtolower($col30);
                        if ($col111 == 'yes')
                        {
                            $col30 = 1;
                        }
                        else if ($col111 == 'no')
                        {
                            $col30 = 0;
                        }
                        else
                        {
                            $col30 = 2;
                        }

                    }
                    elseif ($utype == 5)
                    {
                        $userObj->setvStoreName($col11);
                      //  $col11 = $data[10];//account#
                        $col52 = $data[11];   //No of Store*/
                        $col50 = $data[12];   //preferable from
                        $col51 = $data[13];   //preferable to
                        $col30 = $data[15];//night delivery
                        $col111 = strtolower($col30);
                        if ($col111 == 'yes')
                        {
                            $col30 = 1;
                        }
                        else if ($col111 == 'no')
                        {
                            $col30 = 0;
                        }
                        else
                        {
                            $col30 = 2;
                        }

                    }
                    elseif ($utype == 6)
                    {
                        $userObj->setvStoreName($col11);
                        //$col11 = $data[10];//account#
                        $col50 = $data[11];   //preferable from
                        $col51 = $data[12];   //Preferable To
                        $col53 = $data[13];   //Preferable from hour
                        $col54 = $data[13];   //Preferable To hour
                        $col30 = $data[14];//night delivery
                        $col111 = strtolower($col30);
                        if ($col111 == 'yes')
                        {
                            $col30 = 1;
                        } else if ($col111 == 'no')
                        {
                            $col30 = 0;

                        }
                        else
                        {
                            $col30 = 2;
                        }
                    }
                    elseif ($utype == 2 || $utype == 3)
                    {
                        $col11 = $data[10]; //role
                    }



                    $location = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address='.$col10.'&sensor=true");
                    $adrs = json_decode($location);
                    $city = $adrs->results[0]->address_components[1]->long_name;
                    $state = $adrs->results[0]->address_components[3]->long_name;
                    $country = $adrs->results[0]->address_components[4]->long_name;
                    if ($city == '' || $state == '' || $country == '')
                    {
                        $col7 = $col7;      //City
                        $col8 = $col8;   //State
                        $col9 = $col9; //country
                    }
                    else
                    {
                        $col7 = $city;       //City
                        $col8 = $state;   //State
                        $col9 = $country; //country
                    }

                    $col15 = $sql_p[0]['iUserTypeId'];
                    $col16 = $sql_p[0]['iPlanId'];
                    $col17 = $sql_p[0]['iIndustriesId'];
                    $col18 = $sql_p[0]['iCategoryId'];
                    $col21 = $sql_p[0]['iExpireTime'];
                    $col22 = $sql_p[0]['eHowAboutUs'];
                    $col23 = $sql_p[0]['eActivePaidPlan'];
                    $col24 = $sql_p[0]['ePlanType'];
                    $col25 = $sql_p[0]['fAmountPaid'];

                    //get lat long from address
                    $address = $col7;
                    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=true');
                    $coordinates = json_decode($coordinates);
                    $lat[0] = $coordinates->results[0]->geometry->location->lat;
                    $long[0] = $coordinates->results[0]->geometry->location->lng;

                    $name = $generalfuncobj->get_unique("user", "vStoreUniqueId", $col1);
                    $email = $generalfuncobj->get_unique("user", "vEmail", $col2);


                    $currentcount="0";
                    if ($name)
                    {
                        $userObj->setvStoreUniqueId($col1);
                    }
                    else
                    {
                        $currentcount = 1;
                    }

                    if ($email)
                    {
                        $userObj->setvEmail($col2);
                    }
                    else
                    {
                        $currentcount = 1;
                    }

                    if($currentcount=="0")
                    {

                        if($col2!="Email") {
                        if ($col1 != '' && $col2 != '' && $col3 != '' && $col4 != '' && $col5 != '' && $col6 != '' && $col7 != '' && $col8 != '' && $col9 != '' && $col10 != '')
                        {

                            $userObj->setvStoreUniqueId($col1);
                            $userObj->setvEmail($col2);
                            $userObj->setvContact($col3);
                            $userObj->setvFirstName($col4);
                            $userObj->setvLastName($col5);
                            $userObj->setvAddress($col6);
                            $userObj->setvCity($col7);
                            $userObj->setvState($col8);
                            $userObj->setiCountryId($col9);
                            $userObj->setvZip($col10);
                            $userObj->setNumberOfStore($col52);
                            $NUMBER = $generalfuncobj->generateNumericUniqueToken(6);
                            $userObj->setvPassword(md5($NUMBER));
                            $userObj->seteStatus("1");
                            $userObj->setiDtAdded(strtotime(gmdate('Y-m-d H:i:s')));
                            $userObj->setiParentId($parentid);
                            $userObj->setiUserTypeId($utype);
                            $userObj->setiUserTypeId($col15);
                            $userObj->setiPlanId($col16);
                            $userObj->setiIndustriesId($col17);
                            $userObj->setiCategoryId($col18);
                            $userObj->setvLatitude($lat[0]);
                            $userObj->setvLongitude($long[0]);
                            $userObj->setiExpireTime($col21);
                            $userObj->seteHowAboutUs($col22);
                            $userObj->seteActivePaidPlan($col23);
                            $userObj->setePlanType($col24);
                            $userObj->setfAmountPaid($col25);
                            $userObj->setiDeliveryHoursFrom($col53);
                            $userObj->setiDeliveryHoursTo($col54);
                            $userObj->setiPreferDeliveryTimeFrom($col50);
                            $userObj->setiPreferDeliveryTimeTo($col51);
                            $userObj->seteNightDelivery($col30);

                            if ($utype == 2 || $utype == 3) {

                                if ($col11 != '') {
                                    echo "->All->";
                                    $rolee = "select vName,iUserRoleId from user_role where vName='$col11' AND iUserTypeId='$utype'";
                                    $roleexist = $obj->sql_query($rolee);
                                    if (count($roleexist) > 0) {
                                        $userObj->setiUserRoleId($roleexist[0]['iUserRoleId']);
                                    } else {
                                   //     echo "else";
                                        $user_role->setvName($col11);
                                        $user_role->seteStatus("1");
                                        $user_role->setiUserTypeId($utype);
                                        $user_role->setiDtAdded($iDtAdded);
                                        $iUserRoleId = $user_role->insert();
                                        $userObj->setiUserRoleId($iUserRoleId);
                                    }
                                    $iUserId = $userObj->upload_csv();
                                    $counter++;
                                  //  echo "->Done->";
                                    $DATA = array('email' => $col2, 'password' => $NUMBER);
                                    echo   $generalfuncobj->get_SC_SEND_mail('csv_register', $DATA, $col2, 'Welcome To Service Calibrate');

                                    $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($parentid)";
                                    $m = $obj->sql_query($store_count);


                                }
                                else
                                {
                                //    echo "fail";

                                    $fail++;

                                }
                                }
                            else if ($utype == 4)
                            {

                                if ($col11 != '' && $col50 != '' && $col51 != '' && $col30 != '2')
                                {
                                    $iUserId = $userObj->upload_csv();
                                  //  echo $iUserId;
                                    $vStoreUniqueId = $sql_p[0]['vStoreUniqueId'];
                                    $qr = $retailer_detailObj->get_detail_by_retailer($sql_p[0]['iUserId']);
                                    $qrcode = $qr[0]['vOriginal_code'];

                                    $account = substr($qrcode, 0, 3);
                                    $userObj->GENERATE_SC_QR_CODE($iUserId, $account);
                                     $counter++;
                                    $DATA = array('email' => $col2, 'password' => $NUMBER);
                                    echo     $generalfuncobj->get_SC_SEND_mail('csv_register', $DATA, $col2, 'Welcome To Service Calibrate');

                                    $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($parentid)";
                                    $m = $obj->sql_query($store_count);
                                }
                                else
                                {
                                    $fail++;
                                }
                            }
                            else if ($utype == 5)
                            {
                                if ($col11 != '' && $col50 != '' && $col51 != '' && $col52 != '')
                                {
                                    $iUserId = $userObj->upload_csv();
                                    $vStoreUniqueId = $sql_p[0]['vStoreUniqueId'];
                                    $qr = $retailer_detailObj->get_detail_by_retailer($sql_p[0]['iUserId']);
                                    $qrcode = $qr[0]['vOriginal_code'];

                                    $account = substr($qrcode, 0, 3);
                                    $userObj->GENERATE_SC_QR_CODE($iUserId, $account);
                                    $counter++;
                                    $DATA = array('email' => $col2, 'password' => $NUMBER);
                                   // echo   $generalfuncobj->get_SC_SEND_mail('csv_register', $DATA, $col2, 'Welcome To Service Calibrate');

                                    $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($parentid)";
                                    $m = $obj->sql_query($store_count);
                                }
                                else
                                {
                                    $fail++;
                                }
                            }
                            else if ($utype == 6)
                            {

                                if ($col11 != '' && $col50 != '' && $col51 != '' && $col53 != '' && $col54 != '') {
                                    $iUserId = $userObj->upload_csv();
                                    $vStoreUniqueId = $sql_p[0]['vStoreUniqueId'];
                                    $qr = $retailer_detailObj->get_detail_by_retailer($sql_p[0]['iUserId']);
                                    $qrcode = $qr[0]['vOriginal_code'];

                                    $account = substr($qrcode, 0, 3);
                                    $userObj->GENERATE_SC_QR_CODE($iUserId, $account);
                                    $counter++;
                                    $DATA = array('email' => $col2, 'password' => $NUMBER);
                                    $generalfuncobj->get_SC_SEND_mail('csv_register', $DATA, $col2, 'Welcome To Service Calibrate');
                                    $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($parentid)";
                                    $m = $obj->sql_query($store_count);
                                }
                                else
                                {
                                    $fail++;
                                }
                            }
                        }

                        else
                        {
                            echo $col1;
                            $fail++;
                            echo "blank";
                        }
                    }}
                    else
                    {
                        $fail++;
                        echo "exist";
                    }




                }
                $CSV++;
            }

        }
        else
        {
            echo "Uploaded Failed";
            $_SESSION['admin_upload_excel']="Uploaded Failed";
            $_SESSION['admin_upload_class']="alert-danger";
           header("Location:index.php?file=u-subuserlist&iId=$parentid&type=$utype");
        }
    }
    else
    {
        echo "Uploaded Failed";
        $_SESSION['admin_upload_excel']="Uploaded Failed";
        $_SESSION['admin_upload_class']="alert-danger";
        header("Location:index.php?file=u-subuserlist&iId=$parentid&type=$utype");
    }

    if($counter>0)
    {
        $_SESSION['admin_upload_excel']=$counter." Account/Users have been uploaded successfully.";
        $_SESSION['admin_upload_class']="alert-success";
    }
    else if($fail>=0)
    {
        $_SESSION['admin_upload_excel']="Sorry, Please check the data on excel and try again";
        $_SESSION['admin_upload_class']="alert-danger";
    }
    header("Location:index.php?file=u-subuserlist&iId=$parentid&type=$utype");
    exit;

}



