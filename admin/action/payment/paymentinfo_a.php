<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 28/3/15
 * Time: 1:40 PM
 */

include_once($inc_class_path . 'billing_info.class.php');
$BillinginfoObj = new billing_info();

include_once($inc_class_path . 'user.class.php');
$userObj = new user();
pr($_REQUEST);
$MODE = $_REQUEST['mode'];

/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $admin->setvFirstName($_REQUEST['vFirstName']);
    $admin->setvLastName($_REQUEST['vLastName']);
    $admin->setvEmail($_REQUEST['vEmail']);
    $admin->setvPasswd(base64_encode($_REQUEST['vPasswd']));
    $admin->setdtAdded(strtotime(gmdate('Y-m-d H:i:s')));
    $admin->setdtUpdated('0');
    $admin->setdtLastLogin('0');
    $admin->setvLastLoginIP('');
    $admin->seteStatus($_REQUEST['eStatus']);
    $admin->seteType($_REQUEST['eType']);

    $admin->insert();
    $generalfuncobj->func_set_temp_sess_msg("Admin Details Added Successfully");
    header("Location:index.php?file=sc-adminlist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $BillinginfoObj->setvCountry($_REQUEST['vCountryId']);
    $BillinginfoObj->setvCity($_REQUEST['vCity']);
    $BillinginfoObj->setvZip($_REQUEST['vZip']);
    $BillinginfoObj->setvAddress($_REQUEST['vAddress']);
    $BillinginfoObj->setiDtUpdated(strtotime(gmdate('Y-m-d H:i:s')));
    $BillinginfoObj->setVState($_REQUEST['vState']);
    $BillinginfoObj->seteStatus($_REQUEST['eStatus']);

    $BillinginfoObj->update_adrs_admin($_REQUEST['iId']);

    $originalDateend = $_REQUEST['iEndDate'];
    $dend = explode("-",$originalDateend);
    $d2 = $dend[2]."-".$dend[0]."-".$dend[1];
    $edate = strtotime(date('Y-m-d H:i:s',strtotime($d2)));
    $amount=$_REQUEST['fAmountPaid'];

    $ID=$_REQUEST['uid'];

    $exe_del = "Update user set iExpireTime=$edate , fAmountPaid=$amount where iUserId in ($ID)";
    $obj->sql_query($exe_del);

    $generalfuncobj->func_set_temp_sess_msg("Payment Details Update Successfully");
    header("Location:index.php?file=py-paymentinfo");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];

        $date=strtotime(date('Y-m-d H:i:s'));
        $exe_del = "Update billing_info set eStatus='2' , iDtUpdated=$date where iBillingId in ($ID)";
        $obj->sql_query($exe_del);

        $generalfuncobj->func_set_temp_sess_msg("Payment Details Deleted Successfully");
        header("Location:index.php?file=py-paymentinfo");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $date=strtotime(date('Y-m-d H:i:s'));
          echo  $exe_del = "Update billing_info set eStatus='2' , iDtUpdated=$date where iBillingId in ($ID)";
            $obj->sql_query($exe_del);
        }

        $generalfuncobj->func_set_temp_sess_msg("Payment Details Deleted Successfully");
        header("Location:index.php?file=py-paymentinfo");
        exit;
    }
}

