<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 1/4/15
 * Time: 11:50 AM
 */

include_once($inc_class_path . 'coupon.class.php');

$coupon = new coupon();
$MODE = $_REQUEST['mode'];

/* Add Mode */
if ($MODE == 'add') {
    #pr($_REQUEST);exit;
    $coupon->setvCouponCode($_REQUEST['vCouponCode']);
    $coupon->settCouponDescription($_REQUEST['tCouponDescription']);
    $coupon->setiExpireDate(strtotime($_REQUEST['iExpireDate']));
    $coupon->setiDtAdded($generalfuncobj->gm_date_string());
    $coupon->seteStatus($_REQUEST['eStatus']);

    $coupon->insert();
    $generalfuncobj->func_set_temp_sess_msg("Coupon Details Added Successfully");
    header("Location:index.php?file=cou-couponlist");
    exit;
}

/* Update Mode */
if ($MODE == 'edit') {
    $coupon->setvCouponCode($_REQUEST['vCouponCode']);
    $coupon->settCouponDescription($_REQUEST['tCouponDescription']);
    $coupon->setiExpireDate(strtotime($_REQUEST['iExpireDate']));
    $coupon->seteStatus($_REQUEST['eStatus']);

    $coupon->update($_REQUEST['iCouponId']);
    $generalfuncobj->func_set_temp_sess_msg("Coupon Details Updated Successfully");
    header("Location:index.php?file=cou-couponlist");
    exit;
}

/* Delete Mode */
if ($MODE == 'delete') {
    if ($_REQUEST['delete_type'] == 'single_delete') {
        $ID = $_REQUEST['iId'];
        $coupon->delete($ID);

        $generalfuncobj->func_set_temp_sess_msg("Category Details Deleted Successfully");
        header("Location:index.php?file=cou-couponlist");
        exit;
    } elseif ($_REQUEST['delete_type'] == 'multi_delete') {
        foreach ($_REQUEST['delete'] as $ID) {
            $coupon->delete($ID);
        }

        $generalfuncobj->func_set_temp_sess_msg("Category Details Deleted Successfully");
        header("Location:index.php?file=cou-couponlist");
        exit;
    }
}

