
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/jquery-tags-input/jquery.tagsinput.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/select2/select2.js"></script>

<?php

include_once($admin_path . 'js_datatable.php');
?>
<? include_once($admin_path . 'js_form.php');?>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/select-init.js"></script>

<?php

include_once($inc_class_path . 'user_mapping.class.php');
$user_mappingObj =new  user_mapping();
$mode=$_REQUEST['mode'];

if($mode=="link_user")
{
    ob_get_clean();
    //pr($_REQUEST);
    $user = $_REQUEST['type'];
    $selectuser=$_REQUEST['user'];
    if ($user == "1") {
        $userdrop = "select iUserId,vStoreUniqueId from user where iUserTypeId in(4,5,6) AND eStatus!='2' AND iParentId=0 AND iUserId!=$selectuser order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }
    else if ($user == "2") {
        $userdrop = "select iUserId,vStoreUniqueId from user where iUserTypeId in(2) AND eStatus!='2' AND iParentId=0 order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }
    else if ($user == "3") {
        $userdrop = "select iUserId,vStoreUniqueId from user where iUserTypeId in(2,3) AND eStatus!='2' order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }
    else if ($user == "4") {
        $userdrop = "select iUserId,vStoreUniqueId from user where iUserTypeId in(2,3) AND eStatus!='2' order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }
    else if ($user == "5") {
        $userdrop = "select iUserId,vStoreUniqueId from user where eStatus!='2' AND iParentId=$selectuser or iUserId=$selectuser order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }

    else if ($user == "6") {
        $userdrop = "select iUserId,vStoreUniqueId from user where iUserTypeId in(3) AND iParentId=0 AND eStatus!='2' AND iUserId!=$selectuser order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }
    //pr($userlist);
    /*$HTML='<label class="col-md-2 control-label"><span class="red"> *</span> Link To</label>';
    $HTML.=' <div class="col-md-9">
            <select multiple="multiple" class="multi-select"
                    id="my_multi_select8" name="linkto[]">';*/
    $HTML='';
    $SELECTED="";

    foreach ($userlist as $DATA) {
        $HTML.='  <option value="' . $DATA['iUserId'] . '" ' . $SELECTED . '>' . $DATA['vStoreUniqueId'] . '</option> ';
        //$HTML.='<li class="ms-elem-selectable" id="'.$DATA['iUserId'].'-selectable"><span>'.$DATA['vStoreUniqueId'].'</span></li>';
    }
    // $HTML.='</ul>';


    $response = array('OUTPUT' => $HTML);
    echo json_encode($response);

}
else if($mode=="link_employee")
{
    ob_get_clean();


    $user = $_REQUEST['type'];
    $selectuser=$_REQUEST['user'];
    if ($user == "1" ) {
        $userdrop = "select iUserId,vStoreUniqueId from user where eStatus!='2' AND iParentId=$selectuser AND iUserId=$selectuser order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }
    else if ($user == "2") {
        $userdrop = "select iUserId,vStoreUniqueId from user where iUserTypeId in(2) AND eStatus!='2' AND iParentId=0 order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }
    else if ($user == "3") {
        $userdrop = "select iUserId,vStoreUniqueId from user where iUserTypeId in(2,3) AND eStatus!='2' order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }
    else if ($user == "4") {
        $userdrop = "select iUserId,vStoreUniqueId from user where iUserTypeId in(2,3) AND eStatus!='2' order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }
    else if ($user == "5") {
        $userdrop = "select iUserId,vStoreUniqueId from user where iUserTypeId in(3) AND iParentId=0 AND eStatus!='2' order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }

    else if ($user == "6") {
        $userdrop = "select iUserId,vStoreUniqueId from user where iUserTypeId in(3) AND iParentId=0 AND eStatus!='2' AND iUserId!=$selectuser order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }

    if ($user == "5") {
        $userdrop = "select iUserId,vStoreUniqueId,vFirstName,vLastName from user where iUserTypeId in(3) AND iParentId=0 AND eStatus!='2' order by vStoreUniqueId";
        $userlist = $obj->select($userdrop);

    }
else {
    $userdrop = "select iUserId,vStoreUniqueId,vFirstName,vLastName from user where eStatus!='2' AND iParentId=$selectuser or iUserId=$selectuser order by vStoreUniqueId";
    $userlist = $obj->select($userdrop);
}    //pr($userlist);
    /*$HTML='<label class="col-md-2 control-label"><span class="red"> *</span> Link To</label>';
    $HTML.=' <div class="col-md-9">
            <select multiple="multiple" class="multi-select"
                    id="my_multi_select8" name="linkto[]">';*/
    $HTML='';
    $SELECTED="";

    foreach ($userlist as $DATA) {
        $HTML.='  <option value="' . $DATA['iUserId'] . '" ' . $SELECTED . '>' . $DATA['vFirstName']." ".$DATA['vLastName'] . '</option> ';
        //$HTML.='<li class="ms-elem-selectable" id="'.$DATA['iUserId'].'-selectable"><span>'.$DATA['vStoreUniqueId'].'</span></li>';
    }

    $response = array('OUTPUT' => $HTML);
    echo json_encode($response);
}
?>
