<?
include_once($inc_class_path . 'user.class.php');
$userObj = new user();
include_once($inc_class_path.'orders.class.php');
$orderobj=new orders();

include_once($inc_class_path . 'info.class.php');
$info = new info();

$info_icon = $info->select_report_info("contact_web");
$utype=$_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$report_file_name = 'contact';
unset($_SESSION[$report_file_name]);
//echo $utype;
if($utype=='2'||$utype=='3')
{
    // echo "ho";
    $sql = " select ur.vName as role,u.vStoreUniqueId as accountname,u.vStoreName as storename,u.vFirstName as fname,u.vLastName as lname,u.vEmail as email,u.vContact as contact,u.vAddress,u.vCity,u.vState,u.vZip,u.iUserRoleId, o.* from orders o left join user u on  o.iRetailerId=u.iUserId left join user_role ur on u.iUserRoleId=ur.iUserRoleId where o.iUserId='$iUserId' group by o.iRetailerId";
    $select_retailer=$obj->select($sql);
    // pr($select_retailer);
}
else
{
    //  echo "ho/...";
    $sql=" select ur.vName as role,u.vStoreUniqueId as accountname,u.vFirstName as fname,u.vLastName as lname,u.vEmail as email,u.vContact as contact,u.vAddress,u.vCity,u.vState,u.vZip,u.iUserRoleId, o.* from orders o left join user u on  o.iUserId=u.iUserId left join user_role ur on u.iUserRoleId=ur.iUserRoleId where o.iRetailerId='$iUserId' group by o.iUserId";
    $select_retailer = $obj->select($sql);
    //pr($select_retailer);
    // $uid=$select_retailer[0]['iUserId'];
    //  echo $uid;
    // pr($select_retailer);

}
?>

<section id="main-content">

<section class="wrapper">
<!-- page start-->

<div class="row">
    <div class="col-md-9">
        <ul class="breadcrumbs-alt">
            <li>
                <a href="index.php"><i class=""></i> Dashboard</a>
            </li>
            <li>
                <a class="current" href="javascript:;"><i class=""></i> Contact</a>
            </li>
        </ul>
    </div>
    <div class="col-md-3 pull-right text-right">
        <div class="form-group">
            <a href="#myModal2" name="SearchReport" data-toggle="modal" class="btn btn-success1"
               id="help_show" style="display: none">Help (?)</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Contact

            </header>
            <div class="panel-body">
                <div class="adv-table">
                    <div class="table-main-scroll">
                    <div class="col-md-12">
                        <table class="display table table-bordered table-striped" id="contact_table">
                            <thead>
                            <tr>
                                <th style="display: none"></th>
                                <!--   <th>Broadcast For</th>-->
                                <!--                                    <th>Details</th>-->
                                <? if ($utype == "2" || $utype == "3") { ?>
                                    <th>Account Name</th>
                                <? } else { ?>
                                    <th>Vendor Name</th>
                                <? } ?>
                                <? if ($utype == "2" || $utype == "3") { ?>
                                    <th>Account #</th>
                                <? } ?>
                                <th>Employee Name</th>
                                <th>Email</th>
                                <? if ($utype == "4" || $utype == "6" || $utype == "5") { ?>
                                    <th>Role</th>
                                <? } ?>
                                <th>Contact Number</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Zip</th>


                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            for ($i = 0; $i < count($select_retailer); $i++) {
                                ?>

                                <tr class="gradeX">
                                    <td style="display: none"><?php echo $i ?></td>
                                    <td>
                                        <?php echo $select_retailer[$i]['accountname']; ?>
                                    </td>

                                    <? if ($utype == "2" || $utype == "3") { ?>
                                        <td> <?php
                                        if ($select_retailer[$i]['storename'] != '') {
                                            echo $select_retailer[$i]['storename']; ?></td>
                                        <?
                                        } else {
                                            echo "--";

                                        }
                                    }?>


                                    <td>
                                        <?php echo $select_retailer[$i]['fname'] . " " . $select_retailer[$i]['lname']; ?>
                                    </td>
                                    <td>
                                        <?php if ($select_retailer[$i]['email'] != '') {
                                            echo $select_retailer[$i]['email'];

                                        } else {
                                            echo "Not Available";

                                        }?>

                                    </td>

                                    <? if ($utype == "4" || $utype == "6" || $utype == "5") { ?>
                                        <td><?
                                            if ($select_retailer[$i]['role'] != '') {
                                                echo $select_retailer[$i]['role'];
                                            } else {
                                                echo "--";
                                            }?></td>
                                    <? } ?>
                                    <td>
                                        <?php if ($select_retailer[$i]['contact'] != '') {
                                            echo $select_retailer[$i]['contact'];

                                        } else {
                                            echo "0000000000";

                                        }?>

                                    </td>
                                    <td><?php echo $select_retailer[$i]['vAddress']; ?>
                                    </td>
                                    <td><?php echo $select_retailer[$i]['vCity']; ?>
                                    </td>

                                    <td><?php echo $select_retailer[$i]['vState']; ?>
                                    </td>
                                    <td><?php echo $select_retailer[$i]['vZip']; ?>
                                    </td>

                                </tr>
                                <?php
                                if ($utype == "2" || $utype == "3") {
                                    $_SESSION[$report_file_name][$i]['Account Name'] = $select_retailer[$i]['accountname'];
                                    if ($select_retailer[$i]['storename'] != '') {
                                        $_SESSION[$report_file_name][$i]['Account#'] = $select_retailer[$i]['storename'];
                                    } else {
                                        $_SESSION[$report_file_name][$i]['Account#'] = "--";

                                    }

                                } else {
                                    $_SESSION[$report_file_name][$i]['Vendor Name'] = $select_retailer[$i]['accountname'];
                                }

                                $_SESSION[$report_file_name][$i]['Employee Name'] = $select_retailer[$i]['fname'] . " " . $select_retailer[$i]['lname'];
                                if ($select_retailer[$i]['email'] != '')
                                {
                                    $_SESSION[$report_file_name][$i]['Email'] = $select_retailer[$i]['email'];

                                } else {
                                    $_SESSION[$report_file_name][$i]['Email'] = "Not Available";

                                }

                                if ($utype == "4" || $utype == "6" || $utype == "5") {
                                    if ($select_retailer[$i]['role'] != '') {
                                        $_SESSION[$report_file_name][$i]['Role'] = $select_retailer[$i]['role'];
                                    } else {
                                        $_SESSION[$report_file_name][$i]['Role'] = "--";
                                    }


                                }
                                if ($select_retailer[$i]['contact'] != '') {
                                    $_SESSION[$report_file_name][$i]['Contact no'] = $select_retailer[$i]['contact'];

                                } else {
                                    $_SESSION[$report_file_name][$i]['Contact no'] = "000000000000000000000";

                                }


                                $_SESSION[$report_file_name][$i]['Address'] = $select_retailer[$i]['vAddress'];
                                $_SESSION[$report_file_name][$i]['city'] = $select_retailer[$i]['vCity'];
                                $_SESSION[$report_file_name][$i]['state'] = $select_retailer[$i]['vState'];
                                $_SESSION[$report_file_name][$i]['zip'] = $select_retailer[$i]['vZip'];
                                ?>
                            <?php } ?>
                            </tbody>

                        </table>
							</div>
                    </div>
                    <div class="text-right">
                        <button id="downloadexcelBlutton" name="downloadexcelBlutton" type="submit"
                                class="download-excel-btn btn btn-success1"
                                onclick="location.href='index.php?file=sf-download_contact&file_name=<?php echo $report_file_name; ?>'">
                            Download Excel
                        </button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- page end-->
</section>
</section>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog-brodcast">
        <div class="modal-content">
            <div class="modal-header red-light-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Help</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="frmadd_view_promotion1" name="frmadd12">
                            <center>
                                <img src="<? echo $dafault_image_logo ?>" height="70px" width="70px"></center>
                            <center>

                                <center><h2>Contact</h2></center>
                                <? echo $info_icon[0]['tDescription'] ?>
                        </form>
                        </br>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#contact_table').dataTable({


        });
    });
</script>
<?php

include_once($admin_path . 'js_datatable.php');
?>
<script src="<?php echo $admin_url; ?>assets/js/advanced-form.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstra<<?php

        include_once($admin_path . 'js_datatable.php');
        ?>
?php

include_once($admin_path . 'js_datatable.php');
?>
p-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<!--<script type="text/javascript" language="javascript"
        src="<?php /*echo $admin_url; */?>assets/js/advanced-datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript"
        src="<?php /*echo $admin_url; */?>assets/js/advanced-datatable/js/jquery-1.11.1.min.js"></script>-->


<script src="<?php echo $assets_url?>js/icheck-init.js"