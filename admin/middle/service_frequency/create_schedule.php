<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:51 PM
 */
include_once($inc_class_path . 'orders.class.php');
$ordersObj = new orders();


include_once($inc_class_path . 'user.class.php');
$userObj = new user();

$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';

$mode1 = $_REQUEST['mode'];

if ($iId != '') {
    include_once($inc_class_path . 'schedule.class.php');
    $scheduleObj = new schedule();
    $db_res=$scheduleObj->select($iId);

}

include_once($inc_class_path . 'info.class.php');
$info = new info();

$info_icon = $info->select_report_info("service_frequency_report");



$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$utype = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
$MODE_TYPE = ucfirst($mode);
$MODULE = 'Schedule';
$PRE_LINK = 'index.php?file=sf-schedulelist';
$ACT_LINK = 'index.php?file=sf-create_schedule_a';

if($mode=="add")
{
    $display='style="display: none"';
    $display1='style="display: none"';
}
else
{

    $display="";
    $display1='style="display: none"';
    // echo $db_res[0]['eSchedule'];
    if($db_res[0]['eSchedule']=="Daily")
    {

        $display='style="display: none"';
        $display1='style="display: none"';
    }

    else if($db_res[0]['eSchedule']=="2 days a week")
    {

        $display1="";
    }
}
?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <li>
                        <a href="index.php?file=sf-service_schedule"> Service Schedule</a>
                    </li>

                    <li>
                        <a class="current" href="javascript:;"> <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Form Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Form Section Header Starts -->
                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail
                        </h4>
                    </header>
                    <!-- Form Section Header Ends -->

                    <!-- Form Section Body Starts -->
                    <div class="panel-body">

                        <form class="form-horizontal bucket-form" method="post" name="admin_form" id="schedule_form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <input type="hidden" name="mode" value="<?php echo $mode; ?>">
                            <input type="hidden" name="iScheduleId" id="iScheduleId"
                                   value="<?php echo $db_res[0]['iScheduleId']; ?>">
                            <input type="hidden" name="retailer" id="retailer"
                                   value="<?php echo $iUserId; ?>">

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Select Vendor</label>
                                <div class="col-md-8">
                                    <?php

                                    if ($utype == "2" || $utype == "3") {
                                        $vendor = $ordersObj->select_dist_man1($iUserId);
                                    } else {
                                        $vendor = $ordersObj->select_retailer1($iUserId);
                                    }?>
                                    <select class="form-control" name="vendor" id="vendor" onchange="get_emp(this.value)">

                                        <option selected disabled>- Select Vendor -</option>
                                        <?php for($i=0;$i<count($vendor);$i++)
                                        {?>
                                            <option
                                                value="<?php echo $vendor[$i]['iUserId']?>" <? echo ($db_res[0]['iDistributorId']==$vendor[$i]['iUserId']) ? 'Selected' :""?>>
                                                <?php echo $vendor[$i]['vAccountName'];?>
                                            </option>

                                        <?}?>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Select Employee</label>
                                <div class="col-md-8">

                                    <select class="form-control" name="iEmployeeId" id="iEmployeeId">

                                        <option selected disabled>- Select Employee -</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Select Schedule</label>
                                <div class="col-md-8">

                                    <select class="form-control" name="schedule" id="schedule" onchange="get_day(this.value)">

                                        <option selected disabled>- Select Schedule -</option>
                                        <?$val=$obj->getEnumFields('schedule','eSchedule');


                                        for($i=0;$i<count($val);$i++)
                                        {
                                            ?><option
                                            value="<?php echo $val[$i]?>" <? echo ($db_res[0]['eSchedule']==$val[$i]) ? 'Selected' :""?>>
                                            <?php echo $val[$i]?>
                                            </option>
                                        <?}?>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group" name="daydiv" id="daydiv" <?echo $display?>>
                                <label class="col-md-2 control-label"><span class="red"> *</span> Select Schedule day</label>
                                <div class="col-md-8">
                                    <?if($db_res[0]['eSchedule']=="2 days a week")
                                    {
                                        $day=explode(",",$db_res[0]['tDays']);
                                        $dayselect=$day[0];
                                        $dayselect1=$day[1];

                                    }
                                    else
                                    {
                                        $dayselect=$db_res[0]['tDays'];
                                    }?>

                                    <select class="form-control" name="day" id="day">

                                        <option selected disabled>- Select Day -</option>
                                        <option  value="0" <?php echo($dayselect == '0' ? 'selected' : ''); ?>> Sunday </option>
                                        <option  value="1" <?php echo($dayselect == '1' ? 'selected' : ''); ?>> Monday </option>
                                        <option  value="2" <?php echo($dayselect == '2' ? 'selected' : ''); ?>> Tuesday </option>
                                        <option  value="3" <?php echo($dayselect == '3' ? 'selected' : ''); ?>> Wednesday </option>
                                        <option  value="4" <?php echo($dayselect == '4' ? 'selected' : ''); ?>> Thursday </option>
                                        <option  value="5" <?php echo($dayselect == '5' ? 'selected' : ''); ?>> Friday </option>
                                        <option  value="6" <?php echo($dayselect == '6' ? 'selected' : ''); ?>> Saturday </option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="day2div" name="day2div" <?php echo  $display1?>>
                                <label class="col-md-2 control-label"><span class="red"> *</span> Select Second Schedule day</label>
                                <div class="col-md-8">

                                    <select class="form-control" name="day2" id="day2">

                                        <option selected disabled>- Select Day -</option>
                                        <option  value="0" <?php echo($dayselect1 == '0' ? 'selected' : ''); ?>> Sunday </option>
                                        <option  value="1" <?php echo($dayselect1 == '1' ? 'selected' : ''); ?>> Monday </option>
                                        <option  value="2" <?php echo($dayselect1 == '2' ? 'selected' : ''); ?>> Tuesday </option>
                                        <option  value="3" <?php echo($dayselect1 == '3' ? 'selected' : ''); ?>> Wednesday </option>
                                        <option  value="4" <?php echo($dayselect1 == '4' ? 'selected' : ''); ?>> Thursday </option>
                                        <option  value="5" <?php echo($dayselect1== '5' ? 'selected' : ''); ?>> Friday </option>
                                        <option  value="6" <?php echo($dayselect1 == '6' ? 'selected' : ''); ?>> Saturday </option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit"
                                            class="btn btn-success1"><?php if ($mode == "add") {
                                            echo "Create Schedule";
                                        } else {
                                            echo "Edit Schedule";
                                        } ?></button>
                                    <?php if ($mode1 != '') {
                                        if ($iId != '') {
                                            $PRE_LINK1 = 'index.php?file=sf-schedulelist';
                                        } else {
                                            $PRE_LINK1 = "index.php?file=sf-service_schedule";
                                        }

                                    } else {

                                        $PRE_LINK1 = 'index.php?file=sf-schedulelist';
                                    }
                                    ?>
                                    <a href="<?php echo $PRE_LINK1; ?>" class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- Form Section Body Starts -->

                </section>
            </div>
        </div>
        <!-- Form Section Ends -->

        <!-- page end-->
    </section>
</section>

<?
include_once($admin_path . 'js_form.php');
?>

<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>

    function get_emp(user_id) {
        var selected = '<?php echo (isset($db_res[0]['iEmployeeId'])) ? $db_res[0]['iEmployeeId'] : ""; ?>';
        $.ajax({
            url: 'index.php?file=sf-create_schedule_a',
            type: 'POST',
            data: {"mode": 'get_emp', "distributor_id": user_id, "selected": selected},
            success: function (result) {
                console.log('sdf : ', result);
                $('#iEmployeeId').html(result);
            }
        });
    }
</script>

<script>
    function get_day(days)
    {
        if(days=="2 days a week")
        {
            daydiv.style.display = "";
            day2div.style.display = "";
        }
        else if(days=="Daily")
        {
            daydiv.style.display = "none";
            day2div.style.display = "none";
        }
        else
        {
            daydiv.style.display = "";
            day2div.style.display = "none";
        }
    }
</script>
<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#schedule_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    vendor: {
                        required: true
                    },
                    iEmployeeId: {
                        required: true
                    },
                    schedule: {
                        required: true
                    }

                },
                messages: {
                    vendor: "Please Select Vendor",
                    iEmployeeId: "Plese Select Employee",
                    schedule: {
                        required: "Please Select Schedule"
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        <?if($mode=="edit"){?>
        get_emp('<?php echo $db_res[0]['iEmployeeId']; ?>');
        <?}?>
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#schedule_form').submit();
        });
    });
</script>