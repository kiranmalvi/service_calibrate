<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:29 PM
 */
date_default_timezone_set('Asia/Kolkata');
include_once($inc_class_path . 'orders.class.php');
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$usertype = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
include_once($inc_class_path . 'user.class.php');
$userObj = new user();
$orders = new orders();
$id=$_REQUEST['iuser'];
$date = strtotime(date('Y-m-d 00:00:00'));
$end = strtotime("-180 day", $date);
//$row = $orders->get_history_all($iUserId,$date,$end);
if($id=='') {
    unset($history_report_name);
    $sql = "SELECT o.*,u.*,us.vStoreUniqueId as accountname ,us.vStoreName as storename FROM orders o left join user u on o.iUserId=u.iUserId left join user us on o.iRetailerId=us.iUserId WHERE o.iUserId in(select iUserId from user where iParentId='$iUserId' or iUserId='$iUserId') AND o.eStatus='1' AND o.iDeliveryDate BETWEEN '$end' AND '$date' order by o.iDeliveryDate Desc";
}
else{
    $sql = "SELECT o.*,u.*,us.vStoreUniqueId as accountname ,us.vStoreName as storename FROM orders o left join user u on o.iUserId=u.iUserId left join user us on o.iRetailerId=us.iUserId WHERE o.iUserId in(select iUserId from user where iParentId='$id' or iUserId='$id') AND o.eStatus='1' AND o.iDeliveryDate BETWEEN '$end' AND '$date' order by o.iDeliveryDate Desc";
}
$row=$obj->select($sql);
include_once($inc_class_path . 'help_faq.class.php');
$helpfaqObj = new help_faq();


//left us for distributo&manufacturer(usertype 2 or 3)
//left u for Retailer(usertype  1(parent) 4,5,6)

//pr($row);
//print_r($res_db);
$MODULE = 'History';
$history_report_name = 'history_report';

if($row<0)
{
    echo "No data available in table";
}
?>

<!--main content start-->
<section id="main-content">
<section class="wrapper">
<div class="row">
    <div class="col-md-9">
        <ul class="breadcrumbs-alt">
            <li>
                <a href="index.php"> Dashboard</a>
            </li>
            <li>
                <a class="current" href="javascript:;"><?php echo $MODULE; ?></a>
            </li>
        </ul>
    </div>
    <div class="col-md-3 text-right pull-right">
        <div class="form-group">
            <a href="#myModal" name="SearchReport" data-toggle="modal" class="btn btn-success1" id="help_show" style="display: none">Help
                (?)</a>
        </div>
    </div>
</div>

<!-- Breadcrumbs Ends -->
<!--<div class="row">
    <div class="col-md-3 pull-right">
        <div class="form-group text-right">
            <a href="#myModal" name="SearchReport"  data-toggle="modal" class="btn btn-success1" id="help_show" >Show Help</a>
        </div>
    </div>
</div>-->
<section class="panel">
    <div class="panel-body">
        <div class="square-red single-row">
            <div class="col-md-6">

                <form class="form-horizontal bucket-form" method="post" name="frmadd" id="frmadd"
                      action="<?php echo 'index.php?file=sf-history';?>"
                      enctype="multipart/form-data">
                    <div class="row">
                        <label class="col-md-3 control-label" style="text-align: left;"> Select Retailer </label>
                        <div class="col-md-6" id="stime">
                            <select class="form-control" name="iuser" id="iuser">
                                <option   selected disabled >
                                    --Select Retailer --
                                </option>
                                <?php  $sql_user=$userObj->get_subuser($iUserId);
                                for($i=0;$i<count($sql_user);$i++)
                                {
                                    $selected="";

                                    if($_REQUEST['iuser']==$sql_user[$i]['iUserId']){
                                        $selected="Selected";}
                                    ?>
                                    <option  value="<?php echo $sql_user[$i]['iUserId']?>" <?php echo $selected;?>>
                                        <?php  echo $sql_user[$i]['vFirstName']." ".$sql_user[$i]['vLastName']?>
                                    </option>
                                <?php }?>


                            </select>
                        </div>
                    </div>
            </div>
            <div class=" pull-right">
                <button id="addbutton" type="submit" class="btn btn-success1">View Report</button>

                <button id="resetbutton" type="reset" class="btn btn-success1"
                        onclick="location.href='index.php?file=sf-history&type=reset'">Reset
                </button>
            </div>




            </form>
        </div>
    </div>


    <!-- Table Section Starts -->
    <div class="panel-body">
        <div class="tab-content">
            <div id="today" class="tab-pane active">
                <div class="row">
                    <div class="col-sm-12">
                        <section class="panel">
                            <div class="panel-body">
                                <div class="adv-table">
                                    <div class="table-main-scroll">
                                        <table class="display table table-bordered table-striped" id="history_table">
                                            <!-- Changes HERE Start -->
                                            <thead>
                                            <tr>
                                                <td style="display: none"></td>
                                                <td>Date and Time</td>
                                                <td>Account Name</td>
                                                <td>Account #</td>
                                                <td>Amount</td>
                                                <td>Total Service Time</td>
                                                <td>Vendor Name</td>
                                                <td>Employee Name</td>
                                                <td>Flag</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php

                                            foreach ($row as $key => $ordr) {

                                                $sql_ty=$userObj->select($ordr['iParentId']);
                                                //pr($sql_ty);
                                                ?>
                                                <tr>
                                                    <td style="display: none"><?php echo $key; ?></td>
                                                    <td><?php  echo date('m-d-Y h:i:s A',$ordr['iDeliveryDate']);?></td>
                                                    <td><?php echo $ordr['accountname']; ?></td>
                                                    <td><?php
                                                        if ($ordr['storename'] != '') {
                                                            echo $ordr['storename'];
                                                        } else {
                                                            echo "Not Available";
                                                        }
                                                        //echo $ordr['rstorename'];
                                                        ?></td>
                                                    <td><?php echo $generalfuncobj->get_amount($ordr['fAmount']);?></td>
                                                    <td><?php echo $ordr['iTotalTime'];?></td>
                                                    <td><?php if($ordr['iParentId']==0)
                                                        {

                                                            echo $ordr['vStoreUniqueId'];
                                                        }else {
                                                            echo  $sql_ty[0]['vStoreUniqueId'];
                                                        }
                                                        ?></td>
                                                    <td><?php echo $ordr['vFirstName']." ".$ordr['vLastName']?></td>
                                                    <td>
                                                        <?
                                                        if($ordr['eFlag']=="0")
                                                        {
                                                            $img="green.png";
                                                        }
                                                        else if($ordr['eFlag']=="1") {
                                                            $img = "red.png";
                                                        }
                                                        else
                                                        {
                                                            $img="yellow.png";
                                                        }
                                                        ?>
                                                        <img src="<?echo $admin_url.'images/flag/'.$img;?>" height="15px" width="15px"></td>
                                                    </td>

                                                    <?php
                                                    $_SESSION[$history_report_name][$key]['Date And Time'] =date('d-m-Y h:i:s A', $ordr['iDeliveryDate']);
                                                    $_SESSION[$history_report_name][$key]['Account Name'] = $ordr['accountname'];
                                                    $_SESSION[$history_report_name][$key]['Account #'] = $ordr['storename'];
                                                    $_SESSION[$history_report_name][$key]['Amount'] = $ordr['fAmount'];
                                                    $_SESSION[$history_report_name][$key]['Total Service Time'] = $ordr['iTotalTime'];
                                                    if($ordr['iParentId']==0)
                                                    {

                                                        $_SESSION[$history_report_name][$key]['Vendor Name'] = $ordr['vStoreUniqueId'];
                                                    }else
                                                    {
                                                        $_SESSION[$history_report_name][$key]['Vendor Name'] =  $sql_ty[0]['vStoreUniqueId'];

                                                    }

                                                    $_SESSION[$history_report_name][$key]['Employee Name'] = $ordr['vFirstName'] . $ordr['vLastName'];

                                                    ?>
                                                    <!-- <td><?php /*echo $ordr['eFlag'];*/?></td>-->


                                                </tr>
                                            <?php

                                            } ?>
                                            </tbody>
                                            <!-- Changes HERE Ends -->
                                        </table>
                                    </div>


                                    <div class="text-right pull-right">
                                        <button id="downloadexcelBlutton" name="downloadexcelBlutton" type="submit"
                                                class="btn btn-success1"
                                                onclick="location.href='index.php?file=sf-download_a&file_name=<?= $history_report_name; ?>'">
                                            Download Excel
                                        </button>
                                    </div>

                                </div>

                            </div>

                        </section>

                    </div>
                </div>
            </div>







</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog-brodcast">
        <div class="modal-content">
            <div class="modal-header red-light-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Help</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="frmadd_view_promotion" name="frmadd">
                            <center>
                                <img src="<? echo $dafault_image_logo ?>" height="70px" width="70px"></center>
                            <center><h2> History</h2></center>
                            <center><p class="control-label promoter-name"><h3><? echo $info_icon[0]['vQuestion']; ?></h3> </p>
                                <p class="control-label promoter-name">  <h4><? echo $info_icon[0]['vAnswer'];?></h4></p>


                                <iframe width="420" height="285px"
                                        src="http://www.youtube.com/embed/<?php echo $info_icon[0]['vLink']?>?autoplay=1"></iframe>
                            </center>

                        </form>
                        </br>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Table Section Ends -->

<script>
    $(document).ready(function () {
        $('#history_table').dataTable({
            /*"aoColumnDefs": [
             {'bSortable': false, 'aTargets': [0]}
             ]*/
        });
    });

</script>
<script type="text/javascript" src="http://www.youtube.com/player_api"></script>

<?php
include_once($admin_path . 'js_datatable.php');
?>
