<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:29 PM
 */

include_once($inc_class_path . 'schedule.class.php');
include_once($inc_class_path . 'user.class.php');
$userObj = new  user();


$schedule = new schedule();
$MODULE='Schedule';


$ADD_LINK = 'index.php?file=sf-create_schedule';
$ACT_LINK = 'index.php?file=sf-create_schedule_a';
$atype = $_SESSION['SC_LOGIN']['USER']['iUserId'];
//$db_res = $schedule->select($atype);
$sql = "SELECT u.vFirstName,u.vLastName,s.* FROM schedule s LEFT JOIN user u ON s.iEmployeeId=u.iUserId WHERE s.iRetailerId='$atype' AND s.eStatus='1' order by iDtUpdated";
$db_res=$obj->select($sql);


?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php"> Dashboard</a>
                        </li>
                        <li>
                            <a href="index.php?file=sf-service_schedule"> Service Schedule</a>
                        </li>
                        <li>
                            <a class="current" href="javascript:;"><?php echo $MODULE; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">


                        <!-- Table Section Header Starts -->
                        <header class="panel-heading">
                            <?php echo $MODULE; ?> Listing
                        <span class=" pull-right header-btn">

                             <? if ($atype != "R") { ?>
                                 <a class="btn btn-primary" href="<?php echo $ADD_LINK; ?>">
                                     Add New <?php echo $MODULE; ?>
                                 </a>


                                 <button class="btn btn-danger" onclick="delete_record()">
                                     DELETE
                                 </button>
                             <? } ?>
                        </span>
                        </header>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <?php  if (isset($_SESSION['schedule_error']) && $_SESSION['schedule_error'] != "") {
                                ?>
                                <div class="row">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="alert alert-danger fade in col-md-6" style="font-size: 14px">
                                        <?php echo $_SESSION['schedule_error']; ?>
                                    </div>
                                </div>
                            <? } ?>
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <table class="display table table-bordered table-striped" id="admin_table">
                                        <!-- Changes HERE Start -->
                                        <thead>
                                        <tr>

                                            <td style="display: none"></td>
                                            <td><input type="checkbox" onclick="check_all();"></td>

                                            <td>Action</td>
                                            <td>Employee Name</td>
                                            <td>Schedule</td>
                                            <td>Date</td>
                                            <td>Status</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        for($i=0;$i<count($db_res);$i++) {

                                            ?>
                                            <tr>

                                                <td style="display: none"><?php echo $i; ?></td>

                                                <td>


                                                    <input type="checkbox" name="delete[]"
                                                           value="<?php echo $db_res[$i]['iScheduleId']; ?>">

                                                </td>
                                                <td>
                                                    <a class="btn btn-success btn-xs"
                                                       href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $db_res[$i]['iScheduleId']; ?>"><i
                                                            class="fa fa-edit"></i> Edit</a>


                                                    <a class="btn btn-danger btn-xs"
                                                       href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $db_res[$i]['iScheduleId']; ?>&delete_type=single_delete"><i
                                                            class="fa fa-edit"></i> Delete</a>

                                                </td>

                                                <td><?php echo $db_res[$i]['vFirstName'] . ' ' . $db_res[$i]['vLastName']; ?></td>
                                                <td><?php echo $db_res[$i]['eSchedule']; ?></td>

                                                <td>
                                                    <?php echo date('m-d-Y H:i:s',$db_res[$i]['iDtAdded']);?>
                                                </td>

                                                <td>
                                                    <?php
                                                    if($db_res[$i]['eStatus'] == '1') {
                                                        echo '<span class="label label-success">Active</span>';
                                                    }
                                                     else {
                                                        echo '<span class="label label-danger">Inactive</span>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                        <!-- Changes HERE Ends -->
                                    </table>
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>


<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#admin_table').dataTable({
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>

<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#admin_table').dataTable({
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });
    </script>
<?php
}

include_once($admin_path . 'js_datatable.php');
?>