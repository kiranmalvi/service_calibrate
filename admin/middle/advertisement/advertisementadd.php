<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:51 PM
 */

$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';
if ($iId != '') {
    include_once($inc_class_path . 'advertisement.class.php');

    $advertisement = new advertisement();
    $db_res = $advertisement->select($iId);
}

$MODE_TYPE = ucfirst($mode);
$MODULE = 'Advertisement';
$PRE_LINK = 'index.php?file=ad-advertisementlist';
$ACT_LINK = 'index.php?file=ad-advertisement_a';


?>

<section id="main-content">
    <section class="wrapper">
        <!--main content start-->

        <!-- page start-->

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>
                    <li>
                        <a href="<?php echo $PRE_LINK; ?>"><?php echo $MODULE; ?></a>
                    </li>
                    <li>
                        <a class="current" href="#"><?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Form Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Form Section Header Starts -->
                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail
                        </h4>
                    </header>
                    <!-- Form Section Header Ends -->

                    <!-- Form Section Body Starts -->
                    <div class="panel-body">

                        <form class="form-horizontal bucket-form" method="post" name="admin_form" id="admin_form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <input type="hidden" name="mode" value="<?php echo $mode; ?>">
                            <input type="hidden" name="iAdvertisementId" id="iAdvertisementId"
                                   value="<?php echo $db_res[0]['iAdvertisementId']; ?>">


                            <div class="form-group">
                                <label class="control-label col-md-3">Image Upload</label>

                                <div class="col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">

                                            <? if ($mode == "add") {
                                                ?>
                                                <img src="<? echo $dafault_image_camera; ?>"
                                                     style="width: 150px; height: 150px; alt=""><?
                                            } else if ($db_res[0]['vImage'] == "" || (!is_file($ad_image_path . $db_res[0]['vImage']))) {
                                                ?>
                                                <img src="<?= $dafault_image_logo; ?>" alt="" height="150" width="150">
                                            <? } else { ?>
                                                <img src="<? echo $ad_image_url . $db_res[0]['vImage']; ?>"
                                                     style="width: 2010px; height: 150px; alt="">
                                            <?} ?>
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                                   <span class="btn btn-white btn-file">
                                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                   <span class="fileupload-exists"><i
                                                           class="fa fa-undo"></i> Change</span>
                                                   <input type="file" class="default" id="vImage" name="vImage"
                                                          value="<?php echo $db_res[0]['vImage']; ?>">
                                                   </span>
                                            <a href="#" class="btn btn-danger fileupload-exists"
                                               data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Advertisement Name
                                    :</label>

                                <div class="col-md-8">
                                    <input type="text" name="vAddName" id="vAddName" class="form-control"
                                           value="<?php echo $db_res[0]['vAddName']; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Start Date :</label>


                                <div class="col-md-8">
                                    <input class="form-control form-control-inline input-medium default-date-picker"
                                           type="text" name="iStartDate" id="iStartDate"
                                           value="<?php echo (isset($db_res[0]['iStartDate'])) ? date('m-d-Y', $db_res[0]['iStartDate']) : ''; ?>"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> End Date :</label>


                                <div class="col-md-8">
                                    <input class="form-control form-control-inline input-medium default-date-picker"
                                           type="text" name="iEndDate" id="iEndDate"
                                           value="<?php echo (isset($db_res[0]['iEndDate'])) ? date('m-d-Y', $db_res[0]['iEndDate']) : ''; ?>"/>
                                </div>


                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Status :</label>

                                <div class="col-md-8">
                                    <select class="form-control" name="eStatus" id="eStatus">
                                        <option selected disabled> Select Status</option>
                                        <option
                                            value="1" <?php echo($db_res[0]['eStatus'] == '1' ? 'selected' : ''); ?>>
                                            Active
                                        </option>
                                        <option
                                            value="0" <?php echo($db_res[0]['eStatus'] == '0' ? 'selected' : ''); ?>>
                                            Inactive
                                        </option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <? if ($mode == "add") { ?>
                                        <button id="addbutton" type="submit" class="btn btn-success1">Add</button>
                                    <? } else { ?>
                                        <button id="addbutton" type="submit" class="btn btn-success1">Update</button>

                                    <? } ?>
                                    <a href="<?php echo $PRE_LINK; ?>" class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- Form Section Body Starts -->

                </section>
            </div>
        </div>
        <!-- Form Section Ends -->

        <!-- page end-->
    </section>
</section>

<?php
include_once($admin_path . 'js_form.php');
?>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#admin_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    if ($(element).hasClass('default')) {
                        error.insertAfter($(element).closest('.fileupload-new'));
                    }
                    else {
                        error.insertAfter(element);
                    }
                    //error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    vAddName: {
                        required: true
                    },
                    vImage: {
                        required: true
                    },
                    iStartDate: {

                        required: true
                    },
                    iEndDate: {
                        required: true
                    },
                    eStatus: {
                        required: true
                    }

                },
                messages: {
                    vAddName: "Please Enter Advertisement Name",
                    vImage: "Please Select Image ",
                    iStartDate: "Please Enter StartDate",
                    iEndDate: "Please Enter EndDate",
                    eStatus: "Please Select Status"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#admin_form').submit();
        });
    });

</script>
