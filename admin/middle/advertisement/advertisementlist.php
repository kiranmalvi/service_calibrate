<section id="main-content">
    <section class="wrapper">
        <?php
        /**
         * Created by PhpStorm.
         * User: chintan
         * Date: 26/3/15
         * Time: 5:29 PM
         */

        include_once($inc_class_path . 'advertisement.class.php');
        $advertisement = new advertisement();
        $db_res = $advertisement->select();
        //echo "<pre>"; print_r($db_res);exit;


        $MODULE = 'Advertisement';
        $ADD_LINK = 'index.php?file=ad-advertisementadd';
        $ACT_LINK = 'index.php?file=ad-advertisement_a';
        $atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];


        ?>


        <!--main content start-->


        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>
                    <li>
                        <a class="current" href="#"><?php echo $MODULE; ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Table Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Table Section Header Starts -->
                    <header class="panel-heading">
                        <?php echo $MODULE; ?> Listing
                        <span class=" pull-right header-btn">
                              <? if ($atype != "R") { ?>
                            <a class="btn btn-success1" href="<?php echo $ADD_LINK; ?>">
                                Add New <?php echo $MODULE; ?>
                            </a>


                            <button class="btn btn-success1" onclick="delete_record()">
                                DELETE
                            </button>
                            <? } ?>
                        </span>
                    </header>
                    <!-- Table Section Header Ends -->

                    <!-- Table Section Body Starts -->
                    <div class="panel-body">
                        <div class="adv-table">
                            <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                <input type="hidden" name="mode" id="mode" value="">
                                <input type="hidden" name="delete_type" value="multi_delete">
                                <div class="admin-tbl-main">
                                <table class="display table table-bordered table-striped" id="ad_table">
                                    <!-- Changes HERE Start -->
                                    <thead>
                                    <tr>
                                        <? if ($atype != "R") { ?>
                                        <td><input type="checkbox" onclick="check_all();"></td>
                                        <td>Action</td>
                                        <? } ?>
                                        <td>Advertisement Name</td>
                                        <td>Image</td>
                                        <td>Start Date</td>
                                        <td>End Date</td>
                                        <td>Status</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($db_res as $ADDVERTISEMENT) {
                                        ?>
                                        <tr>
                                            <? if ($atype != "R") { ?>
                                            <td>
                                                <input type="checkbox" name="delete[]"
                                                       value="<?php echo $ADDVERTISEMENT['iAdvertisementId']; ?>">
                                            </td>
                                            <td>
                                                <a class="btn btn-success btn-xs edit-delete-btn"
                                                   href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $ADDVERTISEMENT['iAdvertisementId']; ?>"><i
                                                        class="fa fa-edit"></i> Edit</a>
                                                <a class="btn btn-danger btn-xs edit-delete-btn"
                                                   href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $ADDVERTISEMENT['iAdvertisementId']; ?>&delete_type=single_delete"><i
                                                        class="fa fa-edit"></i> Delete</a>
                                            </td>
                                            <? } ?>
                                            <td><?php echo $ADDVERTISEMENT['vAddName'] ?></td>

                                            <!--        for user default image-->
                                            <? if ($ADDVERTISEMENT['vImage'] == "" || (!is_file($ad_image_path . $ADDVERTISEMENT['vImage']))) { ?>
                                                <td><img src="<?= $dafault_image_logo; ?>" alt="" height="50"
                                                         width="70"></td>
                                            <? } else { ?>
                                                <!--         fro user uploaded image-->
                                                <td><img src="<?= $ad_image_url . $ADDVERTISEMENT['vImage']; ?>" alt=""
                                                         height="50"
                                                         width="70"></td>
                                            <? } ?>
                                            <td><?= $generalfuncobj->gmt_Date_Time_FormatWithoutTime($ADDVERTISEMENT["iStartDate"]); ?></td>
                                            <td><?= $generalfuncobj->gmt_Date_Time_FormatWithoutTime($ADDVERTISEMENT["iEndDate"]); ?></td>

                                            <td>
                                                <?php
                                                if ($ADDVERTISEMENT['eStatus'] == '1') {
                                                    echo '<span class="label label-success edit-active">Active</span>';
                                                } else {
                                                    echo '<span class="label label-danger edit-active">Inactive</span>';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                    <!-- Changes HERE Ends -->
                                </table>
											</div>                            
                            </form>
                        </div>
                    </div>
                    <!-- Table Section Body Ends -->

                </section>
            </div>
        </div>
        <!-- Table Section Ends -->

    </section>
</section>


<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#ad_table').dataTable({
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>
<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#ad_table').dataTable({
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });
    </script>
<?php

}
include_once($admin_path . 'js_datatable.php');
?>