<?php
include_once($inc_class_path . 'user_mapping.class.php');
$user_mappingObj =new  user_mapping();



$PRE_LINK = 'index.php?file=ln-linkaccount';
$ACT_LINK = 'index.php?file=ln-linkaccount_a';

//$all_retailers  = $user_mappingObj->get_all_retailers("retailer","");
#pr($all_retailers);exit;

?>



<script type="text/javascript">

    function select_radio_mi(val)
    {

        document.getElementById('settype').value=val;

        $.ajax({
            url:"index.php?file=ln-linkaccount_a",
            type:'POST',
            data:{mode:"link_data",val:val},
            success: function (result) {
                var data = JSON.parse(result);

                $('#select1').html(data.OUTPUT);


                //document.getElementsByClassName("ms-elem-selectable").style.display="none";
            }

        });

        if(val=="1" || val=="6" )
        {
            document.getElementById("user").style.display="none";
        }
        else
        {
            document.getElementById("user").style.display="";
        }


    }


</script>


<section id="main-content">
<section class="wrapper">


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog-brodcast">
        <div class="modal-content">
            <div class="modal-header red-light-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">View Promotion</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="frmadd_view_promotion" name="frmadd">


                        </form>
                        </br>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <ul class="breadcrumbs-alt">
            <li>
                <a href="index.php">Dashboard</a>
            </li>
            <li>
                <a class="" href="index.php?file=ln-linkaccount"></i>
                    Link Account
                </a>
            </li>
            <li>
                <a class="current" href="javascript:;"></i>
                   Add Link Account
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <section class="panel">

            <header class="panel-heading red-bg">
                <h4 class="gen-case">
                    <? echo "Link Account"; ?>
                </h4>
            </header>

            <div class="panel-body">

                <form class="form-horizontal bucket-form" method="post" name="frmadd" id="frmadd1"
                      action="index.php?file=p-promotions_a" enctype="multipart/form-data">

                    <input type="hidden" name="type" id="type" value="<?php echo $type; ?>">
                    <input type="hidden" name="iId" id="iId" value="<?php echo $iId; ?>">
                    <input type="hidden" name="mode" id="mode" value="<?php echo $mode; ?>">


                    <div class="col-md-6">
                        <div class="radio mi-fld-fx-wdth">
                            <input tabindex="1" id="rtr"
                                   type="radio" <? echo($_SESSION['Search']['radio'] == "1" ? 'checked' : ''); ?>
                                   name="select_radio" id="select_radio" value="1" onchange="select_radio_mi(this.value)">
                            <label class="control-label"> Retailer to Retailer </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="radio mi-fld-fx-wdth">
                            <input tabindex="1"
                                   type="radio" <? echo($_SESSION['Search']['radio'] == "1" ? 'checked' : ''); ?>
                                   name="select_radio" id="select_radio" onchange="return select_radio_mi(this.value)" value="2">
                            <label class="control-label"> Retailer to User </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="radio mi-fld-fx-wdth">
                            <input tabindex="1"
                                   type="radio" <? echo($_SESSION['Search']['radio'] == "1" ? 'checked' : ''); ?>
                                   name="select_radio" id="select_radio" onchange="return select_radio_mi(this.value)" value="6">
                            <label class="control-label"> Manufacturer to Manufacturer </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="radio mi-fld-fx-wdth">
                            <input tabindex="1"
                                   type="radio" <? echo($_SESSION['Search']['radio'] == "1" ? 'checked' : ''); ?>
                                   name="select_radio" id="select_radio" onchange="return select_radio_mi(this.value)" value="3">
                            <label class="control-label"> Manufacturer to User </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="radio mi-fld-fx-wdth">
                            <input tabindex="2" value="5" id="select_radio" type="radio" <? echo  ($_SESSION['Search']['radio']=="2"  ? 'checked' : ''); ?>  name="select_radio" onchange="return select_radio_mi(this.value)">
                            <label class="control-label"> Distributor to Manufacturer </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="radio mi-fld-fx-wdth">
                            <input tabindex="1"
                                   type="radio" <? echo($_SESSION['Search']['radio'] == "1" ? 'checked' : ''); ?>
                                   name="select_radio" id="select_radio" onchange="return select_radio_mi(this.value)" value="4">
                            <label class="control-label"> Distributor to User</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="radio mi-fld-fx-wdth">

                        </div>
                    </div>

                </form>
            </div>

        </section>

        <section class="panel">
            <div class="panel-body">
                <form class="form-horizontal bucket-form" method="post" name="link_form"
                      id="link_form"
                      action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">
                    <input type="hidden" name="settype" id="settype" value="">
                    <input type="hidden" name="mode" id="mode" value="add">

                    <div class="form-group">
                        <label class="col-md-2 control-label"><span class="red"> *</span>Account :</label>

                        <div class="col-md-8" id="selection1">
                            <select class="form-control" name="select1" id="select1" onchange="getusers(this.value);">
                                <option value="">-Select Account-</option>

                            </select>

                        </div>
                    </div>



                    <div class="form-group" id="user" style="display: none">
                        <label class="col-md-2 control-label"><span class="red"> *</span>Link Account :</label>

                        <div class="col-md-8" id="selection2">
                            <select class="form-control" name="select2" id="select2" >
                                <option value="">-Select Link Account-</option>

                            </select>

                        </div>
                    </div>






                    <div class="form-group" id="linkto" name="linkto">
                        <label class="col-md-2 control-label"><span class="red"> *</span> Link To :</label>

                        <div class="col-md-9">
                            <select multiple="multiple" class="multi-select"
                                    id="my_multi_select8" name="linkto[]">
                                <option value="0" disabled>Select Reference</option>

                            </select>
                            <input type="hidden" name="Child_Products_Map_OLD"
                                   value="<?php echo implode(',', $child_product); ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button id="addbutton" type="submit"
                                    class="btn btn-success1">Add
                                </button>
                            <a href="<?php echo $PRE_LINK; ?>" class="btn btn-default1">Back</a>
                        </div>
                    </div>
                </form>
            </div>
        </section>

        <script type="text/javascript"
                src="<?php echo $admin_url; ?>assets/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>

        <script src="<?php echo $admin_url; ?>assets/js/jquery-tags-input/jquery.tagsinput.js"></script>

        <script src="<?php echo $admin_url; ?>assets/js/select2/select2.js"></script>


        <?php

        include_once($admin_path . 'js_datatable.php');
        ?>
        <? include_once($admin_path . 'js_form.php');?>
        <script type="text/javascript"
                src="<?php echo $admin_url; ?>assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>
        <script type="text/javascript"
                src="<?php echo $admin_url; ?>assets/js/jquery-multi-select/js/jquery.quicksearch.js"></script>
        <script src="<?php echo $admin_url; ?>assets/js/select-init.js"></script>

        <script type="text/javascript">
            function getusers(val)
            {
                 var type = $('input:radio[name=select_radio]:checked').val();

                if(type=="1" || type=="6") {
                    $.ajax({
                        url: "index.php?file=ln-link_a",
                        type: "post",
                        data: {mode: "link_user", type: type, user: val},
                        success: function (result) {
                            //console.log(result);
                            //console.log("============================");

                            var data = JSON.parse(result);
                            console.log(data.OUTPUT);
                            $('#my_multi_select8').multiSelect();
                            $('#my_multi_select8').html(data.OUTPUT); // clear out old list
                            $('#my_multi_select8').multiSelect('refresh');

                        }

                    });
                }
                else if(type=="5") {

                    $.ajax({
                        url: "index.php?file=ln-link_a",
                        type: "post",
                        data: {mode: "link_user", type: type, user: val},
                        success: function (result) {
                            //console.log(result);
                            //console.log("============================");

                            var data = JSON.parse(result);
                            console.log(data.OUTPUT);


                            $('#my_multi_select8').multiSelect();
                            $('#my_multi_select8').html(data.OUTPUT); // clear out old list
                            $('#my_multi_select8').multiSelect('refresh');

                        }

                    });

                    $.ajax({
                        url: "index.php?file=ln-link_a",
                        type: "post",
                        data: {mode: "link_employee", type: type, user: val},
                        success: function (result) {
                            //console.log(result);
                            //console.log("============================");

                            var data = JSON.parse(result);
                            console.log(data.OUTPUT);

                            $('#select2').html(data.OUTPUT);

                        }

                    });
                }

                else
                {
                    $.ajax({
                        url: "index.php?file=ln-link_a",
                        type: "post",
                        data: {mode: "link_employee", type: type, user: val},
                        success: function (result) {
                            console.log(result);
                            //console.log("============================");

                            var data = JSON.parse(result);

                            $('#select2').html(data.OUTPUT);

                            $('#my_multi_select8').multiSelect();
                            $('#my_multi_select8').html(data.OUTPUT); // clear out old list
                            $('#my_multi_select8').multiSelect('refresh');

                        }

                    });
                }

            }
        </script>
