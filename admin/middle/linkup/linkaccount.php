<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:29 PM
 */

include_once($inc_class_path . 'user_mapping.class.php');
$user_mappingObj =new  user_mapping();

$db_res_check ="SELECT  a.*,us.vStoreUniqueId as username,
        GROUP_CONCAT(b.vStoreUniqueId ORDER BY b.iUserId) name
FROM    user_mapping a
        INNER JOIN user b
            ON FIND_IN_SET(b.iUserId, a.iUserLinkId) > 0
             left join user us on us.iUserId=a.iUserId
             where a.eStatus!='2'
GROUP  BY a.iUser_mappingId order by a.iUser_mappingId desc ";
$db_res= $obj->select($db_res_check);
//print_r($de_res);
$MODULE = 'Link Account';
$ADD_LINK = 'index.php?file=ln-linkedup_add';
$ACT_LINK = 'index.php?file=ln-linkaccount_a';
$Edit_LINK="index.php?file=ln-linkup_edit";
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];

?>



    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php"> Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="javascript:;"><?php echo $MODULE; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <!-- Table Section Header Starts -->
                        <header class="panel-heading">
                            <?php echo $MODULE; ?> Listing
                        <span class=" pull-right header-btn">

                             <? if ($atype != "R") { ?>
                                 <a class="admin-btn btn btn-success1" href="<?php echo $ADD_LINK; ?>">
                                     Add New <?php echo $MODULE; ?>
                                 </a>


                                 <button class="admin-btn btn btn-success1" onclick="delete_record()">
                                     DELETE
                                 </button>
                             <? } ?>
                        </span>
                        </header>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <div class="admin-tbl-main">
                                        <table class="display table table-bordered table-striped" id="admin_table">
                                            <!-- Changes HERE Start -->
                                            <thead>
                                            <tr>
                                               
                                                <? if ($atype != "R") { ?>
                                                    <td width="3%"><input type="checkbox" onclick="check_all();"></td>
                                                    <td>Action</td> <? } ?>
                                                <td>User</td>
                                                <td>Link Up Accounts</td>
                                                <td>Type</td>
                                                <td>Date</td>
                                                <td>Status</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($db_res as $key=>$LINK) {

                                                ?>
                                                <tr>
                                                   
                                                    <? if ($atype != "R") { ?>
                                                        <td width="3%">

                                                                <input type="checkbox" name="delete[]"
                                                                       value="<?php echo $LINK['iUser_mappingId']; ?>">

                                                        </td>
                                                        <td>
                                                            <a class="btn btn-success btn-xs edit-delete-btn" href="<?php echo $Edit_LINK; ?>&mode=update&iId=<?php echo $LINK['iUser_mappingId']; ?>"><i
                                                                    class="fa fa-edit"></i> Edit</a>

                                                                <a class="btn btn-danger btn-xs edit-delete-btn" href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $LINK['iUser_mappingId']; ?>&delete_type=single_delete"><i
                                                                        class="fa fa-edit"></i> Delete</a>

                                                        </td>
                                                    <? } ?>
                                                    <td><?php echo $LINK['username']; ?></td>
                                                    <td><?php echo $LINK['name']; ?></td>
                                                    <?if ($LINK['eType']=='1'){
                                                        $LINKTYPE="Retailer To Retailer";
                                                    }else if($LINK['eType']=='2'){
                                                        $LINKTYPE="Retailer to User";
                                                    }else if($LINK['eType']=='3'){
                                                        $LINKTYPE="Manufacturer to User";
                                                    }else if($LINK['eType']=='4'){
                                                        $LINKTYPE="Distributor to User";
                                                    }else if($LINK['eType']=='5'){
                                                        $LINKTYPE="Distributor to Manufacturer";
                                                    }

                                                    else if($LINK['eType']=='6'){
                                                        $LINKTYPE="Manufacturer to Manufacturer";
                                                    }?>

                                                    <td><?php echo $LINKTYPE; ?></td>
                                                    <td><?php  echo date('m-d-Y H:i:s', $LINK['iDtAdded']);?>

                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($LINK['eStatus'] == '1') {
                                                            echo '<span class="label label-success edit-active">Active</span>';
                                                        } else {
                                                            echo '<span class="label label-danger edit-active">Inactive</span>';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                            </tbody>
                                            <!-- Changes HERE Ends -->
                                        </table>

                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>

<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#admin_table').dataTable({
            	"aaSorting": [[ 3, "desc" ]]
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>

<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#admin_table').dataTable({
            	"aaSorting": [[ 5, "desc" ]],
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1,2]}
                ]
            });
        });
    </script>
<?php
}

include_once($admin_path . 'js_datatable.php');
?>