<?php

/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 1/4/15
 * Time: 11:26 AM
 */
include_once($inc_class_path . 'gift_card.class.php');
$gift_card_Obj = new gift_card();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();
//$db_res = $coupon->select();
$db_res = $gift_card_Obj->giftcardlist();

include_once($inc_class_path . 'info.class.php');
$info = new info();

$info_icon = $info->select_report_info("redeempoint_web");

/*$DATA = array('name' => 'chintan');
echo $generalfuncobj->get_SC_SEND_mail('forgot_password', $DATA, 'chintan.mind@gmail.com', 'test');

$DATA = array('name' => 'chintan');
echo $generalfuncobj->get_SC_SEND_mail('register', $DATA, 'chintan.mind@gmail.com', 'test');*/
/*
$DATA = array('name' => 'chintan');
echo $generalfuncobj->get_SC_SEND_mail('feedback', $DATA, 'chintan.mind@gmail.com', 'test');
exit;*/
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$points = $referralObj->get_total_point($iUserId);
$point = $points[0]['vPoint'];
//echo print_r($iUserId); exit;
$MODULE = 'Redeem Points';
$ADD_LINK = 'index.php?file=rp-redeem';
$ACT_LINK = 'index.php?file=rp-redeem_a';

$Breadcrumbs = array(
    'index.php' => 'Dashboard',
    '#' => $MODULE
);

?>

    <!--main content start-->
    <section id="main-content" xmlns="http://www.w3.org/1999/html">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->

            <div class="row">
                <div class="col-md-9">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php">Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="javascript:;">Reedem Point </a>
                        </li>
                    </ul>
                </div>


                <div class="col-md-3 pull-right text-right">
                    <div class="form-group">
                        <a href="#myModal2" name="SearchReport" data-toggle="modal" class="btn btn-success1"
                           id="help_show" style="display: none">Help (?)</a>
                    </div>
                </div>
            </div>

            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <header class="panel-heading">
                            Reedem Point


                        </header>
                        <!-- Table Section Header Starts -->
                        <!-- --><?php /*echo $generalfuncobj->TBL_header($MODULE, $ADD_LINK); */ ?>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <div class="table-main-scroll">
                                    <form name="admin_list" id="listing" method="post"
                                          action="<?php echo $ACT_LINK; ?>">
                                        <input type="hidden" name="mode" id="mode" value="">
                                        <input type="hidden" name="delete_type" value="multi_delete">


                                        <div class="col-md-6 col-md-offset-6">

                                            <div class="col-md-12 pull-right text-right">
                                              <span class="pull-right">

                                                      <img src="<?$admin_url?>images/gift_new_icon.png">
                                                      <div class="mini-statnew-info">
                                                          <span class="mini_statnew_points"><?php if($point>0){echo $point;}else{echo "0";}; ?></span><br>
                                                          Points
                                                      </div>

                                                  </span>



                                            </div>

                                        </div>


                                        <!--                                        <TR><TD colspan="4">-->
                                        <?PHP //echo "Points:".$point;?><!--</TD></TR>-->
													<div class="col-md-12">                                        
                                        <table class="display table table-bordered table-striped" id="redeem">
                                            <!-- Changes HERE Start -->

                                            <thead>
                                            <tr>

                                                <td>Name</td>
                                                <td>Points</td>
                                                <td>Description</td>
                                                <td>Redeem</td>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($db_res as $redeem) {

                                                ?>
                                                <tr>
                                                    <td><?php echo $redeem['vGift_CardName']; ?></td>
                                                    <td><?php echo $redeem['vPoints']; ?></td>
                                                    <td><?php echo $redeem['vDescription']; ?></td>
                                                    <td>
                                                        <? if ($point >= $redeem['vPoints']) { ?>
                                                            <a class="btn btn-success btn-xs"
                                                               href="<?php echo $ACT_LINK; ?>&mode=addredeempoints&iId=<?php echo $redeem['iCardId']; ?>&iuserId=<?php echo $iUserId; ?>&points=<?php echo $redeem['vPoints']; ?>">
                                                                <i class="fa fa-plus-circle"></i> Redeem Point
                                                            </a>
                                                        <? } else { ?>
                                                            <a class="btn btn-danger btn-xs" href="#">
                                                                Not Applicable
                                                            </a><? } ?>
                                                    </td>
                                                </tr>

                                            <?php
                                            }
                                            ?>
                                            </tbody>
                                            <!-- Changes HERE Ends -->
                                        </table>
													</div>                                    
                                    
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>
    <script>
        $(document).ready(function () {
            $('#redeem').dataTable({
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog-brodcast">
            <div class="modal-content">
                <div class="modal-header red-light-bg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Help</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="frmadd_view_promotion1" name="frmadd12">
                                <center>
                                    <img src="<? echo $dafault_image_logo ?>" height="70px" width="70px"></center>
                                <center>

                                    <center><h2>Reedem Point</h2></center>
                                    <?echo   $info_icon[0]['tDescription']?>
                            </form>
                            </br>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include_once($admin_path . 'js_datatable.php');
?>