<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 1/4/15
 * Time: 11:26 AM
 */
include_once($inc_class_path . 'redeem.class.php');
$redeem_Obj = new redeem();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();

//$db_res = $coupon->select();
$db_res = $redeem_Obj->redeem_userlist();

//pr($db_res);

$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
//echo print_r($iUserId); exit;
$MODULE = 'Redeem Points';
$ADD_LINK = 'index.php?file=rp-redeem';
$ACT_LINK = 'index.php?file=rp-redeem_a';

$Breadcrumbs = array(
    'index.php' => 'Dashboard',
    '#' => $MODULE
);
?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <?php
            echo $generalfuncobj->Breadcrumbs($Breadcrumbs);
            ?>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Redeem Points

                        </header>

                        <!-- Table Section Header Starts -->
                        <!-- --><?php /*echo $generalfuncobj->TBL_header($MODULE, $ADD_LINK); */ ?>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <div class="admin-tbl-main">
                                    <table class="display table table-bordered table-striped" id="redeemlist_table">
                                        <!-- Changes HERE Start -->
                                        <thead>
                                        <tr>

                                            <td>sr.no</td>
                                            <td>Name</td>
                                            <td>Points</td>
                                            <td>User Name</td>
                                            <td>Status</td>
                                            <td>Date Added</td>
                                            <td>Accept</td>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($db_res as $key => $redeem) {

                                            $point = $redeem['point'];
                                            ?>
                                            <tr>
                                                <td><?php echo $key + 1 ?></td>
                                                <td><?php echo $redeem['vGift_CardName']; ?></td>
                                                <td><?php echo $redeem['vPoints']; ?></td>
                                                <td><?php echo $redeem['vStoreUniqueId']; ?></td>
                                                <td>
                                                    <?php
                                                    if ($redeem['eAccept'] == '1') {
                                                        echo '<span class="label label-success edit-active">Accepted</span>';
                                                    } else {
                                                        echo '<span class="label label-danger edit-active">Pending</span>';
                                                    }
                                                    ?>
                                                </td>
                                                <td><? echo date('m-d-Y', $redeem['iDtAdded']) ?></td>
                                                <td>
                                                    <? if ($redeem['eAccept'] == "0" && $point >= $redeem['vPoints']) { ?>
                                                        <a class="btn btn-success btn-xs edit-active"
                                                           href="<?php echo $ACT_LINK; ?>&mode=accept&iuniquestoreid=<?php echo $redeem['vStoreUniqueId']; ?>&iId=<?php echo $redeem['iUserId']; ?>&iredeemId=<?php echo $redeem['iRedeemId']; ?>&usercoins=<?php echo $redeem['vPoint']; ?>&Totalcoins=<?php echo $redeem['vPoints']; ?>">
                                                            Accept
                                                        </a>
                                                    <?
                                                    } elseif ($redeem['eAccept'] == "1") {
                                                        ?>
                                                        <a class="btn btn-info btn-xs edit-active" href="#">
                                                            Accepted
                                                        </a>
                                                    <?
                                                    } else {
                                                        ?> <a class="btn btn-danger btn-xs edit-active" href="#">
                                                            Not Applicable
                                                        </a>
                                                    <? } ?>

                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                        <!-- Changes HERE Ends -->
                                    </table>
												</div>                                
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>

    <script>
        $(document).ready(function () {
            $('#redeemlist_table').dataTable({

                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>
<?php
include_once($admin_path . 'js_datatable.php');
?>