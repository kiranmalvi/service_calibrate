<?
//echo "<pre>";print_r($_REQUEST); echo "</pre>";
include_once($inc_class_path . 'feature.class.php');
$featureObj = new feature();

include_once($inc_class_path . 'billing_info.class.php');
$billingObj = new billing_info();

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];

$billingAddress = $billingObj->billingAddress($iUserId);
if(count($billingAddress)!=0)
{
    //echo "billingObj "; echo "<pre>";print_r($billingAddress);echo "</pre>";
    $vAddress = $billingAddress['vAddress'];
    $vCity = $billingAddress['vCity'];
    $VState = $billingAddress['VState'];
    $vCountry = $billingAddress['vCountry'];
    $vZip = $billingAddress['vZip'];
}
else
{
    $billingAddress =  $userObj->userInfo($iUserId);
    $vAddress = $billingAddress['vAddress'];
    $vCity = $billingAddress['vCity'];
    $VState = $billingAddress['vState'];
    $vCountry = $billingAddress['iCountryId'];
    $vZip = $billingAddress['vZip'];
    // echo "<pre>";print_r($billingAddress);echo "</pre>";
}

$ptype=$_REQUEST['eUniqueType'];
$updateid=$_REQUEST['oldid'];

if ($ptype == "0") {
    $_SESSION['cityi'] = $_REQUEST['cityref'];
    $cityid = $_REQUEST['cityref'];
    $ctcount=count($_REQUEST['cityref']);
    $featureObj->select('66');
    $price=$featureObj->getfPrice();
    $amount=$ctcount*$price;
    $str = implode(",", $cityid);
}
else if($ptype=="1")
{
    $_SESSION['coni'] = $_REQUEST['countryref'];
    $countryid = $_REQUEST['countryref'];
    $cncount=count($_REQUEST['countryref']);
    $featureObj->select('66');
    $price=$featureObj->getfPrice();
    $amount=$cncount*$price;
    $str = implode(",", $countryid);
}
else if ($ptype=="2")
{
    $_SESSION['stati'] = $_REQUEST['stateref'];
    $stateid = $_REQUEST['stateref'];
    $scount=count($_REQUEST['stateref']);
    $featureObj->select('66');
    $price=$featureObj->getfPrice();
    $amount=$scount*$price;
    $str = implode(",", $stateid);
}
else if ($ptype == "4")
{
    $_SESSION['zipref'] = $_REQUEST['zipref'];
    $zipid = $_REQUEST['zipref'];
    $zcount=count($_REQUEST['zipref']);
    $featureObj->select('66');
    $price=$featureObj->getfPrice();
    $amount=$zcount*$price;
    $str = implode(",", $zipid);
}
else if($ptype=="3")
{
    $_SESSION['retailerref'] = $_REQUEST['retailerref'];
    $rid = $_REQUEST['retailerref'];
    $rcount=count($_REQUEST['retailerref']);
    $featureObj->select('66');
    $price=$featureObj->getfPrice();
    $amount=$rcount*$price;
    $str = implode(",", $rid);
}

?><section id="main-content">
    <section class="wrapper">

        <div class="row">
            <div class="col-md-12 ">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>
                    <li>

                        <a href="index.php?file=p-promotions">
                            Promotions</a>
                    </li>
                    <?if($updateid==""){?>
                        <li>

                            <a href="index.php?file=p-addpromotion&mode=add">
                                Add Promotion</a>
                        </li>
                    <?}else{?>
                        <li>

                            <a href="index.php?file=p-addpromotion&mode=add&mode=update&iId=<?php echo $updateid;?>">
                                Update Promotion</a>
                        </li>
                    <?}?>
                    <li>

                        <a class="current" href="javascript:;"></i> Promotion Payment</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            <? echo  "Promotion Payment"; ?>
                        </h4>
                    </header>

                    <div class="panel-body">



                        <form id="frmpayment" name="frmpayment" method="post" class="form-horizontal bucket-form" action="index.php?file=p-promotions_a">
                            <input type="hidden" value="<?php echo "addPayment";?>" name="mode">
                            <input type="hidden" value="<?php echo $_REQUEST['p'];?>" name="iPromotionId">
                            <input type="hidden" value="<?php echo $_REQUEST['am'];?>" name="amount">
                            <input type="hidden" value="<?php echo $updateid;?>" name="updateid">



                            <div class="form-group">
                                <label class="col-md-2 control-label"> Total Amount  </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="tamount" id="tamount" value="<?php echo "$".$_REQUEST['am'];?>" readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"> Billing Address  </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="vAddress" id="vAddress" value="<?php echo isset($vAddress) ? $vAddress : ''; ?>" >
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label"> City  </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="vCity" id="vCity" value="<?php echo isset($vCity) ? $vCity : ''; ?>" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"> State </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="VState" id="VState" value="<?php echo isset($VState) ? $VState : ''; ?>" >
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label"> Country </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="vCountry" id="vCountry" value="<?php echo isset($vCountry) ? $vCountry : ''; ?>" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"> Zip  </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="vZip" id="vZip" value="<?php echo isset($vZip) ? $vZip : ''; ?>" >
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label"> Card Number </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="card_no" id="card_no">
                                </div></div>
                            <div class="form-group">

                                <label class="col-md-2 control-label"> CSV Code </label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="csv_code" id="csv_code">
                                </div></div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"> Expire Month </label>
                                <div class="col-md-8"><select class="form-control" name="exp_mon" id="exp_mon">
                                        <option value=""> Select expire month</option>
                                        <? for ($i = 1; $i <= 12; $i++) { ?>
                                            <option value="<?php echo $i; ?>"> <?php echo $i; ?></option>
                                        <? } ?>

                                    </select></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"> Expire year for payment </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="exp_year" id="exp_year">
                                        <option value=""> Select expire year for payment</option>

                                        <option value="2015"> 2015</option>
                                        <option value="2016"> 2016</option>
                                        <option value="2017"> 2017</option>
                                        <option value="2018"> 2018</option>
                                        <option value="2019"> 2019</option>
                                        <option value="2020"> 2020</option>
                                        <option value="2021"> 2021</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" name="paymentprm" id="paymentprm" value="Payment" class="btn btn-success1">
                                </div>
                            </div>

                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<?
include_once($admin_path . 'js_form.php');
?>

<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#frmpayment');

            $('#frmpayment').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    exp_year: {
                        required: true
                    },
                    exp_mon: {
                        required: true
                    },
                    csv_code: {
                        required: true,
                        number: true,
                    },
                    card_no: {
                        required: true,
                        minlength: 6
                    },
                    vAddress: {
                        required: true
                    },
                    vCity: {
                        required: true
                    },
                    VState: {
                        required: true
                    },
                    vCountry: {
                        required: true
                    },
                    vZip: {
                        required: true
                    }

                },
                messages: {
                    vAddress: "Please Enter Address",
                    vCity: "Please Enter City Name",
                    VState: "Please Enter State Name",
                    vCountry: "Please Enter Country Name",
                    vZip: "Please Enter Zipcode",
                    exp_year: "Please Enter Expire Year",
                    exp_mon: "Plese Enter Expire Month",
                    csv_code: {
                        required: "Please Enter CSV Code",
                        number: "Please Enter Only digits "
                    },
                    card_no: {
                        required: "Please Enter Card Number",
                        minlength: "Please Enter Only digits."

                    }

                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmpayment) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#paymentprm').click(function () {
            $('#frmpayment').submit();
        });
    });
</script>