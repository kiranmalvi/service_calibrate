<?php


include_once($inc_class_path . 'promotion_right.class.php');
$promotion_right= new promotion_right();

include_once($inc_class_path . 'info.class.php');
$info = new info();

$info_icon = $info->select_report_info("promo_right");


$uid = $_SESSION['SC_LOGIN']['USER']['iUserId'];


$db_res = $promotion_right->select_rightby_user($uid);
//echo "<pre>";
//print_r($db_res);
//print_r($db_res);

//pr($db_res);
//exit;
?>

    <section id="main-content">
    <section class="wrapper">
    <!--main content start-->

    <!-- page start-->

    <div class="row">
        <div class="col-md-9 ">
            <ul class="breadcrumbs-alt">
                <li>
                    <a href="index.php">Dashboard</a>
                </li>
                <li>
                    <a class="current" href="javascript:;">Promotion Rights</a>
                </li>
            </ul>
        </div>
        <div class="col-md-3 pull-right text-right">
            <div class="form-group">
                <a href="#myModal2" name="SearchReport" data-toggle="modal" class="btn btn-success1"
                   id="help_show" style="display: none">Help (?)</a>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-sm-12">
    <section class="panel">
    <header class="panel-heading">
        Promotion Rights

            <span class="pull-right header-btn">
                            <a class="btn btn-success1" data-toggle="modal"
                               href="index.php?file=p-promouser&mode=add">
                                Add New Promotion Right
                            </a>

                 <button class="admin-btn btn btn-success1" onclick="delete_record()">
                     DELETE
                 </button>
                            </span>
         </header>



    <div class="panel-body">
        <div class="adv-table">
            <div class="table-main-scroll">
                <div class="col-md-12">
                    <form name="admin_list" id="listing" method="post" action="index.php?file=p-promouser_a">
                        <input type="hidden" name="mode" id="mode" value="">
                        <input type="hidden" name="delete_type" value="multi_delete">

                    <table class="display table table-bordered table-striped" id="promotion-table">
                        <thead>
                        <tr>
                            <th><input type="checkbox" onclick="check_all();"></th>
                            <th>Action</th>

                            <th>Employee Name</th>
                            <th>City</th>

                            <th>Date Added</th>
                            <th>Status</th>

                        </tr>

                        </thead>
                        <tbody>
                        <?
                        // pr($db_res);
                        for ($i = 0, $ni = count($db_res); $i < $ni; $i++) {

                            ?>
                            <tr class="gradeX">
                                <td>

                                        <input type="checkbox" name="delete[]"
                                               value="<?php echo $db_res[$i]['iPromorightId']; ?>">

                                </td>
                                <td>

                                        <a class="btn btn-success btn-xs edit-delete-btn"
                                           href="index.php?file=p-promouser&mode=update&iId=<?php echo $db_res[$i]['iPromorightId']; ?>"><i
                                                class="fa fa-edit"></i> Edit</a>
                                    <a class="btn btn-danger btn-xs edit-delete-btn" href="index.php?file=p-promouser_a&mode=delete&iId=<?php echo $db_res[$i]['iPromorightId']; ?>&delete_type=single_delete"><i
                                            class="fa fa-trash-o"></i> Delete</a>

                                </td>
                                <td>
                                    <?
                                   echo $db_res[$i]['vFirstName']." ".$db_res[$i]['vLastName'];
                                    ?>

                                </td>

                                <td><? echo $db_res[$i]["vName"] ?> </td>





                                <td><?php
                                    echo date('m-d-Y',$db_res[$i]["iDtAdded"]); ?></td>



                                <td>
                                    <?
                                    if ($db_res[$i]["eStatus"] == "1") {
                                        ?>
                                        <span class="label label-sm label-success">active</span>
                                    <?
                                    } else if ($db_res[$i]["eStatus"] == "0") {
                                        ?>
                                        <span class="label label-sm label-danger">inactive</span>
                                    <?
                                    }
                                    ?>
                                </td>


                        <?php
                        }
                        ?>
                        </tbody>

                    </table>
                        </form>
                </div>
            </div>
        </div>
    </div>
    </section>
    </div>
    </div>
    <!-- page end-->
    </section>
    </section>
    <!--main content end-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog-brodcast">
            <div class="modal-content">
                <div class="modal-header red-light-bg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">View Promotion</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="frmadd_view_promotion" name="frmadd">


                            </form>
                            </br>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog-brodcast">
            <div class="modal-content">
                <div class="modal-header red-light-bg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Promotion Image</h4>
                </div>
                <div class="modal-body">

                    <form id="frmadd_view_rating" name="frmadd">


                    </form>
                    </br>


                </div>
            </div>
        </div>
    </div>





    <script>
        $(document).ready(function(){
            $('#promotion-table').dataTable({
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0]}
                ]
            });
        });

        function get_popup_value(id) {
            $('#frmadd_view_promotion').html($('#div_' + id).html());
            console.log($('#div_' + id).html());
        }
    </script>
    <script>

        function get_popup_value1(id) {
            $('#frmadd_view_rating').html($('#div1_' + id).html());
            console.log($('#div1_' + id).html());
        }

    </script>

    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog-brodcast">
            <div class="modal-content">
                <div class="modal-header red-light-bg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Help</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="frmadd_view_promotion1" name="frmadd12">

                                <center>
                                    <img src="<? echo $dafault_image_logo ?>" height="70px" width="70px"></center>
                                <center>

                                    <center><h2>Promotion Rights</h2></center>
                                    <? echo $info_icon[0]['tDescription'] ?>

                            </form>
                            </br>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
include_once($admin_path . 'js_datatable.php');
$_SESSION['err_payment_promotion']='';
$_SESSION['err_payment_promotion_class']='';
?>