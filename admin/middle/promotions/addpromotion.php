<section id="main-content">
<section class="wrapper">
<?php
include_once($inc_class_path . 'promotion.class.php');
$promotionObj =new  promotion();

include_once($inc_class_path . 'city.class.php');
$cityObj =new  city();

include_once($inc_class_path . 'state.class.php');
$stateObj =new  state();

include_once($inc_class_path . 'country.class.php');
$countryObj =new  country();


include_once($inc_class_path . 'promotion_right.class.php');

$promotion_right= new promotion_right();



$mode = $_REQUEST['mode'];
$iId = $_REQUEST['iId'];


$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];

if ($mode == "add") {
    $header = "Add New";
} else {
    $header = "Update";
    $db_res1 = $promotionObj->select($iId);
    $db_res = $db_res1[0];
}

$type = $_REQUEST['type'];

## Country List
include_once($inc_class_path . 'country.class.php');
$country = new country();
$COUNTRY = $country->select();


include_once($inc_class_path . 'info.class.php');
$info = new info();

$info_icon = $info->select_report_info("promotion_add");

if($mode=="add")
{
    $uid=$_SESSION['SC_LOGIN']['USER']['iUserId'];
    $logo=$promotionObj->get_logo($uid);
    if($logo[0]['vLogo']!="" && file_exists($user_image_path.$logo[0]['vLogo']))
    {
        $logoimg=$user_image_url.$logo[0]['vLogo'];
        $lg=$logo[0]['vLogo'];
    }
    else
    {
        $logoimg=$dafault_image_camera;
    }
}


$parent=$_SESSION['SC_LOGIN']['USER']['iParentId']

?>


<!--main content start-->

<!-- page start-->


<div class="row">
    <div class="col-md-9">
        <ul class="breadcrumbs-alt">
            <li>
                <a href="index.php">Dashboard</a>
            </li>
            <li>

                <a href="index.php?file=p-promotions&type=<? echo $type; ?>">
                    Promotion</a>
            </li>
            <li>

                <a class="current" href="javascript:;"></i> <?
                    echo $header . " ";
                    if ($type == "add") {
                        echo "Add Promotion";

                    } else {
                        echo " Promotion";
                    }?> </a>
            </li>
        </ul>
    </div>
    <div class="col-md-3 pull-right text-right">
        <div class="form-group">
            <a href="#myModal2" name="SearchReport" data-toggle="modal" class="btn btn-success1"
               id="help_show" style="display: none">Help (?)</a>
        </div>
    </div>
</div>

<div class="row">
<div class="col-sm-12">
<section class="panel">

<header class="panel-heading red-bg">
    <h4 class="gen-case">
        <? echo $header . " " . "Promotion"; ?>
    </h4>
</header>

<div class="panel-body">

<form class="form-horizontal bucket-form" method="post" name="frmadd" id="frmadd1"
      action="index.php?file=p-promotions_a" enctype="multipart/form-data">

<input type="hidden" name="type" id="type" value="<?php echo $type; ?>">
<input type="hidden" name="iId" id="iId" value="<?php echo $iId; ?>">
<input type="hidden" name="mode" id="mode" value="<?php echo $mode; ?>">
<input type="hidden" name="image_hid" id="image_hid" value="<?php echo $db_res['vImage']; ?>">
<input type="hidden" name="image_logo_hid" id="image_logo_hid"
       value="<?php echo $db_res['vLogo']; ?>">
<input type="hidden" name="old_logo" id="old_logo"
       value="<?php echo $logo['0']['vLogo'] ?>">

<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Promotion Name
        :</label>

    <div class="col-md-8">
        <input type="text" name="vPromotionName" id="vPromotionName" class="form-control" value="<?php echo $db_res['vPromotionName']; ?>">
    </div>
</div>


<?if($parent=="0")
{?>

<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Promotion By :</label>


    <div class="col-md-8">
        <select class="form-control" id="eUniqueType" name="eUniqueType"
                value="<?php echo $db_res['vPromotionName']; ?>" onchange="ref(this.value)">
            <option value="">Select Promotion By</option>
            <option <?= ($db_res['eUniqueType'] == "3") ? "selected" : "" ?>
                value="3">Retailer
            </option>
            <option <?= ($db_res['eUniqueType'] == "4") ? "selected" : "" ?>
                value="4">Zipcode
            </option>
            <option <?= ($db_res['eUniqueType'] == "0") ? "selected" : "" ?> value="0">
                City
            </option>
            <option <?= ($db_res['eUniqueType'] == "2") ? "selected" : "" ?> value="2">
                State
            </option>
            <option <?= ($db_res['eUniqueType'] == "1") ? "selected" : "" ?>
                value="1">Country
            </option>




        </select>
    </div>
</div>

<div class="form-group" id="retailer" name="retailer" style="display: none">
    <label class="col-md-2 control-label"><span class="red"> *</span> Retailers</label>


    <div class="col-md-9">
        <select multiple="multiple" class="multi-select"
                id="my_multi_select7" name="retailerref[]">


            <? $retailer = $db_res['iRefId'];
            $option = explode(",", $retailer);
            $count = count($option);?>

            <?      $retail_count = $promotionObj->get_retailers($iUserId);
            for ($i = 0; $i < count($retail_count); $i++) {
                for ($j = 0; $j < $count; $j++) {
                    if ($retail_count[$i]['iUserId'] == $option[$j]) {
                        $selected = "Selected";
                        break;

                    } else {

                        $selected = "";
                    }
                }

                ?>

                <option
                    value="<?php echo $retail_count[$i]['iUserId']; ?>" <?php echo $selected; ?>><?php echo $retail_count[$i]['vStoreUniqueId'] ?></option>
            <?php } ?>
        </select>
        <input type="hidden" name="Child_Products_Map_OLD"
               value="<?php echo implode(',', $child_product); ?>">
    </div>
</div>



<div class="form-group" id="city" name="city" style="display: none">
    <label class="col-md-2 control-label"><span class="red"> *</span> City</label>


    <!-- <div class="col-md-8">


                                    <?/* $city = $db_res['iRefId'];
                                    $option = explode(",", $city);
                                    $count = count($option);*/?>


                                    <select name="cityref[]" id="cityref" multiple="multiple"
                                            size="5">
                                        <option value="">Select City</option>
                                        <?/*
                                        $city_count = $cityObj->select();
                                        for ($i = 0; $i < count($city_count); $i++) {
                                            for ($j = 0; $j < $count; $j++) {
                                                if ($city_count[$i]['iCityId'] == $option[$j]) {
                                                    $selected = "Selected";
                                                    break;

                                                } else {

                                                    $selected = "";
                                                }
                                            }

                                            */?>
                                            <option
                                                value="<?php /*echo $city_count[$i]['iCityId']; */?>" <?php /*echo $selected; */?>>
                                                <?php /*echo $city_count[$i]['vName']; */?>
                                            </option>

                                        <?/* } */?>

                                    </select>
                                </div>-->

    <div class="col-md-9">
        <select multiple="multiple" class="multi-select"
                id="my_multi_select3" name="cityref[]">


            <? $city = $db_res['iRefId'];
            $option = explode(",", $city);
            $count = count($option);?>

            <?  $city_count = $cityObj->select();
            for ($i = 0; $i < count($city_count); $i++) {
                for ($j = 0; $j < $count; $j++) {
                    if ($city_count[$i]['iCityId'] == $option[$j]) {
                        $selected = "Selected";
                        break;

                    } else {

                        $selected = "";
                    }
                }

                ?>

                <option
                    value="<?php echo $city_count[$i]['iCityId']; ?>" <?php echo $selected; ?>><?php echo $city_count[$i]['vName']; ?></option>
            <?php } ?>
        </select>
        <input type="hidden" name="Child_Products_Map_OLD"
               value="<?php echo implode(',', $child_product); ?>">
    </div>
</div>


<div class="form-group" id="state" name="state" style="display: none">
    <label class="col-md-2 control-label"><span class="red"> *</span> State</label>

    <div class="col-md-9">
        <select multiple="multiple" class="multi-select"
                id="my_multi_select4" name="stateref[]">


            <? $State = $db_res['iRefId'];
            $option = explode(",", $State);
            $count = count($option);?>

            <?  $state_count = $stateObj->select();
            for ($i = 0; $i < count($state_count); $i++) {
                for ($j = 0; $j < $count; $j++) {
                    if ($state_count[$i]['iStateId'] == $option[$j]) {
                        $selected = "Selected";
                        break;

                    } else {

                        $selected = "";
                    }
                }

                ?>

                <option
                    value="<?php echo $state_count[$i]['iStateId']; ?>" <?php echo $selected; ?>><?php echo $state_count[$i]['vName']; ?></option>
            <?php } ?>
        </select>
        <input type="hidden" name="Child_Products_Map_OLD"
               value="<?php echo implode(',', $child_product); ?>">
    </div>
</div>
<div class="form-group" id="country" name="country" style="display: none">
    <label class="col-md-2 control-label"><span class="red"> *</span> Country</label>

    <div class="col-md-9">
        <select multiple="multiple" class="multi-select"
                id="my_multi_select5" name="countryref[]">

            <? $country = $db_res['iRefId'];
            $options = explode(",", $country);
            $count = count($option);?>
            <?php
            $country_count=$countryObj->select();

            for($i=0;$i<count($country_count);$i++)
            {
                for ($j = 0; $j < $count; $j++) {
                    if ($country_count[$i]['iCountryId'] == $options[$j]) {
                        $selected = "selected";
                        break;
                    } else {

                        $selected = " ";
                    }
                }

                ?>

                <option
                    value="<?php echo $country_count[$i]['iCountryId']; ?>" <?php echo $selected; ?>><?php echo $country_count[$i]['vName']; ?></option>
            <?php } ?>
        </select>
        <input type="hidden" name="Child_Products_Map_OLD"
               value="<?php echo implode(',', $child_product); ?>">
    </div>
</div>


<div class="form-group" id="zip" name="zip" style="display: none">
    <label class="col-md-2 control-label"><span class="red"> *</span> Zipcode</label>

    <div class="col-md-9">
        <select multiple="multiple" class="multi-select"
                id="my_multi_select6" name="zipref[]">

            <? $zip = $db_res['iRefId'];
            $options = explode(",", $zip);
            $count = count($option);?>
            <?php
            $zip_count=$promotionObj->select_zip();

            for($i=0;$i<count($zip_count);$i++)
            {
                for ($j = 0; $j < $count; $j++) {
                    if ($zip_count[$i]['vZip'] == $options[$j]) {
                        $selected = "selected";
                        break;
                    } else {

                        $selected = " ";
                    }
                }

                ?>

                <option
                    value="<?php echo $zip_count[$i]['vZip']; ?>" <?php echo $selected; ?>><?php echo $zip_count[$i]['vZip']; ?></option>
            <?php } ?>
        </select>
        <input type="hidden" name="Child_Products_Map_OLD"
               value="<?php echo implode(',', $child_product); ?>">
    </div>
</div>
<?}else{?>

<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Promotion By :</label>


    <div class="col-md-8">
        <select class="form-control" id="eUniqueType" name="eUniqueType"
                value="<?php echo $db_res['vPromotionName']; ?>" >

            <option selected value="0">
                City
            </option>

        </select>

</div>
</div>


<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> City</label>

<div class="col-md-9">
    <select multiple="multiple" class="multi-select"
            id="my_multi_select3" name="cityref[]">


        <? $city = $db_res['iRefId'];
        $option = explode(",", $city);
        $count = count($option);?>

        <?  $city_count = $promotion_right->get_promotioncity_of_user($iUserId);
        for ($i = 0; $i < count($city_count); $i++) {
            for ($j = 0; $j < $count; $j++) {
                if ($city_count[$i]['iCityId'] == $option[$j]) {
                    $selected = "Selected";
                    break;

                } else {

                    $selected = "";
                }
            }

            ?>

            <option
                value="<?php echo $city_count[$i]['iCityId']; ?>" <?php echo $selected; ?>><?php echo $city_count[$i]['vName']; ?></option>
        <?php } ?>
    </select>
    <input type="hidden" name="Child_Products_Map_OLD"
           value="<?php echo implode(',', $child_product); ?>">
</div>
    </div>





<?php }?>

<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Description :</label>

    <div class="col-md-8">
        <textarea name="vDescription" class="form-control"
                  id="vDescription"><?php echo $db_res['vDescription']; ?></textarea>
    </div>
</div>


<div class="form-group">
    <label class="col-md-2 control-label" id="lblstr"><span class="red"> *</span> Start Date :</label>

    <div class="col-md-8">
        <input class="form-control form-control-inline input-medium default-date-picker"
               type="text" name="iStartDate" id="iStartDate"
               value="<?php echo (isset($db_res['iStartDate'])) ? date('m-d-Y', $db_res['iStartDate']) : ''; ?>"/>

        <div id="strerr">

        </div>
    </div>


</div>

<div class="form-group">
    <label class="col-md-2 control-label" id="lblend"><span class="red"> *</span> End Date :</label>


    <div class="col-md-8">
        <input class="form-control form-control-inline input-medium default-date-picker"
               type="text" name="iEndDate" id="iEndDate" onclick="check_date()"
               value="<?php echo (isset($db_res['iEndDate'])) ? date('m-d-Y', $db_res['iEndDate']) : ''; ?>"/>

        <div id="enderr">

        </div>
    </div>



</div>


<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Status :</label>


    <div class="col-md-8">
        <select class="form-control" id="eStatus" name="eStatus">
            <option <?= ($db_res['eStatus'] == "1") ? "selected" : "" ?> value="1">
                Active
            </option>
            <option <?= ($db_res['eStatus'] == "0") ? "selected" : "" ?>
                value="0">Inactive
            </option>
        </select>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3">Upload Promotion Image</label>
    <div class="col-md-9">
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                <? if ($mode == "add") { ?>
                    <img src="<? echo $dafault_image_camera; ?>"
                         style="width: 150px; height: 150px; alt=""><?}
                else if ($db_res['vImage'] == "" || (!is_file($promotion_image_path . $db_res['vImage'])) ) { ?>
                    <img src="<?= $dafault_image_logo; ?>" alt="" height="150" width="150">
                <?} else { ?>
                    <img src="<? echo $promotion_image_url . $db_res['vImage']; ?>"
                         style="width: 2010px; height: 150px; alt="">

                <?} ?>
            </div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
            <div>
                                                   <span class="btn btn-white btn-file">
                                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                   <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                  <input type="file" class="default" id="vImage" name="vImage"
                                                         value="<?php echo $db_res['vImage']; ?>"
                                                         onchange="Checkfiles(this.value);">
                                                   </span>
                <a href="#" class="btn btn-danger fileupload-exists"
                   data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
            </div>
            <span style="font-size: 15px;font-style: normal" id="error_msg" id="error_msg">upload pictures only in png,jpg,jpeg,gif or bmp format</span>
        </div>

    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3">Logo Upload</label>

    <div class="col-md-9">
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                <? if ($mode == "add") { ?>
                    <img src="<? echo $logoimg; ?>"
                         style="width: 150px; height: 150px;" alt=""><?} else if ($db_res['vLogo'] == "" || (!is_file($promotion_image_path . $db_res['vLogo']))) {
                    ?>
                    <img src="<?= $dafault_image_logo; ?>" alt="" height="150" width="150">
                <? } else { ?>
                    <img src="<? echo $promotion_image_url . $db_res['vLogo']; ?>"
                         style="width: 2010px; height: 150px; alt="">

                <?} ?>
            </div>
            <div class="fileupload-preview fileupload-exists thumbnail"
                 style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
            <div>
                                                   <span class="btn btn-white btn-file">
                                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                   <span class="fileupload-exists"><i
                                                           class="fa fa-undo"></i> Change</span>
                                                   <input type="file" class="default" id="vLogo" name="vLogo"
                                                          value="<?php echo $db_res['vLogo']; ?>"
                                                          onchange="Checkfiles1(this.value);">
                                                   </span>
                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
            </div>
            <span style="font-size: 15px;font-style: normal" id="error_msg1">upload pictures only in png,jpg,jpeg,gif or bmp format</span>
        </div>

    </div>
</div>


<div class="form-group">
    <div class="col-lg-offset-2 col-lg-10">
        <? if ($mode == "add") { ?>
            <button id="addbutton" type="submit" class="btn btn-success1" onclick="check_date();">Add</button>
        <? } else { ?>
            <button id="addbutton" type="submit" class="btn btn-success1" onclick="check_date();">Update</button>
        <? } ?>
        <a href="index.php?file=p-promotions" class="btn btn-default1">Back</a>
    </div>
</div>

</form>

</div>

</section>
</div>
</div>

<!-- page end-->
</section>
</section>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog-brodcast">
        <div class="modal-content">
            <div class="modal-header red-light-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Help</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="frmadd_view_promotion1" name="frmadd12">

                            <center>
                                <img src="<? echo $dafault_image_logo ?>" height="70px" width="70px"></center>
                            <center>

                                <center><h2>Add Promotion</h2></center>
                                <? echo $info_icon[0]['tDescription'] ?>

                        </form>
                        </br>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>





<? include_once($admin_path . 'js_form.php'); ?>

<!-- end: MAIN JAVASCRIPTS -->
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>

    function check_date() {
        var datepickerstart = $("#iStartDate").val();
        var datepickerend = $("#iEndDate").val();

        document.getElementById("enderr").innerHTML = "";
        document.getElementById("lblend").innerHTML = "<font color='#000'> End Date</font>";
        document.getElementById('iEndDate').style.borderColor = '#1FB5AD';
        if (datepickerend !== "") {
            if (datepickerstart > datepickerend) {
                console.log("Start date must be less than End date");
                document.getElementById("enderr").innerHTML = "<font color='#a94442'> End date must be greater than End date .</font>";
                document.getElementById("lblend").innerHTML = "<font color='#a94442'> End Date</font>";
                document.getElementById('iEndDate').style.borderColor = '#a94442';
                return false;
            }
            else {
                var a = moment(datepickerstart, 'M/D/YYYY');
                var b = moment(datepickerend, 'M/D/YYYY');
                var diffDays = b.diff(a, 'days');
                if (diffDays > 30) {
                    document.getElementById("enderr").innerHTML = "<font color='#a94442'> Please Select a date within a month .</font>";
                    document.getElementById("lblend").innerHTML = "<font color='#a94442'> End Date</font>";
                    document.getElementById('iEndDate').style.borderColor = '#a94442';
                    return false;
                }
                else {
                    console.log("Go ahead");
                }
            }
        }
    }

</script>
<script>

function MyRules() {
    var eUniqueType = $('#eUniqueType').val();

    var RULES;
    if (eUniqueType == '0') {
        RULES = CITY_RULES()
    }
    else if (eUniqueType == '1') {
        RULES = COUNTRY_RULES()
    }
    else if (eUniqueType == '2') {
        RULES = STATE_RULES()
    }
    else if (eUniqueType == '3') {
        RULES = RETAILER_RULES()
    }
    else if (eUniqueType == '4') {
        RULES = ZIPCODE_RULES()
    }

    else {
        RULES = Normal_Rules();
    }
    return RULES;
}
function Normal_Rules() {
    var eUniqueType = $('#eUniqueType').val();

    <?if($mode=="add"){?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        },
        vImage: {
            required: true
        }

    };
    <?}else{?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        }

    };
    <?}?>

    return Rules;
}
function CITY_RULES() {
    var eUniqueType = $('#eUniqueType').val();

    <?if($mode=="add"){?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        },
        vImage: {
            required: true
        },
        cityref: {
            required: true
        }

    };
    <?}else{?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        },
        cityref: {
            required: true
        }

    };
    <?}?>

    return Rules;
}
function ZIPCODE_RULES() {
    var eUniqueType = $('#eUniqueType').val();

    <?if($mode=="add"){?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        },
        vImage: {
            required: true
        },
        zipref: {
            required: true
        }

    };
    <?}else{?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        },
        zipref: {
            required: true
        }

    };
    <?}?>

    return Rules;
}
function COUNTRY_RULES() {
    var eUniqueType = $('#eUniqueType').val();

    <?if($mode=="add"){?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        },
        vImage: {
            required: true
        },
        countryref: {
            required: true
        }

    };
    <?}else{?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        },
        countryref: {
            required: true
        }

    };
    <?}?>

    return Rules;
}
function STATE_RULES() {
    var eUniqueType = $('#eUniqueType').val();

    <?if($mode=="add"){?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        },
        vImage: {
            required: true
        },
        stateref: {
            required: true
        }

    };
    <?}else{?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        },
        stateref: {
            required: true
        }

    };
    <?}?>

    return Rules;
}
function RETAILER_RULES()
{
    var eUniqueType = $('#eUniqueType').val();

    <?if($mode=="add"){?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        },
        vImage: {
            required: true
        },
        retailerref: {
            required: true
        }

    };
    <?}else{?>
    var Rules = {
        vPromotionName: {
            required: true
        },
        eUniqueType: {
            required: true
        },
        vDescription: {
            required: true
        },
        iStartDate: {
            required: true
        },
        iEndDate: {
            required: true
        },
        retailerref: {
            required: true
        }

    };
    <?}?>

    return Rules;
}
var validationMessages = {
    vPromotionName: "Please Enter Promotion Name",
    eUniqueType: "Please Select Promotion Type",
    vDescription: "Please Enter Description",
    iStartDate: "Please Select Start Date",
    iEndDate: {
        required: "Please Select End Date",
        minDate: "Select less than"
    },
    vImage: "Please Select Image",
    countryref: "Please Select Country",
    cityref: "Please Select City",
    stateref: "Please Select State",
    zipref: "Please Select State",
    retailerref: "Please Select Retailer"

};


var validation = $('#frmadd1').validate();
validation.settings.messages = validationMessages;
validation.settings.messages = validationMessages;
validation.settings.errorPlacement = function (error, element) {
    error.insertAfter(element);
};
validation.settings.ignore = "";
validation.settings.errorElement = "span";
validation.settings.errorClass = 'help-block';
validation.settings.highlight = function (element) {
    $(element).closest('.help-block').removeClass('valid');
    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
};
validation.settings.unhighlight = function (element) {
    $(element).closest('.form-group').removeClass('has-error');
};
validation.settings.success = function (label, element) {
    label.addClass('help-block valid');
    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
};
validation.settings.submitHandler = function (frmadd) {
    successHandler1.show();
    errorHandler1.hide();
};



//$('#frmadd').submit();



var FormAdminValidator = function () {

    return {
        init: function () {
            validation.settings.rules = MyRules();
        }
    };

}();

//$('#frmadd').submit();
$(document).ready(function () {
//        Main.init();
    <?if($mode=="update"){?>
    ref('<?php echo $db_res['eUniqueType']; ?>');
    <?}?>
    $('#addbutton').click(function () {
        FormAdminValidator.init();
        $('#frmadd1').submit();
    });
});
</script>


<script>
    function ref(val) {
        var reference = val;

        if (reference == 0) {
            retailer.style.display ="none";
            city.style.display = "";
            country.style.display ="none";
            state.style.display = "none";
            zip.style.display = "none";

        }
        else  if (reference == 1) {
            retailer.style.display ="none";
            city.style.display = "none";
            country.style.display ="";
            state.style.display = "none";
            zip.style.display = "none";


        }
        else if (reference == 2) {
            retailer.style.display = "none";
            city.style.display = "none";
            country.style.display ="none";
            state.style.display = "";
            zip.style.display = "none";


        }

        else if (reference == 3) {
            retailer.style.display = "";
            city.style.display = "none";
            country.style.display ="none";
            state.style.display = "none";
            zip.style.display = "none";


        }
        else if (reference == 4) {

            retailer.style.display = "none";
            city.style.display = "none";
            country.style.display ="none";
            state.style.display = "none";
            zip.style.display = "";


        }}
</script>

<script language="javascript">
    function Checkfiles(value1) {
        // alert(value1);
        $.ajax({
            url: 'index.php?file=p-promotions_a',
            type: 'POST',
            data: {"id": value1, "mode": 'jpg_chk'},
            success: function (result) {
                console.log('sdf : ', result);
                $('#error_msg').html(result);
            }

        });


    }
    function Checkfiles1(value1) {
        //alert(value1);
        $.ajax({
            url: 'index.php?file=p-promotions_a',
            type: 'POST',
            data: {"id1": value1, "mode": 'jpg_chk1'},
            success: function (result) {
                console.log('sdf : ', result);
                $('#error_msg1').html(result);
            }

        });


    }
</script>



<script>
    $(document).ready(function () {
        $('#iStartDate').on('changeDate', function(){
            $(this).datepicker('hide');
        });

        $('#iEndDate').on('changeDate', function(){
            $(this).datepicker('hide');
        });

    });


</script>
