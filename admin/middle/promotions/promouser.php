<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:51 PM
 */



$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';
if ($iId != '') {
    include_once($inc_class_path . 'promotion_right.class.php');

    $promotion_right= new promotion_right();

    include_once($inc_class_path . 'user.class.php');

    $userObj= new user();
    $db_res = $promotion_right->select($iId);
}


include_once($inc_class_path . 'city.class.php');
$cityObj =new  city();

$MODE_TYPE = ucfirst($mode);
$MODULE = 'Promotion Right';

$PRE_LINK = 'index.php?file=p-promouserlist';
$ACT_LINK = 'index.php?file=p-promouser_a';


$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];

?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <?if($_REQUEST['page']!='profile'){?>
                        <li>
                            <a href="<?php echo $PRE_LINK; ?>"> <?php echo $MODULE; ?></a>
                        </li>
                    <?}?>
                    <li>
                        <a class="current" href="javascript:;"> <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Form Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Form Section Header Starts -->
                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail
                        </h4>
                    </header>
                    <!-- Form Section Header Ends -->

                    <!-- Form Section Body Starts -->
                    <div class="panel-body">

                        <form class="form-horizontal bucket-form" method="post" name="admin_form" id="admin_form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <input type="hidden" name="mode" value="<?php echo $mode; ?>">
                            <input type="hidden" name="iId" id="iId"  value="<?php echo $db_res[0]['iPromorightId'] ?>">
<div class="row" style="margin-left: 15px;margin-bottom: 15px;">

                                <div class="alert alert-info fade in col-md-9 ">
                                    <strong>Note : </strong>
                                    This selection will give this admin to add promotions for specific areas.Please select employee & select city.
                                </div>
                                </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Employee Name :</label>
                                <div class="col-md-8">
                                <select class="form-control" name="employee" id="employee">
                                    <?php $emp=$userObj->get_subuser($iUserId)?>
                                    <option selected disabled>- Select Employee -</option>
                                    <?php for($i=0;$i<count($emp);$i++)
                                    {?>
                                    <option
                                        value="<?php echo $emp[$i]['iUserId'] ?>" <?php echo($emp[$i]['iUserId'] == $db_res[0]['iPromoIUserId'] ? 'selected' : ''); ?>>
                                        <?php echo $emp[$i]['vFirstName']." ".$emp[$i]['vLastName'] ?>
                                    </option>
                                    <?php }?>

                                </select>
                                    </div>
                            </div>

                            <?php $city_count = $cityObj->select();
                           // pr($city_count);?>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> City:</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="city" id="city">
                                        <?php $city_count = $cityObj->select();?>
                                        <option selected disabled>- Select City -</option>
                                        <?php for($i=0;$i<count($city_count);$i++)
                                        {?>
                                            <option
                                                value="<?php echo $city_count[$i]['iCityId'] ?>" <?php echo($city_count[$i]['iCityId'] == $db_res[0]['iCityId'] ? 'selected' : ''); ?>>
                                                <?php echo $city_count[$i]['vName'] ?>
                                            </option>
                                        <?php }?>

                                    </select>
                                </div>
                            </div>




                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Status :</label>

                                <div class="col-md-8">
                                    <select class="form-control" name="eStatus" id="eStatus">
                                        <option selected disabled>- Select Status -</option>
                                        <option
                                            value="1" <?php echo($db_res[0]['eStatus'] == '1' ? 'selected' : ''); ?>>
                                            Active
                                        </option>
                                        <option
                                            value="0" <?php echo($db_res[0]['eStatus'] == '0' ? 'selected' : ''); ?>>
                                            Inactive
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit"
                                            class="btn btn-success1"><?php if ($mode == "add") {
                                            echo "Add";
                                        } else {
                                            echo "Update";
                                        } ?></button>
                                    <a href="<?php echo $PRE_LINK; ?>" class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- Form Section Body Starts -->

                </section>
            </div>
        </div>
        <!-- Form Section Ends -->

        <!-- page end-->
    </section>
</section>

<?
include_once($admin_path . 'js_form.php');
?>

<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#admin_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    city: {
                        required: true
                    },
                    employee: {
                        required: true
                    },
                    eStatus: {
                        required: true
                    }

                },
                messages: {
                    city: "Please Select City",
                    employee: "Please Select Employee",
                    eStatus: "Please Select Status"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#admin_form').submit();
        });
    });
</script>
