<?php
include_once($inc_class_path . 'promotion.class.php');
$promotionObj = new  promotion();
include_once($inc_class_path . 'user.class.php');
$userObj = new  user();
include_once($inc_class_path . 'generalfunction.class.php');
$generalfuncobj = new generalfunc();

include_once($inc_class_path . 'info.class.php');
$info = new info();

include_once($inc_class_path . 'promotion_right.class.php');
$promotion_right= new promotion_right();

$info_icon = $info->select_report_info("promotion_web");

$type = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
$uid = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$country = $_SESSION['SC_LOGIN']['USER']['iCountryId'];
$state = $_SESSION['SC_LOGIN']['USER']['vState'];
$city = $_SESSION['SC_LOGIN']['USER']['vCity'];
$zip = $_SESSION['SC_LOGIN']['USER']['vZip'];
$userparent = $_SESSION['SC_LOGIN']['USER']['iParentId'];

if ($type == "4" || $type == "5" || $type == "6") {

    // echo "select_Promotion";
    $db_res = $promotionObj->select_Promotion($uid, $country, $state, $city, $zip);
    /*    print_r($db_res);*/

} else {
    //echo "select_promotion_by_man_dist";
    $db_res = $promotionObj->select_promotion_by_man_dist($uid);
    /*    print_r($db_res);*/
}
//echo "<pre>";
//print_r($db_res);
//print_r($db_res);

//pr($db_res);
//exit;
?>

    <section id="main-content">
    <section class="wrapper">
    <!--main content start-->

    <!-- page start-->

    <div class="row">
        <div class="col-md-9 ">
            <ul class="breadcrumbs-alt">
                <li>
                    <a href="index.php">Dashboard</a>
                </li>
                <li>
                    <a class="current" href="javascript:;">Promotions</a>
                </li>
            </ul>
        </div>
        <div class="col-md-3 pull-right text-right">
            <div class="form-group">
                <a href="#myModal2" name="SearchReport" data-toggle="modal" class="btn btn-success1"
                   id="help_show" style="display: none">Help (?)</a>
            </div>
    </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Promotions
                    <? if ($type == "2" || $type == "3") { 
                     $user=$promotion_right->get_promotion($uid);
                        if($user[0]['total']>0 || $userparent=="0")
                        {?>
                        <span class="pull-right header-btn">
                            <a class="btn btn-success1" data-toggle="modal"
                               href="index.php?file=p-addpromotion&mode=add">
                                Add New Promotion
                            </a>
                            </span>
                    <? } }?>
                </header>

                     <?php if($_SESSION['err_payment_promotion_class'] == "SUCCESS") { $style = 'alert-success';
                      }else { $style = 'alert-danger'; }
                     if($_SESSION['err_payment_promotion']!=''){
                     ?>

                <div class="row" style="margin-left: 15px; margin-top: 5px;">
                    <div class="alert  fade in col-md-8 <?php echo $style; ?>">
                        <strong>Status : </strong> <?php echo $_SESSION['err_payment_promotion'];?>
                    </div>
                </div>
<?php }?>
                <div class="panel-body">
                    <div class="adv-table">
                        <div class="table-main-scroll">
                         <div class="col-md-12">
                        	<table class="display table table-bordered table-striped" id="promotion-table">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Posted By</th>
                                <th>Promotion Name</th>
                                <th>Promotion Description</th>
                                <th>
                                    <center>Image</center>
                                </th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Status</th>
                                <th style="display: none"></th>
                            </tr>

                            </thead>
                            <tbody>
                            <?
                            // pr($db_res);
                            for ($i = 0, $ni = count($db_res); $i < $ni; $i++) {

                                ?>
                                <tr class="gradeX">
                                    <td>
                                        <? if ($type == "2" || $type == "3") { ?>
                                            <a class="btn btn-success btn-xs"
                                               href="index.php?file=p-addpromotion&mode=update&iId=<?php echo $db_res[$i]['iPromotionId']; ?>"><i
                                                    class="fa fa-edit"></i> Edit</a>
                                            <a class="btn btn-danger btn-xs"
                                               href="index.php?file=p-promotions_a&mode=delete&iId=<?php echo $db_res[$i]['iPromotionId']; ?>"
                                               id="delete"><i class="fa fa-trash-o"></i> Delete</a>
                                        <? } else { ?>

                                            <a class="btn btn-info btn-xs"  onclick="get_popup_value('<?php echo $db_res[$i]['iPromotionId']; ?>');"
                                               href="#myModal"
                                               data-toggle="modal">View

                                            </a>
                                        <? } ?>
                                    </td>
                                    <td>
                                        <?
                                        $posted=$db_res[$i]['iUserId'];
                                        $user=$userObj->select($posted);
                                        echo $userObj->getvStoreUniqueId();
                                        ?>

                                    </td>

                                    <td><? echo $db_res[$i]["vPromotionName"] ?> </td>
                                    <td><? echo $db_res[$i]["vDescription"] ?> </td>

                                  <!--  <td><?/*if ($db_res[$i]['eUniqueType'] == "0") {
                                            echo "city";
                                        } else if ($db_res[$i]['eUniqueType'] == "1") {
                                            echo "Country";
                                        } else if ($db_res[$i]['eUniqueType'] == "2") {
                                            echo "State";
                                        } else if ($db_res[$i]['eUniqueType'] == "3") {
                                            echo "Retailer";
                                        }*/?></td>-->


                                    <td align="center"><?
                                        if ($db_res[$i]['vImage'] != "") {
                                            $img = $db_res[$i]['vImage'];

                                        } else {
                                            $img = 'no_photo_icon.png';
                                        }
                                       // echo $db_res[$i]['vImage'];
                                        ?>

                                        <?php $id = $db_res[$i]['iPromotionId']; ?>
                                        <a href="#myModal1" onclick=get_popup_value1(<?php echo $id ?>)
                                           data-toggle="modal"><img src="<? echo $img; ?>" height="50px"
                                                                    width="70px"></a>
                                    </td>
                                    <td id="div1_<?php echo $db_res[$i]['iPromotionId']; ?>" style="display: none">
                                        <?
                                        if ($db_res[$i]['vImage'] != "") {
                                            $img = $db_res[$i]['vImage'];

                                        } else {
                                            $img = 'no_photo_icon.png';
                                        }
                                        ?>
                                        <center><img src="<? echo $img; ?>" height="550px" width="610px"></center>

                                    </td>
                                    <td><?php
                                        echo $db_res[$i]["iStartDate"]; ?></td>

                                    <td><?= $db_res[$i]["iEndDate"]; ?></td>

                                    <td>
                                        <?
                                        if ($db_res[$i]["eStatus"] == "1") {
                                            ?>
                                            <span class="label label-sm label-success">active</span>
                                        <?
                                        } else if ($db_res[$i]["eStatus"] == "0") {
                                            ?>
                                            <span class="label label-sm label-danger">inactive</span>
                                        <?
                                        }
                                        ?>
                                    </td>

                                    <div id="div_<?php echo $db_res[$i]['iPromotionId']; ?>" style="display: none">
                                        <div class="view-prmotn-mi">

                                            <img src="<?php echo $promotion_img_url . $db_res[$i]['vImage'] ?>" alt=""
                                                 class="vw-prmtn-prdct-img">

                                            <p class="vw-prmtn-lgo">
                                                <img src="<?php echo $promotion_img_url . $db_res[$i]['vLogo'] ?>"
                                                     alt="">
                                            </p>

                                            <p class="control-label promoter-name"><?php echo $db_res[$i]['vPromotionName']; ?></p>

                                            <p class="control-label prmotn-dscrptn"><?php echo $db_res[$i]['vDescription']; ?></p>

                                            <p class="control-label strt-end-dt">Start Date :-
                                                <span><?php echo $db_res[$i]['iStartDate']; ?></span></p>

                                            <p class=" control-label  strt-end-dt">End Date :-
                                                <span><?php echo "    " . $db_res[$i]['iEndDate']; ?></span></p>


                                            <!--                                                                                                <table>-->
                                                <!--                                                    <tr>-->
                                                <!---->
                                                <!--                                                        <td width="50%">Promotion Name</td>-->
                                                <!--                                                        <td width="70%">--><?php //echo $db_res[$i]['vPromotionName'] ?><!--</td>-->
                                                <!---->
                                                <!--                                                    </tr>-->
                                                <!--                                                    <tr>-->
                                                <!--                                                        <td colspan="2"><img-->
                                                <!--                                                                src="--><?php //echo $promotion_img_url . $db_res[$i]['vImage'] ?><!--"-->
                                                <!--                                                                height="200px" width="200px"></td>-->
                                                <!--                                                    </tr>-->
                                                <!--                                                    <tr>-->
                                                <!---->
                                                <!--                                                        <td width="50%">Description</td>-->
                                                <!--                                                        <td width="70%">--><?php //echo $db_res[$i]['vDescription'] ?><!--</td>-->
                                                <!---->
                                                <!--                                                    </tr>-->
                                                <!--                                                    <tr>-->
                                                <!--                                                        <td width="50%">Start Date:</td>-->
                                                <!--                                                        <td width="50%">--><?php //echo $db_res[$i]['iStartDate'] ?><!--</td>-->
                                                <!--                                                    </tr>-->
                                                <!--                                                    <tr>-->
                                                <!--                                                        <td width="50%">End Date:</td>-->
                                                <!--                                                        <td width="50%">--><?php //echo $db_res[$i]['iEndDate'] ?><!--</td>-->
                                                <!--                                                    </tr>-->
                                                <!--                                                </table>-->

                                            </div>
                                    </div>

                                    <!--<td class="center"><img src="<? /*=$user_image_url.$db_res[$i]["vuserImg"];*/ ?>" alt="" height="50" width="70" ></td>-->
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>

                        </table>
								 </div>                    		
                    		</div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
    </section>
    </section>
    <!--main content end-->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog-brodcast">
            <div class="modal-content">
                <div class="modal-header red-light-bg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">View Promotion</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="frmadd_view_promotion" name="frmadd">


                            </form>
                            </br>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog-brodcast">
            <div class="modal-content">
                <div class="modal-header red-light-bg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Promotion Image</h4>
                </div>
                <div class="modal-body">

                    <form id="frmadd_view_rating" name="frmadd">


                    </form>
                    </br>


                </div>
            </div>
        </div>
    </div>





    <script>
        $(document).ready(function(){
            $('#promotion-table').dataTable({
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0]}
                ]
            });
        });

        function get_popup_value(id) {
            $('#frmadd_view_promotion').html($('#div_' + id).html());
            console.log($('#div_' + id).html());
        }
    </script>
    <script>

        function get_popup_value1(id) {
            $('#frmadd_view_rating').html($('#div1_' + id).html());
            console.log($('#div1_' + id).html());
        }

    </script>

    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog-brodcast">
            <div class="modal-content">
                <div class="modal-header red-light-bg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Help</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="frmadd_view_promotion1" name="frmadd12">

                                <center>
                                    <img src="<? echo $dafault_image_logo ?>" height="70px" width="70px"></center>
                                <center>

                                    <center><h2>Promotions</h2></center>
                                    <? echo $info_icon[0]['tDescription'] ?>

                            </form>
                            </br>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
include_once($admin_path . 'js_datatable.php');
$_SESSION['err_payment_promotion']='';
$_SESSION['err_payment_promotion_class']='';
?>