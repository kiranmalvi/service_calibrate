<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 23/3/15
 * Time: 7:21 PM
 */

?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <li>
                        <a href="index.php?file=b-broadcast"> Broadcast</a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;"> Temporary Close</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            Temporary Close
                        </h4>
                    </header>

                    <div class="panel-body">

                        <form class="form-horizontal bucket-form" method="post" name="frmadd1" id="frmadd1"
                              action="index.php?file=b-broadcast_temporary_close_a" enctype="multipart/form-data">


                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Closing Date :</label>

                                <div class="col-md-8">
                                    <input class="form-control form-control-inline input-medium default-date-picker"
                                           type="text" name="closing_date" id="closing_date" onblur="check_date();">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label" id="lblend"><span class="red"> *</span> Opening
                                    Date :</label>


                                <div class="col-md-8">
                                    <input class="form-control form-control-inline input-medium default-date-picker"
                                           type="text" name="opening_date" id="opening_date" onblur="check_date();">

                                    <div id="enderr">

                                    </div>
                                </div>
                            </div>


                       


                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Message For All
                                        :</label>

                                    <div class="col-md-8">

                                        <textarea name="message" id="message" class="form-control"></textarea>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" id="addbutton" class="btn btn-success1">Broadcast</button>
                                        <a href="index.php?file=b-broadcastadd" class="btn btn-default1">Back</a>
                                    </div>
                                </div>

                        </form>

                    </div>

                </section>
            </div>
        </div>

        <!-- page end-->
    </section>
</section>


<?
include_once($admin_path . 'js_form.php');
?>


<!-- end: MAIN JAVASCRIPTS -->
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    function check_date() {
        // alert("hi");
        var datepickerstart = $("#closing_date").val();
        var datepickerend = $("#opening_date").val();
//        $.ajax({
//            url: 'index.php?file=b-broadcast_temporary_close_a',
//            type: 'POST',
//            data: {"mode1": 'get_opening', "c_date": datepickerstart, "o_date": datepickerend},
//            success: function (result) {
//                console.log('sdf : ', result);
//                $('#enderr').html(result);
//            }
//        });
        // alert(datepickerend,datepickerstart);
        document.getElementById("enderr").innerHTML = "";
        document.getElementById("lblend").innerHTML = "<font color='#000'> Opening Date</font>";
        document.getElementById('opening_date').style.borderColor = '#1FB5AD';
        if (datepickerend != "") {
            if (datepickerstart > datepickerend) {
                console.log("Opening date must be greater than Closing date");
                document.getElementById("enderr").innerHTML = "<font color='#a94442'> Opening date must be greater than Closing date .</font>";
                document.getElementById("lblend").innerHTML = "<font color='#a94442'> Opening Date</font>";
                document.getElementById('opening_date').style.borderColor = '#a94442';
                return false;
            }

//            else {
//                var a = moment(datepickerstart, 'M/D/YYYY');
//                var b = moment(datepickerend, 'M/D/YYYY');
//                var diffDays = b.diff(a, 'days');
//                if (diffDays > 30) {
//                    document.getElementById("enderr").innerHTML = "<font color='#a94442'> Please Select a date within a month .</font>";
//                    document.getElementById("lblend").innerHTML = "<font color='#a94442'> Opening Date</font>";
//                    document.getElementById('opening_date').style.borderColor = '#a94442';
//                    return false;
//                }
//                else {
//                    console.log("Go ahead");
//                }
//            }
        }
        else {
            console.log("Go ahead");
        }
    }

</script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#frmadd1');


            //var errorHandler1 = $('.errorHandler', form1);
            //var successHandler1 = $('.successHandler', form1);
            $('#frmadd1').validate({


                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {

                    message: "required",
                    opening_date: "required",
                    closing_date: "required"
                },
                messages: {

                    message: "Please Enter Message",
                    opening_date: "Please Enter Opening Date",
                    closing_date: "Please Enter Closing Time"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };


        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
//        Main.init();
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#frmadd1').submit();
        });
    });


</script>
<script>
    $(document).ready(function () {
        $('#opening_date').on('changeDate', function () {
            $(this).datepicker('hide');
        });
        $('#closing_date').on('changeDate', function () {
            $(this).datepicker('hide');
        });


    });


</script>
