<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 23/3/15
 * Time: 7:21 PM
 */

?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <li>
                        <a href="index.php?file=b-broadcast"> Broadcast</a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;"> Add New Location</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            Add New Location
                        </h4>
                    </header>

                    <div class="panel-body">

                        <form class="form-horizontal bucket-form" method="post" name="frmadd" id="frmadd1"
                              action="index.php?file=b-broadcast_new_store_a" enctype="multipart/form-data">

                            <!--                            <div class="form-group">-->
                            <!--                                <label class="col-md-2 control-label"><span class="red"> *</span> Location Address-->
                            <!--                                    :</label>-->
                            <!---->
                            <!--                              <div class="col-md-8">-->
                            <!--                                    <textarea name="location_address" class="form-control"-->
                            <!--                                              id="location_address"></textarea>-->
                            <!--                                </div>-->
                            <!--                            </div>-->

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Street Address
                                    :</label>

                                <div class="col-md-8">
                                    <input type="text" name="street_address" id="street_address" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Zip :</label>

                                <div class="col-md-8">
                                    <input type="text" name="zip" id="zip" class="form-control"
                                           onblur="get_loc(this.value)">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> City :</label>

                                <div class="col-md-8">
                                    <input type="text" name="city" id="city" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> State :</label>

                                <div class="col-md-8">
                                    <input type="text" name="state" id="state" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Contact Name:</label>

                                <div class="col-md-8">
                                    <input type="text" name="contact" id="contact" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Phone :</label>

                                <div class="col-md-8">
                                    <input type="text" name="phone" id="phone" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Opening Date :</label>

                                <div class="col-md-8">
                                    <input class="form-control default-date-picker"
                                           type="text" name="opening_date" id="opening_date">
                            </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Message For All
                                    :</label>

                                <div class="col-md-8">
                                    <textarea name="message" id="message" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit" class="btn btn-success1">Broadcast</button>
                                    <a href="index.php?file=b-broadcastadd" class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>
                    </div>


                </section>
            </div>
        </div>

        <!-- page end-->
    </section>
</section>


<?
include_once($admin_path . 'js_form.php');
?>
<!--<script src="<?php /*echo $assets_url */ ?>js/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php /*echo $assets_url */ ?>bs3/js/bootstrap.min.js"></script>-->
<script src="<?php echo $assets_url ?>js/jquery.blockUI.js"></script>
<script src="<?php echo $assets_url ?>js/iCheck/jquery.icheck.min.js"></script>

<script src="<?php echo $assets_url ?>js/less-1.5.0.min.js"></script>

<!-- end: MAIN JAVASCRIPTS -->
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>


<!-- end: MAIN JAVASCRIPTS -->
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>


<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#frmadd1');


            //var errorHandler1 = $('.errorHandler', form1);
            //var successHandler1 = $('.successHandler', form1);
            $('#frmadd1').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    street_address: {
                        minlength: 2,
                        required: true
                    },
                    location_address: {
                        minlength: 2,
                        required: true
                    },
                    phone: {
                        required: true,
                        number: true,
                        minlength: 6,
                        maxlength: 12

                    },
                    zip: "required",
                    city: "required",
                    state: "required",
                    contact: "required",
                    message: "required",
                    opening_date: "required"

                },
                messages: {

                    street_address: "Please Enter Street Address",
                    location_address: "Please Enter Location Address",
                    zip: "Please Enter Zip code",
                    city: "Please Enter City",
                    state: "Please Enter State",
                    // contact: "Please Enter Contact",
                    phone: {
                        required: "Please Enter Phone",
                        number: "Enter Only digits",
                        minlength: "Enter Minimum 6 digits",
                        maxlength: "Enter Maximum 12 digits"

                    },
                    contact: "Please Enter Contact Name",
                    message: "Please Enter Message",
                    opening_date: "Please Enter Opening Date",


                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
//        Main.init();
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#frmadd1').submit();
        });
    });
</script>
<link rel="stylesheet" type="text/css" href="<?= $admin_url ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.css"/>
<script type="text/javascript" src="<?= $admin_url ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.js"></script>


<!--css & js for timepicker-->

<script>
    function get_loc(loc) {

        $.ajax({
            url: 'index.php?file=su-subuseradd_a',
            type: 'POST',
            data: {"mode": 'get_zipcode', "zipcode": loc},
            success: function (result) {
                var data = JSON.parse(result);
                var location = data['results'][0]['formatted_address'].split(',');
                console.log(location);

                var cnt = location.length;
                console.log(cnt);
                if (cnt == "3") {
                    var statetrim = location[1].trim();
                    var state = statetrim.split(" ");
                    $('#state').val(state[0]);
                    $('#city').val(location[0]);
                }
                else {

                    $('#state').val("");
                    $('#city').val("");
                }
                //$('#iCountryId').val(data['results'][0]['address_components'][4]['long_name']);

            }
        });
    }
</script>

<script>
    $('.timepicker-default').timepicker({});
</script>


<link rel="stylesheet"
      href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/timepicker.css">
<link rel="stylesheet"
      href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/datetimepicker.css">
<script src="<?php echo $assets_url ?>plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?php echo $assets_url ?>plugins/bootstrap-timepicker/js/jquery-ui-timepicker-addon.js"></script>

<script>
    $(document).ready(function () {
        $('#opening_date').on('changeDate', function () {
            $(this).datepicker('hide');
        });


    });

</script>
