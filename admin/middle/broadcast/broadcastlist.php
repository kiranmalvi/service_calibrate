<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 24/3/15
 */

include_once($inc_class_path . 'user.class.php');
$userObj = new user();
$type=$_REQUEST['type'];

$id=$_SESSION['SC_LOGIN']['ADMIN']['iAdminId'];

if($type=="1")
{
     $type="4,5,6";
}
else
{
    $type=$type;
}

 $sql = "SELECT *,GROUP_CONCAT(iMessageId SEPARATOR ',')as id  FROM message WHERE eMessage_Type= 'b' AND eDeleted='' AND iSenderId In(select iUserId from user where iUserTypeId IN ($type) )Group By vMessage";


$ADD_LINK = 'index.php?file=b-broadcastedit';
$ACT_LINK = 'index.php?file=b-broadcastad_a';
$db_res = $obj->select($sql);
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];

?>
    <!--main content start-->
    <section id="main-content">

        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-md-9">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php"> Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="javascript:;"> Broadcast</a>
                        </li>
                    </ul>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Broadcast
  <span class=" pull-right header-btn">

                             <? if ($atype != "R") { ?>


                                 <button class="admin-btn btn btn-success1" onclick="delete_record()">
                                     DELETE
                                 </button>
                             <? } ?>
                        </span>



                        </header>
                        <div class="panel-body">
                            <div class="adv-table">
                                <div class="table-main-scroll">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select class="form-control" name="eType" id="eType" onchange="getbroadcast(this.value);">
                                                    <option value="1" <?php echo($_REQUEST['type'] == '1' ? 'selected' : ''); ?>> All</option>
                                                    <option value="4" <?php echo($_REQUEST['type'] == '4' ? 'selected' : ''); ?>> Single Location Owner</option>
                                                    <option value="5" <?php echo($_REQUEST['type'] == '5' ? 'selected' : ''); ?>> Multiple Location Owner</option>
                                                    <option value="6" <?php echo($_REQUEST['type'] == '6' ? 'selected' : ''); ?>> Corporate Account</option>
                                                    </select>
                                        </div>
                                            <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                                <input type="hidden" name="delete_type" value="multi_delete">


                                        <table class="display table table-bordered table-striped" id="broadcast_table">
                                            <thead>
                                            <tr>
                                                <!--                                            <th style="display: none"></th>-->
                                                <!--   <th>Broadcast For</th>-->
                                                <!--                                            --><?// if(isset($_SESSION['SC_LOGIN']['USER'])) {
                                                //                                                if($type=="4" || $type=="5" || $type=="6"){
                                                ?>
                                                <!--                                                    <th>Action</th>-->
                                                <!--                                                --><? //}?>
                                                <!--                                            --><? //}?>




                                                <th style="display: none"></th>
                                                <? if ($atype != "R") { ?>
                                                    <th style="width:4%"><input type="checkbox" onclick="check_all();"></td>
                                                    <th style="width:15%">Action</td>
                                                <?}?>
                                                <th width="5%">Account Name</th>
                                                <th width="3%">Account #</th>
                                                <th width="5%">User Name</th>
                                                <th width="7%">Broadcast Type</th>
                                                <th width="30%">Message</th>
                                                <th width="5%">Date Added</th>


                                            </tr>
                                            </thead>
                                            <tbody>

                                            <? for ($i = 0; $i < count($db_res); $i++) {

                                                ?>

                                                <tr>
                                                    <td style="display: none"></td>
                                                    <? if ($atype != "R") { ?>
                                                        <td style="width:4%">
                                                            <input type="checkbox" name="delete[]"
                                                                   value="<?php echo $db_res[$i]['id']; ?>">
                                                        </td>
                                                        <td style="width:15%">
                                                            <a class="btn btn-success btn-xs edit-delete-btn" href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $db_res[$i]['id']; ?>"><i
                                                                    class="fa fa-edit"></i> Edit</a>


                                                                <a class="btn btn-danger btn-xs edit-delete-btn" href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $db_res[$i]['id']; ?>&delete_type=single_delete"><i
                                                                        class="fa fa-edit"></i> Delete</a>



                                                        </td>
                                                    <?}?>

                                                    <!--                                                 <td>-->
                                                    <!--                                        --><?php //$type = $db_res[$i]['eType'];
                                                    //                                        if ($type == 0) {
                                                    //                                            $typename = "New Store Opening";
                                                    //                                        } elseif ($type == 1) {
                                                    //                                            $typename = "Temporary Close";
                                                    //                                        } elseif ($type == 2) {
                                                    //                                            $typename = "Permanent Close";
                                                    //                                        } elseif ($type == 3) {
                                                    //                                            $typename = "Change of Ownership";
                                                    //                                        } else {
                                                    //                                            $typename = "";
                                                    //                                        }
                                                    //
                                                    //                                        echo $typename;*/
                                                    ?>
                                                    <!--                                    </td>-->

                                                    <!--                                                --><?// if(isset($_SESSION['SC_LOGIN']['USER'])) {
                                                    //                                                    if($type=="4" || $type=="5" || $type=="6"){
                                                    ?>
                                                    <!--                                                        <td>-->
                                                    <!--                                                            <a class="btn btn-danger btn-xs"-->
                                                    <!--                                                               href="-->
                                                    <?php //echo $ACT_LINK; ?><!--&mode=delete&iId=-->
                                                    <?php //echo $db_res[$i]['iMessageId']; ?><!--&delete_type=single_delete&id=-->
                                                    <?php //echo $id;?><!--"><i-->
                                                    <!--                                                                    class="fa fa-edit"></i> Delete</a>-->
                                                    <!--                                                        </td>-->
                                                    <!--                                                    --><?php
                                                    //
                                                    //                                                    }
                                                    ?>
                                                    <!--                                                --><? //}?>
                                                    <td width="5%">
                                                        <?php $uid = $db_res[$i]['iSenderId'];
                                                        $uname = $userObj->getStoreName($uid);
                                                        echo $uname[0]['vStoreUniqueId'];


                                                        ?></td>
                                                    <td width="3%"><?php echo $uname[0]['vStoreName']; ?></td>
                                                    <td width="4%"><?php echo $uname[0]['vFirstName'] . " " . $uname[0]['vLastName']; ?></td>
                                                    <?php
                                                    $str = $db_res[$i]['vMessage'];

                                                    $result = split("[\n]", $str);

                                                    //                                    if($result[0] == "New")
                                                    //                                    {
                                                    //                                        $result1 = $result[0]." ".$result[1]." ".$result[2];
                                                    //                                    }
                                                    //                                    else
                                                    //                                    {
                                                    //                                       // echo $result[1];
                                                    //                                        $result2 = preg_split('/[W,T]/',$result[1]);
                                                    //                                        $result1 = $result[0]." ".$result2[0];
                                                    //                                    }

                                                    ?>
                                                    <td width="7%"><?php echo $result[0] ?></td>
                                                    <td width="30%"><?php echo $db_res[$i]['vMessage']; ?></td>
                                                    <td width="5%"><?php echo date('m-d-Y', $db_res[$i]['iDateAdded']); ?></td>

                                                </tr>
                                            <? } ?>
                                            </tbody>

                                        </table>
                                                </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <!--main content end-->

    <!-- Modal -->


    <!-- modal -->

    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog-brodcast">
            <div class="modal-content">
                <div class="modal-header red-light-bg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Help</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="frmadd_view_promotion1" name="frmadd12">
                                <center>
                                    <img src="<? echo $dafault_image_logo ?>" height="70px" width="70px"></center>
                                <center>

                                    <center><h2>Broadcast</h2></center>
                                    <? echo $info_icon[0]['tDescription'] ?>
                            </form>
                            </br>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

if (($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R')) {
    ?>
    <script>
        $(document).ready(function () {
            $('#broadcast_table').dataTable({
"aaSorting": [[ 6, "desc" ]]
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>

<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#broadcast_table').dataTable({
            	"aaSorting": [[ 8, "desc" ]],
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0]}
                ]
            });
        });
    </script>

<?php
}
include_once($admin_path . 'js_datatable.php');
?>
<script>

    function getbroadcast(val)
    {
        window.location.href = 'index.php?file=b-broadcastlist&type='+val;
    }

</script>
