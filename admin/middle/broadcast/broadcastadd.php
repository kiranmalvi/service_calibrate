<section id="main-content">

    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <li>
                        <a href="index.php?file=b-broadcast"> Broadcast</a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;"> Add New Broadcast</a>
                    </li>

                </ul>
            </div>
        </div>
        <div class="panel">
            <header class="panel-heading"> Broadcast</header>
            <div class="row add-broadcast-mi">
                <div class="col-md-6 col-md-offset-3">
                    <button onclick="location.href='index.php?file=b-broadcast_new_store_opening'" type="button"
                            class="nwstropng">
                        New Location Opening
                    </button>
                    </div>
                <div class="col-md-6 col-md-offset-3">
                    <button onclick="location.href='index.php?file=b-broadcast_temporary_close'" type="button"
                            class="tmprycls">
                        Temporary Close
                    </button>
                    </div>
                <div class="col-md-6 col-md-offset-3">
                    <button onclick="location.href='index.php?file=b-broadcast_permanent_close'" type="button"
                            class="prmntcls">
                        Permanent Close
                    </button>
                    </div>
                <div class="col-md-6 col-md-offset-3">
                    <button onclick="location.href='index.php?file=b-broadcast_ownership_change'" type="button"
                            class="ownrshpchng">
                        Ownership Change
                    </button>
                    </div>
            </div>
        </div>
                </div>
                    </div>

                </div>
            </center>
        </div>


    </section>
</section>
<?php
include_once($admin_path . 'js_datatable.php');
?>