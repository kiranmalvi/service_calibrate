<?php
include_once("../include.php");
//echo $inc_class_path;exit;
include_once($inc_class_path . 'register.class.php');
$registerObj = new register();

ob_get_clean();
$type       =   $_REQUEST['type'];
$eventid    =   $_REQUEST['eventid'];
$userid     =   $_REQUEST['userid'];
$message     =  urlencode($_REQUEST['message']);
$from     =  $_REQUEST['from'];
$check     =  $_REQUEST['check'];
//pr($_REQUEST);exit;
$html   =   "";



#$message     =  str_replace(' ','%20',$_REQUEST['message']);
if($from == "send")
{

    if($type    ==  "checkin")
    {
        if($check == "true")
        {
            $check_val  =   "in";
        }
        else{
            $check_val  =   "out";
        }
        $user_data  =   $registerObj->select($userid);
        //pr($user_data);exit;

        if($user_data['vTimeZone'] == '')
        {
            date_default_timezone_set($DEFAULT_TIMEZONE);
        } else {
            date_default_timezone_set($user_data['vTimeZone']);
        }
//echo "TIME". $user_data['vTimeZone'];exit;
        $today  =   date('h:i A F d,Y');

        $username   =   ucfirst($user_data['vFirstName']).' '.ucfirst($user_data['vLastName']);
        $message    =  $check_val;
        //$message    =   $today." check ".$check_val." ".$username;
        $message = str_replace(' ','%20',$message);
    }


     //echo $site_url . "api/?version=1&tag=sendmessage&userid=" . $userid . "&eventid=".$eventid."&type=".$type."&message=".$message;//exit;
    $response_api_call = json_decode(file_get_contents($site_url . "api/?version=1&tag=sendmessage&userid=" . $userid . "&eventid=" . $eventid . "&type=" . $type . "&message=" . $message), true);
    //pr($response_api_call);exit;
}

if($from == "load")
{
    $last_checked_timestamp =   $_REQUEST['last_checked_timestamp'];
    $last_loaded_timestamp =   $_REQUEST['last_loaded_timestamp'];
    $no_of_rows =   $_REQUEST['no_of_rows'];
    $response_api_call = json_decode(file_get_contents($site_url . "api/?version=1&tag=chatlistwithoutlogin&userid=" . $userid . "&eventid=" . $eventid . "&no_of_rows=" . $no_of_rows . "&last_checked_timestamp=" . $last_checked_timestamp . "&last_loaded_timestamp=" . $last_loaded_timestamp), true);
//pr($response_api_call);//exit;


$user_data  =   $registerObj->select($userid);
        //pr($user_data);exit;

        if($user_data['vTimeZone'] == '')
        {
            date_default_timezone_set($DEFAULT_TIMEZONE);
        } else {
            date_default_timezone_set($user_data['vTimeZone']);
        }

    //pr($response_api_call['response']['data']);exit;
    if(count($response_api_call['response']['data']) > 0){
        $chat_list1  =   array_reverse($response_api_call['response']['data']);

        $new_timestamp = $response_api_call['response']['new_checked_timestamp'];


        foreach($chat_list1 as $chat_data)
        {
            $chat_list[date('d F Y',$chat_data['added'])][] = $chat_data;
        }

        foreach ($chat_list as $k=>$chat_list_arr1) {

            $html   .=   '<div class="mi-chat-date"><span>'.$k.'</span></div>';

            if(count($chat_list) > 0) {


                foreach ($chat_list_arr1 as $chat_list_arr) {
                    if ($chat_list_arr['type'] == "checkin") {

                        $start_date         =   $chat_list_arr['added'];
                        $start_date_f       =   date('h:i A F d,Y',$start_date);
                        $html   .=   '<div class="mi-chat-date-name"><span>' . $start_date_f.' check '.$chat_list_arr['message'] .' '.ucfirst($chat_list_arr["username"]). '</span></div>';

                    }
                    else{

                        #pr($chat_list_arr);exit;
                        if ($chat_list_arr['thumb_image'] == "") {
                            $user_image = $default_web_user_thumb_image;
                        } else {
                            $user_image = $chat_list_arr['thumb_image'];
                        }

                    $chat_list_arr['added'];
                    $todate_msg = strtotime(date('Y-m-d H:i:s'));
                    $sender_date_msg = $chat_list_arr['added'];
                    $remining = $todate_msg - $sender_date_msg;

                    $remaining_time_hours = $generalfuncobj->func_date_remind($remining);
                    $remaining_time_hours; //exit;

                    $remaining_time_hours_arr = explode(":", $remaining_time_hours);

                    // pr($remaining_time_hours_arr);exit;

                    if ($remaining_time_hours_arr[0] > 0) {
                        if ($remaining_time_hours_arr[0] == 1) {
                            $display = $remaining_time_hours_arr[0] . " day ago";
                        } else {
                            $display = $remaining_time_hours_arr[0] . " days ago";
                        }
                    } else if ($remaining_time_hours_arr[1] > 0) {
                        if ($remaining_time_hours_arr[1] == 1) {
                            $display = $remaining_time_hours_arr[1] . " hour ago";
                        } else {
                            $display = $remaining_time_hours_arr[1] . " hours ago";
                        }
                    } else if ($remaining_time_hours_arr[2] > 0) {
                        if ($remaining_time_hours_arr[2] == 1) {
                            $display = $remaining_time_hours_arr[2] . " min ago";
                        } else {
                            $display = $remaining_time_hours_arr[2] . " mins ago";
                        }
                    } else if ($remaining_time_hours_arr[3] > 0) {
                        if ($remaining_time_hours_arr[3] == 1) {
                            $display = $remaining_time_hours_arr[3] . " sec ago";
                        } else {
                            $display = $remaining_time_hours_arr[3] . " sec ago";
                        }
                    } else if ($remaining_time_hours_arr[4] > 0) {
                        $display = $remaining_time_hours_arr[4] . " sec ago";
                    }

                    if ($chat_list_arr['user_id'] == $userid) {

                        $html .= '<div class="mi-msg-frm-rgt">';
                        $html .= '<div class="mi-msg-main">' . $chat_list_arr['message'];
                        $html .= '</div>';
                        $html .= '<span class="mi-msg-tm">' . $display . '</span>';

                        $html .= '<div class="mi-msg-usr-dtl">';
                        $html .= '<img src="' . $user_image . '" alt="" class="image mi-msg-usr-img" /><br>';
                        $html .= '<span class="mi-msg-usrnm">' . $chat_list_arr["username"] . '</span>';
                        $html .= '</div>';
                        $html .= '</div>';

                        $html .= '</div>';
                    } else {
                        $html .= '<div class="mi-msg-frm-lft">';
                        $html .= '<div class="mi-msg-main">' . $chat_list_arr['message'];
                        $html .= '</div>';
                        $html .= '<span class="mi-msg-tm">' . $display . '</span>';

                        $html .= '<div class="mi-msg-usr-dtl">';
                        $html .= '<img src="' . $user_image . '" alt="" class="image mi-msg-usr-img" /><br>';
                        $html .= '<span class="mi-msg-usrnm">' . $chat_list_arr["username"] . '</span>';
                        $html .= '</div>';
                        $html .= '</div>';
                    }
                }
                }
            }

        }

    }
    $html .= '<div id="append"></div>';
}



echo $html;
//print_r($response_api_call);exit;
?>
