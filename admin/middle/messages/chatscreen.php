<script src="<?php echo $site_url;?>js/jquery.min.js"></script>
<?php
//pr($_SERVER);exit;

include_once($inc_class_path . 'register.class.php');
$registerObj = new register();

include_once($inc_class_path . 'event.class.php');
$eventObj = new event();

//$generalfuncobj->func_check_session();

$event_id   =   base64_decode($_REQUEST['id']);


$chatresponse = $_REQUEST['chat'];
//$no_of_rows =   $_REQUEST['no_of_rows'];
$no_of_rows =   '0';
$last_checked_timestamp =   0;
$last_loaded_timestamp =   0;
$chat_list  =   array();


if (isset($_SESSION[$session_prefix . 'front_user_iUserId']) && $_SESSION[$session_prefix . 'front_user_iUserId'] != "") {
    $user_id = $_SESSION[$session_prefix . 'front_user_iUserId'];

    $response_api_call = json_decode(file_get_contents($site_url . "api/?version=1&tag=chatlist&userid=" . $user_id . "&eventid=" . $event_id . "&no_of_rows=" . $no_of_rows . "&last_checked_timestamp=" . $last_checked_timestamp . "&last_loaded_timestamp=" . $last_loaded_timestamp), true);

} else {
    $user_id = base64_decode($_REQUEST['uid']);

    $response_api_call = json_decode(file_get_contents($site_url . "api/?version=1&tag=chatlistwithoutlogin&userid=" . $user_id . "&eventid=" . $event_id . "&no_of_rows=" . $no_of_rows . "&last_checked_timestamp=" . $last_checked_timestamp . "&last_loaded_timestamp=" . $last_loaded_timestamp), true);

}
$user_data  =   $registerObj->regular_user_data_array($user_id);

$event_data =   $eventObj->select($event_id);
$event_user_data =   $eventObj->get_specific_event_data($event_id,$user_id);
//pr($event_user_data);exit;
$user_event_status  =   $event_user_data['iUserEventData']['eType'];
$event_eStatus =   $event_data[0]['eStatus'];
//pr($event_data);exit;
//pr($user_data);exit;

if ($user_data['vThumbImage'] == "") {
    $image = $default_web_user_thumb_image;
} else {
    $image = $user_data['vThumbImage'];
}

if($user_data['vTimeZoneString'] == '')
{
    date_default_timezone_set($DEFAULT_TIMEZONE);
} else {
    date_default_timezone_set($user_data['vTimeZoneString']);
}

/*echo $site_url . "api/?version=1&tag=chatlist&userid=" . $user_id . "&eventid=" . $event_id . "&no_of_rows=" . $no_of_rows . "&last_checked_timestamp=" . $last_checked_timestamp . "&last_loaded_timestamp=" . $last_loaded_timestamp;exit;*/

//pr($response_api_call);exit;
//echo $site_url . "/api/?version=1&tag=chatlist&userid=" . $user_id . "&eventid=".$event_id."&no_of_rows=".$no_of_rows."&last_checked_timestamp=".$last_checked_timestamp."&last_loaded_timestamp=".$last_loaded_timestamp;exit;

//pr($response_api_call);exit;
array_reverse($response_api_call['response']['data']);

$chat_list1  =   array_reverse($response_api_call['response']['data']);
//pr($chat_list1);exit;
$new_timestamp = $response_api_call['response']['new_checked_timestamp'];

foreach($chat_list1 as $chat_data)
{
    $chat_list[date('d F Y',$chat_data['added'])][] = $chat_data;
}
//pr($chat_list);exit;
//exit;
//pr($chat_list);exit;
//exit;


//  IF USER LOGIN AND CLICK ON EMAIL UVITE DETAIL LINK AND FROM THAT USER REDIRECT TO CHAT AND IF USER NOT EXIST IN EVENT THAN DISPLAY MSG OVER SEND AREA
$edata2 =   array();
$_SESSION[$session_prefix . 'front_user_iUserId'];
$edata2 = $eventObj->get_data_user_and_friends_from_event_id($event_id, $_SESSION[$session_prefix . 'front_user_iUserId']);
//pr($edata2);exit;
if(count($edata2) > 0)
{
    $EXIST  =   "yes";
}
else{
    $EXIST  =   "no";
}
//echo $EXIST;

// IF PREV USER LIKE(http://192.168.1.196/uvite/uvite-detail?2Z2At142917923794) THAN BACK URL ALSO JUST LIKE THIS


$pre_url    =   $_SERVER['HTTP_REFERER'];
if($pre_url != "")
{
    if (strpos($pre_url, 'id') == FALSE)
    {
        $redirect_email_link    =   "yes";
    }
    else{
        $redirect_email_link    =   "no";
    }
}


?>
<link href="<?=$site_url;?>css/jquery.loader.min.css" rel="stylesheet" />
<style>
    .guest_in_bg_images img {
        width: 94px;
        height: 94px;
        border-radius: 100%;
    }
    .popup-main-mi{
        position:fixed;
        display:none;
        width:100%;
        top:0;
        z-index:10;
    }
    .popup-mi-overlay{
        background:rgba(0, 0, 0, 0.5);
        position:fixed;
        width:100%;
        height:100%;
        top:0;
    }
    .popup-mi{
        background:#fff;
        padding:15px;
        width:500px;
        margin:50px auto;
        border-radius: 10px;
        box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.4);
        margin: 70px 22px 0 180px;
        font-size: 16px;
    }

    @-webkit-keyframes zoomInDown {
        0% {
            opacity: 0;
            -webkit-transform: scale3d(.1, .1, .1) translate3d(0, -1000px, 0);
            transform: scale3d(.1, .1, .1) translate3d(0, -1000px, 0);
            -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
            animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
        }

        60% {
            opacity: 1;
            -webkit-transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
            transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
            -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
            animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
        }
    }

    @keyframes zoomInDown {
        0% {
            opacity: 0;
            -webkit-transform: scale3d(.1, .1, .1) translate3d(0, -1000px, 0);
            transform: scale3d(.1, .1, .1) translate3d(0, -1000px, 0);
            -webkit-animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
            animation-timing-function: cubic-bezier(0.550, 0.055, 0.675, 0.190);
        }

        60% {
            opacity: 1;
            -webkit-transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
            transform: scale3d(.475, .475, .475) translate3d(0, 60px, 0);
            -webkit-animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
            animation-timing-function: cubic-bezier(0.175, 0.885, 0.320, 1);
        }
    }

    .zoomInDown {
        -webkit-animation-name: zoomInDown;
        animation-name: zoomInDown;

        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    }

    .alert_btn{
        width: 100px;
        background: #3480d2;
        border-radius: 6px;
        height: 50px;
        line-height: 30px;
        font-size: 25px;
        color: #ffffff;
        text-align: center;
        border: none;
        font-family:initial;
        cursor: pointer;
    }

    .image {
        width: 70px;
        height: 70px;
    }

    .popup_display{
        width: 390px;
        font-size: 25px;
        margin-top: 280px;
        margin-left: 100px;
    }
</style>
<body>

<div class="main" id="main">
    <div class="header_bg_im">
        <?php
        if ($chatresponse != 'no') {
            if($_REQUEST['from'] != "")
            {
                $FROM   =   $_REQUEST['from'];
            }
            else{
                $FROM = 'current';
            }

        } else {
            if($_REQUEST['from'] != "")
            {
                $FROM   =   $_REQUEST['from'];
            }
            else{
                $FROM = 'past';
            }

        }
        ?>

        <div class="header_bg_im_leftarrow" style="margin-top: 20px;">
            <?php if (isset($_SESSION[$session_prefix . 'front_user_iUserId']) && $_SESSION[$session_prefix . 'front_user_iUserId'] != "" && $pre_url != "")
            {
                if($redirect_email_link == "yes")
                {
            ?>
            <a href="<?php echo $pre_url; ?>">
                <img src="<? echo $site_url; ?>images/arrow_left_header.png" alt=""/>
            </a>
            <?php
                }
                else{
                ?>
                    <a href="<?php echo $site_url; ?>uvite-detail?id=<?php echo base64_encode($event_id); ?>&from=<?php echo $FROM; ?>">
                        <img src="<? echo $site_url; ?>images/arrow_left_header.png" alt=""/>
                    </a>
                <?php
                }
            }
            else { ?>
                <a href="<?php echo $site_url; ?>uvite-detail?id=<?php echo base64_encode($event_id); ?>&uid=<?php echo base64_encode($user_id); ?>">
                    <img src="<? echo $site_url; ?>images/arrow_left_header.png" alt=""/>
                </a>
            <?php } ?>
        </div>

        <div class="header_bg">Chats</div>
    </div>
    <!--<div class="header_bg">Chats</div>-->
    <div class="popup-main-mi" style=" text-align:center;" id="popup-main-mi1">
        <div class="popup-mi-overlay"></div>
        <div class="popup-mi zoomInDown popup_display"  id="zoomInDown1">
            Please enter some text to comment
            <br><br>
            <input type="button" value="OK" id="exit" class="alert_btn" onclick="return hide_DIV();">
        </div>

    </div>
    <div id="chat_div" style="overflow-x: hidden; position: fixed; width: 640px; top: 91px; overflow-y: auto;<?php if ($chatresponse == 'no' OR $user_event_status == 'out') {?> bottom: 10px;<?php }else {?> bottom: 105px;<?php }?>">
        <?php
        if (count($chat_list) > 0) {
            foreach ($chat_list as $k => $chat_list_arr1) {

                echo '<div class="mi-chat-date"><span>' . $k . '</span></div>';


                foreach ($chat_list_arr1 as $chat_list_arr) {
                        //pr($chat_list_arr); exit;
                    if ($chat_list_arr['type'] == "checkin") {
                    //echo $chat_list_arr['added'];


                        $start_date         =   $chat_list_arr['added'];
                        $start_date_f       =   date('h:i A F d,Y',$start_date);

                        echo '<div class="mi-chat-date-name"><span>' .$start_date_f; echo " check ".$chat_list_arr['message']." ". ucfirst($chat_list_arr["username"]) ; echo'</span></div>';
                    } else {

                        #pr($chat_list_arr);exit;
                        if ($chat_list_arr['thumb_image'] == "") {
                            $user_image = $default_web_user_thumb_image;
                        } else {
                            $user_image = $chat_list_arr['thumb_image'];
                        }

                        $chat_list_arr['added'];
                        $todate_msg = strtotime(date('Y-m-d H:i:s'));
                        $sender_date_msg = $chat_list_arr['added'];
                        $remining = $todate_msg - $sender_date_msg;

                        $remaining_time_hours = $generalfuncobj->func_date_remind($remining);

                        $remaining_time_hours_arr = explode(":", $remaining_time_hours);

                        if ($remaining_time_hours_arr[0] > 0) {
                            if ($remaining_time_hours_arr[0] == 1) {
                                $display = $remaining_time_hours_arr[0] . " day ago";
                            } else {
                                $display = $remaining_time_hours_arr[0] . " days ago";
                            }
                        } else if ($remaining_time_hours_arr[1] > 0) {
                            if ($remaining_time_hours_arr[1] == 1) {
                                $display = $remaining_time_hours_arr[1] . " hour ago";
                            } else {
                                $display = $remaining_time_hours_arr[1] . " hours ago";
                            }
                        } else if ($remaining_time_hours_arr[2] > 0) {
                            if ($remaining_time_hours_arr[2] == 1) {
                                $display = $remaining_time_hours_arr[2] . " min ago";
                            } else {
                                $display = $remaining_time_hours_arr[2] . " mins ago";
                            }
                        } else if ($remaining_time_hours_arr[3] > 0) {
                            if ($remaining_time_hours_arr[3] == 1) {
                                $display = $remaining_time_hours_arr[3] . " sec ago";
                            } else {
                                $display = $remaining_time_hours_arr[3] . " sec ago";
                            }
                        } else if ($remaining_time_hours_arr[4] > 0) {
                            $display = $remaining_time_hours_arr[4] . " sec ago";
                        }

                        $html   =   "";
                        if ($chat_list_arr['user_id'] == $user_id) {

                            $html .= '<div class="mi-msg-frm-rgt">';
                            $html .= '<div class="mi-msg-main">' . $chat_list_arr['message'];
                            $html .= '</div>';
                            $html .= '<span class="mi-msg-tm">' . $display . '</span>';

                            $html .= '<div class="mi-msg-usr-dtl">';
                            $html .= '<img src="' . $user_image . '" alt="" class="image mi-msg-usr-img" /><br>';
                            $html .= '<span class="mi-msg-usrnm">' . $chat_list_arr["username"] . '</span>';
                            $html .= '</div>';
                            $html .= '</div>';
                        } else {

                            $html .= '<div class="mi-msg-frm-lft">';
                            $html .= '<div class="mi-msg-main">' . $chat_list_arr['message'];
                            $html .= '</div>';
                            $html .= '<span class="mi-msg-tm">' . $display . '</span>';

                            $html .= '<div class="mi-msg-usr-dtl">';
                            $html .= '<img src="' . $user_image . '" alt="" class="image mi-msg-usr-img" /><br>';
                            $html .= '<span class="mi-msg-usrnm">' . $chat_list_arr["username"] . '</span>';
                            $html .= '</div>';
                            $html .= '</div>';
                        }
                        echo $html;
                        ?>

                    <?php
                    }
                }
            }
            ?>

        <?php
        }
        ?>
        <div id="append"></div>
    </div>
    <?php
    if ($chatresponse == 'no' || $user_event_status == 'out' || $_SESSION[$session_prefix . 'front_user_iUserId'] == '') {
        $Text = "none";

        } else {
        $Text = "block";

        }
    if ($_SESSION[$session_prefix . 'front_user_iUserId'] == '') {
        $banner = "block";
        $banner_add_event   =   "none";
    }
    else{
        if($EXIST == "no")
        {
            $banner_add_event =   "block";
            $Text = "none";
        }
        if($EXIST == "yes")
        {
            $banner_add_event =   "none";
            if($chatresponse == 'no' || $user_event_status == 'out')
            {
                $Text = "none";
            }
            else{
                $Text = "block";
            }

        }
        $banner = "none";
    }
    ?>

    <input type="hidden" value="<?php echo $new_timestamp; ?>" name="new_timestamp" id="new_timestamp"/>

    <div class="footer_massage_box" style="display: <?php echo $Text; ?>">

        <div class="your_massage mail_form" style="margin-right: 10px;"><input type="text" id="msg" name="msg" placeholder="Your Message"></div>
        <div class="your_massage_check_box"><input class="your_massage_check_box" type="checkbox" onchange="return check_box(<?php echo $event_id;?>,<?php echo $user_id;?>,'checkin',this);" id="check" ></div>

        <button class="your_send_button" id="send" style="  padding: 12px 0px;"
                onclick="return send_msg(<?php echo $event_id; ?>,<?php echo $user_id; ?>,'message');">SEND
        </button>

    </div>

    <div class="banner_massage_box" style="display: <?php echo $banner; ?>">
        <a href="<?php echo $site_url; ?>home">Please log-in or register to send a chat message.</a>
    </div>

    <div class="banner_massage_box" style="display: <?php echo $banner_add_event; ?>">
        <a>You must add yourself to this event to send a Chat message.</a>
    </div>

</div>

<script>function minheightresize(){	h = window.innerHeight;	document.getElementById("main").style.minHeight = h + 'px';	}window.onload = minheightresize;window.onresize = minheightresize;	</script>
</body>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->

<script type="text/javascript">

    setInterval(
        function ()
        {
            var eventid =   "<?php echo $event_id;?>";
            var userid =   "<?php echo $user_id;?>";
            var no_of_rows =   "<?php echo $no_of_rows;?>";
            var last_checked_timestamp =   "<?php echo $last_checked_timestamp;?>";
            var last_loaded_timestamp =   "<?php echo $last_loaded_timestamp;?>";
            var from    =   "load";

            $.ajax({
                url: ajax_url+"ajax-chat.php",
                //dataType:'json',
                data: {eventid:eventid,userid:userid,from:from,no_of_rows:no_of_rows,last_checked_timestamp:last_checked_timestamp,last_loaded_timestamp:last_loaded_timestamp},
                success : function( result ) {
                    $('#chat_div').html(result);

                }
            });

        }, 2000);
</script>

<script>
    /*$(document).ready(function(){
        $('#popup-main-mi1').hide();
    });*/
    function check_box(eventid,userid,type,ch)
    {
       // alert('hiii');
        var check   =   ch.checked;
        var from    =   "send";
        //var msg =   "Hii";
//alert(check);
        $.ajax({
            url: ajax_url+"ajax-chat.php",
            //dataType:'json',
            data: {type:type,eventid:eventid,userid:userid,from:from,check:check},
            success : function( result ) {
                //console.log('result',result);
                //$('#radio_form').html(result);
                // window.location.href='<?php echo $site_url; ?>chat?id=<?php echo base64_encode($event_id); ?>';
            }
        });

        //$('#check').val('');
        return true;

    }
    function send_msg(eventid,userid,type)
    {
        var msg = document.getElementById('msg').value;
        var username    =   '<?php echo $user_data['vFirstName'].' '. $user_data['vLastName'];?>';
        var userimage    =   '<?php echo $image;?>';

        if(msg == "")
        {

            $("#popup-main-mi1").show();
            $("body").css('overflow','hidden');
            $("#zoomInDown1").addClass("zoomInDown");
            return false;
        }
        else{
            var append = '';
            append += '<div class="mi-msg-frm-rgt">';
            append += '<div class="mi-msg-main" id="send_new">'+msg+'</div>';
            append += '<div class="mi-msg-tm">Now</div>';
            append += '<div class="mi-msg-usr-dtl">';
            append += '<img src="' +userimage+'" alt="" class="image mi-msg-usr-img" /><br>';
            append += '<span class="mi-msg-usrnm">'+username+'</span>';
            append += '</div>';
            append += '</div>';
            append += '<div id="append"></div>';


            $('#append').html($('#append').html()+append);
            $('#msg').val('');

            $("#popup-main-mi1").hide();

            var wtf    = $('#chat_div');
            var height = wtf[0].scrollHeight;
            wtf.scrollTop(height);

            //var new_timestamp = document.getElementById('new_timestamp').value;
            var from    =   "send";
           // console.log(msg);
            $.ajax({
                url: ajax_url+"ajax-chat.php",
                //dataType:'json',
                data: {type:type,eventid:eventid,userid:userid,message:msg,from:from},
                success : function( result ) {
                    //console.log('result',result);
                    //$('#radio_form').html(result);
                    // window.location.href='<?php echo $site_url; ?>chat?id=<?php echo base64_encode($event_id); ?>';
                }
            });
            return true;
        }


    }
</script>

<script>

    $(document).bind("keydown", function(event){
        if(event.which=="13")
        {
            $("#send").click();
        }
    });


</script>

<script>
    function hide_DIV()
    {
        $('#popup-main-mi1').hide();
        $("body").css('overflow','auto');
        return false;
    }

    $(document).ready(function(){
        var wtf    = $('#chat_div');
        var height = wtf[0].scrollHeight;
        wtf.scrollTop(height);
    });

</script>
<script src="<?=$site_url;?>js/jquery.loader.min.js"></script>