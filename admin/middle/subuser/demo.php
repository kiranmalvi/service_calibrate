<style>
    html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
    }
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<script>
    function initialize() {
        var mapOptions = {
            zoom: 10,
            center: new google.maps.LatLng(-33.9, 151.2),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        setMarkers(map, beaches);
    }

    var beaches = [
        ['Bondi Beach', -33.890542, 151.274856, 4],
        ['Coogee Beach', -33.923036, 151.259052, 5],
        ['Cronulla Beach', -34.028249, 151.157507, 3],
        ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
        ['Maroubra Beach', -33.950198, 151.259302, 1]
    ];

    function setMarkers(map, locations) {
        var image = {
            url: 'green.png',
            size: new google.maps.Size(20, 32),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 32)
        };
        var image1 = {
            url: 'green.png',
            size: new google.maps.Size(32, 32),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 32)
        };
        var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
        };

        var infowindow = new google.maps.InfoWindow({
            content: ""
        });
        for (var i = 0; i < locations.length; i++) {
            var beach = locations[i];
            var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
            var newimage;
            if (i == 1) {
                newimage = image1;
            } else {
                newimage = image;
            }

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: newimage,
                shape: shape,
                title: beach[0],
                zIndex: beach[3]
            });
            bindInfoWindow(marker, map, infowindow, beach[0]);
        }
    }

    function bindInfoWindow(marker, map, infowindow, description) {
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(description);
            infowindow.open(map, marker);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="map-canvas" width="200px" height="200px"></div>