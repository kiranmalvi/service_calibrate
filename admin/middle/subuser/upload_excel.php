<?php
$utype = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'feature.class.php');
$featureObj = new feature();
include_once($inc_class_path . 'addtional_feature.class.php');
$addtional_featureObj = new addtional_feature();
include_once($inc_class_path . 'payment.class.php');
$paymentObj = new payment();

include_once($inc_class_path . 'retailer_detail.class.php');
$retailerdetailObj = new retailer_detail();

include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();
if(isset($_REQUEST['iId']) && $_REQUEST['iId'] > 0) {
    $iId = $_REQUEST['iId'];
}
else {
    $iId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
}
$USER = $userObj->select($iId);

$extrafeature = $addtional_featureObj->Get_addfeatuer_by_user($iId);

## Count Total Store
$TOTAL_STORE_SQL = "SELECT COUNT(iUserId) as tot  FROM user WHERE iParentId = " . $iId;
$TOTAL_STORE_DATA = $obj->select($TOTAL_STORE_SQL);

$type = $USER[0]['iUserTypeId'];

## Plan Data
$PLAN_SQL = "SELECT * FROM plan WHERE iPlanId = " . $USER[0]['iPlanId'];
$PLAN_DATA = $obj->select($PLAN_SQL);

$features = $featureObj->get_feature_per_plan($USER[0]['iPlanId']);

$payment = $paymentObj->select_payment_by_user($iId);


$get_point=$referralObj->get_total_point($iId);
$points=$get_point[0]['vPoint'];

if($points>0)
{
    $points=$points;
}
else
{
    $points='0';
}

if ($payment[0]['iDtAdded'] == "") {
    $lasttransction = "";
} else {
    $lasttransction = date('m-d-Y H:i:s', $payment[0]['iDtAdded']);
}
$lastlogin = $user_settingsObj->select_user_setting($iId);


if ($lastlogin[0]['iDtLogin'] == "0") {
    $date = "Not Login Until";
    $device = "none";
} else {
    $date = date('m-d-Y H:i:s', $lastlogin[0]['iDtLogin']);
    $device = $user_settingsObj->geteDeviceType();
}

$val1 = date('Y-m-d H:i:s');
$val2 = date('Y-m-d H:i:s',$USER['0']['iExpireTime']);
$datetime1 = new DateTime($val1);
$datetime2 = new DateTime($val2);
$diff=$datetime2->diff($datetime1);
$days=$diff->format("%a days");
$currentstore1=$USER['0']['iStoreCount'];
$limitstore=$USER['0']['vComment'];
$currentstore=$limitstore-$currentstore1;


if($currentstore>0)
{
	$currentstore=$currentstore;
}
else 
{
$currentstore="0";
	}
?>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">

                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>
                    <li>
                        <a href="index.php?file=su-manage_store">Manage Account</a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;"> Upload Excel</a>                    </li>
                </ul>
            </div>
        </div>

        <section class="panel">


            <header class="panel-heading red-bg">

                <h4>Upload Excel</h4>
            </header>


            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-body">
                        </div>
                        <div class="col-md-12">
                            <form class="form-horizontal" role="form" enctype="multipart/form-data" id="FORM_CSV_UPLOAD"
                                  name="FORM_CSV_UPLOAD" method="post"
                                  action="index.php?file=su-subuseradd_a&mode=upload_CSV1">
                                <input type="hidden" id="type" name="type" value="<?php echo $type; ?>">
                                <input type="hidden" id="iId" name="iId" value="<?php echo $iId; ?>">
                                <input type="hidden" id="storecount" name="storecount"
                                       value="<?php echo $USER[0]['iStoreCount']; ?>">
                                <input type="hidden" id="max" name="max"
                                       value="<?php echo $PLAN_DATA[0]['iToStore']; ?>">


                                <div class="row" style="margin-left: 15px;">

                                     <div class="alert alert-info fade in col-md-8 ">
                                        <strong>Note : </strong>
                                        <br>1) We ONLY accept the provided format.Please click on "Download Excel Format".
                                        <br>
                                       2) All account name & email address needs to be unique.
                                        <br>
                                        3) All fields on excel need to be filled.
                                        <br>
                                        4) You can only add <?php echo $currentstore;?> account currently. To add additional accounts please email <a href="mailto:sales@servicecalibrate.com">sales@servicecalibrate.com
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Upload</label>

                                    <div class="col-lg-7">
                                        <input type="file" class="" name="CSV">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">


                                        <button id="addbutton" type="submit" class="btn btn-success1">Submit</button>

                                        <a href="index.php?file=su-manage_store"
                                           class="btn btn-default1">Back</a>

                            </form>


                            <form name="sampledownload"
                                  action="index.php?file=su-download_sample_a&utype=<?= $utype; ?>"
                                  id="sampledownload" method="post" style="float:inherit">


                                <!--<button id="downloadexcelBlutton" name="downloadexcelBlutton" type="submit"
                                            class="btn btn-success">Download Excel
                                    </button>-->
                                <!--<button id="downloadexcelBlutton" name="downloadexcelBlutton" type="submit"
                                        class="btn btn-primary">Download Excel Format
                                </button> &nbsp;-->
                                <?php if ($utype == 2 || $utype == 3) { ?>
                                    <a href="Vendor.csv" onclick="return popitup('Distributor.csv')"
                                       class="btn btn-success"> Download Excel Format </a>
                                <?php } elseif ($utype == 4) { ?>
                                    <a href="retail4.csv" onclick="return popitup('retail4.csv')"
                                       class="btn btn-success"> Download Excel Format </a>
                                <?php } elseif ($utype == 5) { ?>
                                    <a href="retail5.csv" onclick="return popitup('retail5.csv')"
                                       class="btn btn-success"> Download Excel Format </a>

                                <?php } elseif ($utype == 6) { ?>
                                    <a href="retail6.csv" onclick="return popitup('retail6.csv')"
                                       class="btn btn-success"> Download Excel Format </a>
                                <? } ?>
                                &nbsp;
                            </form>

                            <!--                                    <a href="index.php?file=su-download_sample_a&utype=-->
                            <? //=$utype;?><!--"-->
                            <!--                                       class="btn btn-success">Download Excell</a>-->
                        </div>
                    </div>


                </div>

            </div>

            </div>
            </div>
            </div>
            <script> // This function will pop a window which will tell the
                // user the order of fields  and format of .csv file
                // you can create pop_up_csv.html file in same directory and
                // modify it with required format
                function popitup(url) {
                    if (url) {
                        return true;
                    }
                }
            </script>


        </section>
    </section>
</section>
<?
include_once($admin_path . 'js_form.php');
?>
