<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 24/3/15
 */

$USER_TYPE = $_REQUEST['type'];
$iId = $_REQUEST['iId'];
include_once($inc_class_path . 'user_type.class.php');
$usertypeObj = new user_type();

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'plan.class.php');
$planObj = new plan();

if (isset($_REQUEST['pid']) && $_REQUEST['pid'] > "0") {
    $pid = $_REQUEST['pid'];
    $ssql = "where 1=1 AND eStatus !='2' AND (iUserId = $pid or iParentId=$pid)";
} else {
    $ssql = "where 1=1 AND eStatus !='2' AND (iUserId = $iId or iParentId=$iId)";
}
//$ssql = "where 1=1 AND eStatus !='2' AND (iUserId = $iId or iParentId=$iId)";
$sql = "select count(*) as tot  from user " . $ssql;

$db_res = $obj->select($sql);
$num_totrec = $db_res[0]['tot'];

$sql = "select * from user " . $ssql;
$db_res = $obj->select($sql);
/*echo "<pre>";
print_r($db_res);
exit;*/

$parent = $userObj->select($iId);
$plan = $userObj->getiPlanId();

$planObj->select($plan);
$maxstore = $planObj->getiToStore();

$type = $_REQUEST['type'];


$USER_TYPE_QUERY = "SELECT vName FROM user_type WHERE iUserTypeId = $USER_TYPE ";
$GET_USER_TYPE = $obj->select($USER_TYPE_QUERY);
$USER_TYPE_DATA = $GET_USER_TYPE[0]['vName'];


$ACT_LINK = "index.php?file=su-subuseradd_a&type=$USER_TYPE&iId=$iId";
?>
<!--main content start-->
<section id="main-content">

    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">

                    <? if (isset($_SESSION['SC_LOGIN']['ADMIN'])) { ?>
                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>
                    <? } ?>
                    <li>
                        <a href="index.php?file=su-sudashboard&type=<?php echo $type; ?>&iId=<?php echo $db_res[0]['iUserId']; ?>">User
                            (<? echo $USER_TYPE_DATA; ?>)</a>
                    </li>
                    <li>
                        <a class="current" href="#"><? echo $db_res[0]['vStoreUniqueId'] ?></a>
                    </li>

                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <?php
                    $SUB_USER = "SELECT iUserTypeId,vName FROM user_type WHERE iParentId = $USER_TYPE";
                    $GET_SUB = $obj->select($SUB_USER);
                    $USER_SUB_COUNT = count($GET_SUB);
                    ?>

                    <header class="panel-heading">
                        User
                        <span class=" pull-right header-btn">

                        <? if ($parent[0]['iStoreCount'] < $maxstore) { ?>
                                <a class="btn btn-primary"
                                   href="index.php?file=su-subuseradd&mode=add&iId=<? echo $iId; ?>&type=<? echo $USER_TYPE ?>">Add
                                    New Employee</a>
                        <? } ?>

                            <button class="btn btn-danger" onclick="delete_record()">
                                DELETE
                            </button>
                            </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            <form name="user_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                <input type="hidden" name="mode" id="mode" value="">
                                <input type="hidden" name="delete_type" value="multi_delete">

                                <table class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" onclick="check_all();"></th>
                                    <th>Edit</th>
                                    <th>User Name</th>
                                    <th>Account Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Date Added</th>


                                </tr>
                                </thead>
                                <tbody>

                                <? for ($i = 0; $i < $num_totrec; $i++) {
                                    $parent = $usertypeObj->getparent($db_res[$i]['iUserTypeId']);?>

                                    <tr class="gradeX">

                                        <td>
                                            <?// Owner can not be deleted (role id=0 for owner)
                                            if ($db_res[$i]['iParentId'] != "0") {
                                                ?>

                                            <input type="checkbox" name="delete[]"
                                                   value="<?php echo $db_res[$i]['iUserId']; ?>">
                                            <? } ?>
                                        </td>

                                        <td>
                                            <a class="btn btn-success btn-xs"
                                               href="index.php?file=su-subuseradd&type=<?php echo $type; ?>&mode=update&iId=<?php echo $db_res[$i]['iUserId']; ?>">Edit</a>

                                            <?// Owner can not be deleted (role id=0 for owner)
                                            if ($db_res[$i]['iParentId'] != "0") {
                                                ?>
                                            <a class="btn btn-danger btn-xs"
                                               href="index.php?file=su-subuseradd_a&mode=delete&type=<?php echo $type; ?>&pid=<?php echo $iId; ?>&iId=<?php echo $db_res[$i]['iUserId']; ?>&delete_type=single_delete">Delete

                                            </a>
                                            <? } ?>
                                        </td>
                                        <td>

                                            <? echo $db_res[$i]['vFirstName'] . " " . $db_res[$i]['vLastName']; ?>
                                        </td>

                                        <td>

                                            <? echo $db_res[$i]['vStoreUniqueId']; ?>
                                        </td>

                                        <td><?php echo $db_res[$i]['vEmail']; ?></td>
                                        <td><?php
                                            $roleid = $db_res[$i]['iUserRoleId'];
                                            if ($db_res[$i]['iParentId'] == "0") {
                                                echo "Owner";
                                            } else {

                                                $user_role = "SELECT vName FROM user_role WHERE iUserRoleId = $roleid";
                                                $role_name = $obj->select($user_role);
                                                echo $role_name[0]['vName'];
                                            }?></td>
                                        <td><?php echo $generalfuncobj->gmt_Date_Time_FormatWithoutTime($db_res[$i]['iDtAdded']); ?></td>

                                    </tr>
                                <? } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

<!-- Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog-brodcast">
        <div class="modal-content">
            <div class="modal-header purple-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New Brodcast</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <?php
                        foreach ($GET_SUB as $SUB_DATA) {
                            ?>
                            <a href="index.php?file=u-useradd&type=<?php echo $SUB_DATA['iUserTypeId'] ?>" type="button"
                               class="btn btn-primary">Add <?php echo $SUB_DATA['vName']; ?></a>
                        <?php
                        }
                        ?>
                        <!--<a href="index.php?file=u-useradd&type=singlestore" type="button" class="btn btn-primary">Add
                            Single Store Owner</a>
                        <a href="index.php?file=u-useradd&type=chain" type="button" class="btn btn-primary">Add Multiple
                            Store Owner</a>
                        <a href="index.php?file=u-useradd&type=corporate" type="button" class="btn btn-primary">Add
                            Corporate Account </a>-->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- modal -->


<?php
include_once($admin_path . 'js_datatable.php');
?>