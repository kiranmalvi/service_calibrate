<?php
/**
 * Created by PhpStorm.
 * User: Hardik
 * Date: 25/05/2015
 * Time: 12:29 AM
 */

include_once($inc_class_path . 'payment.class.php');
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];

$paymentObj = new payment();

$payment=$paymentObj->select_payment_five_user($iUserId);
//pr($payment);exit;

//$res_db = $userObj->get_subuser($iUserId);
//pr($res_db);
$MODULE = 'Billing History';


$transactions = 'transactions_report';



$_SESSION[$transactions][0]['Transaction Id'] = "";
$_SESSION[$transactions][0]['Amount'] ="";
$_SESSION[$transactions][0]['Transaction For'] ="";
$_SESSION[$transactions][0]['Date'] ="";
//echo "<pre>"; echo print_r($res_db); exit;
?>

    <!--main content start-->
<section id="main-content">
<section class="wrapper">

    <!-- Breadcrumbs Starts -->
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumbs-alt">
                <li>
                    <a href="index.php"> Dashboard</a>
                </li>
                <!--                    --><?php
                //                    $url="index.php?file=su-subuseradd&type=2";
                //                    if($url=="index.php?file=su-subuseradd&type=2")
                //                    {
                //
                ?>
                <!--                        <li>-->
                <!--                            <a class="current" href="javascript:;">-->
                <?php //echo $MODULE; ?><!--</a>-->
                <!--                        </li>-->
                <!--                        --><?php
                //                    }
                //
                ?>
                <li>
                    <a class="current" href="javascript:;"><?php echo $MODULE; ?></a>
                </li>
            </ul>
        </div>
    </div>
</section>
<!-- Breadcrumbs Ends -->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <ul class="nav nav-tabs">

            <li class="">
                <a data-toggle="tab" onclick="location.href='index.php?file=su-manage_store'">
                    Manage Account
                </a>
            </li>


            <li class="">
                <a data-toggle="tab" onclick="location.href='index.php?file=su-change_password'">
                    Change Password
                </a>
            </li>

            <li class="">
                <a data-toggle="tab" onclick="location.href='index.php?file=su-payment_info'">
                   Billing Address
                </a>
            </li>
            <li class="active">
                <a data-toggle="tab" onclick="location.href='index.php?file=su-transaction'">
                    Billing History
                </a>
            </li>
            <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-recurring'">
                                Payment Settings
                            </a>
                        </li>
            <!-- <li class="'">
                                    <a data-toggle="tab" onclick="location.href='index.php?file=su-delete_account'">
                                        Delete Account

                                    </a>
                                </li>-->


        </ul>

    </header>

    <!-- Table Section Starts -->
    <div class="panel-body">
        <div class="tab-content">
            <div id="manage_store" class="tab-pane active">
                <div id="manage_store"
                     class="tab-pane <?php if (!isset($_REQUEST['t'])) { ?> <? echo 'active'; ?>" <?php } ?> <?php if (isset($_REQUEST['t']) && $_REQUEST['t'] == 'about-2') { ?> <? echo 'active'; ?>" <?php } ?>
                ">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">

                            </div>
                        </div>

                        <table class="display table table-bordered table-striped" id="tmrw_table">
                            <!-- Changes HERE Start -->
                            <thead>
                            <tr>
                                <td style="display:none"></td>
                                <td>
                                    Action                                </td>

                                <td>Transaction Id</td>
                                <td>Invoice#</td>
                                <td>Amount</td>
                                <td>Transaction For</td>
                                 <td>Date of Payment</td>


                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            for($i=0;$i<count($payment);$i++) {
                                if($payment[$i]['type']=="0")
                                {
                                    $type="Purchase Plan";
                                }else
                                {
                                    $type="Promotion";
                                }
                                ?>
                                <tr>


                                    <td style="display:none"><?php echo $i;?></td>
                                    <td>
                                        <?if($payment[$i]['type']=="0"){?>
                                        <a class="btn btn-success btn-xs"
                                           href="<?php echo $site_url?>userinvoice.php?tran=<?php echo $payment[$i]['iPaymentId']?>" target="_blank">
                                            <i class="fa fa-edit"></i> Print
                                        </a>
                                        <?}else{?>
                                            <a class="btn btn-success btn-xs"
                                               href="<?php echo $site_url?>promoinvoice.php?iId=<?php echo $payment[$i]['iPaymentId']?>&page=transation" target="_blank">
                                                <i class="fa fa-edit"></i> Print
                                            </a>
                                        <?}?>

                                    </td>
                                    <td><?php echo $payment[$i]['vTransactionId']; ?></td>
                                    <td><?php


                                        echo "SC".$payment[$i]['iPaymentId'];    ?></td>
                                    <td><?php echo $payment[$i]['fAmount']; ?></td>
                                    <td>
                                        <?php echo $type;?></td>

                                    <td><?php echo date('d-m-Y', ($payment[$i]['iDtAdded'])); ?></td>


<?
$_SESSION[$transactions][$i]['Transaction Id'] = $payment[$i]['vTransactionId'];
$_SESSION[$transactions][$i]['Amount'] =$payment[$i]['fAmount'];
$_SESSION[$transactions][$i]['Transaction For'] =$type;
$_SESSION[$transactions][$i]['Date'] = date('m-d-Y', $payment[$i]['iDtAdded']);
?>
                                </tr>
                            <?php } ?>
                            </tbody>
                            <!-- Changes HERE Ends -->
                        </table>

                    </div>
                    <div class="text-right">
                        <button id="downloadexcelBlutton" name="downloadexcelBlutton" type="submit"
                                class="download-excel-btn btn btn-success1"
                                onclick="location.href='index.php?file=sf-download_a&file_name=<?= $transactions; ?>'">
                            Download Excel
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div id="change_pwd" class="tab-pane">
            <?php echo 'change_pwd'; ?>
        </div>

        <div id="upload_excel" class="tab-pane">
            <?php echo 'upload_excel'; ?>
        </div>

        <div id="payment" class="tab-pane">
            <?php echo 'payment'; ?>
        </div>
        <div id="delete_account" class="tab-pane">
            <?php echo 'delete_account'; ?>
        </div>

    </div>


</section>


<!-- Table Section Ends -->

<script>
    $(document).ready(function () {
        $('#today_table').dataTable({
            /*"aoColumnDefs": [
             {'bSortable': false, 'aTargets': [0]}
             ]*/
        });
    });
    $(document).ready(function () {
        $('#tmrw_table').dataTable({
            /*"aoColumnDefs": [
             {'bSortable': false, 'aTargets': [0]}
             ]*/
        });
    });
    $(document).ready(function () {
        $('#seven_table').dataTable({
            /*"aoColumnDefs": [
             {'bSortable': false, 'aTargets': [0]}
             ]*/
        });
    });
    $(document).ready(function () {
        $('#mnth_table').dataTable({
            /*"aoColumnDefs": [
             {'bSortable': false, 'aTargets': [0]}
             ]*/
        });
    });
</script>

<?php
include_once($admin_path . 'js_datatable.php');
?>