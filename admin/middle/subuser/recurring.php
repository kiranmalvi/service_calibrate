<?php
$mode='chk_pass';
include_once($inc_class_path . 'user.class.php');
$userObj = new user();
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];

$db_res=$userObj->select($iUserId);
//pr($db_res);
?>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">

                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>

                    <li>
                        <a class="current" href="javascript:;">Payment Settings</a>                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="panel">

        <header class="panel-heading tab-bg-dark-navy-blue">
            <div class="manu-main-scroll">
                <div class="manu-main-scroll-next">
                    <ul class="nav nav-tabs">
                        <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-manage_store'">
                                Manage Account
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-change_password'">
                                Change Password
                            </a>
                        </li>

                        <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-payment_info'">
                                Billing Address
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-transaction'">
                                Billing History
                            </a>
                        </li>
                        <li class="active">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-recurring'">
                                Payment Settings
                            </a>
                        </li>
                        <!-- <li class="'">
                                    <a data-toggle="tab" onclick="location.href='index.php?file=su-delete_account'">
                                        Delete Account

                                    </a>
                                </li>-->
                    </ul>
                </div>
            </div>
        </header>

        <div class="row">
            <div class="col-sm-12">

                <div class="panel">
                    <div class="panel-body">



                        <form method="post" class="form-horizontal bucket-form" action="index.php?file=su-subuseradd_a&iId=<?php echo $iUserId ?>" name="chnagepwd" id="chnagepwd">
                            <input type="hidden" name="mode" id="mode" value="recurring">
                            <input type="hidden" name="recurring" id="recurring" value="<?php echo $db_res[0]['ePlanType'];?>">
                            <input type="hidden" name="subscribe" id="subscribe" value="<?php echo $db_res[0]['eStatus'];?>">


                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Recurring</label>


                                <div class="col-md-8" >


                                    <?

                                    if ($db_res[0]['ePlanType'] == "1") {
                                        //$check=data-on-label="<i class='fa fa-check'></i>";
                                        $check = 'checked' . ' ' . 'data-on-label="<i class=\'fa fa-check\'></i>"';
                                        $start="stop";
                                    }
                                    else
                                    {
                                        $start="start";
                                    }
                                    ?>

                                    <input type="checkbox" name="recur" id="recur" class="form-control"  <?php echo $check; ?> onchange="check(this)"
                                           value="true">
                                    <div class ="col-md-10" class="recurring">   (NOTE:Turn off to <?php echo $start;?> recurring payment. Please contact billing@servicecalibrate.com for additional information.)</div>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Subscribe :</label>


                                <div class="col-md-8">


                                    <?if ($db_res[0]['eStatus'] == "1") {

                                        $check1 = 'checked' . ' ' . 'data-on-label="<i class=\'fa fa-check\'></i>"';
                                        $check1 = 'checked' . ' ' . 'data-on-label="<i class=\'fa fa-check\'></i>"';

                                    }
                                    ?>

                                    <input type="checkbox" name="sub"  id="sub" class="form-control"  <?php echo $check1; ?> onchange="checksub(this)" >

                                    <div class ="col-md-10" class="recurring">   (NOTE:This will inactivate your account & all the data will be erased.Please contact sales@servicecalibrate.com)</div>

                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">

                                    <button id="addbutton" type="submit" class="btn btn-success1">Submit</button>
                                    <a href="index.php?file=su-manage_store"
                                       class="btn btn-default1">Back</a>
                                </div>
                            </div>
                    </div>
                    </form>
                </div>

            </div>
        </div>

    </section>
</section>

<? include_once($admin_path . 'js_form.php'); ?>
<!--<script src="<?php /*echo $assets_url */ ?>js/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php /*echo $assets_url */ ?>bs3/js/bootstrap.min.js"></script>-->
<script src="<?php echo $assets_url ?>js/jquery.blockUI.js"></script>
<script src="<?php echo $assets_url ?>js/iCheck/jquery.icheck.min.js"></script>

<script src="<?php echo $assets_url ?>js/less-1.5.0.min.js"></script>

<!-- end: MAIN JAVASCRIPTS -->
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#chnagepwd');


            //var errorHandler1 = $('.errorHandler', form1);
            //var successHandler1 = $('.successHandler', form1);
            $('#chnagepwd').validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    oldpass: {
                        required: true,
                        minlength: "6",
                        maxlength: "12"
                    },
                    nwpass: {
                        required: true,
                        minlength: "6",
                        maxlength: "12"
                    },
                    cpass: {
                        required: true,
                        minlength: "6",
                        maxlength: "12",
                        equalTo:"#nwpass"

                    }

                },
                messages: {
                    nwpass: {
                        required:"Please Enter New Password",
                        minlength: "Please Enter Minimum 6 characters",
                        maxlength: "Please Enter only 12 characters"
                    },

                    oldpass: {
                        required:"Please Enter old Password",
                        minlength: "Please Enter 6 characters",
                        maxlength: "Please Enter only 12 characters"
                    },
                    cpass: {
                        required: "Please Enter Confirm Password",
                        equalTo:"please Enter Same Password as above "

                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();


</script>

<script>

    $(document).ready(function () {

        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#chnagepwd').submit();
        });
    });
</script>
</section>
<script>

    function check(th) {
        val1 = $(th).prop('checked');


        if (val1 == true) {

            document.getElementById('recurring').value = 1;
        }
        else {
            document.getElementById('recurring').value = 0;
        }

    }
    function checksub(th) {
        val1 = $(th).prop('checked');


        if (val1 == true) {

            document.getElementById('subscribe').value = 1;
        }
        else {
            document.getElementById('subscribe').value = 0;
        }

    }

</script>
<!--common script init for all pages-->
<script src="<?php echo $admin_url; ?>assets/js/scripts.js"></script>
<!--icheck init -->
<!-- End Check box-->


<!-- multiple -->
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/jquery-multi-select/js/jquery.quicksearch.js"></script>

<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/jquery-tags-input/jquery.tagsinput.js"></script>



