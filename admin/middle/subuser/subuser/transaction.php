<?php
/**
 * Created by PhpStorm.
 * User: Hardik
 * Date: 25/05/2015
 * Time: 12:29 AM
 */

include_once($inc_class_path . 'user.class.php');
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];

$userObj = new user();

$res_db = $userObj->get_subuser($iUserId);
//pr($res_db);
$MODULE = 'Manage Store';
$ADD_LINK = 'index.php?file=su-subuseradd';
$ACT_LINK = 'index.php?file=su-subuseradd_a';

//echo "<pre>"; echo print_r($res_db); exit;
?>

    <!--main content start-->
<section id="main-content">
<section class="wrapper">

    <!-- Breadcrumbs Starts -->
    <div class="row">
        <div class="col-md-12">
            <ul class="breadcrumbs-alt">
                <li>
                    <a href="index.php"> Dashboard</a>
                </li>
                <!--                    --><?php
                //                    $url="index.php?file=su-subuseradd&type=2";
                //                    if($url=="index.php?file=su-subuseradd&type=2")
                //                    {
                //
                ?>
                <!--                        <li>-->
                <!--                            <a class="current" href="javascript:;">-->
                <?php //echo $MODULE; ?><!--</a>-->
                <!--                        </li>-->
                <!--                        --><?php
                //                    }
                //
                ?>
                <li>
                    <a class="current" href="javascript:;"><?php echo $MODULE; ?></a>
                </li>
            </ul>
        </div>
    </div>
</section>
<!-- Breadcrumbs Ends -->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <ul class="nav nav-tabs">

            <li class="">
                <a data-toggle="tab" onclick="location.href='index.php?file=su-manage_store'">
                    Manage Store
                </a>
            </li>


            <li class="">
                <a data-toggle="tab" onclick="location.href='index.php?file=su-change_password'">
                    Change Password
                </a>
            </li>

            <li class="">
                <a data-toggle="tab" onclick="location.href='index.php?file=su-payment_info'">
                    Payment
                </a>
            </li>
            <li class="">
                <a data-toggle="tab" onclick="location.href='index.php?file=su-delete_account'">
                    Delete Account

                </a>
            </li>
            <li class="active">
                <a data-toggle="tab" onclick="location.href='index.php?file=su-transaction'">
                   Transactions

                </a>
            </li>

        </ul>

    </header>

    <!-- Table Section Starts -->
    <div class="panel-body">
        <div class="tab-content">
            <div id="manage_store" class="tab-pane active">
                <div id="manage_store"
                     class="tab-pane <?php if (!isset($_REQUEST['t'])) { ?> <? echo 'active'; ?>" <?php } ?> <?php if (isset($_REQUEST['t']) && $_REQUEST['t'] == 'about-2') { ?> <? echo 'active'; ?>" <?php } ?>
                ">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">

                            </div>
                        </div>

                        <table class="display table table-bordered table-striped" id="tmrw_table">
                            <!-- Changes HERE Start -->
                            <thead>
                            <tr>
                                <td><input type="checkbox" onclick="check_all();"></td>
                                <td>Action</td>
                                <td>User Name</td>
                                <td>Email</td>
                                <td>Account</td>
                                <td>Contact Number</td>
                                <td>Date Added</td>


                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($res_db as $manage_store) {
                                ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="delete[]"
                                               value="<?php echo $manage_store['iUserId']; ?>">
                                    </td>
                                    <td>
                                        <a class="btn btn-success btn-xs"
                                           href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $manage_store['iUserId']; ?>">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>

                                        <a class="btn btn-danger btn-xs"
                                           href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $manage_store['iUserId']; ?>&pid=<?php if ($manage_store['iParentId'] == 0) {
                                               echo $manage_store['iUserId'];
                                           } else {
                                               echo $manage_store['iParentId'];
                                           } ?>&delete_type=single_delete">
                                            <i class="fa fa-edit"></i> Delete
                                        </a>
                                    </td>
                                    </td>
                                    <td><?php echo $manage_store['vFirstName'] . ' ' . $manage_store['vLastName']; ?></td>
                                    <td><?php echo $manage_store['vEmail']; ?></td>
                                    <td><?php echo $manage_store['vStoreUniqueId']; ?></td>
                                    <td><?php echo $manage_store['vContact']; ?></td>
                                    <td><?php echo date('d-m-Y', ($manage_store['iDtAdded'])); ?></td>


                                    <!-- <td><?php /*echo $ordr['eFlag'];*/ ?></td>-->


                                </tr>
                            <?php } ?>
                            </tbody>
                            <!-- Changes HERE Ends -->
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div id="change_pwd" class="tab-pane">
            <?php echo 'change_pwd'; ?>
        </div>

        <div id="upload_excel" class="tab-pane">
            <?php echo 'upload_excel'; ?>
        </div>

        <div id="payment" class="tab-pane">
            <?php echo 'payment'; ?>
        </div>
        <div id="delete_account" class="tab-pane">
            <?php echo 'delete_account'; ?>
        </div>

    </div>


</section>


<!-- Table Section Ends -->

<script>
    $(document).ready(function () {
        $('#today_table').dataTable({
            /*"aoColumnDefs": [
             {'bSortable': false, 'aTargets': [0]}
             ]*/
        });
    });
    $(document).ready(function () {
        $('#tmrw_table').dataTable({
            /*"aoColumnDefs": [
             {'bSortable': false, 'aTargets': [0]}
             ]*/
        });
    });
    $(document).ready(function () {
        $('#seven_table').dataTable({
            /*"aoColumnDefs": [
             {'bSortable': false, 'aTargets': [0]}
             ]*/
        });
    });
    $(document).ready(function () {
        $('#mnth_table').dataTable({
            /*"aoColumnDefs": [
             {'bSortable': false, 'aTargets': [0]}
             ]*/
        });
    });
</script>

<?php
include_once($admin_path . 'js_datatable.php');
?>