<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 23/3/15
 * Time: 7:21 PM
 */



include_once($inc_class_path . 'category.class.php');
$categoryObj = new category();

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'user_type_store.class.php');
$usertoreObj = new user_type_store();

include_once($inc_class_path . 'industries.class.php');
$industryObj = new industries();

include_once($inc_class_path . 'user_role.class.php');
$userroleObj = new user_role();

include_once($inc_class_path . 'how_did_hear.class.php');
$referObj = new how_did_hear();


include_once($inc_class_path . 'state.class.php');
$stateObj = new state();

include_once($inc_class_path . 'city.class.php');
$cityObj = new city();

include_once($inc_class_path . 'country.class.php');
$countryObj = new country();


include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();

//$referralObj->update_points(8,100);


$mode = $_REQUEST['mode'];
$type = $_REQUEST['type'];
$usertype = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
//echo $usertype;
$iId = $_REQUEST['iId'];


if ($mode == "add") {
    $header = "Add New";
    $pid = $iId;

} else {
    $header = "Update";
    $db_res = $userObj->select($iId);
    $pid = $db_res[0]['iParentId'];
    if ($pid == 0) {
        $pid = $iId;
    }
}

$USER_TYPE_QUERY = "SELECT vName FROM user_type WHERE iUserTypeId = '$type' ";
$GET_USER_TYPE = $obj->select($USER_TYPE_QUERY);
$user_type = $GET_USER_TYPE[0]['vName'];

?>


<!--main content start-->
<section id="main-content">
<section class="wrapper">
<!-- page start-->


<div class="row">
    <div class="col-md-12">
        <ul class="breadcrumbs-alt">

            <li>
                <a href="index.php">Dashboard</a>
            </li>
            <?php
            if ($_REQUEST['type'] == '') {
                ?>
                <li>
                    <a href="index.php?file=su-manage_store">Manage Account</a>
                </li>
            <?php } ?>
            <li>
                <a class="current" href="#"> <? if ($mode == "add") { ?>Add User<? } else { ?>Update User<? } ?></a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
<div class="col-sm-12">
<section class="panel">

<header class="panel-heading red-bg">

    <? echo $header . " " . "User"; ?>

    <!-- <span class="pull-right header-btn">-->
    <!--                            <a class="btn btn-primary1" data-toggle="modal"-->
    <!--                               href="index.php?file=su-change_password&iId=--><? // echo $iId ?><!--">-->
    <!--                                Change Password-->
    <!--                            </a>-->
    <!--     </span>-->

</header>

<div class="panel-body">


<form class="form-horizontal bucket-form" method="post" name="frmadd" id="frmadd"
      action="index.php?file=su-subuseradd_a&iId=<? echo $iId ?>"
      enctype="multipart/form-data">
<input type="hidden" name="type" id="type" value="<?php echo $type; ?>">
<input type="hidden" name="pId" id="pId" value="<?php echo $db_res[0]['iParentId']; ?>">
<input type="hidden" name="mode" id="mode" value="<?php echo $mode; ?>">
<!--<input type="hidden" name="mail" id="mail" value="--><?php //echo $mode; ?><!--">-->
<input type="hidden" name="pswd" id="pswd" value="<?php echo $db_res[0]['vPassword']; ?>">
<input type="hidden" name="nightdelivery" id="nightdelivery" value="1">
<input type="hidden" name="image_hid" id="image_hid" value="<?php echo $db_res[0]['vImage']; ?>">


<?
//print_r($db_res);
?>
<div class="form-group">
<label class="control-label col-md-3">Image Upload</label>

<div class="col-md-9">
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">

            <? if ($mode == "add") { ?>
                <img src="<? echo $dafault_image_camera; ?>"
                     style="width: 150px; height: 150px; alt=""><?} else if ($db_res[0]['vImage'] == '' || (!is_file($user_image_path . $db_res[0]['vImage']))) {
                ?>
                <img src="<? echo $dafault_image_logo; ?>"
                     style="width: 150px; height: 150px; alt=""><?} else {
                ?>
                <img src="<? echo $user_image_url . $db_res[0]['vImage']; ?>"
                     style="width: 2010px; height: 150px; alt="">

                <?} ?>
        </div>
        <div class="fileupload-preview fileupload-exists thumbnail"
             style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
        <div>
                                                   <span class="btn btn-white btn-file">
                                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                   <span class="fileupload-exists"><i
                                                           class="fa fa-undo"></i> Change</span>
                                                   <input type="file" class="default" id="vImage" name="vImage"
                                                          onchange="Checkfiles(this.value);">
                                                   </span>
            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i
                    class="fa fa-trash"></i> Remove</a>
        </div>
        <span style="font-size: 15px;font-style: normal" id="error_msg">upload pictures only in png,jpg,jpeg,gif or bmp format</span>

    </div>

</div>


<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Account Name
        :</label>

    <div class="col-md-8">
        <input type="text" name="uniqid" id="uniqid" class="form-control"
               value="<?php echo $db_res[0]['vStoreUniqueId']; ?>" onkeyup="checkname(this.value)">
               
                <span id="accountspan" style="color: #a94442"></span>
    </div>
</div>

<?if ($usertype == "4" || $usertype == "5" || $usertype == "6") {
    if ($usertype == "4" || $usertype == "5" || $usertype == "6") {
        ?>


        <div class="form-group">
            <label class="col-md-2 control-label"><span class="red"> *</span> Account #
                :</label>

            <div class="col-md-8">
                <input type="text" name="store" id="store" class="form-control"
                       value="<?php echo $db_res[0]['vStoreName'] ?>">
            </div>
        </div>
    <?}
    if ($type == "5") {
        ?>
        <div class="form-group">
            <label class="col-md-2 control-label"><span class="red"> *</span> Number Of Account
                :</label>

            <div class="col-md-8">
                <input type="text" name="storenum" id="storenum" class="form-control"
                       value="<? echo $db_res[0]['NumberOfStore']; ?>">
            </div>
        </div>
    <?
    }

    

}
?>

<div class=" form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> First Name
        :</label>

    <div class="col-md-8">
        <input type="text" name="firstname" id="firstname" class="form-control"
               value="<? echo $db_res[0]['vFirstName']; ?>">
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Last Name
        :</label>

    <div class="col-md-8">
        <input type="text" name="lastname" id="lastname" class="form-control"
               value="<? echo $db_res[0]['vLastName']; ?>">
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Contact Number
        :</label>

    <div class="col-md-8">
        <input type="text" name="contact" id="contact" class="form-control"
               value="<? echo $db_res[0]['vContact']; ?>">
    </div>
</div>

<? if ($usertype == "3" || $usertype == "2") { ?>



    <div class="form-group">
        <label class="col-md-2 control-label"><span class="red"> *</span> Category
            :</label>

        <div class="col-md-8">
            <?
            $catlist = $categoryObj->selectall();
            ?>
            <select class="form-control m-bot15" name="category" id="category">


                <option value="">Select Category</option>

                <?for ($i = 0; $i < count($catlist); $i++) {
                    $selected = "";
                    if ($db_res[0]['iCategoryId'] == $catlist[$i]['iCategoryId']) {
                        $selected = "selected";
                    }
                    ?>
                    <option
                        value="<?php echo $catlist[$i]['iCategoryId'] ?>" <? echo $selected; ?>><?php echo $catlist[$i]['vCategoryName'] ?></option>
                <? } ?>
            </select>

        </div>
    </div>



<? } ?>

<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Indusrty
        :</label>

    <div class="col-md-8">
        <?

        $industrylist = $industryObj->selectall();
        ?>
        <select class="form-control m-bot15" name="industry" id="industry">
            <option value="">Select Industry</option>

            <?for ($i = 0; $i < count($industrylist); $i++) {
                $selected = "";
                if ($db_res[0]['iIndustriesId'] == $industrylist[$i]['iIndustriesId']) {
                    $selected = "selected";
                }
                ?>

                <option
                    value="<?php echo $industrylist[$i]['iIndustriesId'] ?>"
                    echo <? echo $selected ?>><?php echo $industrylist[$i]['vName'] ?></option>
            <? } ?>
        </select>
    </div>
</div>


<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Email
        :</label>

    <div class="col-md-8">
        <input type="text" name="email" id="email" class="form-control" value="<? echo $db_res[0]['vEmail']; ?>" onkeyup="checkmail(this.value)">
<span id="mailspan" style="color: #a94442"></span>    
    
    </div>
</div>

<!--<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Password
        :</label>

    <div class="col-md-8">
        <input type="password" name="pwd" id="pwd" class="form-control" onclick="cnfpswd()">
    </div>
</div>

            <div class="form-group" style="display: none" id="cnf">
    <label class="col-md-2 control-label"><span class="red"> *</span> Confirm Password
        :</label>

    <div class="col-md-8">
        <input type="password" name="cnfpwd" id="cnfpwd" class="form-control">
    </div>
</div>
-->

<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Address
        :</label>

    <div class="col-md-8">
        <textarea name="adrs" class="form-control" onkeypress="enter(event)"
                  id="adrs" value="<? echo $db_res[0]['vAddress']; ?>"><? echo $db_res[0]['vAddress']; ?></textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Zip :</label>

    <div class="col-md-8">
        <input type="text" name="zip" id="zip" class="form-control" value="<? echo $db_res[0]['vZip']; ?>"
               onblur="get_loc(this.value);">
    </div>
</div>


<div class="form-group">

    <?
    /* $zip="380027";
     $location=file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=380027&sensor=true");
     $adrs = json_decode($location);
     $city=$adrs->results[0]->address_components[1]->long_name;
     $state=$adrs->results[0]->address_components[3]->long_name;
     $country=$adrs->results[0]->address_components[4]->long_name;

     if($mode=="add") {
         $city = $adrs->results[0]->address_components[1]->long_name;
         $state = $adrs->results[0]->address_components[3]->long_name;
         $country = $adrs->results[0]->address_components[4]->long_name;
     }
     else
     {
         $city=$db_res[0]['vCity'];
         $state=$db_res[0]['vState'];
         $country=$db_res[0]['iCountryId'];
     }*/
    ?>

    <label class="col-md-2 control-label"><span class="red"> *</span> City :</label>

    <div class="col-md-8">

        <!--   <?/*
        $citylist = $cityObj->select("1");
        */
        ?>
        <select class="form-control m-bot15" name="iCityId" id="iCityId">
            <option selected disabled>- Select City -</option>
        </select>-->

        <input type="text" name="iCityId" id="iCityId" class="form-control" value="<?php echo $db_res[0]['vCity'] ?>">

    </div>
</div>


<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> State :</label>

    <div class="col-md-8">

        <!--<select class="form-control" name="iStateId" id="iStateId" onchange="get_city(this.value)">
            <option selected disabled>- Select State -</option>
        </select>-->
        <input type="text" name="iStateId" id="iStateId" class="form-control"
               value="<?php echo $db_res[0]['vState'] ?>">

    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label"><span class="red"> *</span> Country :</label>

    <div class="col-md-8">
        <!--  <?/*
        $countrylist = $countryObj->select();
        */
        ?>
        <select class="form-control m-bot15" name="iCountryId" id="iCountryId" onchange="get_state(this.value)">
            <option value="">Select Country</option>

            <?/*for ($i = 0; $i < count($countrylist); $i++) {
                $selected = "";
                if ($db_res[0]['iCountryId'] == $countrylist[$i]['iCountryId']) {
                    $selected = "selected";
                }
                */
        ?>

                <option
                    value="<?php /*echo $countrylist[$i]['iCountryId'] */ ?>"
                    echo <? /* echo $selected */ ?>><?php /*echo $countrylist[$i]['vName'] */ ?></option>
            <? /* } */ ?>
        </select>-->


        <?if ($mode == "add" || $db_res[0]['iCountryId'] == "") {
            $country = "USA";
        } else {
            $country = $db_res[0]['iCountryId'];
        }?>
        <input type="text" name="iCountryId" id="iCountryId" class="form-control"
               value="<?php echo $country ?>">


    </div>
</div>


<? if ($usertype == "4" || $usertype == "5" || $usertype == "6") { ?>
    <div class="form-group">
        <label class="col-md-2 control-label"><span class="red"> *</span> Type Of Account:</label>

        <div class="col-md-8">
            <? $storetype = $usertoreObj->selectall();


            ?>
            <select class="form-control m-bot15" name="storetype" id="storetype">
                <option value="">Select Type of Account</option>

                <?for ($i = 0; $i < count($storetype); $i++) {
                    $selected = "";
                    if ($db_res[0]['iTypeOfStoreId'] == $storetype[$i]['iUserTypeStoreId']) {
                        $selected = "selected";
                    }
                    ?>
                    ?>
                    <option
                        value="<?php echo $storetype[$i]['iUserTypeStoreId'] ?>"<?php echo $selected; ?>><?php echo $storetype[$i]['vName'] ?></option>
                <? } ?>
            </select>
        </div>
    </div>
    
    
    <div class="form-group">
        <label class="col-md-2 control-label"><span class="red"> *</span> Hours Of Operation From :</label>

        <div class="col-md-8 bootstrap-timepicker">

            <input type="text" class="form-control timepicker-default endtime col-md-8" name="hoursfrom"
                   id="hoursfrom" value="<?php echo($db_res[0]['iDeliveryHoursFrom'] == '' ? '10:00 AM' : $db_res[0]['iDeliveryHoursFrom']); ?>">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label"><span class="red"> *</span> Hours Of Operation To:</label>

        <div class="col-md-8 bootstrap-timepicker">
            <input type="text" class="form-control timepicker-default endtime col-md-8" name="hoursto" id="hoursto"
                    value="<?php echo($db_res[0]['iDeliveryHoursTo'] == '' ? '07:00 PM' : $db_res[0]['iDeliveryHoursTo']); ?>">


        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label"><span class="red"> *</span> Prefer Delivery Time From :</label>

        <div class="col-md-8 bootstrap-timepicker">
            <input type="text" class="form-control timepicker-default endtime1 col-md-8" name="deliverytimefrom"
                   id="deliverytimefrom"
                   value="<?php echo($db_res[0]['iPreferDeliveryTimeFrom'] == '' ? '10:00 AM' : $db_res[0]['iPreferDeliveryTimeFrom']); ?>">
        </div>
    </div>

    <div class="form-group">

        <label class="col-md-2 control-label"><span class="red"> *</span> Prefer Delivery Time To:</label>

        <div class="col-md-8 bootstrap-timepicker">
            <input type="text" class="form-control timepicker-default endtime col-md-8" name="deliverytimeto"
 id="deliverytimeto" value="<?php echo($db_res[0]['iPreferDeliveryTimeTo'] == '' ? '07:00 PM' : $db_res[0]['iPreferDeliveryTimeTo']); ?>">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label"><span class="red"> *</span> Night Delivery :</label>


        <div class="col-md-8">


            <?if ($db_res[0]['eNightDelivery'] == "1") {
                //$check=data-on-label="<i class='fa fa-check'></i>";
                $check = 'checked' . ' ' . 'data-on-label="<i class=\'fa fa-check\'></i>"';
            } else if ($db_res[0]['eNightDelivery'] == "0") {
                //$check=data-off-label="<i class='fa fa-times'></i>";
                $check = 'data-off-label="<i class=\'fa fa-times\'></i>"';
            } else {
                //$check=data-on-label="<i class='fa fa-check'></i>";
                $check = 'checked' . ' ' . 'data-on-label="<i class=\'fa fa-check\'></i>"';
            }
            ?>

            <input type="checkbox" class="form-control"  <?php echo $check; ?> onchange="check(this)"
                   value="true">

        </div>
    </div>

<? } ?>

<?if ($db_res[0]['iUserRoleId'] != "0") {

    if ($usertype == '2' || $usertype == '3') {


        ?>

        <div class="form-group">
            <label class="col-md-2 control-label"><span class="red"> *</span> User Role:</label>

            <div class="col-md-8">
                <? $userrole = $userroleObj->selectall($usertype);
                // pr($userrole);

                ?>
                <select class="form-control m-bot15" name="userrole" id="userrole">
                    <option value="">Select Role of Employee</option>

                    <?for ($i = 0; $i < count($userrole); $i++) {
                        $selected = "";
                        if ($db_res[0]['iUserRoleId'] == $userrole[$i]['iUserRoleId']) {
                            $selected = "selected";
                        }
                        ?>
                        ?>
                        <option
                            value="<?php echo $userrole[$i]['iUserRoleId'] ?>"<?php echo $selected; ?>><?php echo $userrole[$i]['vName'] ?></option>
                    <? } ?>
                </select>
            </div>
        </div>
    <?
    }
}?>
<? if ($mode == "add") { ?>



    <!-- <div class="form-group">
         <label class="col-md-2 control-label"><span class="red"> *</span> Security Quetion :</label>

         <div class="col-md-8">
             <input type="text" name="sque" id="sque" class="form-control">
         </div>
     </div>

     <div class="form-group">
         <label class="col-md-2 control-label"><span class="red"> *</span> Answer :</label>

         <div class="col-md-8">
             <input type="text" name="ans" id="ans" class="form-control">
         </div>
     </div>-->
    <div class="form-group">
        <label class="col-md-2 control-label"><span class="red"> *</span> How did You Hear About Us :</label>

        <div class="col-md-8">
            <select class="form-control m-bot15" name="aboutus" id="aboutus" onchange="getreffrence(this.value)">
                <option value="">Select Reference</option>
                <?$ref = $referObj->select();
                for ($i = 0; $i < count($ref); $i++) {
                    ?>
                    <option value="<?php echo $ref[$i]['iHDHId']; ?>"> <?php echo $ref[$i]['vName']; ?></option>
                <? } ?>

            </select>


        </div>
    </div>


    <div class="form-group" id="refemail" style="display: none">
        <label class="col-md-2 control-label"><span class="red"> </span> </label>

        <div class="col-md-8">
            <input type="text" name="refmail" id="refmail" class="form-control"
                   placeholder="Enter Email of Refferal Person">
        </div>
    </div>

<? } ?>



<div class="form-group">
    <div class="col-lg-offset-2 col-lg-10">

        <? if ($mode == "add") { ?>
            <button id="addbutton" type="submit" class="btn btn-success1">Add</button>
            <a href="index.php?file=su-manage_store"
               class="btn btn-default1">Back</a>
        <?
        } else
            if ($_REQUEST['type'] == '') {
                ?>
                <button id="addbutton" type="submit" class="btn btn-success1">Update</button>
                <a href="index.php?file=su-manage_store"
                   class="btn btn-default1">Back</a>
            <? } else { ?>
                <button id="addbutton" type="submit" class="btn btn-success1">Update</button>
                <a href="index.php?file=su-sudashboard"
                   class="btn btn-default1">Back</a><?php } ?>

    </div>
</div>

</form>
</div>


</section>
</div>
</div>

<!-- page end-->
</section>
</section>
<script>
    function get_loc(loc) {
        $.ajax({
            url: 'index.php?file=su-subuseradd_a',
            type: 'POST',
            data: {"mode": 'get_zipcode', "zipcode": loc},
            success: function (result) {
                var data = JSON.parse(result);

                var location = data['results'][0]['formatted_address'].split(',');
                console.log(location);

                var cnt = location.length;
                console.log(cnt);
                if (cnt == "3") {
                    $('#iCountryId').val(location[2]);
                    var statetrim = location[1].trim();
                    var state = statetrim.split(" ");
                    $('#iStateId').val(state[0]);
                    $('#iCityId').val(location[0]);
                }
                else {
                    $('#iCountryId').val("");
                    $('#iStateId').val("");
                    $('#iCityId').val("");
                }
                //$('#iCountryId').val(data['results'][0]['address_components'][4]['long_name']);

            }
        });
    }
</script>

<?

?>
<? include_once($admin_path . 'js_form.php'); ?>
<!--<script src="--><?php //echo $assets_url ?><!--plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>-->
<!--<script src="<?php /*echo $assets_url */ ?>plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="<?php /*echo $assets_url */ ?>plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!--<script src="--><?php //echo $assets_url ?><!--plugins/dist/js/pwstrength-bootstrap-1.0.0.min.js"></script>-->
<!--<script type="text/javascript" src="--><?php //echo $assets_url ?><!--js/jquery.complexify.js"></script>-->
<!--<script src="--><?php //echo $assets_url ?><!--js/file-uploader/js/main.js"></script>-->
<!--<![endif]-->
<!--<script src="<?php /*echo $assets_url */ ?>js/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php /*echo $assets_url */ ?>bs3/js/bootstrap.min.js"></script>-->
<script src="<?php echo $assets_url ?>js/jquery.blockUI.js"></script>
<script src="<?php echo $assets_url ?>js/iCheck/jquery.icheck.min.js"></script>

<script src="<?php echo $assets_url ?>js/less-1.5.0.min.js"></script>

<!-- end: MAIN JAVASCRIPTS -->
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>


<script>
var FormAdminValidator = function () {
    // function to initiate Validation Sample 1
    var temp = 0;
    var runValidator1 = function () {
        var form1 = $('#frmadd');


        //var errorHandler1 = $('.errorHandler', form1);
        //var successHandler1 = $('.successHandler', form1);
        $('#frmadd').validate({


            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                error.insertAfter(element);
                // for other inputs, just perform default behavior
                },
            ignore: "",
            rules: {
                storenum: {
                    required: "true",
                    number: "true"
                },
                firstname: {
                    required: true
                },
                lastname: {
                    required: true
                },
                zip: {
                    required: true,
                    number: true

                },
                iCityId: "required",
                iStateId: "required",
                iCountryId: "required",
                contact: {
                    required: true,
                    number: true,
                    minlength: "10",
                    maxlength: "12"
                },
                title: "required",
                category: "required",
                industry: "required",
                email: {
                    required: true,
                    email: true
                },
                pwd: {
                    minlength: 6,
                    maxlength: 10
                },

                cname: "required",
                nightdelivery: "required",
                storetype: "required",
                deliverytimeto: "required",
                deliverytimefrom: "required",


                userrole: "required",
                hoursfrom: "required",
                hoursto: "required",
                uniqid: "required",
                eStatus: "required",
                adrs: "required"

                },
            messages: {
                storenum: {
                    required: "Please Enter Number Of Account",
                    number: "Enter only digits"
                },
                firstname: "Please Enter First Name",
                lastname: "Plese Enter Last Name",
                zip: {
                    required: "Please Enter Zip Code",
                    number: "Enter Only Digits"

                },
                iCityId: "Please Enter City",
                iStateId: "Please Enter State",
                iCountryId: "Please Enter Country",
                contact: {
                    required: "Please Enter Contact",
                    number: "Enter Only digits",
                    minlength: "Enter minimum 10 digits",
                    maxlength: "Enter only 12 digits"
                },
                title: "Please Enter Title",
                category: "Please Enter Category",
                industry: "Please Enter Industry",
                email: {
                    required: "Please Enter Email",
                    email: "Please Enter valid Email"
                },
                pwd: {
                    minlength: "Enter 10 characters",
                    maxlength: "Enter only 12 characters"
                },
                cname: "Please Enter Company Name",
                nightdelivery: "Please select option for Nightdelivery",
                storetype: "Please Select Type of Account",
                deliverytimeto: "Please Select Deliver Time Ends At",
                deliverytimefrom: "Please Select Deliver Time Starts From",


                userrole: "Please Select User Role",
                hoursfrom: "Select Working Hours Starts From",
                hoursto: "Select Working Hours Ends At",
                uniqid: "Please Enter Account Name",
                eStatus: "Please Select Status",
                adrs: "Please Enter Company Address"

                },
            invalidHandler: function (event, validator) { //display error alert on form submit
                //successHandler1.hide();
                //errorHandler1.show();
                },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
                },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
                },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
            submitHandler: function (frmadd) {

                successHandler1.show();
                errorHandler1.hide();
            }

        });

    };

    return {
        //main function to initiate template pages
        init: function () {
            runValidator1();
        }
    };

    //$('#frmadd').submit();
}();

    //$('#frmadd').submit();
function get_state(country_id) {

    var selected = '<?php echo (isset($db_res[0]['vState'])) ? $db_res[0]['vState'] : ""; ?>';

    $.ajax({
        url: 'index.php?file=ma-state_a',
        type: 'POST',
        data: {"mode": 'get_state', "country_id": country_id, "selected": selected},
        success: function (result) {
            console.log('sdf : ', result);
            $('#iStateId').html(result);
        }
    });
}


function get_city(state_id) {

    var selected = '<?php echo (isset($db_res[0]['vCity'])) ? $db_res[0]['vCity'] : ""; ?>';

    $.ajax({
        url: 'index.php?file=ma-city_a',
        type: 'POST',
        data: {"mode": 'get_city', "state_id": state_id, "selected": selected},
        success: function (result) {

            $('#iCityId').html(result);
        }
    });
}
$(document).ready(function () {
    <?if($mode=="update"){?>
    get_state('<?php echo $db_res[0]['iCountryId']; ?>');
    get_city('<?php echo $db_res[0]['vState']; ?>');
    <?}?>
//        Main.init();
    FormAdminValidator.init();
    $('#addbutton').click(function () {
        $('#frmadd').submit();
    });
    });
</script>

<script>
    function checkname(name) {
        var id=<?php echo $db_res[0]['iUserId'];?>;
        $.ajax({
            url: 'index.php?file=u-useradd_a',
            type: 'POST',
            data: {"mode": 'Get_Unique', "name": name, "id": id},
            success: function (result) {

                var data=JSON.parse(result)
                console.log(data);
                $("#accountspan").text(data.MSG);

            }
        });
    }

    function checkmail(mail)
    {
        var id=<?php echo $db_res[0]['iUserId'];?>;
        $.ajax({
            url: 'index.php?file=u-useradd_a',
            type: 'POST',
            data: {"mode": 'Get_Uniquemail', "name": mail, "id": id},
            success: function (result) {

                var data=JSON.parse(result)
                console.log(data);
                $("#mailspan").text(data.MSG);

            }
        });
    }
</script>
<script>
    function getreffrence(val) {
        var ref = val;
        if (ref == 1) {
            refemail.style.display = "";
        }
        else {
            refemail.style.display = "none";
        }
    }
</script>
<script>
    function cnfpswd() {

        cnf.style.display = "";
    }
</script>

<script>

    function Checkfiles(value1) {
        // alert(value1);
        $.ajax({
            url: 'index.php?file=su-subuseradd_a',
            type: 'POST',
            data: {"id": value1, "mode": 'jpg_chk'},
            success: function (result) {
                console.log('sdf : ', result);
                $('#error_msg').html(result);
            }

        });


    }
    function check(th) {
        val1 = $(th).prop('checked');


        if (val1 == true) {
            document.getElementById('nightdelivery').value = 1;
        }
        else {
            document.getElementById('nightdelivery').value = 0;
        }

    }
</script>
<link rel="stylesheet" type="text/css" href="<?= $admin_url ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.css"/>
<script type="text/javascript" src="<?= $admin_url ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.js"></script>


<!--css & js for timepicker-->


<script>

     <?php if($mode=="add")
     {
    $timeto="07:00 PM";
    $timefrom="10:00 AM";
    $hoursto="07:00 PM";
    $hoursfrom="10:00 AM";
    }
    else{
     $timeto=$db_res[0]['iPreferDeliveryTimeTo'];
    $timefrom=$db_res[0]['iPreferDeliveryTimeFrom'];
    $hoursto=$db_res[0]['iDeliveryHoursTo'];
    $hoursfrom=$db_res[0]['iDeliveryHoursFrom'];
    }
    ?>


   
    $('#deliverytimefrom').timepicker({
        defaultTime: '<?php echo $timefrom;?>'
    });

    $('#deliverytimeto').timepicker({
        defaultTime: '<?php echo $timeto;?>'
    });

    $('#hoursto').timepicker({
        defaultTime: '<?php echo $hoursto;?>'
    });

    $('#hoursfrom').timepicker({
        defaultTime: '<?php echo $hoursfrom;?>'
    });



   
</script>
<script>
    function enter(e)
    {
        var key=e.keyCode;
        if(key=="13")
        {
            e.preventDefault();
            items.create(this.newAttributes());
            $("#adrs :input").val('');
            $("#adrs :input")[0].focus();
        }
    }
</script>

<link rel="stylesheet"
      href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/timepicker.css">
<link rel="stylesheet"
      href="<?php echo $assets_url ?>plugins/bootstrap-timepicker/css/datetimepicker.css">
<script src="<?php echo $assets_url ?>plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?php echo $assets_url ?>plugins/bootstrap-timepicker/js/jquery-ui-timepicker-addon.js"></script>