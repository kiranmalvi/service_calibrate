<?php
include_once('include.php');
include_once($inc_class_path . 'user.class.php');
$userObj = new user();


$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$exe_del = "select p.iPaymentId,pl.vPlanName,us.*, u.* from user u
                left join user_settings us on u.iUserId=.us.iUserId
                left join payment p on u.iUserId=.p.iUserId
                 left join plan pl on u.iPlanId=.pl.iPlanId
                where u.iUserId='$iUserId'";
$user=$obj->sql_query($exe_del);

$adid=$_REQUEST['adid'];
//$adid="4";
$addonamount="-";
$addonstore="-";
$prevamount=$user[0]['fAmountPaid'];
$amount=$user[0]['fAmountPaid'];
$unitprice=$amount/$user[0]['vComment'];

if($adid!="") {
    $exe_del_ad = "select * from addtional_feature where iAdditionalfeatureID='$adid'";
    $addon = $obj->sql_query($exe_del_ad);
    $addonamount = $addon[0]['fAmount'];
    $addonstore = $user[0]['vComment'];
    $famount = $amount;
    $prevamount = $famount - $addonamount;
    $unitprice=$prevamount/$user[0]['vComment'];
}

//$user=$userObj->select($iUserId);
//pr($user);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link href="<?php echo $site_url; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/custom.css" rel="stylesheet" type="text/css">
</head>

<body>

<section>
    <div id="inv-main">
        <div class="inv-main">
            <div class="row">
                <a class="navbar-brand" href="index.php">
                    <img src="<?php echo $site_url; ?>assets/images/logo.png" alt="Service Calibrate">
                </a>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="inv-address">
                        <span>[3655 Torrance Blvd Suite #300]</span>
                        <span>[Torrance, CA  90503]</span>
                        <span>Phone: [000-000-0000]</span>
                        <span>Fax: [000-000-0000]</span>
                        <span>Website: www.servicecalibrate.com </span>
                    </div>
                </div>
                <div class="col-md-4 table-pad">
                    <div class="inv-address">
                        <table>
                            <tr>
                                <td>DATE</td>
                                <td><?php echo date('m/d/Y');?></td>
                            </tr>
                            <tr>
                                <td>INVOICE #</td>
                                <td style="color: #000000">[<?php echo "SC".$user[0]['iPaymentId'];?>]</td>
                            </tr>
                            <tr>
                                <td>CUSTOMER ID</td>
                                <td>[<?php echo $user[0]['iCustomerId']?>]</td>
                            </tr>
                            <tr>
                                <td>DUE DATE</td>
                                <td><?php echo date('m/d/Y',$user[0]['iExpireTime']);?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="bill-to">
                        <p> BILL TO </p>
                        <span> [<?php echo $user[0]['vFirstName']." ".$user[0]['vLastName'];?>] </span>
                        <span> [<?php echo $user[0]['vStoreUniqueId'];?>] </span>
                        <span> [<?php echo $user[0]['vAddress'];?> </span>
                        <span> [<?php echo $user[0]['vCity'];?>, <?php echo $user[0]['vState'];?>  <?php echo " ".$user[0]['vZip'];?>] </span>
                        <span> [<?php echo $user[0]['vContact'];?>] </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="invdes-box">
                    <table class="col-md-8" border="1">
                        <tr>
                            <th> DESCRIPTION </th>
                            <th> DATE </th>
                            <th> UNIT PRICE </th>
                            <th> QTY </th>
                            <th> TAXED </th>
                            <th> AMOUNT </th>
                        </tr>
                        <tr>
                            <td> <?php echo $user[0]['vPlanName']?> </td>
                            <td> <?php echo date('m/d/Y');?></td>
                            <td>  <?php echo $unitprice;?></td>
                            <td>  <?php echo $user[0]['vComment'];?> </td>
                            <td>  </td>
                            <td> <?php echo $prevamount;?> </td>
                        </tr>
                        <tr>
                            <td> Payment Received - Thank You! </td>
                            <td> ### </td>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td> (<?php echo $prevamount;?>) </td>
                        </tr>
                        <tr>
                            <td> [Service 1] </td>
                            <td> <?php echo date('m/d/Y');?></td>
                            <td>  <?php echo $unitprice;?></td>
                            <td> <?php echo $user[0]['vComment'];?> </td>
                            <td> X </td>
                            <td> <?php echo $prevamount;?> </td>
                        </tr>

                        <tr>
                            <td> [Add-On service] </td>
                            <td> <?php echo date('m/d/Y');?></td>
                            <td> 8 </td>
                            <td> <?php echo $addonstore;?> </td>
                            <td> X </td>
                            <td> <?php echo $addonamount;?> </td>
                        </tr>
                        <tr>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td> - </td>
                        </tr>
                        <tr>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td>  </td>
                            <td> - </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 other-comment-main">
                    <div class="other-comment">
                        <p> OTHER COMMENTS </p>
                        <ul>
                            <li> <b>  1. AUTOPAY - Do not submit check or cash! </b> </li>
                            <li>  2. $399.96 will be charged to your credit card on 6/30/2015. <br>
                                To avoid a late payment charge on your total due over $20, payment must be received before Jul 17, 2015. The charge is $2.50 or 1.5% of your total due, whichever is greater.
                            </li>
                            <li> 3. To avoid a late payment charge payment must be received before 07/15/15. The charge is $X.XX or X.X% of your total due, whichever is greater. </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="other-totale">
                        <table>
                            <tr>
                                <td>Subtotal</td>
                                <td> <?php echo$amount;?> </td>
                            </tr>
                            <tr>
                                <td>Taxable</td>
                                <td> - </td>
                            </tr>
                            <tr>
                                <td>Tax rate</td>
                                <td> - </td>
                            </tr>
                            <tr>
                                <td>Tax due</td>
                                <td> - </td>
                            </tr>
                            <tr>
                                <td>Other</td>
                                <td> - </td>
                            </tr>
                            <tr>
                                <td> Total </td>
                                <td> <?php echo $amount;?> </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <div class="text-center footer-main">
                        <p> If you have any questions about this invoice, please contact </p>
                        <p> [Name, Phone #, E-mail] </p>
                        <b> Thank You For Your Business! </b>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="invoice-button col-md-8 text-center">
                    <input type="button" name="freebutton" id="freebutton"value="Go To Dashboard" onclick="window.location.href='admin/index.php?file=su-sudashboard&iId=<?php echo $iUserId?>'" >
                    <input type="button" name="freebutton" id="freebutton"value="Print" onclick="printContent('inv-main')">
                </div>
            </div>
        </div>
    </div>
</section>

</body>
</html>

<script>
    function printContent(el) {
        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById(el).innerHTML;
        document.body.innerHTML = printcontent;
        window.print();
        document.body.innerHTML = restorepage;
    }
</script>




