<?

include_once($inc_class_path . 'payment.class.php');
$paymentObj = new payment();

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'billing_info.class.php');
$BillinginfoObj = new billing_info();

$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$exist=$BillinginfoObj->check_user($iUserId);

$user=$userObj->userInfo($iUserId);

if(count($exist)>0)
{
    $address=$exist[0]['vAddress'];
    $city=$exist[0]['vCity'];
    $state=$exist[0]['VState'];
    $zip=$exist[0]['vZip'];
    $country=$exist[0]['vCountry'];
}
else
{
    $address=$user['vAddress'];
    $city=$user['vCity'];
    $state=$user['vState'];
    $zip=$user['vZip'];
    $country=$user['iCountryId'];
}


?>


<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">

                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>

                    <li>
                        <a class="current" href="javascript:;">  Billing Address</a>                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="panel">
        <header class="panel-heading tab-bg-dark-navy-blue">
            <div class="manu-main-scroll">
                <div class="manu-main-scroll-next">
                    <ul class="nav nav-tabs">
                        <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-manage_store'">
                                Manage Account
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-change_password'">
                                Change Password
                            </a>
                        </li>

                        <li class="active">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-payment_info'">
                               Billing Address
                            </a>
                        </li>
                         <li class="">
                        <a data-toggle="tab" onclick="location.href='index.php?file=su-transaction'">
                            Billing History
                        </a>
                    </li>
                    <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-recurring'">
                                Payment Settings
                            </a>
                        </li>
                        <!-- <li class="'">
                                    <a data-toggle="tab" onclick="location.href='index.php?file=su-delete_account'">
                                        Delete Account

                                    </a>
                                </li>-->
                    </ul>
                </div>
            </div>
        </header>


        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-body">

                        <form class="form-horizontal bucket-form" method="post" name="frm_payment" id="frm_payment"
                              action="index.php?file=su-paymentinfo_a"
                              enctype="multipart/form-data">

                            <input type="hidden" value="edit" name="mode">

                            <div class="row" style="margin-left: 15px; display: none;" id="smsg">
                                <div class="alert alert-success fade in col-md-4  ?>">
                                    <spna id="submsg"></spna>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Address
                                    :</label>

                                <div class="col-md-8">
                                    <input type="text" name="address" id="address" class="form-control"
                                           value="<? echo $address; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Zip
                                    :</label>

                                <div class="col-md-8">
                                    <input type="text" name="zip" id="zip" class="form-control"
                                           value="<? echo $zip; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> City
                                    :</label>

                                <div class="col-md-8">
                                    <input type="text" name="city" id="city" class="form-control"
                                           value="<? echo $city; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> State
                                    :</label>

                                <div class="col-md-8">
                                    <input type="text" name="state" id="state" class="form-control"
                                           value="<? echo $state; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Country
                                    :</label>

                                <div class="col-md-8">
                                    <input type="text" name="country" id="country" class="form-control"
                                           value="<? echo $country; ?>">
                                </div>

                            </div>
                            <?

                            $subStatus = '';
                            $subVal = '';
                            if($user['ePlanType']!=0)
                            {
                                if($user['ePlanType']==2)
                                {
                                    $subStatus = "Subscribe";
                                    $subVal = 1;
                                }
                                else
                                {
                                    $subStatus = "Unsubscribe";
                                    $subVal = 2;
                                }
                            }
                            ?>
                           
                            <!-- <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="unsubscribe" type="button" class="btn btn-primary" value="<?php /*echo $subVal;*/?>" name="unsubscribe">Unsubscribe</button>
                                </div>
                            </div>-->

                            <div class="form-group" <?php echo $stylesub?> id="unsub">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit" class="btn btn-success1">Change</button>
                                    <a href="index.php?file=su-manage_store"
                                       class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>



                    </div>

                </div>
            </div>
    </section>
</section>

<? include_once($admin_path . 'js_form.php'); ?>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/scripts.js"></script>
<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#frm_payment');


            //var errorHandler1 = $('.errorHandler', form1);
            //var successHandler1 = $('.successHandler', form1);
            $('#frm_payment').validate({


                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    address: {
                        required: "true"
                    },
                    city: {
                        required: "true"
                    },
                    state: {
                        required: "true"
                    },
                    country: {
                        required: "true"
                    },
                    zip: {
                        required: "true"
                    }
                },
                messages: {
                    address: {
                        required: "Please Enter Address"
                    },
                    city: {
                        required: "Please Enter City"
                    },
                    state: {
                        required: "Please Enter State"
                    },
                    country: {
                        required: "Please Enter Country"
                    },
                    zip: {
                        required: "Please Enter Zipcode"
                    }


                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {

                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#frm_payment').submit();
        });





    });

    function subscribe_unscribe(v,id)
    {
        $.ajax({

            url: 'index.php?file=su-paymentinfo_a',
            type: 'POST',
            data: {"value": v, "mode": id},
            success: function (result) {

                var data=JSON.parse(result);
                //console.log(data);
                $("#smsg").show();
                $("#submsg").text(data.MSG);
                $("#sub_unsub_div").html(data.OUTPUT);


            }

        });
    }
</script>