<!DOCTYPE html>
<?php
////////////////////////////////////  Dashboard Changes of Query 18_06_2015_PM //////////////////////////////////////////////////
$user_type_id = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
/*echo $user_type_id;
exit;*/


include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'feature.class.php');
$featureObj = new feature();
include_once($inc_class_path . 'addtional_feature.class.php');
$addtional_featureObj = new addtional_feature();
include_once($inc_class_path . 'payment.class.php');
$paymentObj = new payment();

include_once($inc_class_path . 'retailer_detail.class.php');
$retailerdetailObj = new retailer_detail();

include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();
if (isset($_REQUEST['iId']) && $_REQUEST['iId'] > 0) {
    $iId = $_REQUEST['iId'];
} else {
    $iId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
}
$USER = $userObj->select($iId);

$extrafeature = $addtional_featureObj->Get_addfeatuer_by_user($iId);

## Count Total Store
$TOTAL_STORE_SQL = "SELECT COUNT(iUserId) as tot  FROM user WHERE iParentId = " . $iId;
$TOTAL_STORE_DATA = $obj->select($TOTAL_STORE_SQL);

$type = $USER[0]['iUserTypeId'];

## Plan Data
$PLAN_SQL = "SELECT * FROM plan WHERE iPlanId = " . $USER[0]['iPlanId'];
$PLAN_DATA = $obj->select($PLAN_SQL);

$features = $featureObj->get_feature_per_plan($USER[0]['iPlanId']);

$payment = $paymentObj->select_payment_by_user($iId);


$get_point = $referralObj->get_total_point($iId);
$points = $get_point[0]['vPoint'];
if($user_type_id=="4"|| $user_type_id=="5"||$user_type_id=="6")
{
    $payment_url=$site_url.'payment_retailers.php?page=upgrade';
}
else if($user_type_id=="2")
{
    $payment_url=$site_url.'payment_distributor.php?page=upgrade';
}
else if($user_type_id=="3")
{
    $payment_url=$site_url.'payment_manufacturer.php?page=upgrade';
}
if ($points > 0) {
    $points = $points;
} else {
    $points = '0';
}

if ($payment[0]['iDtAdded'] == "") {
    $lasttransction = "";
} else {
    $lasttransction = date('m-d-Y H:i:s', $payment[0]['iDtAdded']);
}
$lastlogin = $user_settingsObj->select_user_setting($iId);


if ($lastlogin[0]['iDtLogin'] == "0") {
    $date = "Not Login Until";
    $device = "none";
} else {
    $date = date('m-d-Y H:i:s', $lastlogin[0]['iDtLogin']);
    $device = $user_settingsObj->geteDeviceType();
}

$val1 = date('Y-m-d H:i:s');
$val2 = date('Y-m-d H:i:s', $USER['0']['iExpireTime']);
$datetime1 = new DateTime($val1);
$datetime2 = new DateTime($val2);
$diff = $datetime2->diff($datetime1);
$days = $diff->format("%a");

$yr = date("Y");

$First = date('01-01-' . $yr);
/*echo $First;*/


$FirstDt = strtotime(date($First));
$CurrentDt = strtotime(date('d-m-Y 23:59:59'));


$ct_dt = strtotime(date('Y-m-d H:i:s'));
$s_graph = " SELECT iUserId,vStoreUniqueId,vLatitude,vLongitude,vStoreName FROM user where iUserId in (select iUserId from user where iUserId='$iId' or iParentId='$iId' AND eStatus='$iId') ORDER BY iUserId ASC";
$gph = $obj->select($s_graph);
/*echo "<pre>";
print_r($gph);
exit;*/
$yr = date("Y");

$First = date('01-01-' . $yr);
/*echo $First;*/


$FirstDt = strtotime(date($First));
$CurrentDt = strtotime(date('d-m-Y 23:59:59'));


$ct_dt = strtotime(date('Y-m-d H:i:s'));

 $s_graph = " SELECT iUserId,vStoreUniqueId,vLatitude,vLongitude,vStoreName FROM user where iUserId in (select iUserId from user where iUserId='$iId' or iParentId='$iId' AND eStatus='1') ORDER BY iUserId ASC";
$gph = $obj->select($s_graph);

/*echo "<pre>";
print_r($gph);
exit;*/

if ($user_type_id == 4 || $user_type_id == 5 || $user_type_id == 6) {


    $sql1 = "SELECT u.vFirstName,u.vLastName,u.vStoreUniqueId,sum(o.fAmount) as total FROM orders o left join user u on o.iUserId=u.iUserId where o.iRetailerId='$iId' AND o.iDtAdded between " . $FirstDt . " AND " . $CurrentDt . " AND o.fAmount > 0.00 group by o.iuserId ORDER BY o.fAmount ASC";
    $db_fet1 = $obj->select($sql1);

    $db_fet6 = array();
    $sql5check = "select monthname(date(FROM_UNIXTIME(iDtAdded))) as month,count(iUserId) as count from orders where iRetailerId='$iId' group by month ORDER BY iDeliveryDate Asc";
    $row5check = $obj->select($sql5check);

    $sql6check = "SELECT count(distinct o.iUserId) as count, monthname(date(FROM_UNIXTIME(iDtAdded))) as month,sum(o.fAmount) as total,GROUP_CONCAT(iTotalTime SEPARATOR ',') AS iTotalTime FROM orders as o where iRetailerId='$iId' group by month";
    $row6check = $obj->select($sql6check);

    for($zz=1; $zz <= 12; $zz++) {
        ///retailer  Area 1
        $sql5 = "select monthname(date(FROM_UNIXTIME(iDtAdded))) as month,count(iUserId) as count from orders where iRetailerId='$iId' and MONTH(date(FROM_UNIXTIME(iDtAdded))) = '{$zz}' group by month ORDER BY iDeliveryDate Asc";
        $row = $obj->select($sql5);
        if(count($row) == 0) {
            $row[0]['count'] = 0;
            $row[0]['month'] = date('F', mktime(0, 0, 0, $zz, 10));;
        }
        $db_fet5[] = $row[0];

        ///retailer  Line 2



        $sql6 = "SELECT count(distinct o.iUserId) as count, monthname(date(FROM_UNIXTIME(iDtAdded))) as month,sum(o.fAmount) as total,GROUP_CONCAT(iTotalTime SEPARATOR ',') AS iTotalTime FROM orders as o where iRetailerId='$iId' AND MONTH(date(FROM_UNIXTIME(iDtAdded))) = '{$zz}' group by month";
        $row = $obj->select($sql6);
        if(count($row) == 0) {
            $row[0]['count'] = 0;
            $row[0]['month'] = date('F', mktime(0, 0, 0, $zz, 10));;
            $row[0]['total'] = 0;
            $row[0]['iTotalTime'] = 0;
        }
        $db_fet6[] = $row[0];

    }

///retailer  column 3

    $sql7 = "select u.vFirstName,u.vLastName,u.vStoreUniqueId as name,count(o.iUserId) as count,sum(o.fAmount) as total,GROUP_CONCAT(o.iTotalTime SEPARATOR ',') as iTotalTime from orders o left join user u on o.iUserId=u.iUserId where o.iRetailerId='$iId' AND o.iDtAdded between '$FirstDt' AND '$CurrentDt' group by o.iUserId order by total asc limit 10";
    $db_fet7 =$obj->select($sql7);

    $sql7check = "select u.vFirstName,u.vLastName,u.vStoreUniqueId as name,count(o.iUserId) as count,sum(o.fAmount) as total,GROUP_CONCAT(o.iTotalTime SEPARATOR ',') as iTotalTime from orders o left join user u on o.iUserId=u.iUserId where o.iRetailerId='$iId' AND o.iDtAdded between '$FirstDt' AND '$CurrentDt' group by o.iUserId order by total asc limit 10";

    $db_fet7check =$obj->select($sql7check);



}
else {
    $sql1 = "SELECT u.vFirstName,u.vLastName,u.vStoreUniqueId,sum(o.fAmount) as total FROM orders o left join user u on o.iUserId=u.iUserId where o.iUserId in (select iUserId from user where iUserId='$iId' or iParentId='$iId') AND o.iDtAdded between " . $FirstDt . " AND " . $CurrentDt . " AND o.fAmount > 0.00 group by o.iUserId ORDER BY fAmount ASC";
    $db_fet1 = $obj->select($sql1);

    $sql5check = "select monthname(date(FROM_UNIXTIME(iDtAdded))) as month,count(iRetailerId) as count from orders where iUserId='$iId'  group by month ORDER BY iDeliveryDate Asc";
    $row5check = $obj->select($sql5check);

   $sql6check = "SELECT count(distinct o.iRetailerId) as count, monthname(date(FROM_UNIXTIME(iDtAdded))) as month,sum(o.fAmount) as total,GROUP_CONCAT(iTotalTime SEPARATOR ',') AS iTotalTime FROM orders as o where iUserId='$iId'  group by month";
    $row6check = $obj->select($sql6check);

    $db_fet6 = array();
    for($zz=1; $zz <= 12; $zz++) {
        ///Distributor  Area 1


        $sql5 = "select monthname(date(FROM_UNIXTIME(iDtAdded))) as month,count(iRetailerId) as count from orders where iUserId='$iId' and MONTH(date(FROM_UNIXTIME(iDtAdded))) = '{$zz}' group by month ORDER BY iDeliveryDate Asc";
        $row = $obj->select($sql5);
        if(count($row) == 0) {
            $row[0]['count'] = 0;
            $row[0]['month'] = date('F', mktime(0, 0, 0, $zz, 10));;
        }
        $db_fet5[] = $row[0];

        ///Distributor  Line 2
        $sql6 = "SELECT count(distinct o.iRetailerId) as count, monthname(date(FROM_UNIXTIME(iDtAdded))) as month,sum(o.fAmount) as total,GROUP_CONCAT(iTotalTime SEPARATOR ',') AS iTotalTime FROM orders as o where iUserId='$iId' AND MONTH(date(FROM_UNIXTIME(iDtAdded))) = '{$zz}' group by month";
        $row = $obj->select($sql6);
        if(count($row) == 0) {
            $row[0]['count'] = 0;
            $row[0]['month'] = date('F', mktime(0, 0, 0, $zz, 10));;
            $row[0]['total'] = 0;
            $row[0]['iTotalTime'] = 0;
        }
        $db_fet6[] = $row[0];

    }

///Distributor  column 3

    $sql7check = "select u.vFirstName,u.vLastName,u.vStoreUniqueId as name,count(o.iRetailerId) as count,sum(o.fAmount) as total,GROUP_CONCAT(o.iTotalTime SEPARATOR ',') as iTotalTime from orders o left join user u on o.iRetailerId=u.iUserId where o.iUserId='$iId' AND o.iDtAdded between '$FirstDt' AND '$CurrentDt' group by o.iRetailerId order by total asc limit 10";
    $db_fet7check =$obj->select($sql7check);
    /* $sql7 = "SELECT count(distinct o.iRetailerId) as count,monthname(date(FROM_UNIXTIME(iDtAdded))) as month,sum(o.fAmount) as total,GROUP_CONCAT(iTotalTime SEPARATOR ',') AS iTotalTime  from orders o where iUserId = '$iId'  group by month ORDER BY iDtAdded ASC";
    $db_fet7 =$obj->select($sql7);*/

    $sql7="select u.vFirstName,u.vLastName,u.vStoreUniqueId as name,count(o.iRetailerId) as count,sum(o.fAmount) as total,GROUP_CONCAT(o.iTotalTime SEPARATOR ',') as iTotalTime from orders o left join user u on o.iRetailerId=u.iUserId where o.iUserId='$iId' AND o.iDtAdded between '$FirstDt' AND '$CurrentDt' group by o.iRetailerId order by total asc limit 10";
    $db_fet7 =$obj->select($sql7);

}

for ($s = 0; $s < count($db_fet7); $s++) {
    $SUM1 = "";
    $totalt = $db_fet7[$s]['iTotalTime'];
    $tt = explode(',', $totalt);

    for ($t = 0; $t < count($tt); $t++) {

        //  echo "abc".$data1[$k]['iTotalTime'];
        //echo " => ".$data1[$k]['iOrderId'];
        $T_A = explode(':', $tt[$t]);
        $SUM1 += (int)($T_A[0] * 3600) + ($T_A[1] * 60) + ($T_A[2]);
    }

    $bar_chart_data[] = $SUM1;

    $hours = (strlen(floor($SUM1 / 3600)) == 1) ? '0' . floor($SUM1 / 3600) : floor($SUM1 / 3600);
    $minutes = (strlen(floor(($SUM1 / 60) % 60)) == 1) ? '0' . floor(($SUM1/ 60) % 60) : floor(($SUM1 / 60) % 60);
    $seconds = (strlen($SUM1 % 60) == 1) ? '0' . $SUM1 % 60 : $SUM1 % 60;

    $FINAL = $hours . ":" . $minutes . ":" . $seconds;
    $timevar9[$s]['time'] = $FINAL;
    $timevar9[$s]['name'] = $db_fet7[$s]['name'];
    $timevar9[$s]['hour'] = $hours . "." . $minutes . "" . $seconds;
    $timevar9[$s]['Seconds'] = $SUM1;
    $timevar9[$s]['Total_count'] = $db_fet7[$s]['total'];
    $timevar9[$s]['Total_count_average'] = $db_fet7[$s]['total'] / $db_fet7[$s]['count'];
    $timevar9[$s]['count_No'] = $db_fet7[$s]['count'];
    $timevar9[$s]['Seconds_average'] = $SUM1 / $db_fet7[$s]['count'];

}

/*
echo "<pre>";
print_r($timevar9);*/


for ($p = 0; $p < count($db_fet6); $p++) {
    $SUM1 = "";
    $totalt = $db_fet6[$p]['iTotalTime'];
    $tt = explode(',', $totalt);

    for ($q = 0; $q < count($tt); $q++) {

        //  echo "abc".$data1[$k]['iTotalTime'];
        //echo " => ".$data1[$k]['iOrderId'];
        $T_A = explode(':', $tt[$q]);
        $SUM1 += (int)($T_A[0] * 3600) + ($T_A[1] * 60) + ($T_A[2]);
    }

    $bar_chart_data[] = $SUM1;

    $hours = (strlen(floor($SUM1 / 3600)) == 1) ? '0' . floor($SUM1 / 3600) : floor($SUM1 / 3600);
    $minutes = (strlen(floor(($SUM1 / 60) % 60)) == 1) ? '0' . floor(($SUM1/ 60) % 60) : floor(($SUM1 / 60) % 60);
    $seconds = (strlen($SUM1 % 60) == 1) ? '0' . $SUM1 % 60 : $SUM1 % 60;

    $FINAL = $hours . ":" . $minutes . ":" . $seconds;
    $timevar7[$p]['time'] = $FINAL;
    $timevar7[$p]['name'] = $db_fet6[$p]['month'];
    $timevar7[$p]['hour'] = $hours . "." . $minutes . "" . $seconds;
    $timevar7[$p]['Seconds'] = $SUM1;
    // $timevar7[$p]['Seconds'] = $SUM1/$db_fet6[$p]['count'];
    $timevar7[$p]['Seconds_average'] = $SUM1 / $db_fet6[$p]['month'];
}
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="mind" >
    <link rel="shortcut icon" href="images/favicon.html">

    <title>Dashboard</title>

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet"/>
    <!--Morris Chart CSS -->
    <!--<link rel="stylesheet" href="js/morris-chart/morris.css">-->
    <script src="<?php echo $assets_url; ?>js/morris-chart/morris.js"></script>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery Flot Chart-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">

        // Load the Visualization API and the piechart package.
        google.load('visualization', '1.0', {'packages': ['corechart']});


        google.setOnLoadCallback(drawChart);


        function drawChart() {

            var data1 = new google.visualization.DataTable();
            data1.addColumn('string', 'Topping');
            data1.addColumn('number', 'Slices');
            data1.addRows([


                <?php   for($i=0;$i<count($db_fet1);$i++) {?>
                [
                    '<?php echo $db_fet1[$i]['vStoreUniqueId']; ?>',
                    <?php echo $db_fet1[$i]['total']; ?>
                ], <?php } ?>

            ]);


            var data5 = google.visualization.arrayToDataTable([
                ['Total # of Accounts Visited by Month', 'No Of Vender'],

                <?php   for($i=0;$i<count($db_fet5);$i++) {?>
                [
                    '<?php echo substr($db_fet5[$i]['month'],0,3); ?>',
                    <?php echo $db_fet5[$i]['count']; ?>
                ], <?php } ?>

            ]);


            var data6 = google.visualization.arrayToDataTable([
                ['Avg. Service Time of Accounts by Month', 'Average Service Time(Minutes)'],


                <?php   for($j=0;$j<count($timevar7);$j++) {?>
                [
                    '<?php echo substr($timevar7[$j]['name'],0,3); ?>',
                    <?php echo round($timevar7[$j]['Seconds'] / 60); ?>
                ], <?php } ?>

            ]);

            var data7 = google.visualization.arrayToDataTable([
                ['Month', 'Amount($)', 'Minutes'],
                <?php   for($d=0;$d<count($timevar9);$d++) {?>
                [
                    '<?php echo $timevar9[$d]['name']; ?>',
                    <?php echo $timevar9[$d]['Total_count_average']; ?>,
                    <?php echo $timevar9[$d]['Seconds']/ 60; ?>

                ], <?php } ?>


            ]);

            var data8 = google.visualization.arrayToDataTable([
                ['Genre', 'Fantasy & Sci Fi', 'Romance', 'Mystery/Crime', 'General',
                    'Western', 'Literature', { role: 'annotation' } ],
                ['Jan', 10, 24, 20, 32, 18, 5, ''],
                ['Feb', 16, 22, 23, 30, 16, 9, ''],
                ['March', 0, 24, 20, 32, 18, 5, ''],
                ['April', 16, 0, 23, 30, 16, 9, ''],
                ['May', 16, 22, 23, 30, 16, 9, ''],
                ['June', 28, 19, 29, 30, 12, 13, '']
            ]);


            var options1 = {
                'title': '',
                'width': 485,
                'height': 280,
                is3D: true,
                chartArea: {
                    left: 80,
                    top: 70
                },

            };



            var options5 = {
                series: [
                    {type: 'line', lineWidth: 2,pointSize: 7}, {}, {}

                ],
                chartArea: {
                    left: 40,
                    top: 20
                },
                legend:{position: 'none'},
                title: '',
                <?php if($user_type_id=="2"||$user_type_id=="3"){?>
                hAxis: {title: 'Total Number of Retailers Visited by Month',  titleTextStyle: {color: '#333'},titleTextStyle: {bold: true}},
                vAxis: { title: 'Retailers',titleTextStyle: {color: '#333'},titleTextStyle: {bold: true}},
                <?php }else{?>
                hAxis: {title: 'Total Number of Vendors Visited by Month',  titleTextStyle: {color: '#333'},titleTextStyle: {bold: true}},
                vAxis: { title: 'Vendors',titleTextStyle: {color: '#333'},titleTextStyle: {bold: true}}, <?}?>
            };

            var options6 = {
                series: [
                    {color: 'red',visibleInLegend: false,type: 'line', lineWidth: 2,pointSize: 7}, {}, {}

                ],
                chartArea: {
                    left: 40,
                    top: 20
                },
                title: '',


                hAxis: {title: 'Average Service Time of vendors by Month',  titleTextStyle: {color: '#333'},titleTextStyle: {bold: true}},
                vAxis: { title: 'Minutes',titleTextStyle: {color: '#333'},titleTextStyle: {bold: true}},

            };

            var options7 = {
                chart: {
                    title: 'Multi category bar chart',
                    subtitle: 'Sales, Expenses, and Profit: 2014-2017',

                },
                chartArea: {
                    left: 35,
                    top: 20
                },
                width: 520,
                height:280,

            };
            var options8 = {
                width: 700,
                height: 300,

                legend: { position: 'top', maxLines: 3 },
                bar: { groupWidth: '75%' },
                isStacked: true
            };

            // Instantiate and draw our chart, passing in some options.

            var chart1 = new google.visualization.PieChart(document.getElementById('chart_div1'));
            chart1.draw(data1, options1);


            var chart5 = new google.visualization.AreaChart(document.getElementById('chart_div_first'));
            chart5.draw(data5, options5);
            var chart6 = new google.visualization.AreaChart(document.getElementById('chart_div_second'));
            chart6.draw(data6, options6);

            var chart7 = new google.visualization.ColumnChart(document.getElementById('columnchart_material1'));

            chart7.draw(data7, options7);


            var chart8 = new google.visualization.ColumnChart(document.getElementById('multi_columnchart'));

            chart8.draw(data8, options8);

        }
    </script>

    <!--Divs that will hold the charts-->
</head>
<body>
<section id="main-content" xmlns="http://www.w3.org/1999/html">
<section class="wrapper">

<?php 
$plan=$USER[0]['eActivePaidPlan'];
 if($plan=="0"){?>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- First_ad -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-1473821199541954"
     data-ad-slot="4063164228"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

<?php }?>
<!--<div id="chart_div3"></div>-->

<!--main content start-->



<!-- page start-->
<? echo $_SESSION['error_csv'] ?>

<? if (isset($_SESSION['error_csv'])) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-block alert-danger fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <strong>Warning!</strong> <?php echo $_SESSION['error_csv'] ?>
            </div>
        </div>
    </div>
<? } ?>

<div class="row">
<div class="col-md-12">
<section class="panel">
<div class="panel-body profile-information">
    <div class="row des-pan-border">
        <div class="des-bord-title_new"> Welcome
            <span> <?php echo $USER[0]['vFirstName'] . ' ' . $USER[0]['vLastName'] ?> </span>
        </div>

        <div class="col-md-1" align="center">
            <div class="profile-pic dashboard_new_1 img">
                <?php $IMAGE = ((file_exists($user_image_path . $USER[0]['vImage']))&& $USER[0]['vImage']!="") ? $user_image_url . $USER[0]['vImage'] : $dafault_image_logo_user; ?>
                <img src="<?php echo $IMAGE; ?>" alt=""/>
            </div>
        </div>

        <div class="example col-md-4">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <th scope="col" class="dashboard-address_owner">Owner</th>
                </tr>
                <tr>
                    <td scope="col">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td valign="top" scope="col" class="dashboard-address_font">Account</td>
                                <td valign="top" scope="col">:&nbsp; </td>
                                <td valign="top" scope="col" class="dashboard-address_font"><?php echo $USER[0]['vStoreUniqueId']?> <?php
                                    if($USER[0]['vStoreName']!="")
                                    {


                                    if($user_type_id=='4' ||$user_type_id=='5' ||$user_type_id=='6'){?>#<?php echo $USER[0]['vStoreName']?><?}}?> </td>
                            </tr>

                            <tr>
                                <td valign="top" scope="col" class="dashboard-address_font">Email</td>
                                <td valign="top" scope="col">:&nbsp; </td>
                                <td valign="top" scope="col" class="dashboard-address_font"><?php echo $USER[0]['vEmail']?> </td>
                            </tr>

                            <tr>
                                <td valign="top" scope="col" class="dashboard-address_font">Phone</td>
                                <td valign="top" scope="col">:&nbsp; </td>
                                <td valign="top" scope="col" class="dashboard-address_font"><?php echo $USER[0]['vContact']?> </td>
                            </tr>

                            <tr>
                                <td valign="top" scope="col" class="dashboard-address_font">Address</td>
                                <td valign="top" scope="col">:&nbsp; </td>
                                <td valign="top" scope="col" class="dashboard-address_font"><?php echo $USER[0]['vAddress']?>,<?php echo $USER[0]['vCity']?>,<?php echo $USER[0]['vState']?>,<?php echo " ".$USER[0]['vZip']?>,<?php echo $USER[0]['iCountryId']?></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="col-md-2" align="center">
            <div class="giftexample">
                <a href="index.php?file=rp-redeem">
                    <div class="mini-statnew">
                        <img src="<?$admin_url?>images/gift_new_icon.png">
                        <div class="mini-statnew-info">
                            <span class="mini_statnew_points"><?php echo $points; ?></span><br>
                            Points
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <?
        $userplan=$USER[0]['eActivePaidPlan'];
        if($userplan=="0" && $user_type_id!='4' ){?>
            <div class="col-md-4">
                <!--     <div class="dashboard-day-box-bor">
            <div class="des-day-box text-center">
                <div class="day-title-bg"> <?php echo $days;?> </div> Days Left
            </div>
        </div>
    </div>-->

                <div class="remaining-days-alert">
                    <p>Please use the full dashboard features for the next 30 day free trial.
                    </p>
                    <h3> <span style="font-size:24px; color:#666;">You have</span><br>
                        <span class="user_round_text"><?php echo $days?></span><br>
                        <span style="font-size:24px; color:#666;">Days for Free Trial</span> </h3>

                    <h5 class="updatenow" align="right"><a href="<?php echo $payment_url; ?>">Upgrade now</a></h5>
                </div>
            </div>
        <?}?>
    </div>

    <div class="row des-pan-border">
        <div class="col-md-6">
            <?php if ($user_type_id == 4 || $user_type_id == 5 || $user_type_id == 6) { ?>
            <style>
                .panel {
                    padding: 0 !important;
                    margin: 0 !important;
                }
                /*	.panel-body {
                        padding: 0 0 0 15px;
                }*/

                .panel-body {
                    padding-bottom: 15px !important;
                }
            </style>

            <?php if ($db_fet1) { ?>

            <!-- <div id="chart_div1"></div>-->
            <section class="panel">
                <header class="panel-heading_dashboard">
                    $ Spent by Vendor
                </header>
                <div class="panel-body">
                    <div id="chart_div1"></div>
                </div>

            </section>
        </div>
        <?php } else { ?>

                <header class="panel-heading_dashboard">

                    $ Spent by Vendor
                </header>
        <div class="panel-body">
                <div class="col-md-12 no_image_area1" style="  margin-left: -54px !important;">
                    <h1 style="text-align: center">  <br> No Charts Available!</h1>
                </div>
            </div>
            </div>

        <?php } ?>
        <?php } elseif ($user_type_id == 2 || $user_type_id == 3) { ?>
        <style>
            .panel {
                padding: 0 !important;
                margin: 0 !important;
            }

            .panel-body {
                padding-bottom: 15px !important;
            }
        </style>


        <?php if ($db_fet1) { ?>
            <section class="panel">
                <header class="panel-heading_dashboard">
                    $ Sell by Retailer
                </header>
                <div class="panel-body">
                    <div id="chart_div1"></div>
                </div>

            </section>
        <?php } else { ?>
            <header class="panel-heading_dashboard">
                $ Sell by Retailer
            </header>
            <div class="panel-body">
                <div class="col-md-12 no_image_area1" style="  margin-left: -54px !important;">
                    <h1 style="text-align: center">  <br> No Charts Available!</h1>
                </div>

            </div>
        <?php } ?>
    </div>
    <?php } ?>


    <div class="col-md-6">
        <!--  <div class="des-day-box-bor">
            <div class="des-day-box text-center">
                <div class="day-title-bg"> <?php echo $days;?> </div> Days Left
            </div>
        </div>-->
        <section class="panel">

            <header class="panel-heading_dashboard">
                <?php if ($user_type_id == 2 || $user_type_id == 3) { ?>
                    Total Number of Retailers Visited by Month
                <?php } else {?>
                    Total Number of Vendors Visited by Month
                <?php } ?>
            </header>


<?php //echo count($row5check);?>
                    <?php if(count($row5check)>0){?>
                        <div id="visitors-chart" align="center">
                            <div class="panel-body panel_part_new">
                        <!--  <img src="<?php /*echo $admin_url*/?>images/column-chart.png" alt="" width="440">-->
                        <!--<img src="images/column-chart.png" alt="" width="440">-->
                        <div id="chart_div_first" class="first_graph"></div>
                </div>
            </div>
                    <?php }else{?>

                            <div class="col-md-12 no_image_area1" style="  ">
                                <h1 style="text-align: center;margin:35px 0px -1px -60px">  <br> No Charts Available!</h1>
                            </div>


                    <?php }?>


        </section>
    </div>
</div>

<div class="row des-pan-border">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading_dashboard">
                <?php if ($user_type_id == 2 || $user_type_id == 3) { ?>
                    $ Spent vs. Service by Retailers by Month (top 10 high dollar)
                <?php } else {?>
                    $ Spent vs. Service by Vendor by Month (top 10 high dollar)
                <?php } ?>
            </header>

            <div class="panel-body" align="center">
                <div id="visitors-chart">

                    <?php if(count($db_fet7check)>0){?>

                        <div id="columnchart_material1"  class="three_graph"></div>
                    <?php }else {?>
                        <div class="col-md-12 no_image_area1" style="  margin-left: -54px !important;">
                            <h1 style="text-align: center">  <br> No Charts Available!</h1>
                        </div>
                    <?}?>
                </div>
            </div>
        </section>
    </div>

    <div class="col-md-6">
        <header class="panel-heading_dashboard">
            <?php if ($user_type_id == 2 || $user_type_id == 3) { ?>
                Average Service Time of Retailers by Month
            <?php } else {?>
                Average Service Time of Vendors by Month
            <?php } ?>

        </header>
        <div class="panel-body panel_part_new">

            <?if(count($db_fet7check)>0){?>

                <div id="chart_div_second"  class="first_graph"></div>
            <?}else{?>
                <div class="col-md-12 no_image_area1" style="  margin-left: -54px !important;">
                    <h1 style="text-align: center">  <br> No Charts Available!</h1>
                </div>
            <?}?>

            <!--  <img src="images/column-chart.png" alt="" width="440">-->
            <!--   <div id="chart_div_second" style="width: 450px; height: 300px;"></div>-->
        </div>
    </div>
</div>

<div class="row">
    <?php if ($user_type_id == 4 || $user_type_id == 5 || $user_type_id == 6) { ?>
        <!--  <div class="des-pan-border" id="top_x_div1"></div>-->
        <style>
            /* #map-canvas {
                 width: 94%;
                 margin: 10px;
                 padding: 10px
             }*/
        </style>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
        <script type="text/javascript">
        
            function initialize() {
                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng(<?php echo $gph[0]['vLatitude']; ?>, <?php echo $gph[0]['vLongitude'] ?>),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                setMarkers(map, beaches);
            }

            var beaches = [
                <?php
                    $i=1;
                    foreach($gph as $v){
                    if($v['vLatitude'] != '') {
                ?>
                ['<?php echo $v['vStoreUniqueId'].'<br>'.$v['vStoreName']; ?>', <?php echo $v['vLatitude']; ?>, <?php echo $v['vLongitude'];?>, <?php echo $i;?>],
                <?php
                    $i++;
                        }
                    }
                    ?>
            ];

            function setMarkers(map, locations) {
                var image = {
                    url: '<?php echo $rate_image_url?>mark123.png',
                    size: new google.maps.Size(70, 70),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(70, 70)
                };
                var image1 = {
                    url: '<?php echo $rate_image_url?>mark123.png',
                    size: new google.maps.Size(70, 70),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(70, 70)
                };
                var shape = {
                    coords: [1, 1, 1, 20, 18, 20, 18, 1],
                    type: 'poly'
                };

                var infowindow = new google.maps.InfoWindow({
                    content: ""
                });
                for (var i = 0; i < locations.length; i++) {
                    var beach = locations[i];
                    var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
                    var newimage;
                    if (i == 1) {
                        newimage = image1;
                    } else {
                        newimage = image;
                    }

                    var marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,

                        shape: shape,
                        title: beach[0],
                        zIndex: beach[3]
                    });
                    bindInfoWindow(marker, map, infowindow, beach[0]);

                }
            }

            function bindInfoWindow(marker, map, infowindow, description) {
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.setContent(description);
                    infowindow.open(map, marker);
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
        <div id="map-canvas" width="200px" height="200px"></div>
    <?php } elseif ($user_type_id == 2 || $user_type_id == 3) { ?>

        <!-- <?php /*if ($timevar1) { */?>
            <div class="col-sm-12" style="padding-left: 0 !important; right:1% !important; margin-left: 1.5%">
                <div id="top_x_div" style="width: 900px; height: 500px;"></div>
            </div>

        <?php /*} else { */?>
            <div class="col-sm-12" style="padding-left: 0 !important; right:1% !important;">
                <img src="<?/*= $user_image_url */?>sorry_no_data.png">
            </div>-->
    <?php  } ?>
</div>
</div>
</section>
</div>
</div>


</section>
</section>


<style>
    rect {
        width: 50 !important;
    }
</style>
<? /* include_once($admin_path . 'js_form.php');*/ ?>

<?php

//include_once($admin_path . 'js_datatable.php');
?>

<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 21/5/15
 * Time: 3:19 PM
 */
?>


<script src="<?php echo $assets_url ?>js/jquery.js"></script>
<script src="<?php echo $assets_url ?>bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo $assets_url ?>js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo $assets_url ?>js/jquery.scrollTo.min.js"></script>
<script src="<?php echo $assets_url ?>js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php echo $assets_url ?>js/jquery.nicescroll.js"></script>
<script src="<?php echo $assets_url ?>js/sparkline/jquery.sparkline.js"></script>
<script src="<?php echo $assets_url ?>js/scripts.js"></script>
<script type="text/javascript" src="<?php echo $admin_url; ?>assets/js/gritter/js/jquery.gritter.js"></script>
<script src="<?php echo $assets_url ?>js/icheck-init.js"></script>
<script src="<?php echo $assets_url; ?>js/morris-chart/raphael-min.js"></script>



</body>
<!-- Mirrored from bucketadmin.themebucket.net/flot_chart.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 24 May 2014 12:09:34 GMT -->
</html>