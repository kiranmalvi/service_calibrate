<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">

                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>

                    <li>
                        <a class="current" href="javascript:;"> Manage Account</a></li>
                </ul>
            </div>
        </div>
    </section>
    <section class="panel">


        <header class="panel-heading tab-bg-dark-navy-blue">
            <div class="manu-main-scroll">
                <div class="manu-main-scroll-next">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-manage_store'">
                                Manage Account
                            </a>
                        </li>


                        <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-change_password'">
                                Change Password
                            </a>
                        </li>

                        <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-payment_info'">
                                Billing Address
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-transaction'">
                                Billing History
                            </a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" onclick="location.href='index.php?file=su-recurring'">
                                Payment Settings
                            </a>
                        </li>
                        <!-- <li class="'">
                                        <a data-toggle="tab" onclick="location.href='index.php?file=su-delete_account'">
                                            Delete Account

                                        </a>
                                    </li>-->
                    </ul>
                </div>
            </div>
        </header>


        <div class="panel">
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="col-md-pull-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <center><label class="col-md-12 control-label"><h4>
                                            You have reached your maximum account limit.Contact <a href='mailto:sales@servicecalibrate.com' style="color:#0000FF !important"><u>sales@servicecalibrate.com</u> </a> to add more accounts.
                                </h4>


                                        <button id="addbutton" type="button" class="btn btn-success1"
                                                onclick="location.href='index.php?file=su-manage_store'">Back
                                        </button>
                                    </label></center>

                            </div>


                        </div>


                    </div>
                </div>

            </div>

        </div>
        </div>

    </section>
</section>

<? include_once($admin_path . 'js_form.php'); ?>
<!--<script src="<?php /*echo $assets_url */ ?>js/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php /*echo $assets_url */ ?>bs3/js/bootstrap.min.js"></script>-->
<script src="<?php echo $assets_url ?>js/jquery.blockUI.js"></script>
<script src="<?php echo $assets_url ?>js/iCheck/jquery.icheck.min.js"></script>

<script src="<?php echo $assets_url ?>js/less-1.5.0.min.js"></script>

<!-- end: MAIN JAVASCRIPTS -->
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>
<script src="<?php echo $admin_url; ?>assets/js/scripts.js"></script>
<!--icheck init -->
<!-- End Check box-->


<!-- multiple -->
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/jquery-multi-select/js/jquery.quicksearch.js"></script>

<script type="text/javascript"
        src="<?php echo $admin_url; ?>assets/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>

<script src="<?php echo $admin_url; ?>assets/js/jquery-tags-input/jquery.tagsinput.js"></script>


