
<?php
include_once($inc_class_path . 'plan.class.php');
$planObj = new plan();
include_once($inc_class_path . 'feature.class.php');
$featureObj = new feature();


$planListing = $planObj->selectAllPlan();
//echo "<pre>"; print_r($planListing);echo "</pre>";
//$featureListing = $featureObj->selectAllFeature();

?>
<section id="main-content">

    <section class="wrapper">
        <?php if ($_SESSION[$session_prefix . 'admin_var_msg'] != "") {

            ?>
            <div class="alert <?php echo $_SESSION[$session_prefix . 'admin_msg_class']; ?> alert-block fade in">
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <p><? echo $_SESSION[$session_prefix . 'admin_var_msg']; ?> </p>
            </div>
        <? } ?>
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Title
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-cog" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                         </span>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal" action="index.php?file=pl-plan_a" method="post" id="planFeatureAdd" name="planFeatureAdd">
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Plan</label>
                            <div class="col-lg-6">
                                <select class="form-control" style="width: 300px" id="iPlanId" name="iPlanId" onchange="getFeatureList()" >
                                    <option value="">Select Plan</option>
                                    <?php
                                     foreach($planListing as $plan)
                                     {
                                    ?>
                                         <option value="<?php echo $plan['iPlanId'];?>"><?php echo $plan['vPlanName'];?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">Feature</label>
                            <div class="col-lg-6">
                                <select class="form-control" style="width: 300px" id="iFeatureId" name="iFeatureId">
                                    <option value="">Select Feature</option>
                                   <!-- <?php
/*                                    foreach($featureListing as $feature)
                                    {
                                        */?>
                                        <option value="<?php /*echo $feature['iFeatureId'];*/?>"><?php /*echo $feature['vFeatureName'];*/?></option>
                                    --><?php /*}*/?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <button class="btn btn-primary" type="submit" name="planFeature" id="planFeature" value="Add">Save</button>
                                <button class="btn btn-default" type="button">Cancel</button>
                            </div>
                        </div>


                    </form>
                </div>
            </section>
        </div>

        </section>
    </section>
<script>
    function getFeatureList()
    {
        var planId = $('#iPlanId').val();
        //alert("plan Id : " + planId);
        $.ajax({
            //url: getBaseURL() + 'getcouponlist.php',
            url: 'index.php?file=pl-plan_a',
            type: 'POST',
            data: {
                "iPlanId": planId,
                "action":"featureList"
            },
            success: function (result) {
            $('#iFeatureId').html(result);

            }
        });
    }
</script>
<? include_once($admin_path . 'js_form.php'); ?>
<!-- end: MAIN JAVASCRIPTS -->
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>


<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#planFeatureAdd');
            $('#planFeatureAdd').validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    iPlanId: {
                        required: true
                    },
                    iFeatureId: {
                        required: true
                    }

                },
                messages: {
                    iPlanId: {
                        required: "Please Select Plan"
                    },
                    iFeatureId: {
                        required: "Please Select Feature"

                    }


                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
//        Main.init();
        FormAdminValidator.init();
        $('#planFeature').click(function () {
            $('#planFeatureAdd').submit();
        });
    });
</script>
