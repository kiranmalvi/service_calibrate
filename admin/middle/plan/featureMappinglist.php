<?php
include_once($inc_class_path . 'plan.class.php');
$planObj = new plan();

$plans = $planObj->select_plan_list($_REQUEST['type']);

include_once($inc_class_path . 'feature.class.php');
$featureObj = new feature();
$features = $featureObj->select_with_user($_REQUEST['type']);

include_once($inc_class_path . 'user_type.class.php');
$usertypeObj = new user_type();

$typename = $usertypeObj->get_name($_REQUEST['type']);



?>
<section id="main-content" class="">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo $typename[0]['vName']; ?>   Plan List
                    </header>
                    <div class="panel-body">

                        <div class="adv-table">
                            <table class="table table-bordered table-striped table-condensed cf">
                                <thead>
                                <tr>
                                    <td></td>
                                    <?php
                                    foreach ($plans as $plan) {
                                        echo '<td>' . $plan['vPlanName'] . '</td>';
                                    }
                                    ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                include_once($inc_class_path . 'plan_feature_mapping.class.php');
                                $planFeatureMappingObj = new plan_feature_mapping();
                                foreach ($features as $feature) {
                                    ?>
                                    <tr>
                                        <td><?php echo $feature['vFeatureName']; ?></td>
                                        <?php
                                        foreach ($plans as $plan) {
                                            $CHECK = $planFeatureMappingObj->select_plan_feature($plan['iPlanId'], $feature['iFeatureId']);
                                            $checked = ($CHECK['tot'] == '1') ? 'checked' : '';
                                            echo '<td><input type="checkbox" ' . $checked . ' data-on="danger" data-off="default" onchange="change_Feture_Plan(this,\'' . $plan['iPlanId'] . '\',\'' . $feature['iFeatureId'] . '\')"></td>';
                                        }
                                        ?>
                                    </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>

<script>
    function change_Feture_Plan(th, plan, feature) {
        var action = ($(th).is(':checked') == true) ? 'insert' : 'delete';
        //  console.log(action);
        $.ajax({
            url: 'index.php?file=pl-plan_a',
            type: 'POST',
            data: {
                "action": action,
                "planId": plan,
                "featureId": feature,
                "TYPE": 'Change_Plan'
            }
        });

    }
</script>

<?
include_once($admin_path . 'js_form.php');
?>
