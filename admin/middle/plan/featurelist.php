<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 15/4/15
 * Time: 12:26 PM
 */

include_once($inc_class_path . 'feature.class.php');
$featureObj = new feature();
$features = $featureObj->select_with_user($_REQUEST['type']);

$MODULE = 'Feature';
$ADD_LINK = 'index.php?file=pl-featureadd';
$ACT_LINK = 'index.php?file=pl-feature_a';
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];

?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php">Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="#"><?php echo $MODULE; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <!-- Table Section Header Starts -->
                        <header class="panel-heading">
                            <?php echo $MODULE; ?> Listing
                        </header>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="plan_list" id="feature_list" method="post"
                                      action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <table class="display table table-bordered table-striped" id="feature_list_table">
                                        <!-- Changes HERE Start -->
                                        <thead>
                                        <tr>
                                            <? if ($atype != "R") { ?>
                                            <td>Action</td>
                                            <? } ?>
                                            <td>Feature</td>
                                            <td>Type</td>
                                            <td>Added</td>
                                            <td>Status</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($features as $FEATURE) {

                                            ?>
                                            <tr>
                                                <? if ($atype != "R") { ?>
                                                <td>
                                                    <a class="btn btn-success btn-xs"
                                                       href="<?php echo $ADD_LINK; ?>&mode=update&type=<?php echo $_REQUEST['type']; ?>&iId=<?php echo $FEATURE['iFeatureId']; ?>">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>
                                                </td>
                                                <? } ?>
                                                <td><?php echo $FEATURE['vFeatureName']; ?></td>
                                                <td><?php
                                                    if($FEATURE['eFeatureType']==0)
                                                    {
                                                        echo "Normal";
                                                    }
                                                    else{
                                                        echo "Additional";
                                                    }
                                                    ?></td>
                                                <td><?php echo date('m-d-Y h:i:s', $FEATURE['iDtAdded']); ?></td>
                                                <td>
                                                    <?php
                                                    if ($FEATURE['eStatus'] == '1') {
                                                        echo '<span class="label label-success">Active</span>';
                                                    } else {
                                                        echo '<span class="label label-danger">Inactive</span>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                        <!-- Changes HERE Ends -->
                                    </table>
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>


<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#feature_list_table').dataTable({
                /* "aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>

<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#feature_list_table').dataTable({
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0]}
                ]
            });
        });
    </script>
<?php
}?>

<?php
include_once($admin_path . 'js_datatable.php');
?>