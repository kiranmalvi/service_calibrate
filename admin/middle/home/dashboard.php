<?php
/**
 * Created by PhpStorm.
 * User: Vikrant
 * Date: 23/3/15
 * Time: 4:25 PM
 */


include_once('adddatatomaster.php');
if (isset($_SESSION['SC_LOGIN']['USER'])) {
    header('location:index.php?file=su-sudashboard&type=' . $_SESSION['SC_LOGIN']['USER']['iUserTypeId'] . '&iId=' . $_SESSION['SC_LOGIN']['USER']['iUserId']);
    exit;
}
$tsql = "SELECT iUserTypeId FROM user_type WHERE iUserTypeId = 4";
$ssql = "where iParentId=0 AND eStatus !='2' AND iUserTypeId in ($tsql) ";
$sql = "select count(*) as tot  from user " . $ssql;
$db_res = $obj->select($sql);
$single_location = $db_res[0]['tot'];

$tsql = "SELECT iUserTypeId FROM user_type WHERE iUserTypeId = 5";
$ssql = "where iParentId=0 AND eStatus !='2' AND iUserTypeId in ($tsql) ";
$sql = "select count(*) as tot  from user " . $ssql;
$db_res = $obj->select($sql);
$multi_location = $db_res[0]['tot'];

$tsql = "SELECT iUserTypeId FROM user_type WHERE iUserTypeId = 6";
$ssql = "where iParentId=0 AND eStatus !='2' AND iUserTypeId in ($tsql) ";
$sql = "select count(*) as tot  from user " . $ssql;
$db_res = $obj->select($sql);
$corporate_location = $db_res[0]['tot'];

$tsql = "SELECT iUserTypeId FROM user_type WHERE iUserTypeId = 2";
$ssql = "where iParentId=0 AND eStatus !='2' AND iUserTypeId in ($tsql) ";
$sql = "select count(*) as tot  from user " . $ssql;
$db_res = $obj->select($sql);
$distributor_location = $db_res[0]['tot'];

$tsql = "SELECT iUserTypeId FROM user_type WHERE iUserTypeId = 3";
$ssql = "where iParentId=0 AND eStatus !='2' AND iUserTypeId in ($tsql) ";
$sql = "select count(*) as tot  from user " . $ssql;
$db_res = $obj->select($sql);
$manufacturer_location = $db_res[0]['tot'];



/* for map */
$msql = "SELECT vFirstName,vLastName,vEmail,vLatitude,vLongitude FROM user WHERE eStatus != '2' AND iParentId = 0 AND iUserTypeId !=7  ";
$mdb_res = $obj->select($msql);
//echo "<pre>"; print_r(($mdb_res)); echo "</pre>";

/* end map */
?>
<style>
    .btcolor{
        background-color: white !important;
        height: 97px !important;
        width: 100% !important;
        padding-top: 18px !important;
        /*margin-top: -10px !important;*/

    }
    .col_margin{
        margin-top: -42px;
    }
    .col_margin_sec_row{
        margin-top: -47px;
    }
</style>


<section id="main-content">
    <section class="wrapper">

        <!--mini statistics start-->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading" style=" ;">
                        Total Users
                    </header>
                    <div class="col-md-4 col_margin">
                        <div class="mini-stat clearfix btcolor">
                            <a href="index.php?file=u-userlist&type=4">
                                <span class="mini-stat-icon orange"><i class="fa fa-shopping-cart"></i></span>
                                <div class="mini-stat-info">
                                    <span><?php echo $single_location;?></span>
                                    Single Location Owner
                                </div>
                            </a>
                        </div>

                    </div>
                    <div class="col-md-4 col_margin">
                        <div class="mini-stat clearfix btcolor">
                            <a href="index.php?file=u-userlist&type=5">
                                <span class="mini-stat-icon orange"><i class="fa fa-shopping-cart"></i></span>
                                <div class="mini-stat-info">
                                    <span><?php echo $multi_location;?></span>
                                    Multiple Location Owner
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col_margin">
                        <div class="mini-stat clearfix btcolor">
                            <a href="index.php?file=u-userlist&type=6">
                                <span class="mini-stat-icon orange"><i class="fa fa-shopping-cart"></i></span>
                                <div class="mini-stat-info">
                                    <span><?php echo $corporate_location;?></span>
                                    Corporate Account
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col_margin col_margin_sec_row">
                        <div class="mini-stat clearfix btcolor">
                            <a href="index.php?file=u-userlist&type=2">
                                <span class="mini-stat-icon tar"><i class="fa fa-truck"></i></span>
                                <div class="mini-stat-info">
                                    <span><?php echo $distributor_location;?></span>
                                    Distributor
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col_margin col_margin_sec_row">
                        <div class="mini-stat clearfix btcolor">
                            <a href="index.php?file=u-userlist&type=3">
                                <span class="mini-stat-icon pink"><i class="fa fa-building-o"></i></span>
                                <div class="mini-stat-info">
                                    <span><?php echo $manufacturer_location;?></span>
                                    Manufacturer
                                </div>
                            </a>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
    <!--mini statistics end-->
    <section class="wrapper" style=" margin-top: -15px;  ">
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                      Active Users 
                        <!--                        <span class="tools pull-right">
                                                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                                                    <a href="javascript:;" class="fa fa-cog"></a>
                                                    <a href="javascript:;" class="fa fa-times"></a>
                                                 </span>-->
                    </header>
                    <div class="panel-body">

                        <div id="gmap-tabs"></div>
                    </div>
                </section>
            </div>
        </div>
    </section>

</section>
<?php
include_once($admin_path . 'js_datatable.php');
?>


<!--Core js-->
<script src="js/jquery.js"></script>
<script src="bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.pie.resize.js"></script>
<!--Google Map-->
<!--<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>-->


<!--common script init for all pages-->
<script src="js/scripts.js"></script>
<!--<script src="http://maps.googleapis.com/maps/api/js"></script>-->
<script>
    /*function initialize() {
     var mapProp = {
     center:new google.maps.LatLng(51.508742,-0.120850),
     zoom:5,
     mapTypeId:google.maps.MapTypeId.ROADMAP
     };
     var map=new google.maps.Map(document.getElementById("gmap-tabs"),mapProp);
     }
     google.maps.event.addDomListener(window, 'load', initialize);*/

    jQuery(function($) {
        // Asynchronously Load the map API
        var script = document.createElement('script');
        script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
        document.body.appendChild(script);
    });

    function initialize() {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            zoom: 20,
            center: new google.maps.LatLng(23, 70),

            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        /*var myLatlng = new google.maps.LatLng(23,70);
         var mapOptions = {
         zoom: 4,
         center: myLatlng
         };*/

        // Display a map on the page

        map = new google.maps.Map(document.getElementById("gmap-tabs"), mapOptions);
        //map.setTilt(45);

        // Multiple Markers
        /* ['<?php echo $mapValue['vEmail']?>', <?php echo  $mapValue['vLatitude'];?>,<?php echo  $mapValue['vLongitude'];?>], */
        var markers = [
            <?php foreach($mdb_res as $k => $mapValue){ ?>

            ['<?php echo $mapValue['vEmail']?>', <?php echo  $mapValue['vLatitude'];?>,<?php echo  $mapValue['vLongitude'];?>],

            <?php }?>
        ];
        //console.log(markers);
        //console.log( markers.length);

        // Info Window Content
        var infoWindowContent1 = [

            <?php foreach($mdb_res as $k => $mapValue){ ?>

            ['<div class="info_content">' +
            '<h3><?php echo $mapValue['vFirstName']." ".$mapValue['vLastName'];?></h3>' +
            '<p><?php echo $mapValue['vEmail'];?></p>' +        '</div>'],

            <?php }?>


        ];

        // Display multiple markers on a map
        var infoWindow = new google.maps.InfoWindow(), marker, i;

        // Loop through our array of markers & place each one on the map
        for( i = 0; i < markers.length; i++ ) {
            console.log("#"+i+" Email : "+markers[i][0]+ "  Lat = >"+markers[i][1]+"  long=>     "+ markers[i][2]);
            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map,
                title: markers[i][0]
            });

            // Allow each marker to have an info window
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infoWindow.setContent(infoWindowContent[i][0]);
                    infoWindow.open(map, marker);
                }
            })(marker, i));

            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
        }

        // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
            this.setZoom(2);
            google.maps.event.removeListener(boundsListener);
        });

    }
</script>