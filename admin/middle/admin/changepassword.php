<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:51 PM
 */

$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';
if ($iId != '') {
    include_once($inc_class_path . 'admin.class.php');

    $admin = new admin();
    $db_res = $admin->select($iId);
}


$MODE_TYPE = ucfirst($mode);
$MODULE = 'Change Password';
$PRE_LINK = 'index.php';
$ACT_LINK = 'index.php?file=sc-admin_a';

?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>

                    <li>
                        <a class="current" href="javascript:;"> <?php echo $MODULE; ?> </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Form Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Form Section Header Starts -->
                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            <?php echo  $MODULE; ?>
                        </h4>
                    </header>
                    <!-- Form Section Header Ends -->

                    <!-- Form Section Body Starts -->
                    <div class="panel-body">
                        <?if(isset($_SESSION['admin_pwd_err'])&&$_SESSION['admin_pwd_err']!=""){?>
                        <div class="row" style="margin-left: 15px;">
                            <div class="alert  fade in col-md-8 <?php echo  $_SESSION['admin_pwd_clas']; ?>">
                               <?php echo $_SESSION['admin_pwd_err'];?>
                            </div>
                        </div>
    <?}?>

                        <form class="form-horizontal bucket-form" method="post" name="admin_form" id="admin_form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <input type="hidden" name="mode" value="changepwd">
                            <input type="hidden" name="iAdminId" id="iAdminId"
                                   value="<?php echo $db_res[0]['iAdminId']; ?>">


                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Old Password :</label>

                                <div class="col-md-8">
                                    <input type="password" name="oPasswd" id="oPasswd" class="form-control"
                                           value=""
                                           placeholder="Old Password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> New Password :</label>

                                <div class="col-md-8">
                                    <input type="password" name="nPasswd" id="nPasswd" class="form-control"
                                           value=""
                                           placeholder=" New Password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Confirm Password :</label>

                                <div class="col-md-8">
                                    <input type="password" name="cPasswd" id="cPasswd" class="form-control"
                                           value=""
                                           placeholder="Confirm Password">
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit"
                                            class="btn btn-success1">Change</button>
                                    <a href="<?php echo $PRE_LINK; ?>" class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- Form Section Body Starts -->

                </section>
            </div>
        </div>
        <!-- Form Section Ends -->

        <!-- page end-->
    </section>
</section>

<?
include_once($admin_path . 'js_form.php');
?>

<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#admin_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    oPasswd: {
                        required: true
                    },
                    nPasswd: {
                        required: true
                    },
                    cPasswd: {
                        required: true,
                        equalTo:"#nPasswd"

                    }
                },
                messages: {
                    oPasswd: {
                        required: "Please Enter Old Password"

                    },
                    nPasswd: "Plese Enter New Password",
                    cPasswd:{
                        required: "Please Enter Confirm Password",
                        equalTo:"please Enter Same Password as above "
                    }


                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#admin_form').submit();
        });
    });
</script>
<?unset($_SESSION['admin_pwd_err']);?>
<?unset($_SESSION['admin_pwd_cls']);?>