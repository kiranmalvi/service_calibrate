<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:29 PM
 */



echo date_default_timezone_get();
date_default_timezone_set(date_default_timezone_get());
echo '<br>';
echo date('Y-m-d H:i:s');
?>

    <script>
        var date = new Date();
        document.write("<br>"+date);
    </script>


<?php //pr($_SERVER);




include_once($inc_class_path . 'admin.class.php');

$admin = new admin();
$db_res = $admin->select();

$MODULE = 'Admin';
$ADD_LINK = 'index.php?file=sc-adminadd';
$ACT_LINK = 'index.php?file=sc-admin_a';
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];

$vEmail="foram.mindinventory@gmail.com";
$vStoreUniqueId="Sejal";

$NUMBER="123";
$col2="foram.mindinventory@gmail.com";
$DATA = array('email' => $col2, 'password' => $NUMBER);
echo   $generalfuncobj->get_SC_SEND_mail('csv_register', $DATA, $col2, 'Welcome To Service Calibrate');


?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;"><?php echo $MODULE; ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Table Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Table Section Header Starts -->
                    <header class="panel-heading">
                        <?php echo $MODULE; ?> Listing
                        <span class=" pull-right header-btn">

                             <? if ($atype != "R") { ?>
                            <a class="admin-btn btn btn-success1" href="<?php echo $ADD_LINK; ?>">
                                Add New <?php echo $MODULE; ?>
                            </a>


                            <button class="admin-btn btn btn-success1" onclick="delete_record()">
                                DELETE
                            </button>
                            <? } ?>
                        </span>
                    </header>
                    <!-- Table Section Header Ends -->

                    <!-- Table Section Body Starts -->
                    <div class="panel-body">
                        <div class="adv-table">
                            <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                <input type="hidden" name="mode" id="mode" value="">
                                <input type="hidden" name="delete_type" value="multi_delete">
                                <div class="admin-tbl-main"> 
                                <table class="display table table-bordered table-striped" id="admin_table">
                                    <!-- Changes HERE Start -->
                                    <thead>
                                    <tr>
                                      <? if ($atype != "R") { ?>
                                        <td><input type="checkbox" onclick="check_all();"></td>
                                        <td>Action</td> <? } ?>
                                        <td>User</td>
                                        <td>Email</td>
                                        <td>Admin Type</td>
                                        <td>Last Login</td>
                                        <td>Status</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($db_res as $ADMIN) {

                                        ?>
                                        <tr>
                                            <? if ($atype != "R") { ?>
                                            <td>
                                                <?php
                                                if ($ADMIN['iAdminId'] != $_SESSION['SC_LOGIN']['ADMIN']['iAdminId']) {
                                                    ?>
                                                    <input type="checkbox" name="delete[]"
                                                           value="<?php echo $ADMIN['iAdminId']; ?>">
                                                <?php
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a class="btn btn-success btn-xs edit-delete-btn" href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $ADMIN['iAdminId']; ?>"><i
                                                        class="fa fa-edit"></i> Edit</a>

                                                <?php
                                                if ($ADMIN['iAdminId'] != $_SESSION['SC_LOGIN']['ADMIN']['iAdminId']) {
                                                    ?>
                                                    <a class="btn btn-danger btn-xs edit-delete-btn" href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $ADMIN['iAdminId']; ?>&delete_type=single_delete"><i
                                                            class="fa fa-edit"></i> Delete</a>
                                                <?php
                                                }
                                                ?>
                                            </td>
                                            <? } ?>
                                            <td><?php echo $ADMIN['vFirstName'] . ' ' . $ADMIN['vLastName']; ?></td>
                                            <td><?php echo $ADMIN['vEmail']; ?></td>
                                            <td><?php
                                                if($ADMIN['eType']=="R")
                                                {
                                                    $ADMINTYPE="Regular Admin";
                                                }
                                                else if($ADMIN['eType']=="A")
                                                {
                                                    $ADMINTYPE="Admin Admin";
                                                }
                                                else
                                                {
                                                    $ADMINTYPE="Super Admin";
                                                }

                                                echo $ADMINTYPE; ?></td>
                                            <td>
                                                <?php
                                                if ($ADMIN['dtLastLogin'] != '0') {
                                                    echo gmdate('m-d-Y H:i:s', $ADMIN['dtLastLogin']);
                                                } else {
                                                    echo 'Not Login Until';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($ADMIN['eStatus'] == '1') {
                                                    echo '<span class="label label-success edit-active">Active</span>';
                                                } else {
                                                    echo '<span class="label label-danger edit-active">Inactive</span>';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                    <!-- Changes HERE Ends -->
                                </table>

											</div>                            
                            </form>
                        </div>
                    </div>
                    <!-- Table Section Body Ends -->

                </section>
            </div>
        </div>
        <!-- Table Section Ends -->

    </section>
</section>

<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#admin_table').dataTable({
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>

<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#admin_table').dataTable({
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });
    </script>
<?php
}

include_once($admin_path . 'js_datatable.php');
?>