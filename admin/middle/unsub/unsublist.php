<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:29 PM
 */

include_once($inc_class_path . 'user.class.php');

$user = new user();

include_once($inc_class_path . 'plan.class.php');

$planObj = new plan();
$db_res = $user->select_unsub();

//pr($db_res);
//pr($db_res);
$MODULE = 'Unsubscribe User';
$ADD_LINK = 'index.php?file=sc-adminadd';
$ACT_LINK = 'index.php?file=sc-admin_a';
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];
$today=strtotime(date('Y-m-d H:i:s'));
//$seconds = strtotime($date11) - strtotime($date22);
 $seconds=$expire-$today;
$hours = $seconds / 3600;
 $hrs = round($hours);
$FINALTIME= $hrs/24;

?>



<!--main content start-->
<section id="main-content">
    <section class="wrapper">

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;"><?php echo $MODULE; ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Table Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Table Section Header Starts -->
                    <header class="panel-heading">
                        <?php echo $MODULE; ?> Listing

                    </header>
                    <!-- Table Section Header Ends -->

                    <!-- Table Section Body Starts -->
                    <div class="panel-body">
                        <div class="adv-table">
                            <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                <input type="hidden" name="mode" id="mode" value="">
                                <input type="hidden" name="delete_type" value="multi_delete">
                                <div class="admin-tbl-main"> 
                                <table class="display table table-bordered table-striped" id="admin_table">
                                    <!-- Changes HERE Start -->
                                    <thead>
                                    <tr>

                                        <td>User</td>
                                        <td>Account NAme</td>
                                        <td>Email</td>
                                        <td>Customer Id</td>
                                        <td>Amount</td>
                                        <td>Plan</td>

                                        <td>Expire Date</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($db_res as $USER) {

                                        ?>
                                        <tr>


                                            <td><?php echo $USER['vFirstName'] . ' ' . $USER['vLastName']; ?></td>
                                            <td><?php echo $USER['vStoreUniqueId']; ?></td>
                                            <td><?php echo $USER['vEmail']; ?></td>
                                            <td>
                                                <?php echo $USER['iCustomerId']; ?>
                                            </td>
                                            <td>
                                                <?php
                                                 $expire= $USER['iExpireTime'];
                                                $today=$USER['iDtAdded'];
                                                $seconds=$expire-$today;

                                                $hours = $seconds / 3600;
                                                $hrs = round($hours);
                                               $FINALTIME= round($hrs/24);
                                                $plan=$USER['iPlanId'];

                                                $userplan=$planObj->select($plan);
                                                $store=$USER['vComment'];
                                                $store="1";

                                                if($USER['ePlanType']=="annual")
                                                {
                                                     $planprice=$userplan[0]['fDayPrice'];
                                                       $FINALPRICE=($planprice/30)*$FINALTIME*$store;


                                             }
                                                else
                                                {
                                                     $planprice=$userplan[0]['fMonthPrice'];
                                                     $FINALPRICE=($planprice/30)*$FINALTIME*$store;

                                                }

                                            echo    "$".$FINALPRICE; ?>
                                            </td>

                                            <td>
                                                <?php echo $USER['ePlanType']; ?>
                                            </td>
  <td>                                          <?php echo date('m-d-Y',$USER['iExpireTime']); ?>
                                            </td>


                                        </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                    <!-- Changes HERE Ends -->
                                </table>

											</div>                            
                            </form>
                        </div>
                    </div>
                    <!-- Table Section Body Ends -->

                </section>
            </div>
        </div>
        <!-- Table Section Ends -->

    </section>
</section>

<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#admin_table').dataTable({
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>

<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#admin_table').dataTable({
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });
    </script>
<?php
}

include_once($admin_path . 'js_datatable.php');
?>