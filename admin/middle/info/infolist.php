<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:29 PM
 */

##TEST

include_once($inc_class_path . 'info.class.php');
$info = new info();


$db_res = $info->select();

$MODULE = 'Information';
$ADD_LINK = 'index.php?file=i-infoadd';
$ACT_LINK = 'index.php?file=i-info_a';
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];

?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php">Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="javascript:;"> <?php echo $MODULE; ?></a>
                        </li>
                    </ul>
                </div>
            </div>


            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <!-- Table Section Header Starts -->
                        <header class="panel-heading">
                            <?php echo $MODULE; ?> Listing
                        <span class=" pull-right header-btn">
                          <? if ($atype != "R") { ?>
                            <a class="btn btn-success1" href="<?php echo $ADD_LINK; ?>">
                                Add New <?php echo $MODULE; ?>
<?php }?>                            </a>

                        </span>
                        </header>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <div class="admin-tbl-main">
                                    <table class="display table table-bordered table-striped" id="info_table">
                                        <!-- Changes HERE Start -->
                                        <thead>
                                        <tr>
                                            <? if ($atype != "R") { ?>
                                            <td>Action</td>
                                            <? } ?>
                                            <td>Title</td>
                                            <td>Keyword</td>
                                            <td>For</td>
                                            <td>Status</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($db_res as $INFO) {

                                            ?>
                                            <tr>

                                                <? if ($atype != "R") { ?>
                                                <td>
                                                    <a class="btn btn-success btn-xs edit-delete-btn"
                                                       href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $INFO['iInfoId']; ?>"><i
                                                            class="fa fa-edit"></i> Edit</a>
                                                </td>
                                                <? } ?>
                                                <td><?php echo $INFO['vTitle'] ?></td>
                                                <td><?php echo $INFO['vKeyword'] ?></td>
                                                <td><?php echo $INFO['VFor'] ?></td>


                                                <td>
                                                    <?php
                                                    if ($INFO['eStatus'] == '1') {
                                                        echo '<span class="label label-success edit-active">Active</span>';
                                                    } else {
                                                        echo '<span class="label label-danger edit-active">Inactive</span>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                        <!-- Changes HERE Ends -->
                                    </table>
												</div>                                
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>


<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#info_table').dataTable({
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>
<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#info_table').dataTable({
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0]}
                ]
            });
        });
    </script>
<?php
}
include_once($admin_path . 'js_datatable.php');
?>