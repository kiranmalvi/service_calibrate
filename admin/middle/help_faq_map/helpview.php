
<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 6/4/15
 */

include_once($inc_class_path . 'help_faq.class.php');
$helpfaqObj = new help_faq();

$db_res = $helpfaqObj->select();

include_once($inc_class_path . 'info.class.php');
$info = new info();

$info_icon = $info->select_report_info("help_faq_web");
//pr($db_res);

//echo "<pre>"; print_r($db_res); exit;

$MODULE = 'Help / FAQs / Guide';
?>
<style>
    body {
        font-family: "Trebuchet MS", "Helvetica", "Arial", "Verdana", "sans-serif";
        font-size: 10px;
    }
</style>
<link rel="stylesheet" href="<?php echo $admin_url ?>assets/js/helpfaq/jquery-ui.css">
<script src="<?php echo $admin_url ?>assets/js/helpfaq/jquery-1.10.2.js"></script>
<script src="<?php echo $admin_url ?>assets/js/helpfaq/jquery-ui.js"></script>

<script>
    $(function () {
        $("#accordion").accordion({
            collapsible: true
        });
    });
</script>

<section id="main-content">
    <section class="wrapper">
        <section class="panel">
            <div class="row">
                <div class="col-md-9">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php">Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="javascript:;">Help / FAQs / Guide </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 pull-right text-right">
                    <div class="form-group">
                        <a href="#myModal2" name="SearchReport" data-toggle="modal" class="btn btn-success1"
                           id="help_show" style="display: none">Help (?)</a>
                    </div>
                </div>
            </div>
            <header class="panel-heading">
                <?php echo $MODULE; ?>
            </header>


            <div class="panel-body">


                <div id="accordion">

                    <? for ($i = 0; $i < count($db_res); $i++) { ?>
                        <h3><i class="fa fa-youtube-play"></i><span
                                style="margin-left: 5px"><?php echo $db_res[$i]['vQuestion'] ?></span></h3>
                        <div>
                            <p><?php echo $db_res[$i]['vAnswer'] ?></p>
                            
                           <?php if($db_res[$i]['vLink']!=""){?>
                            <p>
                                <iframe class="yt_players" id="player<?php echo $i; ?>" width="420" height="285px"
                                        src="http://www.youtube.com/embed/<?php echo $db_res[$i]['vLink'] ?>?rel=0&wmode=Opaque&enablejsapi=1;"></iframe>
                            </p>
                            <?php }?>
                        </div>
                    <? } ?>

                </div>
        </section>
    </section>
</section>
<!--
    </div>

</div>-->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog-brodcast">
        <div class="modal-content">
            <div class="modal-header red-light-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Help</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="frmadd_view_promotion1" name="frmadd12">
                            <center>
                                <img src="<? echo $dafault_image_logo ?>" height="70px" width="70px"></center>
                            <center>

                                <center><h2>Help / FAQs / Guide </h2></center>
                                <? echo $info_icon[0]['tDescription'] ?>
                        </form>
                        </br>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!--Core js-->
<script type="text/javascript" src="http://www.youtube.com/player_api"></script>
<script type="application/javascript">

    players = new Array();

    function onYouTubeIframeAPIReady() {
        var temp = $("iframe.yt_players");
        for (var i = 0; i < temp.length; i++) {
            var t = new YT.Player($(temp[i]).attr('id'), {
                events: {
                    'onStateChange': onPlayerStateChange
                }
            });
            players.push(t);
        }

    }
    onYouTubeIframeAPIReady();


    function onPlayerStateChange(event) {

        if (event.data == YT.PlayerState.PLAYING) {
            //alert(event.target.getVideoUrl());
            // alert(players[0].getVideoUrl());
            var temp = event.target.getVideoUrl();
            var tempPlayers = $("iframe.yt_players");
            for (var i = 0; i < players.length; i++) {
                if (players[i].getVideoUrl() != temp) players[i].stopVideo();

            }
        }
    }

</script>
<?php
include_once($admin_path . 'js_form.php');
?>


