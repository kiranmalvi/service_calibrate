<?php

$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';
if ($iId != '') {
    include_once($inc_class_path . 'help_faq.class.php');

    $helpfaqObj = new help_faq();
    $db_res = $helpfaqObj->select($iId);

}


$MODE_TYPE = ucfirst($mode);
$MODULE = 'Help/FAQs/Guide';

$PRE_LINK = 'index.php?file=hfq-help_faq_map';
$ACT_LINK = 'index.php?file=hfq-help_faq_map_a';

?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <li>
                        <a href="<?php echo $PRE_LINK; ?>"> <?php echo $MODULE; ?></a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;"> <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Form Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Form Section Header Starts -->
                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail
                        </h4>
                    </header>
                    <!-- Form Section Header Ends -->

                    <!-- Form Section Body Starts -->
                    <div class="panel-body">

                        <form class="form-horizontal bucket-form" method="post" name="admin_form" id="admin_form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <input type="hidden" name="mode" value="<?php echo $mode; ?>">
                            <input type="hidden" name="iHelpId" id="iHelpId"
                                   value="<?php echo $db_res[0]['iHelpId']; ?>">
                            <?php if($mode == 'edit'){?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Question:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="vQuestion" id="vQuestion" class="form-control"
                                               value="<?php echo $db_res[0]['vQuestion']; ?>" placeholder="Question">
                                    </div>
                                </div>
                            <?}else{?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Question:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="vQuestion" id="vQuestion" class="form-control"
                                               value="<?php echo $db_res[0]['vQuestion']; ?>" placeholder="Question" >
                                    </div>
                                </div>

                            <?}?>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Answer:</label>

                                <div class="col-md-8">
                                    <textarea class="ckeditor form-control" placeholder="vAnswer"
                                              name="vAnswer"
                                              id="vAnswer"><?= $db_res[0]['vAnswer']; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"> Link :</label>

                                <div class="col-md-8">
                                    <input type="text" name="vLink" id="vLink" class="form-control"
                                           value="<?php echo $db_res[0]['vLink']; ?>" placeholder="link">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Status :</label>

                                <div class="col-md-8">
                                    <select class="form-control" name="eStatus" id="eStatus">
                                        <option selected disabled>- Select Status -</option>
                                        <option
                                            value="1" <?php echo($db_res[0]['eStatus'] == '1' ? 'selected' : ''); ?>>
                                            Active
                                        </option>
                                        <option
                                            value="0" <?php echo($db_res[0]['eStatus'] == '0' ? 'selected' : ''); ?>>
                                            Inactive
                                        </option>

                                    </select>
                                </div>
                            </div>





                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit"
                                            class="btn btn-success1"><?php if ($mode == "add") {
                                            echo "Add";
                                        } else {
                                            echo "Update";
                                        } ?></button>
                                    <a href="<?php echo $PRE_LINK; ?>" class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- Form Section Body Starts -->

                </section>
            </div>
        </div>
        <!-- Form Section Ends -->

        <!-- page end-->
    </section>
</section>

<?
include_once($admin_path . 'js_form.php');
?>
<script src="<?php echo $assets_url ?>plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    var editor = CKEDITOR.replace('vAnswer', {
        filebrowserBrowseUrl: '<?php echo $assets_url ?>ckeditor-ckfinder-integration/ckfinder/ckfinder.html',
        //filebrowserBrowseUrl : '<?php echo $assets_url ?>plugins/ckeditor-ckfinder-integration/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: '<?php echo $assets_url ?>ckeditor-ckfinder-integration/ckfinder/ckfinder.html?type=Images',
        filebrowserFlashBrowseUrl: '<?php echo $assets_url ?>ckeditor-ckfinder-integration/ckfinder/ckfinder.html?type=Flash',

        filebrowserUploadUrl: '<?php echo $assets_url ?>ckeditor-ckfinder-integration/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?php echo $assets_url ?>ckeditor-ckfinder-integration/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '<?php echo $assets_url ?>ckeditor-ckfinder-integration/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
    CKFinder.setupCKEditor(editor, '../');
</script>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#admin_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    vQuestion: {
                        required: true
                    },

                    vEmail: {
                        email: true,
                        required: true
                    },

                    eStatus: {
                        required: true
                    },
                    vAnswer: {
                        required: function () {
                            CKEDITOR.instances.vAnswer.updateElement();
                        },

                        minlength: 1
                    }

                },
                messages: {
                    vQuestion: "Please Enter Question",
                    vAnswer: "Plese Enter Answer",
                    vEmail: {
                        required: "Please Enter Email",
                        email: "Please Enter valid Email Address "
                    },

                    eStatus: "Please Select Status"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#admin_form').submit();
        });
    });
</script>
