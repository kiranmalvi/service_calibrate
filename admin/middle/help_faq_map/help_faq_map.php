<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 6/4/15
 */

include_once($inc_class_path . 'help_faq.class.php');
$helpfaqObj = new help_faq();

$db_res = $helpfaqObj->select();

//echo "<pre>"; print_r($db_res); exit;

$MODULE = 'Help/FAQs/Guide';
$ACTIVE_LINK = 'index.php?file=hfq-help_faq_map_a&mode=active';
$ADD_LINK = 'index.php?file=hfq-help_faq_mapadd';
$ACT_LINK = 'index.php?file=hfq-help_faq_map_a';
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];

?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php"> Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="javascript:;"> <?php echo $MODULE; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <!-- Table Section Header Starts -->
                        <header class="panel-heading">
                            <?php echo $MODULE; ?> Listing

                            <? if ($atype != "R") { ?>
                                <span class=" pull-right header-btn">


                             <button class="btn btn-success1" onclick="delete_record()">
                                 DELETE
                             </button>
                             <a class="btn btn-success1" href="<?php echo $ADD_LINK; ?>">
                                 Add New <?php echo $MODULE; ?>
                             </a>


                        </span>
                            <? } ?>
                        </header>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <div class="admin-tbl-main">
                                    <table class="display table table-bordered table-striped" id="feedback_table">
                                        <!-- Changes HERE Start -->
                                        <thead>
                                        <tr>
                                            <? if ($atype != "R") { ?>
                                                <td><input type="checkbox" onclick="check_all();"></td>
                                                <td>Action</td>
                                            <? } ?>

                                            <!--                                            <td>Updated</td>-->
                                            <td>Question</td>
                                            <td>Answer</td>
                                            <td>Link</td>
                                            <td>Date Added</td>
                                            <td>Status</td>
                                            <!-- <td>Status</td>-->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($db_res as $help) {

                                            ?>
                                            <tr>
                                                <? if ($atype != "R") { ?>
                                                    <td>
                                                        <input type="checkbox" name="delete[]"
                                                               value="<?php echo $help['iHelpId']; ?>">

                                                    </td>

                                                    <td>

                                                    <a class="btn btn-danger btn-xs edit-delete-btn"
                                                       href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $help['iHelpId']; ?>&delete_type=single_delete"><i
                                                            class="fa fa-edit"></i> Delete</a>
                                                    <a class="btn btn-success btn-xs edit-delete-btn"
                                                       href="<?php echo $ADD_LINK; ?>&mode=edit&iId=<?php echo $help['iHelpId']; ?>"><i
                                                            class="fa fa-edit"></i> Edit</a>

                                                    </td><? } ?>

                                                <!--                                                <td>-->
                                                <?php //echo date('m-d-Y H:i:s', $help['idtUpdated']); ?><!--</td>-->
                                                <td><?php echo $help['vQuestion'] ?></td>
                                                <td><?php echo $help['vAnswer'] ?></td>

                                                <td><?php echo $help['vLink']; ?></td>
                                                <td><?php echo date('m-d-Y H:i:s', $help['idtAdded']); ?></td>
                                                <td>
                                                    <?php
                                                    if ($help['eStatus'] == '1') {
                                                        echo '<span class="label label-success edit-active">Active</span>';
                                                    } elseif($help['eStatus'] == '0') {
                                                        echo '<span class="label label-danger edit-active">Inactive</span>';
                                                    }
                                                    else
                                                    {
                                                        echo '<span class="label label-danger edit-active">Delete</span>';
                                                    }
                                                    ?>
                                                </td>
                                                <!-- <td>
                                                    <?php
                                                /*                                                    if ($FEEDBACK['eStatus'] == '1') {
                                                                                                        echo '<span class="label label-success">Active</span>';
                                                                                                    }
                                                                                                    else if ($FEEDBACK['eStatus'] == '0') {
                                                                                                        echo '<span class="label label-danger">Inactive</span>';
                                                                                                    }
                                                                                                    else {
                                                                                                        echo '<span class="label label-warning">Pending</span>';
                                                                                                    }
                                                                                                    */
                                                ?>
                                                </td>-->
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                        <!-- Changes HERE Ends -->
                                    </table>
												</div>                                
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>


<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#feedback_table').dataTable({
  	"aaSorting": [[ 3, "desc" ]]
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>
<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#feedback_table').dataTable({
            	  	"aaSorting": [[ 5, "desc" ]],
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });
    </script>

<?php
}
include_once($admin_path . 'js_datatable.php');
?>