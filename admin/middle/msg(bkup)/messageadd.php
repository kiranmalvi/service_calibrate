<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:51 PM
 */
include_once($inc_class_path . 'orders.class.php');
$ordersObj = new orders();


include_once($inc_class_path . 'user.class.php');
$userObj = new user();

$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';

if ($iId != '') {
    include_once($inc_class_path . 'schedule.class.php');
    $scheduleObj = new schedule();
    $db_res=$scheduleObj->select($iId);

}


$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$Usertype = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
//echo $Usertype;
$MODE_TYPE = ucfirst($mode);
$MODULE = 'Conversation';
$PRE_LINK = 'index.php?file=m-messages';
$ACT_LINK = 'index.php?file=m-messages_a';

if($mode=="add")
{
    $display='style="display: none"';
    $display1='style="display: none"';
}
else
{
    $display="";
    if($db_res[0]['eSchedule']=="2 days a week")
    {
        $display1="";
    }
}
?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>

                    <li>
                        <a class="current" href="javascript:;"> <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Form Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Form Section Header Starts -->
                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail
                        </h4>
                    </header>
                    <!-- Form Section Header Ends -->

                    <!-- Form Section Body Starts -->
                    <div class="panel-body">

                        <form class="form-horizontal bucket-form" method="post" name="admin_form" id="schedule_form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <input type="hidden" name="mode" value="<?php echo $mode; ?>">
                            <input type="hidden" name="iScheduleId" id="iScheduleId"
                                   value="<?php echo $db_res[0]['iScheduleId']; ?>">
                            <input type="hidden" name="retailer" id="retailer"
                                   value="<?php echo $iUserId; ?>">

                            <div class="form-group">
                                <?php
                                if($Usertype=="4"||$Usertype=="5"||$Usertype=="6"){
                                ?>
                                <label class="col-md-2 control-label"><span class="red"> *</span> Select Vendor</label>
                                <div class="col-md-8">
                                    <?php $vendor=$ordersObj->select_retailer1($iUserId);
                                    ?>
                                    <select class="form-control" name="vendor" id="vendor" onchange="get_emp(this.value)">

                                        <option selected disabled>- Select Vendor -</option>
                                        <?php for($i=0;$i<count($vendor);$i++)
                                        {?>
                                            <option
                                                value="<?php echo $vendor[$i]['iUserId']?>" <? echo ($db_res[0]['iDistributorId']==$vendor[$i]['iUserId']) ? 'Selected' :""?>>
                                                <?php echo $vendor[$i]['vAccountName'];?>
                                            </option>

                                        <?}?>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Select Employee</label>
                                <div class="col-md-8">

                                    <select class="form-control" name="iEmployeeId" id="iEmployeeId">

                                        <option selected disabled>- Select Employee -</option>

                                    </select>
                                </div>
                            </div>
                            <?}else{

                                $vendor_empl=$ordersObj->select_dist_man1($iUserId);
                                // pr($vendor_empl);

                                ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Select Retailer</label>
                                    <div class="col-md-8">

                                        <select class="form-control" name="iEmployeeId" id="iEmployeeId">

                                            <option selected disabled>- Select Retailer -</option>
                                            <?php
                                            for($t=0;$t<count($vendor_empl);$t++)
                                            {
                                                ?>
                                                <option value="<?php echo $vendor_empl[$t]['iUserId'];?>"><?php echo $vendor_empl[$t]['vFirstName']." ".$vendor_empl[$t]['vLastName'];?> </option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                            <?}?>




                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit"
                                            class="btn btn-success1"><?php if ($mode == "add") {
                                            echo "add";

                                        } ?></button>
                                    <a href="<?php echo $PRE_LINK; ?>" class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- Form Section Body Starts -->

                </section>
            </div>
        </div>
        <!-- Form Section Ends -->

        <!-- page end-->
    </section>
</section>

<?
include_once($admin_path . 'js_form.php');
?>

<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>

    function get_emp(user_id) {
        var selected = '<?php echo (isset($db_res[0]['iEmployeeId'])) ? $db_res[0]['iEmployeeId'] : ""; ?>';
        $.ajax({
            url: 'index.php?file=m-messages_a',
            type: 'POST',
            data: {"mode": 'get_emp', "distributor_id": user_id, "selected": selected},
            success: function (result) {
                console.log('sdf : ', result);
                $('#iEmployeeId').html(result);
            }
        });
    }
</script>

<script>
    function get_day(days)
    {
        if(days=="2 days a week")
        {
            daydiv.style.display = "";
            day2div.style.display = "";
        }
        else
        {
            daydiv.style.display = "";
            day2div.style.display = "none";
        }
    }
</script>
<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#schedule_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    vendor: {
                        required: true
                    },
                    iEmployeeId: {
                        required: true
                    }

                },
                messages: {
                    vendor: "Please Select Vendor",
                    iEmployeeId: "Please Select Employee"

                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        <?if($mode=="edit"){?>
        get_emp('<?php echo $db_res[0]['iEmployeeId']; ?>');
        <?}?>
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#schedule_form').submit();
        });
    });
</script>