<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 1/4/15
 * Time: 11:26 AM
 */
include_once($inc_class_path . 'coupon.class.php');

$coupon = new coupon();
$db_res = $coupon->select();


/*$DATA = array('name' => 'chintan');
echo $generalfuncobj->get_SC_SEND_mail('forgot_password', $DATA, 'chintan.mind@gmail.com', 'test');

$DATA = array('name' => 'chintan');
echo $generalfuncobj->get_SC_SEND_mail('register', $DATA, 'chintan.mind@gmail.com', 'test');*/
/*
$DATA = array('name' => 'chintan');
echo $generalfuncobj->get_SC_SEND_mail('feedback', $DATA, 'chintan.mind@gmail.com', 'test');
exit;*/

$MODULE = 'Coupon';
$ADD_LINK = 'index.php?file=cou-couponadd';
$ACT_LINK = 'index.php?file=cou-coupon_a';

$Breadcrumbs = array(
    'index.php' => 'Dashboard',
    '#' => $MODULE
);
?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <?php
            echo $generalfuncobj->Breadcrumbs($Breadcrumbs);
            ?>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <!-- Table Section Header Starts -->
                        <?php echo $generalfuncobj->TBL_header($MODULE, $ADD_LINK); ?>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <table class="display table table-bordered table-striped" id="dynamic-table">
                                        <!-- Changes HERE Start -->
                                        <thead>
                                        <tr>
                                            <td><input type="checkbox" onclick="check_all();"></td>
                                            <td>Action</td>
                                            <td>Coupon</td>
                                            <td>Description</td>
                                            <td>Expire Date</td>
                                            <td>Status</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($db_res as $COUPON) {

                                            ?>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="delete[]"
                                                           value="<?php echo $COUPON['iCouponId']; ?>">
                                                </td>
                                                <td>
                                                    <a class="btn btn-success btn-xs"
                                                       href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $COUPON['iCouponId']; ?>">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>

                                                    <a class="btn btn-danger btn-xs"
                                                       href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $COUPON['iCouponId']; ?>&delete_type=single_delete">
                                                        <i class="fa fa-edit"></i> Delete
                                                    </a>
                                                </td>
                                                <td><?php echo $COUPON['vCouponCode']; ?></td>
                                                <td><?php echo $COUPON['tCouponDescription']; ?></td>
                                                <td>
                                                    <?php echo date('Y-m-d H:i:s', $COUPON['iExpireDate']); ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($COUPON['eStatus'] == '1') {
                                                        echo '<span class="label label-success">Active</span>';
                                                    } else {
                                                        echo '<span class="label label-danger">Inactive</span>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                        <!-- Changes HERE Ends -->
                                    </table>
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>

<?php
include_once($admin_path . 'js_datatable.php');
?>