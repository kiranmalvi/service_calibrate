<?
//$utype=$_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
//$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$report_file_name1 = 'contact_us';
unset($_SESSION[$report_file_name1]);
//echo $utype;
$MODULE="Contact Us";
$sql = "select * from contact_us where eStatus!='2'ORDER BY iDtUpdated DESC ";
$select_retailer = $obj->select($sql);
$ACT_LINK="index.php?file=cu-contactus_a";
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];
// pr($select_retailer);
?>

    <section id="main-content">

        <section class="wrapper">
            <!-- page start-->

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php"><i class=""></i> Dashboard</a>
                        </li>

                        <li>
                            <a class="current" href="javascript:;"><i class=""></i> Contact Us</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <header class="panel-heading">
                            <?php echo $MODULE; ?> Listing
                        <span class=" pull-right header-btn">

                             <? if ($atype != "R") { ?>

                                 <button class="admin-btn btn btn-success1" onclick="delete_record()">
                                     DELETE
                                 </button>
                             <? } ?>
                        </span>
                        </header>
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <div class="table-main-scroll">
                                        <table class="display table table-bordered table-striped" id="contact_table">
                                            <thead>
                                            <tr>

                                                <th style="display: none"></th>

                                                <? if ($atype != "R") { ?>
                                                    <th style="width:10px"><input type="checkbox" onclick="check_all();"></td>
                                                    <th>Action</td>
                                                <?}?>
                                                <th>First Name</th>

                                                <th>Last Name</th>

                                                <th>Email</th>

                                                <th>Contact Number</th>
                                                <th>Message</th>
                                                <th>Date Added</th>
                                                 <th style="display: none"></th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            for($i=0;$i<count($select_retailer);$i++)
                                            {
                                                ?>

                                                <tr class="gradeX">
                                                    <td style="display: none"><?php echo $i ?></td>

                                                    <? if ($atype != "R") { ?>
                                                        <td style="width:10px">
                                                            <input type="checkbox" name="delete[]"
                                                                   value="<?php echo $select_retailer[$i]['iContactusId']; ?>">
                                                        </td>
                                                        <td> <a class="btn btn-danger btn-xs"
                                                                href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $select_retailer[$i]['iContactusId']; ?>&delete_type=single_delete">
                                                                <i class="fa fa-edit"></i> Delete
                                                            </a>

                                                            <a class="btn btn-info btn-xs"  onclick="get_popup_value('<?php echo $select_retailer[$i]['iContactusId']; ?>');"
                                                               href="#myModal"
                                                               data-toggle="modal">View

                                                            </a>
                                                        </td>
                                                    <?}?><td>

                                                        <?php echo $select_retailer[$i]['vFirstName'];?>
                                                    </td>

                                                    <td>
                                                        <?php echo $select_retailer[$i]['vLastName'];?>
                                                    </td>

                                                    <td>
                                                        <a href="mailto:<?php echo $select_retailer[$i]['vEmail']; ?>" style="text-decoration: underline"><?php echo $select_retailer[$i]['vEmail']; ?></a>
                                                    </td>
                                                    <td><?php echo $select_retailer[$i]['vPhone'];?>
                                                    </td>

                                                    <td>
                                                    
  <?php
                                                    $slen = strlen($select_retailer[$i]['vMessage']);
                                                    $string2 ='';
                                                    if($slen > 25 )
                                                    {
                                                        $string2 .= substr($select_retailer[$i]['vMessage'], 0, 25);
                                                        $string2 .= "...";
                                                    }
                                                    else {
                                                        $string2 = $select_retailer[$i]['vMessage'];
                                                    }
                                                    ?>                                                    
                                                    <?php echo $string2;?>
                                                    </td>
                                                    <td><?php echo date('m-d-Y', $select_retailer[$i]['iDtAdded']);?>

                                                    </td>

                                                    <!--<div id="div_<?php /*echo $select_retailer[$i]['iContactusId']; */?>" style="display: none">
                                                        <div class="view-prmotn-mi">


                                                            <p class="control-label dashboard-address_font">Message From:-
                                                                <span><?php /*echo $select_retailer[$i]['vFirstName']." ".$select_retailer[$i]['vLastName']; */?></span></p>


                                                            <p class="control-label dashboard-address_font">Email:-/s
                                                                <span><?php /*echo $select_retailer[$i]['vEmail']; */?></span></p>

                                                            <p class="control-label strt-end-dt">Contact Number:-
                                                                <span><?php /*echo $select_retailer[$i]['vPhone']; */?></span></p>

                                                            <p class="control-label strt-end-dt">Message:-
                                                                <span><?php /*echo $select_retailer[$i]['vMessage']; */?></span></p>
                                                            <p class="control-label strt-end-dt">Date:-
                                                                <span><?php /*echo date('m-d-Y',$select_retailer[$i]['iDtAdded']); */?></span></p>


                                                        </div>
                                                    </div>-->
                                                    <td id="div_<?php echo $select_retailer[$i]['iContactusId']; ?>" style="display: none">
                                                        <table class="display table table-bordered table-striped" width="60%">
                                                            <tr>
                                                                <td width="10%">Message From:</td>
                                                                <td width="20%"><?php echo $select_retailer[$i]['vFirstName']." ".$select_retailer[$i]['vLastName'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Message:</td>
                                                                <td><?php echo $select_retailer[$i]['vMessage'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Contact:</td>
                                                                <td><?php echo $select_retailer[$i]['vPhone'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Email:</td>
                                                                <td><?php echo $select_retailer[$i]['vEmail'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Date:</td>
                                                                <td><?php echo date('m-d-Y',$select_retailer[$i]['iDtAdded']);?></td>
                                                            </tr>

                                                        </table>
                                                    </td>


                                                </tr>
                                            <?php }?>
                                            </tbody>

                                        </table>

                                    </div>
                                </form>

                                <!--<div class="text-right">
                                <button id="downloadexcelBlutton" name="downloadexcelBlutton" type="submit"
                                        class="download-excel-btn btn btn-success1"
                                        onclick="location.href='index.php?file=sf-download_a&file_name=<?php /*echo $report_file_name1; */?>'">
                                    Download Excel
                                </button>
                            </div>-->
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
    </section>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog-brodcast">
            <div class="modal-content">
                <div class="modal-header red-light-bg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Contact Us</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="frmadd_view_promotion" name="frmadd">


                            </form>
                            </br>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#contact_table').dataTable({
            	  	"aaSorting": [[ 6, "desc" ]],
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>

<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#contact_table').dataTable({
            	  	"aaSorting": [[ 8, "desc" ]],
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });

        function get_popup_value(id) {
            $('#frmadd_view_promotion').html($('#div_' + id).html());
            console.log($('#div_' + id).html());
        }
    </script>
<?php
}
include_once($admin_path . 'js_datatable.php');
?>