<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 31/3/15
 * Time: 12:53 PM
 */

include_once($inc_class_path . 'how_did_hear.class.php');

$how_did_hear = new how_did_hear();
$db_res = $how_did_hear->select();

$MODULE = 'How Did you Hear About us';
$ADD_LINK = 'index.php?file=cat-how_hear_add';
$ACT_LINK = 'index.php?file=cat-how_hear_a';
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];
?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php">Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="#"><?php echo $MODULE; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <!-- Table Section Header Starts -->
                        <header class="panel-heading">
                            <?php echo $MODULE; ?> Listing
                            <? if ($atype != "R") { ?>

                        <span class=" pull-right header-btn">
                            <a class="btn btn-success1" href="<?php echo $ADD_LINK; ?>">
                                Add New <?php echo $MODULE; ?>
                            </a>
                           <!-- <button class="btn btn-danger" onclick="delete_record()">
                                DELETE
                            </button>-->
                        </span>
                            <?php } ?>
                        </header>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <div class="admin-tbl-main">
                                    <table class="display table table-bordered table-striped" id="howabout-table">
                                        <!-- Changes HERE Start -->
                                        <thead>
                                        <tr>
                                            <!--<td><input type="checkbox" onclick="check_all();"></td>-->
                                            <? if ($atype != "R") { ?>
                                            <td width="10%">Action</td>
                                            <?php } ?>
                                            <td>Title</td>
                                            <td>Date Added</td>
                                            <td>Status</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($db_res as $HDYHAU) {

                                            ?>
                                            <tr>
                                                <!-- <td>
                                                    <input type="checkbox" name="delete[]"
                                                           value="<?php /*echo $HDYHAU['iHDHId']; */ ?>">
                                                </td>-->
                                                <? if ($atype != "R") { ?>
                                                <td>

                                                    <? if ($HDYHAU['iHDHId'] != "1") { ?>
                                                   <input type="hidden" value="<?php echo $HDYHAU['iHDHId']; ?>">
                                                    <a class="btn btn-success btn-xs edit-delete-btn"
                                                       href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $HDYHAU['iHDHId']; ?>">
                                                        <i class="fa fa-edit"></i> Edit
                                                        </a><? } ?>
                                                    <!-- <a class="btn btn-danger btn-xs"
                                                       href="<?php /*echo $ACT_LINK; */ ?>&mode=delete&iId=<?php /*echo $HDYHAU['iHDHId']; */ ?>&delete_type=single_delete">
                                                        <i class="fa fa-edit"></i> Delete
                                                    </a>-->
                                                </td>
                                             <?php } ?>
                                                <td><?php echo $HDYHAU['vName']; ?></td>
                                                <td>
                                                    <?php echo gmdate('m-d-Y H:i:s', $HDYHAU['iDtAdded']); ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($HDYHAU['eStatus'] == '1') {
                                                        echo '<span class="label label-success edit-active">Active</span>';
                                                    } else {
                                                        echo '<span class="label label-danger edit-active">Inactive</span>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                        <!-- Changes HERE Ends -->
                                    </table>
												</div>                                
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>

<?php
include_once($admin_path . 'js_datatable.php');
?>




<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#howabout-table').dataTable({
            	"aaSorting": [[ 1, "desc" ]]
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>

<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#howabout-table').dataTable({
            	"aaSorting": [[ 3, "desc" ]],
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });
    </script>
<?php
}?>