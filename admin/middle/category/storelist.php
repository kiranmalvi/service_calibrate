<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 1/4/15
 * Time: 11:07 AM
 */

include_once($inc_class_path . 'user_type_store.class.php');

$user_type_store = new user_type_store();
$db_res = $user_type_store->select();

$MODULE = 'Account';
$ADD_LINK = 'index.php?file=cat-storeadd';
$ACT_LINK = 'index.php?file=cat-store_a';

$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];
?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php">Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="#"><?php echo $MODULE; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <!-- Table Section Header Starts -->
                        <header class="panel-heading">
                            <?php echo $MODULE; ?> Listing
            <? if ($atype != "R") { ?>
                        <span class=" pull-right header-btn">
                            <a class="btn btn-success1" href="<?php echo $ADD_LINK; ?>">
                                Add New <?php echo $MODULE; ?>
                            </a>
                            <button class="btn btn-success1" onclick="delete_record()">
                                DELETE
                            </button>
                        </span>
                    <? } ?>
                        </header>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <div class="admin-tbl-main">
                                    <table class="display table table-bordered table-striped" id="store_table">
                                        <!-- Changes HERE Start -->
                                        <thead>
                                        <tr>
<? if ($atype != "R") { ?>                       <td><input type="checkbox" onclick="check_all();"></td>
                                            <td>Action</td>
         <?php } ?>                                   <td>Account</td>
                                            <td>Date Added</td>
                                            <td>Status</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($db_res as $STORE) {

                                            ?>
                                            <tr>
                                            <? if ($atype != "R") { ?>
                                                <td>
                                                    <input type="checkbox" name="delete[]"
                                                           value="<?php echo $STORE['iUserTypeStoreId']; ?>">
                                              <input type="hidden" value="<?php echo $STORE['iUserTypeStoreId']; ?>">
                                                </td>
                                                <td>
                                                    <a class="btn btn-success btn-xs edit-delete-btn"
                                                       href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $STORE['iUserTypeStoreId']; ?>">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>

                                                    <a class="btn btn-danger btn-xs edit-delete-btn"
                                                       href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $STORE['iUserTypeStoreId']; ?>&delete_type=single_delete">
                                                        <i class="fa fa-edit"></i> Delete
                                                    </a>
                                                </td>
                                                <?php } ?>
                                                <td><?php echo $STORE['vName']; ?></td>
                                                <td>
                                                    <?php echo gmdate('m-d-Y H:i:s', $STORE['iDtAdded']); ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($STORE['eStatus'] == '1') {
                                                        echo '<span class="label label-success edit-active">Active</span>';
                                                    } else {
                                                        echo '<span class="label label-danger edit-active">Inactive</span>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                        <!-- Changes HERE Ends -->
                                    </table>
												</div>                                
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>

<?php
include_once($admin_path . 'js_datatable.php');
?>


<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#store_table').dataTable({
            	"aaSorting": [[ 1, "desc" ]]
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>

<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#store_table').dataTable({
            	"aaSorting": [[ 3, "desc" ]],
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });
    </script>
<?php
}?>