<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:29 PM
 */

include_once($inc_class_path . 'promotional_code.class.php');

$promotional_code = new promotional_code();
$db_res = $promotional_code->selectall();

$MODULE = 'Promotion Code';
$ADD_LINK = 'index.php?file=pc-promocodeadd';
$ACT_LINK = 'index.php?file=pc-promotionalcode_a';
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];

?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php"> Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="javascript:;"><?php echo $MODULE; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <!-- Table Section Header Starts -->
                        <header class="panel-heading">
                            <?php echo $MODULE; ?> Listing
                        <span class=" pull-right header-btn">

                             <? if ($atype != "R") { ?>
                                 <a class="admin-btn btn btn-success1" href="<?php echo $ADD_LINK; ?>">
                                     Add New <?php echo $MODULE; ?>
                                 </a>


                                 <button class="admin-btn btn btn-success1" onclick="delete_record()">
                                     DELETE
                                 </button>
                             <? } ?>
                        </span>
                        </header>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <div class="admin-tbl-main">
                                        <table class="display table table-bordered table-striped" id="admin_table">
                                            <!-- Changes HERE Start -->
                                            <thead>
                                            <tr>
                                                <td style="display: none"></td>
                                                <? if ($atype != "R") { ?>
                                                    <td style="width:10px"><input type="checkbox" onclick="check_all();"></td>
                                                    <td>Action</td> <? } ?>
                                                <td>Promotion Name</td>
                                                <td>Promotion Code</td>
                                                <td>% Less</td>
                                                <td>Date Added</td>
                                                <td>Expired Date</td>
                                                <td>Status</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach ($db_res as $key=>$PROMOCODE) {

                                                ?>
                                                <tr>
                                                    <td style="display: none">
                                                        <?php echo $key;?>
                                                    </td>
                                                    <? if ($atype != "R") { ?>
                                                        <td style="width:10px">
                                                            <input type="checkbox" name="delete[]"
                                                                       value="<?php echo $PROMOCODE['iPromotionalcodeId']; ?>">

                                                        </td>
                                                        <td>
                                                            <a class="btn btn-success btn-xs edit-delete-btn" href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $PROMOCODE['iPromotionalcodeId']; ?>"><i
                                                                    class="fa fa-edit"></i> Edit</a>


                                                                <a class="btn btn-danger btn-xs edit-delete-btn" href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $PROMOCODE['iPromotionalcodeId']; ?>&delete_type=single_delete"><i
                                                                        class="fa fa-edit"></i> Delete</a>

                                                        </td>
                                                    <? } ?>
                                                    <td><?php echo $PROMOCODE['vPromotinalName'] . ' ' . $ADMIN['vLastName']; ?></td>
                                                    <td><?php echo $PROMOCODE['vPromotionalCode']; ?></td>
                                                    <td><?php echo $PROMOCODE['vPromoPrice']; ?></td>
                                                    <td>
                                                        <?php echo date('m-d-Y',$PROMOCODE['iDtUpdated']); ?>

                                                    </td>
                                                    <td>
                                                        <?php echo date('m-d-Y',$PROMOCODE['iExpireDate']); ?>

                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($PROMOCODE['eStatus'] == '1') {
                                                            echo '<span class="label label-success edit-active">Active</span>';
                                                        } else {
                                                            echo '<span class="label label-danger edit-active">Inactive</span>';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                            </tbody>
                                            <!-- Changes HERE Ends -->
                                        </table>

                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>

<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#admin_table').dataTable({
            	  	"aaSorting": [[4, "desc" ]]
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>

<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#admin_table').dataTable({
            	  	"aaSorting": [[ 6, "desc" ]],
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });
    </script>
<?php
}

include_once($admin_path . 'js_datatable.php');
?>