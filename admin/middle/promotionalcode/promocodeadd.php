<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:51 PM
 */

$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';
if ($iId != '') {
    include_once($inc_class_path . 'promotional_code.class.php');

    $promotional_code = new promotional_code();
    $db_res = $promotional_code->select($iId);
}



$MODE_TYPE = ucfirst($mode);
$MODULE = 'Promotion Code';
$PRE_LINK = 'index.php?file=pc-promotionalcodelist';
$ACT_LINK = 'index.php?file=pc-promotionalcode_a';

?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <li>
                        <a href="<?php echo $PRE_LINK; ?>"> <?php echo $MODULE; ?></a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;"> <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Form Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Form Section Header Starts -->
                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail
                        </h4>
                    </header>
                    <!-- Form Section Header Ends -->

                    <!-- Form Section Body Starts -->
                    <div class="panel-body">

                        <form class="form-horizontal bucket-form" method="post" name="admin_form" id="admin_form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <input type="hidden" name="mode" value="<?php echo $mode; ?>">
                            <input type="hidden" name="iId" id="iId"
                                   value="<?php echo $db_res[0]['iPromotionalcodeId']; ?>">

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Promotion Name :</label>
                                <div class="col-md-8">
                                    <input type="text" name="vPromoname" id="vPromoname" class="form-control"
                                           value="<?php echo $db_res[0]['vPromotinalName']; ?>" placeholder=" Promotion Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Promotion Code  :</label>

                                <div class="col-md-8">
                                    <input type="text" name="vPromocode" id="vPromocode" class="form-control"
                                           value="<?php echo $db_res[0]['vPromotionalCode']; ?>" placeholder="Promotion Code">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> % Less :</label>

                                <div class="col-md-8">
                                    <input type="text" name="vPromoPrice" id="vPromoPrice" class="form-control"
                                           value="<?php echo $db_res[0]['vPromoPrice']; ?>" placeholder="% Less">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" id="lblend"><span class="red"> *</span> Expire Date :</label>


                                <div class="col-md-8">
                                    <input class="form-control form-control-inline input-medium default-date-picker"
                                           type="text" name="iEndDate" id="iEndDate"                                           value="<?php echo (isset($db_res[0]['iExpireDate'])) ? date('m-d-Y', $db_res[0]['iExpireDate']) : ''; ?>"/>

                                    <div id="enderr">

                                    </div>
                                </div>



                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Status :</label>

                                <div class="col-md-8">
                                    <select class="form-control" name="eStatus" id="eStatus">
                                        <option selected disabled>- Select Status -</option>
                                        <option
                                            value="1" <?php echo($db_res[0]['eStatus'] == '1' ? 'selected' : ''); ?>>
                                            Active
                                        </option>
                                        <option
                                            value="0" <?php echo($db_res[0]['eStatus'] == '0' ? 'selected' : ''); ?>>
                                            Inactive
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit"
                                            class="btn btn-success1"><?php if ($mode == "add") {
                                            echo "Add";
                                        } else {
                                            echo "Update";
                                        } ?></button>
                                    <a href="<?php echo $PRE_LINK; ?>" class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- Form Section Body Starts -->

                </section>
            </div>
        </div>
        <!-- Form Section Ends -->

        <!-- page end-->
    </section>
</section>

<?
include_once($admin_path . 'js_form.php');
?>

<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#admin_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    vPromoname: {
                        required: true
                    },
                    vPromocode: {
                        required: true
                    },
                    vPromoPrice: {
                        required: true,
                        number:true
                    },
                    eStatus: {
                        required: true
                    },
                    iEndDate:{
                        required: true
                    }

                },
                messages: {
                    vPromoname: "Please Enter Promotion Name",
                    vPromocode: "Plese Enter Promotion Code",
                    vPromoPrice: {
                        required: "Please Enter How Many % Less",
                        number: "Enter Only Digits"
                    },
                    eStatus: "Please Select Status",
                    iEndDate:{
                        required: "Please Select Expire Date"
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#admin_form').submit();
        });
    });
</script>
<script>
    $(document).ready(function () {

        var dateToday = new Date();

        var someDate = new Date();
        var numberOfDaysToAdd = -1;
        someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
        var dd = someDate.getDate();
        var mm = someDate.getMonth() + 1;
        var y = someDate.getFullYear();
        var someFormattedDate = dd + '/' + mm + '/' + y;

        $('#iEndDate').datepicker({

            format: 'mm dd, yyyy',
            startDate: 0,
            endDate: someFormattedDate,
            autoclose: true
        });

        $('#iEndDate').on('changeDate', function(){
            $(this).datepicker('hide');
        });

    });


</script>