<? include_once($inc_class_path . 'rating.class.php');
$ratingObj = new  rating();

include_once($inc_class_path . 'info.class.php');
$info = new info();


if(isset($_REQUEST['id'])&&$_REQUEST['id']!="")
{
    $sid=$_REQUEST['id'];

}
else
{
    $sid="0";
}


$info_icon = $info->select_report_info("rating_web");

$userid=$_SESSION['SC_LOGIN']['USER']['iUserId'];
$usertype=$_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
//$parentId =$_SESSION['SC_LOGIN']['USER']['iParentId'];
$ratingdt = $_REQUEST['rating'];
$rate = $ratingObj->get_rating($userid, $usertype, $ratingdt,$sid);
//pr($rate);

if($usertype == 2 || $usertype == 3)
{
    $sql = "SELECT u.iUserId,u.vStoreUniqueId as storeiniqid,u.vStoreName as accounth,us.vStoreUniqueId,ud.vStoreUniqueId as vendor,u.vFirstName as fname,us.vFirstName,u.vLastName as lname,us.vLastName,r.* FROM rating as r left join user u on r.iUserId=u.iUserId left join user ud on r.iDistributorId=ud.iUserId left join user us on r.iEmployeeId=us.iUserId where r.iEmployeeId in(select iUserId from user where iUserId='$userid' or iParentId='$userid') ORDER by r.iRatingId desc";
    $res_db =  $obj->select($sql);
    $db_res=$ratingObj->get_rated_retailer($userid);

}
else
{
    //  $sql =  "select u.vStoreUniqueId as storeiniqid,us.vStoreUniqueId,u.vFirstName as fname,us.vFirstName,u.vLastName as lname,us.vLastName,r.* from rating  as r left join user u on r.iUserId=u.iUserId left join user us on r.iEmployeeId=us.iUserId  where r.iUserId in (select iUSerId from user where iUserId='$userid' or iParentId='$userid')";

     $sql = "select u.vStoreUniqueId as storeiniqid,u.vStoreName as accounth,us.vStoreUniqueId,ud.vStoreUniqueId as vendor,u.vFirstName as fname,us.vFirstName,u.vLastName as lname,us.vLastName,r.* from rating as r left join user u on r.iUserId=u.iUserId left join user us on r.iEmployeeId=us.iUserId left join user ud on r.iDistributorId=ud.iUserId where r.iUserId in (select iUserId from user where iUserId='$userid' or iParentId='$userid') ORDER by r.iRatingId desc";
    $res_db =  $obj->select($sql);

    $db_res=$ratingObj->get_rated_distributor($userid);
}
//exit;




//$sql =  "SELECT u.iUserId,u.vStoreUniqueId,u.vFirstName,u.vLastName, r.* FROM `rating` as r left join user u on u.iUserId=r.iRatingId where r.iDistributorId = $userid";
/*$sql =  "SELECT u.iUserId,u.vStoreUniqueId,u.vFirstName,u.vLastName,us.vFirstName as fname,us.vLastName as lname,us.vStoreName as rstore, r.* FROM `rating` as r left join user u on u.iUserId=r.iEmployeeId left join user us on us.iUserId=r.iUserId where r.iEmployeeId in(select iUserId from user where iUserId='$userid' or iParentId='$userid')";
$res_db =  $obj->select($sql);*/
//print_r($res_db);
$MODULE = 'Rating';

/*echo "<pre>";
print_r($rate);
exit;*/
?>
<section id="main-content">
<section class="wrapper">

<!-- Breadcrumbs Starts -->
<div class="row">
    <div class="col-md-9">
        <ul class="breadcrumbs-alt">
            <li>
                <a href="index.php"> Dashboard</a>
            </li>
            <li>
                <a class="current" href="javascript:;">Rating</a>
            </li>
        </ul>
    </div>
    <div class="col-md-3 pull-right text-right">
        <div class="form-group">
            <a href="#myModal2" name="SearchReport" data-toggle="modal" class="btn btn-success1"
               id="help_show" style="display: none">Help (?)</a>
        </div>
    </div>

</div>


<style>
    html, body, #map-canvas {
        width: 100%;
        height: 480px;
        margin: 0px;
        padding: 0px
    }
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<script>
    function initialize() {
        var mapOptions = {
            zoom: 20,
            //center: new google.maps.LatLng(-33.9, 151.2)
            <? if(count($rate)>0){
            $lat = $rate[0]['vLatitude'];
            $long = $rate[0]['vLongitude'];
             }
             else
             {
                $lat = "33.849182";
                $long = "-118.388408";
            }?>
            center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>, 5),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        //----------------------------------------

        var infowindow = new google.maps.InfoWindow();
        //  ----------------------------------
        setMarkers(map, beaches);
    }
    <?php //echo "<pre>";print_r($rate);echo "</pre>";
    if(count($rate)>0){
  ?>
    var beaches = [
        <?php foreach($rate as $key=>$v){?>
        ['<?php echo "Account Name :".$v['vStoreUniqueId']." "."#".$v['vStoreName']."<br>" ."Address :".$v['vAddress'] ."<br>".  "Employee Name :".$v['fVender']." " .$v['lVender']." <br>"."Vendor Name :".$v['dvndr'] ?>', <?php echo $v['vLatitude'];?>, <?php echo $v['vLongitude'];?>, <?php echo $v['eRating'];?>],
        <?php }?>
    ];
    <?php }else{?>
    var beaches =
        [['1', <?php echo $lat; ?>, <?php echo $long; ?>, 5]];

    <?}?>

    function setMarkers(map, locations) {

        var image = {
            url: '<?php echo $rate_image_url?>rsz_1rsz_green.png',
            size: new google.maps.Size(150, 150),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 150)
        };

        var image1 = {
            url: '<?php echo $rate_image_url?>rsz_rsz_red1.png',
            size: new google.maps.Size(150, 150),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 150)
        };
        var image2 = {

            size: new google.maps.Size(150, 150),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 150)
        };
        var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
        };


        var infowindow = new google.maps.InfoWindow({});


        for (var i = 0; i < locations.length; i++) {
            var beach = locations[i];

            var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
            //alert(myLatLng);
            var newimage;
            if (beach[3] == 0) {

                newimage = image1;
            }
            else if (beach[3] == 1) {

                newimage = image;
            }
            else {
                newimage = image2;
            }
            <?php   if(count($rate)>0) {  ?>

            var marker = new google.maps.Marker({

                position: myLatLng,
                map: map,
                icon: newimage,
                shape: shape,
                title: beach[0],
                zIndex: beach[3]


            });
            console.log(marker);
            bindInfoWindow(marker, map, infowindow, beach[0]);
            <?php } else { ?>

            var marker = new google.maps.Marker({

                position: myLatLng,
                map: map,

                shape: shape,
                title: beach[0],
                zIndex: beach[3]


            });
            console.log(marker);
            bindInfoWindow(marker, map, infowindow, 'Sorry No Data Found');

            <?php } ?>

        }

    }

    function bindInfoWindow(marker, map, infowindow, description) {
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(description);
            infowindow.open(map, marker);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>

<script>

    function change_class(th) {
        $(th).addClass('img_round1');
    }

</script>
<div style="padding: 14px 0px 9px 0px; background: #333" class="col-md-12">
    <div class="col-md-1"><a href="index.php?file=r-rating&rating=1&id=<?php echo $sid;?>"> <img
                src="<?php echo $rate_image_url ?>rsz_1rsz_green.png" onclick="change_class(this);"
                class="<?php if ($ratingdt == '1') { ?>img_round1 <?php } else { ?> img_round <?php } ?>"/> </a></div>

    <div class="col-md-1"><a href="index.php?file=r-rating&rating=0&id=<?php echo $sid;?>"> <img
                src="<?php echo $rate_image_url ?>rsz_rsz_red1.png"
                class="<?php if ($ratingdt == '0') { ?> img_round1 <?php } else { ?> img_round <?php } ?>"/> </a></div>
    <div class="col-md-1"><a href="index.php?file=r-rating&rating=3&id=<?php echo $sid;?>"> <img src="<?php echo $rate_image_url ?>updown.png"
                                                                           class="<?php if ($ratingdt == '3' || $ratingdt == 3) { ?>img_round1 <?php } else { ?> img_round <?php } ?>"
                                                                           height="35px" width="35px"/> </a></div>

    <div class="form-group">

        <div class="col-md-3">

            <select class="form-control" id="distributor" name="distributor" value="" onchange="get_rate(this.value)">
                <option value="0" <?php echo($sid==  '0' ? 'selected': '');?>>All</option>
                <?for ($i = 0; $i < count($db_res); $i++) {?>

                    <option
                        value="<?=$db_res[$i]['iUser']; ?>" <?php echo($sid==  $db_res[$i]['iUser'] ? 'selected': '');?> ><?=$db_res[$i]['vAccountName']; ?></option>
                <? } ?>
            </select>
        </div>
    </div>
    <?php if ($usertype == 4 || $usertype == 5 || $usertype == 6) { ?>
        <div class="pull-right col-md-2"><a href="index.php?file=r-addreting" class="btn red_btn_gen">Add Rating</a>
        </div>
    <?php } ?>

</div>

<div id="map-canvas" class="col-md-12"></div>

<?php
/*$id="2";
$ratedetail=$ratingObj->rating_detail($id);
print_r($ratedetail);
*/?>

<div class="panel-body">
    <div class="tab-content">
        <div class="col-sm-12">

            <section class="panel">
                <div class="panel-body">
                    <div class="adv-table">
                        <div class="table-main-scroll">
                            <table class="display table table-bordered table-striped" id="rating_table">
                                <!-- Changes HERE Start -->
                                <thead>
                                <tr>
                                    <th style="display: none"></th>
                                    <th>Account Name</th>
                                    <th>Account #</th>
                                    <th>Account Employee</th>
                                    <th>Vendor Name</th>
                                    <th>Vendor Employee</th>
                                    <th>Brand</th>
                                    <th style="width: 20%!important">Message</th>
                                    <th>Image</th>
                                    <th>Rating</th>
                                    <th>Date</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                foreach ($res_db as $key => $ordr) {
                                    ?>
                                    <tr>
                                        <td style="display: none"><?php echo $key; ?></td>
                                        <td>
                                            <?
                                            // echo $ordr['storeiniqid'];
                                            if ($ordr['eFlag'] == "1") {
                                                echo $ordr['storeiniqid'];
                                            } else {
                                                echo "Anonymous";
                                            }
                                            ?></td>
                                        <td>
                                            <? if ($ordr['eFlag'] == "1") {
                                                echo $ordr['accounth'];

                                            } else {
                                                echo "Anonymous";
                                            }
                                            ?></td>

                                        <td> <? if ($ordr['eFlag'] == "1") {
                                                echo $ordr['fname'] . " " . $ordr['lname'];


                                            } else {
                                                echo "Anonymous";
                                            }
                                            ?></td>
                                        </td>
                                        <td>
                                            <?
                                            echo $ordr['vendor'];

                                            ?> </td>

                                        <td>
                                            <?
                                            echo $ordr['vFirstName'] . " " . $ordr['vLastName'];

                                            ?> </td>
                                        <td><?php if ($ordr['vBrand'] == "") {
                                                echo "-";
                                            } else {
                                                echo $ordr['vBrand'];
                                            }?></td>
                                        <td width="5%"><?php echo $ordr['vDescription']; ?></td>
                                        <td><?
                                            if ($ordr['vImage'] != "" && file_exists($rate_image_path . $ordr['vImage'])) {
                                                $img = $rate_image_url . $ordr['vImage'];

                                            } else {
                                                $img = $rate_image_url . 'no_photo_icon.png';
                                            }
                                            ?>
                                            <?php $id = $ordr['iRatingId'] ?>
                                            <a href="#myModal" onclick=get_popup_value(<?php echo $id ?>)
                                               data-toggle="modal"><img src="<? echo $img; ?>" height="50px"
                                                                        width="70px"></a>
                                        </td>
                                        <div id="div_<?php echo $ordr['iRatingId']; ?>" style="display: none">

                                            <div id="">

                                                <?
                                                if ($ordr['vImage'] != "" && file_exists($rate_image_path . $ordr['vImage'])) {
                                                    $img = $rate_image_url . $ordr['vImage'];

                                                } else {
                                                    $img = $rate_image_url . 'no_photo_icon.png';
                                                }
                                                ?>
                                                <center><img src="<? echo $img; ?>" height="550px" width="610px">
                                                </center>


                                                </br>



                                            </div>

                                        </div>


                                        <td>
                                            <?
                                            if ($ordr['eRating'] == "1") {
                                                $imgr = "rsz_1rsz_green.png";
                                            } else if ($ordr['eRating'] == "0") {
                                                $imgr = "rsz_rsz_red1.png";
                                            }

                                            ?>
                                            <img src="<? echo $rate_image_url . $imgr; ?>" height="50px" width="50px">
                                        </td>
                                        <td> <?php echo $ordr['vTitle']; ?></td>

                                        <!-- <td><?php /*echo $ordr['eFlag'];*/ ?></td>-->


                                    </tr>

                                <?php } ?>
                                </tbody>
                                <!-- Changes HERE Ends -->
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div></div></div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog-brodcast">
        <div class="modal-content">
            <div class="modal-header red-light-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Rating Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="frmadd_view_rating" name="frmadd">


                        </form>
                        </br>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog-brodcast">
        <div class="modal-content">
            <div class="modal-header red-light-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Help</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="frmadd_view_promotion1" name="frmadd12">
                            <center>
                                <img src="<? echo $dafault_image_logo ?>" height="70px" width="70px"></center>
                            <center>

                                <center><h2>Rating</h2></center>
                                <? echo $info_icon[0]['tDescription'] ?>
                        </form>
                        </br>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#rating_table').dataTable({
        });
    });
</script>
<script>

    function get_popup_value(id) {
        $('#frmadd_view_rating').html($('#div_' + id).html());
        console.log($('#div_' + id).html());
    }

    function get_rate(id)
    {
        window.location.href="index.php?file=r-rating&id="+id;
    }
</script>

<?php
include_once($admin_path . 'js_datatable.php');
?>