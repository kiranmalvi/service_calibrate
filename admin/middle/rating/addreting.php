<?php
/**
 * Created by PhpStorm.
 * User: Hardik
 * Date: 2/6/15
 * Time: 5:25 PM
 */
$iUserId = $_SESSION['SC_LOGIN']['USER']['iUserId'];

include_once($inc_class_path . 'orders.class.php');
//$lastdate = strtotime(date('Y-m-1 00:00:01'));;
$orderobj = new orders();
$db_res = $orderobj->select_retailer($iUserId);
//print_r($db_res); exit;
?>
<section id="main-content">
    <section class="wrapper">

        <!--main content start-->

        <!-- page start-->


        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>
                    <li>
                        <a href="index.php?file=r-rating&type=<? echo $type; ?>">
                            Rating</a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;">Add New Rating</a>

                    </li>

                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading red-bg">

                        <h4>Rating</h4>
                    </header>
                    <!--   <div style="padding: 10px 0; background: #eb4343; margin-bottom: 15px;" class="col-md-12" >
                       <h3 style="color: #ffffff; padding-left: 10px; ">Rating</h3>
                       </div>
                   -->
                    <div class="panel-body">
<?if(isset($_SESSION['rate_err'])&& $_SESSION['rate_err']!==""){?>
                     <center>   <div class="row">
                             <div class="col-md-2"></div>
                            <div class="alert alert-danger fade in col-md-8" style="margin-left: 15px">
                                <?echo $_SESSION['rate_err'];

                                unset($_SESSION['rate_err'])?>
                            </div>
                        </div>
                     </center>
                        <?}?>
                        <input type="hidden" name="iUserId" id="iUserId" value="" >
                        <form class="form-horizontal bucket-form" method="post" name="frmratingadd" id="frmratingadd"
                              action="index.php?file=r-rating_a&mode=add&uid=<?= $iUserId; ?>" enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Vendor </label>
                                <div class="col-md-8">

                                    <select class="form-control" id="distributor" name="distributor" value="" onchange="get_employee(this.value)">
                                        <option value=""> - Select Vendor -
                                        </option>
                                        <?for ($i = 0; $i < count($db_res); $i++) {?>

                                            <option
                                                value="<?=$db_res[$i]['iScannedId']; ?>" ><?=$db_res[$i]['vAccountName']; ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Employee </label>
                                <div class="col-md-8">
                                    <select class="form-control" id="manufacture" name="manufacture"
                                            value="">

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" style="margin-top: 15px;" > Brand Name </label>
                                <div class="col-md-8">
                                    <input type="text" name="brand" id="brand" class="form-control" value="" placeholder="Brand Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Select Any One </label>
                                    <input type="hidden" name="like_dislike" id="like_dislike" value="">
                                    <div class="col-md-2" style="margin-left: 2%">
                                        <img src="green.png" id="green" onclick="setlike_dislike('1');">
                                        <!--<input type="image" id="myimage" src="green.png" />-->
                                    </div>
                                    <div class="col-md-2">
                                        <img src="red.png" id="red" onclick="setlike_dislike('0');">
                                        <!--<input type="image" id="myimage" src="red.png" />-->
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"> Feedback </label>
                                <div class="col-md-8">
                                    <!--<input type="text" name="feedback" id="feedback" class="form-control" value="" placeholder="Write Your Feedback Here">-->
                                    <textarea name="feedback" id="feedback" class="form-control"  placeholder="Write Your Feedback Here"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Image Upload</label>

                                <div class="col-md-8">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">


                                            <img src="<? echo $dafault_image_camera; ?>"
                                                 style="width: 150px; height: 150px; alt="">


                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                                   <span class="btn btn-white btn-file">
                                                   <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                   <span class="fileupload-exists"><i
                                                           class="fa fa-undo"></i> Change</span>
                                                   <input type="file" class="default" id="vImage" name="vImage">
                                                   </span>
                                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i
                                                    class="fa fa-trash"></i> Remove</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Post As Anonymous</label>
                                    <input type="hidden" name="eFlag" id="eFlag" value="1">
                                    <div class="col-md-8">
                                        <input type="checkbox" class="form-control" onchange="checkuser(this)" value="true">
                                    </div>
                                    <!--<div class="col-md-2">
                                        <input  type="file" name="vImage" id="vImage">
                                    </div>-->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">

                                    <button id="addbutton" type="submit" class="btn btn-success1"">Add</button>
                                    <a href="index.php?file=r-rating" class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>

                    </div>

                </section>
            </div>
        </div>
        <script>
            function setlike_dislike(id){
                $("#like_dislike").val(id);
                if(id == '0') {
                    $("#green").css("border","none");
                    $("#green").css("border-radius","none");
                    $("#red").css("border","5px solid #E30000");
                    $("#red").css("border-radius","80px");
                }
                if(id == '1') {
                    $("#red").css("border","none");
                    $("#red").css("border-radius","none");
                    $("#green").css("border","5px solid #00B900");
                    $("#green").css("border-radius","80px");
                }
                return false;
            }
        </script>
        <!-- page end-->
    </section>
</section>
<?
include_once($admin_path . 'js_form.php');
?>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#frmratingadd');

            $('#frmratingadd').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    distributor: {
                        required: true
                    },
                    manufacture: {
                        required: true
                    },
                    like_dislike: {
                        required: true
                    },
                    eFlag:{
                        required : true
                    }

                },
                messages: {
                    distributor: "Select Distributor",
                    manufacture: "Please Select Employee Name",
                    like_dislike: "Please Select thumb for like or Dislike",
                    eFlag: "Please Select Anonymous Use"

                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#coupon_form').submit();
        });
    });



</script>
<script>
    function get_employee(iUserId) {

        $.ajax({
            url: 'index.php?file=r-rating_a',
            type: 'POST',
            data: {"mode": 'get_employee', "user_id": iUserId},
            success: function (result) {
                console.log('sdf : ', result);
                $('#manufacture').html(result);
            }
        });
    }
</script>


<script>
    function checkuser(th) {
        val1 = $(th).prop('checked');
        if (val1 == true) {
            document.getElementById('eFlag').value = 0;
        }
        else {
            document.getElementById('eFlag').value = 1;
        }
    }
</script>