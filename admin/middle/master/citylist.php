<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 6/4/15
 * Time: 2:39 PM
 */
include_once($inc_class_path . 'city.class.php');

$city = new city();
$db_res = $city->select_with_state_country();

$MODULE = 'City';
$ADD_LINK = 'index.php?file=ma-cityadd';
$ACT_LINK = 'index.php?file=ma-city_a';
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];
?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php">Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="#"><?php echo $MODULE; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <!-- Table Section Header Starts -->
                        <header class="panel-heading">
                            <?php echo $MODULE; ?> Listing
                        <span class=" pull-right header-btn">
<? if ($atype != "R") { ?>
                            <a class="btn btn-success1" href="<?php echo $ADD_LINK; ?>">
                                Add New <?php echo $MODULE; ?>
                            </a>
                            <button class="btn btn-success1" onclick="delete_record()">
                                DELETE
                            </button>
    <?}?>
                        </span>
                        </header>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <div class="admin-tbl-main">
                                    <table class="display table table-bordered table-striped" id="city_table">
                                        <!-- Changes HERE Start -->
                                        <thead>
                                        <tr>
<? if ($atype != "R") { ?>
                                            <td><input type="checkbox" onclick="check_all();"></td>
                                            <td>Action</td>
    <?}?>
                                            <td>City</td>
                                            <td>State</td>
                                            <td>Country</td>
                                            <td>Date Added</td>
                                            <td>Status</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($db_res as $STATE) {
                                            ?>
                                            <tr>
                                                <? if ($atype != "R") { ?>
                                                <td>
                                                    <input type="checkbox" name="delete[]"
                                                           value="<?php echo $STATE['iCityId']; ?>">
                                                </td>
                                                <td>
                                                    <a class="btn btn-success btn-xs edit-delete-btn"
                                                       href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $STATE['iCityId']; ?>">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>

                                                    <a class="btn btn-danger btn-xs edit-delete-btn"
                                                       href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $STATE['iCityId']; ?>&delete_type=single_delete">
                                                        <i class="fa fa-edit"></i> Delete
                                                    </a>
                                                </td>
                                                <?}?>
                                                <td><?php echo $STATE['vName']; ?></td>
                                                <td><?php echo $STATE['state']; ?></td>
                                                <td><?php echo $STATE['country']; ?></td>
                                                <td>
                                                    <?php echo gmdate('m-d-Y H:i:s', $STATE['iDtAdded']); ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($STATE['eStatus'] == '1') {
                                                        echo '<span class="label label-success edit-active">Active</span>';
                                                    } else {
                                                        echo '<span class="label label-danger edit-active">Inactive</span>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                        <!-- Changes HERE Ends -->
                                    </table>
												</div>                                
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>

<?php
include_once($admin_path . 'js_datatable.php');
?>


<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#city_table').dataTable({
   "aaSorting": [[ 3, "desc" ]]
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>

<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#city_table').dataTable({
            	   "aaSorting": [[ 5, "desc" ]],
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });
    </script>
<?php
}