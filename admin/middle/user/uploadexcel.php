<?php

//pr($_REQUEST);
$type = $_REQUEST['type'];
$iId = $_REQUEST['iId'];

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

$USER = $userObj->select($iId);

$currentcount=$USER['0']['iStoreCount'];
$maxstore=$USER['0']['vComment'];

$currentavlcount=$maxstore-$currentcount;



?>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">

                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>
                    <li>
                        <a href="index.php?file=u-userlist&type=<?php echo $type; ?>"> User</a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;"> Upload Excel</a>                    </li>
                </ul>
            </div>
        </div>

        <section class="panel">


            <header class="panel-heading red-bg">

                <h4>Upload Excel</h4>
            </header>


            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-body">
                        </div>
                        <div class="col-md-12">
                            <form class="form-horizontal" role="form" enctype="multipart/form-data" id="FORM_CSV_UPLOAD"
                                  name="FORM_CSV_UPLOAD" method="post"
                                  action="index.php?file=u-useradd_a&mode=upload_CSV">
                                <input type="hidden" id="type" name="type" value="<?php echo $type; ?>">
                                <input type="hidden" id="iId" name="iId" value="<?php echo $iId; ?>">
                                <input type="hidden" id="storecount" name="storecount"
                                       value="<?php echo $USER[0]['iStoreCount']; ?>">
                                <input type="hidden" id="max" name="max"
                                       value="<?php echo $USER[0]['iStoreCount']; ?>">


                                <div class="row" style="margin-left: 15px;">

                                     <div class="alert alert-info fade in col-md-8 ">
                                        <strong>Note : </strong>
                                        <br>1) We ONLY accept the provided format.Please click on "Download Excel Format".
                                        <br>
                                       2) All account name & email address needs to be unique.
                                        <br>
                                        3) All fields on excel need to be filled.
                                         <br>
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Upload</label>

                                    <div class="col-lg-7">
                                        <input type="file" class="" name="CSV">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">


                                        <button id="addbutton" type="submit" class="btn btn-success1">Submit</button>

                                        <a href="index.php?file=u-userlist&type=<?php echo $type; ?>"
                                           class="btn btn-default1">Back</a>

                            </form>


                            <form name="sampledownload"
                                  action="index.php?file=su-download_sample_a&utype=<?= $utype; ?>"
                                  id="sampledownload" method="post" style="float:inherit">


                                <!--<button id="downloadexcelBlutton" name="downloadexcelBlutton" type="submit"
                                            class="btn btn-success">Download Excel
                                    </button>-->
                                <!--<button id="downloadexcelBlutton" name="downloadexcelBlutton" type="submit"
                                        class="btn btn-primary">Download Excel Format
                                </button> &nbsp;-->
                                <?php if ($type == 2 || $type == 3) { ?>
                                    <a href="Distributor.csv" onclick="return popitup('Distributor.csv')"
                                       class="btn btn-success"> Download Excel Format </a>
                                <?php } elseif ($type == 4) { ?>
                                    <a href="retail4.csv" onclick="return popitup('retail4.csv')"
                                       class="btn btn-success"> Download Excel Format </a>
                                <?php } elseif ($type == 5) { ?>
                                    <a href="retail5.csv" onclick="return popitup('retail5.csv')"
                                       class="btn btn-success"> Download Excel Format </a>

                                <?php } elseif ($type == 6) { ?>
                                    <a href="retail6.csv" onclick="return popitup('retail6.csv')"
                                       class="btn btn-success"> Download Excel Format </a>
                                <? } ?>
                                &nbsp;
                            </form>

                            <!--                                    <a href="index.php?file=su-download_sample_a&utype=-->
                            <? //=$utype;?><!--"-->
                            <!--                                       class="btn btn-success">Download Excell</a>-->
                        </div>
                    </div>


                </div>

            </div>

            </div>
            </div>
            </div>
            <script> // This function will pop a window which will tell the
                // user the order of fields  and format of .csv file
                // you can create pop_up_csv.html file in same directory and
                // modify it with required format
                function popitup(url) {
                    if (url) {
                        return true;
                    }
                }
            </script>


        </section>
    </section>
</section>
<?
include_once($admin_path . 'js_form.php');
?>
