<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 24/3/15
 */

$USER_TYPE = $_REQUEST['type'];
$iId = $_REQUEST['iId'];
include_once($inc_class_path . 'user_type.class.php');
$usertypeObj = new user_type();

include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'plan.class.php');
$planObj = new plan();

if (isset($_REQUEST['pid']) && $_REQUEST['pid'] > "0") {
    $pid = $_REQUEST['pid'];
    $ssql = "where 1=1 AND u.eStatus !='2' AND (u.iUserId = $pid or u.iParentId=$pid) ORDER BY u.iUserId ASC";
} else {
    $ssql = "where 1=1 AND u.eStatus !='2' AND (u.iUserId = $iId or u.iParentId=$iId) ORDER BY u.iUserId ASC";
}
//$ssql = "where 1=1 AND eStatus !='2' AND (iUserId = $iId or iParentId=$iId)";
 $sql = "select count(*) as tot  from user " . $ssql;

$db_res = $obj->select($sql);
$num_totrec = $db_res[0]['tot'];

$sql = "select u.*,r.vPoint as point from user u left join referral r on u.iUserId=r.iUserId " . $ssql;
$db_res = $obj->select($sql);

//pr($db_res);


$parent = $userObj->select($iId);
$plan = $userObj->getiPlanId();

$planObj->select($plan);
$maxstore = $planObj->getiToStore();

$type = $_REQUEST['type'];


$USER_TYPE_QUERY = "SELECT vName FROM user_type WHERE iUserTypeId = $USER_TYPE ";
$GET_USER_TYPE = $obj->select($USER_TYPE_QUERY);
$USER_TYPE_DATA = $GET_USER_TYPE[0]['vName'];


$ACT_LINK = "index.php?file=u-useradd_a&type=$USER_TYPE&iId=$iId&pid=$iId&page=sublist";
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];


?>
<!--main content start-->
<section id="main-content">

    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">

                    <? if (isset($_SESSION['SC_LOGIN']['ADMIN'])) { ?>
                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>
                    <? } ?>
                    <li>
                        <a href="index.php?file=u-userlist&type=<?php echo $type; ?>&iId=<?php echo $db_res[0]['iUserId']; ?>">User
                            (<? echo $USER_TYPE_DATA; ?>)</a>
                    </li>
                    <li>
                        <a class="current" href="#"><? echo $db_res[0]['vStoreUniqueId'] ?></a>
                    </li>

                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue">
                        <div class="manu-main-scroll">
                            <div class="manu-main-scroll-next">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a data-toggle="tab" onclick="location.href='index.php?file=u-subuserlist&type=<?php echo $type?>&iId=<?php echo $iId?>'">
                                            Manage Account
                                        </a>
                                    </li>


                                    <li class="">
                                        <a data-toggle="tab" onclick="location.href='index.php?file=u-paymentadd&iId=<?php echo $iId?>&type=<?php echo $type?>'">
                                            Payment Information
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>


                    </header>
                    <?php
                    $SUB_USER = "SELECT iUserTypeId,vName FROM user_type WHERE iUserTypeId = $USER_TYPE";
                    $GET_SUB = $obj->select($SUB_USER);
                   //pr($GET_SUB);
                    $USER_SUB_COUNT = count($GET_SUB);
                    ?>
<!--
                    <header class="panel-heading">
                        User
                        <span class=" pull-right header-btn">

                        <?/* if ($parent[0]['iStoreCount'] < $maxstore) { */?>
                                <a class="btn btn-success1"
                                   href="index.php?file=u-useradd&mode=add&iId=<?/* echo $iId; */?>&type=<?/* echo $USER_TYPE */?>&page=sublist">Add
                                    New Employee</a>
                        <?/* } */?>

                            <button class="btn btn-success1" onclick="delete_record()">
                                DELETE
                            </button>
                            </span>
                    </header>-->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                  <span class="tools pull-right">
                             <?php if($USER_TYPE!='4' && $atype!="R")
                             {?>
                             	    
                             	    


                                 <a class="btn btn-success1"
                                    href="index.php?file=u-uploadexcel&mode=uploadexcel&iId=<? echo $iId; ?>&type=<? echo $USER_TYPE ?>&page=sublist">Upload Excel</a>


                              
                            <a class="btn btn-success1"
                               href="index.php?file=u-useradd&mode=add&iId=<? echo $iId; ?>&type=<? echo $USER_TYPE ?>&page=sublist">Add
                                New Employee</a>
 <a class="btn btn-success1"
                                    onclick="delete_record();">
                                     DELETE</a>
    <?}?>
                        </span>
                                <?if(isset($_SESSION['admin_upload_excel'])&& $_SESSION['admin_upload_excel']!="")
                                {?>

                                    <div style="margin-left: 15px;">
                                        <div class="alert <?php echo $_SESSION['admin_upload_class'];?>  fade in col-md-6">
                                            <?php echo $_SESSION['admin_upload_excel']?>
                                        </div>
                                    </div>
                                <?}?>

                            </div>
                        </div>
                        <div class="adv-table">
                            <form name="user_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                <input type="hidden" name="mode" id="mode" value="">
                                <input type="hidden" name="delete_type" value="multi_delete">

                                <table class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                <tr>
                                   <?php if($atype!="R") {?>
                                    <th><input type="checkbox" onclick="check_all();"></th>
                                    <th>Action</th>
                                    <?php }?>
                                    <th>Employee Name</th>
                                    <?php
                                     if($USER_TYPE=='2' || $USER_TYPE=='3') {?>
                                         <th>Vendor Name</th>
                                         <?php

                                     }
                                     else if($USER_TYPE=='4' || $USER_TYPE=='5'  || $USER_TYPE=='6') {

                                    ?>
                                    <th>Account Name</th>
                                         <?php }?>
                                    <th>Email</th>
                                    <?php
                                    if($USER_TYPE=='2' || $USER_TYPE=='3') {?>
                                        <th>Role</th>
                                    <?php

                                    }
                                    else if($USER_TYPE=='4' || $USER_TYPE=='5'  || $USER_TYPE=='6') {

                                        ?>
                                        <th>Account #</th>
                                    <?php }?>
   <th>Points</th>
                                    <th>Date Added</th>


                                </tr>
                                </thead>
                                <tbody>

                                <? for ($i = 0; $i < count($db_res); $i++) {
                                    $parent = $usertypeObj->getparent($db_res[$i]['iUserTypeId']);?>

                                    <tr class="gradeX">
                                     <?php if($atype!="R") {?>

                                        <td>
                                            <?// Owner can not be deleted (role id=0 for owner)
                                            if ($db_res[$i]['iParentId'] != "0") {
                                                ?>

                                            <input type="checkbox" name="delete[]"
                                                   value="<?php echo $db_res[$i]['iUserId']; ?>">
                                            <? } ?>
                                        </td>

                                        <td>
                                            <a class="btn btn-success btn-xs"
                                               href="index.php?file=u-useradd&type=<?php echo $type; ?>&mode=update&pid=<?php echo $iId;?>&iId=<?php echo $db_res[$i]['iUserId']; ?>&page=sublist">Edit</a>

                                            <?// Owner can not be deleted (role id=0 for owner)
                                            if ($db_res[$i]['iParentId'] != "0") {
                                                ?>
                                            <a class="btn btn-danger btn-xs"
                                               href="index.php?file=u-useradd_a&mode=delete&type=<?php echo $type; ?>&pid=<?php echo $iId; ?>&iId=<?php echo $db_res[$i]['iUserId']; ?>&delete_type=single_delete&page=sublist">Delete

                                            </a>
                                            <? } ?>
                                        </td>
                                        <?php }?>
                                        <td>

                                            <? echo $db_res[$i]['vFirstName'] . " " . $db_res[$i]['vLastName']; ?>
                                        </td>

                                        <td>

                                            <? echo $db_res[$i]['vStoreUniqueId']; ?>
                                        </td>

                                        <td><?php echo $db_res[$i]['vEmail']; ?></td>
                                        <?php
                                        if($USER_TYPE=='2' || $USER_TYPE=='3') {
                                            $roleid = $db_res[$i]['iUserRoleId'];
                                            if ($db_res[$i]['iParentId'] == "0") {
                                                $d_name="Owner";
                                            } else {

                                                $user_role = "SELECT vName FROM user_role WHERE iUserRoleId = $roleid";
                                                $role_name = $obj->select($user_role);
                                                $d_name= $role_name[0]['vName'];
                                            }

                                        }
                                        else if($USER_TYPE=='4' || $USER_TYPE=='5'  || $USER_TYPE=='6') {

                                            $d_name=$db_res[$i]['vStoreName'];

                                      }?>
                                        <td><?php
                                            echo $d_name;
                                            ?></td>
                                                 
                                        <td>

                                            <?
                                            if($db_res[$i]['point']>0)
                                            {
                                                $point=$db_res[$i]['point'];
                                            }
                                            else
                                            {
                                                $point=0;
                                            }

                                            echo $point; ?>
                                        </td>
                                        <td><?php echo date('m-d-Y H:i:s',$db_res[$i]['iDtAdded']); ?></td>

                                    </tr>
                                <? } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

<!-- Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog-brodcast">
        <div class="modal-content">
            <div class="modal-header purple-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New Brodcast</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <?php
                        foreach ($GET_SUB as $SUB_DATA) {
                            ?>
                            <a href="index.php?file=u-useradd&type=<?php echo $SUB_DATA['iUserTypeId'] ?>&page=sublist" type="button"
                               class="btn btn-primary">Add <?php echo $SUB_DATA['vName']; ?></a>
                        <?php
                        }
                        ?>
                        <!--<a href="index.php?file=u-useradd&type=singlestore" type="button" class="btn btn-primary">Add
                            Single Store Owner</a>
                        <a href="index.php?file=u-useradd&type=chain" type="button" class="btn btn-primary">Add Multiple
                            Store Owner</a>
                        <a href="index.php?file=u-useradd&type=corporate" type="button" class="btn btn-primary">Add
                            Corporate Account </a>-->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- modal -->


<?php
unset($_SESSION['admin_upload_excel']);
unset($_SESSION['admin_upload_class']);
include_once($admin_path . 'js_datatable.php');
?>