<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:51 PM
 */

$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';
if ($iId != '') {
    include_once($inc_class_path . 'billing_info.class.php');
    $BillinginfoObj = new billing_info();

    include_once($inc_class_path . 'billing_info.class.php');
    $UserObj = new user();
    $db_res = $BillinginfoObj->billingAddress($iId);
    $db_res_user = $UserObj->select($iId);

    $plan_info="select * from user_plan_info where iUserId='$iId'";
    $plan=$obj->select($plan_info);
    //pr($plan);
}

$account=$db_res_user[0]['vStoreUniqueId'];
if(count($db_res)>0)
{

    $address=$db_res['vAddress'];
    $city=$db_res['vCity'];
    $state=$db_res['VState'];
    $zip=$db_res['vZip'];
    $country=$db_res['vCountry'];
}
else
{
    $address=$db_res_user[0]['vAddress'];
    $city=$db_res_user[0]['vCity'];
    $state=$db_res_user[0]['vState'];
    $zip=$db_res_user[0]['vZip'];
    $country=$db_res_user[0]['iCountryId'];
}

//pr($db_res);
$MODE_TYPE = ucfirst($mode);


$USER_TYPE=$_REQUEST['type'];
$USER_TYPE_QUERY = "SELECT vName FROM user_type WHERE iUserTypeId = $USER_TYPE ";
$GET_USER_TYPE = $obj->select($USER_TYPE_QUERY);
$USER_TYPE_DATA = $GET_USER_TYPE[0]['vName'];

$MODULE = 'User('.$USER_TYPE_DATA.')';
$PRE_LINK = "index.php?file=u-userlist&type=$USER_TYPE&iId=$iId";
$ACT_LINK = "index.php?file=u-useradd_a&type=$USER_TYPE";

?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <li>
                        <a href="<?php echo $PRE_LINK; ?>"> <?php echo $MODULE; ?></a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;"> <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Form Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Form Section Header Starts -->
                    <header class="panel-heading tab-bg-dark-navy-blue">
                        <div class="manu-main-scroll">
                            <div class="manu-main-scroll-next">
                                <ul class="nav nav-tabs">
                                    <li class="">
                                        <a data-toggle="tab" onclick="location.href='index.php?file=u-subuserlist&type=<?php echo $type?>&iId=<?php echo $iId?>'">
                                            Manage Account
                                        </a>
                                    </li>


                                    <li class="active">
                                        <a data-toggle="tab" onclick="location.href='index.php?file=u-paymentadd&iId=<?php echo $iId?>&type=<?php echo $type?>'">
                                            Payment Information
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </header>
                    <!-- Form Section Header Ends -->

                    <!-- Form Section Body Starts -->
                    <div class="panel-body">
                        <div class="row">

                            <form class="form-horizontal bucket-form" method="post" name="admin_form" id="admin_form"
                                  action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                                <input type="hidden" name="mode" value="update_payment">
                                <input type="hidden" name="bId" id="iId"
                                       value="<?php echo $db_res['iBillingId']; ?>">
                                <input type="hidden" name="uid" id="uid"
                                       value="<?php echo $iId; ?>">

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> User Name :</label>
                                    <div class="col-md-8">
                                        <input type="text" name="vFirstName" id="vFirstName" class="form-control"
                                               value="<?php echo $account; ?>" placeholder="User Name" readonly>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span>Address:</label>

                                    <div class="col-md-8">
                                        <input type="text" name="vAddress" id="vAddress" class="form-control"
                                               value="<?php echo $address; ?>" placeholder="Address">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span>Zip:</label>

                                    <div class="col-md-8">
                                        <input type="text" name="vZip" id="vZip" class="form-control"
                                               value="<?php echo $zip; ?>" placeholder="Zip">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span>City:</label>

                                    <div class="col-md-8">
                                        <input type="text" name="vCity" id="vCity" class="form-control"
                                               value="<?php echo $city; ?>" placeholder="City">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> State :</label>

                                    <div class="col-md-8">
                                        <input type="text" name="vState" id="vState" class="form-control"
                                               value="<?php echo $state; ?>" placeholder="State">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Country :</label>

                                    <div class="col-md-8">
                                        <input type="text" name="vCountryId" id="vCountryId" class="form-control"
                                               value="<?php echo $country; ?>" placeholder="Country">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Amount :</label>

                                    <div class="col-md-8">
                                        <input type="text" name="fAmountPaid" id="fAmountPaid" class="form-control"
                                               value="<?php  echo(($plan[0]['fPlanAmout'] == '') ? '0.00' : $plan[0]['fPlanAmout']); ?>" placeholder="Amount">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" id="lblend"><span class="red"> *</span> Expire Date :</label>


                                    <div class="col-md-8">
                                        <input class="form-control form-control-inline input-medium default-date-picker"
                                               type="text" name="iEndDate" id="iEndDate"                                           value="<?php echo (isset($db_res_user[0]['iExpireTime'])) ? date('m-d-Y', $db_res_user[0]['iExpireTime']) : ''; ?>"/>

                                        <div id="enderr">

                                        </div>
                                    </div>



                                </div>




                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Plan :</label>

                                    <div class="col-md-8">
                                        <select class="form-control" name="plantype" id="plantype">
                                            <option selected disabled>- Select Plan -</option>
                                            <option
                                                value="monthly" <?php echo($plan[0]['ePlanType'] == 'monthly' ? 'selected' : ''); ?>>
                                             Monthly
                                            </option>
                                            <option
                                                value="annual" <?php echo($plan[0]['ePlanType'] == 'annual' ? 'selected' : ''); ?>>
                                             Annually
                                            </option>
                                            
                                             </option>
 <option  value="free" <?php echo(($plan[0]['ePlanType'] == ''||$plan[0]['ePlanType'] == 'free') ? 'selected' : ''); ?>>
                                             Free
                                            </option>

                                        </select>
                                    </div>
                                </div>




                                <div class="form-group">
                                    <label class="col-md-2 control-label"><span class="red"> *</span> Status :</label>

                                    <div class="col-md-8">
                                        <select class="form-control" name="eStatus" id="eStatus">
                                            <option selected disabled>- Select Status -</option>
                                            <option
                                                value="1" <?php echo($db_res_user[0]['eActivePaidPlan'] == '1' ? 'selected' : ''); ?>>
                                                Paid
                                            </option>
                                            <option
                                                value="0" <?php echo($db_res_user[0]['eActivePaidPlan'] == '0' ? 'selected' : ''); ?>>
                                                Free Trail
                                            </option>
                                            <option
                                                value="2" <?php echo($db_res_user[0]['eActivePaidPlan'] == '2' ? 'selected' : ''); ?>>
                                                Expired
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button id="addbutton" type="submit"
                                                class="btn btn-success1">Update</button>
                                        <a href="<?php echo $PRE_LINK; ?>" class="btn btn-default1">Back</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <!-- Form Section Body Starts -->

                </section>
            </div>
        </div>
        <!-- Form Section Ends -->

        <!-- page end-->
    </section>
</section>

<?
include_once($admin_path . 'js_form.php');
?>

<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>



<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#admin_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    vAddress: {
                        required: true
                    },
                    vCity: {
                        required: true
                    },
                    VState: {
                        email: true,
                        required: true
                    },
                    vZip: {
                        required: true

                    },   vCountryId: {
                        required: true

                    },
                    iEndDate:{
                        required: true
                    },
                    fAmountPaid: {
                        required: true,
                        number:true

                    },
                    eStatus: {
                        required: true
                    }, plantype: {
                        required: true
                    }

                },
                messages: {
                    vAddress: "Please Enter Address",
                    vCity: "Plese Enter City",
                    VState: {
                        required: "Please Enter State"

                    },
                    vZip: {
                        required: "Please Enter Password"
                    },
                    vCountryId: {
                        required: "Please Enter Country"

                    },
                    iEndDate:{
                        required: "Please Select Expire Date"
                    },
                    fAmountPaid: {
                        required: "Please Enter Amount",
                        number:"Enter Only Digits"

                    },
                    eStatus: "Please Select Status",
                    plantype:"Please Select Plan Type"

                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#admin_form').submit();
        });
    });
</script>
