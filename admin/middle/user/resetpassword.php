<?
$type = $_REQUEST['type'];
$iId = $_REQUEST['iId'];
?>


<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <li>
                        <a href="index.php?file=u-userlist&type=<?php echo $type; ?>"> User</a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;"> Reset Password </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            <? echo $header . " " . "User"; ?>
                            <span class=" pull-right header-btn">

                        </h4>
                    </header>

                    <div class="panel-body">


                        <form class="form-horizontal bucket-form" method="post" name="reset_pwd" id="reset_pwd"
                              action="index.php?file=u-useradd_a&mode=resetpassword"
                              enctype="multipart/form-data">

                            <input type="hidden" name="iId" id="iId" value="<?php echo $iId; ?>">
                            <input type="hidden" name="type" id="type" value="<?php echo $type; ?>">

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Password
                                    :</label>

                                <div class="col-md-8">
                                    <input type="password" name="password" id="password" class="form-control"
                                           value="<? /* echo $db_res[0]['vPassword'] */ ?>">
                                </div>
                            </div>

                            <div class="form-group" id="cnf">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Confirm Password
                                    :</label>

                                <div class="col-md-8">
                                    <input type="password" name="cnfpwd" id="cnfpwd" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">


                                    <button id="addbutton" type="submit" class="btn btn-success1">Update</button>

                                    <a href="index.php?file=u-userlist&type=<?php echo $type; ?>"
                                       class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>



<?
include_once($admin_path . 'js_form.php');
?>
<script src="<?php
echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#reset_pwd');

            $('#reset_pwd').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    password: {
                        required: true,
                        minlength: "6",
                        maxlength: "10"
                    },
                    cnfpwd: {
                        required: true,
                        equalTo: "#password"
                    }

                },
                messages: {
                    password: {
                        required: "Please Enter Password",
                        minlength: "Please Enter Minimum 6 Character",
                        maxlength: "Password can not be more than 10 character"
                    },
                    cnfpwd: {
                        required: "Please Enter Confirm Password",
                        equalTo: "Please Enter Confirm Password Same As Password"
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#reset_pwd').submit();
        });
    });
</script>