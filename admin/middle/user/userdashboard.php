<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 26/3/15
 * Time: 5:51 PM
 */

$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';
if ($iId != '') {
    include_once($inc_class_path . 'billing_info.class.php');
    $BillinginfoObj = new billing_info();

    include_once($inc_class_path . 'billing_info.class.php');
    $UserObj = new user();
    $db_res = $BillinginfoObj->billingAddress($iId);
    $db_res_user = $UserObj->select($iId);

    $plan_info="select * from user_plan_info where iUserId='$iId'";
    $plan=$obj->select($plan_info);
    //pr($plan);
}

$account=$db_res_user[0]['vStoreUniqueId'];
if(count($db_res)>0)
{

    $address=$db_res['vAddress'];
    $city=$db_res['vCity'];
    $state=$db_res['VState'];
    $zip=$db_res['vZip'];
    $country=$db_res['vCountry'];
}
else
{
    $address=$db_res_user[0]['vAddress'];
    $city=$db_res_user[0]['vCity'];
    $state=$db_res_user[0]['vState'];
    $zip=$db_res_user[0]['vZip'];
    $country=$db_res_user[0]['iCountryId'];
}

//pr($db_res);
$MODE_TYPE = ucfirst($mode);


$USER_TYPE=$_REQUEST['type'];
$USER_TYPE_QUERY = "SELECT vName FROM user_type WHERE iUserTypeId = $USER_TYPE ";
$GET_USER_TYPE = $obj->select($USER_TYPE_QUERY);
$USER_TYPE_DATA = $GET_USER_TYPE[0]['vName'];

$MODULE = 'User('.$USER_TYPE_DATA.')';
$PRE_LINK = "index.php?file=u-userlist&type=$USER_TYPE&iId=$iId";
$ACT_LINK = 'index.php?file=u-useradd_a';

?>

<!--main content start-->
<section id="main-content">
<section class="wrapper">
<!-- page start-->

<!-- Breadcrumbs Starts -->
<div class="row">
    <div class="col-md-12">
        <ul class="breadcrumbs-alt">
            <li>
                <a href="index.php"> Dashboard</a>
            </li>
            <li>
                <a href="<?php echo $PRE_LINK; ?>"> <?php echo $MODULE; ?></a>
            </li>
            <li>
                <a class="current" href="javascript:;"> <?php echo "Dashboard"; ?> </a>
            </li>
        </ul>
    </div>
</div>
<!-- Breadcrumbs Ends -->

<!-- Form Section Starts -->
<div class="row">
    <div class="col-sm-12">
        <section class="panel">

            <!-- Form Section Header Starts -->
            <header class="panel-heading tab-bg-dark-navy-blue">
                <div class="manu-main-scroll">
                    <div class="manu-main-scroll-next">
                        <ul class="nav nav-tabs">
                            <li class="">
                                <a data-toggle="tab" onclick="location.href='index.php?file=u-subuserlist&type=<?php echo $type?>&iId=<?php echo $iId?>'">
                                    Manage Account
                                </a>
                            </li>


                            <li class="">
                                <a data-toggle="tab" onclick="location.href='index.php?file=u-paymentadd&iId=<?php echo $iId?>&type=<?php echo $type?>'">
                                    Payment Information
                                </a>
                            </li>

                            <li class="active">
                                <a data-toggle="tab" onclick="location.href='index.php?file=u-userdashboard&iId=<?php echo $iId?>&type=<?php echo $type?>'">
                                   Dashboard
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </header>
            <!-- Form Section Header Ends -->

            <!-- Form Section Body Starts -->
            <div class="panel-body">
                <div class="row">


                </div>
            </div>
            <!-- Form Section Body Starts -->

        </section>
    </div>
</div>
<!-- Form Section Ends -->

<!-- page end-->
</section>
</section>

<?
include_once($admin_path . 'js_form.php');
?>

<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>



<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#admin_form');

            $('#admin_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    vAddress: {
                        required: true
                    },
                    vCity: {
                        required: true
                    },
                    VState: {
                        email: true,
                        required: true
                    },
                    vZip: {
                        required: true

                    },   vCountryId: {
                        required: true

                    },
                    iEndDate:{
                        required: true
                    },
                    fAmountPaid: {
                        required: true,
                        number:true

                    },
                    eStatus: {
                        required: true
                    }, plantype: {
                        required: true
                    }

                },
                messages: {
                    vAddress: "Please Enter Address",
                    vCity: "Plese Enter City",
                    VState: {
                        required: "Please Enter State"

                    },
                    vZip: {
                        required: "Please Enter Password"
                    },
                    vCountryId: {
                        required: "Please Enter Country"

                    },
                    iEndDate:{
                        required: "Please Select Expire Date"
                    },
                    fAmountPaid: {
                        required: "Please Enter Amount",
                        number:"Enter Only Digits"

                    },
                    eStatus: "Please Select Status",
                    plantype:"Please Select Plan Type"

                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#admin_form').submit();
        });
    });
</script>
