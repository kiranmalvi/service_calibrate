<?php
$USER_ID = $_SESSION['SC_LOGIN']['USER']['iUserId'];

include_once($inc_class_path . 'retailer_detail.class.php');
$retailer_detail = new retailer_detail();


if((isset($_REQUEST['type'])&&$_REQUEST['type']!=="")) {
    $UID=$_REQUEST['type'];

}
else {
    $UID=$USER_ID;
}

$QR = $retailer_detail->get_detail_by_retailer($UID);

include_once($inc_class_path . 'info.class.php');
$info = new info();

$info_icon = $info->select_report_info("QRcode_web");


if(isset($_REQUEST['t']))
{
    $type=$_REQUEST['t'];
}
else
{
    $type="verticle";
}



if($type=="verticle")
{
    $vdisplay='style="display: "';
    $hdisplay='style="display: none"';
}
else
{
    $vdisplay='style="display: none"';
    $hdisplay='style="display: "';
}

if($type=="verticle") {
    $print = "'vertical_view_print'";
}
else
{
    $print = "'horizontal_print'";
}
?>
<? $Image = $QR[0]['vQRImage'];
$IMAGE = (file_exists($qr_image_path . $Image)) ? $qr_image_url . $Image : $admin_url . 'images/logo_default.png'; ?>


    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-9">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php">Dashboard</a>
                        </li>
                         <?php if(isset($_SESSION['SC_LOGIN']['ADMIN'])){?>
                            <li>
                                <a class="" href="index.php?file=q-qrcodelist">Print / View QR Code</a>
                            </li>
    <?}?>
                        <li>
                            <a class="current" href="javascript:;">QR Code</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 pull-right text-right">
                    <div class="form-group">
                        <a href="#myModal2" name="SearchReport" data-toggle="modal" class="btn btn-success1"
                           id="help_show" style="display: none">Help (?)</a>
                    </div>
                </div>                </div>



            <div class="row">

                <div class="col-md-12">
                    <header class="panel-heading tab-bg-dark-navy-blue">
                        <ul class="nav nav-tabs">
                            <?php $FACTIVE = ($type=='verticle') ? 'active' : '';?>
                            <li class="<?php echo $FACTIVE; ?>">
                                <a data-toggle="tab" href="#verticle" onclick="location.href='index.php?file=q-qrcode&t=verticle&type=<?php echo $UID;?>'">
                                    Option-1
                                </a>
                            </li>
                            <?php $FACTIVE = ($type=='horizontal') ? 'active' : '';?>
                            <li class="<?php echo $FACTIVE; ?>">
                                <a data-toggle="tab" href="#horizontal" onclick="location.href='index.php?file=q-qrcode&t=horizontal&type=<?php echo $UID;?>'">
                                    Option-2
                                </a>
                            </li>

                        </ul>
                    </header>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <center class="print_text_button">
                            <button id="addbutton" type="button" class="btn-success_print_text"
                                    onclick="printContent(<?php echo $print;?>)">Print
                            </button>
                        </center>
                    </div>
                </div>
                <div id="verticle" class="tab-pane" <?php echo $vdisplay;?> >
                    <div class="row">
                        <section class="panel">
                            <div class="panel-body">

                                <!-- <div class="col-md-12">
                                     <h4 class="modal-title btn-success1">
                                         <center style="display:none">Vertical View</center>
                                     </h4>
                                 </div>
-->

                                <br/><br>
                                <div id="vertical_view_print">
                                    <div class="vertical-dwnld-main" >
                                        <img src="<? echo $admin_url ?>middle/qrcode/vertical.png" alt="vertical">

                                        <div class="vertical-dynamic-data-main" style="position:absolute; top: 284px; right:0; left:0; width: 180px;margin: 0 auto;">
                                            <img src="<?php echo $IMAGE ?>" alt="qr-code" style="width:100%;">

                                            <p style="font-size: 22px"> <? echo $QR[0]['vOriginal_code']; ?></p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                    </div>
        </section>
        </div>
        </div>
        <div id="horizontal" class="tab-pane" <?php echo $hdisplay;?>>

            <div class="row">

                <section class="panel">
                    <div class="panel-body">
                        <div class="col-md-12">
                        </div><br>
                        <div id="horizontal_print">
                            <div class="horizontal-dwnld-main">
                                <img src="<? echo $admin_url ?>middle/qrcode/horizontal.png" alt="vertical">

                                <div class="horizontal-dynamic-data-main" style="position:absolute; top: 290px; right:0; left:0; width: 180px;margin: 0 auto;">
                                    <img src="<?php echo $IMAGE ?>" alt="qr-code" style="width:100%;">

                                    <p style="font-size: 22px"> <? echo $QR[0]['vOriginal_code']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
    </section>
    </div>
    </div>
    </div>
    </section>
    </section>

    <!--                  rint">-->
    <!--                            <div class="col-md-8">-->
    <!--                                <img src="--><?php //echo $admin_url . 'images/logo.png'; ?><!--" alt=""/>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">Easy Steps,</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 1 Download Free app,<span-->
    <!--                                        style="font-size: 20px;font-style:Bold">"Service Calibrate"</span></label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 2 Register For <span-->
    <!--                                        style="font-size: 20px;font-style:Bold">Free</span> & Start measuring-->
    <!--                                    service!</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 3 Unable to Scan? Enter digits below QR Code-->
    <!--                                    into Service Calibrate app</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">Contact us :--->
    <!--                                    feedback@servicecalibrate.com</label>-->
    <!---->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                --><?// $Image = $QR[0]['vQRImage'] ?>
    <!--                                --><?php //$IMAGE = (file_exists ($qr_image_path . $Image)) ? $qr_image_url . $Image : $admin_url . 'images/logo_default.png'; ?>
    <!--                                <img src="--><?php //echo $IMAGE; ?><!--" alt=""/>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <h2 style="font-size: 30px;font-style:Bold">&nbsp;&nbsp;Please Scan Here</h2>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-5 control-label"><span style="font-size: 20px;font-style:Bold">www.servicecalibrate.com</span>-->
    <!--                                </label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">All copyright reserved Service-->
    <!--                                    Calibrate,inc.</label>-->
    <!---->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                        <div class="col-md-5">-->
    <!--                            <center>-->
    <!--                                <button id="addbutton" type="button" class="btn btn-success1"-->
    <!--                                        onclick="printContent('horizontal_print')">Print-->
    <!--                                </button>-->
    <!--                            </center>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="col-md-6">-->
    <!---->
    <!--                        <div id="vertical_view_print">-->
    <!--                            <div class="col-md-12">-->
    <!--                                <h4 class="modal-title btn-success1">-->
    <!--                                    <center>Vertical View</center>-->
    <!--                                </h4>-->
    <!--                            </div>-->
    <!--                            <br/><br>-->
    <!---->
    <!--                            <div class="col-md-8">-->
    <!--                                <img src="--><?php //echo $admin_url . 'images/logo.png'; ?><!--" alt=""/>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                --><?// $Image = $QR[0]['vQRImage'] ?>
    <!--                                --><?php //$IMAGE = (file_exists ($qr_image_path . $Image)) ? $qr_image_url . $Image : $admin_url . 'images/logo_default.png'; ?>
    <!--                                <img src="--><?php //echo $IMAGE; ?><!--" alt=""/>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <h2 style="font-size: 30px;font-style:Bold">Please Scan Here<br/><br></h2>-->
    <!--                            </div>-->
    <!---->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">Easy Steps,</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 1 Download Free app, <h5-->
    <!--                                        style="font-size: 20px;font-style:Bold">"Service Calibrate"</h5></label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 2 Register For <span-->
    <!--                                        style="font-size: 20px;font-style:Bold">Free</span> & Start measuring-->
    <!--                                    service!</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 3 Unable to Scan? Enter digits below QR Code-->
    <!--                                    into Service Calibrate app</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">Contact us :--->
    <!--                                    feedback@servicecalibrate.com</label>-->
    <!---->
    <!--                            </div>-->
    <!--                            <br><br>-->
    <!---->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">All copyright reserved Service-->
    <!--                                    Calibrate,inc.</label>-->
    <!---->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                        <div class="col-md-5">-->
    <!--                            <center>-->
    <!--                                <button id="addbutton" type="button" class="btn btn-success1"-->
    <!--                                        onclick="printContent('vertical_view_print')">Print-->
    <!--                                </button>-->
    <!--                            </center>-->
    <!--                        </div>-->
    <!---->
    <!--
    <!---->

    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog-brodcast">
            <div class="modal-content">
                <div class="modal-header red-light-bg">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Help</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="frmadd_view_promotion1" name="frmadd12">
                                <center>
                                    <img src="<? echo $dafault_image_logo ?>" height="70px" width="70px"></center>
                                <center>

                                    <center><h2>QR Code</h2></center>
                                    <?echo   $info_icon[0]['tDescription']?>
                            </form>
                            </br>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include_once($admin_path . 'js_datatable.php');
?>