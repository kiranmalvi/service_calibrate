<?
include_once($inc_class_path . 'retailer_detail.class.php');
$retailer_detail = new retailer_detail();
$iId=$_REQUEST['iId'];
$qrcode=$retailer_detail->qr_code_detail_with_user($iId);

?>


<section id="main-content">
    <section class="wrapper">

        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>
                    <li>

                        <a href="index.php?file=q-qrcodelist">
                            Print / View QR code</a>
                    </li>
                    <li>

                        <a class="current" href="javascript:;">
                        Edit QR Code
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            Edit QR Code
                        </h4>
                    </header>

                    <div class="panel-body">


                        <form class="form-horizontal bucket-form" method="post" name="frm_qr" id="frm_qr"
                              action="index.php?file=q-qrcode_a" enctype="multipart/form-data">

                            <input type="hidden" name="mode" id="mode" value="add">
                            <input type="hidden" name="iId" id="iId" value="<?php echo $iId;?>">

                            <div class="form-group">
                                <label class="control-label col-md-3">QR Code Image</label>
                                <div class="col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <?php
                                        $Image = $qrcode[0]['vQRImage'];

                                         $IMAGE = (file_exists($qr_image_path . $Image)) ? $qr_image_url . $Image : $admin_url . 'images/logo_default.png';?>
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="<?php echo $IMAGE; ?>" alt="" height="150" width="150">
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" id="lblend"><span class="red"> *</span> Original Code</label>


                                <div class="col-md-8">
                                    <input class="form-control"
                                           type="text" name="iEndDate" id="iEndDate"
                                           value="<?php echo $qrcode[0]['vOriginal_code'];?>" readonly/>

                                </div>



                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label" id="lblend"><span class="red"> *</span> Account Name</label>


                                <div class="col-md-8">
                                    <input class="form-control"
                                           type="text" name="iEndDate" id="iEndDate"
                                           value="<?php echo $qrcode[0]['account_name'];?>" readonly/>


                                </div>



                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" id="lblend"><span class="red"> *</span> User Name </label>


                                <div class="col-md-8">
                                    <input class="form-control"
                                           type="text" name="iEndDate" id="iEndDate"
                                           value="<?php echo $qrcode[0]['fname']." ".$qrcode[0]['lname'];?>" readonly/>


                                </div>



                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label" id="lblend"><span class="red"> *</span> New code </label>

                                <div class="col-md-8">
                                    <input class="form-control"
                                           type="text" name="newcode" id="newcode" onchange="return get_qrcode()"/>

                                    <div id="error_msg">

                                    </div>
                                </div>

                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                        <button id="addbutton1"  type="submit" class="btn btn-success1" >Update</button>

                                    <a href="index.php?file=q-qrcodelist" class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    </section>
                </div>
            </div>
        </section>
    </section>

<script>
function get_qrcode()
{
    var code=document.getElementById("newcode").value;

    $.ajax({
        url: 'index.php?file=q-qrcode_a',
        type: 'POST',
        data: {"code": code, "mode": 'qr_code'},
        success: function (result) {
            console.log(result);
            var data = JSON.parse(result);
            console.log(data);
            if(data.ACTION == "fail")
            {
                $('#error_msg').text(data.MSG);
                $('#addbutton1').attr('disabled','disabled');
                $('#error_msg').css("color", "#a94442");
                return false;
            }
            if(data.ACTION == "success")
            {
                $('#error_msg').text(data.MSG);
                $('#addbutton1').removeAttr('disabled');
                return true;
            }



        }

    });

}
    </script>
<?
include_once($admin_path . 'js_form.php');
?>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#frm_qr');

            $('#frm_qr').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    newcode: {
                        required: true,
                        minlength:10,
                        maxlength:10
                    }

                },
                messages: {
                    newcode:{
                        required:"Please Enter New QR Code",
                        minlength:"Please Enter Minimum 10 characters",
                        maxlength:"Please Enter Maximum 10 characters"
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frm_qr) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton1').click(function () {
            $('#frm_qr').submit();
        });
    });
</script>
