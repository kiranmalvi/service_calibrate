<?
include_once($inc_class_path . 'user.class.php');
$userObj = new user();
include_once($inc_class_path . 'retailer_detail.class.php');
$retailer_detail = new retailer_detail();
//$report_file_name1 = 'contacts';
//unset($_SESSION[$report_file_name1]);
//echo $utype;


    // echo "ho";
     $sql = "select u.iUserId,u.iUserTypeId,ur.vOriginal_code as code,u.vStoreUniqueId as accountname,u.vStoreName as storename,ur.vQRImage as images,u.*,ur.* from retailer_detail ur  left join  user u on ur.iUserId=u.iUserId  where u.iUserTypeId in(4,5,6) and u.eStatus='1'";
    $select_retailer = $obj->select($sql);
    // pr($select_retailer);


//pr($select_retailer);

$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];
?>
<section id="main-content">
    <section class="wrapper">

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php"> Dashboard</a>
                    </li>
                    <li>
                        <a class="current" href="javascript:;">Print / View QR Code</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Table Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Table Section Header Starts -->
                    <header class="panel-heading">
                        Print/View QR Code

                    </header>
                    <!-- Table Section Header Ends -->

                    <!-- Table Section Body Starts -->
                    <div class="panel-body">
                        <div class="adv-table">
                            <form name="qr_frm" id="qr_frm" method="post" action="">
                                <input type="hidden" name="mode" id="mode" value="">
                                <input type="hidden" name="delete_type" value="multi_delete">
                                <table class="display table table-bordered table-striped" id="qr_list">
                                    <!-- Changes HERE Start -->
                                    <thead>
                                    <tr>



                                            <td style="width:160px">Action</td>
                                        <td>Chain / Group  Name</td>
                                        <td>Account Name</td>
                                        <td>Account #</td>
                                        <td>QR Image</td>
                                        <td>Original Code</td>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php for($i=0;$i<count($select_retailer);$i++)
                                    {
                                        $Image = $select_retailer[$i]['images'];
                                        $IMAGE = (file_exists($qr_image_path . $Image)) ? $qr_image_url . $Image : $admin_url . 'images/logo_default.png';
                                    ?>
                                        <tr>

                                                <td style="width:160px">
                                                  <? if ($atype !="R") { ?>
                                                    <a class="btn btn-success btn-xs edit-delete-btn"
                                                       href="index.php?file=q-qredit&iId=<?php echo $select_retailer[$i]['iUserId'] ?>"><i
                                                            class="fa fa-edit"></i> Edit</a>

<?php  } ?>
<!--                                                        <a class="btn btn-danger btn-xs"-->
<!--                                                           href="#">View</a>-->
                                                    <a class="btn btn-info btn-xs edit-delete-btn"
                                                       href="index.php?file=q-qrcode&type=<?php echo $select_retailer[$i]['iUserId'] ?>">
                                                        Print
                                                    </a>

                                                </td>
                                            <td>
                                                    <?php   $p_id=$select_retailer[$i]['iParentId'];
                                                    if($p_id=='0')
                                                    {
                                                        echo $select_retailer[$i]['accountname'];
                                                    }
                                                    else {
                                                        $parentuser=$userObj->getStoreName($p_id);


                                                 echo $parentuser[0]['vStoreUniqueId'];

                                                }?>
                                            </td>

                                            <td> <?php echo $select_retailer[$i]['accountname'];?></td>
                                            <td><?php
                                                if( $select_retailer[$i]['storename']!='') {
                                                    echo $select_retailer[$i]['storename'];
                                                }
                                                else
                                                echo "--"?></td>
                                            <td>
                                             <a href="#myModal" onclick=get_popup_value1(<?php echo $select_retailer[$i]['iRetailerDetailId']; ?>)
                                                data-toggle="modal">   <img src="<?php echo $IMAGE;?>" height="40px"
                                                     width="50px">
                                                 </a>
                                            </td>
                                            <div id="div_<?php echo $select_retailer[$i]['iRetailerDetailId']; ?>" style="display: none">
                                                <div class="view-prmotn-mi">

                                                    <img src="<?php echo $IMAGE; ?>" alt=""
                                                         class="vw-prmtn-prdct-img">


                                                </div>
                                            </div>
                                            <td>
                                                <?php echo $select_retailer[$i]['code'];?>
                                            </td>
                            </tr>
                                    <?php }?>

                                    </tbody>
                                    <!-- Changes HERE Ends -->
                                </table>
                            </form>
                        </div>
                    </div>
                    <!-- Table Section Body Ends -->

                </section>
            </div>
        </div>
        <!-- Table Section Ends -->

    </section>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog-brodcast">
        <div class="modal-content">
            <div class="modal-header red-light-bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">QR Image View</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="frmadd_view_QR" name="frmadd">


                        </form>
                        </br>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#qr_list').dataTable({


        });
    });
</script>
<script>

    function get_popup_value1(id) {
        $('#frmadd_view_QR').html($('#div_' + id).html());
        console.log($('#div_' + id).html());
    }

</script>

<?php

include_once($admin_path . 'js_datatable.php');
?>
