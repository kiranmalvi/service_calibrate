<style>
    .horizontal-dwnld-main {
        position: relative;
        text-align: center;
    }

    .horizontal-dwnld-main img {
        width: 100%;
    }

    .horizontal-dynamic-data-main {
        height: 100px;
        position: absolute;
        left: 50%;
    }

    .horizontal-dynamic-data-main img {
        width: 100%;
        height: auto;
    }
</style>

<style>
    .vertical-dwnld-main {
        position: relative;
        text-align: center;
    }

    .vertical-dwnld-main img {
        width: 100%;
    }

    .vertical-dynamic-data-main {
        height: 100px;
        position: absolute;
        left: 50%;
    }

    .vertical-dynamic-data-main img {
        width: 100%;
        height: auto;
    }
</style>

<?php
$USER_ID = $_SESSION['SC_LOGIN']['USER']['iUserId'];

include_once($inc_class_path . 'retailer_detail.class.php');
$retailer_detail = new retailer_detail();
$QR = $retailer_detail->get_detail_by_retailer($USER_ID);

if(isset($_REQUEST['t']))
{
    $type=$_REQUEST['t'];
}
else
{
    $type="verticle";
}


if($type=="verticle")
{
    $vdisplay='style="display: "';
    $hdisplay='style="display: none"';
}
else
{
    $vdisplay='style="display: none"';
    $hdisplay='style="display: "';
}
?>
<? $Image = $QR[0]['vQRImage'] . '.png';
$IMAGE = (file_exists($qr_image_path . $Image)) ? $qr_image_url . $Image : $admin_url . 'images/logo_default.png'; ?>


<section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php">Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="javascript:;">Qr Code</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">

                <div class="col-md-12">
                    <header class="panel-heading tab-bg-dark-navy-blue">
                        <ul class="nav nav-tabs">
                            <?php $FACTIVE = ($type=='verticle') ? 'active' : '';?>
                            <li class="<?php echo $FACTIVE; ?>">
                                <a data-toggle="tab" href="#verticle" onclick="location.href='index.php?file=q-qrcode&t=verticle'">
                                    Option-1
                                </a>
                            </li>
                            <?php $FACTIVE = ($type=='horizontal') ? 'active' : '';?>
                            <li class="<?php echo $FACTIVE; ?>">
                                <a data-toggle="tab" href="#horizontal" onclick="location.href='index.php?file=q-qrcode&t=horizontal'">
                                    Option-2
                                </a>
                            </li>

                        </ul>
                    </header>
                </div>
                <div id="verticle" class="tab-pane" <?php echo $vdisplay;?> >
                    <div class="row">
                        <section class="panel">
                            <div class="panel-body">

                                <!-- <div class="col-md-12">
                                     <h4 class="modal-title btn-success1">
                                         <center style="display:none">Vertical View</center>
                                     </h4>
                                 </div>
-->

                                <br/><br>
                                <div id="vertical_view_print">
                                    <div class="vertical-dwnld-main">
                                        <img src="<? echo $admin_url ?>middle/qrcode/vertical.jpeg" alt="vertical">

                                        <div class="vertical-dynamic-data-main">
                                            <img src="<?php echo $IMAGE ?>" alt="qr-code">

                                            <p style="font-size: 24px"> <? echo $QR[0]['vOriginal_code']; ?></p>
                                        </div>
                                    </div>

                                </div>
                                </div>
                                <div class="col-md-12">
                                    <center>
                                        <button id="addbutton" type="button" class="btn btn-success1"
                                                onclick="printContent('vertical_view_print')">Print
                                        </button>
                                    </center>
                                </div>

                            </div>
                        </section>
                    </div>
                </div>
                <div id="horizontal" class="tab-pane" <?php echo $hdisplay;?>>

                    <div class="row">

                        <section class="panel">
                            <div class="panel-body">
                                <div class="col-md-12">
                                </div><br>
                                <div id="horizontal_print">
                                    <div class="horizontal-dwnld-main">
                                        <img src="<? echo $admin_url ?>middle/qrcode/horizontal.jpeg" alt="vertical">

                                        <div class="horizontal-dynamic-data-main">
                                            <img src="<?php echo $IMAGE ?>" alt="qr-code">

                                            <p style="font-size: 24px"> <? echo $QR[0]['vOriginal_code']; ?></p>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-12">
                                        <center>
                                            <button id="addbutton" type="button" class="btn btn-success1"
                                                    onclick="printContent('horizontal_print')">Print
                                            </button>
                                        </center>
                                    </div></div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <!--                  rint">-->
    <!--                            <div class="col-md-8">-->
    <!--                                <img src="--><?php //echo $admin_url . 'images/logo.png'; ?><!--" alt=""/>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">Easy Steps,</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 1 Download Free app,<span-->
    <!--                                        style="font-size: 20px;font-style:Bold">"Service Calibrate"</span></label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 2 Register For <span-->
    <!--                                        style="font-size: 20px;font-style:Bold">Free</span> & Start measuring-->
    <!--                                    service!</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 3 Unable to Scan? Enter digits below QR Code-->
    <!--                                    into Service Calibrate app</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">Contact us :--->
    <!--                                    feedback@servicecalibrate.com</label>-->
    <!---->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                --><?// $Image = $QR[0]['vQRImage'] ?>
    <!--                                --><?php //$IMAGE = (file_exists ($qr_image_path . $Image)) ? $qr_image_url . $Image : $admin_url . 'images/logo_default.png'; ?>
    <!--                                <img src="--><?php //echo $IMAGE; ?><!--" alt=""/>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <h2 style="font-size: 30px;font-style:Bold">&nbsp;&nbsp;Please Scan Here</h2>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-5 control-label"><span style="font-size: 20px;font-style:Bold">www.servicecalibrate.com</span>-->
    <!--                                </label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">All copyright reserved Service-->
    <!--                                    Calibrate,inc.</label>-->
    <!---->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                        <div class="col-md-5">-->
    <!--                            <center>-->
    <!--                                <button id="addbutton" type="button" class="btn btn-success1"-->
    <!--                                        onclick="printContent('horizontal_print')">Print-->
    <!--                                </button>-->
    <!--                            </center>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="col-md-6">-->
    <!---->
    <!--                        <div id="vertical_view_print">-->
    <!--                            <div class="col-md-12">-->
    <!--                                <h4 class="modal-title btn-success1">-->
    <!--                                    <center>Vertical View</center>-->
    <!--                                </h4>-->
    <!--                            </div>-->
    <!--                            <br/><br>-->
    <!---->
    <!--                            <div class="col-md-8">-->
    <!--                                <img src="--><?php //echo $admin_url . 'images/logo.png'; ?><!--" alt=""/>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                --><?// $Image = $QR[0]['vQRImage'] ?>
    <!--                                --><?php //$IMAGE = (file_exists ($qr_image_path . $Image)) ? $qr_image_url . $Image : $admin_url . 'images/logo_default.png'; ?>
    <!--                                <img src="--><?php //echo $IMAGE; ?><!--" alt=""/>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <h2 style="font-size: 30px;font-style:Bold">Please Scan Here<br/><br></h2>-->
    <!--                            </div>-->
    <!---->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">Easy Steps,</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 1 Download Free app, <h5-->
    <!--                                        style="font-size: 20px;font-style:Bold">"Service Calibrate"</h5></label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 2 Register For <span-->
    <!--                                        style="font-size: 20px;font-style:Bold">Free</span> & Start measuring-->
    <!--                                    service!</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label"># 3 Unable to Scan? Enter digits below QR Code-->
    <!--                                    into Service Calibrate app</label>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">Contact us :--->
    <!--                                    feedback@servicecalibrate.com</label>-->
    <!---->
    <!--                            </div>-->
    <!--                            <br><br>-->
    <!---->
    <!--                            <div class="col-md-12">-->
    <!--                                <label class="col-md-8 control-label">All copyright reserved Service-->
    <!--                                    Calibrate,inc.</label>-->
    <!---->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                        <div class="col-md-5">-->
    <!--                            <center>-->
    <!--                                <button id="addbutton" type="button" class="btn btn-success1"-->
    <!--                                        onclick="printContent('vertical_view_print')">Print-->
    <!--                                </button>-->
    <!--                            </center>-->
    <!--                        </div>-->
    <!---->
    <!--
    <!---->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    $(window).on('resize', function () {
        var imgheight = $(".horizontal-dwnld-main img").height();
        var qrcodeheight = imgheight * 0.3136;
        var qrcodetop = imgheight * 0.4245;
        var qrcodeleft = -qrcodeheight / 2;

        $(".horizontal-dynamic-data-main").css({"width": qrcodeheight, "top": qrcodetop, "margin-left": qrcodeleft});
    });
    $(document).ready(function () {
        var imgheight = $(".horizontal-dwnld-main img").height();
        var qrcodeheight = imgheight * 0.3136;
        var qrcodetop = imgheight * 0.4245;
        var qrcodeleft = -qrcodeheight / 2;

        $(".horizontal-dynamic-data-main").css({"width": qrcodeheight, "top": qrcodetop, "margin-left": qrcodeleft});
    });
</script>

<script>
    $(window).on('resize', function () {
        var imgheight = $(".vertical-dwnld-main img").height();
        var qrcodeheight = imgheight * 0.1867;
        var qrcodetop = imgheight * 0.3178;
        var qrcodeleft = -qrcodeheight / 2;

        $(".vertical-dynamic-data-main").css({"width": qrcodeheight, "top": qrcodetop, "margin-left": qrcodeleft});
    });
    $(document).ready(function () {
        var imgheight = $(".vertical-dwnld-main img").height();
        var qrcodeheight = imgheight * 0.1867;
        var qrcodetop = imgheight * 0.3178;
        var qrcodeleft = -qrcodeheight / 2;

        $(".vertical-dynamic-data-main").css({"width": qrcodeheight, "top": qrcodetop, "margin-left": qrcodeleft});
    });
</script>

<?php
include_once($admin_path . 'js_datatable.php');
?>