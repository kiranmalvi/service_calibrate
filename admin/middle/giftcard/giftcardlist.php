<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 1/4/15
 * Time: 11:26 AM
 */
include_once($inc_class_path . 'gift_card.class.php');


$gift_card_Obj = new gift_card();
//$db_res = $coupon->select();
$db_res = $gift_card_Obj->giftcardlist();



/*$DATA = array('name' => 'chintan');
echo $generalfuncobj->get_SC_SEND_mail('forgot_password', $DATA, 'chintan.mind@gmail.com', 'test');

$DATA = array('name' => 'chintan');
echo $generalfuncobj->get_SC_SEND_mail('register', $DATA, 'chintan.mind@gmail.com', 'test');*/
/*
$DATA = array('name' => 'chintan');
echo $generalfuncobj->get_SC_SEND_mail('feedback', $DATA, 'chintan.mind@gmail.com', 'test');
exit;*/

$MODULE = 'Gift Card';
$ADD_LINK = 'index.php?file=gc-giftcardadd';
$ACT_LINK = 'index.php?file=gc-giftcard_a';
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];
?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php">Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="javascript:;"> <?php echo $MODULE; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <!-- Table Section Header Starts -->
                        <header class="panel-heading">
                            Gift Card
                            
                        <span class=" pull-right header-btn">
<?php if($atype != "R"){?>
                        <a class="btn btn-success1"
                           href="index.php?file=gc-giftcardadd">
                            Add New Gift Card
                        </a>

                                <button class="btn btn-success1" onclick="delete_record()">
                                    DELETE
                                </button>
<?php }?>
                            </span>
                        </header>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <div class="admin-tbl-main">
                                    <table class="display table table-bordered table-striped" id="gift_table">
                                        <!-- Changes HERE Start -->
                                        <thead>
                                        <tr>
<? if ($atype != "R") { ?>
                                            <td><input type="checkbox" onclick="check_all();"></td>
                                            <td>Action</td>
    <?}?>
                                            <td>Name</td>
                                            <td>Points</td>
                                            <td>Description</td>
                                            <td>Date Added</td>
                                            <td>Status</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($db_res as $groupcard) {

                                            ?>
                                            <tr>
                                                <? if ($atype != "R") { ?>
                                                <td>
                                                    <input type="checkbox" name="delete[]"
                                                           value="<?php echo $groupcard['iCardId']; ?>">
                                                </td>
                                                <td>
                                                    <a class="btn btn-success btn-xs edit-delete-btn"
                                                       href="<?php echo $ADD_LINK; ?>&mode=update&iId=<?php echo $groupcard['iCardId']; ?>">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>

                                                    <a class="btn btn-danger btn-xs edit-delete-btn"
                                                       href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $groupcard['iCardId']; ?>&delete_type=single_delete">
                                                        <i class="fa fa-edit"></i> Delete
                                                    </a>
                                                </td>
                                                <? }?>
                                                <td><?php echo $groupcard['vGift_CardName']; ?></td>
                                                <td><?php
                                                            $word=$groupcard['vPoints'];


                                                       echo  $nombre_format_francais = number_format($word,2);

                                                     ?></td>
                                                <td><?php echo $groupcard['vDescription']; ?></td>
                                                <td><?php echo date('m-d-Y H:i:s', $groupcard['idtAdded']); ?></td>
                                                <td>
                                                    <?php
                                                    if ($groupcard['eStatus'] == '1') {
                                                        echo '<span class="label label-success edit-active">Active</span>';
                                                    } else {
                                                        echo '<span class="label label-danger edit-active">Inactive</span>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                        <!-- Changes HERE Ends -->
                                    </table>
												</div>                                
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>


<?php
include_once($admin_path . 'js_datatable.php');
?>
<script type="text/javascript" src="jquery.numberformatter.js"></script>
<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#gift_table').dataTable({
            	"aaSorting": [[ 3, "desc" ]]
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>
<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#gift_table').dataTable({
            	"aaSorting": [[ 5, "desc" ]],
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });
    </script>
<?php
}?>