<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 1/4/15
 * Time: 11:26 AM
 */
$iId = (isset($_REQUEST['iId'])) ? $_REQUEST['iId'] : '';
$mode = ($iId != '') ? 'edit' : 'add';
if ($iId != '') {
    include_once($inc_class_path . 'gift_card.class.php');

    $gift_card_Obj = new gift_card();
    $db_res = $gift_card_Obj->select($iId);
}


$MODE_TYPE = ucfirst($mode);
$MODULE = 'Gift Card';
$PRE_LINK = 'index.php?file=gc-giftcardlist';
$ACT_LINK = 'index.php?file=gc-giftcard_a';

?>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- Breadcrumbs Starts -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="index.php">Dashboard</a>
                    </li>
                    <li>
                        <a href="<?php echo $PRE_LINK; ?>"><?php echo $MODULE; ?></a>
                    </li>
                    <li>
                        <a class="current" href="#"><?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Breadcrumbs Ends -->

        <!-- Form Section Starts -->
        <div class="row">
            <div class="col-sm-12">
                <section class="panel">

                    <!-- Form Section Header Starts -->
                    <header class="panel-heading red-bg">
                        <h4 class="gen-case">
                            <?php echo $MODE_TYPE . ' ' . $MODULE; ?> Detail
                        </h4>
                    </header>
                    <!-- Form Section Header Ends -->

                    <!-- Form Section Body Starts -->
                    <div class="panel-body">

                        <form class="form-horizontal bucket-form" method="post" name="giftcard_form" id="giftcard_form"
                              action="<?php echo $ACT_LINK; ?>" enctype="multipart/form-data">

                            <input type="hidden" name="mode" value="<?php echo $mode; ?>">
                            <input type="hidden" name="iCardId" id="iCardId"
                                   value="<?php echo $db_res[0]['iCardId']; ?>">

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Gift Card Name :</label>

                                <div class="col-md-8">
                                    <input type="text" name="vGift_CardName" id="vGift_CardName" class="form-control"
                                           value="<?php echo $db_res[0]['vGift_CardName']; ?>" placeholder="Gift Card Name ">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Points :</label>

                                <div class="col-md-8">
                                    <input type="text" name="vPoints" id="vPoints" class="form-control"
                                           value="<?php echo $db_res[0]['vPoints']; ?>" placeholder="Points">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Gift Card Description :</label>

                                <div class="col-md-8">
                                    <textarea name="vDescription" id="vDescription"
                                              class="form-control"><?php echo $db_res[0]['vDescription']; ?>
                                    </textarea>
                                </div>
                            </div>

                            <!--<div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Expire Date :</label>

                                <div class="col-md-8">
                                    <input class="form-control form-control-inline input-medium default-date-picker"
                                           type="text" name="iExpireDate" id="iExpireDate"
                                           value="<?php /*echo (isset($db_res[0]['iExpireDate'])) ? date('d-m-Y', $db_res[0]['iExpireDate']) : ''; */?>"/>
                                </div>
                            </div>
-->
                            <div class="form-group">
                                <label class="col-md-2 control-label"><span class="red"> *</span> Status :</label>

                                <div class="col-md-8">
                                    <select class="form-control" name="eStatus" id="eStatus">
                                        <option selected disabled>- Select Status -</option>
                                        <option
                                            value="1" <?php echo($db_res[0]['eStatus'] == '1' ? 'selected' : ''); ?>>
                                            Active
                                        </option>
                                        <option
                                            value="0" <?php echo($db_res[0]['eStatus'] == '0' ? 'selected' : ''); ?>>
                                            Inactive
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button id="addbutton" type="submit" class="btn btn-success1"><?php if ($mode == "add") {
                                            echo "Add";
                                        } else {
                                            echo "Update";
                                        } ?></button>
                                    <a href="<?php echo $PRE_LINK; ?>" class="btn btn-default1">Back</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- Form Section Body Starts -->

                </section>
            </div>
        </div>
        <!-- Form Section Ends -->

        <!-- page end-->
    </section>
</section>
<?
include_once($admin_path . 'js_form.php');
?>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    var FormAdminValidator = function () {
        // function to initiate Validation Sample 1
        var temp = 0;
        var runValidator1 = function () {
            var form1 = $('#giftcard_form');

            $('#giftcard_form').validate({

                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                },
                ignore: "",
                rules: {
                    vGift_CardName: {
                        required: true
                    },
                    vPoints: {
                        required: true
                    },
                    vDescription: {
                        required: true
                    },
                    eStatus: {
                        required: true
                    }

                },
                messages: {
                    vGift_CardName: "Please Enter Gift Card Name",
                    vPoints: "Please Enter Gift Card Points",
                    vDescription: "Please Enter Gift Card Description",
                    eStatus: "Please Select Status"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    //successHandler1.hide();
                    //errorHandler1.show();
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function (frmadd) {
                    successHandler1.show();
                    errorHandler1.hide();
                }

            });

        };

        return {
            //main function to initiate template pages
            init: function () {
                runValidator1();
            }
        };

        //$('#frmadd').submit();
    }();

    //$('#frmadd').submit();
    $(document).ready(function () {
        FormAdminValidator.init();
        $('#addbutton').click(function () {
            $('#coupon_form').submit();
        });
    });
</script>