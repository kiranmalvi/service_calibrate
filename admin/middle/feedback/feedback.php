<?php
/**
 * Created by PhpStorm.
 * User: Foram
 * Date: 6/4/15
 */

include_once($inc_class_path . 'feedback.class.php');

$feedback = new feedback();
$db_res = $feedback->select();

$MODULE = 'Feedback';
$ACTIVE_LINK = 'index.php?file=f-feedback_a&mode=active';
$INACTIVE_LINK = 'index.php?file=f-feedback_a&mode=inactive';
$ACT_LINK = 'index.php?file=f-feedback_a';
$atype = $_SESSION['SC_LOGIN']['ADMIN']['eType'];

?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">

            <!-- Breadcrumbs Starts -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="index.php"> Dashboard</a>
                        </li>
                        <li>
                            <a class="current" href="javascript:;"> <?php echo $MODULE; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs Ends -->

            <!-- Table Section Starts -->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">

                        <!-- Table Section Header Starts -->
                        <header class="panel-heading">
                            <?php echo $MODULE; ?> Listing

                            <? if ($atype != "R") { ?>
                        <span class=" pull-right header-btn">


                             <button class="btn btn-danger" onclick="delete_record()">
                                 DELETE
                             </button>
                            

                        </span>
                            <? } ?>
                        </header>
                        <!-- Table Section Header Ends -->

                        <!-- Table Section Body Starts -->
                        <div class="panel-body">
                            <div class="adv-table">
                                <form name="admin_list" id="listing" method="post" action="<?php echo $ACT_LINK; ?>">
                                    <input type="hidden" name="mode" id="mode" value="">
                                    <input type="hidden" name="delete_type" value="multi_delete">
                                    <div class="admin-tbl-main">
                                    <table class="display table table-bordered table-striped" id="feedback_table">
                                        <!-- Changes HERE Start -->
                                        <thead>
                                        <tr>
                                            <? if ($atype != "R") { ?>
                                            <td width="3%"><input type="checkbox" onclick="check_all();"></td>
                                            <td width="5%">Action</td>
                                            <? } ?>
                                            <td width="10%">User</td>
                                            <td width="10%">Email</td>
                                            <td>Feedback</td>
                                            <!-- <td>Status</td>-->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($db_res as $FEEDBACK) {

                                            ?>
                                            <tr>
                                                <? if ($atype != "R") { ?>
                                                <td>
                                                    <input type="checkbox" name="delete[]"
                                                               value="<?php echo $FEEDBACK['iFeedbackId']; ?>">

                                                </td>
                                                <td>

                                                        <a class="btn btn-danger btn-xs edit-delete-btn"
                                                           href="<?php echo $ACT_LINK; ?>&mode=delete&iId=<?php echo $FEEDBACK['iFeedbackId']; ?>&delete_type=single_delete"><i
                                                                class="fa fa-edit"></i> Delete</a>

                                                    </td><? } ?>
                                                <td><?php echo $FEEDBACK['vName'] ?></td>
                                                <td>
                                                    <a href="mailto:<?php echo $FEEDBACK['vEmail']; ?>" style="text-decoration: underline">      <?php echo $FEEDBACK['vEmail']; ?></a>
                                                </td>
                                                <td>
                                                    <?php echo $FEEDBACK['tFeedback']; ?>
                                                </td>
                                                <!-- <td>
                                                    <?php
                                                /*                                                    if ($FEEDBACK['eStatus'] == '1') {
                                                                                                        echo '<span class="label label-success">Active</span>';
                                                                                                    }
                                                                                                    else if ($FEEDBACK['eStatus'] == '0') {
                                                                                                        echo '<span class="label label-danger">Inactive</span>';
                                                                                                    }
                                                                                                    else {
                                                                                                        echo '<span class="label label-warning">Pending</span>';
                                                                                                    }
                                                                                                    */
                                                ?>
                                                </td>-->
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                        <!-- Changes HERE Ends -->
                                    </table>
												</div>                                
                                </form>
                            </div>
                        </div>
                        <!-- Table Section Body Ends -->

                    </section>
                </div>
            </div>
            <!-- Table Section Ends -->

        </section>
    </section>


<?php

if ($_SESSION['SC_LOGIN']['ADMIN']['eType'] == 'R') {
    ?>
    <script>
        $(document).ready(function () {
            $('#feedback_table').dataTable({
                /*"aoColumnDefs": [
                 {'bSortable': false, 'aTargets': [0]}
                 ]*/
            });
        });
    </script>
<?php
} else {
    ?>
    <script>
        $(document).ready(function () {
            $('#feedback_table').dataTable({
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0, 1]}
                ]
            });
        });
    </script>

<?php
}
include_once($admin_path . 'js_datatable.php');
?>