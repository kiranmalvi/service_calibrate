<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 23/3/15
 * Time: 2:32 PM
 */

//error_reporting(0);

require_once ('include.php');

if (!isset($_SESSION['SC_LOGIN']))
{
    header ('location:'.$site_url.'login.php');
    exit;
}

if (($_SESSION['SC_LOGIN']['USER']['allowlogin']=="no"))
{
    echo $_SESSION['SC_LOGIN']['USER']['allowlogin'];
    header ('location:'.$site_url.'login.php');
    exit;
}


include_once ('header.php');

//$file = $_REQUEST['file'];
//pr($_REQUEST);
$var = explode ("-", $_GET['file']);
$prefix = $var[0];
$script = $var[1];


if ($prefix == "")
    $prefix = "h";
if ($script == "")
    $script = "dashboard";
switch ($prefix) {
    case "cou" :
        $module = "coupon";
        break;
    case "ma" :
        $module = "master";
        break;
    case "b" :
        $module = "broadcast";
        break;
    case "cat" :
        $module = "category";
        break;
    case "cmr" :
        $module = "chart_map_report";
        break;
    case "f" :
        $module = "feedback";
        break;
    case "h" :
        $module = "home";
        break;
    case "hfq" :
        $module = "help_faq_map";
        break;
    case "m" :
        $module = "messages";
        break;
    case "p" :
        $module = "promotions";
        break;
    case "q" :
        $module = "qrcode";
        break;
    case "r" :
        $module = "rating";
        break;
    case "rp" :
        $module = "redeem";
        break;
    case "gc" :
        $module = "giftcard";
        break;
    case "sf" :
        $module = "service_frequency";
        break;
    case "u" :
        $module = "user";
        break;
    case "c" :
        $module = "contact";
        break;
    case "i" :
        $module = "info";
        break;
    case "su" :
        $module = "subuser";
        break;
    case "pl" :
        $module = "plan";
        break;
    case "sc" :
        $module = "admin";
        break;
    case "ad" :
        $module = "advertisement";
        break;
    case "cu" :
        $module = "contact_us";
        break;
        
    case "ln" :
        $module = "linkup";
        break;
    case "un" :
        $module = "unsub";
        break;
    case "pc" :
        $module = "promotionalcode";
        break;
    default :
        $module = "home";
        break;
}

// Add File Path
//echo $script;
$include_script = "middle/" . $module . "/" . $script . ".php";

//Action File Path middle/admin/adminlist.php
$include_script_a = "action/" . $module . "/" . $script . ".php";//action/admin/adminlist.php
?>
<body>

<section id="container">

<?php
include_once ('top.php');
include_once ('menu.php');
//echo $include_script_a;
if (file_exists ($include_script)) {
    include_once ($include_script);
} else {
    include_once ($include_script_a);
}

//include_once ('footer.php');

?>
   
<?php
if (isset($_SESSION[$session_prefix . 'admin_var_msg'])) {
    ?>
    <script>
        $(document).ready(function(){
            $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: '<?php echo (isset($_SESSION[$session_prefix . 'admin_var_msg_title'])) ? $_SESSION[$session_prefix . 'admin_var_msg_title'] : 'Success'; ?>',
                text : '<?php echo $_SESSION[$session_prefix . 'admin_var_msg']; ?>'
                // (bool | optional) if you want it to fade out on its own or just sit there
            });
        });
    </script>

<?php
}

unset($_SESSION[$session_prefix . 'admin_var_msg']);
unset($_SESSION[$session_prefix . 'admin_msg_class']);
unset($obj);
unset($generalfuncobj);
unset($_SESSION[$file_name]);
unset($_SESSION['frnt_payment']);