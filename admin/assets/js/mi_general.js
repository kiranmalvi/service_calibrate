/**
 * Created by chintan on 30/3/15.
 */

function check_all() {
    $('input[name="delete[]"]').each(function () {
        this.checked = this.checked ? false : true;
    });
}

function delete_record() {
    if (confirm("Are You Sure you want to delete records")) {
        $('#mode').val('delete');
        $('#listing').submit();
        return true;
    }
    else {
        return false;
    }
}


function active_record() {
    if (confirm("Are You Sure you want to active records")) {
        $('#mode').val('active');
        $('#listing').submit();
        return true;
    }
    else {
        return false;
    }
}


function inactive_record() {
    if (confirm("Are You Sure you want to inactive records")) {
        $('#mode').val('inactive');
        $('#listing').submit();
        return true;
    }
    else {
        return false;
    }
}