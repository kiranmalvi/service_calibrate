<?php
// to add the city, state country in the database  automatically


require_once ('include.php');
include_once($inc_class_path . 'city.class.php');
$cityObj =new  city();

include_once($inc_class_path . 'state.class.php');
$stateObj =new  state();

include_once($inc_class_path . 'country.class.php');
$countryObj =new  country();


$get_city="SELECT vCity FROM user GROUP BY LOWER(TRIM(vCity))";
$citylist=$obj->select($get_city);


$get_state="SELECT vState FROM user GROUP BY LOWER(TRIM(vState))";
$statelist=$obj->select($get_state);

$get_country="SELECT iCountryId FROM user GROUP BY LOWER(TRIM(iCountryId))";
$countrylist=$obj->select($get_country);


$date=strtotime(date('d-m-Y H:i:s'));
//pr($citylist);

for($i=0;$i<count($countrylist);$i++)
{

    $checkexist=$generalfuncobj->get_unique("country","vName",$countrylist[$i]['iCountryId']);
    if($checkexist)
    {
        $countryObj->setiDtAdded($date);
        $countryObj->setvName($countrylist[$i]['iCountryId']);
        $countryObj->seteStatus("1");
        $countryObj->insert();
    }


}

for($i=0;$i<count($statelist);$i++)
{

    $checkexist=$generalfuncobj->get_unique("state","vName",$statelist[$i]['vState']);
    if($checkexist)
    {
        $state=$statelist[0]['vState'];
         $country="select  c.iCountryId as cid,u.iCountryId,u.vState from user u left join country c on u.iCountryId=c.vName where vState='$state' group by u.iCountryId";
        $countryid=$obj->select($country);
        $stateObj->setiCountryId($countryid[0]['cid']);
        $stateObj->setiDtAdded($date);
        $stateObj->setvName($statelist[$i]['vState']);
        $stateObj->seteStatus("1");
        $stateObj->insert();
    }


}


for($j=0;$j<count($citylist);$j++)
{
    $checkexist=$generalfuncobj->get_unique("city","vName",$citylist[$j]['vCity']);
    if($checkexist)
    {
        $city=$citylist[0]['vCity'];
        $city="select s.iStateId as sid,u.iCountryId,u.vState,u.vCity from user u left join state s on u.vState=s.vName where vCity='$city' group by u.vCity";
        $cityid=$obj->select($city);

        $cityObj->setiStateId($cityid[0]['sid']);
       $cityObj->setiDtAdded($date);
        $cityObj->setvName($citylist[$j]['vCity']);
        $cityObj->seteStatus("1");
        $cityObj->insert();
    }


}
?>