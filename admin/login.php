<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 30/3/15
 * Time: 2:39 PM
 */
require_once('include.php');


if (isset($_SESSION['SC_LOGIN'])) {
    header('location:index.php');
} else {

    include_once('header.php');

    if (isset($_REQUEST['login'])) {
        $email = $_REQUEST['email'];
        $password = base64_encode($_REQUEST['password']);

        $SQL = "SELECT * FROM admin WHERE vEmail = '{$email}' AND vPasswd = '{$password}' AND eStatus = '1'";
        $DATA = $obj->select($SQL);

        if (count($DATA) > 0) {
            $login = array(
                'iAdminId' => $DATA[0]['iAdminId'],
                'vFirstName' => $DATA[0]['vFirstName'],
                'vLastName' => $DATA[0]['vLastName'],
                'vEmail' => $DATA[0]['vEmail'],
                'dtLastLogin' => $DATA[0]['dtLastLogin'],
                'eType' => $DATA[0]['eType'],
            );

            $_SESSION['SC_LOGIN']['ADMIN'] = $login;

            $TIME = ($generalfuncobj->getUserTime($_SESSION['SC_userTimeZone']));
            $UPDATE = "UPDATE admin SET dtLastLogin = '{$TIME}' , vLastLoginIP = '{$_SERVER['REMOTE_ADDR']}' WHERE iAdminId = '{$DATA[0]['iAdminId']}'";
            $obj->sql_query($UPDATE);

            $generalfuncobj->func_set_temp_sess_msg($DATA[0]['vFirstName'] . ' ' . $DATA[0]['vLastName'], null, 'WelCome');
            header('location:index.php');
        } else {
            $error = 'You are not Authorized User';
        }
    }

    ?>

<body class="login-body">

<div class="container">

    <form class="form-signin" method="post" >
        <h2 class="form-signin-heading">
        sign in now
        <?php
    if (isset($error)) {
        echo '<br><br>';
        echo '<span><b>' . $error . '</b></span>';
    }
    ?>

        </h2>

        <div class="login-wrap">
            <div class="user-login-info">
                <input type="text" name="email" class="form-control" placeholder="User ID" autofocus>
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <input class="btn btn-lg btn-login btn-block" type="submit" value="Sign in" name="login">
             
        </div>
      
    </form>

</div>


</body>

</html>

<?php } ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
    function settime()
    {
        var visitortime = new Date();
        var visitortimezone =  -visitortime.getTimezoneOffset() / 60;
        var ajax_url    =   '<?php echo $ajax_url; ?>';
        $.ajax({
            url:ajax_url+"ajax_timezone.php",
            type:'POST',
            data:{"time":visitortimezone},
            success: function (result) {
                console.log(result);
            }
        });
    }
    settime();


</script>
