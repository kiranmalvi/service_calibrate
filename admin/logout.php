<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 30/3/15
 * Time: 3:23 PM
 */
include_once('include.php');
$PREFIX = (isset($_SESSION['SC_LOGIN']['USER'])) ? '../' : '';
unset($_SESSION['SC_LOGIN']);
header('location:' . $PREFIX . 'login.php');
exit;