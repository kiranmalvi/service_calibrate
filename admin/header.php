<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 23/3/15
 * Time: 2:38 PM
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="mind" >
    <link rel="shortcut icon" href="images/favicon.png">

    <title><?php echo $ADMIN_PANEL_TITLE ?></title>

    <link href="<?php echo $admin_url; ?>assets/js/iCheck/skins/minimal/red.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/js/iCheck/skins/square/red.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/js/iCheck/skins/flat/red.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-colorpicker/css/colorpicker.css"/>
    <!--Core CSS -->
    <link href="<?php echo $admin_url; ?>assets/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--dynamic table-->
    <link href="<?php echo $admin_url; ?>assets/js/advanced-datatable/css/demo_page.css" rel="stylesheet"/>
    <link href="<?php echo $admin_url; ?>assets/js/advanced-datatable/css/demo_table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo $admin_url; ?>assets/js/data-tables/DT_bootstrap.css"/>
    <link href="<?php echo $admin_url; ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/css/style-responsive.css" rel="stylesheet"/>


    <!-- Form CSS -->
    <link rel="stylesheet" href="<?php echo $admin_url; ?>assets/css/bootstrap-switch.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-timepicker/css/timepicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-colorpicker/css/colorpicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/bootstrap-datetimepicker/css/datetimepicker.css"/>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/jquery-multi-select/css/multi-select.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $admin_url; ?>assets/js/jquery-tags-input/jquery.tagsinput.css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo $admin_url; ?>assets/js/select2/select2.css"/>


    <!-- Dashboard CSS -->
    <link href="<?php echo $admin_url; ?>assets/js/jvector-map/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/css/clndr.css" rel="stylesheet">
    <!--clock css-->
    <link href="<?php echo $admin_url; ?>assets/js/css3clock/css/style.css" rel="stylesheet">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo $admin_url; ?>assets/js/morris-chart/morris.css">


    <!-- Custom styles for this template -->
    <link href="<?php echo $admin_url; ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo $admin_url; ?>assets/css/style-responsive.css" rel="stylesheet"/>

    <!-- Gritter -->
    <link rel="stylesheet" type="text/css" href="<?php echo $admin_url; ?>assets/js/gritter/css/jquery.gritter.css"/>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="<?php echo $admin_url; ?>assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo $admin_url; ?>assets/js/html5shiv.js"></script>
    <script src="<?php echo $admin_url; ?>assets/js/respond.min.js"></script>

    <!--js Required for date-picker-->
    <!--    <script src="-->
    <?php //echo $admin_url; ?><!--assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"> </script>-->
    <script src="<?php echo $admin_url; ?>assets/js/jquery-1.8.3.min.js"></script>
    <!--    <script src="--><?php //echo $admin_url; ?><!--assets/js/advanced-form.js"> </script>-->

    <!-- -->

    <script src="<?php echo $admin_url; ?>assets/js/mi_general.js"></script>
    
    <script>
        function printContent(el) {
            var restorepage = document.body.innerHTML;
            var printcontent = document.getElementById(el).innerHTML;
            document.body.innerHTML = printcontent;
            window.print();
            document.body.innerHTML = restorepage;
        }
    </script>
</head>
