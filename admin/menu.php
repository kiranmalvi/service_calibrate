    <?php
    /**
     * Created by PhpStorm.
     * User: chintan
     * Date: 23/3/15
     * Time: 4:05 PM
     */
    //echo "hiiiii";
    //pr($script);exit;
    session_start();
    ///pr($_SESSION);exit;
  
    ?>

        <aside>
            <div id="sidebar" class="nav-collapse">
                <!-- sidebar menu start-->
                <div class="leftside-navigation">
                    <ul class="sidebar-menu" id="nav-accordion">

    <?php
    //echo "hiiii... MENU";exit;
    if (isset($_SESSION['SC_LOGIN']['USER'])) {
    	
    	 include_once($inc_class_path . 'message.class.php');
                    $messageObj = new message();
                    $iId=$_SESSION['SC_LOGIN']['USER']['iUserId'];
                    $count=$messageObj->func_count_user_unread_message($iId);

        if($count>0 &&$count<="99")
                    {
                        $count="(".$count.")";
                    }
                    else if($count>99)
                    {
                        $count="(99+)";
                    }
        else
        {
            $count="";
        }
        //pr($_SESSION['SC_LOGIN']['USER']);
        ?>
        <li>
            <a href="index.php?file=su-sudashboard" <?php if ($module == "subuser" && $script == "sudashboard") { ?> class="active" <?php } ?>>
                <i class="fa fa-desktop"></i>
                <span>Dashboard</span>
            </a>

        </li>

        <li class=" ">
            <a href="#"  class="active">
                <i class="fa fa-dashboard"></i>
                <span>Service Reports</span>
            </a>
            <ul class="sub">
                <li>
                    <a href="index.php?file=sf-sf" <?php if ($script == "sf") { ?> class="active" <?php } ?>>
                        Service Frequency
                    </a>
                </li>

                <?php if ($_SESSION['SC_LOGIN']['USER']['iUserTypeId'] != 2 && $_SESSION['SC_LOGIN']['USER']['iUserTypeId'] != 3) { ?>
                    <li>
                        <a href="index.php?file=sf-cs" <?php if ($script == "cs") { ?> class="active" <?php } ?>>
                            Compare Service
                        </a>
                    </li>
                <?php }?>




                <li>
                    <a href="index.php?file=sf-service_schedule" <?php if ($script == "service_schedule"|| $script == "create_schedule"|| $script == "schedulelist") { ?> class="active" <?php } ?>>
                        Service Schedule
                    </a>
                </li>

                <li>
                    <a href="index.php?file=sf-rapid_fire_order" <?php if ($script == "rapid_fire_order") { ?> class="active" <?php } ?>>
                        Rapid Fire Visit
                    </a>
                </li>
                <li>
                    <a href="index.php?file=sf-sales_service" <?php if ($script == "sales_service") { ?> class="active" <?php } ?>>
                        Sales vs. Service
                    </a>
                </li>

                <li>
                    <a href="index.php?file=sf-delivery_report" <?php if ($script == "delivery_report") { ?> class="active" <?php } ?>>
                        Projected Delivery
                    </a>
                </li>
                <?php if ($_SESSION['SC_LOGIN']['USER']['iUserTypeId'] == 2 || $_SESSION['SC_LOGIN']['USER']['iUserTypeId'] == 3) { ?>
                    <li>
                        <a href="index.php?file=sf-history" <?php if ($script == "cs") { ?> class="active" <?php } ?>>
                            History
                        </a>
                    </li>
                <?php }?>

            </ul>
        </li>

        <li>
            <a href="index.php?file=m-messages" <?php if ($module == "messages") { ?> class="active" <?php } ?>>
                <i class="fa fa-envelope"></i>
               <!-- <span id="spanmsg">Messages <p class="message-count"><?php /*echo $count*/?> </p></span>-->
            <span id="spanmsg">Messages <?php echo $count;?> </span>
            </a>

        </li>

        <li>
            <a href="index.php?file=r-rating" <?php if ($module == "rating") { ?> class="active" <?php } ?> >
                <i class="fa fa-star"></i>
                <span>Rating</span>
            </a>
        </li>
       <li>
        <a href="index.php?file=p-promotions" <?php if ($script == "promotions" || $script == "promotion_payment" || $script == "addpromotion") { ?> class="active" <?php } ?>>
            <i class="fa fa-signal"></i>
            <span>View Promotions</span>
        </a>
    </li>
   <?php if($_SESSION['SC_LOGIN']['USER']['iParentId']=="0"){
  if ($_SESSION['SC_LOGIN']['USER']['iUserTypeId'] == 2 || $_SESSION['SC_LOGIN']['USER']['iUserTypeId'] == 3) { ?>   	
   	
    <li>
        <a href="index.php?file=p-promouserlist" <?php if ($script == "promouserlist" || $script == "promouser") { ?> class="active" <?php } ?>>
            <i class="fa fa-quote-left"></i>
            <span>Promotion Rights</span>
        </a>
    </li>
    <?php }}?>
        <li>
            <a href="index.php?file=c-contact" <?php if ($module == "contact") { ?> class="active" <?php } ?>>
                <i class="fa fa-mobile"></i>
                <span>Contact</span>
            </a>
        </li>

        <li>
            <a href="index.php?file=b-broadcast" <?php if ($module == "broadcast") { ?> class="active" <?php } ?>>
                <i class="fa fa-bullhorn"></i>
                <span>Broadcast</span>
            </a>
        </li>

        <li>
            <a href="index.php?file=rp-redeem" <?php if ($module == "redeem") { ?> class="active" <?php } ?>>
                <i class="fa fa-money"></i>    Redeem Point
            </a>
        </li>

        <?php if ($_SESSION['SC_LOGIN']['USER']['iUserTypeId'] != 2 && $_SESSION['SC_LOGIN']['USER']['iUserTypeId'] != 3) { ?>
            <li>
                <a href="index.php?file=q-qrcode" <?php if ($module == "qrcode") { ?> class="active" <?php } ?>>
                    <i class="fa fa-barcode"></i>
                    <span>Print / View QR Code</span>
                </a>
            </li>
        <?php } ?>

        <li >
            <a href="index.php?file=hfq-helpview" <?php if ($module == "help_faq_map") { ?> class="active" <?php } ?> style="display: none">
                <i class="fa fa-comment"></i>
                <span>Help / FAQs / Guide</span>
            </a>
        </li>
    <?php } else if (isset($_SESSION['SC_LOGIN']['ADMIN'])) { ?>
   <li>

                        <a href="index.php" <? if ($module == "home" && $script == "dashboard") { ?> class="active" <? } ?>>
                            <i class="fa fa-desktop"></i>
                            <span>Dashboard</span>
                        </a>

                    </li>
        <?php if (isset($_SESSION['SC_LOGIN']['ADMIN']['eType'])&&$_SESSION['SC_LOGIN']['ADMIN']['eType']=="S") { ?>
        <li>
            <a href="index.php?file=sc-adminlist" <?php if ($module == "admin") { ?> class="active" <?php } ?>>
                <i class="fa fa-user"></i>
                <span>Admin</span>
            </a>
        </li>
            <?}?>

        <li>
            <a href="index.php?file=ma-countrylist" <?php if ($module == "master") { ?> class="active" <?php } ?>>
                 <i class="fa fa-map-marker"></i>
                 <span>Master</span>
            </a>

            <ul class="sub">
                <li>
                    <a href="index.php?file=ma-countrylist" <?php if ($script == "countrylist" OR $script == "countryadd") { ?> class="active" <?php } ?>>
                        Manage Country
                    </a>
                </li>
                <li>
                    <a href="index.php?file=ma-statelist" <?php if ($script == "statelist" OR $script == "stateadd") { ?> class="active" <?php } ?>>
                        Manage State
                    </a>
                </li>
                <li>
                    <a href="index.php?file=ma-citylist" <?php if ($script == "citylist" OR $script == "cityadd") { ?> class="active" <?php } ?>>
                        Manage City
                    </a>
                </li>
             </ul>
        </li>
        <li>
            <a href="index.php?file=sc-adminlist" <?php if ($module == "category") { ?> class="active" <?php } ?>>
                <i class="fa fa-tags"></i>
                <span>Category</span>
            </a>
            <ul class="sub">
                <li>
                    <a href="index.php?file=cat-industrylist" <?php if ($script == "industrylist" OR $script == "industryadd") { ?> class="active" <?php } ?>>
                        Manage Industry
                    </a>
                </li>
                <li>
                    <a href="index.php?file=cat-storelist" <?php if ($script == "storelist" OR $script == "storeadd") { ?> class="active" <?php } ?>>
                        Type of Account
                    </a>
                </li>
                <li>
                    <a href="index.php?file=cat-how_hear_list" <?php if ($script == "how_hear_list" OR $script == "how_hear_add") { ?> class="active" <?php } ?>>
                        How did you hear about us
                    </a>
                </li>
                <li>
                    <a href="index.php?file=cat-titlelist" <?php if ($script == "titlelist" OR $script == "titleadd") { ?> class="active" <?php } ?>>
                        Role
                    </a>
                </li>
                <li>
                    <a href="index.php?file=cat-categorylist" <?php if ($script == "categorylist" OR $script == "categoryadd") { ?> class="active" <?php } ?>>
                        Category
                    </a>
                </li>
            </ul>
        </li>
  <li>
                        <a href="index.php?file=r-admin_rating" <? if ($module == "rating") { ?> class="active" <? } ?> >
                            <i class="fa fa-star"></i>
                            <span>Rating</span>
                        </a>
                    </li>

        <li>
            <a href="index.php?file=i-infolist" <?php if ($module == "info") { ?> class="active" <?php } ?>>
                <i class="fa fa-info"></i>
                <span>Information Icon</span>
            </a>

        </li>
            <li>
                        <a href="index.php?file=pl-planslist" <? if ($module == "plan") { ?> class="active" <? } ?>>
                            <i class="fa fa-dollar"></i>
                            <span>Plan</span>
                        </a>

                    </li>
                    
                    <li>
                        <a href="index.php?file=p-promotionprice" <? if ($module == "promotions") { ?> class="active" <? } ?>>
                            <i class="fa-money"></i>
                            <span>Promotion Price</span>
                        </a>

                    </li>
       
        <li class="sub-menu">
                        <a href="#" <? if ($module == "user" OR $module == "subuser") { ?> class="active" <? } ?>>
                            <i class="fa fa-users"></i>
                            <span>User</span>
                        </a>
                        <ul class="sub">
                         <li>

                                <a href="index.php?file=u-userlist&type=0"<?php echo ($_REQUEST['type'])=="0" ? 'class="active"' : ''?>>
                                  All Users
                                </a>
                            </li>
                            <?php
                            $USER_TYPE = "SELECT iUserTypeId,vName,iParentId FROM user_type WHERE iParentId = 0 AND eStatus = '1'";
                            $QUERY = $obj->select($USER_TYPE);


                            foreach ($QUERY as $DATA) {
                                $SELECTED_ADMIN = ($DATA['iUserTypeId'] == $_REQUEST['type']) ? 'class="active"' : '';
                                $SUB_USER_SQL = "SELECT iUserTypeId,vName,iParentId FROM user_type WHERE iParentId = '{$DATA['iUserTypeId']}' AND eStatus = '1'";
                                $SUB_USER_QUERY = $obj->select($SUB_USER_SQL);
                                if (count($SUB_USER_QUERY) > 0) {
                                    $S = "SELECT iParentId FROM user_type WHERE iUserTypeId = '{$_REQUEST['type']}'";
                                    $S_D = $obj->select($S);
                                    $SUB_U_P = ($S_D[0]['iParentId'] == $DATA['iUserTypeId']) ? 'class="active"' : '';
                                    ?>
                                    <li>
                                        <a href="#" <?php echo $SUB_U_P ?>>
                                            <?php echo ucfirst($DATA['vName']); ?>
                                        </a>
                                        <ul class="sub">
                                            <?php
                                            foreach ($SUB_USER_QUERY as $SUB_DATA) {
                                                @$SUB_U_P_S = ((end(explode('&', $_SERVER['QUERY_STRING']))) == 'type=' . $SUB_DATA['iUserTypeId']) ? 'class="active"' : '';
                                                ?>
                                                <li>
                                                    <a href="index.php?file=u-userlist&type=<?php echo $SUB_DATA['iUserTypeId']; ?>" <?php echo $SUB_U_P_S; ?>>
                                                        <?php echo ucfirst($SUB_DATA['vName']); ?>
                                                    </a>
                                                </li>
                                            <?php
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                <?php
                                } else {
                                    ?>
                                    <li>
                                        <a <?php echo $SELECTED_ADMIN; ?>
                                            href="index.php?file=u-userlist&type=<?php echo $DATA['iUserTypeId']; ?>">
                                            <?php echo ucfirst($DATA['vName']); ?>
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
                            <?php
                            }
                            ?>

                        </ul>
                    </li>
                    
                    <li>
                        <a href="index.php?file=un-unsublist" <? if ($module == "unsub") { ?> class="active" <? } ?>>
                            <i class="fa fa-thumbs-o-down"></i>
                            <span>Unsubscribe User</span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="index.php?file=ln-linkaccount" <? if ($module == "linkup") { ?> class="active" <? } ?>>
                            <i class="fa fa-link"></i>
                            <span>Link Account</span>
                        </a>
                    </li>
          <li>
                        <a href="index.php?file=q-qrcodelist" <? if ($module == "qrcode") { ?> class="active" <? } ?>>
                            <i class="fa fa-barcode"></i>
                            <span>Print / View QR Code</span>
                        </a>
                    </li>
        <li>
                        <a href="index.php?file=b-broadcastlist&type=1" <? if ($module == "broadcast") { ?> class="active" <? } ?>>
                            <i class="fa fa-bullhorn"></i>
                            <span>Broadcast</span>
                        </a>
                    </li>
        <li>
            <a href="#" <?php if ($module == "contact") { ?> class="active" <?php } ?>>
                <i class="fa fa-mobile"></i>
                <span>Contact</span>
            </a>
            <ul class="sub">
                <li>
                    <a href="index.php?file=c-contactus&type=1" <? if ($script == "contactus" AND $_REQUEST['type'] == '1') { ?> class="active" <? } ?>>Retailer</a>
                                <a href="index.php?file=c-contactus&type=2" <? if ($script == "contactus" AND $_REQUEST['type'] == '2') { ?> class="active" <? } ?>>Distributor</a>
                                <a href="index.php?file=c-contactus&type=3" <? if ($script == "contactus" AND $_REQUEST['type'] == '3') { ?> class="active" <? } ?>>Manufacturer</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="index.php?file=gc-giftcardlist" <?php if ($module == "giftcard") { ?> class="active" <?php } ?>>
                <i class="fa fa-gift"></i>
                <span>Gift Card</span>
            </a>
        </li>

        <li>
            <a href="index.php?file=rp-redeemlist_user" <?php if ($module == "redeem") { ?> class="active" <?php } ?>>
                <i class="fa fa-ticket"></i>    Redeem Point
            </a>
        </li>
        <li>
            <a href="index.php?file=f-feedback" <?php if ($module == "feedback") { ?> class="active" <?php } ?>>
                <i class="fa fa-edit"></i>
                <span>Feedback to SC</span>
            </a>
        </li>
         <li>
                        <a href="index.php?file=cu-contact_uslist" <? if ($module == "contact_us") { ?> class="active" <? } ?>>
                            <i class="fa fa-comments-o"></i>
                            <span>Contact Us</span>
                        </a>
                    </li>
                    
                    <li>
                        <a href="index.php?file=pc-promotionalcodelist" <? if ($module == "promotionalcode") { ?> class="active" <? } ?>>
                            <i class="fa fa-credit-card"></i>
                            <span>Promotion Code</span>
                        </a>
         <!--   <li>
                        <a href="index.php?file=ad-advertisementlist" <?/* if ($module == "advertisement") { */?> class="active" <?/* } */?>>
                            <i class="fa fa-barcode"></i>
                            <span>Advertisement</span>
                        </a>

                    </li>-->

        <li>
            <a href="index.php?file=hfq-help_faq_map" <?php if ($module == "help_faq_map") { ?> class="active" <?php } ?>>
                <i class="fa fa-comment"></i>
                <span>Help / FAQs / Guide</span>
            </a>
        </li>

    <?php } else { }?>
                    </ul>
                </div>
                <!-- sidebar menu end-->
            </div>
        </aside>
        
        <script>

    setInterval(
        function () {
            var uid = '<?=$_SESSION['SC_LOGIN']['USER']['iUserId'];?>';
            $.ajax({
                url: 'index.php?file=m-messages_a&uid=' + uid,
                type: 'POST',
                data: {"mode": 'get_unread'},
                success: function (result) {
                    console.log(result);
                    $('#spanmsg').html(result);
                }
            });
        },10 );
</script>
