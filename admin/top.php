<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 23/3/15
 * Time: 2:59 PM
 */


//echo $_SERVER['REQUEST_TIME'];
include_once($inc_class_path . 'user.class.php');
$userObj = new user();
$iId = $_SESSION['SC_LOGIN']['USER']['iUserId'];
$USER = $userObj->select($iId);
$type = $USER[0]['iUserTypeId'];

if (isset($_SESSION['SC_LOGIN']['ADMIN'])) {
    $date = date('m-d-Y', $_SESSION['SC_LOGIN']['ADMIN']['dtLastLogin']);;
} else {
    include_once($inc_class_path . 'user_settings.class.php');
    $user_settingsObj = new user_settings();
    $lastlogin = $user_settingsObj->select_user_setting($iId);
    if ($lastlogin[0]['iDtLogin'] == "0") {
        $date = "Not Login Until";
        $device = "none";
    } else {
        $date = date('m-d-Y', $lastlogin[0]['iDtLogin']);
        $device = $user_settingsObj->geteDeviceType();
    }
}

?>


<!--header start-->
<header class="header fixed-top clearfix">
    <!--logo start-->
    <div class="brand">

        <a href="index.php" class="logo">
            <img src="images/logo.png" alt="">
        </a>

        <div class="sidebar-toggle-box">
            <div class="fa fa-bars"></div>
        </div>
    </div>
    <!--logo end-->

    <?php $utypeId = $_SESSION['SC_LOGIN']['USER']['iUserTypeId'];
    if ($utypeId == 2) {
        ?>
        <div class="col-md-6 col-xs-5" style="color: #ffffff; font-size: 25px; margin-left: 5px; margin-top:1.5%"><b>
                Distributor </b></div>
    <?php } elseif ($utypeId == 3) { ?>
        <div class="col-md-6 col-xs-5" style="color: #ffffff; font-size: 25px; margin-left: 5px; margin-top: 1.5%"><b>
                Manufacturer </b></div>
    <?php } elseif ($utypeId == 4 || $utypeId == 5 || $utypeId == 6) { ?>
        <!--<div style="color: #1fb5ad; font-size: 25px; margin-left: 23%; margin-top: 2% "><b> Retailer </b></div>-->
        <div class="col-md-6 col-xs-5" style="color: #ffffff; font-size: 25px; margin-left: 5px; margin-top: 1.5% "><b>
                Retailer </b></div>

    <?php } ?>
    <div class="top-nav clearfix">
        <!--search & user info start-->
        <ul class="nav pull-right top-menu">
            <!--<li>
                <input type="text" class="form-control search" placeholder=" Search">
            </li>-->
            <!-- user login dropdown start-->
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle icon-user" href="#">
                    <!--<img alt="" src="images/avatar1_small.jpg">-->
                    <i class="fa fa-user"></i>
                    <span class="username">
                        <?php
                        $FIRST_NAME = (isset($_SESSION['SC_LOGIN']['USER'])) ? $_SESSION['SC_LOGIN']['USER']['vFirstName'] : $_SESSION['SC_LOGIN']['ADMIN']['vFirstName'];
                        $LAST_NAME = (isset($_SESSION['SC_LOGIN']['USER'])) ? $_SESSION['SC_LOGIN']['USER']['vLastName'] : $_SESSION['SC_LOGIN']['ADMIN']['vLastName'];
                        echo ucfirst($FIRST_NAME) . ' ' . ucfirst($LAST_NAME);

                        ?>

                    </span>

                    <b class="caret"></b>
                </a>
                <center>
                    <label style="color: #ffffff; font-weight: 600px; font-size: 13px"> Last Login(<?php echo $date; ?>
                        ) </label></center>
                <!--                su-subuseradd&type=4&mode=update&iId=2-->
                <ul class="dropdown-menu extended logout">

                    <?if(isset($_SESSION['SC_LOGIN']['USER'])){?>
                    <li><a href="index.php?file=su-subuseradd&type=<?php echo $type ?>&mode=update&iId=<?php echo $iId; ?>"><i class=" fa fa-suitcase"></i>Profile</a></li>
                    <li><a href="index.php?file=su-manage_store"><i class="fa fa-cog"></i> Settings</a></li>
                    <li><a href="logout.php"><i class="fa fa-key"></i> Log Out</a></li>
<?}else{?>
                        <li><a href="index.php?file=sc-adminadd&mode=update&iId=<?php echo $_SESSION['SC_LOGIN']['ADMIN']['iAdminId']?>&page=profile"><i class=" fa fa-suitcase"></i>Profile</a></li>
                        <li><a href="index.php?file=sc-changepassword"><i class="fa fa-cog"></i> Change Password</a></li>
                        <li><a href="logout.php"><i class="fa fa-key"></i> Log Out</a></li>
          <?
                    }?>


                </ul>
            </li>
            <!-- user login dropdown end -->
        </ul>
        <!--search & user info end-->
    </div>

</header>

<!--header end-->
<input type="hidden" name="dt1" id="dt_header" value="<?=$_SERVER['REQUEST_TIME'];?>">
<script>

    setInterval(
        function () {
            var uid = '<?=$_SESSION['SC_LOGIN']['USER']['iUserId'];?>';
            $.ajax({
                url: 'index.php?file=m-messages_a&uid=' + uid,
                type: 'POST',
                data: {"mode": 'get_notification'},
                success: function (result) {
                    console.log(result);
                    var data = JSON.parse(result);
                    $('#dt_header').val(data['time']);
                    if (data['iMessageId'] != '' && data['iMessageId'] != undefined) {
                        set_message(data['vFirstName'] + ' ' + data['vLastName'], data['vMessage'], data['iSenderId']);
                    }
                }
            });
        }, 5000);

    function set_message(name, msg, userid){
        $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: '<a style="color:#FFFFFF;" href="index.php?file=m-messages&frndid=' + userid + '">' + msg + '</a>',
            text: '<a style="color:#FFFFFF;" href="index.php?file=m-messages&frndid=' + userid + '">' + name + '</a>'
            // (bool | optional) if you want it to fade out on its own or just sit there
        });
    }
</script>