<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 8/4/15
 * Time: 11:41 AM
 */
include_once('include.php');
include_once('header.php');
?>



<!--<section class="image-slider">
    	<img src="images/bnr.jpg" alt="slider1"  class="img-responsive" width="100%">
    </section>-->

<!-- Full Page Image Background Carousel Header -->

<header id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
        <li data-target="#myCarousel" data-slide-to="5"></li>
        <!--<li data-target="#myCarousel" data-slide-to="2"></li>-->
    </ol>

    <!-- Wrapper for Slides -->
    <div class="carousel-inner">
        <div class="item active">
            <!-- Set the first background image using inline CSS below. -->
            <div class="fill" style="background-image:url(assets/images/sc-bnr1.jpg);"></div>
            <div class="carousel-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 table-mi-bnr">
                            <div class="tablecell-mi">
                            	<img src="<?php echo $site_url; ?>assets/images/rtlr-bnr-icon.png" alt="">
                                <h3 class="bnr-ttl btmrdlnbnr">Retailer</h3>
                                <p class="sldr-txt">How do you hold your vendors accountable for promised level of      service?</p>
                            </div>
            			</div>
                    </div>
        		</div>
            </div>
        </div>
        <div class="item">
          <div class="fill" style="background-image:url(assets/images/sc-bnr06.jpg);"></div>
            <div class="carousel-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 table-mi-bnr">
                            <div class="tablecell-mi">
                            	<img  src="<?php echo $site_url; ?>assets/images/dstributor-bnr-icon.png" alt="">
                              <h3 class="bnr-ttl btmrdlnbnr">Distributor</h3>
                              <p class="sldr-txt">How do you show your customers you are their best vendor?</p>
                            </div>
            			</div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="item">
            <div class="fill" style="background-image:url(assets/images/sc-bnr03.jpg);"></div>
            <div class="carousel-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 table-mi-bnr">
                            <div class="tablecell-mi">
                                <img src="<?php echo $site_url; ?>assets/images/mnfctr-bnr-icon.png"  alt="">
                               <h3 class="bnr-ttl btmrdlnbnr">Manufacturer</h3>
                                <p class="sldr-txt">How do you keep track of the distributors who service your customers?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>

        	<div class="item">
            <!-- Set the second background image using inline CSS below. -->
            <div class="fill" style="background-image:url(assets/images/sc-bnr2.jpg);"></div>
	            <div class="carousel-caption">
	                <div class="container">
	                    <div class="row">
	                        <div class="col-md-6 table-mi-bnr">
	                            <div class="tablecell-mi">
		                            <img src="<?php echo $site_url; ?>assets/images/rtlr-bnr-icon.png"  alt="">
	                               <h3 class="bnr-ttl btmrdlnbnr">Retailer</h3>
		                            <p class="sldr-txt">How do you currently award business to your vendors?</p>
	                            </div>
	            			</div>
	                    </div>
	        		</div>
	            </div>  
        	</div>
        <div class="item">
            <!-- Set the second background image using inline CSS below. -->
            <div class="fill header-bgimg-right" style="background-image:url(assets/images/sc-bnr5.jpg);"></div>
            <div class="carousel-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 table-mi-bnr">
                            <div class="tablecell-mi">
                            	<img  src="<?php echo $site_url; ?>assets/images/dstributor-bnr-icon.png" alt="">
                              <h3 class="bnr-ttl btmrdlnbnr">Distributor</h3>
                              <p class="sldr-txt">How do you keep track of your employee’s service time?</p>
                            </div>
            			</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <!-- Set the second background image using inline CSS below. -->
            <div class="fill" style="background-image:url(assets/images/sc-bnr4.jpg);"></div>
            <div class="carousel-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 table-mi-bnr">
                            <div class="tablecell-mi">
                            <img src="<?php echo $site_url; ?>assets/images/mnfctr-bnr-icon.png" alt="">
                            <h3 class="bnr-ttl btmrdlnbnr">Manufacturer</h3>
                            <p class="sldr-txt">Are you tired of competing on price alone?</p>
                         </div>
            			</div>
                    	</div>
        			</div>
            	</div>
        	</div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="icon-prev"></span> </a> <a
        class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="icon-next"></span> </a>
</header>

<!-- welcome content starts here -->
<section class="welcome-txt-main" id="howitworks">
    <div class="container">
        <h2 data-anijs="if: scroll, on: window, do: zoomOutmi animated, before: scrollReveal, after: holdAnimClass">
            <!--What is <span class="red">Service</span> <span class="grey37">Calibrate</span>-->
            How It Works? </h2>

        <div class="row">
            <!-- YouTube player starts -->
            <div class="col-md-8 ipadvideo-main col-md-offset-2"
                 data-anijs="if: scroll, on: window, do: flipInY animated mi-visible, before: scrollReveal">
                <iframe class="hw-wrks-vdo" src="https://www.youtube.com/embed/EHHonBL74Nc?rel=0" frameborder="0"
                        allowfullscreen></iframe>
            </div>
            <!-- YouTube player ends -->
        </div>
        
        <div class="row whatistxt grey65 text-center common-spacing-margin">
            <div class="col-md-12">
                <p> For the first time ever, <span class="grey37">Service is Quantifiable.</span> The world is moving at
                    light speed, whole industries have been overrun. Your business needs a way to ensure it is
                    differentiating itself at the street level, and getting what it has been committed to for your hard
                    earned purchases. We are the only company that can give you that, we are Service Calibrate. </p>
            </div>
        </div>
        <div class="row common-spacing-margin chng-strctr-mbl-mi">
            <div class="col-md-6 whatistxt grey65 text-left">
                <p class="btmrdln revolutionize-paddingtop"> We will revolutionize the service quantification business
                    in the same way Nielsen has revolutionized the market share analysis business. </p>
            </div>
            <div class="col-md-6 lptp-mbls">
                <div class="revolutionize-imgs">
                		<img src="<?php echo $site_url; ?>assets/images/laptop1.png" alt=""
                         class="revolutionize-laptop mi-invisible"
                         data-anijs="if: scroll, on: window, do: bounceInRight animated mi-visible, before :scrollReveal, after: holdAnimClass">
                     <img src="<?php echo $site_url; ?>assets/images/android1.png" alt=""
                         class="revolutionize-android mi-invisible"
                         data-anijs="if: animationend, on: .revolutionize-laptop, do: fadeInUp animated mi-visible, after: holdAnimClass">
                     <img src="<?php echo $site_url; ?>assets/images/iphone1.png" alt=""
                         class="revolutionize-iphone mi-invisible"
                         data-anijs="if: animationend, on: .revolutionize-android, do: fadeInUp animated mi-visible, after: holdAnimClass">
                </div>
            </div>
        </div>
        <div class="row common-spacing-margin">
            <div class="col-md-6">
                <div class="welldone-imgs"><img src="<?php echo $site_url; ?>assets/images/weldone-img1.png" alt=""
                         class="weldone-img1 mi-invisible"
                         data-anijs="if: animationend, on: .anim-hlpr, do: zoomInmi animated mi-visible, after: holdAnimClass">
                    <img src="<?php echo $site_url; ?>assets/images/weldone-img2.png" alt=""
                         class="weldone-img2 mi-invisible"
                         data-anijs="if: animationend, on: .weldone-img1, do: zoomInmi animated mi-visible, after: holdAnimClass">
                    <img src="<?php echo $site_url; ?>assets/images/weldone-img3.png" alt=""
                         class="weldone-img3 mi-invisible"
                         data-anijs="if: animationend, on: .weldone-img2, do: zoomInmi animated mi-visible, after: holdAnimClass">
                    <img src="<?php echo $site_url; ?>assets/images/weldone-truck.png" alt=""
                         class="weldone-truck mi-invisible"
                         data-anijs="if: animationend, on: .weldone-img3, do: zoomInmi animated mi-visible, after: holdAnimClass">
                    <img src="<?php echo $site_url; ?>assets/images/weldone-user.png" alt=""
                         class="weldone-user mi-invisible"
                         data-anijs="if: animationend, on: .weldone-img3, do: zoomInmi animated mi-visible, after: holdAnimClass">
                    <img src="<?php echo $site_url; ?>assets/images/weldone-cart.png" alt=""
                         class="weldone-cart mi-invisible"
                         data-anijs="if: animationend, on: .weldone-img3, do: zoomInmi animated mi-visible, after: holdAnimClass">
                    <img src="<?php echo $site_url; ?>assets/images/anim-helper.png" alt=""
                         class="anim-hlpr  mi-invisible"
                         data-anijs="if: scroll, on: window, do: zoomInmi animated mi-visible, after: holdAnimClass, before :scrollReveal">
                </div>
            </div>
            <div class="col-md-6 whatistxt grey65 txt-lft">
                <p class="btmrdln weldone-paddingtop"> Well done is better than well said. Far too many retailers are
                    unable to hold their vendors/distributors/manufacturers accountable to any promised service levels.
                    For the first time our customers will be able to actually measure the “Say-Do” ratio of the
                    companies they spend their hard earned cash with. </p>
            </div>
    		</div>
    </div>
</section>
<!-- welcome content ends here -->

<section class="mblbgsix" id="feature">
    <div class="container">
        <h2 data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible, after: holdAnimClass"> What you can do with Service Calibrate</h2>

        <div class="row">
            <div class="col-md-3 col-sm-6 text-center table-mi mi-invisible eightofone"
                 data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible delay-01s, after: holdAnimClass, before :scrollReveal, after: holdAnimClass">
                
                <div class="eight-specifications-item eight3icon tablecell-mi"><span></span>Service Frequency
                    <div class="eight-specification-hvr-cntnt">
                        <h4>Service Frequency</h4>

                        <p>How often you are being serviced by your vendor? With Service Calibrate you can see your
                            vendor/distributor/manufacturer representative’s activity by day/time in Account/place of
                            business. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 text-center table-mi mi-invisible eightoftwo"
                 data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible delay-03s, after: holdAnimClass, before :scrollReveal, after: holdAnimClass">
                
                <div class="eight-specifications-item eight8icon tablecell-mi"><span></span> Compare Service
                    <div class="eight-specification-hvr-cntnt">
                        <h4>Compare Service</h4>

                        <p>How do you know which vendor provides you with the best service? With Service Calibrate you
                            can compare vendor service time by category.</p>
                    </div>
        </div>
            </div>
            <div class="col-md-3 col-sm-6 text-center table-mi mi-invisible eightofthree"
                 data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible delay-05s, after: holdAnimClass, before :scrollReveal, after: holdAnimClass">
                
                <div class="eight-specifications-item eight6icon tablecell-mi"><span></span> Projected Delivery
                    <div class="eight-specification-hvr-cntnt">
                        <h4>Projected Delivery</h4>

                        <p>You’ll know in advance when you are projected to receive a delivery & the dollar amount. For
                            individuals managing many locations, knowing how much to leave a payment for is a real time
                            saver.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 text-center table-mi mi-invisible eightoffour"
                 data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible delay-07s, after: holdAnimClass, before :scrollReveal, after: holdAnimClass">
                
                <div class="eight-specifications-item eight2icon tablecell-mi"><span></span>Ratings
                    <div class="eight-specification-hvr-cntnt">
                        <h4>Ratings</h4>

                        <p>Do your complaints & praises sometimes fall on deaf ears? With Service Calibrate, you will
                            always have the
                            latest feedback on your employees service levels- directly from those they see each
                            day! </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 text-center table-mi mi-invisible eightoffive"
                 data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible delay-09s, after: holdAnimClass, before :scrollReveal, after: holdAnimClass">
                
                <div class="eight-specifications-item eight4icon tablecell-mi"><span></span>View Promotions
                    <div class="eight-specification-hvr-cntnt">
                        <h4>View Promotions</h4>

                        <p>“That Promotion Is OVER”.. Does this sound familiar? With Service Calibrate you can keep
                            track on all your vendor/distributor/manufacturer promotions & buy deals BEFORE they
                            expire.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 text-center table-mi mi-invisible eightofsix"
                 data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible delay-11s, after: holdAnimClass, before :scrollReveal, after: holdAnimClass">
                <div class="eight-specifications-item eight1icon tablecell-mi"><span></span>Contacts
                    <div class="eight-specification-hvr-cntnt">
                        <h4>Contacts</h4>

                        <p>We know you are tired of searching through old business cards when we needed to talk with a
                            company rep! Now with Service Calibrate all your contacts are at your finger tips</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 text-center table-mi mi-invisible eightofseven"
                 data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible delay-13s, after: holdAnimClass, before :scrollReveal, after: holdAnimClass">
                
                <div class="eight-specifications-item eight5icon tablecell-mi"><span></span> Broadcast
                    <div class="eight-specification-hvr-cntnt">
                        <h4>Broadcast A Message</h4>

                        <p>Distributors & Manufacturers need to know when the locations they service are open or closed.
                            With Service Calibrate, sites can communicate their status directly. This can potentially
                            save valuable time, labor, and fuel. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 text-center table-mi mi-invisible eightofeight"
                 data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible delay-15s, after: holdAnimClass, before :scrollReveal, after: holdAnimClass">
                
                <div class="eight-specifications-item eight7icon tablecell-mi"><span></span> Signature
                    <div class="eight-specification-hvr-cntnt">
                        <h4>Signature</h4>

                        <p>Safety and Accountability are todays watchwords. With Service Calibrate, you can now request
                            all sites
                            within your network collect signatures at time of visit.</p>
                    </div>
                </div>
            </div>
    </div>
    </div>
</section>
<section class="client-tstmonials">
    <div class="container">
        <h2 data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible, after: holdAnimClass"> Client
            Testimonials </h2>
    </div>
    <!-- Full Page Image Background Carousel Header -->
    <div id="myCarousel1" class="carousel slide">
        <!-- Indicators -->
        <!--<ol class="carousel-indicators">
                        <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel1" data-slide-to="1"></li>
                        <li data-target="#myCarousel1" data-slide-to="2"></li>
                        </ol>-->

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="carousel-caption">
                    <p class="clnt-dtls"><span class="clint-pic"> <img
                                src="<?php echo $site_url; ?>assets/images/distri.png" alt=""> </span> <span
                            class="client-name">Nick Jonas</span> <span
                            class="abt-client">Retailer</span></p>

                    <p class="client-txt"> The first week we started implementing Service Calibrate we saw immediate
                        improvements from our vendors” </p>
                </div>
            </div>
            <div class="item">
                <div class="carousel-caption">
                    <p class="clnt-dtls"><span class="clint-pic"> <img
                                src="<?php echo $site_url; ?>assets/images/manufacring.png" alt=""> </span> <span
                            class="client-name">John  Watson</span> <span
                            class="abt-client">Manufacturer</span></p>

                    <p class="client-txt"> “We closed a big contract by simply stating we had service quantification
                        data that showed we outperformed out competitors with time on site - after the sale” </p>
                </div>
            </div>
            <div class="item">
                <div class="carousel-caption">
                    <p class="clnt-dtls"><span class="clint-pic"> <img
                                src="<?php echo $site_url; ?>assets/images/vendor.png" alt=""> </span> <span
                            class="client-name">Smith Cooper</span> <span
                            class="abt-client">Distributor</span></p>

                    <p class="client-txt"> “We saved $500 per week in fuel costs by routing and tracking our employees
                        in real time” </p>
        </div>
            </div>
    </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel1" data-slide="prev"> <span class="icon-prev"></span> </a> <a
            class="right carousel-control" href="#myCarousel1" data-slide="next"> <span class="icon-next"></span> </a>
    </div>
</section>
<section class="download-main">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center"><img src="<?php echo $site_url; ?>assets/images/download-logo.png" alt="" class="dwnld-lgo-mi">

                <h3>Finally Service is <span class="red" id="js-rotating">Quantifiable, Measurable, Comparable, Assessable, Calculable , Computable, Gaugeable, Fathomable</span>
                </h3>
            </div>
            <div class="col-md-12 text-center">
                <p>Available on <span>iOS</span> and <span>Android</span></p>

                <p class="dwnld-btns-mi">
                    <a href="https://itunes.apple.com/us/app/service-calibrate/id985946615?ls=1&mt=8" class=""><img
                            src="<?php echo $site_url; ?>assets/images/download-frm-appstore.png"
                                              alt=""></a>
                    <a href="#" class="" style="position:relative;"><img
                            src="<?php echo $site_url; ?>assets/images/download-frm-playstore.png" alt=""><img
                            src="<?php echo $site_url; ?>assets/images/coming-soon.png" alt=""
                            style="position: absolute; right: -50px; top: -15px;"></a>
                </p>
            </div>
    </div>
    </div>
</section>
<section class="footer-contact" id="contact">
    <div class="container">


        <div class="row">
            <div class="col-md-6">
                <h2 class="ftr-contact-icon mi-invisible"
                    data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible, after: holdAnimClass">Contact
                    Us
                    <span class="cntct-icn"></span>
                </h2>
                <form method="post" name="contact_frm" id="contact_frm">
                    <div class="col-md-6">
                        <input type="text" placeholder="First Name" name="fname" id="fname">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Last Name" name="lname" id="lname">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Email Address" name="email" id="email">
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Phone Number" name="phone" id="phone">
                    </div>
                    <div class="col-md-12">
                        <textarea placeholder="Write your message" name="message" id="message"></textarea>
                    </div>
                    <div class="col-md-12 text-center">
                        <button type="submit" name="contact_btn" id="contact_btn">Send Message</button>
                    </div>
                </form>
            </div>

            <div class="col-md-6 text-center">
                <h2 class="ftr-contact-icon mi-invisible mrgn-tp-crprt-icn"
                    data-anijs="if: scroll, on: window, do: zoomOutmi animated mi-visible, after: holdAnimClass">
                    Corporate Address
                    <span class="crprt-adrs-icn"></span>
                </h2>

                <div class="sc-sdrs">
                    <address>
                        <span><img src="<?php echo $site_url; ?>assets/images/logo-bottom.png" alt="Service Calibrate"></span><br>
                        3655 Torrance Blvd,<br>
                        Suite #300,<br>
                        Torrance, CA 90503, USA
                    </address>
                </div>
                <div class="sc-eml-adrs">
                    <span>@</span><br>
                    <a href="#">sales@servicecalibrate.com</a>
                </div>
            </div>
        </div>
    </div>
</section>

<? include("footer.php"); ?>
<link rel="stylesheet" href="<?php echo $site_url; ?>assets/css/alertify.core.css"/>
<link rel="stylesheet" href="<?php echo $site_url; ?>assets/css/alertify.default.css" id="toggleCSS"/>

<script type="text/javascript" src="<?= $site_url; ?>assets/js/alertify.min.js"></script>
<script src="<?php echo $assets_url ?>js/jquery.validate.min.js"></script>

<script>
    $(".eight-specifications-item").mouseover(
        function () {
            $(this).children(".eight-specification-hvr-cntnt").addClass("opened-eightcontant");
        });
    $(".eight-specifications-item").mouseout(
        function () {
            $(this).children(".eight-specification-hvr-cntnt").removeClass("opened-eightcontant");
        });

/*video height small devices*/

    $(document).ready(function () {
        /*  $('.hw-wrks-ply').on('click', function(ev) {
         $("iframe.hw-wrks-vdo")[0].src += "&autoplay=1";
         ev.preventDefault();
         });*/

        if (window.innerWidth < 768) {
	var vdowdth = $("iframe.hw-wrks-vdo").width();
            var vdohgt = vdowdth * 0.559;
	$("iframe.hw-wrks-vdo").css("height", vdohgt);
        }

        sub_subscribe_frm.init();

    });

</script>
<style>
    .help-block-contact{
        display: block;
        margin-top: 5px;
        margin-bottom: 10px;
        color: inherit;
        border: 3px solid #ff0000 !important;
    }
</style>
<script>

    var sub_subscribe_frm = function () {
        var subscribe_validate_form = function () {
            $("#contact_frm").validate({
                /*errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block-contact',*/
                onkeyup: false,
                focusInvalid: false,
                onfocusout: false,
                rules: {
                    fname: {
                        required: true
                    },
                    lname: {
                        required: true
                    },
                    email: {
                        email: true,
                        required: true
                    },
                    message: {
                        required: true
                    }
                },
                messages: {
                    fname : "Please Enter First Name",
                    lname : "Please Enter Last Name",
                    email: {
                        required: "Please Enter Email",
                        email: "Please Enter Valid Email "
                    },
                    message : "Please Enter Message"
                },
                errorPlacement: function (error, element) {
                  //  error.insertAfter(element);
                    alertify.error(error[0].innerHTML, 2000);
                },
                submitHandler: function () {
                    contact_us();
                }

            });
        };
        return {

            init: function () {
                subscribe_validate_form();
            }
        };
    }();
    function contact_us()
    {

        var ajax_url    =   '<?php echo $ajax_url; ?>';

        var fname   =   $('#fname').val();
        var lname   =   $('#lname').val();
        var email   =   $('#email').val();
        var phone   =   $('#phone').val();
        var message   =   $('#message').val();
        $.ajax({
            url:ajax_url+"ajax-contact-us.php",
            type:'POST',
            data:{fname:fname,lname:lname,email:email,phone:phone,message:message},
            success: function (result) {
               alertify.success("Thank You for contacting us. We will get back to you as soon as possible", 3000);
                setInterval(
                    function () {
                        window.location.href = "<?php echo $site_url;?>";
                    }, 4000);
            }
        });
    }
    
    
</script>


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-64814851-1', 'auto');
    ga('send', 'pageview');

</script>
