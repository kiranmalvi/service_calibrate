<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 8/4/15
 * Time: 2:32 PM
 */
include_once('include.php');
include_once($inc_class_path . 'user.class.php');
$userObj = new user();

include_once($inc_class_path . 'user_settings.class.php');
$user_settingsObj = new user_settings();

include_once($inc_class_path . 'referral.class.php');
$referralObj = new referral();

$SIGNUP = $_REQUEST['signup'];
$id = $_REQUEST['id1'];
$mode = $_REQUEST['mode'];

if ($mode == "jpg_chk") {
    ob_get_clean();
    //echo "heloloo";
    $id1 = $_REQUEST['id'];
    $imageFileType = pathinfo($id1, PATHINFO_EXTENSION);
    if ($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG"
        && $imageFileType != "gif" && $imageFileType != "GIF" && $imageFileType != "bmp" && $imageFileType != "BMP"
    ) {
        ?>
        <span style="color:maroon;"
              id="error_msg<?php echo $id ?>"><?php echo "Sorry, only JPG, JPEG, PNG, GIF & BMP files are allowed."; ?></span>
        <?php

        //  $uploadOk = 0;
    } else {
        ?>
        <span style="" id="error_msg<?php echo $id ?>"><?php echo $id1; ?></span>
    <?php

    }

    exit;

}

if ($mode == "get_zipcode") {

    ob_get_clean();

    $zip = $_REQUEST['zipcode'];
    $location = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address='.$zip.'&sensor=true");
    //$adrs = json_decode($location);
    /* $city=$adrs->results[0]->address_components[1]->long_name;
     $state=$adrs->results[0]->address_components[3]->long_name;
     $country=$adrs->results[0]->address_components[4]->long_name;*/

    print_r($location);
    exit;
} else if ($SIGNUP == 'single_store_retailer') {
    $userObj->setvStoreName($_POST['vStoreName']);
    $userObj->setvStoreUniqueId($_POST['vStoreUniqueId']);
    $userObj->setvFirstName($_POST['vFirstName']);
    $userObj->setvLastName($_POST['vLastName']);
    $userObj->setvEmail($_POST['vEmail']);
    $userObj->setvPassword(md5($_POST['vPassword']));
    $userObj->setvAddress($_POST['vAddress']);


    if ($_FILES["vImage"]["name"] != "") {
        echo $photopath = $user_image_path;
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];

        //$vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");

        $promotion_img_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);

        echo $promotion_img_name = $promotion_img_arr[0];
        if ($promotion_img_name == '') {
            //$_SESSION['img_prm'] = "Please upload pictures only in png,jpg,jpeg,gif or bmp format.";
            // $generalfuncobj->func_set_temp_sess_msg("Please upload pictures only in png,jpg,jpeg,gif or bmp format.", "alert-danger", "Error");
            header("Location:regi.php");
            exit;
        } else {
            $userObj->setvImage($promotion_img_name);
        }


    } else {
        $old_logo = $_REQUEST['vImage'];
        $userObj->setvImage($old_logo);
    }


    $address = $_POST['vAddress'];
    $city=$_POST['vCity'];
    $state=$_POST['vState'];
    $zip=$_POST['vZip'];
    $fulladdress=$address.",".$city.",".$state.",".$zip;
    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($fulladdress) . '&sensor=true');

    $coordinates = json_decode($coordinates);
    $lat[0] = $coordinates->results[0]->geometry->location->lat;
    $long[0] = $coordinates->results[0]->geometry->location->lng;
    $userObj->setvLongitude($long[0]);
    $userObj->setvLatitude($lat[0]);
    $userObj->setvState($_POST['vState']);
    $userObj->setvCity($_POST['vCity']);
    $userObj->setvZip($_POST['vZip']);
    $userObj->setiCountryId($_POST['vCountry']);
    $userObj->setvContact($_POST['vContact']);

    $userObj->setiIndustriesId($_POST['iIndustriesId']);
    $userObj->setiPreferDeliveryTimeFrom($_POST['iPreferDeliveryTimeFrom']);
    $userObj->setiPreferDeliveryTimeTo($_POST['iPreferDeliveryTimeTo']);
    $userObj->setiDeliveryHoursFrom($_POST['iDeliveryHoursFrom']);
    $userObj->setiDeliveryHoursTo($_POST['iDeliveryHoursTo']);
    $userObj->seteNightDelivery($_POST['eNightDelivery']);
    $userObj->seteHowAboutUs($_POST['aboutus']);
    $userObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
    $userObj->setvReferralEmail(addslashes($_POST['refmailsingle']));


    if(isset($_POST['refmailsingle']))
    {
        $refmail = $_POST['refmailsingle'];
        $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
        $row = $userObj->check_exist_mail($refmail);


        if (count($row) > 0) {
            $userid = $row[0]['iUserId'];
            $point="200";
            if ($referralObj->check_existing_user($userid)) {

                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->update_points($userid,$point);
            } else {
                $referralObj->setiUserId($userid);
                $referralObj->setvPoint($point);
                $referralObj->seteStatus("1");
                $referralObj->setiDtAdded($iDtAdded);
                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->setvReason("Reference");

                $iReffaralId = $referralObj->insert();
            }
        }
    }


    $userObj->seteStatus("1");

    $userObj->setePlanType("0");
    $userObj->seteActivePaidPlan("0");
    $iExpireTime = strtotime("+0 day", strtotime(date('Y-m-d 23:59:59')));
    $userObj->setiExpireTime($iExpireTime);
    $userObj->setiUserTypeId("4");
    $userObj->setiParentId("0");
    $userObj->setiPlanId("1");
    $userObj->setfAmountPaid("0.00");
    $userObj->setiTypeOfStoreId($_POST['storetype']);



    //$userObj->vReferralEmail("foram@gmail.com");

    $iUserId = $userObj->insert();

    $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($iUserId)";
    $obj->sql_query($store_count);

    $user_settingsObj->setiUserId($iUserId);
    $user_settingsObj->seteNotification('1');
    $iUserSettingsId = $user_settingsObj->insert();

    /* $user_securityObj->setiUserId($iUserId);
     $user_securityObj->setvQuestion($_POST['sque']);
     $user_securityObj->settAnswer($_POST['ans']);
     $user_securityObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
     $user_securityObj->seteStatus("1");

     $iUserSecQueId=$user_securityObj->insert();*/
    $vStoreUniqueId = $_POST['vStoreUniqueId'];
    $vStoreUniqueId1 = str_replace(' ', '', $vStoreUniqueId);
    $account = substr($vStoreUniqueId1, 0, 4);
    $userObj->GENERATE_SC_QR_CODE($iUserId);
    $mail = $_POST['vEmail'];
    $vStoreUniqueId = $_POST['vStoreUniqueId'];
    $DATA = array('name' => $vStoreUniqueId);
    echo $generalfuncobj->get_SC_SEND_mail('register', $DATA, $mail, 'Welcome To Service Calibrate');

   header("Location:autologin.php?iId=$iUserId");
   // header("Location:login.php");
    exit;
} else if ($SIGNUP == 'multiple_store_retailer') {

    if ($_FILES["vImage"]["name"] != "") {
        echo $photopath = $user_image_path;
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];

        //$vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");

        $promotion_img_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);

        echo $promotion_img_name = $promotion_img_arr[0];
        if ($promotion_img_name == '') {
            //$_SESSION['img_prm'] = "Please upload pictures only in png,jpg,jpeg,gif or bmp format.";
            // $generalfuncobj->func_set_temp_sess_msg("Please upload pictures only in png,jpg,jpeg,gif or bmp format.", "alert-danger", "Error");
            header("Location:regi.php");
            exit;
        } else {
            $userObj->setvImage($promotion_img_name);
        }


    } else {
        $old_logo = $_REQUEST['vImage'];
        $userObj->setvImage($old_logo);
    }


    $userObj->setvStoreName($_POST['vStoreName']);
    $userObj->setvStoreUniqueId($_POST['vStoreUniqueId']);
    $userObj->setNumberOfStore($_POST['vStoreNumber']);
    $userObj->setvComment($_POST['vStoreNumber']);
    $userObj->setvFirstName($_POST['vFirstName']);
    $userObj->setvLastName($_POST['vLastName']);
    $userObj->setvEmail($_POST['vEmail']);
    $userObj->setvPassword(md5($_POST['vPasswordmulti']));
    $userObj->setvAddress($_POST['vAddress']);
    
    $address = $_POST['vAddress'];
    $city=$_POST['vCity'];
    $state=$_POST['vState'];
    $zip=$_POST['vZip'];
    $fulladdress=$address.",".$city.",".$state.",".$zip;
    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($fulladdress) . '&sensor=true');

    $coordinates = json_decode($coordinates);
    $lat[0] = $coordinates->results[0]->geometry->location->lat;
    $long[0] = $coordinates->results[0]->geometry->location->lng;
    $userObj->setvLongitude($long[0]);
    $userObj->setvLatitude($lat[0]);
    $userObj->setvState($_POST['vState']);
    $userObj->setvCity($_POST['vCity']);
    $userObj->setiCountryId($_POST['vCountry']);
    $userObj->setvZip($_POST['vZip']);
    $userObj->setiCategoryId($_POST['iCategoryId']);
    $userObj->setiIndustriesId($_POST['iIndustriesId']);
    $userObj->setiPreferDeliveryTimeFrom($_POST['iPreferDeliveryTimeFrom']);
    $userObj->setiPreferDeliveryTimeTo($_POST['iPreferDeliveryTimeTo']);
    $userObj->setiDeliveryHoursFrom($_POST['iHoursFrom']);
    $userObj->setiDeliveryHoursTo($_POST['iHoursTo']);
    $userObj->seteNightDelivery($_POST['eNightDelivery1']);
    $userObj->seteHowAboutUs($_POST['aboutus']);
    $userObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
    $userObj->seteStatus("1");
    $userObj->setvContact($_POST['vContact']);
    $userObj->setvReferralEmail(addslashes($_POST['refmailmulti']));

    if(isset($_POST['refmailmulti']))
    {
        $refmail = $_POST['refmailmulti'];
        $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
        $row = $userObj->check_exist_mail($refmail);


        if (count($row) > 0) {
            $userid = $row[0]['iUserId'];
          $point="200";
            if ($referralObj->check_existing_user($userid)) {

                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->update_points($userid,$point);
            } else {

                $referralObj->setiUserId($userid);
                $referralObj->setvPoint($point);
                $referralObj->seteStatus("1");
                $referralObj->setiDtAdded($iDtAdded);
                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->setvReason("Reference");
                $iReffaralId = $referralObj->insert();
            }
        }
    }


    $userObj->setePlanType("0");
    $userObj->seteActivePaidPlan("0");
    $iExpireTime = strtotime("+30 day", strtotime(date('Y-m-d 23:59:59')));
    $userObj->setiExpireTime($iExpireTime);
    $userObj->setiUserTypeId("5");
    $userObj->setiParentId("0");
    $userObj->setiPlanId("1");
    $userObj->setfAmountPaid("0.00");
    $userObj->setiTypeOfStoreId($_POST['storetype']);


    //$userObj->vReferralEmail("foram@gmail.com");

    $iUserId = $userObj->insert();

    $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($iUserId)";
    $obj->sql_query($store_count);

    $user_settingsObj->setiUserId($iUserId);
    $user_settingsObj->seteNotification('1');
    $iUserSettingsId = $user_settingsObj->insert();

    /* $user_securityObj->setiUserId($iUserId);
     $user_securityObj->setvQuestion($_POST['sque']);
     $user_securityObj->settAnswer($_POST['ans']);
     $user_securityObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
     $user_securityObj->seteStatus("1");

     $iUserSecQueId=$user_securityObj->insert();*/

    $vStoreUniqueId = $_POST['vStoreUniqueId'];
    $vStoreUniqueId1 = str_replace(' ', '', $vStoreUniqueId);
    $account = substr($vStoreUniqueId1, 0, 4);
    $userObj->GENERATE_SC_QR_CODE($iUserId);
    $mail = $_POST['vEmail'];
    $vStoreUniqueId = $_POST['vStoreUniqueId'];
    $DATA = array('name' => $vStoreUniqueId);
      echo $generalfuncobj->get_SC_SEND_mail('register', $DATA, $mail, 'Welcome To Service Calibrate');
 header("Location:autologin.php?iId=$iUserId");
    //header("Location:login.php");
    exit;
} else if ($SIGNUP == 'corporate_store_retailer') {

    if ($_FILES["vImage"]["name"] != "") {
        echo $photopath = $user_image_path;
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];

        //$vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");

        $promotion_img_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);

        echo $promotion_img_name = $promotion_img_arr[0];
        if ($promotion_img_name == '') {
            //$_SESSION['img_prm'] = "Please upload pictures only in png,jpg,jpeg,gif or bmp format.";
            // $generalfuncobj->func_set_temp_sess_msg("Please upload pictures only in png,jpg,jpeg,gif or bmp format.", "alert-danger", "Error");
            header("Location:regi.php");
            exit;
        } else {
            $userObj->setvImage($promotion_img_name);
        }


    } else {
        $old_logo = $_REQUEST['vImage'];
        $userObj->setvImage($old_logo);
    }


    $userObj->setiParentId($_POST['parentid']);
    $userObj->setvStoreUniqueId($_POST['vStoreUniqueId']);
    $userObj->setvFirstName($_POST['vFirstName']);
    $userObj->setvLastName($_POST['vLastName']);
    $userObj->setvEmail($_POST['vEmail']);
    $userObj->setvPassword(md5($_POST['vPasswordcorporate']));
    $userObj->setvAddress($_POST['vAddress']);
  
   $address = $_POST['vAddress'];
    $city=$_POST['vCity'];
    $state=$_POST['vState'];
    $zip=$_POST['vZip'];
    $fulladdress=$address.",".$city.",".$state.",".$zip;
    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($fulladdress) . '&sensor=true');


    $coordinates = json_decode($coordinates);
    $lat[0] = $coordinates->results[0]->geometry->location->lat;
    $long[0] = $coordinates->results[0]->geometry->location->lng;
    $userObj->setvLongitude($long[0]);
    $userObj->setvLatitude($lat[0]);
    $userObj->setvState($_POST['vState']);
    $userObj->setvCity($_POST['vCity']);
    $userObj->setiCountryId($_POST['vCountry']);
    $userObj->setvZip($_POST['vZip']);
    $userObj->setiIndustriesId($_POST['iIndustriesId']);
    $userObj->setiPreferDeliveryTimeFrom($_POST['iPreferDeliveryTimeFrom']);
    $userObj->setiPreferDeliveryTimeTo($_POST['iPreferDeliveryTimeTo']);
    $userObj->setiDeliveryHoursFrom($_POST['iHoursFrom']);
    $userObj->setiDeliveryHoursTo($_POST['iHoursTo']);
    $userObj->seteNightDelivery($_POST['eNightDelivery2']);
    $userObj->seteHowAboutUs($_POST['aboutus']);
    $userObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
    $userObj->seteStatus("1");
    $userObj->setvContact($_POST['vContact']);
    $userObj->setvReferralEmail(addslashes($_POST['refmailcor']));

    if(isset($_POST['refmailcor']))
    {
        $refmail = $_POST['refmailcor'];
        $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
        $row = $userObj->check_exist_mail($refmail);


        if (count($row) > 0) {
            $userid = $row[0]['iUserId'];
          $point="200";
            if ($referralObj->check_existing_user($userid)) {

                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->update_points($userid,$point);
            } else {

                $referralObj->setiUserId($userid);
                $referralObj->setvPoint($point);
                $referralObj->seteStatus("1");
                $referralObj->setiDtAdded($iDtAdded);
                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->setvReason("Reference");
                $iReffaralId = $referralObj->insert();
            }
        }
    }


    $userObj->setePlanType("0");
    $userObj->seteActivePaidPlan("0");
    $iExpireTime = strtotime("+30 day", strtotime(date('Y-m-d 23:59:59')));
    $userObj->setiExpireTime($iExpireTime);
    $userObj->setiUserTypeId("6");
    $userObj->setiParentId("0");
    $userObj->setiPlanId("1");
    $userObj->setfAmountPaid("0.00");
    $userObj->setiTypeOfStoreId($_POST['storetype']);


    //$userObj->vReferralEmail("foram@gmail.com");

    $iUserId = $userObj->insert();

    $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($iUserId)";
    $obj->sql_query($store_count);

    $user_settingsObj->setiUserId($iUserId);
    $user_settingsObj->seteNotification('1');
    $iUserSettingsId = $user_settingsObj->insert();

    /* $user_securityObj->setiUserId($iUserId);
     $user_securityObj->setvQuestion($_POST['sque']);
     $user_securityObj->settAnswer($_POST['ans']);
     $user_securityObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
     $user_securityObj->seteStatus("1");

     $iUserSecQueId=$user_securityObj->insert();*/

    $vStoreUniqueId = $_POST['vStoreUniqueId'];
    $vStoreUniqueId1 = str_replace(' ', '', $vStoreUniqueId);
    $account = substr($vStoreUniqueId1, 0, 4);
    $userObj->GENERATE_SC_QR_CODE($iUserId);
    $mail = $_POST['vEmail'];
    $vStoreUniqueId = $_POST['vStoreUniqueId'];
    $DATA = array('name' => $vStoreUniqueId);
    echo $generalfuncobj->get_SC_SEND_mail('register', $DATA, $mail, 'Welcome To Service Calibrate');
   header("Location:autologin.php?iId=$iUserId");
    //header("Location:login.php");
    exit;
} else if ($SIGNUP == 'manufacturer_form') {


    if ($_FILES["vImage"]["name"] != "") {
        echo $photopath = $user_image_path;
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];

        //$vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");

        $promotion_img_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);

        echo $promotion_img_name = $promotion_img_arr[0];
        if ($promotion_img_name == '') {
            //$_SESSION['img_prm'] = "Please upload pictures only in png,jpg,jpeg,gif or bmp format.";
            // $generalfuncobj->func_set_temp_sess_msg("Please upload pictures only in png,jpg,jpeg,gif or bmp format.", "alert-danger", "Error");
            header("Location:regi.php");
            exit;
        } else {
            $userObj->setvImage($promotion_img_name);
        }


    } else {
        $old_logo = $_REQUEST['vImage'];
        $userObj->setvImage($old_logo);
    }


    $userObj->setvStoreUniqueId($_POST['vStoreUniqueId']);
    $userObj->setvFirstName($_POST['vFirstName']);
    $userObj->setvLastName($_POST['vLastName']);
    $userObj->setvEmail($_POST['vEmail']);
    $userObj->setvPassword(md5($_POST['vPasswordmanu']));
    $userObj->setvAddress($_POST['vAddress']);
    
   $address = $_POST['vAddress'];
    $city=$_POST['vCity'];
    $state=$_POST['vState'];
    $zip=$_POST['vZip'];
    $fulladdress=$address.",".$city.",".$state.",".$zip;
    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($fulladdress) . '&sensor=true');

    $coordinates = json_decode($coordinates);
    $lat[0] = $coordinates->results[0]->geometry->location->lat;
    $long[0] = $coordinates->results[0]->geometry->location->lng;
    $userObj->setvLongitude($long[0]);
    $userObj->setvLatitude($lat[0]);
    $userObj->setvState($_POST['vState']);
    $userObj->setvCity($_POST['vCity']);
    $userObj->setiCountryId($_POST['vCountry']);
    $userObj->setvZip($_POST['vZip']);
    $userObj->setiCategoryId($_POST['iCategoryId']);
    $userObj->setiIndustriesId($_POST['iIndustriesId']);
    $userObj->seteHowAboutUs($_POST['aboutus']);
    $userObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
    $userObj->seteStatus("1");
    $userObj->setiUserRoleId($_POST['roleid']);
    $userObj->setvContact($_POST['vContact']);
    $userObj->setvReferralEmail(addslashes($_POST['refmailmanu']));

    if(isset($_POST['refmailmanu']))
    {
        $refmail = $_POST['refmailmanu'];
        $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
        $row = $userObj->check_exist_mail($refmail);


        if (count($row) > 0) {
            $userid = $row[0]['iUserId'];
            $point="100";
            if ($referralObj->check_existing_user($userid)) {

                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->update_points($userid,$point);
            } else {

                $referralObj->setiUserId($userid);
                $referralObj->setvPoint($point);
                $referralObj->seteStatus("1");
                $referralObj->setiDtAdded($iDtAdded);
                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->setvReason("Reference");
                $iReffaralId = $referralObj->insert();
            }
        }
    }

    $userObj->setePlanType("0");
    $userObj->seteActivePaidPlan("0");
    $iExpireTime = strtotime("+30 day", strtotime(date('Y-m-d 23:59:59')));
    $userObj->setiExpireTime($iExpireTime);
    $userObj->setiUserTypeId("3");
    $userObj->setiParentId("0");
    $userObj->setiPlanId("3");
    $userObj->setfAmountPaid("0.00");


    $iUserId = $userObj->insert();

    $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($iUserId)";
    $obj->sql_query($store_count);

    $user_settingsObj->setiUserId($iUserId);
    $user_settingsObj->seteNotification('1');
    $iUserSettingsId = $user_settingsObj->insert();
    $mail = $_POST['vEmail'];
    $vStoreUniqueId = $_POST['vStoreUniqueId'];
    $DATA = array('name' => $vStoreUniqueId);
    echo  $generalfuncobj->get_SC_SEND_mail('register', $DATA, $mail, 'Welcome To Service Calibrate');
   header("Location:autologin.php?iId=$iUserId");   
   // header("Location:login.php");
    exit;


} else if ($SIGNUP == 'distributor_form') {
    if ($_FILES["vImage"]["name"] != "") {
        echo $photopath = $user_image_path;
        $vphoto = $_FILES["vImage"]["tmp_name"];
        $vphoto_name = $_FILES["vImage"]["name"];

        //$vphoto_type = $_FILES["vImage"]["type"];

        $prefix = date("YmdHis");

        $promotion_img_arr = $generalfuncobj->genfile_uploadFile($photopath, $vphoto, $vphoto_name, $prefix);

        echo $promotion_img_name = $promotion_img_arr[0];
        if ($promotion_img_name == '') {
            //$_SESSION['img_prm'] = "Please upload pictures only in png,jpg,jpeg,gif or bmp format.";
            // $generalfuncobj->func_set_temp_sess_msg("Please upload pictures only in png,jpg,jpeg,gif or bmp format.", "alert-danger", "Error");
            header("Location:login.php");
            exit;
        } else {
            $userObj->setvImage($promotion_img_name);
        }


    } else {
        $old_logo = $_REQUEST['vImage'];
        $userObj->setvImage($old_logo);
    }


    $userObj->setvStoreUniqueId($_POST['vStoreUniqueId']);
    $userObj->setvFirstName($_POST['vFirstName']);
    $userObj->setvLastName($_POST['vLastName']);
    $userObj->setvEmail($_POST['vEmail']);
    $userObj->setvPassword(md5($_POST['vPassworddistri']));

    $userObj->setvAddress($_POST['vAddress']);
   $address = $_POST['vAddress'];
    $city=$_POST['vCity'];
    $state=$_POST['vState'];
    $zip=$_POST['vZip'];
    $fulladdress=$address.",".$city.",".$state.",".$zip;
    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($fulladdress) . '&sensor=true');

    $coordinates = json_decode($coordinates);
    $lat[0] = $coordinates->results[0]->geometry->location->lat;
    $long[0] = $coordinates->results[0]->geometry->location->lng;
    $userObj->setvLongitude($long[0]);
    $userObj->setvLatitude($lat[0]);
    $userObj->setvState($_POST['vState']);
    $userObj->setvCity($_POST['vCity']);
    $userObj->setiCountryId($_POST['vCountry']);
    $userObj->setvZip($_POST['vZip']);
    $userObj->setiCategoryId($_POST['iCategoryId']);
    $userObj->setiIndustriesId($_POST['iIndustriesId']);
    $userObj->seteHowAboutUs($_POST['aboutus']);
    $userObj->setiDtAdded(strtotime(date('Y-m-d H:i:s')));
    $userObj->seteStatus("1");
    $userObj->setvReferralEmail(addslashes($_POST['refmaildist']));

    if(isset($_POST['refmaildist']))
    {
        $refmail = $_POST['refmaildist'];
        $iDtAdded = strtotime(gmdate('Y-m-d H:i:s'));
        $row = $userObj->check_exist_mail($refmail);


        if (count($row) > 0) {
            $userid = $row[0]['iUserId'];
         $point="100";
            if ($referralObj->check_existing_user($userid)) {

                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->update_points($userid,$point);
            } else {

                $referralObj->setiUserId($userid);
                $referralObj->setvPoint($point);
                $referralObj->seteStatus("1");
                $referralObj->setiDtAdded($iDtAdded);
                $referralObj->setiDtUpdated($iDtAdded);
                $referralObj->setvReason("Reference");
                $iReffaralId = $referralObj->insert();
            }
        }
    }


    $userObj->setiUserRoleId($_POST['roleid']);
    $userObj->setvContact($_POST['vContact']);
    $userObj->setePlanType("0");
    $userObj->seteActivePaidPlan("0");
    $iExpireTime = strtotime("+30 day", strtotime(date('Y-m-d 23:59:59')));
    $userObj->setiExpireTime($iExpireTime);
    $userObj->setiUserTypeId("2");
    $userObj->setiParentId("0");
    $userObj->setiPlanId("6");
    $userObj->setfAmountPaid("0.00");


    $iUserId = $userObj->insert();

    $store_count = "update user set iStoreCount= CASE WHEN iStoreCount>=0 THEN iStoreCount +1 ELSE 0 END where iUserId in ($iUserId)";
    $obj->sql_query($store_count);

    $user_settingsObj->setiUserId($iUserId);
    $user_settingsObj->seteNotification('1');
    $iUserSettingsId = $user_settingsObj->insert();
    $mail = $_POST['vEmail'];
    $vStoreUniqueId = $_POST['vStoreUniqueId'];
    $DATA = array('name' => $vStoreUniqueId);
       echo $generalfuncobj->get_SC_SEND_mail('register', $DATA, $mail, 'Welcome To Service Calibrate');
   header("Location:autologin.php?iId=$iUserId");    
    //header("Location:login.php");
    exit;
} else if ($SIGNUP == "Get_Unique_Email") {
    global $generalfuncobj;
    $email = $_POST['email'];
    $nid = $_REQUEST['nid'];
    $vemail = $_REQUEST['vEmail'];
    $result = $generalfuncobj->get_unique("user", 'vEmail', $email);
    if ($email == "") {
        ?>
        <span style="color:#a94442; font-size: 15px;font-weight: bold"
              id="error_msg<?php echo $nid ?>"><? echo "Please Enter Email " ?></span>
    <?php
    } else
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // $nameErr = "Only letters and white space allowed";
            ?>
            <span style="color:#a94442; font-size: 15px;font-weight: bold"
                  id="error_msg<?php echo $nid ?>"><? echo "Please Enter valid Email" ?></span>
        <?php

        } else if ($result) {

            ?>
            <span style="font-size: 15px;font-style: oblique" id="error_msg<?php echo $nid ?>"><? echo "" ?></span>

            <?php
            exit;
        } else {

            ?>
            <span style="color:#a94442; font-size: 15px;font-weight: bold"
                  id="error_msg<?php echo $nid ?>"><?php echo "Email Already Exist"; ?></span>

            <?php
            exit;
        }
} else if ($SIGNUP == "Get_Unique_Account") {
    global $generalfuncobj;
    $account = $_POST['accountname'];
    $aid = $_POST['aid'];
    $result = $generalfuncobj->get_unique("user", "vStoreUniqueId", $account);

    if ($account == "") {
        if ($aid == '13') {

            ?>
            <span style="color:#a94442; font-size: 15px;font-weight: bold"
                  id="error_msg<?php echo $aid ?>"><?php echo " Please Enter Chain/Group name " ?></span>
        <?php
        } else
            if ($aid == '16' || $aid == '15') {
                ?>
                <span style="color:#a94442; font-size: 15px;font-weight: bold"
                      id="error_msg<?php echo $aid ?>"><?php echo " Please Enter company name " ?></span>

            <?php
            } elseif ($aid == '11' || $aid == '12' || $aid == '14') {
                ?>
                <span style="color:#a94442; font-size: 15px;font-weight: bold"
                      id="error_msg<?php echo $aid ?>"><?php echo " Please Enter account name " ?></span>
            <?
            }
    } else if ($result) {
        ?>
        <span style="font-size: 15px;font-style: normal" id="error_msg<?php echo $aid ?>"><?php echo "  " ?></span>
    <?php

    } else {
        if ($aid == '13') {
            ?>
            <label style="color:#a94442; font-size: 15px;font-weight: bold"
                   id="error_msg<?php echo $aid ?>"><?php echo "Chain/Group Already Exist"; ?></label>
        <?php
        } else {
            if ($aid == '16' || $aid == '15') {

                ?>
                <label style="color:#a94442; font-size: 15px;font-weight: bold"
                       id="error_msg<?php echo $aid ?>"><?php echo "Company Name Already Exist"; ?></label>
            <?php

            } else {
                if ($aid == '11' || $aid == '12' || $aid == '14') {


                    ?>
                    <label style="color:#a94442; font-size: 15px;font-weight: bold"
                           id="error_msg<?php echo $aid ?>"><?php echo "Account Already Exist"; ?></label>
                <?php
                }
            }
        }
    }
}
?>