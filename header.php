<?php
/**
 * Created by PhpStorm.
 * User: chintan
 * Date: 8/4/15
 * Time: 11:41 AM
 */
include_once('include.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="mind" >
    <title>Service Calibrate</title>
    <!--<link rel="shortcut icon" type="assets/image/x-icon" href="images/favicon.png">-->
    <link rel="shortcut icon" type="assets/image" href="<?php echo $site_url; ?>assets/images/favicon.png">
    <link href="<?php echo $site_url; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/custom.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_url; ?>assets/css/morphext.css" rel="stylesheet" type="text/css">

    <style>
        body {
            overflow-x: hidden;
        }

        .mi-invisible {
            visibility: hidden;
        }

        .mi-invisible.mi-visible {
            visibility: visible;
        }

        .zoomOutmi {
            -webkit-animation-name: zoomOutmi;
            animation-name: zoomOutmi;
            /*-webkit-animation-duration: 1s;
            animation-duration: 1s;*/
        }

        .anim-hlpr {
            bottom: 0;
            position: absolute;
        }

        .bounceInRight {
            -webkit-animation-name: bounceInRight;
            animation-name: bounceInRight;
            -webkit-animation-duration: 0.5s;
            animation-duration: 0.5s;
        }

        .fadeInUp {
            -webkit-animation-name: fadeInUp;
            animation-name: fadeInUp;
            -webkit-animation-duration: 0.5s;
            animation-duration: 0.5s;
        }

        .zoomInmi {
            -webkit-animation-name: zoomInmi;
            animation-name: zoomInmi;
            -webkit-animation-duration: 0.2s;
            animation-duration: 0.2s;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo $site_url;?>assets/js/html5shiv.min.js"></script>
    <script src="<?php echo $site_url;?>assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<nav class="navbar navbar-custom">
  <?php
    $currentscript=$_SERVER['SCRIPT_NAME'];
    $getfile=explode("/",$currentscript);

  // pr($getfile);
    if($getfile[2]!="payment_distributor.php" && $getfile[2]!="payment_manufacturer.php" && $getfile[2]!="payment_retailers.php" && $getfile[2]!="transaction.php")
    {
    if(!isset($_SESSION['SC_LOGIN']['USER'])){ ?>


    <div class="login-register-top">
        <div class="container">
            <ul class="navbar-right">
                <li><a href="login.php" class="red">Login</a></li>
                <li><a href="register.php" class="grey37"><b>Register</b></a></li>
            </ul>
        </div>
    </div>
    <?}
    else
    {
       ?><div class="login-register-top">
        <div class="container">
            <ul class="navbar-right">
                <li><a href="login.php" class="red">View Dashboard</a></li>
              </ul>
        </div>
    </div>

   <? } }?>
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">
                <img src="<?php echo $site_url; ?>assets/images/logo.png" alt="Service Calibrate">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden"><a href="#"></a></li>
                <li><a href="index.php">Home</a></li>
                <li><a href="#howitworks" class="hw-wrks-ply">How it Works</a></li>
                <li><a href="index.php#feature">Features</a></li>
                <!--<li><a href="pricing.php">Pricing</a></li>
                <li><a href="#">Learn More</a></li>
                <li><a href="index.php#contact">Contact Us</a></li>-->
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>